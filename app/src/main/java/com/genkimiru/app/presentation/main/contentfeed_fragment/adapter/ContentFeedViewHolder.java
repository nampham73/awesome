package com.genkimiru.app.presentation.main.contentfeed_fragment.adapter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiContentModel;
import com.genkimiru.app.presentation.content_detail.ContentDetailActivity;
import com.genkimiru.app.presentation.main.GlideApp;

import java.util.Locale;

import butterknife.BindView;

import static com.genkimiru.app.base.util.DateUtil.Format.API_FORMAT;
import static com.genkimiru.app.data.model.GenkiContentModel.ContentType.ADVERTISEMENT;
import static com.genkimiru.app.data.model.GenkiContentModel.ContentType.ARTICLE;

public class ContentFeedViewHolder extends BaseSimpleViewHolder<GenkiContentModel> {

    @BindView(R.id.content_feed_main_image_iv)
    ImageView mContentImage;
    @BindView(R.id.content_feed_main_title_tv)
    TextView mTitleTv;
    @BindView(R.id.content_feed_main_date_tv)
    TextView mDateTv;
    @BindView(R.id.content_feed_main_owner_tv)
    TextView mOwnerTv;
    @BindView(R.id.content_feed_main_description_tv)
    TextView mDescriptionTv;
    @BindView(R.id.content_feed_main_advertisement_tv)
    TextView mAdvertisementTv;

    public ContentFeedViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(GenkiContentModel genkiContentModel) {
        GlideApp.with(mContext)
                .load(genkiContentModel.getPicture())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(mContentImage);
        mTitleTv.setText(genkiContentModel.getTitle());
        mTitleTv.setTextColor(genkiContentModel.getType() == ADVERTISEMENT
                ? mContext.getResources().getColor(R.color.black)
                : mContext.getResources().getColor(R.color.colorPrimary));
        Locale currentLocale = mContext.getResources().getConfiguration().locale;
        mDateTv.setText(String.format(
                mContext.getResources().getString(R.string.content_feed_date),
                DateUtil.convertToYearMonthDayString(API_FORMAT, genkiContentModel.getCreatedAt(), currentLocale)
        ));
        mDateTv.setVisibility(genkiContentModel.getType() == ADVERTISEMENT ? View.GONE : View.VISIBLE);
        mDescriptionTv.setText(genkiContentModel.getDescription());
        mAdvertisementTv.setVisibility(genkiContentModel.getType() == ADVERTISEMENT ? View.VISIBLE : View.GONE);
        mOwnerTv.setText(String.format(
                mContext.getResources().getString(R.string.content_feed_owner),
                genkiContentModel.isCreatedByAdmin()
                        ? mContext.getString(R.string.content_feed_genki_text)
                        : genkiContentModel.getProgram_name()
        ));
        mOwnerTv.setVisibility(genkiContentModel.getType() == ADVERTISEMENT ? View.GONE : View.VISIBLE);

        onItemClick(genkiContentModel);
    }

    private void onItemClick(GenkiContentModel genkiContentModel) {
        itemView.setOnClickListener(view -> {
            switch (genkiContentModel.getType()) {
                case ARTICLE:
                    Intent detailIntent = new Intent(itemView.getContext(), ContentDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.Extra.ARTICLE_ID, genkiContentModel.getId());
                    bundle.putString(Constants.Extra.ARTICLE_TYPE, String.valueOf(genkiContentModel.getType()));
                    bundle.putString(Constants.Extra.ARTICLE_TITLE, genkiContentModel.getTitle());
                    detailIntent.putExtras(bundle);
                    itemView.getContext().startActivity(detailIntent);
                    break;
                case ADVERTISEMENT:
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    if (StringUtil.isEmpty(genkiContentModel.getUrl())) {
                        return;
                    }
                    intent.setData(Uri.parse(genkiContentModel.getUrl()));
                    mContext.startActivity(intent);
                    break;
            }
        });
    }
}
