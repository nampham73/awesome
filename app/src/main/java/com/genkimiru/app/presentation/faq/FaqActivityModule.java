package com.genkimiru.app.presentation.faq;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.extension.ExtensionApi;
import com.genkimiru.app.data.repository.bugreport.BugReportRepository;
import com.genkimiru.app.data.repository.bugreport.BugReportRepositoryImp;
import com.genkimiru.app.domain.bugreport.BugReportUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqPresenter;
import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqRouter;
import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqView;

@Module
public class FaqActivityModule {

    @Provides
    @ActivityScope
    FaqView provideView(FaqActivity faqActivity) {
        return faqActivity;
    }

    @Provides
    @ActivityScope
    FaqRouter provideRouter(FaqActivity faqActivity) {
        return faqActivity;
    }

    @Provides
    @ActivityScope
    FaqPresenter providePresenter(Context context,
                                  FaqView faqView,
                                  FaqRouter faqRouter,
                                  BugReportUseCase bugReportUseCase) {
        return new FaqActivityPresenter(context, faqView, faqRouter, bugReportUseCase);
    }

    @Provides
    @ActivityScope
    BugReportRepository provideBugreport(ExtensionApi api) {
        return new BugReportRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    BugReportUseCase provideReportUseCase(SchedulerProvider schedulerProvider, BugReportRepository bugReportRepository) {
        return new BugReportUseCase(schedulerProvider.io(), schedulerProvider.ui(), bugReportRepository);
    }
}
