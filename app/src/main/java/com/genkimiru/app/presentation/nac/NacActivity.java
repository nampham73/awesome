package com.genkimiru.app.presentation.nac;

import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseSliderActivity;
import com.genkimiru.app.base.widget.CustomViewPager;
import com.genkimiru.app.presentation.nac.adapter.NacAdapter;

import javax.inject.Inject;

import butterknife.BindView;

import static com.genkimiru.app.Constants.Extra.ADVICE;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.DAY;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.MONTH;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.WEEK;
import static com.genkimiru.app.presentation.nac.NacActivityContract.NacPresenter;
import static com.genkimiru.app.presentation.nac.NacActivityContract.NacRouter;
import static com.genkimiru.app.presentation.nac.NacActivityContract.NacView;


public class NacActivity extends BaseSliderActivity<NacAdapter, NacPresenter> implements NacView, NacRouter {

    @BindView(R.id.nac_contest_pager)
    CustomViewPager mViewPager;
    @BindView(R.id.nac_contest_tab)
    TabLayout mTabLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    @Inject
    NacAdapter mNacAdapter;

    @Override
    protected void onResume() {
        super.onResume();
        int viewType = getIntent().getIntExtra(ADVICE, 0);
        TabLayout.Tab tab = null;
        switch (viewType) {
            case DAY:
                tab = getTabLayout().getTabAt(NacAdapter.TypeNac.DAY.getIndex());
                break;
            case WEEK:
                tab = getTabLayout().getTabAt(NacAdapter.TypeNac.WEEK.getIndex());
                break;
            case MONTH:
                tab = getTabLayout().getTabAt(NacAdapter.TypeNac.MONTH.getIndex());
                break;
        }

        if (tab == null) {
            return;
        }

        tab.select();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_nac;
    }

    @Override
    protected NacPresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected CustomViewPager getViewPager() {
        return mViewPager;
    }

    @Override
    protected TabLayout getTabLayout() {
        return mTabLayout;
    }

    @Override
    protected NacAdapter getPagerAdapter() {
        return mNacAdapter;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.nac_title);
    }
}
