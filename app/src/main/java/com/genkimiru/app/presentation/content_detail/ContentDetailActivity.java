package com.genkimiru.app.presentation.content_detail;

import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseActivity;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.ContentDetailPresenter;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.ContentDetailRouter;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.ContentDetailView;

import javax.inject.Inject;

import butterknife.BindView;

public class ContentDetailActivity
        extends BaseActivity<ContentDetailPresenter, ContentDetailView>
        implements ContentDetailRouter {

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;


    @Inject
    ContentDetailView mRegisterView;
    @Inject
    ContentDetailPresenter mPresenter;

    @Override
    protected ContentDetailView registerView() {
        return mRegisterView;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_detail_content_feed;
    }

    @Override
    protected ContentDetailPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        if (getIntent().getExtras() == null) {
            return;
        }
        String title = getIntent().getExtras().getString(Constants.Extra.ARTICLE_TITLE);
        mToolbarTitle.setText(title);
    }
}
