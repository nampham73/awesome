package com.genkimiru.app.presentation.other_wear_device;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GFitUserInfoModel;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDevicePresenter;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDeviceRouter;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDeviceView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.IS_LOGIN_STATE;
import static com.genkimiru.app.Constants.Extra.REGISTER_USER_INFO;
import static com.genkimiru.app.Constants.Google.CLIENT_ID;

public class OtherWearDeviceActivity extends SimpleBaseActivity<OtherWearDevicePresenter>
        implements OtherWearDeviceView, OtherWearDeviceRouter {

    private static final int GOOGLEFIT_SIGN_IN = 100;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @Inject
    OtherWearDevicePresenter mPresenter;

    private GFitUserInfoModel mGFitUserInfoModel;
    private GoogleSignInClient mGoogleSignInClient;
    private boolean mIsLoginState;

    @Override
    protected int getContentViewId() {
        return R.layout.register_wear_device_other;
    }

    @Override
    protected OtherWearDevicePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Timber.d("Redirect data: %s", intent.getData());
        if (intent.getData() != null) {
            Uri data = intent.getData();
            String scheme = data.getScheme();
            String code = data.getQueryParameter("code");
            Timber.d("Google: %s", code);
            mPresenter.loginGoogleFit(code);
        }
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.other_wear_device);
        mToolBarUtils.addBackButton();
    }

    @OnClick({R.id.other_wear_device_nike_layout, R.id.other_wear_device_acer_layout,
            R.id.other_wear_device_xiaomi_layout, R.id.other_wear_device_other_layout})
    void goRegister() {
//        loginWithGoogleFit();
        signIn();
    }

    @Override
    public void loginWithGoogleFit() {
        Timber.d("loginWithGoogleFit");
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.Google.AUTH_URI));
        startActivity(browserIntent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CLIENT_ID)
                .requestServerAuthCode(CLIENT_ID)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mIsLoginState = getIntent().getBooleanExtra(IS_LOGIN_STATE, false);
    }

    @Override
    public void openRegisterScreen(GFitUserInfoModel gFitUserInfoModel) {
        this.mGFitUserInfoModel = gFitUserInfoModel;
        Intent intent = new Intent();
        intent.putExtra(REGISTER_USER_INFO, gFitUserInfoModel);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void showProgressBar() {
        showProgress();
    }

    @Override
    public void hideProgressBar() {
        hideProgress();
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLEFIT_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            mPresenter.handleSignInResult(task, mIsLoginState);
        }
    }

    private void signIn() {
        mGoogleSignInClient.signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLEFIT_SIGN_IN);
    }
}
