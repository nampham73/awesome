package com.genkimiru.app.presentation.eula;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseSliderActivity;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.base.widget.CustomViewPager;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.EULA_INDEX;

public class EulaActivity extends BaseSliderActivity<EulaPagerAdapter, EulaContract.EulaPresenter> implements EulaContract.EulaView {

    private static final int DEFAULT_TAB_INDEX = 0;

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeTabLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected CustomViewPager getViewPager() {
        return findViewById(R.id.activity_eula_view_pager);
    }

    @Override
    protected TabLayout getTabLayout() {
        return findViewById(R.id.activity_eula_tab);
    }

    @Override
    protected EulaPagerAdapter getPagerAdapter() {
        return new EulaPagerAdapter(getApplication(), getSupportFragmentManager());
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_eula;
    }

    @Override
    protected EulaContract.EulaPresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    public void setScreenTitle(String title) {
        mToolbarTitle.setText(title);

    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void initializeTabLayout() {
        int index = getIntent().getIntExtra(EULA_INDEX, DEFAULT_TAB_INDEX);
        Timber.d("initializeTabLayout index = %s", index);
        TabLayout.Tab currentTab = mTabLayout.getTabAt(index);
        if (currentTab == null) {
            return;
        }
        setScreenTitleBySelectedTab(currentTab);
        currentTab.select();

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Timber.d("onTabSelected");
                setScreenTitleBySelectedTab(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setScreenTitleBySelectedTab(TabLayout.Tab tab) {
        Timber.d("setScreenTitleBySelectedTab, title = %s", tab.getText());
        if (StringUtil.isEmpty(tab.getText())) {
            return;
        }
        setScreenTitle(tab.getText().toString());
    }
}
