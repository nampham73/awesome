package com.genkimiru.app.presentation.register;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.genkimiru.app.R;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragment;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragment;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragment;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragment;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragment;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragment;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragment;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragment;
import com.genkimiru.app.presentation.register.height_frament.HeightFragment;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragment;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragment;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragment;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragment;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragment;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragment;

import java.sql.Types;

public class
RegisterActivityAdapter extends FragmentPagerAdapter {
    private SparseArray<Fragment> sparseArray;

    public RegisterActivityAdapter(FragmentManager fm) {
        super(fm);
        sparseArray = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int position) {
        Type type = Type.values()[position];
        switch (type) {
            case WEAR_DEVICE:
                return new WearDeviceFragment();
            case BIRTHDAY:
                return new BirthdayFragment();
            case SEX:
                return new SexFragment();
            case WEIGHT:
                return new WeightFragment();
            case HEIGHT:
                return new HeightFragment();
            case BLOODTYPE:
                return new BloodTypeFragment();
            case BLOODPRESSURE:
                return new BloodPressureFragment();
            case ALCOHOL:
                return new AlcoholFragment();
            case SMOKING:
                return new SmokingFragment();
            case HOSPITAL:
                return new HospitalFragment();
            case HEARTBEAT:
                return new HeartBeatFragment();
            case NICKNAME:
                return new NicknameFragment();
            case PROFILE_AVATAR:
                return new ProfileAvatarFragment();
            case ACCOUNT:
                return new AccountFragment();
            case ACCOUNT_COMPLETE:
                return new AccountCompleteFragment();
        }

        return null;
    }

    public enum Type {

        WEAR_DEVICE(0, R.layout.layout_register_wear_device),
        BIRTHDAY(1, R.layout.layout_register_birthday),
        SEX(2, R.layout.layout_register_sex),
        WEIGHT(3, R.layout.layout_register_weight),
        HEIGHT(4, R.layout.layout_register_height),
        BLOODTYPE(5, R.layout.layout_register_bloodtype),
        BLOODPRESSURE(6, R.layout.layout_register_bloodpressure),
        ALCOHOL(7, R.layout.layout_register_alcohol),
        SMOKING(8, R.layout.layout_register_healthy_smoking),
        HOSPITAL(9, R.layout.layout_register_healthy_smoking),
        HEARTBEAT(10, R.layout.layout_register_healthy_heartbeat),
        NICKNAME(11, R.layout.layout_register_nickname),
        PROFILE_AVATAR(12, R.layout.layout_register_healthy_profile_avatar),
        ACCOUNT(13, R.layout.layout_register_healthy_account),
        ACCOUNT_COMPLETE(14, R.layout.layout_register_healthy_complete);

        private int mIndex;
        private int mBackgroundId;

        Type(int index, int mBackgroundId) {
            this.mIndex = index;
            this.mBackgroundId = mBackgroundId;
        }

        public int getIndex() {
            return mIndex;
        }

        public int getBackgroundId() {
            return mBackgroundId;
        }
    }

    @Override
    public int getCount() {
        return Type.values().length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        sparseArray.put(position, (Fragment) object);
        return object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        sparseArray.remove(position);
    }

    public Fragment getActiveFragment(int position) {
        return sparseArray.get(position);
    }

    public int getPositionByType(Type type) {
        int id = type.getIndex();
        if (Type.values().length > 0) {
            for (int i = 0; i < Type.values().length; i++) {
                if (Type.values()[i].mIndex == id) {
                    return i;
                }
            }
        }
        return -1;
    }
}
