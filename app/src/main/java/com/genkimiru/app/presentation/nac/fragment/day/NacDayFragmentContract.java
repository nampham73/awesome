package com.genkimiru.app.presentation.nac.fragment.day;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NacDayFragmentContract {
    interface NacDayPresenter extends BasePresenter{
            void onUpdateNacDay();
    }
    interface NacDayView{
        void onUpdateSuccess();
        void onUpdateError();
    }
    interface NacDayRouter{

    }
}
