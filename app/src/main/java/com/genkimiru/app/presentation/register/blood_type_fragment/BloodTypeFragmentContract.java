package com.genkimiru.app.presentation.register.blood_type_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface BloodTypeFragmentContract {

    interface BloodTypePresenter extends BasePresenter {

    }

    interface BloodTypeFragmentView {

    }

    interface BloodTypeFragmentRouter {

    }
}
