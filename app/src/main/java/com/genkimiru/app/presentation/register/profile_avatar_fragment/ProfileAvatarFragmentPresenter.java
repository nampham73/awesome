package com.genkimiru.app.presentation.register.profile_avatar_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarFragmentRouter;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarFragmentView;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarPresenter;

public class ProfileAvatarFragmentPresenter extends BasePresenterImp<ProfileAvatarFragmentView,
        ProfileAvatarFragmentRouter> implements ProfileAvatarPresenter {

    private UserUseCase mUserUseCase;

    public ProfileAvatarFragmentPresenter(Context context, ProfileAvatarFragmentView profileAvatarFragmentView,
                                          ProfileAvatarFragmentRouter profileAvatarFragmentRouter, UserUseCase userUseCase) {
        super(context, profileAvatarFragmentView, profileAvatarFragmentRouter);
        mUserUseCase = userUseCase;
    }
}
