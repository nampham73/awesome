package com.genkimiru.app.presentation.chart;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.presentation.chart.adapter.ChartDateAdapter;
import com.genkimiru.app.presentation.nac.NacActivity;
import com.github.mikephil.charting.charts.Chart;
import com.jakewharton.rxbinding2.support.design.widget.RxTabLayout;
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
import static com.genkimiru.app.Constants.Extra.ADVICE;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.DAY;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.MONTH;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.WEEK;
import static java.util.Locale.JAPANESE;

public abstract class BaseChartActivity<TPresenter extends BasePresenter, TChart extends Chart> extends SimpleBaseActivity<TPresenter> {

    @BindView(R.id.calendar_rcv)
    protected RecyclerView mDateRcv;
    @BindView(R.id.calendar_indicator_tv)
    protected TextView mDateIndicator;
    @BindView(R.id.chart_tab_layout)
    protected TabLayout mChartTabLayout;
    @BindView(R.id.date_tv)
    TextView mDateTv;
    @BindView(R.id.summarize_tv)
    TextView mSummarizeTv;

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    protected TextView mToolbarTitle;

    protected TChart mMainChart;

    private LinearSnapHelper mSnapHelper;
    private LinearLayoutManager mLinearLayoutManager;
    private ChartDateAdapter mChartDateAdapter;
    private int mViewType = DAY;
    private Calendar mCurrentCalendar;
    private Locale mCurrentLocale;

    public @interface ViewType {
        int DAY = 1;
        int WEEK = 2;
        int MONTH = 3;
    }

    @OnClick(R.id.advice_btn)
    protected void onAdviceButtonClick() {
        Intent intent = new Intent(this, NacActivity.class);
        intent.putExtra(ADVICE, mViewType);
        startActivity(intent);
    }

    @OnClick(R.id.now_btn)
    protected void onNowButtonClick() {
        scrollToNow();
    }

    protected Calendar getCurrentCalendar() {
        return mCurrentCalendar;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentLocale = getResources().getConfiguration().locale;
        mMainChart = getMainChart();
        initializeToolbar(getToolbarTitle(), getMainColor());
        initRecyclerView();
        initializeTabLayout(getMainColor());
        initializeMainChart();
    }

    protected void onDayChange(Calendar calendar){
        mCurrentCalendar = calendar;
        String date = DateUtil.getWeekDayMonthYearString(calendar, getResources().getConfiguration().locale);
        mDateTv.setText(date);

    }

    protected void onWeekChange(Calendar calendar) {
        mCurrentCalendar = calendar;
        Calendar descriptionCalendar = (Calendar) calendar.clone();
        String year = String.valueOf(descriptionCalendar.get(Calendar.YEAR));
        String month = mCurrentLocale.getLanguage().equals(JAPANESE.getLanguage())
                ? String.valueOf(descriptionCalendar.get(Calendar.MONTH) + 1)
                : DateUtil.getFullMonthString(descriptionCalendar);
        String startDay = String.valueOf(descriptionCalendar.get(Calendar.DAY_OF_MONTH));
        descriptionCalendar.add(Calendar.DAY_OF_MONTH, 6);
        String endDay = String.valueOf(descriptionCalendar.get(Calendar.DAY_OF_MONTH));

        mDateTv.setText(String.format(getString(R.string.chart_detail_weeks_description), year, month, startDay, endDay));
    }

    protected void onMonthChange(Calendar calendar){
        mCurrentCalendar = calendar;
        mDateTv.setText(DateUtil.getYearMonth(calendar, mCurrentLocale));
    }

    protected void showSummarizeText() {
        mSummarizeTv.setVisibility(View.VISIBLE);
        mSummarizeTv.setText(R.string.chart_detail_summarize);
    }

    protected void showAverageText() {
        mSummarizeTv.setVisibility(View.VISIBLE);
        mSummarizeTv.setText(R.string.chart_detail_average);
    }

    protected void hideSummarizeAndAverageText() {
        mSummarizeTv.setVisibility(View.GONE);
    }

    protected abstract int getMainColor();

    protected abstract TChart getMainChart();

    protected abstract String getToolbarTitle();

    protected abstract void initializeMainChart();

    protected void initializeToolbar(String title, @ColorRes int textColor) {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(title);
        mToolbarTitle.setTextColor(getResources().getColor(textColor));
        if (mToolbar.getNavigationIcon() == null) {
            return;
        }
        mToolbar.getNavigationIcon().setColorFilter(getResources().getColor(textColor), PorterDuff.Mode.SRC_ATOP);
    }

    protected void initializeTabLayout(@ColorRes int color) {
        mChartTabLayout.addTab(mChartTabLayout.newTab().setText(R.string.chart_detail_days));
        mChartTabLayout.addTab(mChartTabLayout.newTab().setText(R.string.chart_detail_weeks));
        mChartTabLayout.addTab(mChartTabLayout.newTab().setText(R.string.chart_detail_months));
        mChartTabLayout.setSelectedTabIndicatorColor(getResources().getColor(color));
        mChartTabLayout.setTabTextColors(
                getResources().getColor(android.R.color.darker_gray),
                getResources().getColor(color)
        );

        RxTabLayout.selections(mChartTabLayout)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tab -> {
                    mChartDateAdapter.clear();
                    switch (tab.getPosition()) {
                        case 0:
                            List<Calendar> days = DateUtil.getDaysUntilNow(2);
                            days.add(null);
                            days.add(null);
                            days.add(null);
                            mChartDateAdapter.setViewType(ChartDateAdapter.DateType.DAY);
                            mChartDateAdapter.items(days);
                            mViewType = DAY;
                            break;
                        case 1:
                            List<Calendar> weeks = DateUtil.getWeeksUntilNow(2);
                            weeks.add(null);
                            weeks.add(null);
                            weeks.add(null);
                            mChartDateAdapter.setViewType(ChartDateAdapter.DateType.DAY);
                            mChartDateAdapter.items(weeks);
                            mViewType = WEEK;
                            break;
                        case 2:
                            List<Calendar> months = DateUtil.getMonthsUntilNow(2);
                            months.add(null);
                            months.add(null);
                            months.add(null);
                            mChartDateAdapter.setViewType(ChartDateAdapter.DateType.MONTH);
                            mChartDateAdapter.items(months);
                            mViewType = MONTH;
                            break;
                    }
                    mChartDateAdapter.notifyDataSetChanged();
                    scrollToNow();
                });
    }

    private void initRecyclerView() {
        mChartDateAdapter = new ChartDateAdapter(getApplicationContext());
        mLinearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mSnapHelper = new LinearSnapHelper();

        mDateRcv.setLayoutManager(mLinearLayoutManager);
        mDateRcv.setAdapter(mChartDateAdapter);
        mDateRcv.setOnFlingListener(null);
        mSnapHelper.attachToRecyclerView(mDateRcv);

        RxRecyclerView.scrollEvents(mDateRcv)
                .skip(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recyclerViewScrollEvent -> {
                    mDateIndicator.setText("");
                });

        RxRecyclerView.scrollStateChanges(mDateRcv)
                .filter(state -> state == SCROLL_STATE_IDLE)
                .delay(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(state -> onDateChange());
    }

    private void onDateChange() {
        View view = mSnapHelper.findSnapView(mLinearLayoutManager);
        int position = mDateRcv.getChildAdapterPosition(view);
        Calendar calendar = mChartDateAdapter.getItemAt(position);
        mDateIndicator.setText(
                mViewType == MONTH
                        ? String.valueOf(DateUtil.getMonthString(calendar))
                        : String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))
        );

        if (calendar == mCurrentCalendar) {
            return;
        }

        mCurrentCalendar = calendar;
        switch (mViewType) {
            case DAY:
                onDayChange(calendar);
                break;
            case WEEK:
                onWeekChange(calendar);
                break;
            case MONTH:
                onMonthChange(calendar);
                break;
        }
    }

    private void scrollToNow() {
        if(mDateRcv == null || mChartDateAdapter == null) {
            return;
        }

        mDateRcv.scrollToPosition(mChartDateAdapter.getItemCount() - 1);
        Single.just(true)
                .delay(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integer -> onDateChange());
    }
}
