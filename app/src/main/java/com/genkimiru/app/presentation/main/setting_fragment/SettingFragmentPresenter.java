package com.genkimiru.app.presentation.main.setting_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingPresenter;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingRouter;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingView;

public class SettingFragmentPresenter extends BasePresenterImp<SettingView, SettingRouter>
        implements SettingPresenter{

    public SettingFragmentPresenter(Context context, SettingView settingView, SettingRouter settingRouter) {
        super(context, settingView, settingRouter);
    }
}
