package com.genkimiru.app.presentation.register;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseSliderActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.base.widget.CustomViewPager;
import com.genkimiru.app.data.model.GFitUserInfoModel;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.intro.IntroActivity;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivity;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterPresenter;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterRouter;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterView;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragment;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragment;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Argument.ARG_HEALTH_SOURCE;
import static com.genkimiru.app.Constants.Extra.IS_LOGIN_STATE;
import static com.genkimiru.app.Constants.Extra.REGISTER_USER_INFO;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GET_PHOTO;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GOOGLE_FIT_LOGIN;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_OTHER_DEVICE;

public class RegisterActivity extends BaseSliderActivity<RegisterActivityAdapter, RegisterPresenter>
        implements RegisterView, RegisterRouter {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.register_navigation_layout)
    RelativeLayout mNavigationLayout;
    @BindView(R.id.activity_register_view_pager)
    CustomViewPager mViewPager;
    @BindView(R.id.activity_register_tab_dots)
    TabLayout mTab;

    @Inject
    RegisterPresenter mPresenter;

    private Uri mAvatarUri;
    private boolean isHealthSourceCase;
    private boolean isLoginState;

    @Inject
    RegisterUserRequest mRegisterUserRequest;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (getIntent() != null) {
            isHealthSourceCase = getIntent().getBooleanExtra(ARG_HEALTH_SOURCE, false);
        } else if (savedInstanceState != null) {
            isHealthSourceCase = savedInstanceState.getBoolean(ARG_HEALTH_SOURCE, false);
        }
        super.onCreate(savedInstanceState);
        initialize();
        mRegisterUserRequest.mIsLoginFlag = getIntent().getBooleanExtra(IS_LOGIN_STATE, false);
        checkLoginFacebook();
        checkLoginGoogle();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getAction();
        if (action != null && action.equals(Intent.ACTION_VIEW)) {
            Timber.d("Redirect data: %s", intent.getData());
            if (intent.getData() != null) {
                Uri data = intent.getData();
                String code = data.getQueryParameter("code");
                Timber.d("Fitbitcode: %s", code);
                mPresenter.loginFitbit(code);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_register;
    }

    @Override
    protected RegisterPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected CustomViewPager getViewPager() {
        return mViewPager;
    }

    @Override
    protected TabLayout getTabLayout() {
        return mTab;
    }

    @Override
    protected RegisterActivityAdapter getPagerAdapter() {
        return new RegisterActivityAdapter(getSupportFragmentManager());
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(isHealthSourceCase ? R.string.setting_health_data_source : R.string.register_title);
    }

    @Override
    public void hideNavigationButton() {
        mNavigationLayout.setVisibility(View.GONE);
    }

    @Override
    public void showNavigationButton() {
        mNavigationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBar() {
        showProgress();
    }

    @Override
    public void hideProgressBar() {
        hideProgress();
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void disableSwipe() {
        mViewPager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.left);
    }

    @Override
    public void enableSwipe() {
        mViewPager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.all);
    }

    @OnClick(R.id.register_next_button)
    public void nextPage() {
        if (isHealthSourceCase) {
            executeHealthSourceSetting(Constants.HealthSource.SMART_PHONE);
            return;
        }
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
    }

    @OnClick(R.id.register_skip_button)
    void skipAll() {
        TabLayout.Tab tab = getTabLayout().getTabAt(RegisterActivityAdapter.Type.ACCOUNT.getIndex());
        if (tab == null) {
            return;
        }
        tab.select();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("requestCode = " + requestCode + ", resultCode = " + resultCode);

        switch (requestCode) {
            case REQUEST_CODE_GET_PHOTO:
                if (data == null) {
                    return;
                }
                mAvatarUri = data.getData();
                Timber.w("croppedPhoto Uri = " + mAvatarUri);
                break;
            case REQUEST_CODE_OTHER_DEVICE:
                Timber.d("Go birthday screen");
                //openBirthdayRegisterScreen();
                if (resultCode == Activity.RESULT_OK) {
                    if (!isHealthSourceCase) initialDefaultValue(data);
                    else executeHealthSourceSetting(Constants.HealthSource.GOOGLE_FIT);
                }
                break;
            case REQUEST_CODE_GOOGLE_FIT_LOGIN:
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                mPresenter.handleSignInResult(task);
            default:
                break;
        }
    }

    public void sendActivityForResult() {
        Intent intent = new Intent(this, OtherWearDeviceActivity.class);
        intent.putExtra(IS_LOGIN_STATE, mRegisterUserRequest.mIsLoginFlag);
        startActivityForResult(intent, REQUEST_CODE_OTHER_DEVICE);
    }

    public Uri getAvatarUri() {
        return mAvatarUri;
    }

    private void initialize() {
        initializeViewPager();
        initializeTabLayout();
    }

    private void initializeViewPager() {
        mViewPager.setOffscreenPageLimit(15);
        mViewPager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.left);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Do nothing
            }

            @Override
            public void onPageSelected(int position) {
                hideKeyboard(getCurrentFocus());
                Fragment fragment = mPagerAdapter.getItem(position);

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(fragment instanceof WearDeviceFragment);
                }

                if (fragment instanceof WearDeviceFragment
                        || fragment instanceof AccountFragment
                        || fragment instanceof AccountCompleteFragment) {
                    mViewPager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.left);
                    hideNavigationButton();
                    return;
                }
                mViewPager.setAllowedSwipeDirection(CustomViewPager.SwipeDirection.all);
                showNavigationButton();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initializeTabLayout() {
        if (isHealthSourceCase) {
            mTabLayout.setVisibility(View.GONE);
            return;
        }
        mTabLayout.setVisibility(View.VISIBLE);
        LinearLayout tabStrip = ((LinearLayout) mTabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> true);
        }
    }

    @Override
    public void openBirthdayRegisterScreen() {
        if (isHealthSourceCase) {
            executeHealthSourceSetting(mRegisterUserRequest.health_source);
            return;
        }

        Timber.d("register request: %s", mRegisterUserRequest.birthday);
        TabLayout.Tab tab = getTabLayout().getTabAt(RegisterActivityAdapter.Type.BIRTHDAY.getIndex());
        if (tab == null) {
            return;
        }
        tab.select();
    }

    public void loginGobe(String email, String password) {
        mPresenter.loginGobe(email, password);
    }

    private void initialDefaultValue(Intent data) {
        if (data == null) return;
        GFitUserInfoModel gFitUserInfoModel = (GFitUserInfoModel) data.getSerializableExtra(REGISTER_USER_INFO);
        if (gFitUserInfoModel == null) {
            return;
        }
        showProgress();
        Single.just(true).delay(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    hideProgress();
                    if (StringUtil.isEmpty(mRegisterUserRequest.first_name)) {
                        mRegisterUserRequest.first_name = gFitUserInfoModel.getGiven_name();
                    }
                    if (StringUtil.isEmpty(mRegisterUserRequest.last_name)) {
                        mRegisterUserRequest.last_name = gFitUserInfoModel.getFamily_name();
                    }
                    mRegisterUserRequest.gender = gFitUserInfoModel.getGender().toLowerCase().equals("male") ? 1 : 2;
                    if (StringUtil.isEmpty(mRegisterUserRequest.avatarPath)) {
                        mRegisterUserRequest.avatarPath = gFitUserInfoModel.getAvatarPath();
                    }
                    mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.GOOGLE_FIT.getValue();
                    openBirthdayRegisterScreen();
                });
    }

    @Override
    public void onBackPressed() {
        if (isHealthSourceCase) {
            super.onBackPressed();

            return;
        }
        Intent intent = new Intent(RegisterActivity.this, IntroActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void executeHealthSourceSetting(int healthSource) {
        int currentHealthSource = mGenkiUserInfoModel.getHealthSource();
        if (currentHealthSource == healthSource) {
            Timber.d("Current health source is: %d, we skip change now", healthSource);
            return;
        }
        int position = mPagerAdapter.getPositionByType(RegisterActivityAdapter.Type.WEAR_DEVICE);
        Fragment fragment = mPagerAdapter.getActiveFragment(position);
        if (fragment instanceof WearDeviceFragment) {
            showProgressBar();
            ((WearDeviceFragment) fragment).requestHighLightLoginType(healthSource);
        }
    }

    private void checkLoginFacebook() {
        LoginUserInfoModel loginUserInfoModel = (LoginUserInfoModel) getIntent().getSerializableExtra(Constants.Extra.DETAIL_LOGIN_FACEBOOK);
        if (loginUserInfoModel == null) {
            return;
        }
        mPresenter.loginFacebook(loginUserInfoModel);
    }

    private void checkLoginGoogle() {
        LoginUserInfoModel loginUserInfoModel = (LoginUserInfoModel) getIntent().getSerializableExtra(Constants.Extra.DETAIL_LOGIN_GOOGLE);
        if (loginUserInfoModel == null) {
            return;
        }
        mPresenter.loginGoogle(loginUserInfoModel);
    }

    @Override
    public void hideProgress() {
        if (isHealthSourceCase) return;
        super.hideProgress();
    }

    @Override
    public void forceHideProgress() {
        super.hideProgress();
    }
}
