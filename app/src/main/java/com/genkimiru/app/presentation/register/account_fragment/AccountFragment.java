package com.genkimiru.app.presentation.register.account_fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.eula.EulaActivity;
import com.genkimiru.app.presentation.eula.EulaPagerAdapter;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountFragmentRouter;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountFragmentView;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountPresenter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.EULA_INDEX;

public class AccountFragment extends SimpleBaseFragment<AccountPresenter> implements AccountFragmentView, AccountFragmentRouter {

    private RegisterActivity mRegisterActivity;
    private boolean mIsValidUsernameInput;
    private boolean mIsValidUsernameLength;
    private boolean mIsValidEmailInput;
    private boolean mIsValidEmailPattern;
    private boolean mIsValidPasswordInput;
    private boolean mIsValidPasswordLength;

    @BindView(R.id.register_account_username_edt)
    EditText mUsernameEdt;
    @BindView(R.id.register_account_email_edt)
    EditText mEmailEdt;
    @BindView(R.id.register_account_password_edt)
    EditText mPasswordEdt;
    @BindView(R.id.register_account_lastname_edt)
    EditText mLastNameEdt;
    @BindView(R.id.register_account_firstname_edt)
    EditText mFirstNameEdt;
    @BindView((R.id.register_agree_eula_cbx))
    CheckBox mEulaCheckbox;
    @BindView(R.id.register_account_next_btn)
    Button mNextBtn;


    @Inject
    AccountPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            return;
        }
        initialDefaultValue();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_healthy_account;
    }

    @Override
    protected AccountPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEnableNextButton(false);
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mUsernameEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(username -> {
                    Timber.d("username: %s", username.toString());
                    mIsValidUsernameInput = mPresenter.isValidUsername(username.toString().trim());
                    mIsValidUsernameLength = mPresenter.isValidUsernameLength(username.toString().trim());

                    mRegisterUserRequest.username = mUsernameEdt.getText().toString().trim();

                    setEnableNextButton(mIsValidUsernameInput
                            && mIsValidPasswordInput
                            && mIsValidEmailInput);
                });

        RxTextView.textChanges(mEmailEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(email -> {
                    Timber.d("email: %s", email.toString());
                    mIsValidEmailInput = mPresenter.isValidEmail(email.toString().trim());
                    mIsValidEmailPattern = mPresenter.isValidEmailPattern(email.toString().trim());

                    mRegisterUserRequest.email = mEmailEdt.getText().toString().trim();

                    setEnableNextButton(mIsValidUsernameInput
                            && mIsValidPasswordInput
                            && mIsValidEmailInput);
                });

        RxTextView.textChanges(mPasswordEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(password -> {
                    Timber.d("password: %s", password.toString());
                    mIsValidPasswordInput = mPresenter.isValidPassword(password.toString().trim());
                    mIsValidPasswordLength = mPresenter.isValidPasswordLength(password.toString().trim());

                    mRegisterUserRequest.password = mPasswordEdt.getText().toString().trim();

                    setEnableNextButton(mIsValidUsernameInput
                            && mIsValidPasswordInput
                            && mIsValidEmailInput);
                });

        RxTextView.textChanges(mLastNameEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(lastname -> {
                    Timber.d("lastname: %s", lastname.toString());
                    mRegisterUserRequest.last_name = mLastNameEdt.getText().toString().trim();

                    setEnableNextButton(mIsValidUsernameInput
                            && mIsValidPasswordInput
                            && mIsValidEmailInput);
                });
        RxTextView.textChanges(mFirstNameEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(firstname -> {
                    Timber.d("firstname: %s", firstname.toString());
                    mRegisterUserRequest.first_name = mFirstNameEdt.getText().toString().trim();

                    setEnableNextButton(mIsValidUsernameInput
                            && mIsValidPasswordInput
                            && mIsValidEmailInput);
                });

        RxCompoundButton.checkedChanges(mEulaCheckbox)
                .subscribe(isChecked -> {
                    Timber.d("Is agree eula checked: %s", isChecked);
                });
    }

    @Override
    public void openEulaScreen(int index) {
        Timber.d("openEulaScreen index = %s", index);
        Intent intent = new Intent(getContext(), EulaActivity.class);
        intent.putExtra(EULA_INDEX, index);
        startActivity(intent);
    }

    @OnClick(R.id.register_personal_policy_tv)
    void onPersonalPolicyClick() {
        openEulaScreen(EulaPagerAdapter.TypeView.PRIVACY_POLICY.getIndex());
    }

    @OnClick(R.id.register_terms_of_use_tv)
    void onTermsOfUseClick() {
        openEulaScreen(EulaPagerAdapter.TypeView.TERMS_OF_USE.getIndex());
    }

    @OnClick(R.id.register_account_next_btn)
    void onOpenAccountCompleteScreen() {

        if (!mEulaCheckbox.isChecked()) {
            showMessageDialog(mRegisterActivity.getString(R.string.login_agree_term_message));
            return;
        }

        if(!mIsValidUsernameLength) {
            showMessageDialog(mRegisterActivity.getString(R.string.register_invalid_username_length));
            return;
        }

        if(!mIsValidPasswordLength) {
            showMessageDialog(mRegisterActivity.getString(R.string.register_invalid_password_length));
            return;
        }

        if(!mIsValidEmailPattern) {
            showMessageDialog(mRegisterActivity.getString(R.string.register_invalid_email));
            return;
        }

        mRegisterActivity.nextPage();
    }

    private void initialDefaultValue() {
        if (StringUtil.isNotEmpty(mRegisterUserRequest.last_name)) {
            mLastNameEdt.setText(mRegisterUserRequest.last_name);
        }
        if (StringUtil.isNotEmpty(mRegisterUserRequest.first_name)) {
            mFirstNameEdt.setText(mRegisterUserRequest.first_name);
        }
        if(StringUtil.isNotEmpty(mRegisterUserRequest.email)) {
            mEmailEdt.setText(mRegisterUserRequest.email);
        }
    }

    private void setEnableNextButton(boolean isEnable) {
        mNextBtn.setEnabled(isEnable);
        mNextBtn.setTextColor(isEnable
                ? getResources().getColor(android.R.color.white)
                : getResources().getColor(R.color.transparent_20_black));
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(mRegisterActivity.getSupportFragmentManager());
    }
}
