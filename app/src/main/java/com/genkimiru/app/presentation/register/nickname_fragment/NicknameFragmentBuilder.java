package com.genkimiru.app.presentation.register.nickname_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class NicknameFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = NicknameFragmentModule.class)
    abstract NicknameFragment contestFragment();
}
