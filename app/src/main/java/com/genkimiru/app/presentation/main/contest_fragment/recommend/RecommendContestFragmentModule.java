package com.genkimiru.app.presentation.main.contest_fragment.recommend;

import android.content.Context;

import com.genkimiru.app.base.scope.ChildFragmentScope;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestContract.RecommendContestPresenter;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestContract.RecommendContestRouter;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestContract.RecommendContestView;

import dagger.Module;
import dagger.Provides;

@Module
public class RecommendContestFragmentModule {


    @ChildFragmentScope
    @Provides
    RecommendContestPresenter providePresenter(Context context, RecommendContestView view, RecommendContestRouter router, ContestUseCase useCase) {
        return new RecommendContestFragmentPresenter(context, view, router, useCase);
    }

    @ChildFragmentScope
    @Provides
    RecommendContestView provideView(RecommendContestFragment view) {
        return view;
    }

    @ChildFragmentScope
    @Provides
    RecommendContestRouter provideRouter(RecommendContestFragment router) {
        return router;
    }

    @ChildFragmentScope
    @Provides
    ContestAdapter provideAdapter(Context context) {
        return new ContestAdapter(context);
    }
}
