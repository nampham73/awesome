package com.genkimiru.app.presentation.edit_profile;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import java.io.File;


public interface EditProfileActivityContract {

    interface EditProfilePresenter extends BasePresenter {
        void updateUserInfo(String nickname, String firstName, String lastName, String email);
        void updateAvatar(File avatar);
    }

    interface EditProfileView {
        void onUpdateUserInfoSuccess();
        void onUpdateUserInfoError(String message);
        void showProgressBar();
        void hideProgressBar();
    }

    interface EditProfileRouter {
        void openUpdateAlcohol();
        void openUpdateBirthday();
        void openUpdateBloodPressure();
        void openUpdateBloodType();
        void openUpdateHeartBeat();
        void openUpdateHeight();
        void openUpdateHospital();
        void openUpdateSmoking();
        void openUpdateWeight();
        void openUpdateGender();
        void openDialogUpdateUserInfo();
        void openDialogUpdateLipitInfo();
        void openDialogUpdateLiverInfo();
        void openDialogUpdateSugar();
        void openDialogUpdateUrinaryProtein();
        void openUpdateAvatar();
    }
}
