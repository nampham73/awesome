package com.genkimiru.app.presentation.setting_notification;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.setting_notification.NotificationSettingActivityContract.*;

@Module
public class NotificationSettingActivityModule {

    @Provides
    @ActivityScope
    NotificationSettingView provideView(NotificationSettingActivity notificationSettingActivity) {
        return notificationSettingActivity;
    }

    @Provides
    @ActivityScope
    NotificationSettingRouter provideRouter(NotificationSettingActivity notificationSettingActivity) {
        return notificationSettingActivity;
    }

    @Provides
    @ActivityScope
    NotificationSettingPresenter providePresenter(Context context,
                                                  NotificationSettingView view,
                                                  NotificationSettingRouter router,
                                                  UserUseCase userUseCase) {
        return new NotificationSettingActivityPresenter(context, view, router, userUseCase);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
