package com.genkimiru.app.presentation.chat_with_watson.WatsonAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.data.model.WatsonMessageModel;

import java.util.ArrayList;

public class WatsonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int SELF = 100;
    private ArrayList<WatsonMessageModel> messageArrayList;

    public WatsonAdapter(ArrayList<WatsonMessageModel> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }

    public WatsonAdapter() {
        messageArrayList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        if (viewType == SELF) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_self, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_watson, parent, false);
        }
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        WatsonMessageModel message = messageArrayList.get(position);
        message.setMessage(message.getMessage());
        ((ViewHolder) holder).message.setText(message.getMessage());
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        WatsonMessageModel message = messageArrayList.get(position);
        if (message.getId() == WatsonMessageModel.MessageType.MESSAGE_SELF) {
            return SELF;
        }
        return position;
    }

    public void set(ArrayList<WatsonMessageModel> items) {
        this.messageArrayList = items;
        notifyDataSetChanged();
    }

    public void add(WatsonMessageModel item) {
        if (messageArrayList != null) {
            messageArrayList.add(item);
            notifyItemInserted(messageArrayList.size());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message;

        public ViewHolder(View view) {
            super(view);
            message = (TextView) itemView.findViewById(R.id.message);
        }
    }
}
