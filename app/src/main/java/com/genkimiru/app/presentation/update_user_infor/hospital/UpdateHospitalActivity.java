package com.genkimiru.app.presentation.update_user_infor.hospital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.SurgeryStatus.NO;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.SurgeryStatus.YES;
import static com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityContract.UpdateHospitalPresenter;
import static com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityContract.UpdateHospitalRouter;
import static com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityContract.UpdateHospitalView;

public class UpdateHospitalActivity extends SimpleBaseActivity<UpdateHospitalPresenter>
        implements UpdateHospitalView, UpdateHospitalRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.checkbox_hospital_yes)
    RadioButton mSurgeryStatusYes;
    @BindView(R.id.checkbox_hospital_no)
    RadioButton mSurgeryStatusNo;

    private MenuItem mApplyMenuItem;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateHospitalPresenter mUpdateHospitalPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_healthy_hospital;
    }

    @Override
    protected UpdateHospitalPresenter registerPresenter() {
        return mUpdateHospitalPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateHospitalPresenter.updateHospital(mUserInfoRequest.getSurgeryStatus());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.checkbox_hospital_yes, R.id.checkbox_hospital_no})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.checkbox_hospital_yes:
                if (checked) {
                    mUserInfoRequest.setSurgeryStatus(1);
                    Timber.d("Surgery status: 0");
                }
                break;
            case R.id.checkbox_hospital_no:
                if (checked) {
                    mUserInfoRequest.setSurgeryStatus(0);
                    Timber.d("Surgery status: 1");
                }
                break;
        }
        setEnabledMenuApply(true);
    }

    private void initialDefaultValue() {
        switch (mGenkiUserInfoModel.getSurgeryStatus()) {
            case YES:
                mSurgeryStatusYes.setChecked(true);
                break;
            case NO:
                mSurgeryStatusNo.setChecked(true);
                break;
            default:
                break;
        }

    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
