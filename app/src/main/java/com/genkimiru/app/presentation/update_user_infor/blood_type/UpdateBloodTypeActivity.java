package com.genkimiru.app.presentation.update_user_infor.blood_type;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnItemSelected;
import timber.log.Timber;

import static com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityContract.UpdateBloodTypePresenter;
import static com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityContract.UpdateBloodTypeRouter;
import static com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityContract.UpdateBloodTypeView;

public class UpdateBloodTypeActivity extends SimpleBaseActivity<UpdateBloodTypePresenter>
        implements UpdateBloodTypeView, UpdateBloodTypeRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_bloodtype_spn)
    Spinner spnBloodType;

    private MenuItem mApplyMenuItem;
    int valueSpinner;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateBloodTypePresenter mUpdateBloodTypePresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;
    private String[] array;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        array = getResources().getStringArray(R.array.blood_type);
        setBackgroundLayout();
        showSpinnerBloodType();
        initDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_bloodtype;
    }

    @Override
    protected UpdateBloodTypePresenter registerPresenter() {
        return mUpdateBloodTypePresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateBloodTypePresenter.updateBloodType(mUserInfoRequest.getBlood_type());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("unchecked")
    public void showSpinnerBloodType() {
        Timber.d("showSpinnerBlood");
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, array) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Color.WHITE);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                return tv;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Color.BLACK);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                return tv;
            }

        };
        spnBloodType.setAdapter(arrayAdapter);
    }

    @OnItemSelected(R.id.register_bloodtype_spn)
    public void spinnerItemSelected(Spinner spinner, int position) {
        if (array[position] != null && !array[position].equals(mGenkiUserInfoModel.getBloodType())) {
            setEnabledMenuApply(true);
        }
        mUserInfoRequest.setBlood_type(spinner.getItemAtPosition(position).toString());
        Timber.d("bloodType %s", spinner.getItemAtPosition(position).toString());
    }

    public void initDefaultValue() {
        String bloodType = mGenkiUserInfoModel.getBloodType();
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(bloodType)) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            spnBloodType.setSelection(index);
        }

    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
