package com.genkimiru.app.presentation.faq;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.remote.extension.response.ArticleResponse;
import com.genkimiru.app.presentation.faq.adapter.FaqAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqPresenter;
import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqRouter;
import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqView;

public class FaqActivity extends SimpleBaseActivity<FaqPresenter> implements FaqView, FaqRouter {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_faq_expanded_listview)
    ExpandableListView mExpandableListView;

    private FaqAdapter adapter;
    List<String> listHeader;
    HashMap<String, List<String>> listChild;

    @Inject
    FaqPresenter mFaqPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new FaqAdapter(this);
        mFaqPresenter.onStartListFaq();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_faq;
    }

    @Override
    protected FaqPresenter registerPresenter() {
        return mFaqPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.faq_title);

    }

    @Override
    public void onGetSuccess(List<ArticleResponse> articleResponses) {
        hideProgress();
        if (listHeader == null) {
            listHeader = new ArrayList<>();
        } else listHeader.clear();

        if (listChild == null) {
            listChild = new HashMap<>();
        } else listChild.clear();

        if (articleResponses != null && articleResponses.size() > 0) {
            for (ArticleResponse articleRespons : articleResponses) {
                listHeader.add(articleRespons.title);
                List<String> content = new ArrayList<>();
                content.add(articleRespons.body);
                listChild.put(articleRespons.title, content);
            }
        }
        adapter.setData(listHeader, listChild);
        mExpandableListView.setAdapter(adapter);
    }

    @Override
    public void onGetFail() {
        hideProgress();
        MessageUtil.createMessageDialog(getString(R.string.faq_call_api_fail)).show(getSupportFragmentManager());
    }
}
