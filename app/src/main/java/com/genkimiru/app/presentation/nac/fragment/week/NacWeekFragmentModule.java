package com.genkimiru.app.presentation.nac.fragment.week;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragmentContract.NacWeekPresent;
import static com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragmentContract.NacWeekRouter;
import static com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragmentContract.NacWeekView;

@Module
public class NacWeekFragmentModule {

    @Provides
    @ActivityScope
    NacWeekView provideNacWeekView(NacWeekFragment nacWeekFragment) {
        return nacWeekFragment;
    }

    @Provides
    @ActivityScope
    NacWeekRouter provideNacWeekRouter(NacWeekRouter nacWeekRouter) {
        return nacWeekRouter;
    }

    @Provides
    @ActivityScope
    NacWeekPresent provideNacWeekPresenter(Context context, NacWeekView nacWeekView, NacWeekRouter nacWeekRouter, UserUseCase userUseCase) {
        return new NacWeekFragmentPresenter(context, nacWeekView, nacWeekRouter, userUseCase);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRespository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }
}
