package com.genkimiru.app.presentation.setting_notification;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Switch;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.setting_notification.NotificationSettingActivityContract.*;

public class NotificationSettingActivity extends SimpleBaseActivity<NotificationSettingPresenter>
        implements NotificationSettingView, NotificationSettingRouter {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.notification_setting_accept_push_switch)
    Switch mAcceptPushSwitch;
    @BindView(R.id.notification_setting_genki_announcement_switch)
    Switch mAnnouncementSwitch;
    @BindView(R.id.notification_setting_genki_advice_switch)
    Switch mAdviceSwitch;
    @BindView(R.id.notification_setting_contest_switch)
    Switch mContestSwitch;

    @Inject
    NotificationSettingPresenter mPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    private CompositeDisposable mCompositeDisposable;
    private boolean mIsHandleAllNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setValueFromGenkiUserInfo();
        initializeEventHandler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_notification_setting;
    }

    @Override
    protected NotificationSettingPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(getString(R.string.setting_notification_name));
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mAcceptPushSwitch)
                .skip(1)
                .subscribe(isChecked -> {
                    if (!isChecked) {
                        Timber.d("Disable all notifications");
                        mIsHandleAllNotification = true;
                        disablePushNotification();
                        mPresenter.updateNotificationSetting(false,
                                false,
                                false,
                                false);
                    } else if (!mAnnouncementSwitch.isChecked()
                            && !mAdviceSwitch.isChecked()
                            && !mContestSwitch.isChecked()) {
                        Timber.d("Enable all notifications");
                        mIsHandleAllNotification = true;
                        enablePushNotification();
                        mPresenter.updateNotificationSetting(true,
                                true,
                                true,
                                true);
                    }
                    showProgress();
                }));

        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mAnnouncementSwitch)
                .skip(1)
                .subscribe(isChecked -> {
                    if(mIsHandleAllNotification) {
                        return;
                    }
                    Timber.d("Update announcement notification: %s", isChecked);
                    showProgress();
                    checkAcceptPush();
                    mPresenter.updateNotificationSetting(isChecked,
                            mAdviceSwitch.isChecked(),
                            mContestSwitch.isChecked(),
                            mAcceptPushSwitch.isChecked());
                }));

        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mAdviceSwitch)
                .skip(1)
                .subscribe(isChecked -> {
                    if(mIsHandleAllNotification) {
                        return;
                    }
                    Timber.d("Update advice notification: %s", isChecked);
                    showProgress();
                    checkAcceptPush();
                    mPresenter.updateNotificationSetting(mAnnouncementSwitch.isChecked(),
                            isChecked,
                            mContestSwitch.isChecked(),
                            mAcceptPushSwitch.isChecked());
                }));

        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mContestSwitch)
                .skip(1)
                .subscribe(isChecked -> {
                    if(mIsHandleAllNotification) {
                        return;
                    }
                    Timber.d("Update contest notification: %s", isChecked);
                    showProgress();
                    checkAcceptPush();
                    mPresenter.updateNotificationSetting(mAnnouncementSwitch.isChecked(),
                            mAdviceSwitch.isChecked(),
                            isChecked,
                            mAcceptPushSwitch.isChecked());
                }));
    }

    private void disablePushNotification() {
        mAnnouncementSwitch.setChecked(false);
        mAdviceSwitch.setChecked(false);
        mContestSwitch.setChecked(false);
    }

    private void enablePushNotification() {
        mAnnouncementSwitch.setChecked(true);
        mAdviceSwitch.setChecked(true);
        mContestSwitch.setChecked(true);
    }

    private void checkAcceptPush() {
        if (mAcceptPushSwitch.isChecked()
                && !mAnnouncementSwitch.isChecked()
                && !mAdviceSwitch.isChecked()
                && !mContestSwitch.isChecked()) {
            mAcceptPushSwitch.setChecked(false);
            return;
        }

        if (!mAcceptPushSwitch.isChecked()) {
            mAcceptPushSwitch.setChecked(true);
        }
    }

    private void setValueFromGenkiUserInfo() {
        mAnnouncementSwitch.setChecked(mGenkiUserInfoModel.isAnnouncementFlag());
        mAdviceSwitch.setChecked(mGenkiUserInfoModel.isAdvicesFlag());
        mContestSwitch.setChecked(mGenkiUserInfoModel.isContestNotificationsFlag());
        checkValue();
    }

    private void updateValueToGenkiUserInfo() {
        mGenkiUserInfoModel.setAnnouncementFlag(mAnnouncementSwitch.isChecked());
        mGenkiUserInfoModel.setAdvicesFlag(mAdviceSwitch.isChecked());
        mGenkiUserInfoModel.setContestNotificationsFlag(mContestSwitch.isChecked());
    }

    private void checkValue() {
        if(!mAnnouncementSwitch.isChecked() && !mAdviceSwitch.isChecked() && !mContestSwitch.isChecked()) {
            mAcceptPushSwitch.setChecked(false);
            disablePushNotification();
            return;
        }
        mAcceptPushSwitch.setChecked(true);
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message);
    }

    @Override
    public void onUpdateFail() {
        Timber.d("Update notification fail");
        hideProgress();
        setValueFromGenkiUserInfo();
        mIsHandleAllNotification = false;
    }

    @Override
    public void onUpdateSuccess() {
        Timber.d("Update notification success");
        hideProgress();
        updateValueToGenkiUserInfo();
        MessageUtil.showShortToast(getApplicationContext(), getString(R.string.setting_notification_update_success));
        mIsHandleAllNotification = false;
    }
}
