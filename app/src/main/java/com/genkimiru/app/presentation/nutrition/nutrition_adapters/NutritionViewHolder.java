package com.genkimiru.app.presentation.nutrition.nutrition_adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.dialog.EditFoodDialog;
import com.genkimiru.app.presentation.nutrition.NutritionActivity;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import timber.log.Timber;

public class NutritionViewHolder extends BaseSimpleViewHolder<GenkiNutritionModel.NutriManualModel> {
    @BindView(R.id.food_info_name_txt)
    TextView mFoodName;
    @BindView(R.id.food_info_cal_txt)
    TextView mCal;
    @BindView(R.id.food_info_fat_txt)
    TextView mFat;
    @BindView(R.id.food_info_protein_txt)
    TextView mProtein;
    @BindView(R.id.food_info_carb_txt)
    TextView mCarb;
    @BindView(R.id.food_info_eat_time_txt)
    TextView mTime;

    private UpdateListener mUpdateListener;

    public NutritionViewHolder(View itemView, UpdateListener updateListener) {
        super(itemView);
        mUpdateListener = updateListener;
    }


    @Override
    public void bind(GenkiNutritionModel.NutriManualModel manualModel) {
        mFoodName.setText(manualModel.getFood());
        mCal.setText(StringUtil.formatUnnecessaryZero(manualModel.getKcal()));
        mCarb.setText(StringUtil.formatUnnecessaryZero(manualModel.getCarb()));
        mFat.setText(StringUtil.formatUnnecessaryZero(manualModel.getFat()));
        mProtein.setText(StringUtil.formatUnnecessaryZero(manualModel.getProtein()));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(manualModel.getStart() * 1000L);
        mTime.setText(String.format("%s:%s", String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)), String.valueOf(calendar.get(Calendar.MINUTE))));
        itemView.setOnClickListener(view -> {
            Context context = itemView.getContext();
            NutritionActivity activity = (NutritionActivity) context;
            Bundle bundle = new Bundle();
            bundle.putSerializable("CLICKEDFOOD", manualModel);
            EditFoodDialog editFoodDialog = new EditFoodDialog.EditFoodDialogBuilder().dialogListener(new EditFoodDialog.OnResultListener() {
                @Override
                public void onUpdate(EditFoodDialog editFoodDialog, String name, String cal, String pro, String fat, String carb, String time) {
                    manualModel.setFat(Float.parseFloat(fat));
                    manualModel.setCarb(Float.parseFloat(carb));
                    manualModel.setKcal(Float.parseFloat(cal));
                    manualModel.setProtein(Float.parseFloat(pro));
                    Timestamp timestamp = null;
                    try {
                        SimpleDateFormat printFormat = new SimpleDateFormat("hh:mm");
                        timestamp = new Timestamp(printFormat.parse(time).getTime());
                    } catch (ParseException e) {
                        Timber.d("NutrionViewHoder_ParseException " + e.getMessage());
                    }
                    manualModel.setStart(timestamp.getTime());
                    mUpdateListener.onUpdate(manualModel);
                    editFoodDialog.dismiss();
                }

                @Override
                public void onCancel(EditFoodDialog editFoodDialog) {
                    editFoodDialog.dismiss();
                }

                @Override
                public void onDelete(EditFoodDialog editFoodDialog) {
                    mUpdateListener.onDelete(manualModel);
                    editFoodDialog.dismiss();
                }


            }).build();

            editFoodDialog.setArguments(bundle);
            editFoodDialog.show(activity.getSupportFragmentManager());
        });
    }

    public interface UpdateListener {
        void onUpdate(GenkiNutritionModel.NutriManualModel manualModel);

        void onDelete(GenkiNutritionModel.NutriManualModel manualModel);
    }

}
