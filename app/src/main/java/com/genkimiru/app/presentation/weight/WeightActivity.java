package com.genkimiru.app.presentation.weight;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.data.model.WeightModel;
import com.genkimiru.app.dialog.RecordWeightDialog;
import com.genkimiru.app.dialog.WeightInfoDialog;
import com.genkimiru.app.presentation.weight.weight_adapter.WeightAdapter;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;


public class WeightActivity extends SimpleBaseActivity<WeightActivityContract.WeightPresenter> implements WeightActivityContract.WeightRouter, WeightActivityContract.WeightView {
    @BindView(R.id.weight_activity_record_btn)
    Button mAddBtn;
    @BindView(R.id.weight_activity_info_button)
    Button mInfoBtn;
    @BindView(R.id.weight_activity_height_txt)
    TextView mHeightTv;
    @BindView(R.id.weight_activity_date_txt)
    TextView mDateTv;
    @BindView(R.id.weight_activity_advice_btn)
    Button mAdviceBtn;
    @BindView(R.id.weight_activity_send_btn)
    Button mSendbtn;
    @BindView(R.id.weight_activity_status_txt)
    TextView mStatusTv;
    @BindView(R.id.weight_activity_weight_txt)
    TextView mWeightTv;
    @BindView(R.id.weight_activity_record_list_rcv)
    RecyclerView mRecyclerView;
    @BindView(R.id.weight_activity_weight_define)
    TextView mWeightDefineTv;
    @BindView(R.id.weight_activity_bmi_tv)
    TextView mBMITv;


    private WeightAdapter mAdapter;
    List<WeightModel> mModelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    private void initialize() {
        mModelList = new ArrayList<>();
        mAdapter = new WeightAdapter(this);
        mAdapter.set(mModelList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_weight;
    }

    @Override
    protected WeightActivityContract.WeightPresenter registerPresenter() {
        return null;
    }

    @Override
    protected void onRestart() {
        super.onRestart();


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @OnClick(R.id.weight_activity_record_btn)
    void showRecordWeightDialog() {
        RecordWeightDialog recordWeightDialog = new RecordWeightDialog.RecordWeightDialogBuilder().dialogListener(new RecordWeightDialog.OnResultListener() {
            @Override
            public void onCancel(RecordWeightDialog recordWeightDialog) {
                recordWeightDialog.dismiss();
            }

            @Override
            public void onAdd(RecordWeightDialog recordWeightDialog, String weight, String time) {
                if (weight.length() > 0 && time.length() > 0) {
                    WeightModel weightModel = new WeightModel(Integer.parseInt(weight), time);
                    mAdapter.addItem(weightModel, 0);
                    if (mAdapter.getItemCount() > 1) {
                        mWeightDefineTv.setText(R.string.weight_average);
                    }
                    int averageWeight = 0;
                    int sum = 0;
                    mModelList = mAdapter.getItems();
                    for (int i = 0; i < mModelList.size(); i++) {
                        sum += mModelList.get(i).getWeight();
                        averageWeight = sum / mModelList.size();
                    }

                    mWeightTv.setText(String.valueOf(averageWeight));
                    try {
                        //double bmi = Double.parseDouble(mWeightTv.getText().toString()) / Math.pow(Double.parseDouble(mHeightTv.getText().toString()) / 100, 2);
                        double bmi = weightModel.getWeight() / Math.pow(Double.parseDouble(mHeightTv.getText().toString()) / 100, 2);
                        mBMITv.setText(String.format("%.1f", bmi));
                        if (bmi < 18.5) {
                            mStatusTv.setText(R.string.weight_underweight);
                            mStatusTv.setTextColor(getResources().getColor(R.color.sleep_light));
                        } else if (bmi >= 18.5 && bmi < 24.9) {
                            mStatusTv.setText(R.string.weight_normal);
                            mStatusTv.setTextColor(getResources().getColor(R.color.colorGreenLight));
                        } else if (bmi >= 30 && bmi < 34.9) {
                            mStatusTv.setText(R.string.weight_obesity_i);
                            mStatusTv.setTextColor(getResources().getColor(R.color.end_center_color_stress_view));
                        } else if (bmi >= 35 && bmi < 39.9) {
                            mStatusTv.setText(R.string.weight_obesity_ii);
                            mStatusTv.setTextColor(getResources().getColor(R.color.contest_text_point));
                        } else if (bmi >= 25 && bmi < 29.9) {
                            mStatusTv.setText(R.string.weight_pre_obesity);
                            mStatusTv.setTextColor(getResources().getColor(R.color.center_color_stress_view));
                        } else if (bmi > 40) {
                            mStatusTv.setText(R.string.weight_obesity_iii);
                            mStatusTv.setTextColor(getResources().getColor(R.color.end_color_stress_view));
                        }
                    } catch (NumberFormatException ex) {
                        Log.e("WeightActivity", "NumberFormatException" + ex.getMessage());
                    }
                    recordWeightDialog.dismiss();
                }
            }
        }).build();
        recordWeightDialog.show(getSupportFragmentManager());
    }
    @OnClick(R.id.weight_activity_info_button)
    void showInfoDialog() {
        WeightInfoDialog weightInfoDialog = new WeightInfoDialog();
        weightInfoDialog.show(getSupportFragmentManager());
    }
}
