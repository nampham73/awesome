package com.genkimiru.app.presentation.main.contentfeed_fragment;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.ListPresenterImp;
import com.genkimiru.app.data.model.GenkiContentModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.ListContentRequest;
import com.genkimiru.app.domain.content.ContentParam;
import com.genkimiru.app.domain.content.ContentUseCase;

import java.util.List;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentContract.*;
import static com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentPresenter.LoadType.ALL;
import static com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentPresenter.LoadType.FAVORITE;

public class ContentFeedFragmentPresenter
        extends ListPresenterImp<ContentFeedView, ContentFeedRouter>
        implements ContentFeedPresenter {

    private ContentUseCase mContentUseCase;
    private int mLoadType = ALL;

    public ContentFeedFragmentPresenter(Context context,
                                        ContentFeedView contentFeedView,
                                        ContentFeedRouter contentFeedRouter,
                                        ContentUseCase contentUseCase) {
        super(context, contentFeedView, contentFeedRouter);
        mContentUseCase = contentUseCase;
    }

    @Override
    public void start() {
        Timber.d("Start load list content offset: %s, limit: %s", getNextPage(), getPostPerPage());
        mView.onPrepareLoadingData();
        setLoadingRequesting(true);
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        ListContentRequest request = new ListContentRequest(token, getNextPage(), getPostPerPage());
        switch (mLoadType) {
            case ALL:
                mContentUseCase.execute(new ListContentSubscriber(),
                        ContentParam.Factory.getListContent(request));
                break;
            case FAVORITE:
                mContentUseCase.execute(new ListFavoriteContentSubscriber(),
                        ContentParam.Factory.getListFavoriteContent(request));
                break;
        }
    }

    @Override
    public void setIsLoadFavoriteList(boolean isLoadFavorite) {
        if((isLoadFavorite && mLoadType == FAVORITE)
                || (!isLoadFavorite && mLoadType == ALL)){
            return;
        }

        mLoadType = isLoadFavorite ? FAVORITE : ALL;
        setNextPage(0);

        mView.resetView();
        mContentUseCase.clear();

        start();
    }

    private class ListContentSubscriber extends DefaultSubscriber<List<GenkiContentModel>> {

        @Override
        public void onNext(List<GenkiContentModel> genkiContentModels) {
            Timber.d("Load list content success: %s", genkiContentModels.size());
            mView.onDataLoadDone(genkiContentModels);
            reCalculatedPaging(genkiContentModels.size());
            setLoadingRequesting(false);
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.d("Load list content fail");
            mView.onLoadingDataFailure(throwable, ApiErrorHandler.getMessage(mContext, throwable, 0));
            setLoadingRequesting(false);
        }
    }

    private class ListFavoriteContentSubscriber extends DefaultSubscriber<List<GenkiContentModel>> {

        @Override
        public void onNext(List<GenkiContentModel> genkiContentModels) {
            Timber.d("Load list favorite content success: %s", genkiContentModels.size());
            mView.onDataLoadDone(genkiContentModels);
            reCalculatedPaging(genkiContentModels.size());
            setLoadingRequesting(false);
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.d("Load list favorite content fail");
            mView.onLoadingDataFailure(throwable, ApiErrorHandler.getMessage(mContext, throwable, 0));
            setLoadingRequesting(false);
        }
    }

    public @interface LoadType {
        int ALL = 1;
        int FAVORITE = 2;
    }
}
