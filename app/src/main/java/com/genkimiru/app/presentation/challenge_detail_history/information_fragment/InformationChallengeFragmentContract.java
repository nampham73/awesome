package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import android.location.Location;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface InformationChallengeFragmentContract {

    interface InformationChallengePresenter extends BasePresenter {
        void processChallenge(int id, String flag, Location location);
        Location getLocation();
    }

    interface InformationChallengeView {
        void onProcessChallengeSuccess();
        void onProcessChallengeError(String message);
        void onGetLocationSuccess();
        void onGetLocationError();
    }

    interface InformationChallengeRouter {
        void gotoChallengeWalking();
    }
}
