package com.genkimiru.app.presentation.register.sex_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexFragmentRouter;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexFragmentView;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexPresenter;

public class SexFragmentPresenter extends BasePresenterImp<SexFragmentView,
        SexFragmentRouter> implements SexPresenter {

    private UserUseCase mUserUseCase;

    public SexFragmentPresenter(Context context, SexFragmentView sexFragmentView, SexFragmentRouter sexFragmentRouter, UserUseCase userUseCase) {
        super(context, sexFragmentView, sexFragmentRouter);
        mUserUseCase = userUseCase;
    }
}
