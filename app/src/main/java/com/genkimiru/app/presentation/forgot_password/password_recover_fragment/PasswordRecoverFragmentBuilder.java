package com.genkimiru.app.presentation.forgot_password.password_recover_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PasswordRecoverFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = PasswordRecoverFragmentModule.class)
    abstract PasswordRecoverFragment passwordRecoverFragment();

}
