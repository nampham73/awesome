package com.genkimiru.app.presentation.register.hospital_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HospitalFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = HospitalFragmentModule.class)
    abstract HospitalFragment hospitalFragment();
}
