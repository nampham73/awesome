package com.genkimiru.app.presentation.register.birthday_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayFragmentRouter;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayFragmentView;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class BirthdayFragmentModule {

    @Provides
    @FragmentScope
    BirthdayFragmentRouter provideRouter(BirthdayFragment birthdayFragment) {
        return birthdayFragment;
    }

    @Provides
    @FragmentScope
    BirthdayFragmentView provideView(BirthdayFragment birthdayFragment) {
        return birthdayFragment;
    }

    @Provides
    @FragmentScope
    BirthdayPresenter providePresenter(Context context, BirthdayFragmentView birthdayFragmentView, BirthdayFragmentRouter birthdayFragmentRouter, UserUseCase userUseCase) {
        return new BirthdayFragmentPresenter(context, birthdayFragmentView, birthdayFragmentRouter, userUseCase);
    }
}
