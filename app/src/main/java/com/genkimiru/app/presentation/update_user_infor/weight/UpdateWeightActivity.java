package com.genkimiru.app.presentation.update_user_infor.weight;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.presentation.update_user_infor.weight.UpdateWeightActivityContract.UpdateWeightPresenter;
import static com.genkimiru.app.presentation.update_user_infor.weight.UpdateWeightActivityContract.UpdateWeightRouter;
import static com.genkimiru.app.presentation.update_user_infor.weight.UpdateWeightActivityContract.UpdateWeightView;

public class UpdateWeightActivity extends SimpleBaseActivity<UpdateWeightPresenter>
        implements UpdateWeightView, UpdateWeightRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_weight_edt)
    EditText mWeightEdt;
    @Inject
    UpdateWeightPresenter mUpdateWeightPresenter;
    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    private MenuItem mApplyMenuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_weight;
    }

    @Override
    protected UpdateWeightPresenter registerPresenter() {
        return mUpdateWeightPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateWeightPresenter.updateWeight(mUserInfoRequest.getWeight());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mWeightEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(weight -> {
                    Timber.d("weight: %s", weight.toString());
                    if (StringUtil.isEmpty(weight)) {
                        setEnabledMenuApply(false);
                        return;
                    }
                    setEnabledMenuApply(true);
                    weight = mUpdateWeightPresenter.validateWeight(weight.toString().trim());
                    mWeightEdt.setText(weight.toString());
                    mWeightEdt.setSelection(mWeightEdt.getText().length());
                    setWeight();
                });
    }

    private void setWeight() {
        mUserInfoRequest.setWeight(Float.parseFloat(mWeightEdt.getText().toString().trim()));
        mUserInfoRequest.setWeightType(1);

    }

    public void initialDefaultValue() {
        mWeightEdt.setText(String.valueOf(mGenkiUserInfoModel.getWeight()));
        mGenkiUserInfoModel.setWeightType(1);
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
