package com.genkimiru.app.presentation.update_user_infor.weight;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

@Module
public class UpdateWeightActivityModule {
    @Provides
    @ActivityScope
    UpdateWeightActivityContract.UpdateWeightView updateWeightView(UpdateWeightActivity updateWeightActivity) {
        return updateWeightActivity;
    }

    @Provides
    @ActivityScope
    UpdateWeightActivityContract.UpdateWeightRouter updateWeightRouter(UpdateWeightActivity updateWeightActivity) {
        return updateWeightActivity;
    }

    @Provides
    @ActivityScope
    UpdateWeightActivityContract.UpdateWeightPresenter provideUpdateWeightPresenter(Context context, UpdateWeightActivityContract.UpdateWeightView updateWeightView, UpdateWeightActivityContract.UpdateWeightRouter updateWeightRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        return new UpdateWeightActivityPresenter(context, updateWeightView, updateWeightRouter, userUseCase, genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserInfoRequest provideUserInforRequest(GenkiUserInfoModel genkiUserInfoModel) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return new UserInfoRequest(token, genkiUserInfoModel.getHealthSource());
    }

    @Provides
    @ActivityScope
    UserUseCase providesUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }


}
