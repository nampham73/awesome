package com.genkimiru.app.presentation.other_wear_device;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.GFitUserInfoModel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

public class OtherWearDeviceActivityContract {

    interface OtherWearDevicePresenter extends BasePresenter {
        void loginGoogleFit(String code);
        void handleSignInResult(Task<GoogleSignInAccount> completedTask, boolean isLoginState);
    }

    interface OtherWearDeviceView {
        void showProgressBar();
        void hideProgressBar();
        void showMessageDialog(String message);
    }

    interface OtherWearDeviceRouter {
        void loginWithGoogleFit();
        void openRegisterScreen(GFitUserInfoModel gFitUserInfoModel);
    }
}
