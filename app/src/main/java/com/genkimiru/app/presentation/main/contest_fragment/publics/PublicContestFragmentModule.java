package com.genkimiru.app.presentation.main.contest_fragment.publics;

import android.content.Context;

import com.genkimiru.app.base.scope.ChildFragmentScope;
import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;
import com.genkimiru.app.presentation.main.contest_fragment.publics.PublicContestContract.*;

@Module
public class PublicContestFragmentModule {

    @ChildFragmentScope
    @Provides
    PublicContestPresenter providePresenter(Context context, PublicContestView view, PublicContestRouter router, ContestUseCase useCase) {
        return new PublicContestFragmentPresenter(context, view, router, useCase);
    }

    @ChildFragmentScope
    @Provides
    PublicContestView provideView(PublicsContestFragment view) {
        return view;
    }

    @ChildFragmentScope
    @Provides
    PublicContestRouter provideRouter(PublicsContestFragment router) {
        return router;
    }

    @ChildFragmentScope
    @Provides
    ContestAdapter provideAdapter(Context context) {
        return new ContestAdapter(context);
    }


}
