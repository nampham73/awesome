package com.genkimiru.app.presentation.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.view.BaseActivityView;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.presentation.eula.EulaPagerAdapter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxTextView;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Google.CLIENT_ID;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginPresenter;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginView;

public class LoginActivityView
        extends BaseActivityView<LoginActivity, LoginPresenter>
        implements LoginView {

    public static final int RC_SIGN_IN = 100;

    @BindView(R.id.login_username_edt)
    TextInputEditText mUsernameEdt;
    @BindView(R.id.login_password_edt)
    TextInputEditText mPasswordEdt;
    @BindView(R.id.login_agree_eula_cbx)
    CheckBox mAgreeEulaCbx;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.login_facebook_btn)
    RelativeLayout mLoginFacebookBtn;
    @BindView(R.id.login_google_btn)
    RelativeLayout mLoginGoogleBtn;

    private boolean mIsValidUsernameInput;
    private boolean mIsValidPasswordInput;
    private boolean mIsValidUsernameLength;
    private boolean mIsValidPasswordLength;
    private GoogleSignInClient mGoogleSignInClient;

    public LoginActivityView(LoginActivity activity) {
        super(activity);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        setEnableLoginButton(false);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CLIENT_ID)
                .requestServerAuthCode(CLIENT_ID)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @OnClick(R.id.login_facebook_btn)
    void loginWithFacebook() {
        mActivity.openFacebookScreen();
    }

    @OnClick(R.id.login_forgot_password_tv)
    void onForgotPasswordClick() {
        mActivity.openForgotPasswordScreen();
    }

    @OnClick(R.id.login_personal_policy_tv)
    void onPersonalPolicyClick() {
        mActivity.openEulaScreen(EulaPagerAdapter.TypeView.PRIVACY_POLICY.getIndex());
    }

    @OnClick(R.id.login_terms_of_use_tv)
    void onTermsOfUseClick() {
        mActivity.openEulaScreen(EulaPagerAdapter.TypeView.TERMS_OF_USE.getIndex());
    }

    @OnClick(R.id.login_btn)
    void onLoginClick() {
        if (!mAgreeEulaCbx.isChecked()) {
            showMessageDialog(mActivity.getString(R.string.login_agree_term_message));
            return;
        }
        if (!mIsValidUsernameLength) {
            showMessageDialog(mActivity.getString(R.string.login_invalid_username_length));
            return;
        }
        if (!mIsValidPasswordLength) {
            showMessageDialog(mActivity.getString(R.string.login_invalid_password_length));
            return;
        }
        mPresenter.login(mUsernameEdt.getText().toString().trim(), mPasswordEdt.getText().toString().trim());
    }

    @OnClick(R.id.login_google_btn)
    void onLoginGoogle() {
        signIn();
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mUsernameEdt)
                .compose(bindUntilPause())
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(username -> {
                    Timber.d("username: %s", username.toString());
                    mIsValidUsernameInput = mPresenter.isValidUsername(username.toString().trim());
                    mIsValidUsernameLength = mPresenter.isValidUsernameLength(username.toString().trim());
                    setEnableLoginButton(mIsValidUsernameInput && mIsValidPasswordInput);
                });

        RxTextView.textChanges(mPasswordEdt)
                .compose(bindUntilPause())
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(password -> {
                    Timber.d("password: %s", password.toString());
                    mIsValidPasswordInput = mPresenter.isValidPassword(password.toString().trim());
                    mIsValidPasswordLength = mPresenter.isValidPasswordLength(password.toString().trim());
                    setEnableLoginButton(mIsValidUsernameInput && mIsValidPasswordInput);
                });

        RxCompoundButton.checkedChanges(mAgreeEulaCbx)
                .compose(bindUntilPause())
                .subscribe(isChecked -> {
                    Timber.d("Is agree eula checked: %s", isChecked);
                    setEnableLoginButton(mIsValidUsernameInput && mIsValidPasswordInput);
                });
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(mActivity.getSupportFragmentManager());
    }

    @Override
    public void showProgressDialog() {
        mActivity.showProgress();
    }

    @Override
    public void hideProgressDialog() {
        mActivity.hideProgress();
    }

    private void setEnableLoginButton(boolean isEnable) {
        mLoginBtn.setEnabled(isEnable);
        mLoginBtn.setTextColor(isEnable
                ? mActivity.getResources().getColor(android.R.color.white)
                : mActivity.getResources().getColor(R.color.transparent_20_black));
    }

    private void signIn() {
        mGoogleSignInClient.signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mActivity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
