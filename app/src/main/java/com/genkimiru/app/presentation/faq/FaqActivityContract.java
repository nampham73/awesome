package com.genkimiru.app.presentation.faq;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.remote.extension.response.ArticleResponse;

import java.util.List;

public interface FaqActivityContract {

    interface FaqPresenter extends BasePresenter {
        void onStartListFaq();
    }

    interface FaqView {
        void onGetSuccess(List<ArticleResponse> articleResponses);
        void onGetFail();
    }

    interface FaqRouter {

    }

}
