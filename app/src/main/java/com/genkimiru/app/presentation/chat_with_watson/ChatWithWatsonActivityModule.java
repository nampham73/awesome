package com.genkimiru.app.presentation.chat_with_watson;

import android.content.Context;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonPresenter;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonRouter;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonView;

import dagger.Module;
import dagger.Provides;

@Module
public class ChatWithWatsonActivityModule {

    @Provides
    @ActivityScope
    ChatWithWatsonRouter provideRouter(ChatWithWatsonActivity chatWithWatsonActivity) {
        return chatWithWatsonActivity;
    }

    @Provides
    @ActivityScope
    ChatWithWatsonView provideView(ChatWithWatsonActivity chatWithWatsonActivity) {
        return chatWithWatsonActivity;
    }

    @Provides
    @ActivityScope
    ChatWithWatsonPresenter providePresenter(Context context, ChatWithWatsonView chatWithWatsonView, ChatWithWatsonRouter chatWithWatsonRouter) {
        return new ChatWithWatsonActivityPresenter(context, chatWithWatsonView, chatWithWatsonRouter);
    }
}
