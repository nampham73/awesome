package com.genkimiru.app.presentation.update_user_infor.birthday;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayPresenter;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayRouter;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayView;

public class UpdateBirthdayActivityPresenter extends BasePresenterImp<UpdateBirthdayView, UpdateBirthdayRouter> implements UpdateBirthdayPresenter {

    private UserUseCase mUserUseCase;
    private GenkiUserInfoModel mGenkiUserInfoModel;
    String mBirthDay;

    public UpdateBirthdayActivityPresenter(Context context, UpdateBirthdayView birthdayView, UpdateBirthdayRouter birthdayRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, birthdayView, birthdayRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;

    }


    @Override
    public void updateBirthday(String dataBirthday) {
        mBirthDay = dataBirthday;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setBirthday(dataBirthday)
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();
        mUserUseCase.execute(new UpdateBirthdaySubscriber(), UserParam.Factory.updateUserProfile(request));
    }

    private class UpdateBirthdaySubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            Timber.d("Update Birthday success");
            mGenkiUserInfoModel.setBirthday(mBirthDay);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }

}
