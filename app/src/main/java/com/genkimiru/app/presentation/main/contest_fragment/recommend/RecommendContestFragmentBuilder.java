package com.genkimiru.app.presentation.main.contest_fragment.recommend;

import com.genkimiru.app.base.scope.ChildFragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RecommendContestFragmentBuilder {
    @ChildFragmentScope
    @ContributesAndroidInjector(modules = RecommendContestFragmentModule.class)
    abstract RecommendContestFragment recommendContestFragment();
}
