package com.genkimiru.app.presentation.forgot_password.password_recover_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface PasswordRecoverFragmentContract {

    interface PasswordRecoverPresenter extends BasePresenter {
        void onSendClick(String email);
        boolean isValidEmail(String email);
    }

    interface PasswordRecoverView {
        void showMessageDialog(String message);
        void showLoadingDialog();
        void dismissLoadingDialog();
        void openSentDialog();
    }

    interface PasswordRecoverRouter {
        void openNewPasswordScreen();
    }
}
