package com.genkimiru.app.presentation.register.alcohol_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AlcoholFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = AlcoholFragmentModule.class)
    abstract AlcoholFragment alcoholFragment();
}
