package com.genkimiru.app.presentation.energy;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.domain.mydata.MyDataParam;
import com.genkimiru.app.domain.mydata.MyDataUseCase;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.chart.BarChartActivity.CHART_COLUMN;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.DAY;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.MONTH;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.WEEK;
import static com.genkimiru.app.presentation.energy.EnergyActivityContract.*;

public class EnergyActivityPresenter extends BasePresenterImp<EnergyView, EnergyRouter> implements EnergyPresenter {

    private MyDataUseCase mMyDataUseCase;

    public EnergyActivityPresenter(Context context, EnergyView energyView, EnergyRouter energyRouter, MyDataUseCase myDataUseCase) {
        super(context, energyView, energyRouter);
        mMyDataUseCase = myDataUseCase;
    }

    @Override
    public void getData(Calendar calendar, int viewType) {
        Calendar startCalendar = (Calendar) calendar.clone();
        Calendar endCalendar = (Calendar) calendar.clone();
        long currentTime = DateUtil.getToday() / 1000L;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int dataType = DetailChartDataRequest.DataType.ENERGY;
        DetailChartDataRequest.Builder request
                = new DetailChartDataRequest.Builder(token)
                .setDataType(dataType);
        switch (viewType) {
            case DAY:
                startCalendar.add(Calendar.DAY_OF_MONTH, -3);
                endCalendar.add(Calendar.DAY_OF_MONTH, 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startDay = startCalendar.getTimeInMillis() / 1000L;
                long endDay = endCalendar.getTimeInMillis() / 1000L;
                if (endDay > currentTime) {
                    endDay = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.DAILY)
                        .setStartDate(startDay)
                        .setEndDate(endDay);
                break;
            case WEEK:
                startCalendar.add(Calendar.DAY_OF_MONTH, - (7 * 3));
                endCalendar.add(Calendar.DAY_OF_MONTH, (7 * 3));
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startWeek = startCalendar.getTimeInMillis() / 1000L;
                long endWeek = endCalendar.getTimeInMillis() / 1000L;
                if (endWeek > currentTime) {
                    endWeek = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.WEEKLY)
                        .setStartDate(startWeek)
                        .setEndDate(endWeek);
                break;
            case MONTH:
                startCalendar.add(Calendar.MONTH, -3);
                endCalendar.add(Calendar.MONTH, 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startMonth = startCalendar.getTimeInMillis() / 1000L;
                long endMonth = endCalendar.getTimeInMillis() / 1000L;
                if (endMonth > currentTime) {
                    endMonth = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.MONTHLY)
                        .setStartDate(startMonth)
                        .setEndDate(endMonth);
                break;
        }

        mMyDataUseCase.execute(new EnergyDataSubscription(),
                MyDataParam.Factory.fetchingEnergyChartData(request.create()));
    }

    private BarData createChartData(List<GenkiEnergyModel> genkiEnergyModels){
        if(genkiEnergyModels == null || genkiEnergyModels.size() <= 0) {
            return null;
        }

        for (int i = genkiEnergyModels.size(); i < CHART_COLUMN; i++) {
            genkiEnergyModels.add(new GenkiEnergyModel());
        }

        ArrayList<BarEntry> kCalInEntries = new ArrayList<BarEntry>();
        ArrayList<BarEntry> kCalOutEntries = new ArrayList<BarEntry>();
        float start = 1f;

        for(int i = (int) start; i < genkiEnergyModels.size() + 1;  i++) {
            float kCalIn = genkiEnergyModels.get(i - 1).getKCalIn();
            kCalInEntries.add(new BarEntry(i, kCalIn));

            float kCalOut = genkiEnergyModels.get(i - 1).getKCalOut();
            kCalOutEntries.add(new BarEntry(i, kCalOut));
        }

        BarDataSet kCalInBarDataSet = new BarDataSet(kCalInEntries,"");
        kCalInBarDataSet.setDrawIcons(false);
        kCalInBarDataSet.setColor(mContext.getResources().getColor(R.color.step_chart));

        BarDataSet kCalOutBarDataSet = new BarDataSet(kCalOutEntries,"");
        kCalOutBarDataSet.setDrawIcons(false);
        kCalOutBarDataSet.setColor(mContext.getResources().getColor(R.color.colorPrimary));

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(kCalInBarDataSet);
        dataSets.add(kCalOutBarDataSet);

        return new BarData(dataSets);
    }

    public class EnergyDataSubscription extends DefaultSubscriber<List<GenkiEnergyModel>> {

        @Override
        public void onNext(List<GenkiEnergyModel> genkiEnergyModels) {
            Timber.d("Load energy data success");
            BarData barData = createChartData(genkiEnergyModels);
            mView.showChartData(barData);
            if (barData == null) {
                return;
            }
            mView.showDate(genkiEnergyModels.get(genkiEnergyModels.size() / 2));
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.d("Load energy data error");
            mView.loadDataFail(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }
}
