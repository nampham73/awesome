package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.ListFragment;
import com.genkimiru.app.data.model.GenkiFeedModel;
import com.genkimiru.app.presentation.content_detail.ContentDetailActivity;
import com.genkimiru.app.presentation.content_detail.SocialPopup;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.DetailContentFeedPresenter;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.DetailContentFeedRouter;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.DetailContentFeedView;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.ArticleViewHolder;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.DetailContentFeedAdapter;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.ItemDecoration;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.OnAdapterClickListener;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.util.List;

import javax.inject.Inject;

public class DetailContentFeedFragment extends
        ListFragment<DetailContentFeedAdapter, List<IDetailContentModel>, DetailContentFeedPresenter>
        implements DetailContentFeedRouter, DetailContentFeedView, OnAdapterClickListener, AdapterView.OnItemClickListener {

    public static DetailContentFeedFragment newInstance(Bundle args) {
        DetailContentFeedFragment fragment = new DetailContentFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    DetailContentFeedAdapter mAdapter;
    @Inject
    DetailContentFeedPresenter mPresenter;

    private ContentDetailActivity mActivity;
    private SocialPopup mSocialPopup;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ContentDetailActivity) getActivity();
    }

    @NonNull
    @Override
    protected DetailContentFeedAdapter createAdapter() {
        return mAdapter;
    }

    @Override
    protected DetailContentFeedPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.setArguments(getArguments());
        mSocialPopup = new SocialPopup(mActivity, this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter.setOnAdapterClickedListener(this);
        mPresenter.start();
    }

    @Override
    protected void initRecyclerView(RecyclerView recyclerView) {
        super.initRecyclerView(recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new ItemDecoration());
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected DetailContentFeedPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void onDataLoadDone(List<IDetailContentModel> iDetailContentModels) {
        super.onDataLoadDone(iDetailContentModels);
        if (iDetailContentModels != null && iDetailContentModels.size() > 0) {
            mAdapter.setItems(iDetailContentModels);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void addComment(GenkiFeedModel model) {
        if (getArguments() != null && model != null && mAdapter != null) {
            model.setArticleId(getArguments().getInt(Constants.Extra.ARTICLE_ID));
            mAdapter.add(model, 1);
            if (getRecyclerView() != null) {
                getRecyclerView().smoothScrollToPosition(1);
            }
            RecyclerView.ViewHolder holder = getRecyclerView().findViewHolderForAdapterPosition(0);
            mPresenter.requestAddComment(model.getComment(), holder);
        }
    }

    @Override
    public void onItemClicked(RecyclerView.ViewHolder holder, View view) {
        switch (view.getId()) {
            case R.id.detail_article_like_tv:
                mPresenter.requestLikeOrUnlike(holder);
                break;
            case R.id.detail_article_comment_tv:
                mActivity.showKeyboard();
                break;
            case R.id.detail_article_share_tv:
                mSocialPopup.showAsDropDown(view);
                break;
        }
    }

    @Override
    public void onUpdateLikeView(RecyclerView.ViewHolder holder, boolean status) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            viewHolder.updateStateForLikeView(status);
        }
    }

    @Override
    public void onUpdateCommentView(RecyclerView.ViewHolder holder) {
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            viewHolder.updateStateForCommentView();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RecyclerView.ViewHolder holder = getRecyclerView().findViewHolderForAdapterPosition(0);
        switch (position) {
            case 0:
                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("https://de-genkius.genkimiru.jp/"))
                        .setQuote("Genkimiru")
                        .build();
                ShareDialog facebookDialog = new ShareDialog(mActivity);
                facebookDialog.show(content);
                break;
            case 1:
                String message = "https://de-genkius.genkimiru.jp/";
                TweetComposer.Builder twitterBuilder = new TweetComposer.Builder(mActivity).text(message);
                twitterBuilder.show();
                break;
        }
        if (holder instanceof ArticleViewHolder) {
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            viewHolder.updateShareUi();
        }
        mSocialPopup.dismiss();
    }
}