package com.genkimiru.app.presentation.register.profile_avatar_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface ProfileAvatarFragmentContract {

    interface ProfileAvatarPresenter extends BasePresenter {

    }

    interface ProfileAvatarFragmentView {

    }

    interface ProfileAvatarFragmentRouter {

    }
}
