package com.genkimiru.app.presentation.register.weight_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface WeightFragmentContract {

    interface WeightPresenter extends BasePresenter {
        String validateWeight(String weight);
    }

    interface WeightFragmentView {

    }

    interface WeightFragmentRouter {

    }
}
