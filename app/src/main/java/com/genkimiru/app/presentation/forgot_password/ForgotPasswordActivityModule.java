package com.genkimiru.app.presentation.forgot_password;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivityContract.*;

@Module
public class ForgotPasswordActivityModule {

    @Provides
    @ActivityScope
    ForgotPasswordView provideView(ForgotPasswordActivity forgotPasswordActivity) {
        return forgotPasswordActivity;
    }

    @Provides
    @ActivityScope
    ForgotPasswordRouter provideRouter(ForgotPasswordActivity forgotPasswordActivity) {
        return forgotPasswordActivity;
    }

    @Provides
    @ActivityScope
    ForgotPasswordPresenter providePresenter(Context context, ForgotPasswordView view, ForgotPasswordRouter router) {
        return new ForgotPasswordActivityPresenter(context, view, router);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository){
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
