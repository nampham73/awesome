package com.genkimiru.app.presentation.main.notification_fragment;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public class NotificationFragment extends SimpleBaseFragment {

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main_notification;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }
}
