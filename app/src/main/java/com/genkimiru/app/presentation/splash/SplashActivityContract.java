package com.genkimiru.app.presentation.splash;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;

public interface SplashActivityContract {

    interface SplashPresenter extends BasePresenter {
        boolean checkUserInformation();
        void getUserInformation();
    }

    interface SplashView {
        void onGetUserInfoSuccess();
        void onGetUserInfoError(String message);
    }

    interface SplashRouter {
        void openIntroActivity();
        void openLoginActivity();
        void openMainActivity();
    }
}
