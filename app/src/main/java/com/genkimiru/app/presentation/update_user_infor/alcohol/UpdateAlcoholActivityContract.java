package com.genkimiru.app.presentation.update_user_infor.alcohol;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateAlcoholActivityContract {

    interface UpdateAlcoholPresenter extends BasePresenter {
        void updateAlcohol(int alcoholLevel, int drinkEachTime);
    }

    interface UpdateAlcoholView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateAlcoholRouter {

    }
}
