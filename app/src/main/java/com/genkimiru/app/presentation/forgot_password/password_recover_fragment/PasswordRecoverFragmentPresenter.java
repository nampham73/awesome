package com.genkimiru.app.presentation.forgot_password.password_recover_fragment;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.RecoverPasswordRequest;
import com.genkimiru.app.data.remote.genki.response.RecoverPasswordResponse;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.RECOVER_PASSWORD_CODE;
import static com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragmentContract.*;

public class PasswordRecoverFragmentPresenter extends BasePresenterImp<PasswordRecoverView, PasswordRecoverRouter>
        implements PasswordRecoverPresenter {

    private UserUseCase mUserUseCase;

    public PasswordRecoverFragmentPresenter(Context context,
                                            PasswordRecoverView view,
                                            PasswordRecoverRouter recoverRouter,
                                            UserUseCase userUseCase) {
        super(context, view, recoverRouter);
        mUserUseCase = userUseCase;
    }

    @Override
    public void onSendClick(String email) {
        mView.showLoadingDialog();
        RecoverPasswordRequest recoverPasswordRequest = new RecoverPasswordRequest(email);
        mUserUseCase.execute(new SendEmailRecoverSubscriber(), UserParam.Factory.recoverPassword(recoverPasswordRequest));
    }

    @Override
    public boolean isValidEmail(String email) {
        return StringUtil.isNotEmpty(email) && email.length() >= 8;
    }

    private class SendEmailRecoverSubscriber extends DefaultSubscriber<RecoverPasswordResponse> {
        @Override
        public void onNext(RecoverPasswordResponse recoverPasswordResponse) {
            mView.dismissLoadingDialog();
            Timber.d("SendEmailRecoverSubscriber recover code: %s", recoverPasswordResponse.code);
            GenkiApplication.getPreferences().getString(RECOVER_PASSWORD_CODE).set(recoverPasswordResponse.code);
            mView.openSentDialog();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.dismissLoadingDialog();
            Timber.e("SendEmailRecoverSubscriber Error: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }
}
