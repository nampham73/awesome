package com.genkimiru.app.presentation.energy;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;

public interface EnergyActivityContract {

    interface EnergyPresenter extends BasePresenter {
        void getData(Calendar calendar, int viewType);
    }

    interface EnergyView {
        void showChartData(BarData barData);
        void showDate(GenkiEnergyModel genkiEnergyModel);
        void loadDataFail(String message);
    }

    interface EnergyRouter {

    }

}
