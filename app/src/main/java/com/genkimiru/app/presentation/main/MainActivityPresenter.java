package com.genkimiru.app.presentation.main;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;

public class MainActivityPresenter extends BasePresenterImp<MainContract.MainView ,MainContract.MainRouter> implements MainContract.MainPresenter {

    public MainActivityPresenter(Context context, MainContract.MainView mainView, MainContract.MainRouter mainRouter) {
        super(context, mainView, mainRouter);
    }
}