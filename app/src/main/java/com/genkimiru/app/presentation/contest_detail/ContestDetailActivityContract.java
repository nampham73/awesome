package com.genkimiru.app.presentation.contest_detail;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;
import com.genkimiru.app.data.model.GenkiContestDetailModel;

public interface ContestDetailActivityContract {

    interface ContestDetailPresenter extends BasePresenter{
        void getContestDetail(int id);
        void joinContest(int id);
    }

    interface ContestDetailView extends BaseView<ContestDetailPresenter>{
        void onGetContestDetailSuccess(GenkiContestDetailModel contestDetailModel);
        void onGetContestDetailFail(String message);
        void onJoinContestSuccess(String message);
        void onJoinContestError(String message);
    }

    interface ContestDetailRouter {

    }
}
