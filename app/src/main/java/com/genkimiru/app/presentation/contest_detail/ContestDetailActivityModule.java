package com.genkimiru.app.presentation.contest_detail;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiContestDetailModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.contest.ContestRepository;
import com.genkimiru.app.data.repository.contest.ContestRepositoryImp;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailPresenter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailRouter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailView;

import dagger.Module;
import dagger.Provides;

@Module
public class ContestDetailActivityModule {

    @Provides
    @ActivityScope
    ContestDetailView provideView(ContestDetailActivity contestDetailActivity) {
        return new ContestDetailActivityView

                (contestDetailActivity);
    }

    @Provides
    @ActivityScope
    ContestDetailRouter provideRouter(ContestDetailActivity contestDetailActivity) {
        return contestDetailActivity;
    }

    @Provides
    @ActivityScope
    ContestDetailPresenter providePresenter(Context context, ContestDetailView contestDetailView,
                                            ContestDetailRouter router,
                                            ContestUseCase contestUseCase,
                                            GenkiContestDetailModel contestDetailModel) {
        return new ContestDetailActivityPresenter(context, contestDetailView, router, contestUseCase, contestDetailModel);
    }

    @Provides
    @ActivityScope
    ContestRepository provideUserRepository(GenkiApi api) {
        return new ContestRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    ContestUseCase provideUserUseCase(SchedulerProvider schedulerProvider, ContestRepository contestRepository) {
        return new ContestUseCase(schedulerProvider.io(), schedulerProvider.ui(), contestRepository);
    }
}
