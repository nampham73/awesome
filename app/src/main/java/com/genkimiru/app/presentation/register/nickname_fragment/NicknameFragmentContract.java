package com.genkimiru.app.presentation.register.nickname_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NicknameFragmentContract {

    interface NicknamePresenter extends BasePresenter {

    }

    interface NicknameView {

    }

    interface NicknameRouter {

    }
}
