package com.genkimiru.app.presentation.main.dashboard_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class DashboardFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = DashBoardFragmentModule.class)
    abstract DashboardFragment dashboardFragment();
}
