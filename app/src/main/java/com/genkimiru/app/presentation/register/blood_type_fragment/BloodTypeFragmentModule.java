package com.genkimiru.app.presentation.register.blood_type_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypeFragmentRouter;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypeFragmentView;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class BloodTypeFragmentModule {

    @Provides
    @FragmentScope
    BloodTypeFragmentView provideView(BloodTypeFragment bloodTypeFragment) {
        return bloodTypeFragment;
    }

    @Provides
    @FragmentScope
    BloodTypeFragmentRouter provideRouter(BloodTypeFragment bloodTypeFragment) {
        return bloodTypeFragment;
    }

    @Provides
    @FragmentScope
    BloodTypePresenter providePresenter(Context context, BloodTypeFragmentView bloodTypeFragmentView, BloodTypeFragmentRouter bloodTypeFragmentRouter,
                                        UserUseCase userUseCase) {
        return new BloodTypeFragmentPresenter(context, bloodTypeFragmentView, bloodTypeFragmentRouter, userUseCase);
    }
}
