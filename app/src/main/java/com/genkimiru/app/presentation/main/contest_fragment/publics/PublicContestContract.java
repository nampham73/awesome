package com.genkimiru.app.presentation.main.contest_fragment.publics;

import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.contest_fragment.ContestListContract;

import java.util.List;

public interface PublicContestContract {
    interface PublicContestView extends ListContract.RootView<List<GenkiContestModel>>{}
    interface PublicContestPresenter extends ContestListContract.ListPresenter{}
    interface PublicContestRouter{}
}
