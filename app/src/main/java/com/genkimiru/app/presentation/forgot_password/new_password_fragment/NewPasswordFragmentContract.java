package com.genkimiru.app.presentation.forgot_password.new_password_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NewPasswordFragmentContract {

    interface NewPasswordPresenter extends BasePresenter {
        void onSendClick(String code, String password);
        boolean isValidRecoverCode(String code);
        boolean isValidPassword(String password);
        boolean isValidPasswordLength(String password);
        boolean isValidConfirmPassword(String password, String confirmPassword);
    }

    interface NewPasswordView {
        void showMessageDialog(String message);
        void showLoadingDialog();
        void dismissLoadingDialog();
        void openCompleteDialog();
    }

    interface NewPasswordRouter {

    }
}
