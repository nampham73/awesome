package com.genkimiru.app.presentation.change_password;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.ChangePasswordRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Input.PASSWORD_MIN_LENGTH;
import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.change_password.ChangePasswordActivityContract.*;

public class ChangePasswordActivityPresenter
        extends BasePresenterImp<ChangePasswordView, ChangePasswordRouter>
        implements ChangePasswordPresenter{

    private UserUseCase mUserUseCase;

    public ChangePasswordActivityPresenter(Context context, ChangePasswordView view, ChangePasswordRouter router, UserUseCase userUseCase) {
        super(context, view, router);
        mUserUseCase = userUseCase;
    }

    @Override
    public boolean isValidPassword(String password) {
        return StringUtil.isNotEmpty(password);
}

    @Override
    public boolean isValidPasswordLength(String password) {
        return password.length() >= PASSWORD_MIN_LENGTH;
    }

    @Override
    public boolean isTheSamePassword(String password1, String password2) {
        return password1.equals(password2);
    }

    @Override
    public void sendChangePassword(String oldPassword, String newPassword) {
        Timber.d("Send change password: new password = %s, old password = %s", newPassword, oldPassword);
        mView.showLoadingDialog();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(token, newPassword, oldPassword);
        mUserUseCase.execute(new ChangePasswordSubscriber(), UserParam.Factory.changePassword(changePasswordRequest));
    }

    private class ChangePasswordSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            Timber.d("Change password success: %s", aBoolean);
            mView.dismissLoadingDialog();
            mView.openCompleteDialog();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Change password error: %s", throwable.getMessage());
            mView.dismissLoadingDialog();
            mView.showMessageDialog(mContext.getString(R.string.change_password_error_message));
        }
    }
}
