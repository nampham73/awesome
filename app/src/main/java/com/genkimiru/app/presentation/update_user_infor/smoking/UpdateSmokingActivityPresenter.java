package com.genkimiru.app.presentation.update_user_infor.smoking;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingPresenter;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingRouter;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingView;

public class UpdateSmokingActivityPresenter extends BasePresenterImp<UpdateSmokingView, UpdateSmokingRouter>
        implements UpdateSmokingPresenter {
    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    int mSmokingLevel,mSmokingEachTime;

    public UpdateSmokingActivityPresenter(Context context, UpdateSmokingView updateSmokingView, UpdateSmokingRouter updateSmokingRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateSmokingView, updateSmokingRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;

    }

    @Override
    public void updateSmoking(int smokingLevel, int smokingEachTime) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        mSmokingLevel = smokingLevel;
        mSmokingEachTime = smokingEachTime;
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setSmokingLevel(smokingLevel)
                .setSmokingEachTime(smokingEachTime)
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();

        mUserUseCase.execute(new UpdateSmokingSubcriber(), (UserParam.Factory.updateUserProfile(request)));
    }

    private class UpdateSmokingSubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Updage Smoking Success");
            mGenkiUserInfoModel.setSmokingLevel(mSmokingLevel);
            mGenkiUserInfoModel.setSmokingEachTime(mSmokingEachTime);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
