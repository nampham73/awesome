package com.genkimiru.app.presentation.walking_challenge;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiChallengeModel;
import com.genkimiru.app.presentation.photo.PhotoActivity;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengePresenter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengeRouter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengeView;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.genkimiru.app.Constants.Extra.CHALLENGE_USER_DATA;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_CAMERA_NO_CROP;

public class WalkingChallengeActivity extends SimpleBaseActivity<WalkingChallengePresenter>
        implements WalkingChallengeView, WalkingChallengeRouter {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.tvTimer)
    TextView timer;

    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;
    private long millisecondTime, startTime, timeBuff, updateTime = 0L;
    private int seconds, minutes, hours;
    private Handler handler;
    private GenkiChallengeModel mChallengeData;

    @Inject
    WalkingChallengePresenter mPresenter;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_walking_challenge;
    }

    @Override
    protected WalkingChallengePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.walking_challenge_title);
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        startTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent() == null) return;
        mChallengeData = (GenkiChallengeModel) getIntent().getSerializableExtra(CHALLENGE_USER_DATA);
    }

    @OnClick(R.id.walking_challenge_photo)
    void takePhoto() {
        startActivityForResult(PhotoActivity.createIntent(
                getApplicationContext(),
                PhotoActivity.Type.Take),
                REQUEST_CODE_CAMERA_NO_CROP);
    }

    @OnClick(R.id.walking_challenge_map)
    void goMap() {
        Intent intent = new Intent(WalkingChallengeActivity.this, WalkingChallengeMapActivity.class);
        startActivity(intent);
    }

    public Runnable runnable = new Runnable() {

        @SuppressLint({"DefaultLocale", "SetTextI18n"})
        public void run() {
            millisecondTime = SystemClock.uptimeMillis() - startTime;
            updateTime = timeBuff + millisecondTime;
            seconds = (int) (updateTime / 1000);
            minutes = seconds / 60;
            seconds = seconds % 60;
            hours = minutes / 60;

            timer.setText(String.format("%02d", hours) + ":"
                    + String.format("%02d", minutes) + ":"
                    + String.format("%02d", seconds));

            handler.postDelayed(this, 0);
        }
    };

    @OnClick(R.id.challenge_history_stop)
    void stopChallenge() {
        showProgress();
        timeBuff += millisecondTime;
        handler.removeCallbacks(runnable);
        mPresenter.processChallenge(mChallengeData.getUserContestChallengeId());
    }

    @Override
    public void onProcessChallengeSuccess(String message) {
        MessageUtil.showShortToast(getApplicationContext(), message);
        hideProgress();
    }

    @Override
    public void onProcessChallengeError(String message) {
        hideProgress();
    }

    @Override
    public void gotoChallengeDetail() {
        finish();
    }
}
