package com.genkimiru.app.presentation.update_user_infor.blood_type;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityContract.UpdateBloodTypePresenter;
import static com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityContract.UpdateBloodTypeRouter;
import static com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityContract.UpdateBloodTypeView;

public class UpdateBloodTypeActivityPresenter extends BasePresenterImp<UpdateBloodTypeView, UpdateBloodTypeRouter> implements UpdateBloodTypePresenter {

    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    String mBloodType;

    public UpdateBloodTypeActivityPresenter(Context context,
                                            UpdateBloodTypeView updateBloodTypeView,
                                            UpdateBloodTypeRouter updateBloodTypeRouter,
                                            UserUseCase userUseCase,
                                            GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateBloodTypeView, updateBloodTypeRouter);
        this.mGenkiUserInfoModel = genkiUserInfoModel;
        this.mUserUseCase = userUseCase;
    }

    @Override
    public void updateBloodType(String bloodType) {
        mBloodType = bloodType;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setBloodType(bloodType)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();
        mUserUseCase.execute(new UpdateBloodTypeSubcribe(), UserParam.Factory.updateUserProfile(request));
    }

    private class UpdateBloodTypeSubcribe extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Update Blood Type success");
            mGenkiUserInfoModel.setBloodType(mBloodType);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
