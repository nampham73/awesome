package com.genkimiru.app.presentation.challenge_detail_history.history_fragment.adapter;

import android.content.Context;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.data.model.GenkiContestModel;

public class HistoryChallengeFragmentAdapter extends BaseSimpleAdapter<GenkiContestModel, HistoryChallengeViewHolder> {

    public HistoryChallengeFragmentAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.view_holder_history_item;
    }

    @Override
    public HistoryChallengeViewHolder createHolder(View view) {
        return new HistoryChallengeViewHolder(view);
    }
}

