package com.genkimiru.app.presentation.step;

import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.presentation.chart.BarChartActivity;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.presentation.step.StepActivityContract.StepPresenter;
import static com.genkimiru.app.presentation.step.StepActivityContract.StepRouter;
import static com.genkimiru.app.presentation.step.StepActivityContract.StepView;
import static com.github.mikephil.charting.components.XAxis.XAxisPosition.BOTTOM;

public class StepActivity extends BarChartActivity<StepPresenter> implements StepView, StepRouter {

    @BindView(R.id.activity_steps_number_tv)
    TextView mNumberSteps;
    @BindView(R.id.step_bar_chart_minute)
    BarChart mStepBarChartMinute;
    @BindView(R.id.step_bar_chart_minute_label_layout)
    LinearLayout mLabelStepBarChart;

    @Inject
    StepPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeMinuteBarChart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected int getContentViewId() {
        return R.layout.activity_step;
    }

    @Override
    protected void onDayChange(Calendar calendar) {
        super.onDayChange(calendar);
        Timber.d("on day change");
        showProgress();
        showSummarizeText();
        mPresenter.getData(calendar, ViewType.DAY);
        mStepBarChartMinute.setVisibility(View.VISIBLE);
        mLabelStepBarChart.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onWeekChange(Calendar calendar) {
        super.onWeekChange(calendar);
        Timber.d("on week change");
        showProgress();
        showAverageText();
        mPresenter.getData(calendar, ViewType.WEEK);
        mStepBarChartMinute.setVisibility(View.GONE);
        mLabelStepBarChart.setVisibility(View.GONE);
    }

    @Override
    protected void onMonthChange(Calendar calendar) {
        super.onMonthChange(calendar);
        Timber.d("on month change");
        showProgress();
        showAverageText();
        mPresenter.getData(calendar, ViewType.MONTH);
        mStepBarChartMinute.setVisibility(View.GONE);
        mLabelStepBarChart.setVisibility(View.GONE);
    }

    @Override
    protected int getMainColor() {
        return R.color.step_chart;
    }

    @Override
    protected BarChart getMainChart() {
        return findViewById(R.id.step_bar_chart);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.step_activity_title);
    }

    @Override
    protected StepPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void showChartData(ArrayList<BarEntry> entries) {
        hideProgress();
        setBarChartData(entries, R.color.step_chart);
    }

    @Override
    public void showMinuteDataChart(ArrayList<BarEntry> entries) {
        setBarChartSteps(entries);
    }

    @Override
    public void loadDataFail(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void showData(GenkiStepModel genkiStepModel) {
        if (genkiStepModel == null || genkiStepModel.getTimestamp() == 0) {
            return;
        }

        String numberSteps = String.valueOf((int) genkiStepModel.getNumAverage());
        mNumberSteps.setText(numberSteps);
    }

    private void setBarChartSteps(ArrayList<BarEntry> entries) {
        if (entries == null || entries.size() == 0) {
            mStepBarChartMinute.clear();
            return;
        }
        BarDataSet barDataSet;
        barDataSet = new BarDataSet(entries, "");

        barDataSet.setDrawIcons(false);
        barDataSet.setColors(getResources().getColor(R.color.step_chart));

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(barDataSet);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(5f);
        data.setBarWidth(0.5f);

        if (mStepBarChartMinute.getBarData() != null) {
            mStepBarChartMinute.clearValues();
        }
        mStepBarChartMinute.setData(data);
        mStepBarChartMinute.invalidate();
    }

    private void initializeMinuteBarChart() {
        mStepBarChartMinute.setDrawBarShadow(false);
        mStepBarChartMinute.getDescription().setEnabled(false);
        mStepBarChartMinute.setDrawGridBackground(false);
        mStepBarChartMinute.setPinchZoom(false);
        mStepBarChartMinute.setTouchEnabled(false);
        mStepBarChartMinute.setMaxVisibleValueCount(0);

        YAxis rightAxis = mStepBarChartMinute.getAxisRight();
        rightAxis.setLabelCount(2, false);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        rightAxis.setAxisMinimum(1f);
        rightAxis.setGridDashedLine(new DashPathEffect(new float[]{10.0f, 5.0f}, 0));
        rightAxis.setDrawAxisLine(false);

        YAxis leftAxis = mStepBarChartMinute.getAxisLeft();
        leftAxis.setLabelCount(2, false);
        leftAxis.setAxisMinimum(1f);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);

        mStepBarChartMinute.getLegend().setEnabled(false);
        mStepBarChartMinute.getXAxis().setDrawGridLines(false);
        mStepBarChartMinute.getXAxis().setDrawAxisLine(true);
        mStepBarChartMinute.getXAxis().setPosition(BOTTOM);
        mStepBarChartMinute.getXAxis().setDrawLabels(false);

        mStepBarChartMinute.setVisibleXRangeMaximum(48);
        mStepBarChartMinute.setVisibleXRangeMinimum(48);
        mStepBarChartMinute.setMinOffset(4f);
    }
}
