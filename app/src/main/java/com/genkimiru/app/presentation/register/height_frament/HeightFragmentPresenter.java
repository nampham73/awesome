package com.genkimiru.app.presentation.register.height_frament;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightFragmentRouter;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightFragmentView;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightPresenter;

public class HeightFragmentPresenter extends BasePresenterImp<HeightFragmentView,
        HeightFragmentRouter> implements HeightPresenter {

    private UserUseCase mUserUseCase;

    public HeightFragmentPresenter(Context context, HeightFragmentView heightFragmentView,
                                   HeightFragmentRouter heightFragmentRouter, UserUseCase userUseCase) {
        super(context, heightFragmentView, heightFragmentRouter);
        mUserUseCase = userUseCase;
    }

    @Override
    public String validateHeight(String height) {
        if (Float.parseFloat(height) < 1) {
            height = "1";
        }
        if (Float.parseFloat(height) > 300) {
            height = "300";
        }
        return height;
    }
}
