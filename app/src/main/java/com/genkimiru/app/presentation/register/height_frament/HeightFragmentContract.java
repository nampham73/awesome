package com.genkimiru.app.presentation.register.height_frament;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface HeightFragmentContract {

    interface HeightPresenter extends BasePresenter {
        String validateHeight(String height);
    }

    interface HeightFragmentView {

    }

    interface HeightFragmentRouter {

    }
}
