package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.BaseSliderFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.widget.CustomProgressBar;
import com.genkimiru.app.base.widget.CustomViewPager;
import com.genkimiru.app.data.model.GenkiChallengeModel;
import com.genkimiru.app.data.model.GenkiContestDetailModel;
import com.genkimiru.app.presentation.challenge_detail_history.ChallengeDetailHistoryActivity;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengePresenter;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengeView;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengeRouter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_COUNT;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_DESCRIPTION;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_STATUS;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_TARGET;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_USER_CONTEST_CHALLENGE_ID;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_USER_DATA;

public class InformationChallengeFragment extends BaseSliderFragment<InformationChallengeFragmentAdapter, InformationChallengePresenter>
        implements InformationChallengeView, InformationChallengeRouter {

    @BindView(R.id.challenge_history_count)
    TextView mCountTxt;
    @BindView(R.id.challenge_history_target)
    TextView mTargetTxt;
    @BindView(R.id.challenge_history_description)
    TextView mDescriptionTxt;
    @BindView(R.id.challenge_history_start)
    Button mChallengeStartBtn;
    @BindView(R.id.arc_progress_infor)
    CustomProgressBar mProgressBar;

    @Inject
    InformationChallengePresenter mPresenter;

    private CountDownTimer mCountDownTimer;
    private GenkiChallengeModel mChallengeModel;
    private static String START_CHALLENGE_FLAG = "start";
    private ChallengeDetailHistoryActivity mActivity;

    private static final int FINE_LOCATION_PERMISSION_REQUEST = 1;
    private static final int CONNECTION_RESOLUTION_REQUEST = 2;
    private static final int REQUEST_CHECK_SETTINGS = 3;
    private GoogleApiClient googleApiClient;
    private Location mLastLocation;
    final static int REQUEST_LOCATION = 199;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final LocationManager manager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(mActivity)) {
            MessageUtil.showShortToast(mActivity, "GPS already active");
        }

        if (!hasGPSDevice(mActivity.getApplicationContext())) {
            MessageUtil.showShortToast(mActivity, "Gps not Supported");
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(mActivity)) {
            MessageUtil.showShortToast(mActivity, "Gps not enable");
            enableLocation();
        } else {
            MessageUtil.showShortToast(mActivity, "GPS already active");
        }
    }

    private void enableLocation() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            SettingsClient client = LocationServices.getSettingsClient(mActivity);
            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

            task.addOnFailureListener(mActivity, e -> {
                int statusCode = ((ApiException) e).getStatusCode();
                if (statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult (mActivity, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error
                    }
                }
            });
        }
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void setupGoogleApiClient() {
//        if (mGoogleApiClient == null) {
//            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
//                    .addConnectionCallbacks(mActivity)
//                    .addOnConnectionFailedListener(mActivity)
//                    .addApi(LocationServices.API)
//                    .build();
//        }
    }


    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
            dialog.show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, Constants.ChallengePermission.REQUEST_LOCATION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_CANCELED) {
               return;
            } else if (resultCode == RESULT_OK) {
                findLocation();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mActivity = (ChallengeDetailHistoryActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        setInitialValue();

        Dexter.withActivity(mActivity)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            // all permission are granted
                        } else {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(error -> {
                    MessageUtil.showShortToast(mActivity, error.toString());
                })
                .onSameThread()
                .check();
    }

    @Override
    protected CustomViewPager getViewPager() {
        return getView().findViewById(R.id.fragment_challenge_information_viewpager);
    }

    @Override
    protected TabLayout getTabLayout() {
        return getView().findViewById(R.id.fragment_challenge_information_tablayout);
    }

    @Override
    protected InformationChallengeFragmentAdapter getPagerAdapter() {
        return new InformationChallengeFragmentAdapter(getChildFragmentManager());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_challenge_information;
    }

    @Override
    protected InformationChallengePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @OnClick(R.id.challenge_history_start)
    void countDownToJoin() {
        enableButton(false);
        lockScreen();

        mCountDownTimer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mChallengeStartBtn.setText(String.valueOf((int) millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                unlockScreen();
                resetButton();
                mActivity.showProgress();
                mPresenter.processChallenge(mChallengeModel.getUserContestChallengeId(), START_CHALLENGE_FLAG, mPresenter.getLocation());
            }
        }.start();
    }

    private void lockScreen() {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void unlockScreen() {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void enableButton(Boolean isEnabled) {
        mChallengeStartBtn.setEnabled(isEnabled);
    }

    private void resetButton() {
        mChallengeStartBtn.setText("Go");
        enableButton(true);
    }

    private void setInitialValue() {
        if (getActivity().getIntent() == null) return;
        mChallengeModel = (GenkiChallengeModel) getActivity().getIntent().getSerializableExtra(CHALLENGE_USER_DATA);
        int target = mChallengeModel.getTarget();
        int count = mChallengeModel.getSumChallengeComplete();
        int status = mChallengeModel.getChallengeStatus();
        String description = mChallengeModel.getDescription();

        int percentComplete = (count/target) * 100;

        mProgressBar.setProgress(percentComplete);
        mProgressBar.setStrokeWidth(17);
        mProgressBar.setImageID(getIconChallengeWalking(status));
        mProgressBar.setBottomText(String.valueOf(count/target) + getActivity().getString(R.string.contest_detail_percent));

        mCountTxt.setText(String.valueOf(count));
        mTargetTxt.setText(String.valueOf(target));
        mDescriptionTxt.setText(description);

        hideStartButton(status != GenkiContestDetailModel.ContestStatus.NOT_JOIN);
    }

    private void hideStartButton(Boolean joined) {
        mChallengeStartBtn.setEnabled(joined);
        if (!joined) {
            mChallengeStartBtn.setAlpha(0.5f);
        }
    }

    @DrawableRes
    private int getIconChallengeWalking(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_walking_r;
            case GenkiContestDetailModel.ContestStatus.FINISHED:
                return R.drawable.ic_walking_b;
            default:
                return R.drawable.ic_walking;
        }
    }

    @Override
    public void onProcessChallengeSuccess() {
        mActivity.hideProgress();
        mChallengeStartBtn.setText(R.string.challeng_history_start);
    }

    @Override
    public void onProcessChallengeError(String message) {
        mActivity.hideProgress();
        MessageUtil.createMessageDialog(message).show(getActivity().getSupportFragmentManager());
        mChallengeStartBtn.setText(R.string.challeng_history_start);
    }

    @Override
    public void onGetLocationSuccess() {

    }

    @Override
    public void onGetLocationError() {

    }

    @Override
    public void gotoChallengeWalking() {
        mActivity.hideProgress();
        Intent intent = new Intent(getActivity(), WalkingChallengeActivity.class);
        intent.putExtra(CHALLENGE_USER_DATA, mChallengeModel);
        getActivity().startActivity(intent);
    }

    private void findLocation() {
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    FINE_LOCATION_PERMISSION_REQUEST);
        } else {
            LocationServices.getFusedLocationProviderClient(mActivity)
                    .getLastLocation().addOnSuccessListener(location -> {
                if (location != null) {
                    mLastLocation = location;
                }
            });
        }
    }
}
