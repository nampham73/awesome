package com.genkimiru.app.presentation.walking_challenge;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface WalkingChallengeActivityContract {

    interface WalkingChallengePresenter extends BasePresenter {
        void processChallenge(int id);
    }

    interface WalkingChallengeView {
        void onProcessChallengeSuccess(String message);
        void onProcessChallengeError(String message);
    }

    interface WalkingChallengeRouter {
        void gotoChallengeDetail();
    }
}
