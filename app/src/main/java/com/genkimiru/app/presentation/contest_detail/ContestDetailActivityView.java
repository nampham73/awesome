package com.genkimiru.app.presentation.contest_detail;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.mvp.view.BaseActivityView;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiChallengeModel;
import com.genkimiru.app.data.model.GenkiContestDetailModel;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.data.model.GenkiUserContestChallenge;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailPresenter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailView;
import com.genkimiru.app.presentation.contest_detail.contest_detail_adapter.ContestDetailAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.CONTEST_ID;
import static com.genkimiru.app.Constants.Extra.IS_CONTEST_HISTORY;

public class ContestDetailActivityView extends BaseActivityView<ContestDetailActivity, ContestDetailPresenter>
        implements ContestDetailView, View.OnClickListener {

    @BindView(R.id.contest_detail_description_tv)
    TextView mDescriptionTxt;
    @BindView(R.id.contest_detail_total_join_tv)
    TextView mTotalJoinTxt;
    @BindView(R.id.contest_detail_award_user_tv)
    TextView mAwardUserTxt;
    @BindView(R.id.contest_detail_reward_point_tv)
    TextView mRewardPointTxt;
    @BindView(R.id.contest_detail_challenge_rcv)
    RecyclerView mRecyclerView;
    @BindView(R.id.contest_detail_startday_tv)
    TextView mStartDayTxt;
    @BindView(R.id.contest_detail_endday_tv)
    TextView mEndDayTxt;
    @BindView(R.id.contest_detail_sum_challenge_complete_tv)
    TextView mSumChallengeCompleteTxt;
    @BindView(R.id.contest_detail_challenge_icon_iv)
    ImageView mContestDetailChallengeImg;
    @BindView(R.id.contest_detail_won)
    LinearLayout mWonLayout;
    @BindView(R.id.contest_detail_joined)
    LinearLayout mJoinedLayout;
    @BindView(R.id.contest_detail_join_time_layout)
    RelativeLayout mJoinTimeLayout;
    @BindView(R.id.contest_detail_title_tv)
    TextView mContestDetailTxt;
    @BindView(R.id.contest_detail_image_iv)
    ImageView mImageCover;
    @BindView(R.id.contest_detail_space_view)
    View mSpaceView;
    @BindView(R.id.contest_detail_join_btn)
    Button mJoinButton;

    private ContestDetailAdapter mAdapter;
    private List<GenkiChallengeModel> mChallengeModels;
    private List<GenkiUserContestChallenge> mGenkiUserContestChallenges;
    private boolean mIsExpandableDescription;
    private String mContestDescription;
    private Integer mContestId;

    public ContestDetailActivityView(ContestDetailActivity activity) {
        super(activity);
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        initialize();
    }

    @Override
    public void onGetContestDetailSuccess(GenkiContestDetailModel contestDetailModel) {
        mActivity.hideProgress();
        mActivity.setToolbarTitle(contestDetailModel.getName());

        float challengePercent;

        mChallengeModels = new ArrayList<>();
        mGenkiUserContestChallenges = new ArrayList<>();
        mChallengeModels = contestDetailModel.getListChallenge();
        mGenkiUserContestChallenges = contestDetailModel.getListGenkiUserContestChallenges();

        if (mChallengeModels == null) {
            return;
        }

        SparseIntArray mapChallenge = getSumChallengeCount(mGenkiUserContestChallenges);
        int sumChallengeComplete = 0;
        if (mapChallenge != null) {
            for (int i = 0; i < mChallengeModels.size(); i++) {
                int sumChallengeCount = mapChallenge.get(mChallengeModels.get(i).getId());
                challengePercent = ((float) sumChallengeCount / mChallengeModels.get(i).getTarget()) * 100;
                mChallengeModels.get(i).setPercentChallenge(challengePercent);
                if (challengePercent == 100) {
                    sumChallengeComplete += 1;
                }
                if (mGenkiUserContestChallenges == null) {
                    return;
                }
                for (int j = 0; j < mGenkiUserContestChallenges.size(); j++) {
                    mChallengeModels.get(i).setChallengeStatus(mGenkiUserContestChallenges.get(j).getChallenge_status());
                    mChallengeModels.get(i).setUserContestChallengeId(mGenkiUserContestChallenges.get(j).getId());
                }
            }
        }

        Locale currentLocale = mActivity.getResources().getConfiguration().locale;
        String startDate = DateUtil.convertToHourYearMonthDayString(DateUtil.Format.API_FORMAT, contestDetailModel.getStart_date(), currentLocale);
        String endDate = DateUtil.convertToHourYearMonthDayString(DateUtil.Format.API_FORMAT, contestDetailModel.getEnd_date(), currentLocale);
        String awardUser = userAward(contestDetailModel.getCurrent_number_award_user(),contestDetailModel.getMax_number_award_user());
        String sumComplete = String.format(mActivity.getString(R.string.contest_detail_complete), sumChallengeComplete, mChallengeModels.size());
        int challengeType = getIconForType(contestDetailModel.getType(), contestDetailModel.getUser_contest_status());

        mContestDescription = contestDetailModel.getDescription();
        mDescriptionTxt.setText(mContestDescription);
        makeTextViewResizable(mDescriptionTxt, 2, mActivity.getString(R.string.contest_detail_show_more_text));

        mTotalJoinTxt.setText(String.valueOf(contestDetailModel.getTotal_join()));
        mStartDayTxt.setText(startDate);
        mEndDayTxt.setText(endDate);
        mAwardUserTxt.setText(awardUser);
        mRewardPointTxt.setText(String.valueOf(contestDetailModel.getReward_point()));
        mSumChallengeCompleteTxt.setText(sumComplete);
        mContestDetailChallengeImg.setImageResource(challengeType);
        mContestDetailTxt.setVisibility(StringUtil.isEmpty(contestDetailModel.getTitle()) ? View.GONE : View.VISIBLE);
        mContestDetailTxt.setText(contestDetailModel.getTitle());
        Glide.with(mActivity.getApplicationContext())
                .load(contestDetailModel.getDetailImageUrl())
                .into(mImageCover);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mAdapter.set(mChallengeModels);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);

        hideJoinButton(contestDetailModel.getUser_contest_status() != GenkiContestDetailModel.ContestStatus.NOT_JOIN);
    }

    @Override
    public void onGetContestDetailFail(String message) {
        MessageUtil.createMessageDialog(message, new TextDialogFragment.OnResultListener() {
            @Override
            public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                mActivity.finish();
            }

            @Override
            public void onClickRightButton(TextDialogFragment textDialogFragment) {
                // Do nothing
            }
        });
    }

    @Override
    public void onJoinContestSuccess(String message) {
        MessageUtil.showShortToast(mActivity.getApplicationContext(), message);
        hideJoinButton(true);
        reloadData(mContestId);
    }

    @Override
    public void onJoinContestError(String message) {
        MessageUtil.createMessageDialog(message).show(mActivity.getSupportFragmentManager());
    }

    private void hideJoinButton(Boolean isHidden) {
        mJoinButton.setVisibility(isHidden ? View.INVISIBLE : View.VISIBLE);
    }

    private void reloadData(Integer contestId) {
        mPresenter.getContestDetail(contestId);
    }

    private void initialize() {
        mAdapter = new ContestDetailAdapter(mActivity);
        if (mActivity.getIntent() == null) return;
        mContestId = mActivity.getIntent().getIntExtra(CONTEST_ID, 1);
        boolean isHistory = mActivity.getIntent().getBooleanExtra(IS_CONTEST_HISTORY, false);
        mWonLayout.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        mJoinedLayout.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        mJoinTimeLayout.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        mContestDetailTxt.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        mSpaceView.setVisibility(isHistory ? View.GONE : View.VISIBLE);
        mPresenter.getContestDetail(mContestId);
        mActivity.showProgress();

        mDescriptionTxt.setOnClickListener(view -> {
            if (mIsExpandableDescription && StringUtil.isNotEmpty(mContestDescription)) {
                String text = mContestDescription + mActivity.getString(R.string.contest_detail_show_less_text);
                mDescriptionTxt.setText(text);
                mIsExpandableDescription = false;
            } else if (!mIsExpandableDescription) {
                mDescriptionTxt.setText(mContestDescription);
                makeTextViewResizable(mDescriptionTxt, 2, mActivity.getString(R.string.contest_detail_show_more_text));
            }
        });

        mJoinButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.contest_detail_join_btn:
                mPresenter.joinContest(mContestId);
                break;
            default:break;
        }
    }

    private String userAward(int current, int total) {
        if (current > 100 || total > 100) return String.format("%1$s%n/%2$s", current, total);
        return String.format("%1$s/%2$s", current, total);
    }

    private SparseIntArray getSumChallengeCount(List<GenkiUserContestChallenge> mGenkiUserContestChallenges) {
        SparseIntArray sumChallengeCount = new SparseIntArray();
        if (mGenkiUserContestChallenges == null) {
            return null;
        }
        Disposable disposable = Flowable.just(mGenkiUserContestChallenges)
                .flatMapIterable(userContestChallenges -> userContestChallenges)
                .groupBy(GenkiUserContestChallenge::getChallenge_id)
                .subscribe(items -> items.toList().subscribe(userContestChallenges -> {
                    Timber.d("data: %s", userContestChallenges.toString());
                    if (userContestChallenges.size() > 0) {
                        int sumCount = 0;
                        int id = userContestChallenges.get(0).getChallenge_id();
                        for (int i = 0; i < userContestChallenges.size(); i++) {
                            sumCount += userContestChallenges.get(i).getChallenge_count();
                        }
                        sumChallengeCount.put(id, sumCount);
                    }
                }));
        return sumChallengeCount;
    }

    @DrawableRes
    private int getIconForType(int type, int status) {
        switch (type) {
            case GenkiContestModel.ContestType.DAILY:
                return getIconForDaily(status);
            case GenkiContestModel.ContestType.WEEKLY:
                return getIconForWeekly(status);
            case GenkiContestModel.ContestType.MONTHLY:
                return getIconForMonthly(status);
            default:
                return getIconForOneDay(status);
        }
    }

    @DrawableRes
    private int getIconForWeekly(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_weekly_red;
            case GenkiContestDetailModel.ContestStatus.NOT_START:
                return R.drawable.ic_weekly_blue;
            default:
                return R.drawable.ic_weekly_normal;
        }
    }

    @DrawableRes
    private int getIconForDaily(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_daily_red;
            case GenkiContestDetailModel.ContestStatus.NOT_START:
                return R.drawable.ic_daily_blue;
            default:
                return R.drawable.ic_daily_normal;
        }
    }

    @DrawableRes
    private int getIconForOneDay(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_oneday_red;
            case GenkiContestDetailModel.ContestStatus.NOT_START:
                return R.drawable.ic_oneday_blue;
            default:
                return R.drawable.ic_oneday_normal;
        }
    }

    @DrawableRes
    private int getIconForMonthly(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_monthly_red;
            case GenkiContestDetailModel.ContestStatus.NOT_START:
                return R.drawable.ic_monthly_blue;
            default:
                return R.drawable.ic_monthly_normal;
        }
    }

    private void makeTextViewResizable(final TextView tv, int maxLine, String expandText) {
        if (maxLine <= 0) {
            int lineEndIndex = tv.getLayout().getLineEnd(0);
            String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + "" + expandText;
            tv.setText(text);
            mIsExpandableDescription = true;
        } else if(tv.getLineCount() > maxLine) {
            int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
            String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + "" + expandText;
            tv.setText(text);
            mIsExpandableDescription = true;
        }
    }
}
