package com.genkimiru.app.presentation.chart;

import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;

public abstract class BarChartActivity<TPresenter extends BasePresenter>
        extends BaseChartActivity<TPresenter, BarChart> {

    public static final int CHART_COLUMN = 7;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initializeMainChart() {
        mMainChart.setDrawBarShadow(false);
        mMainChart.getDescription().setEnabled(false);
        mMainChart.setDrawGridBackground(false);
        mMainChart.setPinchZoom(false);
        mMainChart.setTouchEnabled(false);
        mMainChart.setMaxVisibleValueCount(0);

        YAxis rightAxis = mMainChart.getAxisRight();
        rightAxis.setLabelCount(3, false);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setGridDashedLine(new DashPathEffect(new float[]{10.0f, 5.0f}, 0));
        rightAxis.setDrawAxisLine(false);

        YAxis leftAxis = mMainChart.getAxisLeft();
        leftAxis.setLabelCount(3, false);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);

        mMainChart.getLegend().setEnabled(false);
        mMainChart.getXAxis().setDrawGridLines(false);
        mMainChart.getXAxis().setDrawAxisLine(false);
        mMainChart.getXAxis().setDrawLabels(false);
        mMainChart.setMinOffset(0f);
        mMainChart.setExtraTopOffset(15f);
        mMainChart.setExtraBottomOffset(8f);
    }

    protected void setBarChartData(ArrayList<BarEntry> entries, int barColor) {
        if (entries == null || entries.size() == 0) {
            mMainChart.clear();
            return;
        }

        BarDataSet set1;
        set1 = new BarDataSet(entries, "");

        set1.setDrawIcons(false);
        set1.setColors(getResources().getColor(barColor));

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setBarWidth(0.3f);

        if (mMainChart.getBarData() != null) {
            mMainChart.clearValues();
        }

        mMainChart.setData(data);
        mMainChart.invalidate();
    }
}
