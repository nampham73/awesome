package com.genkimiru.app.presentation.register.smorking_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface SmokingFragmentContract {

    interface SmokingPresenter extends BasePresenter {

    }

    interface SmokingFragmentView {

    }

    interface SmokingFragmentRouter {

    }
}
