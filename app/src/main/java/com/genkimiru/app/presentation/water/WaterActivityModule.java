package com.genkimiru.app.presentation.water;

import android.content.Context;

import com.genkimiru.app.base.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterPresenter;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterRouter;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterView;

@Module
public class WaterActivityModule {
    @Provides
    @ActivityScope
    WaterPresenter providePresenter(Context context, WaterView waterView,  WaterRouter waterRouter) {
        return new WaterActivityPresenter(context, waterView, waterRouter);
    }

    @Provides
    @ActivityScope
    WaterView provideView(WaterActivity waterActivity) {
        return waterActivity;
    }

    @Provides
    @ActivityScope
    WaterRouter provideRouter(WaterActivity waterActivity) {
        return waterActivity;
    }
}
