package com.genkimiru.app.presentation.update_user_infor.height;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityContract.UpdateHeightPresenter;
import static com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityContract.UpdateHeightRouter;
import static com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityContract.UpdateHeightView;

public class UpdateHeightActivity extends SimpleBaseActivity<UpdateHeightPresenter>
        implements UpdateHeightView, UpdateHeightRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_height_edt)
    EditText mHeightEdt;

    private MenuItem mApplyMenuItem;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateHeightPresenter mUpdateHeightPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_height;
    }

    @Override
    protected UpdateHeightPresenter registerPresenter() {
        return mUpdateHeightPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateHeightPresenter.updateHeight(mUserInfoRequest.getHeight());
            showProgress();
        }
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mHeightEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(height -> {
                    Timber.d("height: %s", height.toString());
                    if (StringUtil.isEmpty(height)) {
                        setEnabledMenuApply(false);
                        return;
                    }
                    setEnabledMenuApply(true);
                    height = mUpdateHeightPresenter.validateHeight(height.toString().trim());
                    mHeightEdt.setText(height.toString());
                    mHeightEdt.setSelection(mHeightEdt.getText().length());
                    setHeight();
                });
    }

    public void setHeight() {
        mUserInfoRequest.setHeight(Float.parseFloat(mHeightEdt.getText().toString().trim()));
        mUserInfoRequest.setHeightType(1);
    }

    public void initialDefaultValue() {
        Timber.d("initialDefaultValue: %s", mGenkiUserInfoModel.getHeight());
        mHeightEdt.setText(String.valueOf(mGenkiUserInfoModel.getHeight()));
        mGenkiUserInfoModel.setHeightType(1);
    }


    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
