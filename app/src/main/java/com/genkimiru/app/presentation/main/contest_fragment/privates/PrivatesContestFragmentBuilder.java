package com.genkimiru.app.presentation.main.contest_fragment.privates;

import com.genkimiru.app.base.scope.ChildFragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PrivatesContestFragmentBuilder {

    @ChildFragmentScope
    @ContributesAndroidInjector(modules = PrivatesContestFragmentModule.class)
    abstract PrivatesContestFragment privatesContestFragment();
}
