package com.genkimiru.app.presentation.walking_challenge_map;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapPresenter;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapRouter;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;

public class WalkingChallengeMapActivity extends SimpleBaseActivity<WalkingChallengeMapPresenter> implements WalkingChallengeMapView, WalkingChallengeMapRouter, OnMapReadyCallback {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    GenkiUserInfoModel mUserInfoModel;
    @Inject
    WalkingChallengeMapPresenter mWalkingChallengeMapPresenter;

    private GoogleMap mMap;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_walking_challenge_map;
    }

    @Override
    protected WalkingChallengeMapPresenter registerPresenter() {
        return mWalkingChallengeMapPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMap();
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        Drawable circleDrawable = getResources().getDrawable(R.drawable.ic_avatar);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

        LatLng currentLoc = new LatLng(mUserInfoModel.getLatitude(), mUserInfoModel.getLongitude());
        mMap.addMarker(new MarkerOptions().position(currentLoc).title("FPT Software").icon(markerIcon));

        CameraPosition cp = new CameraPosition.Builder().target(currentLoc).zoom(13).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
    }

    private void createMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(WalkingChallengeMapActivity.this);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, 150, 150);
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
