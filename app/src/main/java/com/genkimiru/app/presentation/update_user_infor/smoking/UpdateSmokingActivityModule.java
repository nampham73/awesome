package com.genkimiru.app.presentation.update_user_infor.smoking;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingPresenter;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingRouter;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingView;
@Module
public class UpdateSmokingActivityModule {
    @Provides
    @ActivityScope
    UpdateSmokingView provideSmokingView(UpdateSmokingActivity updateSmokingActivity) {
        return updateSmokingActivity;
    }

    @Provides
    @ActivityScope
    UpdateSmokingRouter provideSmokingRouter(UpdateSmokingActivity updateSmokingActivity) {
        return updateSmokingActivity;
    }

    @Provides
    @ActivityScope
    UpdateSmokingPresenter provideSmokingPresenter(Context context, UpdateSmokingView updateSmokingView, UpdateSmokingRouter updateSmokingRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        return new UpdateSmokingActivityPresenter(context, updateSmokingView, updateSmokingRouter, userUseCase, genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserInfoRequest provideUserInforRequest(GenkiUserInfoModel genkiUserInfoModel) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return new UserInfoRequest(token, genkiUserInfoModel.getHealthSource());
    }

    @Provides
    @ActivityScope
    UserUseCase providesUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }

}
