package com.genkimiru.app.presentation.forgot_password.new_password_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.forgot_password.new_password_fragment.NewPasswordFragmentContract.*;

@Module
public class NewPasswordFragmentModule {

    @Provides
    @FragmentScope
    NewPasswordView provideView(NewPasswordFragment newPasswordFragment) {
        return newPasswordFragment;
    }

    @Provides
    @FragmentScope
    NewPasswordRouter provideRouter(NewPasswordFragment newPasswordFragment) {
        return newPasswordFragment;
    }

    @Provides
    @FragmentScope
    NewPasswordPresenter providePresenter(Context context, NewPasswordView view, NewPasswordRouter router, UserUseCase userUseCase) {
        return new NewPasswordFragmentPresenter(context, view, router, userUseCase);
    }
}
