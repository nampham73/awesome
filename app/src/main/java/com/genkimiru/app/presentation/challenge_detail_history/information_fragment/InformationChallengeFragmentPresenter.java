package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengePresenter;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengeRouter;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengeView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class InformationChallengeFragmentPresenter extends BasePresenterImp<InformationChallengeView, InformationChallengeRouter>
        implements InformationChallengePresenter {

    private UserUseCase mUserUseCase;
    private Location mLocation;

    public InformationChallengeFragmentPresenter(Context context,
                                                 InformationChallengeView informationChallengeView,
                                                 InformationChallengeRouter informationChallengeRouter,
                                                 UserUseCase userUseCase) {
        super(context, informationChallengeView, informationChallengeRouter);

        mUserUseCase = userUseCase;
    }

    @Override
    public void processChallenge(int id, String flag, Location location) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        ChallengeRequest request = new ChallengeRequest(id, flag, location, token);
        mUserUseCase.execute(new ProcessChallengeSubscriber(), UserParam.Factory.processChallenge(request));
    }

    @Override
    public Location getLocation() {
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(mContext);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            client.getLastLocation().addOnSuccessListener(location -> {
                mLocation = location;
            });
        } else {
            mLocation = new Location("0,0");
        }

        return mLocation;
    }

    private class ProcessChallengeSubscriber extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean aBoolean) {
            mRouter.gotoChallengeWalking();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("ProcessChallengeSubscriber Error: %s", throwable.getMessage());
            mView.onProcessChallengeError(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }
}
