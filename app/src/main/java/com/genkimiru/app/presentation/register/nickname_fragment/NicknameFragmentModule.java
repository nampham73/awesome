package com.genkimiru.app.presentation.register.nickname_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragmentContract.NicknameRouter;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragmentContract.NicknamePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class NicknameFragmentModule {

    @Provides
    @FragmentScope
    NicknameFragmentContract.NicknameView provideView(NicknameFragment nicknameFragment) {
        return nicknameFragment;
    }

    @Provides
    @FragmentScope
    NicknameFragmentContract.NicknameRouter contestFragmentRouter(NicknameFragment nicknameFragment) {
        return nicknameFragment;
    }

    @Provides
    @FragmentScope
    NicknamePresenter providePresenter(Context context, NicknameFragmentContract.NicknameView nicknameView,
                                       NicknameRouter nicknameRouter, UserUseCase userUseCase) {
        return new NicknameFragmentPresenter(context, nicknameView, nicknameRouter, userUseCase);
    }
}
