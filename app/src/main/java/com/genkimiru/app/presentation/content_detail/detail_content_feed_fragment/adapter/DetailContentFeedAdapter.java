package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;

import java.util.List;

public class DetailContentFeedAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<IDetailContentModel> mItems;
    private OnAdapterClickListener mOnAdapterClickedListener;

    public void setOnAdapterClickedListener(OnAdapterClickListener mOnAdapterClickedListener) {
        this.mOnAdapterClickedListener = mOnAdapterClickedListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case Constants.RecyclerViewType.DETAIL_ARTICLE_VIEW_TYPE:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.holder_detail_article_view, parent, false);
                return new ArticleViewHolder(view, mOnAdapterClickedListener);
            case Constants.RecyclerViewType.FEED_VIEW_TYPE:
                View feedView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.holder_feed_view, parent, false);
                return new FeedViewHolder(feedView);

        }
        throw new UnsupportedOperationException("we not support viewtype: " + viewType + " please check again!");
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if (isListNotEmpty()) {
            holder.setData(mItems.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isListNotEmpty()) {
            IDetailContentModel model = mItems.get(position);
            return model.getViewType();
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return isListNotEmpty() ? mItems.size() : 0;
    }

    private boolean isListNotEmpty() {
        return mItems != null && mItems.size() > 0;
    }

    public void setItems(List<IDetailContentModel> mItems) {
        this.mItems = mItems;
    }

    public void add(IDetailContentModel model, int index){
        if (mItems!=null && mItems.size()>0){
            mItems.add(index,model);
            notifyItemInserted(index);
        }
    }
}
