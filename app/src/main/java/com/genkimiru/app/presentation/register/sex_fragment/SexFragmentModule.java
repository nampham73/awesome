package com.genkimiru.app.presentation.register.sex_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexFragmentRouter;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexFragmentView;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SexFragmentModule {

    @Provides
    @FragmentScope
    SexFragmentView provideView(SexFragment sexFragment) {
        return sexFragment;
    }

    @Provides
    @FragmentScope
    SexFragmentRouter provideRouter(SexFragment sexFragment) {
        return sexFragment;
    }

    @Provides
    @FragmentScope
    SexPresenter providePresenter(Context context, SexFragmentView sexFragmentView, SexFragmentRouter sexFragmentRouter, UserUseCase userUseCase) {
        return new SexFragmentPresenter(context, sexFragmentView, sexFragmentRouter, userUseCase);
    }
}
