package com.genkimiru.app.presentation.login;

import com.facebook.CallbackManager;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.Task;

public interface LoginActivityContract {

    interface LoginPresenter extends BasePresenter {
        boolean isValidUsernameLength(String dataInput);
        boolean isValidPasswordLength(String dataInput);
        boolean isValidUsername(String username);
        boolean isValidPassword(String password);
        void login(String username, String password);
        void loginFacebook(CallbackManager callbackManager);
        void loginWithGooglePlus(String code);
        void handleSignInResult(Task<GoogleSignInAccount> completedTask);
    }

    interface LoginView extends BaseView<LoginPresenter> {
        void showMessageDialog(String message);
        void showProgressDialog();
        void hideProgressDialog();
    }

    interface LoginRouter {
        void openEulaScreen(int index);
        void openForgotPasswordScreen();
        void openDashboardScreen();
        void openFacebookScreen();
        void openRegisterScreen(LoginUserInfoModel loginUserInfoModel);
        void openMainActivity();
        void openRegisterScreenFromLoginGoole(LoginUserInfoModel loginUserInfoModel);
    }
}
