package com.genkimiru.app.presentation.register.heart_beat_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatFragmentRouter;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatFragmentView;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class HeartBeatFragmentModule {

    @Provides
    @FragmentScope
    HeartBeatFragmentView provideView(HeartBeatFragment heartBeatFragment) {
        return heartBeatFragment;
    }

    @Provides
    @FragmentScope
    HeartBeatFragmentRouter provideRouter(HeartBeatFragment heartBeatFragment) {
        return heartBeatFragment;
    }

    @Provides
    @FragmentScope
    HeartBeatPresenter providePresenter(Context context, HeartBeatFragmentView heartBeatFragmentView,
                                        HeartBeatFragmentRouter heartBeatFragmentRouter, UserUseCase userUseCase) {
        return new HeartBeatFragmentPresenter(context, heartBeatFragmentView, heartBeatFragmentRouter, userUseCase);
    }
}
