package com.genkimiru.app.presentation.nac.fragment.month;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NacMonthFragmentContract {
    interface NacMonthPresenter extends BasePresenter{
            void onUpdateNacMonth();
    }
    interface  NacMonthView{
        void onUpdateSuccess();
        void onUpdateError();
    }
    interface NacMonthRouter{

    }
}
