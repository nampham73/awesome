package com.genkimiru.app.presentation.update_user_infor.smoking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.dialog.CheckboxDialog;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingPresenter;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingRouter;
import static com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityContract.UpdateSmokingView;

public class UpdateSmokingActivity extends SimpleBaseActivity<UpdateSmokingPresenter>
        implements UpdateSmokingView, UpdateSmokingRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_smorking_radio_group)
    RadioGroup mRadioGroup;
    @BindView(R.id.checkbox_sometime_smoking_tv)
    TextView mSometimeSmokingTxt;
    @BindView(R.id.checkbox_only_smoking_tv)
    TextView mOnlySmokingTxt;


    private CheckboxDialog smorkingDialog;
    private Disposable mDisposable;
    private String mLevelTxt;
    private int mCurrentSmokingButtonId;
    private boolean isDialogShowing = false;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;
    @Inject
    UpdateSmokingPresenter mUpdateSmokingPresenter;

    private MenuItem mApplyMenuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_healthy_smoking;
    }

    @Override
    protected UpdateSmokingPresenter registerPresenter() {
        return mUpdateSmokingPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateSmokingPresenter.updateSmoking(mUserInfoRequest.getSmokingLevel(),mUserInfoRequest.getSmokingeEachTime());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrentSmokingButtonId = mRadioGroup.getCheckedRadioButtonId();
        mDisposable = RxRadioGroup.checkedChanges(mRadioGroup)
                .skip(1) // Skip init value
                .subscribe(buttonId -> {
                    mCurrentSmokingButtonId = buttonId;
                    setEnabledMenuApply(true);
                    switch (buttonId) {
                        case R.id.checkbox_no_smoking:
                            Timber.d("Smoking level 1");
                            mUserInfoRequest.setSmokingLevel(RegisterUserRequest.SmokingLevel.LEVEL_NO_SMOKING.getValue());
                            mUserInfoRequest.setSmokingEachTime(RegisterUserRequest.SmokingEachTime.LEVEL_DEFAULT.getValue());
                            setDefaultView();
                            break;
                        case R.id.checkbox_sometime_smoking:
                            Timber.d("Smoking level 2");
                            mUserInfoRequest.setSmokingLevel(RegisterUserRequest.SmokingLevel.LEVEL_SOMETIME_SMOKING.getValue());
                            openSmokingDialog();
                            break;
                        case R.id.checkbox_only_smoking:
                            Timber.d("Smoking level 3");
                            mUserInfoRequest.setSmokingLevel(RegisterUserRequest.SmokingLevel.LEVEL_ALLWAYS_SMOKING.getValue());
                            openSmokingDialog();
                            break;
                    }
                });
        setTextLevelEachTime();
    }

    private void openSmokingDialog() {
        Timber.d("openSmokingDialog");
        if (isDialogShowing) {
            return;
        }

        smorkingDialog = new CheckboxDialog.CheckboxDialogBuilder()
                .setTitle(getString(R.string.register_smoking_title_dialog))
                .setLayout(R.layout.dialog_checbox_smoking_content)
                .dialogListener(new CheckboxDialog.DialogListener() {
                    @Override
                    public void onComplete(CheckboxDialog checkboxDialog) {
                        setTextLevelEachTime();
                        checkboxDialog.dismiss();
                    }

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.register_smoking_small_rb:
                                Timber.d("Smoking level each time: %d", 1);
                                mUserInfoRequest.setSmokingEachTime(RegisterUserRequest.SmokingEachTime.LEVEL_SMALL.getValue());
                                mLevelTxt = getString(R.string.register_smoking_small);
                                break;
                            case R.id.register_smoking_medium_rb:
                                Timber.d("Smoking level each time: %d", 2);
                                mUserInfoRequest.setSmokingEachTime(RegisterUserRequest.SmokingEachTime.LEVEL_MEDIUM.getValue());
                                mLevelTxt = getString(R.string.register_smoking_medium);
                                break;
                            case R.id.register_smoking_large_rb:
                                Timber.d("Smoking level each time: %d", 3);
                                mUserInfoRequest.setSmokingEachTime(RegisterUserRequest.SmokingEachTime.LEVEL_LARGE.getValue());
                                mLevelTxt = getString(R.string.register_smoking_large);
                                break;
                        }
                    }

                    @Override
                    public void onOpen(int checkedId) {
                        mUserInfoRequest.setSmokingEachTime(RegisterUserRequest.SmokingEachTime.LEVEL_SMALL.getValue());
                        mLevelTxt = getString(R.string.register_smoking_small);
                    }

                    @Override
                    public void onDismiss() {
                        isDialogShowing = false;
                    }
                }).build();
        smorkingDialog.show(getSupportFragmentManager());
        isDialogShowing = true;
    }

    private void setTextLevelEachTime() {
        switch (mCurrentSmokingButtonId) {
            case R.id.checkbox_sometime_smoking:
                mSometimeSmokingTxt.setText(("(" + mLevelTxt + ")"));
                mSometimeSmokingTxt.setVisibility(View.VISIBLE);
                mOnlySmokingTxt.setVisibility(View.GONE);
                break;
            case R.id.checkbox_only_smoking:
                mOnlySmokingTxt.setText("(" + mLevelTxt + ")");
                mSometimeSmokingTxt.setVisibility(View.GONE);
                mOnlySmokingTxt.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initialDefaultValue() {
        setSmokingLevel();
    }

    public void setSmokingLevel() {
        int smokingLevel = mGenkiUserInfoModel.getSmokingLevel();
        switch (RegisterUserRequest.SmokingLevel.getFromValue(smokingLevel)) {
            case LEVEL_NO_SMOKING:
                mRadioGroup.check(R.id.checkbox_no_smoking);
                break;
            case LEVEL_SOMETIME_SMOKING:
                mRadioGroup.check(R.id.checkbox_sometime_smoking);
                break;
            case LEVEL_ALLWAYS_SMOKING:
                mRadioGroup.check(R.id.checkbox_only_smoking);
                break;
            case NONE:
                break;
        }
        mCurrentSmokingButtonId = mRadioGroup.getCheckedRadioButtonId();
        setSmokeEachTime();
    }

    public void setSmokeEachTime() {
        int smokingEachTime = mGenkiUserInfoModel.getSmokingEachTime();
        switch (RegisterUserRequest.SmokingEachTime.getFromValue(smokingEachTime)) {
            case LEVEL_SMALL:
                mLevelTxt = getString(R.string.register_smoking_small);
                break;
            case LEVEL_MEDIUM:
                mLevelTxt = getString(R.string.register_smoking_medium);
                break;
            case LEVEL_LARGE:
                mLevelTxt = getString(R.string.register_smoking_large);
                break;
            case LEVEL_DEFAULT:
                break;
            case NONE:
                break;

        }
        setTextLevelEachTime();
    }

    private void setDefaultView() {
        mSometimeSmokingTxt.setVisibility(View.GONE);
        mOnlySmokingTxt.setVisibility(View.GONE);
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
