package com.genkimiru.app.presentation.register.blood_pressure_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressureFragmentRouter;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressureFragmentView;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressurePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class BloodPressureFragmentModule {

    @Provides
    @FragmentScope
    BloodPressureFragmentView provideView(BloodPressureFragment bloodPressureFragment) {
        return bloodPressureFragment;
    }

    @Provides
    @FragmentScope
    BloodPressureFragmentRouter provideRouter(BloodPressureFragment bloodPressureFragment) {
        return bloodPressureFragment;
    }

    @Provides
    @FragmentScope
    BloodPressurePresenter providePresenter(Context context, BloodPressureFragmentView bloodPressureFragmentView,
                                            BloodPressureFragmentRouter bloodPressureFragmentRouter, UserUseCase userUseCase) {
        return new BloodPressureFragmentPresenter(context, bloodPressureFragmentView, bloodPressureFragmentRouter, userUseCase);
    }
}
