package com.genkimiru.app.presentation.water;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;

public interface WaterActivityContract {
    interface WaterPresenter extends BasePresenter {

    }
    interface WaterView {

    }
    interface WaterRouter {

    }
}
