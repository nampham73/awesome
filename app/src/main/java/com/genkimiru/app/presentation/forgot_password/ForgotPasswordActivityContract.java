package com.genkimiru.app.presentation.forgot_password;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface ForgotPasswordActivityContract {

    interface ForgotPasswordPresenter extends BasePresenter {

    }

    interface ForgotPasswordView {

    }

    interface ForgotPasswordRouter {
        void openRecoverPasswordScreen();
        void openNewPasswordScreen();
        void completeRecover();
    }
}
