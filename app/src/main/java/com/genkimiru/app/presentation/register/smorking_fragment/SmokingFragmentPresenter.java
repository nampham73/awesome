package com.genkimiru.app.presentation.register.smorking_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingFragmentRouter;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingFragmentView;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingPresenter;

public class SmokingFragmentPresenter extends BasePresenterImp<SmokingFragmentView,
        SmokingFragmentRouter> implements SmokingPresenter {

    private UserUseCase mUserUseCase;

    public SmokingFragmentPresenter(Context context, SmokingFragmentView smokingFragmentView, SmokingFragmentRouter smokingFragmentRouter, UserUseCase userUseCase) {
        super(context, smokingFragmentView, smokingFragmentRouter);
        mUserUseCase = userUseCase;
    }
}
