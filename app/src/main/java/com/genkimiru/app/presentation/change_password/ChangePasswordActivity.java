package com.genkimiru.app.presentation.change_password;

import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.change_password.ChangePasswordActivityContract.*;

public class ChangePasswordActivity extends SimpleBaseActivity<ChangePasswordPresenter>
        implements ChangePasswordView, ChangePasswordRouter {

    @Inject
    ChangePasswordPresenter mPresenter;

    @BindView(R.id.change_password_old_edt)
    TextInputEditText mCurrentPasswordEdt;
    @BindView(R.id.change_password_new_edt)
    TextInputEditText mNewPasswordEdt;
    @BindView(R.id.change_password_new_confirm_edt)
    TextInputEditText mNewPasswordConfirmEdt;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.change_password_send_btn)
    Button mSendBtn;

    private CompositeDisposable mCompositeDisposable;
    private TextDialogFragment mCompleteDialog;
    private boolean mIsValidCurrentPassword;
    private boolean mIsValidCurrentPasswordLength;
    private boolean mIsValidNewPassword;
    private boolean mIsValidNewPasswordLength;
    private boolean mIsNewPasswordTheSameCurrent;
    private boolean mIsValidNewPasswordConfirm;
    private boolean mIsConfirmPasswordTheSameNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        setEnabledSendButton(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissCompleteDialog();
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar)
                .addBackButton()
                .build();
        mToolbarTitle.setText(getString(R.string.change_password));
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_change_password;
    }

    @Override
    protected ChangePasswordPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @OnClick(R.id.change_password_send_btn)
    void onSendClick(){
        if(!mIsValidCurrentPasswordLength) {
            showMessageDialog(getString(R.string.change_password_invalid_password_1));
            return;
        }

        if(!mIsValidNewPasswordLength) {
            showMessageDialog(getString(R.string.change_password_invalid_password_2));
            return;
        }

        if(mIsNewPasswordTheSameCurrent) {
            showMessageDialog(getString(R.string.change_password_invalid_password_3));
            return;
        }

        if(!mIsConfirmPasswordTheSameNewPassword) {
            showMessageDialog(getString(R.string.change_password_invalid_password_4));
            return;
        }

        mPresenter.sendChangePassword(mCurrentPasswordEdt.getText().toString(), mNewPasswordEdt.getText().toString());
    }

    private void initializeEventHandler(){
        mCompositeDisposable.add(
                RxTextView.textChanges(mCurrentPasswordEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(currentPassword -> {
                            Timber.d("current password: %s", currentPassword.toString());
                            mIsValidCurrentPassword = mPresenter.isValidPassword(currentPassword.toString().trim());
                            mIsValidCurrentPasswordLength = mPresenter.isValidPasswordLength(currentPassword.toString().trim());

                            mIsNewPasswordTheSameCurrent = mPresenter.isTheSamePassword(currentPassword.toString().trim(),
                                    mNewPasswordEdt.getText().toString().trim());

                            setEnabledSendButton(mIsValidCurrentPassword && mIsValidNewPassword && mIsValidNewPasswordConfirm);
                        }));

        mCompositeDisposable.add(
                RxTextView.textChanges(mNewPasswordEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(newPassword -> {
                            Timber.d("new password: %s", newPassword.toString());
                            mIsValidNewPassword = mPresenter.isValidPassword(newPassword.toString().trim());
                            mIsValidNewPasswordLength = mPresenter.isValidPasswordLength(newPassword.toString().trim());

                            mIsNewPasswordTheSameCurrent = mPresenter.isTheSamePassword(newPassword.toString().trim(),
                                    mCurrentPasswordEdt.getText().toString().trim());

                            mIsConfirmPasswordTheSameNewPassword = mPresenter.isTheSamePassword(
                                    newPassword.toString().trim(),
                                    mNewPasswordConfirmEdt.getText().toString().trim());

                            setEnabledSendButton(mIsValidCurrentPassword && mIsValidNewPassword && mIsValidNewPasswordConfirm);
                        }));

        mCompositeDisposable.add(
                RxTextView.textChanges(mNewPasswordConfirmEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(newPasswordConfirm -> {
                            Timber.d("new password confirm: %s", newPasswordConfirm.toString());
                            mIsValidNewPasswordConfirm = mPresenter.isValidPassword(newPasswordConfirm.toString().trim());

                            mIsConfirmPasswordTheSameNewPassword = mPresenter.isTheSamePassword(
                                    newPasswordConfirm.toString().trim(),
                                    mNewPasswordEdt.getText().toString().trim());

                            setEnabledSendButton(mIsValidCurrentPassword && mIsValidNewPassword && mIsValidNewPasswordConfirm);
                        }));
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void showLoadingDialog() {
        showProgress();
    }

    @Override
    public void dismissLoadingDialog() {
        hideProgress();
    }

    @Override
    public void openCompleteDialog() {
        mCompleteDialog = new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .message(getString(R.string.change_password_complete_message))
                .leftButtonText(getString(android.R.string.ok))
                .onResultListener(new TextDialogFragment.OnResultListener() {
                    @Override
                    public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                        completeChangePassword();
                    }

                    @Override
                    public void onClickRightButton(TextDialogFragment textDialogFragment) {
                        // Do nothing
                    }
                })
                .build();
        mCompleteDialog.show(getSupportFragmentManager());
    }

    @Override
    public void completeChangePassword() {
        finish();
    }

    private void dismissCompleteDialog() {
        if (mCompleteDialog == null) {
            return;
        }
        mCompleteDialog.dismiss();
    }

    private void setEnabledSendButton(boolean isEnabled) {
        mSendBtn.setEnabled(isEnabled);
        mSendBtn.setTextColor(isEnabled
                ? getResources().getColor(android.R.color.white)
                : getResources().getColor(R.color.transparent_20_black));
    }
}
