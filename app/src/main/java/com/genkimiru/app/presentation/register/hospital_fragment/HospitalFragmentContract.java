package com.genkimiru.app.presentation.register.hospital_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface HospitalFragmentContract {

    interface HospitalPresenter extends BasePresenter {

    }

    interface HospitalFragmentView {

    }

    interface HospitalFragmentRouter {

    }
}
