package com.genkimiru.app.presentation.update_user_infor.hospital;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateHospitalActivityContract {
    interface UpdateHospitalPresenter extends BasePresenter {
        void updateHospital(int surgeryStatus);
    }

    interface UpdateHospitalView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateHospitalRouter {

    }
}
