package com.genkimiru.app.presentation.update_user_infor.blood_type;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateBloodTypeActivityContract {

    interface UpdateBloodTypePresenter extends BasePresenter {
            void updateBloodType(String bloodType);
    }

    interface UpdateBloodTypeView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateBloodTypeRouter {

    }
}
