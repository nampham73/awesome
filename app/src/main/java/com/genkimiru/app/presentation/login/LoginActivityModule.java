package com.genkimiru.app.presentation.login;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.google.GoogleApi;
import com.genkimiru.app.data.repository.googlefit.GoogleFitRepository;
import com.genkimiru.app.data.repository.googlefit.GoogleFitRepositoryImp;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.google.GoogleUseCase;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.login.LoginActivityContract.*;

@Module
public class LoginActivityModule {

    @Provides
    @ActivityScope
    LoginView provideView(LoginActivity loginActivity) {
        return new LoginActivityView(loginActivity);
    }

    @Provides
    @ActivityScope
    LoginRouter provideRouter(LoginActivity loginActivity) {
        return loginActivity;
    }

    @Provides
    @ActivityScope
    LoginPresenter providePresenter(Context context, LoginView loginView,
                                    LoginRouter loginRouter,
                                    UserUseCase userUseCase,
                                    GenkiUserInfoModel genkiUserInfoModel,
                                    GoogleUseCase googleUseCase,
                                    RegisterUserRequest registerUserRequest) {
        return new LoginActivityPresenter(context, loginView, loginRouter, userUseCase, genkiUserInfoModel, googleUseCase, registerUserRequest);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    GoogleFitRepository provideRepository(GoogleApi api) {
        return new GoogleFitRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository){
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }

    @Provides
    @ActivityScope
    GoogleUseCase provideUseCase(SchedulerProvider schedulerProvider, GoogleFitRepository googleFitRepository) {
        return new GoogleUseCase(schedulerProvider.io(), schedulerProvider.ui(), googleFitRepository);
    }

    @Provides
    @ActivityScope
    RegisterUserRequest provideRegisterUserRequest() {
        return new RegisterUserRequest();
    }
}
