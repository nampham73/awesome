package com.genkimiru.app.presentation.main.contest_fragment.current;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.ListPresenterImp;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.data.remote.genki.request.ListContestRequest;
import com.genkimiru.app.data.remote.genki.response.ContestModelResponse;
import com.genkimiru.app.domain.contest.ContestParam;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.CurrentContestPresenter;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.CurrentContestRouter;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.CurrentContestView;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class CurrentContestFragmentPresenter extends ListPresenterImp<CurrentContestView, CurrentContestRouter>
        implements CurrentContestPresenter {
    private ContestUseCase mContestUseCase;
    private String mSortName = "start_date";
    private String mSortType = "asc";

    CurrentContestFragmentPresenter(Context context,
                                    CurrentContestView currentContestView,
                                    CurrentContestRouter currentContestRouter,
                                    ContestUseCase contestUseCase) {
        super(context, currentContestView, currentContestRouter);
        this.mContestUseCase = contestUseCase;
    }

    @Override
    public void start() {
        super.start();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        setLoadingRequesting(true);
        ListContestRequest request = new ListContestRequest.Builder(getNextPage(), getLimit(), mSortName, mSortType, token).create();
        mContestUseCase.execute(new ListContestSubscriber(), ContestParam.Factory.getContestCurrentUser(request));
    }

    @Override
    public void requestSort(String sortName, String sortType) {
        this.mSortName = sortName;
        this.mSortType = sortType;
        setNextPage(0);
        start();
    }

    private class ListContestSubscriber extends DefaultSubscriber<List<ContestModelResponse>> {
        @Override
        public void onNext(List<ContestModelResponse> response) {
            mView.onDataLoadDone(convert(response));
            setLoadingRequesting(false);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            setLoadingRequesting(false);
            Timber.e("fetching contest page main failed: %s", t.getMessage());
            mView.onLoadingDataFailure(t, R.string.response_contest_Page_loading_data_failure);
        }
    }

    private List<GenkiContestModel> convert(List<ContestModelResponse> responses) {
        List<GenkiContestModel> items = new ArrayList<>();
        if (responses != null && responses.size() > 0) {
            for (ContestModelResponse response : responses) {
                GenkiContestModel model = new GenkiContestModel();
                model.setValueFromResponse(response);
                items.add(model);
            }
        }
        reCalculatedPaging(items.size());
        return items;
    }
}
