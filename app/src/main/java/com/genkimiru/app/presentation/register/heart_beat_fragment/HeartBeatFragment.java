package com.genkimiru.app.presentation.register.heart_beat_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RadioButton;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatFragmentRouter;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatFragmentView;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatPresenter;
import javax.inject.Inject;
import butterknife.OnClick;
import timber.log.Timber;

public class HeartBeatFragment extends SimpleBaseFragment<HeartBeatPresenter>
        implements HeartBeatFragmentView, HeartBeatFragmentRouter {
    private RegisterActivity mRegisterActivity;

    @Inject
    HeartBeatPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_healthy_heartbeat;
    }

    @Override
    protected HeartBeatPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialDefaultValue();
    }

    @OnClick({R.id.checkbox_heart_very_good, R.id.checkbox_heart_good, R.id.checkbox_heart_bad, R.id.checkbox_heart_not_bad})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.checkbox_heart_very_good:
                if (checked) {
                    mRegisterUserRequest.feel_condition = 1;
                    Timber.d("Feel condition: 1");
                }
                break;
            case R.id.checkbox_heart_good:
                if (checked) {
                    mRegisterUserRequest.feel_condition = 2;
                    Timber.d("Feel condition: 2");
                }
                break;
            case R.id.checkbox_heart_not_bad:
                if (checked) {
                    mRegisterUserRequest.feel_condition = 3;
                    Timber.d("Feel condition: 3");
                }
                break;
            case R.id.checkbox_heart_bad:
                if (checked) {
                    mRegisterUserRequest.feel_condition = 4;
                    Timber.d("Feel condition: 4");
                }
                break;
        }
    }

    private void initialDefaultValue() {
        mRegisterUserRequest.feel_condition = 1;
    }
}
