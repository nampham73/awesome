package com.genkimiru.app.presentation.register.account_complete_fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompletePresenter;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompleteRouter;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompleteView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Single;

public class AccountCompleteFragment extends SimpleBaseFragment<AccountCompletePresenter>
        implements AccountCompleteView, AccountCompleteRouter {

    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_account_complete_layout)
    RelativeLayout mAccountCompleteLayout;
    @BindView(R.id.register_complete_layout)
    LinearLayout mCompleteLayout;
    @BindView(R.id.register_account_complete_progress)
    ProgressBar mProgressBar;

    @Inject
    AccountCompletePresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_healthy_account_complete;
    }

    @Override
    protected AccountCompletePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.register_account_complete_btn)
    void userRegister() {
        mPresenter.register(mRegisterUserRequest);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showCompleteMessage() {
        mAccountCompleteLayout.setVisibility(View.GONE);
        mCompleteLayout.setVisibility(View.VISIBLE);
        mPresenter.prepareUserInfo();
    }

    @Override
    public void showMessageDialog(String message) {
        mRegisterActivity.showMessageDialog(message);
    }

    @Override
    public void onRegisterFail() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void openDashboardScreen() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        mRegisterActivity.finish();
    }
}
