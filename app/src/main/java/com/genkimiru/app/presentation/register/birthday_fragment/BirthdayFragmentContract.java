package com.genkimiru.app.presentation.register.birthday_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface BirthdayFragmentContract {

    interface BirthdayPresenter extends BasePresenter {

    }

    interface BirthdayFragmentView {

    }

    interface BirthdayFragmentRouter {

    }
}
