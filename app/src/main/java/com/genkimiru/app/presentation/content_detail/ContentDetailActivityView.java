package com.genkimiru.app.presentation.content_detail;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.view.BaseActivityView;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiFeedModel;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.ContentDetailPresenter;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.ContentDetailView;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedFragment;
import com.genkimiru.app.presentation.main.GlideApp;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContentDetailActivityView
        extends BaseActivityView<ContentDetailActivity, ContentDetailPresenter>
        implements ContentDetailView {

    private DetailContentFeedFragment mContentFeedFragment;
    private ContentDetailActivity mActivity;

    @BindView(R.id.iv_detail_user_current_user_tv)
    CircleImageView mIvAvatar;
    @BindView(R.id.content_detail_chat_edt)
    EditText mEditChatView;

    private GenkiUserInfoModel mGenkiUserInfoModel;

    public ContentDetailActivityView(ContentDetailActivity activity, GenkiUserInfoModel userInfoModel) {
        super(activity);
        this.mGenkiUserInfoModel = userInfoModel;
        mContentFeedFragment = DetailContentFeedFragment.newInstance(activity.getIntent().getExtras());
        mActivity = activity;
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        setUpContentFeedFragment(mContentFeedFragment);

        GlideApp.with(view)
                .load(mGenkiUserInfoModel.getAvatar())
                .error(R.color.light_grey)
                .into(mIvAvatar);
        mEditChatView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                CharSequence message = v.getText();
                if (message != null) {
                    addComment(message.toString());
                    v.post(() -> mActivity.hideKeyboard(mEditChatView));
                }
                return true;
            }
            return false;
        });
    }


    private void setUpContentFeedFragment(Fragment fragmentItem) {
        FragmentTransaction fragmentTransaction = mActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_feed_container_id, fragmentItem);
        fragmentTransaction.commit();
    }

    private void addComment(String message) {
        mEditChatView.setText("");
        if (message==null ||message.length()==0){
         return;
        }
        if (mContentFeedFragment != null && mContentFeedFragment.isAdded()) {
            GenkiFeedModel model = new GenkiFeedModel();
            model.setComment(message);
            model.setOwnerId(mGenkiUserInfoModel.getId());
            model.setOwnerName(mGenkiUserInfoModel.getRealName());
            model.setAvatar(mGenkiUserInfoModel.getAvatar());
            mContentFeedFragment.addComment(model);
        }
    }
}
