package com.genkimiru.app.presentation.register.account_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountFragmentRouter;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountFragmentView;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountPresenter;

import static com.genkimiru.app.Constants.Input.PASSWORD_MIN_LENGTH;
import static com.genkimiru.app.Constants.Input.USERNAME_MIN_LENGTH;

public class AccountFragmentPresenter extends BasePresenterImp<AccountFragmentView,
        AccountFragmentRouter> implements AccountPresenter {

    private UserUseCase mUserUseCase;

    public AccountFragmentPresenter(Context context, AccountFragmentView accountFragmentView,
                                    AccountFragmentRouter accountFragmentRouter, UserUseCase userUseCase) {
        super(context, accountFragmentView, accountFragmentRouter);
        userUseCase = userUseCase;
    }

    @Override
    public boolean isValidUsername(String username) {
        return StringUtil.isNotEmpty(username);
    }

    @Override
    public boolean isValidUsernameLength(String username) {
        return username.length() >= USERNAME_MIN_LENGTH;
    }

    @Override
    public boolean isValidPassword(String password) {
        return StringUtil.isNotEmpty(password);
    }

    @Override
    public boolean isValidPasswordLength(String password) {
        return password.length() >= PASSWORD_MIN_LENGTH;
    }

    @Override
    public boolean isValidEmail(String email) {
        return StringUtil.isNotEmpty(email);
    }

    @Override
    public boolean isValidEmailPattern(String email) {
        return StringUtil.validateEmailFormat(email);
    }
}
