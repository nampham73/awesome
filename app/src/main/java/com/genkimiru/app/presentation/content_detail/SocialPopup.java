package com.genkimiru.app.presentation.content_detail;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.genkimiru.app.R;

import java.util.ArrayList;
import java.util.List;

import static android.widget.ListPopupWindow.WRAP_CONTENT;

public class SocialPopup extends PopupWindow {
    private ListView.OnItemClickListener onItemClickListener;
    private List<Result> results;

    public SocialPopup(@NonNull Context context, ListView.OnItemClickListener listener) {
        super(context);
        init(context, listener);
    }

    private void init(Context context, ListView.OnItemClickListener listener) {
        setWidth(500);
        setHeight(WRAP_CONTENT);
        setBackgroundDrawable(ContextCompat.getDrawable(context,R.drawable.bg_popup_social));
        setFocusable(true);
        setOutsideTouchable(true);
        onItemClickListener = listener;
        setData(context);
    }

    private void setData(Context context) {
        results = new ArrayList<>();
        results.add(new Result(1, R.drawable.ic_facebook_share, "Facebook", R.color.facebook_text));
        results.add(new Result(2, R.drawable.ic_twitter_share, "Twitter", R.color.twitter_text));
        Adapter adapter = new Adapter(context, results);
        ListView listView = new ListView(context);
        listView.setOnItemClickListener(onItemClickListener);
        listView.setAdapter(adapter);
        setContentView(listView);
    }

    public Result findItem(int position) {
        if (results != null && position < results.size()) {
            return results.get(position);
        }
        return null;
    }

    public static class Result {
        public int id;
        public int resourceId;
        public String name;
        public int textColor;


        public Result(int id, int resourceId, String name, int textColor) {
            this.id = id;
            this.resourceId = resourceId;
            this.name = name;
            this.textColor = textColor;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private static class Adapter extends ArrayAdapter<Result> {

        Adapter(@NonNull Context context, List<Result> results) {
            super(context, 0, results);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View listItem = convertView;
            if (listItem == null) listItem = LayoutInflater
                    .from(getContext())
                    .inflate(R.layout.layout_share_view, parent, false);
            Result currentMovie = getItem(position);
            if (currentMovie != null) {
                ImageView image = listItem.findViewById(R.id.iv_social_share);
                image.setImageResource(currentMovie.resourceId);
                TextView name = listItem.findViewById(R.id.tv_name);
                name.setText(currentMovie.name);
                name.setTextColor(currentMovie.textColor);
            }
            return listItem;
        }
    }


}
