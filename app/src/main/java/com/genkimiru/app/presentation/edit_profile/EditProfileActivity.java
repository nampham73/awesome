package com.genkimiru.app.presentation.edit_profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.dialog.SelectAvatarDialog;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.dialog.UpdateUserInfoDialog;
import com.genkimiru.app.dialog.UpdateUserLipitDialog;
import com.genkimiru.app.dialog.UpdateUserLiverDialog;
import com.genkimiru.app.dialog.UpdateUserSugarDialog;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityContract.EditProfileRouter;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityContract.EditProfileView;
import com.genkimiru.app.presentation.main.GlideApp;
import com.genkimiru.app.presentation.photo.PhotoActivity;
import com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivity;
import com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthDayActivity;
import com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivity;
import com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivity;
import com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivity;
import com.genkimiru.app.presentation.update_user_infor.heart_beat.UpdateHeartBeatActivity;
import com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivity;
import com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivity;
import com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivity;
import com.genkimiru.app.presentation.update_user_infor.weight.UpdateWeightActivity;

import java.io.File;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

import static com.genkimiru.app.Constants.HealthSource.FITBIT;
import static com.genkimiru.app.Constants.HealthSource.GOBE;
import static com.genkimiru.app.Constants.HealthSource.GOOGLE_FIT;
import static com.genkimiru.app.Constants.HealthSource.SMART_PHONE;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GET_PHOTO;
import static com.genkimiru.app.base.util.DateUtil.Format.API_YEAR_MONTH_DAY_FORMAT;
import static com.genkimiru.app.base.util.DateUtil.Format.WEEK_MONTH_DAY_YEAR_FORMAT;
import static com.genkimiru.app.base.util.DateUtil.Format.WEEK_MONTH_DAY_YEAR_FORMAT_JAP;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.DrinkEachTime;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.DrinkLevel;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.BAD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.GOOD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.VERY_BAD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.VERY_GOOD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.Gender.FEMALE;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.Gender.MALE;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.SmokingEachTime;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.SmokingLevel;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.SurgeryStatus.NO;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.SurgeryStatus.YES;

public class EditProfileActivity extends SimpleBaseActivity<EditProfileActivityPresenter>
        implements EditProfileRouter, EditProfileView {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.activity_edit_blood_pressure)
    TextView mBloodPressure;
    @BindView(R.id.activity_edit_blood_type)
    TextView mBloodType;
    @BindView(R.id.activity_edit_gender)
    TextView mGender;
    @BindView(R.id.activity_edit_birthday)
    TextView mBirthDay;
    @BindView(R.id.activity_edit_heart_beat)
    TextView mHeartBeat;
    @BindView(R.id.activity_edit_height)
    TextView mHeight;
    @BindView(R.id.activity_edit_weight)
    TextView mWeight;
    @BindView(R.id.activity_edit_hospital)
    TextView mSurgeryStatus;
    @BindView(R.id.activity_edit_alcohol)
    TextView mDrinkLevel;
    @BindView(R.id.activity_edit_drinking_each_tv)
    TextView mDrinkEachTime;
    @BindView(R.id.activity_edit_smoking)
    TextView mSmokingLevel;
    @BindView(R.id.activity_edit_smoking_each_tv)
    TextView mSmokingEachTime;
    @BindView(R.id.setting_user_name_tv)
    TextView mUserName;
    @BindView(R.id.setting_user_full_name_tv)
    TextView mUserFullName;
    @BindView(R.id.setting_user_email_tv)
    TextView mUserEmail;
    @BindView(R.id.activity_update_user_infor)
    Button mUpdateUserInfor;
    @BindView(R.id.activity_edit_select_device)
    TextView mUpdateSelectDevice;
    @BindView(R.id.edit_profile_avatar)
    CircleImageView mAvatarImageView;
    @BindView(R.id.activity_edit_address_tv)
    TextView mAddress;

    private UpdateUserInfoDialog mUpdateUserInfoDialog;
    private UpdateUserLipitDialog mUpdateUserLipitDialog;
    private UpdateUserLiverDialog mUpdateUserLiverDialog;
    private UpdateUserSugarDialog mUpdateUserSugarDialog;
    private SelectAvatarDialog selectAvatarDialog;
    private Uri mAvatarUri;

    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;
    @Inject
    EditProfileActivityPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_edit_profile;
    }

    @Override
    protected EditProfileActivityPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.edit_profile_title);
    }

    @Override
    public void openUpdateAlcohol() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateAlcoholActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateBirthday() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateBirthDayActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateBloodPressure() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateBloodPressureActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateBloodType() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateBloodTypeActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateHeartBeat() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateHeartBeatActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateHeight() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateHeightActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateHospital() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateHospitalActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateSmoking() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateSmokingActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateWeight() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateWeightActivity.class);
        startActivity(intent);
    }

    @Override
    public void openUpdateGender() {
        Intent intent = new Intent(EditProfileActivity.this, UpdateGenderActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.activity_linear_update_user_info)
    void updateUserInfo() {
        openDialogUpdateUserInfo();
    }

    @OnClick(R.id.activity_edit_lipit)
    void updateInfoLipit() {
        openDialogUpdateLipitInfo();
    }

    @OnClick(R.id.activity_edit_liver)
    void updateInfoLiver() {
        openDialogUpdateLiverInfo();
    }

    @OnClick(R.id.activity_edit_surgar)
    void updateInfoSugar() {
        openDialogUpdateSugar();
    }

    @OnClick(R.id.activity_edit_urinary_protein)
    void updateUrinaryProtein() {
        openDialogUpdateUrinaryProtein();
    }

    @OnClick(R.id.activity_edit_alcohol)
    void updateAlcohol() {
        openUpdateAlcohol();
    }

    @OnClick(R.id.activity_edit_birthday)
    void updateBirthday() {
        openUpdateBirthday();
    }

    @OnClick(R.id.activity_edit_blood_pressure)
    void updateBloodPressure() {
        openUpdateBloodPressure();
    }

    @OnClick(R.id.activity_edit_blood_type)
    void updateBloodType() {
        openUpdateBloodType();
    }

    @OnClick(R.id.activity_edit_heart_beat)
    void updateHeartBeat() {
        openUpdateHeartBeat();
    }

    @OnClick(R.id.activity_edit_height)
    void updateHeight() {
        openUpdateHeight();
    }

    @OnClick(R.id.activity_edit_hospital)
    void updateHospital() {
        openUpdateHospital();
    }

    @OnClick(R.id.activity_edit_smoking)
    void updateSmoking() {
        openUpdateSmoking();
    }

    @OnClick(R.id.activity_edit_weight)
    void updateWeight() {
        openUpdateWeight();
    }

    @OnClick(R.id.activity_edit_gender)
    void updateGender() {
        openUpdateGender();
    }

    @OnClick(R.id.edit_profile_avatar)
    void updateAvatar() {
        openUpdateAvatar();
    }


    public void openDialogUpdateUserInfo() {
        Timber.d("Update User Info");
        mUpdateUserInfoDialog = new UpdateUserInfoDialog.UpdateUserDialogBuilder()
                .dialogListener(new UpdateUserInfoDialog.OnResultListener() {
                    @Override
                    public void onUpdateUserInfo(String username, String firstName, String lastName, String email, UpdateUserInfoDialog userInfoDialog) {
                        Timber.d("Update User Info-  Username: %s - Firstname: %s - Lastname : %s - Email : %s", username, firstName, lastName, email);
                        mPresenter.updateUserInfo(username, firstName, lastName, email);
                        showProgress();
                        userInfoDialog.dismiss();
                    }

                    @Override
                    public void onCancel(UpdateUserInfoDialog userInfoDialog) {
                        mUpdateUserInfoDialog.dismiss();
                    }
                }).build();
        mUpdateUserInfoDialog.setInitializeData(mGenkiUserInfoModel.getNickName(),
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName());
        mUpdateUserInfoDialog.show(getSupportFragmentManager());
    }

    public void openDialogUpdateLipitInfo() {
        Timber.d("Update User Lipit Info");
        mUpdateUserLipitDialog = new UpdateUserLipitDialog.UpdateUserLipitBuilder()
                .dialogListener(new UpdateUserLipitDialog.OnResultListener() {
                    @Override
                    public void onUpdateUserLipit(String lipitNeutralFat, String lipitCholesterolHd, String lipitCholesterolLd, UpdateUserLipitDialog updateUserLipitDialog) {
                        mUpdateUserLipitDialog.dismiss();
                    }

                    @Override
                    public void onCancel(UpdateUserLipitDialog userInfoDialog) {
                        mUpdateUserLipitDialog.dismiss();
                    }
                }).build();
        mUpdateUserLipitDialog.show(getSupportFragmentManager());
    }

    @Override
    public void openDialogUpdateLiverInfo() {
        Timber.d("Update User Liver Info");
        mUpdateUserLiverDialog = new UpdateUserLiverDialog.UpdateUserLipitBuilder()
                .dialogListener(new UpdateUserLiverDialog.OnResultListener() {
                    @Override
                    public void onUpdateUserLiver(String LiverGotEdt, String LiverGptEdt, String LiverVgtEdt, UpdateUserLiverDialog updateUserLipitDialog) {
                        mUpdateUserLiverDialog.dismiss();
                    }

                    @Override
                    public void onCancel(UpdateUserLiverDialog updateUserLiverDialog) {
                        mUpdateUserLiverDialog.dismiss();
                    }

                    @Override
                    public void onDelete(UpdateUserLiverDialog updateUserLiverDialog) {
                        mUpdateUserLiverDialog.dismiss();
                    }
                }).build();
        mUpdateUserLiverDialog.show(getSupportFragmentManager());
    }

    @Override
    public void openDialogUpdateSugar() {
        Timber.d("Update User Liver Info");
        mUpdateUserSugarDialog = new UpdateUserSugarDialog.UpdateUserSugarDialogBuilder()
                .setTitle(getString(R.string.update_user_sugar), getString(R.string.update_user_sugar_measure))
                .dialogListener(new UpdateUserSugarDialog.OnResultListener() {
                    @Override
                    public void onUpdateUserSugar(String sugar, UpdateUserSugarDialog updateUserSugarDialog) {
                        mUpdateUserSugarDialog.dismiss();
                    }

                    @Override
                    public void onCancel(UpdateUserSugarDialog updateUserSugarDialog) {
                        mUpdateUserSugarDialog.dismiss();
                    }

                    @Override
                    public void onDelete(UpdateUserSugarDialog updateUserSugarDialog) {
                        mUpdateUserSugarDialog.dismiss();
                    }
                }).build();
        mUpdateUserSugarDialog.show(getSupportFragmentManager());
    }

    @Override
    public void openDialogUpdateUrinaryProtein() {
        Timber.d("Update User Liver Info");
        mUpdateUserSugarDialog = new UpdateUserSugarDialog.UpdateUserSugarDialogBuilder()
                .setTitle(getString(R.string.update_user_urinary_protein), getString(R.string.update_user_lipit_measure))
                .dialogListener(new UpdateUserSugarDialog.OnResultListener() {
                    @Override
                    public void onUpdateUserSugar(String sugar, UpdateUserSugarDialog updateUserSugarDialog) {
                        mUpdateUserSugarDialog.dismiss();
                    }

                    @Override
                    public void onCancel(UpdateUserSugarDialog updateUserSugarDialog) {
                        mUpdateUserSugarDialog.dismiss();
                    }

                    @Override
                    public void onDelete(UpdateUserSugarDialog updateUserSugarDialog) {
                        mUpdateUserSugarDialog.dismiss();
                    }
                }).build();
        mUpdateUserSugarDialog.show(getSupportFragmentManager());
    }

    @Override
    public void openUpdateAvatar() {
        selectAvatarDialog = new SelectAvatarDialog();
        selectAvatarDialog.setDialogSelectListener(new SelectAvatarDialog.DialogSelectListener() {
            @Override
            public void onCameraClick(SelectAvatarDialog selectAvatarDialog) {
                startActivityForResult(PhotoActivity.createIntent(
                        getApplicationContext(),
                        PhotoActivity.Type.Capture),
                        REQUEST_CODE_GET_PHOTO);
                selectAvatarDialog.dismiss();
            }

            @Override
            public void onGalleryClick(SelectAvatarDialog selectAvatarDialog) {
                startActivityForResult(PhotoActivity.createIntent(
                        getApplicationContext(),
                        PhotoActivity.Type.Gallery),
                        REQUEST_CODE_GET_PHOTO);
                selectAvatarDialog.dismiss();
            }
        });
        selectAvatarDialog.show(getSupportFragmentManager());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("requestCode = " + requestCode + ", resultCode = " + resultCode);

        switch (requestCode) {
            case REQUEST_CODE_GET_PHOTO:
                if (data == null) {
                    return;
                }
                mAvatarUri = data.getData();
                Timber.w("croppedPhoto Uri = " + mAvatarUri);
                mPresenter.updateAvatar(new File(mAvatarUri.getPath()));
                break;

            default:
                break;
        }
    }

    private void initializeData() {
        Locale locale = getResources().getConfiguration().locale;
        String bloodPressure = String.valueOf(mGenkiUserInfoModel.getBloodPressureUpper() + "/" + mGenkiUserInfoModel.getBloodPressureUnder());
        mBloodPressure.setText(String.valueOf(bloodPressure));
        mBloodType.setText(String.valueOf(mGenkiUserInfoModel.getBloodType()));
        mBirthDay.setText(DateUtil.convertDateFormat(mGenkiUserInfoModel.getBirthday(), API_YEAR_MONTH_DAY_FORMAT,
                locale.getLanguage().equals("ja") ? WEEK_MONTH_DAY_YEAR_FORMAT_JAP : WEEK_MONTH_DAY_YEAR_FORMAT));
        String height = String.valueOf(mGenkiUserInfoModel.getHeight()).replaceAll("\\.?0*$", "") + " cm";
        mHeight.setText(height);
        String weight = String.valueOf(mGenkiUserInfoModel.getWeight()).replaceAll("\\.?0*$", "") + " kg";
        mWeight.setText(weight);
        if (StringUtil.isNotEmpty(mGenkiUserInfoModel.getNickName())) {
            mUserName.setText(mGenkiUserInfoModel.getNickName());
            mUserFullName.setVisibility(View.VISIBLE);
        } else {
            mUserName.setText(mGenkiUserInfoModel.getRealName());
            mUserFullName.setVisibility(View.GONE);
        }
        String realName = "(" + mGenkiUserInfoModel.getRealName() + ")";
        mUserFullName.setText(realName);
        mUserEmail.setText(mGenkiUserInfoModel.getEmail());
        mAddress.setText(StringUtil.isNotEmpty(mGenkiUserInfoModel.getAddress())
                ? mGenkiUserInfoModel.getAddress()
                : getString(R.string.edit_profile_location));
        setGenderUserInfo();
        setSmokingLevel();
        setAlcoholLevel();
        setSurgeryStatus();
        setFeelCondition();
        setUpdateUserInfo();
        GlideApp.with(this).load(mGenkiUserInfoModel.getAvatar())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(mAvatarImageView);
        setAvatarImage(mAvatarUri);
    }

    private void setUpdateUserInfo() {
        Timber.d("gethealsource :%s", mGenkiUserInfoModel.getHealthSource());
        switch (mGenkiUserInfoModel.getHealthSource()) {
            case GOBE:
                mUpdateSelectDevice.setText(R.string.update_user_gobe);
                mUpdateSelectDevice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_gobe_blue, 0, 0, 0);
                break;
            case FITBIT:
                mUpdateSelectDevice.setText(R.string.update_user_fitbit);
                mUpdateSelectDevice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_fitbit_blue, 0, 0, 0);
                break;
            case GOOGLE_FIT:
                mUpdateSelectDevice.setText(R.string.update_user_other_wearable);
                mUpdateSelectDevice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_other_devices_blue, 0, 0, 0);
                break;
            case SMART_PHONE:
                mUpdateSelectDevice.setText(R.string.update_smart_phone);
                mUpdateSelectDevice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_phone_blue, 0, 0, 0);
                break;
            default:
                break;
        }
    }

    private void setGenderUserInfo() {
        switch (mGenkiUserInfoModel.getGender()) {
            case MALE:
                mGender.setText(R.string.register_sex_male);
                mGender.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_profile_male, 0, 0, 0);
                break;
            case FEMALE:
                mGender.setText(R.string.register_sex_female);
                mGender.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_profile_female, 0, 0, 0);
                break;
            default:
                mGender.setText(R.string.register_sex_male);
                mGender.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_profile_male, 0, 0, 0);
                break;
        }

    }

    public void setSmokingLevel() {
        int smokingLevel = mGenkiUserInfoModel.getSmokingLevel();
        switch (SmokingLevel.getFromValue(smokingLevel)) {
            case LEVEL_NO_SMOKING:
                mSmokingLevel.setText(R.string.register_health_smoking_no);
                break;
            case LEVEL_SOMETIME_SMOKING:
                mSmokingLevel.setText(R.string.register_health_smoking_sometime);
                break;
            case LEVEL_ALLWAYS_SMOKING:
                mSmokingLevel.setText(R.string.register_health_smoking_every_day);
                break;
            case NONE:
                break;
        }
        setSmokeEachTime();
    }

    public void setSmokeEachTime() {
        int smokingEachTime = mGenkiUserInfoModel.getSmokingEachTime();
        switch (SmokingEachTime.getFromValue(smokingEachTime)) {
            case LEVEL_SMALL:
                mSmokingEachTime.setText(R.string.register_smoking_small);
                mSmokingEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_MEDIUM:
                mSmokingEachTime.setText(R.string.register_smoking_medium);
                mSmokingEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_LARGE:
                mSmokingEachTime.setText(R.string.register_smoking_large);
                mSmokingEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_DEFAULT:
                mSmokingEachTime.setVisibility(View.GONE);
                break;
            case NONE:
                mSmokingEachTime.setVisibility(View.GONE);
                break;
        }
    }

    private void setAlcoholLevel() {
        int alcoholLevel = mGenkiUserInfoModel.getAlcoholLevel();
        switch (DrinkLevel.getFromValue(alcoholLevel)) {
            case LEVEL_1:
                mDrinkLevel.setText(R.string.register_alcohol_everyday);
                break;
            case LEVEL_2:
                mDrinkLevel.setText(R.string.register_alcohol_5_6);
                break;
            case LEVEL_3:
                mDrinkLevel.setText(R.string.register_alcohol_3_4);
                break;
            case LEVEL_4:
                mDrinkLevel.setText(R.string.register_alcohol_1_2);
                break;
            case LEVEL_5:
                mDrinkLevel.setText(R.string.register_alcohol_1_3);
                break;
            case LEVEL_6:
                mDrinkLevel.setText(R.string.register_alcohol_stop_drink);
                mDrinkEachTime.setVisibility(View.GONE);
                break;
            case LEVEL_7:
                mDrinkLevel.setText(R.string.register_alcohol_not_drink);
                mDrinkEachTime.setVisibility(View.GONE);
                break;
            case NONE:
                break;

        }
        setAlcoholEachTime();
    }

    private void setAlcoholEachTime() {
        int alcoholEachTime = mGenkiUserInfoModel.getDrinkEachTime();
        switch (DrinkEachTime.getFromValue(alcoholEachTime)) {
            case LEVEL_1_180:
                mDrinkEachTime.setText(R.string.register_alcohol_180ml);
                mDrinkEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_2_360:
                mDrinkEachTime.setText(R.string.register_alcohol_360ml);
                mDrinkEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_3_540:
                mDrinkEachTime.setText(R.string.register_alcohol_540ml);
                mDrinkEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_4_720:
                mDrinkEachTime.setText(R.string.register_alcohol_720ml);
                mDrinkEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_5_900:
                mDrinkEachTime.setText(R.string.register_alcohol_900ml);
                mDrinkEachTime.setVisibility(View.VISIBLE);
                break;
            case LEVEL_6_MORE_900:
                mDrinkEachTime.setText(R.string.register_alcohol_900ml_more);
                mDrinkEachTime.setVisibility(View.VISIBLE);
                break;
            case NONE:
                break;

        }
    }

    private void setSurgeryStatus() {
        switch (mGenkiUserInfoModel.getSurgeryStatus()) {
            case NO:
                mSurgeryStatus.setText(R.string.update_user_info_surgery_status_no);
                break;
            case YES:
                mSurgeryStatus.setText(R.string.update_user_info_surgery_status_yes);
                break;
        }
    }

    private void setFeelCondition() {
        switch (mGenkiUserInfoModel.getFeelCondition()) {
            case VERY_GOOD:
                mHeartBeat.setText(R.string.update_user_health_very_good);
                break;
            case GOOD:
                mHeartBeat.setText(R.string.update_user_health_good);
                break;
            case BAD:
                mHeartBeat.setText(R.string.update_user_health_not_bad);
                break;
            case VERY_BAD:
                mHeartBeat.setText(R.string.update_user_health_bad);
                break;

        }
    }

    @Override
    public void onUpdateUserInfoSuccess() {
        mUpdateUserInfoDialog.dismiss();
        hideProgress();
        initializeData();
    }

    @Override
    public void onUpdateUserInfoError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void showProgressBar() {
        showProgress();
    }

    @Override
    public void hideProgressBar() {
        hideProgress();
    }

    private void setAvatarImage(Uri avatarUri) {
        Timber.d("setAvatarImage avatarUri = %s", avatarUri);
        if (avatarUri == null) {
            return;
        }
        mAvatarImageView.setImageDrawable(null);
        mAvatarImageView.setImageURI(avatarUri);
    }
}
