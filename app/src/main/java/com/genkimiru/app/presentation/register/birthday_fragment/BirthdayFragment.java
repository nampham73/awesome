package com.genkimiru.app.presentation.register.birthday_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.dialog.DatePickerDialog;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayFragmentRouter;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayFragmentView;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayPresenter;

import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Api.MIN_USER_AGE;
import static com.genkimiru.app.base.util.DateUtil.Format.API_YEAR_MONTH_DAY_FORMAT;
import static com.genkimiru.app.base.util.DateUtil.Format.YEAR_MONTH_DAY_FORMAT;

public class BirthdayFragment extends SimpleBaseFragment<BirthdayPresenter> implements BirthdayFragmentRouter, BirthdayFragmentView {

    @BindView(R.id.register_birthday_tv)
    EditText mDateTxt;
    DatePickerDialog mDatePickerDialog;

    @Inject
    BirthdayPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    private RegisterActivity mRegisterActivity;
    private Locale mCurrentLocale;

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_birthday;
    }

    @Override
    protected BirthdayPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Timber.d("RegisterUserRequest %s", mRegisterUserRequest.hashCode());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentLocale = getResources().getConfiguration().locale;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            return;
        }
        initDefaultValue();
    }

    @OnClick(R.id.register_birthday_tv)
    void showDatePicker() {
        showDateDialog();
    }

    public void showDateDialog() {
        Timber.d("showDateDialog");
        mDatePickerDialog
                = new DatePickerDialog((year, month, day) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            DateUtil.getYearMonthDayForApi(calendar);
            mDateTxt.setText(DateUtil.getYearMonthDayString(calendar, mCurrentLocale));
            mDatePickerDialog.dismiss();
            if (mDateTxt.getText().toString().trim().length() <= 0) {
                return;
            }
            mRegisterUserRequest.birthday = DateUtil.getYearMonthDayForApi(calendar);
        });
        mDatePickerDialog.show(mRegisterActivity.getSupportFragmentManager());
    }

    private void initDefaultValue() {
        if (StringUtil.isNotEmpty(mRegisterUserRequest.birthday)) {
            Calendar calendar = DateUtil.getCalendar(mRegisterUserRequest.birthday, API_YEAR_MONTH_DAY_FORMAT);
            mDateTxt.setText(DateUtil.getYearMonthDayString(calendar, mCurrentLocale));
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - MIN_USER_AGE);
            mRegisterUserRequest.birthday = DateUtil.getYearMonthDayForApi(calendar);
            mDateTxt.setText(DateUtil.getYearMonthDayString(calendar, mCurrentLocale));
        }
    }
}
