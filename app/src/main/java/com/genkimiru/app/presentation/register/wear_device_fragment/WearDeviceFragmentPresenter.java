package com.genkimiru.app.presentation.register.wear_device_fragment;

import android.content.Context;
import android.widget.Toast;

import com.genkimiru.app.Constants;
import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.gobe.GobeUseCase;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDeviceFragmentRouter;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDeviceFragmentView;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDevicePresenter;
import com.genkimiru.app.service.stepcounter.StepCounterService;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class WearDeviceFragmentPresenter extends BasePresenterImp<WearDeviceFragmentView,
        WearDeviceFragmentRouter> implements WearDevicePresenter {

    private UserUseCase mUserUseCase;
    private GobeUseCase mGobeUseCase;
    private RegisterUserRequest mRegisterUserRequest;
    private GenkiUserInfoModel mGenkiUserInfoModel;

    public WearDeviceFragmentPresenter(Context context, WearDeviceFragmentView wearDeviceFragmentView,
                                       WearDeviceFragmentRouter wearDeviceFragmentRouter,
                                       UserUseCase userUseCase,
                                       GobeUseCase gobeUseCase,
                                       RegisterUserRequest registerUserRequest,
                                       GenkiUserInfoModel genkiUserInfoModel) {
        super(context, wearDeviceFragmentView, wearDeviceFragmentRouter);
        mUserUseCase = userUseCase;
        mGobeUseCase = gobeUseCase;
        mRegisterUserRequest = registerUserRequest;
        mGenkiUserInfoModel = genkiUserInfoModel;
    }

    @Override
    public void requestDefaultHighLightWearDevice() {
        int healthSource = mGenkiUserInfoModel.getHealthSource();
        requestHighLightWearDevice(healthSource);
    }

    @Override
    public void requestHighLightWearDevice(int healthSource) {
        int currentHealthSource = mGenkiUserInfoModel.getHealthSource();

        if (currentHealthSource == healthSource) {
            Timber.d("Current health source is: %d, we skip change now", currentHealthSource);
            updateWearViews(healthSource);
            return;
        }
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                healthSource)
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(), mGenkiUserInfoModel.getGobePassword())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();
        mUserUseCase.execute(new UpdateHealthSourceSubscriber(), UserParam.Factory.updateHealthSourceSetting(request));
    }

    private class UpdateHealthSourceSubscriber extends DefaultSubscriber<Integer> {
        @Override
        public void onNext(Integer healthSource) {
            if (healthSource == Constants.HealthSource.SMART_PHONE) {
                StepCounterService.Start(mContext);
            } else {
                StepCounterService.Stop(mContext);
            }
            updateWearViews(healthSource);
        }

        @Override
        public void onError(Throwable t) {
            Timber.e("Update health source fail");
            MessageUtil.showShortToast(mContext, "Error! Cannot update health source");
            updateWearViews(mGenkiUserInfoModel.getHealthSource());
        }
    }

    private void updateWearViews(int healthSource) {
        int idResource = -1;
        switch (healthSource) {
            case Constants.HealthSource.SMART_PHONE:
                idResource = R.id.register_smartphone_device_tv;
                break;
            case Constants.HealthSource.GOBE:
                idResource = R.id.register_gobe_wear_device_ib;
                break;
            case Constants.HealthSource.FITBIT:
                idResource = R.id.register_fitbit_wear_device_ib;
                break;
            case Constants.HealthSource.GOOGLE_FIT:
                idResource = R.id.register_other_device_tv;
                break;
        }
        mGenkiUserInfoModel.setHealthSource(healthSource);
        mView.onHighLightWearDevice(idResource);
    }
}
