package com.genkimiru.app.presentation.register.hospital_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalFragmentRouter;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalFragmentView;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalPresenter;

public class HospitalFragmentPresenter extends BasePresenterImp<HospitalFragmentView,
        HospitalFragmentRouter> implements HospitalPresenter {

    private UserUseCase mUserUseCase;

    public HospitalFragmentPresenter(Context context, HospitalFragmentView hospitalFragmentView,
                                     HospitalFragmentRouter hospitalFragmentRouter, UserUseCase userUseCase) {
        super(context, hospitalFragmentView, hospitalFragmentRouter);
        mUserUseCase = userUseCase;
    }
}
