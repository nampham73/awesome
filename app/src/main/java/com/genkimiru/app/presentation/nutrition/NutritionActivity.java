package com.genkimiru.app.presentation.nutrition;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.dialog.AddFoodDialog;
import com.genkimiru.app.presentation.chart.BarChartActivity;
import com.genkimiru.app.presentation.nutrition.NutritionActivityContract.NutritionPresenter;
import com.genkimiru.app.presentation.nutrition.NutritionActivityContract.NutritionView;
import com.genkimiru.app.presentation.nutrition.NutritionActivityContract.NutritionRouter;
import com.genkimiru.app.presentation.nutrition.nutrition_adapters.NutritionAdapter;
import com.genkimiru.app.presentation.nutrition.nutrition_adapters.NutritionViewHolder;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class NutritionActivity extends BarChartActivity<NutritionPresenter> implements NutritionView, NutritionRouter {

    @BindView(R.id.nutri_activity_protein_tv)
    TextView mProteinTxt;
    @BindView(R.id.nutri_activity_fat_tv)
    TextView mFatTxt;
    @BindView(R.id.nutri_activity_carb_txt)
    TextView mCarbTxt;
    @BindView(R.id.nutri_activity_cal_txt)
    TextView mKcal;
    @BindView(R.id.nutri_activity_food_list_rcv)
    RecyclerView mRecyclerView;
    @BindView(R.id.nutri_activity_add_btn)
    Button mAddBtn;
    @BindView(R.id.nutri_activity_borderline)
    View mLine;
    private NutritionAdapter mAdapter;

    @Inject
    NutritionPresenter mPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    private Calendar mCurrentDay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    public NutritionAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_nutrition;
    }

    @Override
    protected void onDayChange(Calendar calendar) {
        super.onDayChange(calendar);
        Timber.d("on day change");
        showProgress();
        hideSummarizeAndAverageText();
        mCurrentDay = calendar;
        mPresenter.getData(calendar, ViewType.DAY);
        mRecyclerView.setVisibility(View.VISIBLE);
        if (calendar.get(Calendar.DATE) != DateUtil.today().get(Calendar.DATE)) {
            mAddBtn.setVisibility(View.GONE);
            mLine.setVisibility(View.GONE);
        } else {
            mAddBtn.setVisibility(View.VISIBLE);
            mLine.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onWeekChange(Calendar calendar) {
        super.onWeekChange(calendar);
        Timber.d("on day change");
        showProgress();
        showAverageText();
        mCurrentDay = calendar;
        mAddBtn.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mLine.setVisibility(View.GONE);
        mPresenter.getData(calendar, ViewType.WEEK);
    }

    @Override
    protected void onMonthChange(Calendar calendar) {
        super.onMonthChange(calendar);
        Timber.d("on day change");
        showProgress();
        showAverageText();
        mCurrentDay = calendar;
        mAddBtn.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mLine.setVisibility(View.GONE);
        mPresenter.getData(calendar, ViewType.MONTH);
    }

    @Override
    protected int getMainColor() {
        return R.color.step_chart;
    }

    @Override
    protected BarChart getMainChart() {
        return findViewById(R.id.nutrition_bar_chart);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.nutrition_activity_title);
    }

    @Override
    protected NutritionPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @OnClick(R.id.nutri_activity_add_btn)
    void showAddFoodDialog() {
        AddFoodDialog addFoodDialog = new AddFoodDialog.AddFoodDialogBuilder().dialogListener(new AddFoodDialog.OnResultListener() {
            @Override
            public void onAdd(AddFoodDialog addFoodDialog, String name, String cal, String pro, String fat, String carb, String time, long timestamp) {

                mPresenter.addListFoodToServer(getCurrentCalendar(), name, cal, pro, fat, carb, timestamp);
                GenkiNutritionModel.NutriManualModel manual = new GenkiNutritionModel.NutriManualModel(
                        Float.parseFloat(cal),
                        Float.parseFloat(fat),
                        name,
                        Float.parseFloat(pro),
                        Float.parseFloat(carb),
                        timestamp,
                        timestamp
                );

                mAdapter.addItem(manual, mAdapter.getItemCount());
                addFoodDialog.dismiss();
            }

            @Override
            public void onCancel(AddFoodDialog addFoodDialog) {
                addFoodDialog.dismiss();
            }
        }).build();
        addFoodDialog.show(getSupportFragmentManager());
    }

    @Override
    public void showChartData(ArrayList<BarEntry> entries) {
        hideProgress();
        setBarChartData(entries, R.color.step_chart);
    }

    @Override
    public void loadDataFail(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void showData(GenkiNutritionModel genkiNutritionModel) {
        mKcal.setText(StringUtil.formatUnnecessaryZero(genkiNutritionModel.getKcal()));
        mProteinTxt.setText(StringUtil.formatUnnecessaryZero(genkiNutritionModel.getProtein()));
        mFatTxt.setText(StringUtil.formatUnnecessaryZero(genkiNutritionModel.getFat()));
        mCarbTxt.setText(StringUtil.formatUnnecessaryZero(genkiNutritionModel.getCarb()));
        mAdapter.set(genkiNutritionModel.getManual_list());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateDataSuccess(boolean isSuccess) {
        if (isSuccess) {
            onDayChange(mCurrentDay);
        }
    }

    private void initialize() {
        mAdapter = new NutritionAdapter(this, new NutritionViewHolder.UpdateListener() {
            @Override
            public void onUpdate(GenkiNutritionModel.NutriManualModel manualModel) {
                mPresenter.updateManualInputData(manualModel.getId(),
                        manualModel.getFood(),
                        String.valueOf(manualModel.getKcal()),
                        String.valueOf(manualModel.getProtein()),
                        String.valueOf(manualModel.getFat()),
                        String.valueOf(manualModel.getCarb()));
                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onDelete(GenkiNutritionModel.NutriManualModel manualModel) {
                mPresenter.deleteManualInputData(manualModel.getId());
                mAdapter.deleteItem(manualModel);
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mRecyclerView.setAdapter(mAdapter);

        if (mGenkiUserInfoModel.getHealthSource() == Constants.HealthSource.GOBE || mGenkiUserInfoModel.getHealthSource() == Constants.HealthSource.FITBIT) {
            mRecyclerView.setVisibility(View.GONE);
            mAddBtn.setVisibility(View.GONE);
            mLine.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mAddBtn.setVisibility(View.VISIBLE);
            mLine.setVisibility(View.VISIBLE);
        }
    }
}
