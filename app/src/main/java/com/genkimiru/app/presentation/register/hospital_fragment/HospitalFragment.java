package com.genkimiru.app.presentation.register.hospital_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RadioButton;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalFragmentRouter;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalFragmentView;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalPresenter;
import javax.inject.Inject;
import butterknife.OnClick;
import timber.log.Timber;

public class HospitalFragment extends SimpleBaseFragment<HospitalPresenter>
        implements HospitalFragmentView, HospitalFragmentRouter {

    private RegisterActivity mRegisterActivity;

    @Inject
    HospitalPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_healthy_hospital;
    }

    @Override
    protected HospitalPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialDefaultValue();
    }

    @OnClick({R.id.checkbox_hospital_yes, R.id.checkbox_hospital_no})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.checkbox_hospital_yes:
                if (checked) {
                    mRegisterUserRequest.surgery_status = 1;
                    Timber.d("Surgery status: 0");
                }
                break;
            case R.id.checkbox_hospital_no:
                if (checked) {
                    mRegisterUserRequest.surgery_status = 0;
                    Timber.d("Surgery status: 1");
                }
                break;
        }
    }

    private void initialDefaultValue() {
        mRegisterUserRequest.surgery_status = 0;
    }
}
