package com.genkimiru.app.presentation.photo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.ImageUtils;
import com.genkimiru.app.base.util.StaticMethods;
import com.genkimiru.app.base.util.StorageUtil;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.CROP;
import static com.genkimiru.app.Constants.Extra.RETAKE_CODE;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_CAMERA;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_CAMERA_NO_CROP;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GALLERY;
import static com.theartofdev.edmodo.cropper.CropImageView.RequestSizeOptions.RESIZE_FIT;

public class PhotoCroppingActivity extends SimpleBaseActivity {

    public static final int RESULT_RETAKE = 201;
    public static final int RESULT_FAILURE_IO = 202;

    private static final int CROP_WIDTH = 720;
    private static final int CROP_HEIGHT = 720;
    private static final String CROPPED_IMAGE_NAME = "cropped_image.png";
    private Boolean mIsCrop = false;

    @BindView(R.id.cropImageView)
    CropImageView mCropImageView;

    @BindView(R.id.tv_cancel)
    TextView mTvCancel;

    @BindView(R.id.tv_ok)
    TextView mTvOk;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_photo_cropping;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Uri photoUri = getIntent().getData();
        mIsCrop = getIntent().getBooleanExtra(CROP, true);
        initCropView(photoUri);
        int retakeCode = getIntent().getIntExtra(RETAKE_CODE, -1);
        setActionButtonText(retakeCode);
    }

    private void setActionButtonText(int retakeCode) {
        switch (retakeCode) {
            case REQUEST_CODE_CAMERA_NO_CROP:
            case REQUEST_CODE_CAMERA:
                mTvCancel.setText(R.string.avatar_picker_cancel_camera);
                mTvOk.setText(R.string.avatar_picker_ok_camera);
                break;
            case REQUEST_CODE_GALLERY:
                mTvCancel.setText(R.string.avatar_picker_cancel_gallery);
                mTvOk.setText(R.string.avatar_picker_ok_gallery);
                break;
            default:break;
        }
    }

    private void initCropView(Uri imageUri) {
        if (mIsCrop) {
            // Set ratio for cropped image by 1:1
            mCropImageView.setFixedAspectRatio(true);
            mCropImageView.setGuidelines(CropImageView.Guidelines.OFF);
            mCropImageView.setScaleType(CropImageView.ScaleType.FIT_CENTER);
            mCropImageView.setAutoZoomEnabled(true);

            // Set fixed size for cropped image with 720 x 720
            mCropImageView.setMinCropResultSize(CROP_WIDTH, CROP_HEIGHT);

            // Begin set image data by Uri
            mCropImageView.setImageUriAsync(imageUri);
        } else {
            mCropImageView.setImageUriAsync(imageUri);
            mCropImageView.setShowCropOverlay(mIsCrop);
        }
    }

    @OnClick(R.id.tv_cancel)
    public void retakeImage() {
        // An old intent will already contain retake_code to decide which action to do when retake: camera or gallery
        Intent returnedIntent = getIntent();
        setResult(RESULT_RETAKE, returnedIntent);
        finish();
    }

    @OnClick(R.id.tv_ok)
    public void useImage() {
        mTvOk.setEnabled(false);
        // If cropped size is larger than 720 x 720, cropped it again before make it as final
        showProgress();
        getCroppedImageObservable().flatMap(bitmap -> getWriteImageObservable(bitmap))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    setReturnResult(uri);
                    hideProgress();
                    finish();
                }, ex -> {
                    hideProgress();
                    finish();
                });
    }

    private Observable<Bitmap> getCroppedImageObservable() {
        return Observable.defer(() -> Observable.just(mCropImageView.getCroppedImage(CROP_WIDTH, CROP_HEIGHT, RESIZE_FIT)));
    }

    private Observable<Uri> getWriteImageObservable(Bitmap cropped) {
        return Observable.defer(() -> {
            if (StaticMethods.isNull(cropped)) {
                return Observable.just(null);
            }
            Uri croppedPhotoUri;
            try {
                String imagePath = StorageUtil.getInternalImagePath(getApplicationContext(), CROPPED_IMAGE_NAME);
                croppedPhotoUri = ImageUtils.writeImageToStorage(imagePath, cropped);
            } catch (IOException e) {
                Timber.e(e, "Failed to save cropped image to storage");
                croppedPhotoUri = null;
            } finally {
                cropped.recycle();
            }
            return Observable.just(croppedPhotoUri);
        }).subscribeOn(Schedulers.io());
    }

    private void setReturnResult(Uri uri) {
        if (uri != null) {
            Intent returnedIntent = getIntent();
            returnedIntent.setData(uri);
            setResult(RESULT_OK, returnedIntent);
        } else {
            // Uri = null, means some IO exceptions happened while writing image to storage
            setResult(RESULT_FAILURE_IO);
        }
    }
}
