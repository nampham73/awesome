package com.genkimiru.app.presentation.main.contest_fragment.privates;

import android.content.Context;

import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.contest_fragment.ContestsListFragment;
import com.genkimiru.app.presentation.main.contest_fragment.privates.PrivatesContestContract.PrivateContestPresenter;
import com.genkimiru.app.presentation.main.contest_fragment.privates.PrivatesContestContract.PrivateContestRouter;
import com.genkimiru.app.presentation.main.contest_fragment.privates.PrivatesContestContract.PrivateContestView;

import javax.inject.Inject;

public class PrivatesContestFragment extends ContestsListFragment<PrivateContestPresenter>
        implements PrivateContestView,PrivateContestRouter {

    @Inject PrivateContestPresenter mPresenter;

    private MainActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    protected PrivateContestPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected PrivateContestPresenter getPresenter() {
        return mPresenter;
    }
}
