package com.genkimiru.app.presentation.register.wear_device_fragment;

import android.support.annotation.IdRes;
import android.support.annotation.IntegerRes;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

public interface WearDeviceFragmentContract {

    interface WearDevicePresenter extends BasePresenter {
        void requestDefaultHighLightWearDevice();
        void requestHighLightWearDevice(int healthSource);
    }

    interface WearDeviceFragmentView {
        void onHighLightWearDevice(@IdRes int id);
    }

    interface WearDeviceFragmentRouter {
    }
}
