package com.genkimiru.app.presentation.splash;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.splash.SplashActivityContract.*;

public class SplashActivityPresenter extends BasePresenterImp<SplashView, SplashRouter>
        implements SplashPresenter {

    private UserUseCase mUserUseCase;
    private GenkiUserInfoModel mGenkiUserInfoModel;

    public SplashActivityPresenter(Context context,
                                   SplashView splashView,
                                   SplashRouter splashRouter,
                                   UserUseCase userUseCase,
                                   GenkiUserInfoModel genkiUserInfoModel) {
        super(context, splashView, splashRouter);
        mUserUseCase = userUseCase;
        mGenkiUserInfoModel = genkiUserInfoModel;
    }

    @Override
    public boolean checkUserInformation() {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return StringUtil.isNotEmpty(token);
    }

    @Override
    public void getUserInformation() {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        mUserUseCase.execute(new UserInformationSubscriber(), UserParam.Factory.getUserInformation(token));
    }

    private class UserInformationSubscriber extends DefaultSubscriber<UserInfoResponse> {

        @Override
        public void onNext(UserInfoResponse userInfoResponse) {
            Timber.d("Get user info success: %s", userInfoResponse.username);
            mGenkiUserInfoModel.setValueFromResponse(userInfoResponse);
            mView.onGetUserInfoSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Get user info error: %s", throwable.getMessage());
            mView.onGetUserInfoError(ApiErrorHandler.getMessage(mContext, throwable, R.string.get_user_information_default_error));
        }
    }
}
