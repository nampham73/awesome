package com.genkimiru.app.presentation.main.contest_fragment.recommend;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.ListFragment;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.contest_fragment.ContestsListFragment;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestContract.RecommendContestPresenter;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestContract.RecommendContestRouter;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestContract.RecommendContestView;

import java.util.List;

import javax.inject.Inject;

public class RecommendContestFragment extends ContestsListFragment<RecommendContestPresenter>
        implements RecommendContestRouter,RecommendContestView{

    @Inject RecommendContestPresenter mPresenter;

    private MainActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    protected RecommendContestPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected RecommendContestPresenter getPresenter() {
        return mPresenter;
    }
}
