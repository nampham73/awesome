package com.genkimiru.app.presentation.register.profile_avatar_fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SelectAvatarDialog;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.photo.PhotoActivity;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarFragmentRouter;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarFragmentView;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarPresenter;
import java.io.File;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GET_PHOTO;

public class ProfileAvatarFragment extends SimpleBaseFragment<ProfileAvatarPresenter>
        implements ProfileAvatarFragmentView, ProfileAvatarFragmentRouter {

    private SelectAvatarDialog selectAvatarDialog;
    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_avatar_iv)
    public ImageView mAvatarImageView;

    @Inject
    ProfileAvatarPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_healthy_profile_avatar;
    }

    @Override
    protected ProfileAvatarPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setInitialValue();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            return;
        }
        setInitialValue();
    }

    @OnClick(R.id.register_avatar_iv)
    void showDialog() {
        showConfirmImage();
    }

    public void showConfirmImage() {
        selectAvatarDialog = new SelectAvatarDialog();
        selectAvatarDialog.setDialogSelectListener(new SelectAvatarDialog.DialogSelectListener() {
            @Override
            public void onCameraClick(SelectAvatarDialog selectAvatarDialog) {
                mRegisterActivity.startActivityForResult(PhotoActivity.createIntent(
                        mRegisterActivity.getApplicationContext(),
                        PhotoActivity.Type.Capture),
                        REQUEST_CODE_GET_PHOTO);
                selectAvatarDialog.dismiss();
            }

            @Override
            public void onGalleryClick(SelectAvatarDialog selectAvatarDialog) {
                mRegisterActivity.startActivityForResult(PhotoActivity.createIntent(
                        mRegisterActivity.getApplicationContext(),
                        PhotoActivity.Type.Gallery),
                        REQUEST_CODE_GET_PHOTO);
                selectAvatarDialog.dismiss();
            }
        });
        selectAvatarDialog.show(mRegisterActivity.getSupportFragmentManager());
    }

    private void setAvatarImage(Uri avatarUri) {
        Timber.d("setAvatarImage avatarUri = %s", avatarUri);
        if (avatarUri == null) {
            return;
        }
        mAvatarImageView.setImageDrawable(null);
        mAvatarImageView.setImageURI(avatarUri);

        mRegisterUserRequest.avatarPath = avatarUri.getPath();
        mRegisterUserRequest.avatar = new File(avatarUri.getPath());
    }

    private void setInitialValue() {
        Timber.d("setInitialValue");
        if (mRegisterActivity.getAvatarUri() != null) {
            setAvatarImage(mRegisterActivity.getAvatarUri());
            return;
        }
        if (StringUtil.isNotEmpty(mRegisterUserRequest.avatarPath)){
            Uri myUri = Uri.parse(mRegisterUserRequest.avatarPath);
            setAvatarImage(myUri);
        }
    }
}
