package com.genkimiru.app.presentation.update_user_infor.heart_beat;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class UpdateHeartBeatActivityPresenter extends BasePresenterImp<UpdateHeartBeatActivityContract.UpdateHeartBeatView, UpdateHeartBeatActivityContract.UpdateHeartBeatRouter>
        implements UpdateHeartBeatActivityContract.UpdateHeartBeatPresenter {
    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    int mFeelCondition;

    public UpdateHeartBeatActivityPresenter(Context context, UpdateHeartBeatActivityContract.UpdateHeartBeatView updateHeartBeatView, UpdateHeartBeatActivityContract.UpdateHeartBeatRouter updateHeartBeatRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateHeartBeatView, updateHeartBeatRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;
    }


    @Override
    public void updateHeartBeat(int feelCondition) {
        mFeelCondition = feelCondition;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setFeelCondition(feelCondition)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();

        mUserUseCase.execute(new UpdateHeartBeatSubcriber(), (UserParam.Factory.updateUserProfile(request)));
    }

    private class UpdateHeartBeatSubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Updage Hear Beat Success");
            mGenkiUserInfoModel.setFeelCondition(mFeelCondition);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
