package com.genkimiru.app.presentation.register.blood_type_fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypeFragmentRouter;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypeFragmentView;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypePresenter;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.OnItemSelected;
import timber.log.Timber;

public class BloodTypeFragment extends SimpleBaseFragment<BloodTypePresenter>
        implements BloodTypeFragmentView, BloodTypeFragmentRouter {

    @Inject
    BloodTypePresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @BindView(R.id.register_bloodtype_spn)
    Spinner spnBloodType;

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_bloodtype;
    }

    @Override
    protected BloodTypePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showSpinnerBloodType();
    }

    @SuppressWarnings("unchecked")
    public void showSpinnerBloodType() {
        Timber.d("showSpinnerBlood");
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.blood_type)) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Color.WHITE);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                return tv;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                tv.setTextColor(Color.BLACK);
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                return tv;
            }

        };
        spnBloodType.setAdapter(arrayAdapter);
    }

    @OnItemSelected(R.id.register_bloodtype_spn)
    public void spinnerItemSelected(Spinner spinner, int position) {
        mRegisterUserRequest.blood_type = spinner.getItemAtPosition(position).toString();
        Timber.d("bloodType %s", spinner.getItemAtPosition(position).toString());
    }
}
