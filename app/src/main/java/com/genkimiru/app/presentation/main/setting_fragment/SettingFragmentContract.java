package com.genkimiru.app.presentation.main.setting_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface SettingFragmentContract {

    interface SettingPresenter extends BasePresenter {

    }

    interface SettingView {

    }

    interface SettingRouter {
        void openEditProfileScreen();
        void openChangePasswordScreen();
        void openPrivacySettingScreen();
        void openNotificationSettingScreen();
        void openSettingEmailMarketingScreen();
        void openTermOfUseScreen();
        void openHealthSourceSetting();
        void openReportBugScreen();
        void openFaqScreen();
        void openChatWithWatson();
    }
}
