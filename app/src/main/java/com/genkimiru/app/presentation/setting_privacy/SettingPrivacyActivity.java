package com.genkimiru.app.presentation.setting_privacy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Switch;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyPresenter;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyRouter;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyView;

public class SettingPrivacyActivity extends SimpleBaseActivity<SettingPrivacyPresenter> implements SettingPrivacyView, SettingPrivacyRouter {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar mToolbar;
    @BindView(R.id.setting_privacy_health_switch_btn)
    Switch mHealthSwitchBtn;
    @BindView(R.id.setting_privacy_account_switch_btn)
    Switch mAccountSwitchBtn;

    private TextDialogFragment mWarningDialogFragment;
    private CompositeDisposable mCompositeDisposable;
    private boolean mIsNeedToCallApi = true;

    @Inject
    SettingPrivacyPresenter mSettingPrivacyPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_setting_privacy;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHealthSwitchBtn.setChecked(mGenkiUserInfoModel.isHealthFlag());
        mAccountSwitchBtn.setChecked(mGenkiUserInfoModel.isProfileFlag());
        initializeEventHandler();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.privacy_setting_name);
    }

    @Override
    protected SettingPrivacyPresenter registerPresenter() {
        return mSettingPrivacyPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    private void openWarningDialog() {
        mWarningDialogFragment = new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .title(getString(R.string.privacy_setting_warning))
                .message(getString(R.string.privacy_setting_message))
                .rightButtonText(getString(R.string.privacy_warning_not_allow))
                .leftButtonText(getString(R.string.privacy_warning_cancel))
                .onResultListener(new TextDialogFragment.OnResultListener() {

                    @Override
                    public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                        mIsNeedToCallApi = false;
                        mHealthSwitchBtn.setChecked(mGenkiUserInfoModel.isHealthFlag());
                        mAccountSwitchBtn.setChecked(mGenkiUserInfoModel.isProfileFlag());
                        mIsNeedToCallApi = true;
                        dismissDialog();
                    }

                    @Override
                    public void onClickRightButton(TextDialogFragment textDialogFragment) {
                        showProgress();
                        mSettingPrivacyPresenter.updatePrivacySetting(mHealthSwitchBtn.isChecked(), mAccountSwitchBtn.isChecked());
                        dismissDialog();
                    }
                })
                .build();
        mWarningDialogFragment.show(getSupportFragmentManager());
    }

    private void dismissDialog() {
        if (mWarningDialogFragment == null) {
            return;
        }
        mWarningDialogFragment.dismiss();
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mHealthSwitchBtn).skip(1)
                .subscribe(isChecked -> {
                    if(!mIsNeedToCallApi) {
                        return;
                    }

                    if (!isChecked) {
                        openWarningDialog();
                        return;
                    }
                    showProgress();
                    mSettingPrivacyPresenter.updatePrivacySetting(isChecked, mAccountSwitchBtn.isChecked());
                }));

        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mAccountSwitchBtn).skip(1)
                .subscribe(isChecked -> {
                    if(!mIsNeedToCallApi) {
                        return;
                    }

                    if (!isChecked) {
                        openWarningDialog();
                        return;
                    }
                    showProgress();
                    mSettingPrivacyPresenter.updatePrivacySetting(mHealthSwitchBtn.isChecked(), isChecked);
                }));
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    public void onUpdateFail() {
        hideProgress();
        mHealthSwitchBtn.setChecked(mGenkiUserInfoModel.isHealthFlag());
        mAccountSwitchBtn.setChecked(mGenkiUserInfoModel.isProfileFlag());
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        MessageUtil.showShortToast(getApplicationContext(), getString(R.string.setting_privacy_notify_update_success));
        mGenkiUserInfoModel.setHealthFlag(mHealthSwitchBtn.isChecked());
        mGenkiUserInfoModel.setProfileFlag(mAccountSwitchBtn.isChecked());
    }
}
