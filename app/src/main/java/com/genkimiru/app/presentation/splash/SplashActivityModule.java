package com.genkimiru.app.presentation.splash;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.splash.SplashActivityContract.*;

@Module
public class SplashActivityModule {

    @Provides
    @ActivityScope
    SplashView provideView(SplashActivity splashActivity) {
        return splashActivity;
    }

    @Provides
    @ActivityScope
    SplashRouter provideRouter(SplashActivity splashActivity) {
        return splashActivity;
    }

    @Provides
    @ActivityScope
    SplashPresenter providePresenter(Context context, SplashView view, SplashRouter router, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        return new SplashActivityPresenter(context, view, router, userUseCase, genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository){
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
