package com.genkimiru.app.presentation.update_user_infor.alcohol;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityPresenter;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivityContract.*;

public class UpdateAlcoholActivityPresenter extends BasePresenterImp<UpdateAlcoholView, UpdateAlcoholRouter>
        implements UpdateAlcoholPresenter {

    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    int mAlcoholLevel;
    int mDrinkEachTime;

    public UpdateAlcoholActivityPresenter(Context context, UpdateAlcoholView updateAlcoholView, UpdateAlcoholRouter updateAlcoholRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateAlcoholView, updateAlcoholRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;

    }

    @Override
    public void updateAlcohol(int alcoholLevel, int drinkEachTime) {
        mAlcoholLevel = alcoholLevel;
        mDrinkEachTime = drinkEachTime;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setDrinkEachTime(drinkEachTime)
                .setAlcohol(alcoholLevel)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();
        mUserUseCase.execute(new UpdateAlcoholSubcriber(), (UserParam.Factory.updateUserProfile(request)));
    }

    private class UpdateAlcoholSubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Updage Alcohol Success");
            mGenkiUserInfoModel.setAlcoholLevel(mAlcoholLevel);
            mGenkiUserInfoModel.setDrinkEachTime(mDrinkEachTime);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
