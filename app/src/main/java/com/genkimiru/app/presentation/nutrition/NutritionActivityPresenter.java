package com.genkimiru.app.presentation.nutrition;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DeleteManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.data.remote.genki.request.UpdateManualInputRequest;
import com.genkimiru.app.domain.mydata.MyDataParam;
import com.genkimiru.app.domain.mydata.MyDataUseCase;
import com.github.mikephil.charting.data.BarEntry;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.chart.BarChartActivity.CHART_COLUMN;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.DAY;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.MONTH;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.WEEK;
import static com.genkimiru.app.presentation.nutrition.NutritionActivityContract.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import timber.log.Timber;

public class NutritionActivityPresenter extends BasePresenterImp<NutritionView, NutritionRouter>
        implements NutritionActivityContract.NutritionPresenter {

    private MyDataUseCase mMyDataUseCase;

    public NutritionActivityPresenter(Context context, NutritionView nutritionView, NutritionRouter nutritionRouter, MyDataUseCase myDataUseCase) {
        super(context, nutritionView, nutritionRouter);
        mMyDataUseCase = myDataUseCase;
    }

    public void getData(Calendar calendar, int viewType) {
        Calendar startCalendar = (Calendar) calendar.clone();
        Calendar endCalendar = (Calendar) calendar.clone();
        long currentTime = DateUtil.getToday() / 1000L;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int dataType = DetailChartDataRequest.DataType.NUTRI;
        DetailChartDataRequest.Builder request = new DetailChartDataRequest.Builder(token).setDataType(dataType);
        switch (viewType) {
            case DAY:
                startCalendar.add(Calendar.DAY_OF_MONTH, -3);
                endCalendar.add(Calendar.DAY_OF_MONTH, 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startDay = startCalendar.getTimeInMillis() / 1000L; //get start day in timestamp
                long endDay = endCalendar.getTimeInMillis() / 1000L; // get end day in time stamp
                if (endDay > currentTime) {
                    endDay = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.DAILY)
                        .setStartDate(startDay)
                        .setEndDate(endDay);
                break;

            case WEEK:
                startCalendar.add(Calendar.DAY_OF_MONTH, -(7 * 3));
                endCalendar.add(Calendar.DAY_OF_MONTH, 7 * 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startWeek = startCalendar.getTimeInMillis() / 1000L;
                long endWeek = endCalendar.getTimeInMillis() / 1000L;
                if (endWeek > currentTime) {
                    endWeek = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.WEEKLY)
                        .setStartDate(startWeek)
                        .setEndDate(endWeek);
                break;

            case MONTH:
                startCalendar.add(Calendar.MONTH, -3);
                endCalendar.add(Calendar.MONTH, 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startMonth = startCalendar.getTimeInMillis() / 1000L;
                long endMonth = endCalendar.getTimeInMillis() / 1000L;
                if (endMonth > currentTime) {
                    endMonth = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.MONTHLY)
                        .setStartDate(startMonth)
                        .setEndDate(endMonth);
                break;

        }
        mMyDataUseCase.execute(new GetNutritionDataSupscription(), MyDataParam.Factory.fetchingNutritionChartData(request.create()));

    }

    private ArrayList<BarEntry> createChartData(List<GenkiNutritionModel> nutritionModels) {
        if (nutritionModels == null || nutritionModels.size() <= 0) {
            return null;
        }

        for (int i = nutritionModels.size(); i < CHART_COLUMN; i++) {
            nutritionModels.add(new GenkiNutritionModel());
        }

        ArrayList<BarEntry> entries = new ArrayList<>();
        for (int i = 1; i < nutritionModels.size() + 1; i++) {
            float value = nutritionModels.get(i - 1).getKcal();
            entries.add(new BarEntry(i, value));
        }

        return entries;
    }

    public class GetNutritionDataSupscription extends DefaultSubscriber<List<GenkiNutritionModel>> {
        @Override
        public void onNext(List<GenkiNutritionModel> genkiNutritionModels) {
            Timber.d("Get nutrition chart detail success");
            mView.showChartData(createChartData(genkiNutritionModels));
            mView.showData(genkiNutritionModels.get(genkiNutritionModels.size()/2));
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Get nutrition chart detail error");
            mView.loadDataFail(ApiErrorHandler.getMessage(mContext, t, 0));
        }
    }

    public void addListFoodToServer(Calendar calendar, String name, String cal, String pro, String fat, String carb, long timestamp) {
        List<AddManualInputRequest.MyData> mMyDataList = new ArrayList<>();
        Calendar startCalendar = (Calendar) calendar.clone();
        Calendar endCalendar = (Calendar) calendar.clone();

        startCalendar.add(Calendar.DAY_OF_MONTH, -3);
        endCalendar.add(Calendar.DAY_OF_MONTH, 3);
        endCalendar.set(Calendar.HOUR_OF_DAY, 23);
        endCalendar.set(Calendar.MINUTE, 59);

        long startDay = startCalendar.getTimeInMillis() / 1000L; //get start day in timestamp
        long endDay = endCalendar.getTimeInMillis() / 1000L; // get end day in time stamp

        AddManualInputRequest.MyData newFood = new AddManualInputRequest.NutriData(
                timestamp,
                timestamp,
                Float.parseFloat(cal),
                Float.parseFloat(fat),
                Float.parseFloat(pro),
                Float.parseFloat(carb),
                name);
        mMyDataList.add(newFood);
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        AddManualInputRequest request = new AddManualInputRequest(token, startDay, endDay, AddManualInputRequest.DataType.NUTRI, mMyDataList);

        mMyDataUseCase.execute(new AddSupscription(), MyDataParam.Factory.addNutritionManualinputData(request));
    }

    public void deleteManualInputData(int id) {
        DeleteManualInputRequest requet = new DeleteManualInputRequest(id, DeleteManualInputRequest.DataType.NUTRI);
        mMyDataUseCase.execute(new DeleteMunualSupscription(), MyDataParam.Factory.deleteNutritionManualInputData(requet));
    }

    @Override
    public void updateManualInputData(int id, String name, String cal, String pro, String fat, String carb) {
        UpdateManualInputRequest.ManualData manual = new UpdateManualInputRequest
                .ManualData(Float.parseFloat(cal)
                , Float.parseFloat(pro)
                , Float.parseFloat(fat)
                , Float.parseFloat(carb)
                , name);
        UpdateManualInputRequest request = new UpdateManualInputRequest(id, UpdateManualInputRequest.DataType.NUTRI, manual);
        mMyDataUseCase.execute(new UpdateManualSupscription(), MyDataParam.Factory.updateNutritionManualInputData(request));
    }


    public class AddSupscription extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean genkiNutritionModels) {
            Timber.d("Add nutrition chart detail success");
            mView.updateDataSuccess(true);
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Add nutrition chart detail error " + t.getMessage());
            mView.updateDataSuccess(false);
        }
    }

    public class DeleteMunualSupscription extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean genkiNutritionModels) {
            Timber.d("Delete nutrition chart detail success");
            mView.updateDataSuccess(true);
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Delete nutrition chart detail error " + t.getMessage());
            mView.updateDataSuccess(false);
        }
    }

    private class UpdateManualSupscription extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean genkiNutritionModels) {
            Timber.d("Update nutrition chart detail success");
            mView.updateDataSuccess(true);
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Update nutrition chart detail error " + t.getMessage());
            mView.updateDataSuccess(false);
        }
    }
}
