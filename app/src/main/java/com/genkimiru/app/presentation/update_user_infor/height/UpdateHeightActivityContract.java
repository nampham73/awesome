package com.genkimiru.app.presentation.update_user_infor.height;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateHeightActivityContract {
    interface UpdateHeightPresenter extends BasePresenter {
        void updateHeight(float height);
        String validateHeight(String height);
    }

    interface UpdateHeightView {
        void onUpdateSuccess();
        void onUpdateError(String message);

    }

    interface UpdateHeightRouter {

    }
}
