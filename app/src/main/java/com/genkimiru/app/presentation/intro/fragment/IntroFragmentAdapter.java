package com.genkimiru.app.presentation.intro.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.genkimiru.app.R;

public class IntroFragmentAdapter extends FragmentPagerAdapter {

    public IntroFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Type type = Type.values()[position];
        IntroFragment fragment = new IntroFragment();
        fragment.setLayoutType(type);
        return fragment;
    }

    @Override
    public int getCount() {
        return Type.values().length;
    }

    public enum Type {

        EXERCISE(R.string.intro_exercise, R.drawable.bg_running),
        SERVICE(R.string.intro_service, R.drawable.bg_service),
        LIMITDIET(R.string.intro_limit_diet, R.drawable.bg_limit_eat),
        EATDIET(R.string.intro_eat_diet, R.drawable.bg_eat_diet),
        ANSWERDIET(R.string.intro_answer_diet, R.drawable.bg_answer_diet),
        WATER(R.string.intro_water, R.drawable.bg_splast_water);

        public int mTextId;
        public int mBackgroundId;

        Type(int mTextId, int mBackgroundId) {
            this.mTextId = mTextId;
            this.mBackgroundId = mBackgroundId;
        }

        public int getTextId() {
            return mTextId;
        }

        public int getBackgroundId() {
            return mBackgroundId;
        }
    }
}
