package com.genkimiru.app.presentation.weight;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface WeightActivityContract {
    interface WeightPresenter extends BasePresenter{

    }
    interface WeightView {

    }
    interface WeightRouter {

    }
}
