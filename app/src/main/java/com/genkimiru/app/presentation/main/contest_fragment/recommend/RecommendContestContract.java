package com.genkimiru.app.presentation.main.contest_fragment.recommend;

import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.contest_fragment.ContestListContract;

import java.util.List;

public interface RecommendContestContract {
    interface RecommendContestView extends ListContract.RootView<List<GenkiContestModel>>{}
    interface RecommendContestPresenter extends ContestListContract.ListPresenter{}
    interface RecommendContestRouter{}
}
