package com.genkimiru.app.presentation.contest_detail;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiContestDetailModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.response.ContestDetailResponse;
import com.genkimiru.app.domain.contest.ContestParam;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailPresenter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailRouter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailView;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class ContestDetailActivityPresenter extends BasePresenterImp<ContestDetailView, ContestDetailRouter>
        implements ContestDetailPresenter {

    private ContestUseCase mContestUseCase;

    private GenkiContestDetailModel mContestDetailModel;

    public ContestDetailActivityPresenter(Context context,
                                          ContestDetailView contestDetailView,
                                          ContestDetailRouter contestDetailRouter,
                                          ContestUseCase contestUseCase,
                                          GenkiContestDetailModel contestDetailModel) {
        super(context, contestDetailView, contestDetailRouter);
        this.mContestUseCase = contestUseCase;
        this.mContestDetailModel = contestDetailModel;
    }

    @Override
    public void getContestDetail(int id) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        mContestUseCase.execute(new ContestDetailSubscriber(), ContestParam.Factory.getContestDetail(id, token));
    }

    @Override
    public void joinContest(int id) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        JoinRequest request = new JoinRequest(id, token);
        mContestUseCase.execute(new JoinContestSubscriber(), ContestParam.Factory.joinContest(request));
    }

    private class ContestDetailSubscriber extends DefaultSubscriber<ContestDetailResponse> {

        @Override
        public void onNext(ContestDetailResponse contestDetailResponse) {
            Timber.d("Get contest detail success: %s", contestDetailResponse.description);
            mContestDetailModel.setContestDetailValue(contestDetailResponse);
            mView.onGetContestDetailSuccess(mContestDetailModel);
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Get contest detail error: %s", throwable.getMessage());
            mView.onGetContestDetailFail(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }

    private class JoinContestSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean bool) {
            Timber.d("Join success");
            mView.onJoinContestSuccess(mContext.getString(R.string.join_contest_success));
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Join error: %s", throwable.getMessage());
            mView.onJoinContestError(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }
}
