package com.genkimiru.app.presentation.forgot_password.new_password_fragment;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.ActivePasswordRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Input.PASSWORD_MIN_LENGTH;
import static com.genkimiru.app.Constants.Preference.RECOVER_PASSWORD_CODE;
import static com.genkimiru.app.presentation.forgot_password.new_password_fragment.NewPasswordFragmentContract.*;

public class NewPasswordFragmentPresenter extends BasePresenterImp<NewPasswordView, NewPasswordRouter>
        implements NewPasswordPresenter {

    private UserUseCase mUserUseCase;

    public NewPasswordFragmentPresenter(Context context,
                                        NewPasswordView newPasswordView,
                                        NewPasswordRouter newPasswordRouter,
                                        UserUseCase userUseCase) {
        super(context, newPasswordView, newPasswordRouter);
        mUserUseCase = userUseCase;
    }

    @Override
    public void onSendClick(String code, String password) {
        mView.showLoadingDialog();
        ActivePasswordRequest activePasswordRequest = new ActivePasswordRequest(password, code);
        mUserUseCase.execute(new ActivePasswordSubscriber(), UserParam.Factory.activePassword(activePasswordRequest));
    }

    @Override
    public boolean isValidRecoverCode(String code) {
        return StringUtil.isNotEmpty(code);
    }

    @Override
    public boolean isValidPassword(String password) {
        return StringUtil.isNotEmpty(password);
    }

    @Override
    public boolean isValidPasswordLength(String password) {
        return password.length() >= PASSWORD_MIN_LENGTH;
    }

    @Override
    public boolean isValidConfirmPassword(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUserUseCase.clear();
    }

    private class ActivePasswordSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean result) {
            mView.dismissLoadingDialog();
            Timber.d("ActivePasswordSubscriber result: %s", result);
            mView.openCompleteDialog();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.dismissLoadingDialog();
            Timber.e("ActivePasswordSubscriber Error: %s", throwable.getMessage());
            mView.showMessageDialog(mContext.getString(R.string.forgot_password_error_message));
        }
    }
}
