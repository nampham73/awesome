package com.genkimiru.app.presentation.main.contest_fragment;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.contest.ContestRepository;
import com.genkimiru.app.data.repository.contest.ContestRepositoryImp;
import com.genkimiru.app.dialog.ContestSortDialogFragment;
import com.genkimiru.app.domain.contest.ContestUseCase;

import dagger.Module;
import dagger.Provides;

@Module
public class PagerContestFragmentModule {

    @Provides
    @FragmentScope
    ContestFragmentAdapter provideAdapter(PagerContestFragment fragment) {
        return new ContestFragmentAdapter(fragment.getActivity(), fragment.getChildFragmentManager());
    }

    @Provides
    @FragmentScope
    ContestUseCase provideContestUseCase(SchedulerProvider schedulerProvider, ContestRepository repository) {
        return new ContestUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }

    @Provides
    @FragmentScope
    ContestRepository provideContestRepository(GenkiApi api) {
        return new ContestRepositoryImp(api);
    }

    @Provides
    @FragmentScope
    ContestSortDialogFragment provideSortPopup(){
        return new ContestSortDialogFragment();
    }
}
