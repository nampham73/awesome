package com.genkimiru.app.presentation.main.contest_fragment.current;

import com.genkimiru.app.base.scope.ChildFragmentScope;
import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CurrentContestFragmentBuilder {

    @ChildFragmentScope
    @ContributesAndroidInjector(modules = CurrentContestFragmentModule.class)
    abstract CurrentContestFragment currentContestFragment();
}
