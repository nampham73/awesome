package com.genkimiru.app.presentation.challenge_detail_history;

import com.genkimiru.app.base.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ChallengeDetailHistoryActivityModule {

    @Provides
    @ActivityScope
    ChallengeDetailHistoryAdapter provideAdapter(ChallengeDetailHistoryActivity activity){
        return new ChallengeDetailHistoryAdapter(activity,activity.getSupportFragmentManager());
    }

}
