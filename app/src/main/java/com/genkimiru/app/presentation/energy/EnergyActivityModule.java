package com.genkimiru.app.presentation.energy;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.mydata.MyDataRepository;
import com.genkimiru.app.data.repository.mydata.MyDataRepositoryImp;
import com.genkimiru.app.domain.mydata.MyDataUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.energy.EnergyActivityContract.*;

@Module
public class EnergyActivityModule {

    @ActivityScope
    @Provides
    EnergyView provideView(EnergyActivity energyActivity) {
        return energyActivity;
    }

    @ActivityScope
    @Provides
    EnergyRouter provideRouter(EnergyActivity energyActivity) {
        return energyActivity;
    }

    @ActivityScope
    @Provides
    EnergyPresenter providePresenter(Context context, EnergyView view, EnergyRouter router, MyDataUseCase myDataUseCase) {
        return new EnergyActivityPresenter(context, view, router, myDataUseCase);
    }

    @Provides
    @ActivityScope
    MyDataUseCase provideUseCase(SchedulerProvider schedulerProvider, MyDataRepository repository){
        return new MyDataUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }

    @Provides
    @ActivityScope
    MyDataRepository provideRepository(GenkiApi api){
        return new MyDataRepositoryImp(api);
    }
}
