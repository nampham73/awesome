package com.genkimiru.app.presentation.step;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public interface StepActivityContract {

    interface StepPresenter extends BasePresenter {
        void getData(Calendar calendar, int viewType);
    }

    interface StepView {
        void showChartData(ArrayList<BarEntry> entries);
        void showMinuteDataChart(ArrayList<BarEntry> entries);
        void loadDataFail(String message);
        void showData(GenkiStepModel genkiStepModel);
    }

    interface StepRouter {

    }
}
