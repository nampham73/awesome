package com.genkimiru.app.presentation.update_user_infor.height;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityContract.UpdateHeightPresenter;
import static com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityContract.UpdateHeightRouter;
import static com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityContract.UpdateHeightView;

public class UpdateHeightActivityPresenter extends BasePresenterImp<UpdateHeightView, UpdateHeightRouter>
        implements UpdateHeightPresenter {
    GenkiUserInfoModel mGenkiUserInfoModel;
    UserUseCase mUserUseCase;
    float mHeight;

    public UpdateHeightActivityPresenter(Context context, UpdateHeightView updateHeightView, UpdateHeightRouter updateHeightRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateHeightView, updateHeightRouter);
        this.mGenkiUserInfoModel = genkiUserInfoModel;
        this.mUserUseCase = userUseCase;
    }

    @Override
    public void updateHeight(float height) {
        mHeight = height;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setHeight(height)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();

        mUserUseCase.execute(new UpdateHeightSubcriber(), (UserParam.Factory.updateUserProfile(request)));
    }

    @Override
    public String validateHeight(String height) {
        if (Float.parseFloat(height) < 1) {
            height = "1";
        }
        if (Float.parseFloat(height) > 300) {
            height = "300";
        }
        return height;
    }

    private class UpdateHeightSubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Updage Height Success");
            mGenkiUserInfoModel.setHeight(mHeight);
            mView.onUpdateSuccess();

        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
