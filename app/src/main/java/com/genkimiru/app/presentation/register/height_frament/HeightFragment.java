package com.genkimiru.app.presentation.register.height_frament;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightFragmentRouter;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightFragmentView;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class HeightFragment extends SimpleBaseFragment<HeightPresenter>
        implements HeightFragmentView, HeightFragmentRouter {

    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_height_edt)
    EditText mHeightEdt;

    @Inject
    HeightPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    private CompositeDisposable mCompositeDisposable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    public void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_height;
    }

    @Override
    protected HeightPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisible()) {
            return;
        }
        initialDefaultValue();
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(RxTextView.textChanges(mHeightEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(height -> {
                    Timber.d("height: %s", height.toString());
                    if (StringUtil.isEmpty(height)) {
                        mRegisterActivity.disableSwipe();
                        return;
                    } else {
                        mRegisterActivity.enableSwipe();
                    }
                    height = mPresenter.validateHeight(height.toString().trim());
                    mHeightEdt.setText(height.toString());
                    mHeightEdt.setSelection(mHeightEdt.getText().length());
                    setHeight();
                }));
    }

    public void setHeight() {
        mRegisterUserRequest.height = Float.parseFloat(mHeightEdt.getText().toString().trim());
        mRegisterUserRequest.height_type = 1;
    }

    public void initialDefaultValue() {
        Timber.d("initialDefaultValue: %s", mRegisterUserRequest.height);
        if (mRegisterUserRequest.height == 0) {
            setHeight();
            return;
        }
        if (StringUtil.isEmpty(mHeightEdt.getText().toString().trim())) {
            mHeightEdt.setText(getString(R.string.register_height_default_value));
        }
        mHeightEdt.setText(String.valueOf(mRegisterUserRequest.height));
        mRegisterUserRequest.height_type = 1;
    }
}
