package com.genkimiru.app.presentation.other_wear_device;

import android.content.Context;

import com.genkimiru.app.R;
import com.genkimiru.app.base.image_loader.WriteFileAsync;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.ImageUtils;
import com.genkimiru.app.data.model.GFitUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.google.request.GoogleAccessTokenRequest;
import com.genkimiru.app.data.remote.google.request.GoogleFitUserProfileRequest;
import com.genkimiru.app.data.remote.google.response.GoogleAccessTokenResponse;
import com.genkimiru.app.data.remote.google.response.GoogleFitUserProfileResponse;
import com.genkimiru.app.domain.google.GoogleParam;
import com.genkimiru.app.domain.google.GoogleUseCase;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDevicePresenter;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDeviceRouter;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDeviceView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Google.CLIENT_ID;
import static com.genkimiru.app.Constants.Google.CLIENT_SECRECT;
import static com.genkimiru.app.Constants.Google.GRANT_TYPE;
import static com.genkimiru.app.Constants.Google.REDIRECT_URI;
import static com.genkimiru.app.base.util.DateUtil.Format.DAY_MONTH_YEAR_FORMAT;

public class OtherWearDeviceActivityPresenter extends BasePresenterImp<OtherWearDeviceView, OtherWearDeviceRouter>
        implements OtherWearDevicePresenter {

    private GoogleUseCase mGoogleUseCase;
    private GFitUserInfoModel mGFitUserInfoModel;
    private String avatarUrl;
    RegisterUserRequest mRegisterUserRequest;
    private boolean mIsLoginState;

    public OtherWearDeviceActivityPresenter(Context context,
                                            OtherWearDeviceView otherWearDeviceView,
                                            OtherWearDeviceRouter otherWearDeviceRouter,
                                            GoogleUseCase googleUseCase,
                                            RegisterUserRequest registerUserRequest) {
        super(context, otherWearDeviceView, otherWearDeviceRouter);
        mGoogleUseCase = googleUseCase;
        mRegisterUserRequest = registerUserRequest;
    }

    @Override
    public void loginGoogleFit(String code) {
        Timber.d("login Google: code = %s", code);
        mView.showProgressBar();
        GoogleAccessTokenRequest request = new GoogleAccessTokenRequest(code, REDIRECT_URI, CLIENT_ID, CLIENT_SECRECT, "", GRANT_TYPE);
        mGoogleUseCase.execute(new GoogleFitAccessTokenSubscriber(), GoogleParam.Factory.getAccessToken(request));
    }

    @Override
    public void handleSignInResult(Task<GoogleSignInAccount> completedTask, boolean isLoginState) {
        try {
            mIsLoginState = isLoginState;
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Timber.d("login Google: code = %s", account.getServerAuthCode());
            mView.showProgressBar();
            GoogleAccessTokenRequest request = new GoogleAccessTokenRequest(account.getServerAuthCode(), REDIRECT_URI, CLIENT_ID, CLIENT_SECRECT, "", GRANT_TYPE);
            mGoogleUseCase.execute(new GoogleFitAccessTokenSubscriber(), GoogleParam.Factory.getAccessToken(request));
        } catch (ApiException e) {

        }
    }

    private class GoogleFitAccessTokenSubscriber extends DefaultSubscriber<GoogleAccessTokenResponse> {

        @Override
        public void onNext(GoogleAccessTokenResponse googleAccessTokenResponse) {
            String auth_header = googleAccessTokenResponse.token_type + " " + googleAccessTokenResponse.access_token;
            Timber.d("Googlefit get access token success: %s", auth_header);
            GoogleFitUserProfileRequest request = new GoogleFitUserProfileRequest(auth_header);
            mGoogleUseCase.execute(new GoogleFitUserProfileSubscriber(), GoogleParam.Factory.getUserProfile(request));
        }

        @Override
        public void onError(Throwable t) {
            Timber.e("Googlefit token error: %s", t.getMessage());
            mView.hideProgressBar();
            mView.showMessageDialog(mContext.getString(R.string.register_gobe_fitbit_gFit_error));
        }
    }

    private class GoogleFitUserProfileSubscriber extends DefaultSubscriber<GoogleFitUserProfileResponse> {
        @Override
        public void onNext(GoogleFitUserProfileResponse googleFitUserProfileResponse) {
            super.onNext(googleFitUserProfileResponse);
            Timber.d("Fitbit get user profile success: %s", googleFitUserProfileResponse.name);
            mGFitUserInfoModel = new GFitUserInfoModel();
            mGFitUserInfoModel.setUserInfoModel(googleFitUserProfileResponse);
            avatarUrl = googleFitUserProfileResponse.picture;
            mGoogleUseCase.execute(new DownloadImageSubscriber(), GoogleParam.Factory.downloadImage(googleFitUserProfileResponse.picture));
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            Timber.e("Google Fit user error: %s", t.getMessage());
            mView.hideProgressBar();
            mView.showMessageDialog(mContext.getString(R.string.register_gobe_fitbit_gFit_error));
        }
    }

    private class DownloadImageSubscriber extends DefaultSubscriber<ResponseBody> {
        @Override
        public void onNext(ResponseBody responseBody) {
            String timeStamp = new SimpleDateFormat(DAY_MONTH_YEAR_FORMAT).format(Calendar.getInstance().getTime());
            String fileName = ImageUtils.getFileNameFromURL(avatarUrl) + timeStamp;
            WriteFileAsync async = new WriteFileAsync(mContext, fileName, responseBody, new WriteFileAsync.OnDownloadCallback() {
                @Override
                public void onDownloadDone(String path) {
                    Timber.d("file name, %s", path);
                    mGFitUserInfoModel.setAvatarPath(path);
                    mRouter.openRegisterScreen(mGFitUserInfoModel);
                    mView.hideProgressBar();
                }

                @Override
                public void onDownloadFailure(Throwable throwable) {
                    Timber.d("Download failed");
                    mRouter.openRegisterScreen(mGFitUserInfoModel);
                    mView.hideProgressBar();
                }
            });
            async.execute();
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }
    }
}
