package com.genkimiru.app.presentation.main.contest_fragment.privates;

import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.contest_fragment.ContestListContract;

import java.util.List;

public interface PrivatesContestContract {

    interface PrivateContestView extends ListContract.RootView<List<GenkiContestModel>>{}
    interface PrivateContestPresenter extends ContestListContract.ListPresenter{}
    interface PrivateContestRouter{}

}
