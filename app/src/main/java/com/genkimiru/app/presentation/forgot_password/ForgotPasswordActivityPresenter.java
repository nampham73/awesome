package com.genkimiru.app.presentation.forgot_password;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;

import static com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivityContract.*;

public class ForgotPasswordActivityPresenter extends BasePresenterImp<ForgotPasswordView, ForgotPasswordRouter>
        implements ForgotPasswordPresenter {

    public ForgotPasswordActivityPresenter(Context context,
                                           ForgotPasswordView forgotPasswordView,
                                           ForgotPasswordRouter forgotPasswordRouter) {
        super(context, forgotPasswordView, forgotPasswordRouter);
    }
}
