package com.genkimiru.app.presentation.update_user_infor.hospital;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class UpdateHospitalActivityPresenter extends BasePresenterImp<UpdateHospitalActivityContract.UpdateHospitalView, UpdateHospitalActivityContract.UpdateHospitalRouter>
        implements UpdateHospitalActivityContract.UpdateHospitalPresenter {

    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    int mSurgeryStatus;

    public UpdateHospitalActivityPresenter(Context context, UpdateHospitalActivityContract.UpdateHospitalView updateHospitalView, UpdateHospitalActivityContract.UpdateHospitalRouter updateHospitalRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateHospitalView, updateHospitalRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;
    }

    @Override
    public void updateHospital(int surgeryStatus) {
        mSurgeryStatus = surgeryStatus;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest userInfoRequest = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setSurgeryStatus(surgeryStatus)
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();

        mUserUseCase.execute(new UpdateHospitalSubcriber(), (UserParam.Factory.updateHealthSourceSetting(userInfoRequest)));
    }

    private class UpdateHospitalSubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Updage Hospital Success");
            mGenkiUserInfoModel.setSurgeryStatus(mSurgeryStatus);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
