package com.genkimiru.app.presentation.nac.fragment.week;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ExpandableListView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.presentation.nac.fragment.NacFragmentAdapter;
import com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragmentContract.NacWeekRouter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragmentContract.*;

public class NacWeekFragment extends SimpleBaseFragment<NacWeekPresent> implements NacWeekView,NacWeekRouter  {
    @BindView(R.id.fragment_nac_week_expandable_listview)
    ExpandableListView mExpandableListView;

    NacFragmentAdapter mAdapter;
    List<String> listHeader;
    HashMap<String, List<String>> listItem;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        mAdapter = new NacFragmentAdapter(getActivity(), listHeader, listItem);
        mExpandableListView.setAdapter(mAdapter);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_nac_week;
    }

    @Override
    protected NacWeekPresent registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    private void setData() {
        listHeader = new ArrayList<>();
        listItem = new HashMap<String, List<String>>();

        listHeader.add("first_week");
        listHeader.add("second_week");
        listHeader.add("third_week");

        List<String> firstWeek = new ArrayList<>();
        firstWeek.add("666");
        firstWeek.add("666");
        firstWeek.add("666");

        List<String> secondWeek = new ArrayList<>();
        secondWeek.add("777");
        secondWeek.add("777");
        secondWeek.add("777");

        List<String> thirdWeek = new ArrayList<>();
        thirdWeek.add("888");
        thirdWeek.add("888");
        thirdWeek.add("999");

        listItem.put(listHeader.get(0), firstWeek);
        listItem.put(listHeader.get(1), secondWeek);
        listItem.put(listHeader.get(2), thirdWeek);

    }

    @Override
    public void onUpdateSuccess() {

    }

    @Override
    public void onUpdateError() {

    }
}
