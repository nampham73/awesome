package com.genkimiru.app.presentation.contest_detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseActivity;
import com.genkimiru.app.dialog.ChallengeJoinDialog;
import com.genkimiru.app.dialog.LoginDialog;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailPresenter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailRouter;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityContract.ContestDetailView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ContestDetailActivity extends BaseActivity<ContestDetailPresenter, ContestDetailView>
        implements ContestDetailRouter {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    @Inject
    ContestDetailView mContestDetailView;
    @Inject
    ContestDetailPresenter mPresenter;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_contest_detail;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    @Override
    protected ContestDetailPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected ContestDetailView registerView() {
        return mContestDetailView;
    }

    public void setToolbarTitle(String title) {
        mToolbarTitle.setText(title);
    }
}
