package com.genkimiru.app.presentation.setting_privacy;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface SettingPrivacyActivityContract {

    interface SettingPrivacyPresenter extends BasePresenter {
        void updatePrivacySetting(boolean isHealthChecked, boolean isAccountChecked);
    }

    interface SettingPrivacyView {
        void showMessageDialog(String message);
        void onUpdateFail();
        void onUpdateSuccess();
    }

    interface SettingPrivacyRouter {

    }
}
