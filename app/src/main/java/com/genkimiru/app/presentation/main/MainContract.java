package com.genkimiru.app.presentation.main;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;

public interface MainContract {

    interface MainPresenter extends BasePresenter {

    }

    interface MainView extends BaseView<MainPresenter> {
    }

    interface MainRouter {
    }
}
