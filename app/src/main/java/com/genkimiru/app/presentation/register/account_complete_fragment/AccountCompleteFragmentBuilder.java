package com.genkimiru.app.presentation.register.account_complete_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AccountCompleteFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = AccountCompleteFragmentModule.class)
    abstract AccountCompleteFragment accountCompleteFragment();
}
