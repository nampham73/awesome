package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.ListPresenterImp;
import com.genkimiru.app.data.remote.genki.request.ContentDetailRequest;
import com.genkimiru.app.domain.content.ContentParam;
import com.genkimiru.app.domain.content.ContentUseCase;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.DetailContentFeedPresenter;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.DetailContentFeedRouter;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.DetailContentFeedView;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.ArticleViewHolder;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;

import java.util.List;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.data.remote.genki.request.ContentDetailRequest.SortType.DESC;

public class DetailContentFeedFragmentPresenter
        extends ListPresenterImp<DetailContentFeedView, DetailContentFeedRouter>
        implements DetailContentFeedPresenter {
    private ContentUseCase mContentUseCase;
    private Bundle mBundle;

    public DetailContentFeedFragmentPresenter(Context context,
                                              DetailContentFeedView detailContentFeedView,
                                              DetailContentFeedRouter detailContentFeedRouter,
                                              ContentUseCase contentUseCase) {
        super(context, detailContentFeedView, detailContentFeedRouter);
        this.mContentUseCase = contentUseCase;
    }

    @Override
    public void start() {
        super.start();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int idArticle = mBundle.getInt(Constants.Extra.ARTICLE_ID);
        String typeArticle = mBundle.getString(Constants.Extra.ARTICLE_TYPE);
        ContentDetailRequest article = new ContentDetailRequest
                .Builder(token)
                .setId(idArticle)
                .setType(typeArticle)
                .create();
        ContentDetailRequest feed = new ContentDetailRequest
                .Builder(token)
                .setArticleId(idArticle)
                .setLimit(getPostPerPage())
                .setOffset(getNextPage())
                .setSortType(DESC)
                .create();
        mView.onPrepareLoadingData();
        mContentUseCase.execute(new GetArticleAndFeedSubscriber(), ContentParam.Factory.getDetailArticle(article, feed));
    }

    @Override
    public void setArguments(Bundle bundle) {
        if (bundle == null) mBundle = new Bundle();
        mBundle = bundle;
    }

    @Override
    public void requestLikeOrUnlike(RecyclerView.ViewHolder holder) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int idArticle = mBundle.getInt(Constants.Extra.ARTICLE_ID);
        ContentDetailRequest request = new ContentDetailRequest
                .Builder(token)
                .setArticleId(idArticle)
                .create();
        if (holder instanceof ArticleViewHolder) {
            boolean isCurrentState = ((ArticleViewHolder) holder).getModel().getStatusLike();
            if (!isCurrentState) {
                mContentUseCase.execute(new RequestLikeSubscriber(holder), ContentParam.Factory.requestLike(request));
            } else {
                mContentUseCase.execute(new RequestUnlikeSubscriber(holder), ContentParam.Factory.requestUnLike(request));
            }
        }
    }

    @Override
    public void requestShareLink() {

    }

    @Override
    public void requestAddComment(String message, RecyclerView.ViewHolder holder) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int idArticle = mBundle.getInt(Constants.Extra.ARTICLE_ID);
        ContentDetailRequest request = new ContentDetailRequest.Builder(token)
                .setSortType("desc")
                .setArticleId(idArticle)
                .setComment(message)
                .create();
        mContentUseCase.execute(new RequestAddCommentSubscriber(holder), ContentParam.Factory.requestAddComment(request));
    }

    private class GetArticleAndFeedSubscriber extends DefaultSubscriber<List<IDetailContentModel>> {
        @Override
        public void onNext(List<IDetailContentModel> iDetailContentModels) {
            super.onNext(iDetailContentModels);
            mView.onDataLoadDone(iDetailContentModels);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            mView.onLoadingDataFailure(t, R.string.response_contest_Page_loading_data_failure);
        }
    }

    private class RequestLikeSubscriber extends DefaultSubscriber<Boolean> {
        private RecyclerView.ViewHolder holder;

        RequestLikeSubscriber(RecyclerView.ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onNext(Boolean aBoolean) {
            super.onNext(aBoolean);
            mRouter.onUpdateLikeView(holder, aBoolean);
        }

        @Override
        public void onError(Throwable t) {
            mRouter.onUpdateLikeView(holder, false);
        }
    }

    private class RequestUnlikeSubscriber extends DefaultSubscriber<Boolean> {
        RecyclerView.ViewHolder holder;

        private RequestUnlikeSubscriber(RecyclerView.ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onNext(Boolean aBoolean) {
            super.onNext(aBoolean);
            mRouter.onUpdateLikeView(holder, aBoolean);
        }

        @Override
        public void onError(Throwable t) {
            mRouter.onUpdateLikeView(holder, true);
        }
    }

    private class RequestAddCommentSubscriber extends DefaultSubscriber<Boolean> {
        private RecyclerView.ViewHolder holder;

        RequestAddCommentSubscriber(RecyclerView.ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onNext(Boolean aBoolean) {
            super.onNext(aBoolean);
            mRouter.onUpdateCommentView(holder);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }
    }
}