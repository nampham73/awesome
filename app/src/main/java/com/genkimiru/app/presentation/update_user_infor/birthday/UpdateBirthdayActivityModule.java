package com.genkimiru.app.presentation.update_user_infor.birthday;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayPresenter;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayRouter;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayView;

@Module
public class UpdateBirthdayActivityModule {

    @Provides
    @ActivityScope
    UpdateBirthdayView providesView(UpdateBirthDayActivity birthDayActivity) {
        return birthDayActivity;
    }

    @Provides
    @ActivityScope
    UpdateBirthdayRouter providesRouter(UpdateBirthDayActivity birthDayActivity) {
        return birthDayActivity;
    }
    @Provides
    @ActivityScope
    UpdateBirthdayPresenter providesPrenenter(Context context, UpdateBirthdayView birthdayView, UpdateBirthdayRouter birthdayRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        return new UpdateBirthdayActivityPresenter(context, birthdayView, birthdayRouter, userUseCase, genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserUseCase providesUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserInfoRequest provideUserInforRequest(GenkiUserInfoModel genkiUserInfoModel) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return new UserInfoRequest(token, genkiUserInfoModel.getHealthSource());
    }

}
