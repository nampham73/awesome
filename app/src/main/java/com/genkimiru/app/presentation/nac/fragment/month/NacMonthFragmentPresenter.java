package com.genkimiru.app.presentation.nac.fragment.month;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;

import static com.genkimiru.app.presentation.nac.fragment.month.NacMonthFragmentContract.*;

public class NacMonthFragmentPresenter extends BasePresenterImp<NacMonthView,NacMonthRouter> implements NacMonthPresenter{

   UserUseCase mUserUseCase;
    public NacMonthFragmentPresenter(Context context, NacMonthView nacMonthView, NacMonthRouter nacMonthRouter,UserUseCase userUseCase) {
        super(context, nacMonthView, nacMonthRouter);
        this.mUserUseCase = userUseCase;
    }

    @Override
    public void onUpdateNacMonth() {

    }
}
