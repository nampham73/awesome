package com.genkimiru.app.presentation.register.smorking_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingFragmentRouter;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingFragmentView;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class SmokingFragmentModule {

    @Provides
    @FragmentScope
    SmokingFragmentView provideView(SmokingFragment smokingFragment) {
        return smokingFragment;
    }

    @Provides
    @FragmentScope
    SmokingFragmentRouter provideRouter(SmokingFragment smokingFragment) {
        return smokingFragment;
    }

    @Provides
    @FragmentScope
    SmokingPresenter providePresenter(Context context, SmokingFragmentView smokingFragmentView,
                                      SmokingFragmentRouter smokingFragmentRouter, UserUseCase userUseCase) {
        return new SmokingFragmentPresenter(context, smokingFragmentView, smokingFragmentRouter, userUseCase);
    }
}
