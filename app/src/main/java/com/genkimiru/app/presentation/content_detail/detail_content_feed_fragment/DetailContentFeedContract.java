package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;

import java.util.List;

public interface DetailContentFeedContract {
    interface DetailContentFeedView extends ListContract.RootView<List<IDetailContentModel>>{}
    interface DetailContentFeedPresenter extends ListContract.ListPresenter{
        void setArguments(Bundle bundle);
        void requestLikeOrUnlike(RecyclerView.ViewHolder holder);
        void requestAddComment(String message, RecyclerView.ViewHolder holder);
        void requestShareLink();
    }
    interface DetailContentFeedRouter{
        void onUpdateLikeView(RecyclerView.ViewHolder holder, boolean status);
        void onUpdateCommentView(RecyclerView.ViewHolder holder);
    }
}
