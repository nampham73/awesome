package com.genkimiru.app.presentation.other_wear_device;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.google.GoogleApi;
import com.genkimiru.app.data.repository.googlefit.GoogleFitRepository;
import com.genkimiru.app.data.repository.googlefit.GoogleFitRepositoryImp;
import com.genkimiru.app.domain.google.GoogleUseCase;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDevicePresenter;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDeviceRouter;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityContract.OtherWearDeviceView;

import dagger.Module;
import dagger.Provides;

@Module
public class OtherWearDeviceActivityModule {

    @Provides
    @ActivityScope
    OtherWearDeviceView provideView(OtherWearDeviceActivity otherWearDeviceActivity) {
        return otherWearDeviceActivity;
    }

    @Provides
    @ActivityScope
    OtherWearDeviceRouter provideRouter(OtherWearDeviceActivity otherWearDeviceActivity) {
        return otherWearDeviceActivity;
    }

    @Provides
    @ActivityScope
    OtherWearDevicePresenter providePresenter(Context context,
                                              OtherWearDeviceView otherWearDeviceView,
                                              OtherWearDeviceRouter otherWearDeviceRouter,
                                              GoogleUseCase googleUseCase,
                                              RegisterUserRequest registerUserRequest) {
        return new OtherWearDeviceActivityPresenter(context, otherWearDeviceView, otherWearDeviceRouter, googleUseCase, registerUserRequest);
    }

    @Provides
    @ActivityScope
    GoogleFitRepository provideRepository(GoogleApi api) {
        return new GoogleFitRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    GoogleUseCase provideUseCase(SchedulerProvider schedulerProvider, GoogleFitRepository googleFitRepository) {
        return new GoogleUseCase(schedulerProvider.io(), schedulerProvider.ui(), googleFitRepository);
    }

    @Provides
    @ActivityScope
    RegisterUserRequest provideRegisterUserRequest() {
        return new RegisterUserRequest();
    }
}
