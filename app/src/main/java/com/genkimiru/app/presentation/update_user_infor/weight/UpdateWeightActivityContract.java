package com.genkimiru.app.presentation.update_user_infor.weight;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateWeightActivityContract {
    interface UpdateWeightPresenter extends BasePresenter {
        void updateWeight(float weight);
        String validateWeight(String weight);
    }

    interface UpdateWeightView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateWeightRouter {

    }
}
