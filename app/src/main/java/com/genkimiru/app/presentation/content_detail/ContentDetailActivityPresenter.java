package com.genkimiru.app.presentation.content_detail;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.*;

public class ContentDetailActivityPresenter
        extends BasePresenterImp<ContentDetailView,ContentDetailRouter>
        implements ContentDetailPresenter{

    public ContentDetailActivityPresenter(Context context,
                                          ContentDetailView contentDetailView,
                                          ContentDetailRouter contentDetailRouter) {
        super(context, contentDetailView, contentDetailRouter);
    }
}
