package com.genkimiru.app.presentation.register.wear_device_fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.dialog.LoginDialog;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDeviceFragmentRouter;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDeviceFragmentView;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDevicePresenter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Google.CLIENT_ID;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GOOGLE_FIT_LOGIN;

public class WearDeviceFragment extends SimpleBaseFragment<WearDevicePresenter>
        implements WearDeviceFragmentView, WearDeviceFragmentRouter {

    public static final int GOOGLEFIT_SIGN_IN = 1150;

    @Inject
    WearDevicePresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    private LoginDialog loginDialog;
    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_smartphone_device_tv)
    TextView mTvRegisterSmartPhoneView;
    @BindView(R.id.register_other_device_tv)
    TextView mTvOtherDevice;
    @BindView(R.id.register_fitbit_wear_device_ib)
    ImageButton mTvRegisterFitBitView;
    @BindView(R.id.register_gobe_wear_device_ib)
    ImageButton mTvRegisterGoBeView;

    private SparseArray<View> mLoginViews;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoginViews = new SparseArray<>();
        mLoginViews.put(mTvRegisterSmartPhoneView.getId(), mTvRegisterSmartPhoneView);
        mLoginViews.put(mTvRegisterFitBitView.getId(), mTvRegisterFitBitView);
        mLoginViews.put(mTvRegisterGoBeView.getId(), mTvRegisterGoBeView);
        mLoginViews.put(mTvOtherDevice.getId(), mTvOtherDevice);
        mPresenter.requestDefaultHighLightWearDevice();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(CLIENT_ID)
                .requestServerAuthCode(CLIENT_ID)
                .requestEmail()
                .requestScopes(new Scope(Scopes.FITNESS_BODY_READ))
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(mRegisterActivity, gso);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_wear_device;
    }

    @Override
    protected WearDevicePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    private void openGobeLoginDialog() {
        Timber.d("login gobe");
        int currentHealthSource = mGenkiUserInfoModel.getHealthSource();
        if (currentHealthSource == Constants.HealthSource.GOBE) {
            Timber.d("Current health source is: %d, we skip change now", currentHealthSource);
            return;
        }
        mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.GOBE.getValue();
        loginDialog = new LoginDialog.LoginDialogBuilder()
                .setTitle(getString(R.string.register_login_gobe_title))
                .dialogListener(new LoginDialog.OnResultListener() {
                    @Override
                    public void onLogin(String username, String password, LoginDialog loginDialog) {
                        Timber.d("Login Gobe-  Username: %s - Password: %s", username, password);
                        mRegisterActivity.loginGobe(username, password);
                        loginDialog.dismiss();
                    }

                    @Override
                    public void onCancel(LoginDialog loginDialog) {
                        loginDialog.dismiss();
                    }
                }).build();
        loginDialog.show(mRegisterActivity.getSupportFragmentManager());
    }

    @OnClick(R.id.register_gobe_wear_device_ib)
    void showGobeLoginDialog() {
        openGobeLoginDialog();
    }

    @OnClick(R.id.register_fitbit_wear_device_ib)
    void loginWithFitbit() {
        loginFitbit();
    }

    @OnClick(R.id.register_other_device_tv)
    void openOtherDeviceScreen() {
        openOtherDevice();
    }

    @OnClick(R.id.register_smartphone_device_tv)
    void loginWithSmartPhone() {
        loginSmartPhone();
    }

    private void openOtherDevice() {
        Timber.d("openOtherDeviceScreen");
        int currentHealthSource = mGenkiUserInfoModel.getHealthSource();
        if (currentHealthSource == Constants.HealthSource.GOOGLE_FIT) {
            Timber.d("Current health source is: %d, we skip change now", currentHealthSource);
            return;
        }
        mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.GOOGLE_FIT.getValue();

        if (getActivity() instanceof RegisterActivity) {
            ((RegisterActivity) getActivity()).sendActivityForResult();
        }
    }

    private void loginFitbit() {
        mRegisterUserRequest.mIsLoginFlag = false;
        int currentHealthSource = mGenkiUserInfoModel.getHealthSource();
        if (currentHealthSource == Constants.HealthSource.FITBIT) {
            Timber.d("Current health source is: %d, we skip change now", currentHealthSource);
            return;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.Fitbit.AUTH_URI));
        startActivity(browserIntent);
    }

    private void loginSmartPhone() {
        mRegisterUserRequest.mIsLoginFlag = false;
        int currentHealthSource = mGenkiUserInfoModel.getHealthSource();
        if (currentHealthSource == Constants.HealthSource.SMART_PHONE) {
            Timber.d("Current health source is: %d, we skip change now", currentHealthSource);
            return;
        }
        mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.SMARTPHONE.getValue();
        signIn();
    }

    @Override
    public void onHighLightWearDevice(@IdRes int id) {
        selected(id);
        mRegisterActivity.forceHideProgress();
    }


    private void deSelectedLoginViews() {
        if (mLoginViews != null && mLoginViews.size() > 0) {
            int total = mLoginViews.size();
            for (int i = 0; i < total; i++) {
                int key = mLoginViews.keyAt(i);
                View view = mLoginViews.get(key);
                if (view != null) view.setSelected(false);
            }
        }
    }

    private void selected(@IdRes int id) {
        deSelectedLoginViews();
        View selView = mLoginViews.get(id);
        if (selView != null) selView.setSelected(true);
    }

    public void requestHighLightLoginType(int healthSource) {
        mPresenter.requestHighLightWearDevice(healthSource);
    }

    private void signIn() {
        mGoogleSignInClient.signOut();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mRegisterActivity.startActivityForResult(signInIntent, REQUEST_CODE_GOOGLE_FIT_LOGIN);
    }

}
