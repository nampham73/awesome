package com.genkimiru.app.presentation.setting_email_marketing;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingPresenter;
import static com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingRouter;
import static com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingView;

@Module
public class SettingEmailMarketingActivityModule {

    @Provides
    @ActivityScope
    SettingEmailMarketingView providesEmailMarketingView(SettingEmailMarketingActivity settingEmailMarketingActivity) {
        return settingEmailMarketingActivity;
    }

    @Provides
    @ActivityScope
    SettingEmailMarketingRouter providesEmailMarketingRouter(SettingEmailMarketingActivity settingEmailMarketingActivity) {
        return settingEmailMarketingActivity;
    }

    @Provides
    @ActivityScope
    SettingEmailMarketingPresenter providesEmailMaketingPresenter(Context context, SettingEmailMarketingView settingEmailMarketingView, SettingEmailMarketingRouter settingEmailMarketingRouter, UserUseCase userUseCase) {
        return new SettingEmailMarketingActivityPresenter(context, settingEmailMarketingView, settingEmailMarketingRouter, userUseCase);
    }

    @Provides
    @ActivityScope
    UserRepository providesUserRespository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
