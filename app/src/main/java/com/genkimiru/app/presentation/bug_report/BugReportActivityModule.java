package com.genkimiru.app.presentation.bug_report;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.extension.ExtensionApi;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.bugreport.BugReportRepository;
import com.genkimiru.app.data.repository.bugreport.BugReportRepositoryImp;
import com.genkimiru.app.domain.bugreport.BugReportUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.bug_report.BugReportActivityContract.*;

@Module
public class BugReportActivityModule {

    @Provides
    @ActivityScope
    BugReportView provideView(BugReportActivity bugReportActivity) {
        return bugReportActivity;
    }

    @Provides
    @ActivityScope
    BugReportRouter provideRouter(BugReportActivity bugReportActivity) {
        return bugReportActivity;
    }

    @Provides
    @ActivityScope
    BugReportPresenter providePresenter(Context context,
                                        BugReportView view,
                                        BugReportRouter router,
                                        BugReportUseCase bugReportUseCase) {
        return new BugReportActivityPresenter(context, view, router, bugReportUseCase);
    }

    @Provides
    @ActivityScope
    BugReportRepository provideBugReportRepository(ExtensionApi api) {
        return new BugReportRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    BugReportUseCase provideBugReportUseCase(SchedulerProvider schedulerProvider, BugReportRepository bugReportRepository) {
        return new BugReportUseCase(schedulerProvider.io(), schedulerProvider.ui(), bugReportRepository);
    }
}
