package com.genkimiru.app.presentation.eula;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface EulaContract {

    interface EulaPresenter extends BasePresenter{

    }
    interface EulaView{
        void setScreenTitle(String title);
    }
    interface EulaRouter{

    }
}
