package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

public interface IDetailContentModel {
    int getId();
    String getName();
    int getViewType();
}
