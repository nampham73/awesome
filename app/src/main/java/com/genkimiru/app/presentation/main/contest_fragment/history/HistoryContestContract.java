package com.genkimiru.app.presentation.main.contest_fragment.history;

import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.contest_fragment.ContestListContract;

import java.util.List;

public interface HistoryContestContract {
    interface HistoryContestPresenter extends ContestListContract.ListPresenter {
    }

    interface HistoryContestRootView extends ListContract.RootView<List<GenkiContestModel>> {
    }

    interface HistoryContestRouter {
    }
}
