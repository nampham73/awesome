package com.genkimiru.app.presentation.register.heart_beat_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatFragmentRouter;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatFragmentView;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentContract.HeartBeatPresenter;

public class HeartBeatFragmentPresenter extends BasePresenterImp<HeartBeatFragmentView
        , HeartBeatFragmentRouter> implements HeartBeatPresenter {

    private UserUseCase mUserUseCase;

    public HeartBeatFragmentPresenter(Context context, HeartBeatFragmentView heartBeatFragmentView,
                                      HeartBeatFragmentRouter heartBeatFragmentRouter, UserUseCase userUseCase) {
        super(context, heartBeatFragmentView, heartBeatFragmentRouter);
        mUserUseCase = userUseCase;
    }
}
