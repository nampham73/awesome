package com.genkimiru.app.presentation.nac.fragment.day;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.NacDayPresenter;
import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.NacDayRouter;
import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.NacDayView;

@Module
public class NacDayFragmentModule {

    @Provides
    @FragmentScope
    NacDayView provideDayView(NacDayFragment nacDayFragment) {
        return nacDayFragment;
    }

    @Provides
    @FragmentScope
    NacDayRouter provideDayRouter(NacDayFragment nacDayFragment) {
        return nacDayFragment;
    }

    @Provides
    @FragmentScope
    NacDayPresenter provideDayPresenter(Context context, NacDayView nacDayView, NacDayRouter nacDayRouter, UserUseCase userUseCase) {
        return new NacDayFragmentPresenter(context, nacDayView, nacDayRouter, userUseCase);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
