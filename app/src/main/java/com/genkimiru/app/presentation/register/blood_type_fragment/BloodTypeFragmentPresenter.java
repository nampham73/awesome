package com.genkimiru.app.presentation.register.blood_type_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypeFragmentRouter;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.BloodTypeFragmentView;

import static com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentContract.*;

public class BloodTypeFragmentPresenter extends BasePresenterImp<BloodTypeFragmentView,
        BloodTypeFragmentRouter> implements BloodTypePresenter {

    private UserUseCase mUserUseCase;

    public BloodTypeFragmentPresenter(Context context, BloodTypeFragmentView bloodTypeFragmentView,
                                      BloodTypeFragmentRouter bloodTypeFragmentRouter, UserUseCase userUseCase) {
        super(context, bloodTypeFragmentView, bloodTypeFragmentRouter);
        this.mUserUseCase = userUseCase;
    }
}
