package com.genkimiru.app.presentation.register.weight_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class WeightFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = WeightFragmentModule.class)
    abstract WeightFragment weightFragment();
}
