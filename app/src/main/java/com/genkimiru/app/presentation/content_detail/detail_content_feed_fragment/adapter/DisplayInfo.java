package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class DisplayInfo {
    private int screen_height = 0, screen_width = 0;

    DisplayInfo(Context context) {
        getdisplayheightWidth(context);
    }

    private void getdisplayheightWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (wm == null) return;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        screen_height = displaymetrics.heightPixels;
        screen_width = displaymetrics.widthPixels;
    }

    public int getScreenHeight() {
        return screen_height;
    }

    public int getScreenWidth() {
        return screen_width;
    }
}
