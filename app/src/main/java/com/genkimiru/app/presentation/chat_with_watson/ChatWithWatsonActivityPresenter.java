package com.genkimiru.app.presentation.chat_with_watson;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import com.genkimiru.app.Constants;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.WatsonMessageModel;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonPresenter;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonRouter;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonView;
import com.ibm.watson.developer_cloud.conversation.v1.Conversation;
import com.ibm.watson.developer_cloud.conversation.v1.model.InputData;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;

import java.util.ArrayList;
import java.util.Locale;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.genkimiru.app.Constants.Watson.DATE_CREATED;
import static com.genkimiru.app.data.model.WatsonMessageModel.MessageType.MESSAGE_BOT;
import static com.genkimiru.app.data.model.WatsonMessageModel.MessageType.MESSAGE_SELF;

public class ChatWithWatsonActivityPresenter extends BasePresenterImp<ChatWithWatsonView, ChatWithWatsonRouter>
        implements ChatWithWatsonPresenter {

    private com.ibm.watson.developer_cloud.conversation.v1.model.Context context;
    private ArrayList<WatsonMessageModel> messageArrayList = new ArrayList<>();
    private Disposable disposable;
    private TextToSpeech textToSpeech;

    public ChatWithWatsonActivityPresenter(Context context, ChatWithWatsonView chatWithWatsonView, ChatWithWatsonRouter chatWithWatsonRouter) {
        super(context, chatWithWatsonView, chatWithWatsonRouter);
    }

    @Override
    public void requestMessage(String message) {
        WatsonMessageModel inputMessage = new WatsonMessageModel();
        if (message.length() > 0) {
            inputMessage.setId(MESSAGE_SELF);
            mView.onSuccess(inputMessage);
        }
        inputMessage.setMessage(message);
        disposable = Flowable.just(message)
                .subscribeOn(Schedulers.newThread())
                .map(this::getResponseMessage)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    mView.onSuccess(data);
                    disposable.dispose();
                }, throwable -> mView.onError(throwable));
    }

    @Override
    public void pauseTextToSpeech() {
        if (textToSpeech == null) {
            return;
        }
        textToSpeech.stop();
        textToSpeech.shutdown();
    }

    private WatsonMessageModel getResponseMessage(String message) {
        Conversation service = new Conversation(DATE_CREATED);
        service.setUsernameAndPassword(Constants.Watson.USERNAME, Constants.Watson.PASSWORD);
        InputData input = new InputData.Builder(message).build();
        MessageOptions options = new MessageOptions.Builder(Constants.Watson.WORKSPACE_ID).input(input).context(context).build();
        MessageResponse response = service.message(options).execute();

        WatsonMessageModel outMessage = new WatsonMessageModel();
        if (response != null) {
            if (response.getContext() != null) context = response.getContext();
            if (response.getOutput() != null && response.getOutput().containsKey("text")) {
                ArrayList responseList = (ArrayList) response.getOutput().get("text");
                if (null != responseList && responseList.size() > 0) {
                    outMessage.setMessage((String) responseList.get(0));
                    outMessage.setId(MESSAGE_BOT);
                    textToSpeech(outMessage.getMessage());
                }
                messageArrayList.add(outMessage);
            }
        }
        return outMessage;
    }

    private void textToSpeech(String message) {
        textToSpeech = new TextToSpeech(mContext, status -> {
            textToSpeech.setLanguage(Locale.JAPAN);
            textToSpeech.speak(message, TextToSpeech.QUEUE_FLUSH, null, null);
        });
    }
}
