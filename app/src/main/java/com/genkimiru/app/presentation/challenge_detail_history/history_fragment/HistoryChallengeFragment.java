package com.genkimiru.app.presentation.challenge_detail_history.history_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.genkimiru.app.base.fragment.ListFragment;
import com.genkimiru.app.base.widget.RecyclerViewPaginationListener;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.challenge_detail_history.ChallengeDetailHistoryActivity;
import com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengePresenter;
import com.genkimiru.app.presentation.challenge_detail_history.history_fragment.adapter.HistoryChallengeFragmentAdapter;

import java.util.List;

import javax.inject.Inject;

import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengeRouter;
import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengeView;

public class HistoryChallengeFragment extends ListFragment<HistoryChallengeFragmentAdapter, List<GenkiContestModel>, HistoryChallengePresenter>
        implements HistoryChallengeView, HistoryChallengeRouter {

    @Inject
    HistoryChallengePresenter mHistoryChallengePresenter;
    @Inject
    HistoryChallengeFragmentAdapter mAdapter;

    private ChallengeDetailHistoryActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ChallengeDetailHistoryActivity) getActivity();
    }

    @NonNull
    @Override
    protected HistoryChallengeFragmentAdapter createAdapter() {
        return mAdapter;
    }

    @Override
    protected HistoryChallengePresenter registerPresenter() {
        return mHistoryChallengePresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHistoryChallengePresenter.start();
    }

    @Override
    public void onDataLoadDone(List<GenkiContestModel> genkiContestModels) {
        super.onDataLoadDone(genkiContestModels);
        if (mAdapter != null) {
            mAdapter.set(genkiContestModels);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void initRecyclerView(RecyclerView recyclerView) {
        super.initRecyclerView(recyclerView);

        recyclerView.addOnScrollListener(new RecyclerViewPaginationListener(recyclerView.getLayoutManager()) {
            @Override
            protected void loadMore() {
                mHistoryChallengePresenter.requestLoadingMore();
            }
        });
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected HistoryChallengePresenter getPresenter() {
        return mHistoryChallengePresenter;
    }
}
