package com.genkimiru.app.presentation.water;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.dialog.InputWaterDialog;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterPresenter;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterRouter;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class WaterActivity extends SimpleBaseActivity<WaterPresenter> implements WaterRouter, WaterView {
    @BindView(R.id.water_activity_water_number_txt)
    TextView mWaterTxt;
    @BindView(R.id.water_activity_water_percent_txt)
    TextView mWaterPercentTxt;
    @BindView(R.id.water_activity_water_level_change_txt)
    TextView mWaterLvTxt;

    @Inject
    WaterPresenter mWaterPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_water;
    }

    @Override
    protected WaterPresenter registerPresenter() {
        return mWaterPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @OnClick(R.id.water_decrease_btn)
    public void decreaseWater(View view) {
        float curent;
        float percent;
        try {
            curent = (Float.parseFloat(mWaterTxt.getText().toString()) * 1000 - Integer.parseInt(mWaterLvTxt.getText().toString())) / 1000;
            if (curent <= 0) {
                curent = 0;
            }
            mWaterTxt.setText(String.valueOf(curent));
            percent = curent / 2 * 100;
            if (percent > 100) {
                mWaterPercentTxt.setText(R.string.water_percent_title);
            } else {
                mWaterPercentTxt.setText(String.format(getString(R.string.water_percent), (int)percent) + "%");
            }
        } catch (NumberFormatException ex) {
            Log.e("NumberFormatException", ex.getMessage());
        }

    }

    @OnClick(R.id.water_increase_btn)
    public void increaseWater(View view) {
        float curent;
        float percent;
        try {
            if (view.getId() != R.id.dialog_input_water_level_edt) {
                curent = (Float.parseFloat(mWaterTxt.getText().toString()) * 1000 + Integer.parseInt(mWaterLvTxt.getText().toString())) / 1000;

            } else {
                EditText mWaterInput = (EditText) view;
                curent = (Float.parseFloat(mWaterTxt.getText().toString()) * 1000 + Integer.parseInt(mWaterInput.getText().toString())) / 1000;
            }
            mWaterTxt.setText(Float.toString(curent));
            percent = curent / 2 * 100;
            if (percent > 100) {
                mWaterPercentTxt.setText(R.string.water_percent_title);
            } else {
                mWaterPercentTxt.setText(String.format(getString(R.string.water_percent), (int)percent) + "%");
            }
        } catch (NumberFormatException ex) {
            Log.e("NumberFormatException", ex.getMessage());
        }
    }

    @OnClick(R.id.water_level_change_txt)
    public void showInputWaterDialog(View view) {
        InputWaterDialog waterDialog = new InputWaterDialog.InputWaterDialogBuilder().dialogListener(new InputWaterDialog.OnResultListener() {
            @Override
            public void onAdd(View view, InputWaterDialog inputWaterDialog) {
                increaseWater(view);
                inputWaterDialog.dismiss();
            }

            @Override
            public void onCancel(InputWaterDialog inputWaterDialog) {
                inputWaterDialog.dismiss();
            }
        }).buider();
        waterDialog.show(getSupportFragmentManager());
    }


}
