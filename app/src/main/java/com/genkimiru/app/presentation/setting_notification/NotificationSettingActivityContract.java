package com.genkimiru.app.presentation.setting_notification;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NotificationSettingActivityContract {

    interface NotificationSettingPresenter extends BasePresenter {
        void updateNotificationSetting(boolean isAnnouncementChecked,
                                       boolean isAdviceChecked,
                                       boolean isContestChecked,
                                       boolean isAcceptPush);
    }

    interface NotificationSettingView {
        void showMessageDialog(String message);
        void onUpdateFail();
        void onUpdateSuccess();
    }

    interface NotificationSettingRouter {

    }
}
