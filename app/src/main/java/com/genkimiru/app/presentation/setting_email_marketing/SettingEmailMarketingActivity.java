package com.genkimiru.app.presentation.setting_email_marketing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Switch;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingPresenter;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingRouter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class SettingEmailMarketingActivity extends SimpleBaseActivity<SettingEmailMarketingPresenter> implements SettingEmailMarketingActivityContract.SettingEmailMarketingView, SettingEmailMarketingRouter {
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.activity_setting_email)
    TextView mSettingEmail;
    @BindView(R.id.activity_setting_email_marketting_switch)
    Switch mReceiveEmailBtn;

    private CompositeDisposable mCompositeDisposable;

    @Inject
    SettingEmailMarketingPresenter mSettingEmailMarketingPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("email marketing : %s", mGenkiUserInfoModel.isEmailMarketingSettingFlag());
        mSettingEmail.setText(mGenkiUserInfoModel.getEmail());
        mReceiveEmailBtn.setChecked(mGenkiUserInfoModel.isEmailMarketingSettingFlag());
        initializeEventHandler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_setting_email_marketing;
    }

    @Override
    protected SettingEmailMarketingPresenter registerPresenter() {
        return mSettingEmailMarketingPresenter ;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.setting_email_marketing_title);
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(RxCompoundButton.checkedChanges(mReceiveEmailBtn)
                .skip(1)
                .subscribe(isChecked -> {
                    showProgress();
                    mSettingEmailMarketingPresenter.updateEmailMarketingSetting(isChecked);
                }));
    }


    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message);
    }

    @Override
    public void onUpdateFail() {
        hideProgress();
        mReceiveEmailBtn.setChecked(mGenkiUserInfoModel.isEmailMarketingSettingFlag());
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        MessageUtil.showShortToast(getApplicationContext(), getString(R.string.setting_email_marketing_success));
        mGenkiUserInfoModel.setEmailMarketingSettingFlag(mReceiveEmailBtn.isChecked());
    }
}
