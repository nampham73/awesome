package com.genkimiru.app.presentation.update_user_infor.birthday;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateBirthdayActivityContract {

    interface  UpdateBirthdayPresenter extends BasePresenter{
        void updateBirthday(String dataBirthday);
    }
    interface UpdateBirthdayView {
        void onUpdateSuccess();
        void onUpdateError(String message);

    }
    interface  UpdateBirthdayRouter{

    }
}
