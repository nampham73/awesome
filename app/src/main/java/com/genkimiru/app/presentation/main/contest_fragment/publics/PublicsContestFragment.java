package com.genkimiru.app.presentation.main.contest_fragment.publics;

import android.content.Context;

import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.contest_fragment.ContestsListFragment;
import com.genkimiru.app.presentation.main.contest_fragment.publics.PublicContestContract.PublicContestPresenter;

import javax.inject.Inject;

public class PublicsContestFragment extends ContestsListFragment<PublicContestPresenter>
        implements PublicContestContract.PublicContestView,
        PublicContestContract.PublicContestRouter{

    @Inject PublicContestPresenter mPresenter;

    private MainActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    protected PublicContestPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected PublicContestPresenter getPresenter() {
        return mPresenter;
    }
}
