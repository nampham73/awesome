package com.genkimiru.app.presentation.register.account_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AccountFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = AccountFragmentModule.class)
    abstract AccountFragment accountFragment();
}
