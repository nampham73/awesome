package com.genkimiru.app.presentation.bug_report.adapter;

import android.net.Uri;

public class BugImage {

    private Uri mImageUri;
    private String mTitle;
    private ImageType mImageType;

    public BugImage(Uri imageUri, ImageType imageType, String title) {
        mImageUri = imageUri;
        mImageType = imageType;
        mTitle = title;
    }

    public Uri getImageUri() {
        return mImageUri;
    }

    public void setImageUri(Uri imageUri) {
        mImageUri = imageUri;
    }

    public ImageType getImageType() {
        return mImageType;
    }

    public void setImageType(ImageType mImageType) {
        this.mImageType = mImageType;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public enum ImageType {
        BUG_IMAGE,
        ADD_IMAGE
    }
}
