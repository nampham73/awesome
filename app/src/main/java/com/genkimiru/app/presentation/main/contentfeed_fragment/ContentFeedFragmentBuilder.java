package com.genkimiru.app.presentation.main.contentfeed_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ContentFeedFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = ContentFeedFragmentModule.class)
    abstract ContentFeedFragment contentFeedFragment();
}
