package com.genkimiru.app.presentation.main;

import android.os.Bundle;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseActivity;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.service.stepcounter.StepCounterService;
import com.genkimiru.app.widget.WheelView;

import javax.inject.Inject;

import timber.log.Timber;

public class MainActivity extends BaseActivity<MainContract.MainPresenter, MainContract.MainView>
        implements MainContract.MainRouter, WheelView.OnWheelViewListener {

    boolean mIsShowMenuItem;

    @Inject
    MainContract.MainView mMainView;
    @Inject
    MainContract.MainPresenter mMainPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mGenkiUserInfoModel.getHealthSource() == Constants.HealthSource.SMART_PHONE) {
            StepCounterService.Start(getApplicationContext());
        } else {
            StepCounterService.Stop(getApplicationContext());
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected MainContract.MainPresenter registerPresenter() {
        return mMainPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected MainContract.MainView registerView() {
        return mMainView;
    }

    @Override
    public void onWheelViewItemSelected(int selectedIndex, WheelView.Result item) {
        Timber.d("position of wheelview: %s", selectedIndex);
    }
}
