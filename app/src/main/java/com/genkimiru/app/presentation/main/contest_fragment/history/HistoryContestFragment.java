package com.genkimiru.app.presentation.main.contest_fragment.history;

import android.content.Context;

import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.contest_fragment.ContestsListFragment;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestContract.HistoryContestPresenter;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestContract.HistoryContestRootView;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestContract.HistoryContestRouter;

import javax.inject.Inject;

public class HistoryContestFragment extends ContestsListFragment<HistoryContestPresenter>
        implements HistoryContestRouter,HistoryContestRootView {

    @Inject HistoryContestPresenter mPresenter;

    private MainActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    protected HistoryContestPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected HistoryContestPresenter getPresenter() {
        return mPresenter;
    }
}
