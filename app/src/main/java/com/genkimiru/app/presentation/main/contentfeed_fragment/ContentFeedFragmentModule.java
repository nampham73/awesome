package com.genkimiru.app.presentation.main.contentfeed_fragment;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.content.ContentRepository;
import com.genkimiru.app.data.repository.content.ContentRepositoryImp;
import com.genkimiru.app.domain.content.ContentUseCase;
import com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentContract.*;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentContract.*;

@Module
public class ContentFeedFragmentModule {

    @FragmentScope
    @Provides
    ContentFeedView provideView(ContentFeedFragment contentFeedFragment) {
        return contentFeedFragment;
    }

    @FragmentScope
    @Provides
    ContentFeedRouter provideRouter(ContentFeedFragment contentFeedFragment) {
        return contentFeedFragment;
    }

    @FragmentScope
    @Provides
    ContentFeedPresenter providePresenter(Context context,
                                          ContentFeedView view,
                                          ContentFeedRouter router,
                                          ContentUseCase contentUseCase) {
        return new ContentFeedFragmentPresenter(context, view, router, contentUseCase);
    }

    @FragmentScope
    @Provides
    ContentRepository provideContentRepository(GenkiApi genkiApi) {
        return new ContentRepositoryImp(genkiApi);
    }

    @FragmentScope
    @Provides
    ContentUseCase provideContentUseCase(SchedulerProvider schedulerProvider, ContentRepository contentRepository) {
        return new ContentUseCase(schedulerProvider.io(), schedulerProvider.ui(), contentRepository);
    }
}
