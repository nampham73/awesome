package com.genkimiru.app.presentation.update_user_infor.heart_beat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.BAD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.GOOD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.VERY_BAD;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.FeelCondition.VERY_GOOD;
import static com.genkimiru.app.presentation.update_user_infor.heart_beat.UpdateHeartBeatActivityContract.UpdateHeartBeatPresenter;
import static com.genkimiru.app.presentation.update_user_infor.heart_beat.UpdateHeartBeatActivityContract.UpdateHeartBeatRouter;
import static com.genkimiru.app.presentation.update_user_infor.heart_beat.UpdateHeartBeatActivityContract.UpdateHeartBeatView;

public class UpdateHeartBeatActivity extends SimpleBaseActivity<UpdateHeartBeatPresenter>
        implements UpdateHeartBeatView, UpdateHeartBeatRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.checkbox_heart_very_good)
    RadioButton mHeartVeryGood;
    @BindView(R.id.checkbox_heart_good)
    RadioButton mHeartGood;
    @BindView(R.id.checkbox_heart_bad)
    RadioButton mHeartBad;
    @BindView(R.id.checkbox_heart_not_bad)
    RadioButton mHeartNotBad;

    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    private MenuItem mApplyMenuItem;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateHeartBeatPresenter mUpdateHeartBeatPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_healthy_heartbeat;
    }

    @Override
    protected UpdateHeartBeatPresenter registerPresenter() {
        return mUpdateHeartBeatPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateHeartBeatPresenter.updateHeartBeat(mUserInfoRequest.getFeelcondition());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.checkbox_heart_very_good, R.id.checkbox_heart_good, R.id.checkbox_heart_bad, R.id.checkbox_heart_not_bad})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.checkbox_heart_very_good:
                if (checked) {
                    mUserInfoRequest.setFeelcondition(VERY_GOOD);
                    Timber.d("Feel condition: 1");
                }
                break;
            case R.id.checkbox_heart_good:
                if (checked) {
                    mUserInfoRequest.setFeelcondition(GOOD);
                    Timber.d("Feel condition: 2");
                }
                break;
            case R.id.checkbox_heart_not_bad:
                if (checked) {
                    mUserInfoRequest.setFeelcondition(BAD);
                    Timber.d("Feel condition: 3");
                }
                break;
            case R.id.checkbox_heart_bad:
                if (checked) {
                    mUserInfoRequest.setFeelcondition(VERY_BAD);
                    Timber.d("Feel condition: 4");
                }
                break;
        }
        setEnabledMenuApply(true);
    }

    private void initialDefaultValue() {
        //  mGenkiUserInfoModel.getFeelCondition();
        switch (mGenkiUserInfoModel.getFeelCondition()) {
            case VERY_GOOD:
                mHeartVeryGood.setChecked(true);
                break;
            case GOOD:
                mHeartGood.setChecked(true);
                break;
            case BAD:
                mHeartNotBad.setChecked(true);
                break;
            case VERY_BAD:
                mHeartBad.setChecked(true);
                break;
            default:
                break;
        }
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
