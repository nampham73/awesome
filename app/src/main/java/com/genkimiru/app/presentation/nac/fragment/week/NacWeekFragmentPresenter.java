package com.genkimiru.app.presentation.nac.fragment.week;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;

import static com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragmentContract.*;

public class NacWeekFragmentPresenter extends BasePresenterImp< NacWeekView,NacWeekRouter> implements NacWeekPresent{

    UserUseCase mUserUseCase;
    public NacWeekFragmentPresenter(Context context, NacWeekView nacWeekView, NacWeekRouter nacWeekRouter, UserUseCase userUseCase) {
        super(context,nacWeekView,nacWeekRouter);
        this.mUserUseCase = userUseCase;
    }

    @Override
    public void onUpdateNacWeek() {

    }
}
