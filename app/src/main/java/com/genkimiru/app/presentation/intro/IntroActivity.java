package com.genkimiru.app.presentation.intro;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseSliderActivity;
import com.genkimiru.app.base.widget.CustomViewPager;
import com.genkimiru.app.presentation.intro.fragment.IntroFragmentAdapter;
import com.genkimiru.app.presentation.login.LoginActivity;
import com.genkimiru.app.presentation.register.RegisterActivity;

import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.presentation.intro.IntroContract.IntroPresenter;
import static com.genkimiru.app.presentation.intro.IntroContract.IntroRouter;
import static com.genkimiru.app.presentation.intro.IntroContract.IntroView;

public class IntroActivity extends BaseSliderActivity<IntroFragmentAdapter, IntroPresenter>
        implements IntroView, IntroRouter {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_intro;
    }

    @Override
    protected IntroPresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    protected CustomViewPager getViewPager() {
        return findViewById(R.id.activity_intro_view_pager);
    }

    @Override
    protected TabLayout getTabLayout() {
        return findViewById(R.id.activity_intro_tab_dots);
    }

    @Override
    protected IntroFragmentAdapter getPagerAdapter() {
        return new IntroFragmentAdapter(getSupportFragmentManager());
    }

    @Override
    public void openLoginScreen() {
        Timber.d("openLoginScreen");
        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void openRegisterScreen() {
        Timber.d("loginWithGoogleFit");
        Intent intent = new Intent(IntroActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.activity_intro_log_in_btn)
    public void onOpenLoginScreenClick() {
        Timber.d("onOpenLoginScreenClick");
        openLoginScreen();
    }

    @OnClick(R.id.activity_intro_register_btn)
    public void onOpenRegisterScreenClick() {
        Timber.d("onOpenRegisterScreenClick");
        openRegisterScreen();
    }
}
