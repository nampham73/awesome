package com.genkimiru.app.presentation.nac.fragment.month;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ExpandableListView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.presentation.nac.fragment.NacFragmentAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static com.genkimiru.app.presentation.nac.fragment.month.NacMonthFragmentContract.*;

public class NacMonthFragment extends SimpleBaseFragment<NacMonthPresenter> implements NacMonthView,NacMonthRouter{

    @BindView(R.id.fragment_nac_month_expanded_listview)
    ExpandableListView mExpandableListView;

    NacFragmentAdapter mAdapter;
    List<String> listHeader;
    HashMap<String, List<String>> listItem;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        mAdapter = new NacFragmentAdapter(getActivity(),listHeader,listItem);
        mExpandableListView.setAdapter(mAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_nac_month;
    }

    @Override
    protected NacMonthPresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    private void setData(){
        listHeader = new ArrayList<>();
        listItem = new HashMap<String,List<String>>();

        listHeader.add("first_month");
        listHeader.add("second_month");
        listHeader.add("third_month");

        List<String> firstMonth = new ArrayList<>();
        firstMonth.add("123");
        firstMonth.add("123");
        firstMonth.add("123");

        List<String> secondMonth= new ArrayList<>();
        secondMonth.add("456");
        secondMonth.add("456");
        secondMonth.add("456");

        List<String> thirdMonth = new ArrayList<>();
        thirdMonth.add("789");
        thirdMonth.add("789");
        thirdMonth.add("789");

        listItem.put(listHeader.get(0),firstMonth);
        listItem.put(listHeader.get(1),secondMonth);
        listItem.put(listHeader.get(2),thirdMonth);
    }

    @Override
    public void onUpdateSuccess() {

    }

    @Override
    public void onUpdateError() {

    }
}
