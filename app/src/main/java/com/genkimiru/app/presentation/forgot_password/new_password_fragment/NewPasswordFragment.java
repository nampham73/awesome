package com.genkimiru.app.presentation.forgot_password.new_password_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivity;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.forgot_password.new_password_fragment.NewPasswordFragmentContract.*;

public class NewPasswordFragment extends SimpleBaseFragment<NewPasswordPresenter>
        implements NewPasswordView, NewPasswordRouter {

    @Inject
    NewPasswordPresenter mPresenter;

    @BindView(R.id.new_password_code_edt)
    TextInputEditText mRecoverCodeEdt;
    @BindView(R.id.new_password_edt)
    TextInputEditText mPasswordEdt;
    @BindView(R.id.new_password_confirm_edt)
    TextInputEditText mConfirmPasswordEdt;
    @BindView(R.id.new_password_send_btn)
    Button mSendBtn;

    private ForgotPasswordActivity mActivity;
    private TextDialogFragment mCompleteDialog;
    private CompositeDisposable mCompositeDisposable;
    private boolean mIsValidRecoverCode;
    private boolean mIsValidPassword;
    private boolean mIsValidPasswordLength;
    private boolean mIsValidConfirmPasswordInput;
    private boolean mIsValidConfirmPassword;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ForgotPasswordActivity) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEnabledSendButton(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissCompleteDialog();
        mCompositeDisposable.clear();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_new_password;
    }

    @Override
    protected NewPasswordPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(mActivity.getSupportFragmentManager());
    }

    @Override
    public void showLoadingDialog() {
        mActivity.showProgress();
    }

    @Override
    public void dismissLoadingDialog() {
        mActivity.hideProgress();
    }

    @Override
    public void openCompleteDialog() {
        mCompleteDialog = new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .message(getString(R.string.forgot_password_complete_message))
                .leftButtonText(getString(android.R.string.ok))
                .onResultListener(new TextDialogFragment.OnResultListener() {
                    @Override
                    public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                        mActivity.completeRecover();
                    }

                    @Override
                    public void onClickRightButton(TextDialogFragment textDialogFragment) {
                        // Do nothing
                    }
                })
                .build();
        mCompleteDialog.show(mActivity.getSupportFragmentManager());
    }

    @OnClick(R.id.new_password_send_btn)
    void onSendClick() {

        if (!mIsValidPasswordLength) {
            showMessageDialog(getString(R.string.forgot_password_invalid_password_message));
            return;
        }

        if (!mIsValidConfirmPassword) {
            showMessageDialog(getString(R.string.forgot_password_invalid_confirm_password_message));
            return;
        }

        String recoverCode = mRecoverCodeEdt.getText().toString().trim();
        String password = mPasswordEdt.getText().toString().trim();
        mPresenter.onSendClick(recoverCode, password);
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(
                RxTextView.textChanges(mRecoverCodeEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(recoverCode -> {
                            Timber.d("recover code: %s", recoverCode.toString());
                            mIsValidRecoverCode = mPresenter.isValidRecoverCode(recoverCode.toString().trim());
                            setEnabledSendButton(mIsValidRecoverCode && mIsValidPassword && mIsValidConfirmPasswordInput);
                        }));

        mCompositeDisposable.add(
                RxTextView.textChanges(mPasswordEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(password -> {
                            Timber.d("password: %s", password.toString());
                            mIsValidPassword = mPresenter.isValidPassword(password.toString().trim());
                            mIsValidPasswordLength = mPresenter.isValidPasswordLength(password.toString().trim());

                            mIsValidConfirmPassword = mPresenter.isValidConfirmPassword(password.toString().trim(),
                                    mConfirmPasswordEdt.getText().toString().trim());

                            setEnabledSendButton(mIsValidRecoverCode && mIsValidPassword && mIsValidConfirmPasswordInput);
                        }));

        mCompositeDisposable.add(
                RxTextView.textChanges(mConfirmPasswordEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(confirmPassword -> {
                            Timber.d("confirm password: %s", confirmPassword.toString());
                            mIsValidConfirmPasswordInput = mPresenter.isValidPassword(confirmPassword.toString().trim());
                            mIsValidConfirmPassword = mPresenter.isValidConfirmPassword(confirmPassword.toString().trim(),
                                    mPasswordEdt.getText().toString().trim());

                            setEnabledSendButton(mIsValidRecoverCode && mIsValidPassword && mIsValidConfirmPasswordInput);
                        }));
    }

    private void dismissCompleteDialog() {
        if (mCompleteDialog == null) {
            return;
        }
        mCompleteDialog.dismiss();
    }

    private void setEnabledSendButton(boolean isEnabled) {
        mSendBtn.setEnabled(isEnabled);
        mSendBtn.setTextColor(isEnabled
                ? mActivity.getResources().getColor(android.R.color.white)
                : mActivity.getResources().getColor(R.color.transparent_20_black));
    }
}
