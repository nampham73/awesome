package com.genkimiru.app.presentation.main.dashboard_fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiBmiModel;
import com.genkimiru.app.data.model.GenkiDashboardDataModel;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.genkimiru.app.data.model.GenkiHeartModel;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.data.model.GenkiSleepModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.model.GenkiStressModel;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.model.GenkiWaterModel;
import com.genkimiru.app.dialog.HealthCarePointDialog;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivity;
import com.genkimiru.app.presentation.energy.EnergyActivity;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.nutrition.NutritionActivity;
import com.genkimiru.app.presentation.step.StepActivity;
import com.genkimiru.app.widget.BMIChartView;
import com.genkimiru.app.widget.SleepChartView;
import com.genkimiru.app.widget.StressChartView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.BuildConfig.DATA_SERVER_URL_ECOMMERCE;
import static com.genkimiru.app.Constants.LoginEcommerce.DATA_SERVER_CHECK_TOKEN;
import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.DashboardPresenter;
import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.DashboardRouter;
import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.DashboardView;

public class DashboardFragment extends SimpleBaseFragment<DashboardFragmentContract.DashboardPresenter>
        implements DashboardView, DashboardRouter {

    private static final float STEP_DELTA = 0.065f;
    private static final String NONE_VALUE = "--";

    @BindView(R.id.main_dashboard_user_name_tv)
    TextView mUsernameTv;
    @BindView(R.id.main_dashboard_user_email_tv)
    TextView mUserEmailTv;
    @BindView(R.id.main_dashboard_avatar_iv)
    ImageView mAvatarIv;
    @BindView(R.id.dashboard_step_count_tv)
    TextView mTvStepCount;
    @BindView(R.id.energy_kcal_in_tv)
    TextView mTvEnergyKCalIn;
    @BindView(R.id.energy_kcal_out_tv)
    TextView mTvKEnergyKCalOut;
    @BindView(R.id.nutri_kcal_tv)
    TextView mTvNutriKCal;
    @BindView(R.id.heart_rate_average_tv)
    TextView mTvHeartAverage;
    @BindView(R.id.heart_rate_min_tv)
    TextView mTvHeartRateMin;
    @BindView(R.id.heart_rate_max_tv)
    TextView mTvHeartRateMax;
    @BindView(R.id.dashboard_stress_view)
    StressChartView mStressView;
    @BindView(R.id.index_medial_dashboard_tv)
    TextView mTvHealthCareName;
    @BindView(R.id.dashboard_stress_level_tv)
    TextView mTvStressLevel;
    @BindView(R.id.dashboard_weight_tv)
    TextView mTvWeight;
    @BindView(R.id.dashboard_water_tv)
    TextView mTvWater;
    @BindView(R.id.dashboard_bmi_view)
    BMIChartView mBMIChartView;
    @BindView(R.id.sleep_view)
    SleepChartView mSleepView;
    @BindView(R.id.sleep_error_tv)
    TextView mTvSleepError;
    @BindView(R.id.dashboard_sleep_hours_tv)
    TextView mSleepHours;
    @BindView(R.id.dashboard_slee_minutes_tv)
    TextView mSleepMinutes;
    @BindView(R.id.dashboard_refreshing_iv)
    ImageView mIvDashboardRefreshing;
    @BindView(R.id.dashboard_sleep_rating)
    RatingBar mSleepRating;

    @Inject
    DashboardPresenter mDashboardPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;
    @Inject
    GenkiDashboardDataModel mGenkiDashboardDataModel;
    @Inject
    List<SleepChartView.Entry> mSleepEntries;

    private MainActivity mMainActivity;
    private HealthCarePointDialog mHealthCarePointDialog;


    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMainActivity = (MainActivity) getActivity();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mGenkiDashboardDataModel.isEmpty()) {
            setUserInfo();
            onDataLoadDone(mGenkiDashboardDataModel);
            List<SleepChartView.Entry> entries = new ArrayList<>();
            entries.addAll(mSleepEntries);
            onLoadSleepDataDone(entries);
            return;
        }
        initialize();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main_dashboard;
    }

    @Override
    protected DashboardPresenter registerPresenter() {
        return mDashboardPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    private void initialize() {
        setUserInfo();
        mDashboardPresenter.requestAsyncCommonData();
        mDashboardPresenter.requestAsyncSleepData();
    }

    private void setUserInfo() {
        if (StringUtil.isNotEmpty(mGenkiUserInfoModel.getNickName())) {
            mUsernameTv.setText(mGenkiUserInfoModel.getNickName());
        } else {
            String username = String.format("%1$s %2$s",
                    mGenkiUserInfoModel.getFirstName(),
                    mGenkiUserInfoModel.getLastName());
            mUsernameTv.setText(username);
        }
        mUserEmailTv.setText(mGenkiUserInfoModel.getEmail());
        Glide.with(mMainActivity.getApplicationContext())
                .load(mGenkiUserInfoModel.getAvatar())
                .into(mAvatarIv);
    }

    @OnClick(R.id.dashboard_watson_layout)
    void openChatWithWatsonScreen() {
        goChatWithWatson();
    }

    @OnClick(R.id.dashboard_ecommerce_layout)
    void openECommerceScreen() {
        goECommerce();
    }

    @OnClick(R.id.dashboard_step_layout)
    void openStepScreen() {
        Intent intent = new Intent(mMainActivity, StepActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_nutrition_layout)
    void openNutritionScreen() {
        Intent intent = new Intent(mMainActivity, NutritionActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_energy_layout)
    void openEnergyScreen() {
        Intent intent = new Intent(mMainActivity, EnergyActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_health_care_point)
    void openHealthCarePoint() {
      mDashboardPresenter.requestStepMonthData();
    }

    @Override
    public void goChatWithWatson() {
        Intent intent = new Intent(mMainActivity, ChatWithWatsonActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_refreshing_iv)
    void onDashboardRefreshing() {
        ValueAnimator anim = ValueAnimator.ofFloat(0, 3);
        anim.setDuration(350);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.addUpdateListener(animation -> {
            float value = (float) animation.getAnimatedValue();
            float rotation = value * 180;
            mIvDashboardRefreshing.setRotation(rotation);
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mDashboardPresenter.requestAsyncCommonData();
                mDashboardPresenter.requestAsyncSleepData();
            }
        });
        anim.start();
    }

    @Override
    public void onDataLoadDone(GenkiDashboardDataModel model) {
        mMainActivity.hideProgress();
        if (model != null) {
            mGenkiDashboardDataModel.copy(model);
            updateStepCountView(model.getStep());
            updateHealthCareView(model.getStep());
            updateEnergyView(model.getEnergy());
            updateNutriView(model.getNutri());
            updateHeartRateView(model.getHeart());
            updateStressView(model.getStress());
            updateWeightView(model.getBmi());
            updateBmiView(model.getBmi());
            updateWaterView(model.getWater());
            updateSleepView(model.getSleep());
        }
    }

    private void updateSleepView(GenkiSleepModel sleep) {
        long averageStartTime = sleep.getAverageStartTime();
        long averageEndTime = sleep.getAverageEndTime();
        mSleepView.setStartTime(averageStartTime);
        mSleepView.setEndTime(averageEndTime);
        mSleepView.invalidate();
        if ((sleep.getAverageStartTime() == 0 || sleep.getAverageEndTime() == 0)
                && sleep.getTotalSleep() == 0) {
            mSleepHours.setText(NONE_VALUE);
            mSleepMinutes.setText(NONE_VALUE);
            mTvSleepError.setVisibility(View.VISIBLE);
            return;
        }
        int[] hours = splitToComponentTimes(sleep.getTotalSleep());
        updateSleepRating(hours[0]);
        mSleepHours.setText(String.valueOf(hours[0]));
        mSleepMinutes.setText(String.valueOf(hours[1]));
    }

    private void updateSleepRating(int hour) {
        if (hour < 3) {
            mSleepRating.setRating(1);
        } else if (hour < 5) {
            mSleepRating.setRating(2);
        } else if (hour < 6) {
            mSleepRating.setRating(3);
        } else if (hour < 8) {
            mSleepRating.setRating(4);
        } else {
            mSleepRating.setRating(5);
        }
    }

    @Override
    public void showMonthHealthCarePoint(GenkiStepModel genkiStepModel) {
        String val = NONE_VALUE;

        if (genkiStepModel != null && genkiStepModel.getTimestamp() != 0) {
            float step = genkiStepModel.getStep();
            val = StringUtil.formatNumber(Math.floor((STEP_DELTA * step) + 0.5));
        }

        String mHealthCare = String.format(getString(R.string.index_medical_dashboard), val);
        Timber.d("open health care point");
        mHealthCarePointDialog = new HealthCarePointDialog.HealthCarePointDialogBuilder()
                .setHealthCarePoint(getString(R.string.index_healthy_dashboard), mHealthCare)
                .dialogListener(DialogFragment::dismiss).build();
        mHealthCarePointDialog.show(getFragmentManager());
    }

    public static int[] splitToComponentTimes(float biggy) {
        long longVal = (long) biggy;
        int hours = (int) longVal / 60;
        int mins = (int) longVal - hours * 60;
        return new int[]{hours, mins};
    }

    @Override
    public void onDataLoadFailure(String message) {
        mMainActivity.hideProgress();
        MessageUtil.createMessageDialog(message);
    }

    @Override
    public void onPrepareLoadingData() {
        mMainActivity.showProgress();
    }

    @Override
    public void onPrepareToSyncSleepData() {
        mMainActivity.showProgress();
    }

    @Override
    public void onPreparetoSyncStepData() {
        mMainActivity.showProgress();
    }

    @Override
    public void onLoadSleepDataFailure(String message) {
        mMainActivity.hideProgress();
        mTvSleepError.setText(getString(R.string.sleep_dashboard_err));
        mTvSleepError.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadSleepDataDone(List<SleepChartView.Entry> entries) {
        if (entries == null || entries.size() == 0) {
            return;
        }
        mSleepEntries.clear();
        mSleepEntries.addAll(entries);
        mTvSleepError.setVisibility(View.GONE);
        mSleepView.setEntries(entries);
    }

    @Override
    public void onLoadMonthStepSuccess(GenkiStepModel genkiStepModel) {
        mMainActivity.hideProgress();
        showMonthHealthCarePoint(genkiStepModel);
    }

    @Override
    public void onLoadMonthStepFailure(String message) {
        mMainActivity.hideProgress();
        MessageUtil.createMessageDialog(message).show(getFragmentManager());
    }

    private void updateStepCountView(GenkiStepModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        mTvStepCount.setText(StringUtil.formatNumber(model.getStep()));
    }

    private void updateEnergyView(GenkiEnergyModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        mTvEnergyKCalIn.setText(StringUtil.formatNumber(model.getKCalIn()));
        mTvKEnergyKCalOut.setText(StringUtil.formatNumber(model.getKCalOut()));
    }

    private void updateNutriView(GenkiNutritionModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        mTvNutriKCal.setText(StringUtil.formatNumber(model.getKcalAverage()));
    }

    private void updateHeartRateView(GenkiHeartModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }
        mTvHeartAverage.setText(StringUtil.formatNumber(model.getHeartRate()));
        mTvHeartRateMax.setText(StringUtil.formatNumber(model.getMax()));
        mTvHeartRateMin.setText(StringUtil.formatNumber(model.getMin()));
    }

    private void updateStressView(GenkiStressModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        int stressLevel = (int) model.getStressLevel();
        mTvStressLevel.setText(StringUtil.formatNumber(stressLevel));
        mStressView.setMax(100);///default 100 is max values
        mStressView.setValue(stressLevel);
    }

    private void updateHealthCareView(GenkiStepModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        float step = model.getStep();
        String val = StringUtil.formatNumber(Math.floor((STEP_DELTA * step) + 0.5));
        mTvHealthCareName.setText(String.format(getString(R.string.index_medical_dashboard), val));
    }

    private void updateWeightView(GenkiBmiModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        float weight = model.getWeight();
        mTvWeight.setText(weight > 0 ? StringUtil.formatNumber(weight) : NONE_VALUE);
    }

    private void updateWaterView(GenkiWaterModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        if (model.getEnoughWaterPercent() == 0) {
            mTvWater.setText(NONE_VALUE);
            return;
        }

        mTvWater.setText(model.getEnoughWaterPercent() >= 50
                ? R.string.water_enough_reasonable_dashboard
                : R.string.water_not_enough_reasonable_dashboard);
    }

    private void updateBmiView(GenkiBmiModel model) {
        if (model == null || model.getTimestamp() == 0) {
            return;
        }

        mBMIChartView.reset();
        mBMIChartView.setValue(model.getBmi());
    }

    @Override
    public void goECommerce() {
        String url = DATA_SERVER_URL_ECOMMERCE + DATA_SERVER_CHECK_TOKEN + mGenkiUserInfoModel.getToken();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
