package com.genkimiru.app.presentation.main.contest_fragment.current;

import android.content.Context;

import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.contest_fragment.ContestsListFragment;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.CurrentContestPresenter;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.CurrentContestRouter;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.CurrentContestView;

import javax.inject.Inject;

public class CurrentContestFragment extends ContestsListFragment<CurrentContestPresenter>
        implements CurrentContestView, CurrentContestRouter {

    @Inject
    CurrentContestPresenter mPresenter;

    private MainActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) getActivity();
    }

    @Override
    protected CurrentContestPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected void showProgress() {
        mActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mActivity.hideProgress();
    }

    @Override
    protected CurrentContestPresenter getPresenter() {
        return mPresenter;
    }
}
