package com.genkimiru.app.presentation.challenge_detail_history.history_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContestModel;

import java.util.List;

public interface HistoryChallengeContract {
    interface HistoryChallengePresenter extends BasePresenter, ListContract.ListPresenter {

    }

    interface HistoryChallengeView extends ListContract.RootView<List<GenkiContestModel>> {

    }

    interface HistoryChallengeRouter {
    }
}
