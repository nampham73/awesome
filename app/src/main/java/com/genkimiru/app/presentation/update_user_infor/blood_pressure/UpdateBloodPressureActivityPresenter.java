package com.genkimiru.app.presentation.update_user_infor.blood_pressure;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityContract.UpdateBloodPressurePresenter;
import static com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityContract.UpdateBloodPressureRouter;
import static com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityContract.UpdateBloodPressureView;

public class UpdateBloodPressureActivityPresenter extends BasePresenterImp<UpdateBloodPressureView, UpdateBloodPressureRouter>
        implements UpdateBloodPressurePresenter {
    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    int mBloodPressureUpper;
    int mBloodPressureUnder;

    public UpdateBloodPressureActivityPresenter(Context context,
                                                UpdateBloodPressureView updateBloodPressureView,
                                                UpdateBloodPressureRouter updateBloodPressureRouter,
                                                UserUseCase userUseCase,
                                                GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateBloodPressureView, updateBloodPressureRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;
    }


    @Override
    public void updateBloodPressure(int bloodPressureUpper, int bloodPressureUnder) {
        mBloodPressureUpper = bloodPressureUpper;
        mBloodPressureUnder = bloodPressureUnder;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setBloodPressureUpper(bloodPressureUpper)
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setBloodPressureUnder(bloodPressureUnder)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .create();

        mUserUseCase.execute(new UpdateBloodSucriber(), UserParam.Factory.updateUserProfile(request));
    }

    @Override
    public String validateBloodPressure(String bloodPresSure) {
        if (Float.parseFloat(bloodPresSure) < 1) {
            bloodPresSure = "1";
        }
        if (Float.parseFloat(bloodPresSure) > 300) {
            bloodPresSure = "300";
        }
        return bloodPresSure;
    }


    private class UpdateBloodSucriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Update BloodPressure success");
            mGenkiUserInfoModel.setBloodPressureUpper(mBloodPressureUpper);
            mGenkiUserInfoModel.setBloodPressureUnder(mBloodPressureUnder);
            mView.onUpdateSuccess();

        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }

}
