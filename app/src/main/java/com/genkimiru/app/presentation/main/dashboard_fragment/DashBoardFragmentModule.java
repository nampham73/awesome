package com.genkimiru.app.presentation.main.dashboard_fragment;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.mydata.MyDataRepository;
import com.genkimiru.app.data.repository.mydata.MyDataRepositoryImp;
import com.genkimiru.app.domain.mydata.MyDataUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.*;

@Module
public class DashBoardFragmentModule {

    @Provides
    @FragmentScope
    public DashboardView provideView(DashboardFragment dashboardFragment) {
        return dashboardFragment;
    }

    @Provides
    @FragmentScope
    public DashboardRouter provideRouter(DashboardFragment dashboardFragment) {
        return dashboardFragment;
    }

    @Provides
    @FragmentScope
    public DashboardPresenter providePresenter(Context context, DashboardView view, DashboardRouter router, MyDataUseCase useCase) {
        return new DashboardFragmentPresenter(context, view, router,useCase);
    }

    @Provides
    @FragmentScope
    MyDataUseCase provideUseCase(SchedulerProvider schedulerProvider, MyDataRepository repository){
        return new MyDataUseCase(schedulerProvider.newThread(),schedulerProvider.ui(),repository);
    }


    @Provides
    @FragmentScope
    MyDataRepository provideRepository(GenkiApi api){
        return new MyDataRepositoryImp(api);
    }
}
