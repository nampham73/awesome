package com.genkimiru.app.presentation.bug_report;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.presentation.bug_report.adapter.BugImage;

import java.util.List;

public interface BugReportActivityContract {

    interface BugReportPresenter extends BasePresenter {
        void sendBugReport(String title, String problem, String severity, List<BugImage> bugImageList, String userName);
    }

    interface BugReportView {
        void onReportSuccess();
        void onReportFail();
    }

    interface BugReportRouter {

    }
}
