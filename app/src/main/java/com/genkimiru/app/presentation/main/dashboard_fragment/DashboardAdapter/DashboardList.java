package com.genkimiru.app.presentation.main.dashboard_fragment.DashboardAdapter;

public class DashboardList {
    int imageItem;
    String nameItem;

    public DashboardList(int imageItem, String nameItem) {
        this.imageItem = imageItem;
        this.nameItem = nameItem;
    }

    public int getImageItem() {
        return imageItem;
    }

    public void setImageItem(int imageItem) {
        this.imageItem = imageItem;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }
}
