package com.genkimiru.app.presentation.change_password;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface ChangePasswordActivityContract {

    interface ChangePasswordPresenter extends BasePresenter {
        boolean isValidPassword(String password);
        boolean isValidPasswordLength(String password);
        boolean isTheSamePassword(String password1, String password2);
        void sendChangePassword(String oldPassword, String newPassword);
    }

    interface ChangePasswordView {
        void showMessageDialog(String message);
        void showLoadingDialog();
        void dismissLoadingDialog();
        void openCompleteDialog();
    }

    interface ChangePasswordRouter {
        void completeChangePassword();
    }
}
