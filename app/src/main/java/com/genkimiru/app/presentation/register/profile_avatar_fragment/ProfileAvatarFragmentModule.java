package com.genkimiru.app.presentation.register.profile_avatar_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarFragmentRouter;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarFragmentView;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentContract.ProfileAvatarPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileAvatarFragmentModule {

    @Provides
    @FragmentScope
    ProfileAvatarFragmentView provideView(ProfileAvatarFragment profileAvatarFragment) {
        return profileAvatarFragment;
    }

    @Provides
    @FragmentScope
    ProfileAvatarFragmentRouter provideRouter(ProfileAvatarFragment profileAvatarFragment) {
        return profileAvatarFragment;
    }

    @Provides
    @FragmentScope
    ProfileAvatarPresenter providePresenter(Context context, ProfileAvatarFragmentView profileAvatarFragmentView,
                                            ProfileAvatarFragmentRouter profileAvatarFragmentRouter, UserUseCase userUseCase) {
        return new ProfileAvatarFragmentPresenter(context, profileAvatarFragmentView, profileAvatarFragmentRouter, userUseCase);
    }
}
