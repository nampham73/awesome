package com.genkimiru.app.presentation.main.contest_fragment.history;

import android.content.Context;

import com.genkimiru.app.base.scope.ChildFragmentScope;

import dagger.Module;
import dagger.Provides;

import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestContract.*;

@Module
public class HistoryContestFragmentModule {

    @Provides
    @ChildFragmentScope
    HistoryContestRouter provideHistoryContestRouter(HistoryContestFragment router) {
        return router;
    }

    @Provides
    @ChildFragmentScope
    HistoryContestRootView provideHistoryRootView(HistoryContestFragment rootView) {
        return rootView;
    }

    @Provides
    @ChildFragmentScope
    HistoryContestPresenter providePresenter(Context context, HistoryContestRootView view, HistoryContestRouter router, ContestUseCase useCase) {
        return new HistoryContestFragmentPresenter(context, view, router, useCase);
    }

    @Provides
    @ChildFragmentScope
    ContestAdapter provideAdapter(Context context) {
        return new ContestAdapter(context);
    }

}
