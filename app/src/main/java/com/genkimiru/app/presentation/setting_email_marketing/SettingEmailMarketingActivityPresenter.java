package com.genkimiru.app.presentation.setting_email_marketing;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.SettingEmailMarketingRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingPresenter;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingRouter;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityContract.SettingEmailMarketingView;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class SettingEmailMarketingActivityPresenter extends BasePresenterImp<SettingEmailMarketingView, SettingEmailMarketingRouter> implements SettingEmailMarketingPresenter {

    private UserUseCase mUserUseCase;

    public SettingEmailMarketingActivityPresenter(Context context, SettingEmailMarketingView settingEmailMarketingView, SettingEmailMarketingRouter settingEmailMarketingRouter, UserUseCase userUseCase) {
        super(context, settingEmailMarketingView, settingEmailMarketingRouter);
        mUserUseCase = userUseCase;

    }

    @Override
    public void updateEmailMarketingSetting(boolean isReceiveEmailChecked) {
        int receiveEmailChecked = isReceiveEmailChecked ? 1 : 2;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        SettingEmailMarketingRequest settingEmailMarketingRequest = new SettingEmailMarketingRequest(receiveEmailChecked, token);
        mUserUseCase.execute(new UpdateEmailMarketingSubscriber(), UserParam.Factory.updateEmailMarketingSetting(settingEmailMarketingRequest));

    }

    private class UpdateEmailMarketingSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("update mail marketing error: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
            mView.onUpdateFail();
        }
    }
}
