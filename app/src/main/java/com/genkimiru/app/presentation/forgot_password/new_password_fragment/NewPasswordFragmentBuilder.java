package com.genkimiru.app.presentation.forgot_password.new_password_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class NewPasswordFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = NewPasswordFragmentModule.class)
    abstract NewPasswordFragment newPasswordFragment();
}
