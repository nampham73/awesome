package com.genkimiru.app.presentation.register.weight_fragment;

import android.content.Context;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightFragmentRouter;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightFragmentView;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightPresenter;

import butterknife.BindView;

public class WeightFragmentPresenter extends BasePresenterImp<WeightFragmentView,
        WeightFragmentRouter> implements WeightPresenter {


    private UserUseCase mUserUseCase;

    public WeightFragmentPresenter(Context context, WeightFragmentView weightFragmentView, WeightFragmentRouter weightFragmentRouter, UserUseCase userUseCase) {
        super(context, weightFragmentView, weightFragmentRouter);
        mUserUseCase = userUseCase;
    }

    @Override
    public String validateWeight(String weight) {
        if (Float.parseFloat(weight) < 1) {
            weight = "1";
        }
        if(Float.parseFloat(weight) > 1000) {
            weight = "1000";
        }
       return weight;
    }
}
