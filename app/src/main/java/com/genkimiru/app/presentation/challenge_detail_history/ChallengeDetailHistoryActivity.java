package com.genkimiru.app.presentation.challenge_detail_history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseSliderActivity;
import com.genkimiru.app.base.widget.CustomViewPager;
import com.genkimiru.app.data.model.GenkiChallengeModel;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_TARGET;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_TITLE;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_USER_DATA;
import static com.genkimiru.app.Constants.Extra.CONTEST_ID;
import static com.genkimiru.app.presentation.challenge_detail_history.ChallengeDetailActivityContract.*;

public class ChallengeDetailHistoryActivity extends BaseSliderActivity<ChallengeDetailHistoryAdapter,ChallengeDetailPresenter> {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    @Inject
    ChallengeDetailHistoryAdapter mChallengeDetailHistoryAdapter;

    @Override
    protected CustomViewPager getViewPager() {
        return findViewById(R.id.activity_challenge_viewpager);
    }

    @Override
    protected TabLayout getTabLayout() {
        return findViewById(R.id.activity_challenge_tablayout);
    }

    @Override
    protected ChallengeDetailHistoryAdapter getPagerAdapter() {
        return mChallengeDetailHistoryAdapter;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_challenge_detail_history;
    }

    @Override
    protected ChallengeDetailPresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        if (getIntent() == null) return;
        GenkiChallengeModel model = (GenkiChallengeModel) getIntent().getSerializableExtra(CHALLENGE_USER_DATA);
        mToolbarTitle.setText(model.getTitle());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
