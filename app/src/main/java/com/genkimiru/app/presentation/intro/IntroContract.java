package com.genkimiru.app.presentation.intro;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface IntroContract {

    interface  IntroPresenter extends BasePresenter {

    }

    interface IntroView {

    }

    interface IntroRouter {
        void openLoginScreen();
        void openRegisterScreen();
    }
}
