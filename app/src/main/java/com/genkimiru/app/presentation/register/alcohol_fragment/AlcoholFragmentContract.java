package com.genkimiru.app.presentation.register.alcohol_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface AlcoholFragmentContract {

    interface AlcoholPresenter extends BasePresenter {

    }

    interface AlcoholFragmentView {

    }

    interface AlcoholFragmentRouter {

    }
}
