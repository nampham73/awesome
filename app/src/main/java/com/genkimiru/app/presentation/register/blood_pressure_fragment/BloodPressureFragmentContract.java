package com.genkimiru.app.presentation.register.blood_pressure_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface BloodPressureFragmentContract {

    interface BloodPressurePresenter extends BasePresenter {
        String validateBloodPressureUpper(String bloodPresSureUpper);
        String validateBloodPressureUnder(String bloodPresSureUnder);
    }

    interface BloodPressureFragmentView {

    }

    interface BloodPressureFragmentRouter {

    }
}
