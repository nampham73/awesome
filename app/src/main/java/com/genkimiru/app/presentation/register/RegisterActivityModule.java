package com.genkimiru.app.presentation.register;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.fitbit.FitbitApi;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.gobe.GobeApi;
import com.genkimiru.app.data.repository.fitbit.FitbitRepository;
import com.genkimiru.app.data.repository.fitbit.FitbitRepositoryImp;
import com.genkimiru.app.data.repository.gobe.GobeRepository;
import com.genkimiru.app.data.repository.gobe.GobeRepositoryImp;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.fitbit.FitbitUseCase;
import com.genkimiru.app.domain.gobe.GobeUseCase;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterPresenter;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterRouter;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterView;

import dagger.Module;
import dagger.Provides;

@Module
public class RegisterActivityModule {

    @Provides
    @ActivityScope
    RegisterView provideView(RegisterActivity registerActivity) {
        return registerActivity;
    }

    @Provides
    @ActivityScope
    RegisterRouter provideRouter(RegisterActivity registerActivity) {
        return registerActivity;
    }

    @Provides
    @ActivityScope
    RegisterPresenter providePresenter(Context context,
                                       RegisterView registerView,
                                       RegisterRouter registerRouter,
                                       UserUseCase userUseCase,
                                       FitbitUseCase fitbitUseCase,
                                       RegisterUserRequest registerUserRequest,
                                       GobeUseCase gobeUseCase,
                                       GenkiUserInfoModel genkiUserInfoModel) {
        return new RegisterActivityPresenter(context, registerView, registerRouter, userUseCase, fitbitUseCase, registerUserRequest, gobeUseCase, genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }

    @Provides
    @ActivityScope
    RegisterUserRequest provideRegisterUserRequest() {
        return new RegisterUserRequest();
    }

    @Provides
    @ActivityScope
    FitbitRepository provideFitbitRepository(FitbitApi api) {
        return new FitbitRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    FitbitUseCase provideFitbitUseCase(SchedulerProvider schedulerProvider, FitbitRepository fitbitRepository) {
        return new FitbitUseCase(schedulerProvider.io(), schedulerProvider.ui(), fitbitRepository);
    }

    @Provides
    @ActivityScope
    GobeRepository provideGobeRepository(GobeApi api) {
        return new GobeRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    GobeUseCase provideGobeUseCase(SchedulerProvider schedulerProvider, GobeRepository gobeRepository) {
        return new GobeUseCase(schedulerProvider.io(), schedulerProvider.ui(), gobeRepository);
    }
}
