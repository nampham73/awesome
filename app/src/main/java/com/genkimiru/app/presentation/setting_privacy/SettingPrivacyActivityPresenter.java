package com.genkimiru.app.presentation.setting_privacy;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.PrivacySettingRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyPresenter;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyRouter;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyView;

public class SettingPrivacyActivityPresenter extends BasePresenterImp<SettingPrivacyView, SettingPrivacyRouter>
        implements SettingPrivacyPresenter {

    private UserUseCase mUserUseCase;

    public SettingPrivacyActivityPresenter(Context context, SettingPrivacyView settingPrivacyView, SettingPrivacyRouter settingPrivacyRouter, UserUseCase userUseCase) {
        super(context, settingPrivacyView, settingPrivacyRouter);
        mUserUseCase = userUseCase;
    }

    @Override
    public void updatePrivacySetting(boolean isHealthChecked, boolean isAccountChecked) {
        int healthChecked = isHealthChecked ? 1 : 2;
        int accountChecked = isAccountChecked ? 1 : 2;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        PrivacySettingRequest privacySettingRequest = new PrivacySettingRequest(healthChecked, accountChecked, token);
        mUserUseCase.execute(new UpdatePrivacySettingSubscriber(), UserParam.Factory.updatePrivacySetting(privacySettingRequest));
    }


    private class UpdatePrivacySettingSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            Timber.d("Update privacy setting success");
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Update privacy setting error: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
            mView.onUpdateFail();
        }
    }
}
