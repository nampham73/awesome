package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.content.ContentRepository;
import com.genkimiru.app.data.repository.content.ContentRepositoryImp;
import com.genkimiru.app.domain.content.ContentParam;
import com.genkimiru.app.domain.content.ContentUseCase;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedContract.*;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.DetailContentFeedAdapter;

@Module
public class DetailContentFeedFragmentModule {

    @FragmentScope
    @Provides
    DetailContentFeedView provideView(DetailContentFeedFragment view) {
        return view;
    }

    @FragmentScope
    @Provides
    DetailContentFeedRouter provideRouter(DetailContentFeedFragment router) {
        return router;
    }

    @FragmentScope
    @Provides
    ContentUseCase provideContentUseCase(SchedulerProvider schedulerProvider, ContentRepository repository) {
        return new ContentUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }

    @FragmentScope
    @Provides
    ContentRepository provideContentRepository(GenkiApi api) {
        return new ContentRepositoryImp(api);
    }

    @FragmentScope
    @Provides
    DetailContentFeedPresenter providePresenter(Context context, DetailContentFeedView view, DetailContentFeedRouter router, ContentUseCase useCase) {
        return new DetailContentFeedFragmentPresenter(context, view, router, useCase);
    }

    @FragmentScope
    @Provides
    DetailContentFeedAdapter provideAdapter() {
        return new DetailContentFeedAdapter();
    }

}
