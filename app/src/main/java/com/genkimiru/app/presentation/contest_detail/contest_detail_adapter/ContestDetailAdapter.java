package com.genkimiru.app.presentation.contest_detail.contest_detail_adapter;

import android.content.Context;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.data.model.GenkiChallengeModel;
import com.genkimiru.app.data.model.GenkiContestDetailModel;

import java.util.List;

public class ContestDetailAdapter extends BaseSimpleAdapter<GenkiChallengeModel, ContestDetailViewHolder> {

    public ContestDetailAdapter(Context context) {
        super(context);
    }

    @Override
    public ContestDetailViewHolder createHolder(View view) {
        return new ContestDetailViewHolder(view);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.challenge_item_layout;
    }
}
