package com.genkimiru.app.presentation.main.contentfeed_fragment;

import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContentModel;

import java.util.List;

public interface ContentFeedFragmentContract {

    interface ContentFeedPresenter extends ListContract.ListPresenter {
        void setIsLoadFavoriteList(boolean isLoadFavorite);
    }

    interface ContentFeedView extends ListContract.RootView<List<GenkiContentModel>> {
        void resetView();
    }

    interface ContentFeedRouter {

    }
}
