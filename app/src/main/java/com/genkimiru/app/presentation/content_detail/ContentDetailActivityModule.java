package com.genkimiru.app.presentation.content_detail;

import android.content.Context;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.presentation.content_detail.ContentDetailContract.*;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class ContentDetailActivityModule {

    @ActivityScope
    @Provides
    ContentDetailView provideView(ContentDetailActivity view, GenkiUserInfoModel userInfoModel) {
        return new ContentDetailActivityView(view,userInfoModel);
    }

    @ActivityScope
    @Provides
    ContentDetailRouter provideRouter(ContentDetailActivity router) {
        return router;
    }

    @ActivityScope
    @Provides
    ContentDetailPresenter providePresenter(Context context, ContentDetailRouter router, ContentDetailView view) {
        return new ContentDetailActivityPresenter(context, view, router);
    }
}
