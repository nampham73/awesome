package com.genkimiru.app.presentation.update_user_infor.gender;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityContract.UpdateGenderPresenter;
import static com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityContract.UpdateGenderRouter;
import static com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityContract.UpdateGenderView;

public class UpdateGenderActivityPresenter extends BasePresenterImp<UpdateGenderView, UpdateGenderRouter>
        implements UpdateGenderPresenter {

    UserUseCase mUserUseCase;
    GenkiUserInfoModel mGenkiUserInfoModel;
    int mGender;

    public UpdateGenderActivityPresenter(Context context,
                                         UpdateGenderView updateGenderView,
                                         UpdateGenderRouter updateGenderRouter,
                                         UserUseCase userUseCase,
                                         GenkiUserInfoModel genkiUserInfoModel) {
        super(context, updateGenderView, updateGenderRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;
    }


    @Override
    public void updateGender(int gender) {
        mGender = gender;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setGender(gender)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(),mGenkiUserInfoModel.getGobePassword())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();

        mUserUseCase.execute(new UpdateGenderSubcriber(), (UserParam.Factory.updateUserProfile(request)));
    }

    private class UpdateGenderSubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            Timber.d("Updage Gender Success");
            mGenkiUserInfoModel.setGender(mGender);
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }
}
