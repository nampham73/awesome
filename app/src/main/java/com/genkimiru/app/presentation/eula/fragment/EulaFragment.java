package com.genkimiru.app.presentation.eula.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.presentation.eula.EulaActivity;
import com.genkimiru.app.presentation.eula.EulaPagerAdapter;

import butterknife.BindView;
import timber.log.Timber;

public class EulaFragment extends SimpleBaseFragment {

    @BindView(R.id.eula_webview)
    WebView mWebView;

    private EulaActivity mActivity;
    private EulaPagerAdapter.TypeView mType;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (EulaActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_eula;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public void setLayoutTypeView(EulaPagerAdapter.TypeView type) {
        mType = type;
    }

    public void initializeView() {
        Timber.d("initializeView mType = %s", mType);
        if (mType == null) {
            return;
        }
        switch (mType) {
            case TERMS_OF_USE:
                setWebView(Constants.UrlRequest.TERM_OF_USE_REQUEST);
                break;
            case PRIVACY_POLICY:
                setWebView(Constants.UrlRequest.PERSONAL_POLICY_URL_REQUEST);
                break;
            default:
                break;
        }

    }

    public void setWebView(String url) {
        Timber.d("setWebView url = %s",url);
        mWebView.setWebViewClient(new WebViewContentClient());
        mWebView.loadUrl(url);
    }


    public class WebViewContentClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mActivity.showProgress();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mActivity.hideProgress();
        }
    }


}
