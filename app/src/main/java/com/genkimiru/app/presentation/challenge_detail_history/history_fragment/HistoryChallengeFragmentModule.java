package com.genkimiru.app.presentation.challenge_detail_history.history_fragment;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.contest.ContestRepository;
import com.genkimiru.app.data.repository.contest.ContestRepositoryImp;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.challenge_detail_history.history_fragment.adapter.HistoryChallengeFragmentAdapter;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengePresenter;
import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengeRouter;
import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengeView;

@Module
public class HistoryChallengeFragmentModule {

    @Provides
    @FragmentScope
    HistoryChallengePresenter providePresenter(Context context, HistoryChallengeView historyChallengeView, HistoryChallengeRouter historyChallengeRouter, ContestUseCase contestUseCase) {
        return new HistoryChallengeFragmentPresenter(context, historyChallengeView, historyChallengeRouter, contestUseCase);
    }

    @Provides
    @FragmentScope
    ContestUseCase provideContestUseCase(SchedulerProvider schedulerProvider, ContestRepository repository) {
        return new ContestUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }

    @Provides
    @FragmentScope
    ContestRepository provideContestRepository(GenkiApi api) {
        return new ContestRepositoryImp(api);
    }

    @Provides
    @FragmentScope
    HistoryChallengeView provideView(HistoryChallengeFragment view) {
        return view;
    }

    @Provides
    @FragmentScope
    HistoryChallengeRouter provideRouter(HistoryChallengeFragment router) {
        return router;
    }

    @Provides
    @FragmentScope
    HistoryChallengeFragmentAdapter provideAdapter(HistoryChallengeFragment adapter) {
        return new HistoryChallengeFragmentAdapter(adapter.getActivity());
    }

}

