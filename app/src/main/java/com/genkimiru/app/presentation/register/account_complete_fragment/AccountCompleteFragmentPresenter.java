package com.genkimiru.app.presentation.register.account_complete_fragment;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.response.RegisterUserResponse;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompletePresenter;
import static com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompleteRouter;
import static com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompleteView;

public class AccountCompleteFragmentPresenter extends BasePresenterImp<AccountCompleteView,
        AccountCompleteRouter> implements AccountCompletePresenter {

    private UserUseCase mUserUseCase;
    private GenkiUserInfoModel mGenkiUserInfoModel;

    public AccountCompleteFragmentPresenter(Context context, AccountCompleteView accountCompleteView,
                                            AccountCompleteRouter accountCompleteRouter,
                                            UserUseCase userUseCase,
                                            GenkiUserInfoModel genkiUserInfoModel) {
        super(context, accountCompleteView, accountCompleteRouter);
        mUserUseCase = userUseCase;
        mGenkiUserInfoModel = genkiUserInfoModel;
    }

    @Override
    public void register(RegisterUserRequest registerUserRequest) {
        Timber.d("register");

        registerUserRequest.address = "TOKYO";
        registerUserRequest.latitude = 0;
        registerUserRequest.longitude = 0;

        mUserUseCase.execute(new RegisterSubscriber(), UserParam.Factory.register(registerUserRequest));
    }

    @Override
    public void prepareUserInfo() {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        mUserUseCase.execute(new GetUserInfoSubscriber(), UserParam.Factory.getUserInformation(token));
    }

    private class RegisterSubscriber extends DefaultSubscriber<RegisterUserResponse> {

        @Override
        public void onNext(RegisterUserResponse registerUserResponse) {
            Timber.d("RegisterUserResponse success: %s", registerUserResponse.token);
            GenkiApplication.getPreferences().getString(USER_TOKEN).set(registerUserResponse.token);
            mView.showCompleteMessage();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.d("RegisterUserResponse failed: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
            mView.onRegisterFail();
        }
    }

    private class GetUserInfoSubscriber extends DefaultSubscriber<UserInfoResponse> {

        @Override
        public void onNext(UserInfoResponse userInfoResponse) {
            Timber.d("Get user info success");
            mGenkiUserInfoModel.setValueFromResponse(userInfoResponse);
            mRouter.openDashboardScreen();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Get user info Error: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }
}
