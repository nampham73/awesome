package com.genkimiru.app.presentation.register;

import android.content.Context;

import com.genkimiru.app.R;
import com.genkimiru.app.base.image_loader.WriteFileAsync;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.ImageUtils;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.genkimiru.app.data.remote.fitbit.request.FitbitAccessTokenRequest;
import com.genkimiru.app.data.remote.fitbit.request.FitbitUserProfileRequest;
import com.genkimiru.app.data.remote.fitbit.response.FitbitAccessTokenResponse;
import com.genkimiru.app.data.remote.fitbit.response.FitbitUserProfileResponse;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.gobe.request.GobeAuthorizationRequest;
import com.genkimiru.app.data.remote.gobe.response.GobeAuthorizationResponse;
import com.genkimiru.app.domain.fitbit.FitbitParam;
import com.genkimiru.app.domain.fitbit.FitbitUseCase;
import com.genkimiru.app.domain.gobe.GobeParam;
import com.genkimiru.app.domain.gobe.GobeUseCase;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterPresenter;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterRouter;
import com.genkimiru.app.presentation.register.RegisterActivityContract.RegisterView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.Calendar;

import okhttp3.ResponseBody;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Facebook.AVATAR_URL_FACEBOOK;
import static com.genkimiru.app.Constants.Fitbit.CLIENT_ID;
import static com.genkimiru.app.Constants.Fitbit.GRANT_TYPE;
import static com.genkimiru.app.Constants.Fitbit.REDIRECT_URI;

public class RegisterActivityPresenter extends BasePresenterImp<RegisterView, RegisterRouter> implements RegisterPresenter {

    private UserUseCase mUserUseCase;
    private FitbitUseCase mFitbitUseCase;
    private RegisterUserRequest mRegisterUserRequest;
    private RegisterActivity mRegisterActivity;
    private String avatarUrl;
    private GobeUseCase mGobeUseCase;
    private GenkiUserInfoModel mGenkiUserInfoModel;

    public RegisterActivityPresenter(Context context,
                                     RegisterView registerView,
                                     RegisterRouter registerRouter,
                                     UserUseCase userUseCase,
                                     FitbitUseCase fitbitUseCase,
                                     RegisterUserRequest registerUserRequest,
                                     GobeUseCase gobeUseCase,
                                     GenkiUserInfoModel genkiUserInfoModel) {
        super(context, registerView, registerRouter);
        mUserUseCase = userUseCase;
        mFitbitUseCase = fitbitUseCase;
        mRegisterUserRequest = registerUserRequest;
        mGobeUseCase = gobeUseCase;
        mGenkiUserInfoModel = genkiUserInfoModel;
    }

    @Override
    public void loginFitbit(String code) {
        Timber.d("login Fitbit: code = %s", code);
        mView.showProgressBar();
        FitbitAccessTokenRequest request = new FitbitAccessTokenRequest(CLIENT_ID, GRANT_TYPE, REDIRECT_URI, code);
        mFitbitUseCase.execute(new FitbitAccessTokenSubscriber(), FitbitParam.Factory.getAccessToken(request));
    }

    @Override
    public void loginGobe(String email, String password) {
        mView.showProgressBar();
        Timber.d("login Gobe: email: %s - password: = %s", email, password);
        GobeAuthorizationRequest request = new GobeAuthorizationRequest(email, password);
        mRegisterUserRequest.gobe_username = email;
        mRegisterUserRequest.gobe_password = password;
        mGenkiUserInfoModel.setGobeUsername(email);
        mGenkiUserInfoModel.setGobePassword(password);
        mGobeUseCase.execute(new LoginGobeSubscriber(), GobeParam.Factory.authorization(request));
    }

    @Override
    public void loginFacebook(LoginUserInfoModel loginUserInfoModel) {
        if (loginUserInfoModel == null) return;
        mView.showProgressBar();
        mRegisterUserRequest.last_name = loginUserInfoModel.getLastName();
        mRegisterUserRequest.first_name = loginUserInfoModel.getFirstName();
        mRegisterUserRequest.gender = loginUserInfoModel.getGender();
        mRegisterUserRequest.avatarPath = loginUserInfoModel.getAvatar();
        mRegisterUserRequest.email = loginUserInfoModel.getEmail();
        mRegisterUserRequest.birthday = loginUserInfoModel.getBirthDay();
        mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.SMARTPHONE.getValue();
        avatarUrl = loginUserInfoModel.getAvatar();
        if (!mRegisterUserRequest.mIsLoginFlag) {
            mRouter.openBirthdayRegisterScreen();
            return;
        }
        mUserUseCase.execute(new DownloadImageSubscriber(), UserParam.Factory.downloadImage(loginUserInfoModel.getAvatar()));
    }

    @Override
    public void loginGoogle(LoginUserInfoModel loginUserInfoModel) {
        if (loginUserInfoModel == null) return;
        mView.showProgressBar();
        mRegisterUserRequest.email = loginUserInfoModel.getEmail();
        mRegisterUserRequest.last_name = loginUserInfoModel.getLastName();
        mRegisterUserRequest.first_name = loginUserInfoModel.getFirstName();
        mRegisterUserRequest.birthday = loginUserInfoModel.getBirthDay();
        mRegisterUserRequest.latitude = loginUserInfoModel.getLatitude();
        mRegisterUserRequest.longitude = loginUserInfoModel.getLongitude();
        mRegisterUserRequest.gender = loginUserInfoModel.getGender();
        mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.SMARTPHONE.getValue();
        avatarUrl = loginUserInfoModel.getAvatar();
        if (StringUtil.isEmpty(avatarUrl)) {
            return;
        }
        mUserUseCase.execute(new DownloadImageSubscriber(), UserParam.Factory.downloadImage(loginUserInfoModel.getAvatar()));
    }

    @Override
    public void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (StringUtil.isEmpty(mRegisterUserRequest.username)) {
                mRegisterUserRequest.username = account.getDisplayName();
            }
            if (StringUtil.isEmpty(mRegisterUserRequest.last_name)) {
                mRegisterUserRequest.last_name = account.getDisplayName();
            }
            if (StringUtil.isEmpty(mRegisterUserRequest.first_name)) {
                mRegisterUserRequest.first_name = account.getDisplayName();
            }
            if (StringUtil.isEmpty(mRegisterUserRequest.email)) {
                mRegisterUserRequest.email = account.getDisplayName();
            }
            if (StringUtil.isEmpty(avatarUrl)) {
                avatarUrl = account.getPhotoUrl().toString();
            }

            if (StringUtil.isEmpty(avatarUrl)) {
                mRouter.openBirthdayRegisterScreen();
                return;
            }
            mUserUseCase.execute(new DownloadImageSubscriber(), UserParam.Factory.downloadImage(avatarUrl));
        } catch (ApiException e) {
            Timber.d("Login google failed %s", e.toString());
        }
    }

    private class FitbitAccessTokenSubscriber extends DefaultSubscriber<FitbitAccessTokenResponse> {

        @Override
        public void onNext(FitbitAccessTokenResponse fitbitAccessTokenResponse) {
            String auth_header = fitbitAccessTokenResponse.token_type + " " + fitbitAccessTokenResponse.access_token;
            Timber.d("Fitbit get access token success: %s", auth_header);
            FitbitUserProfileRequest request = new FitbitUserProfileRequest(auth_header);
            mRegisterUserRequest.fitbit_access_token = fitbitAccessTokenResponse.access_token;
            mRegisterUserRequest.fitbit_refresh_token = fitbitAccessTokenResponse.refresh_token;
            mRegisterUserRequest.fitbit_expires_in = fitbitAccessTokenResponse.expires_in;
            mGenkiUserInfoModel.setFitbitAccessToken(fitbitAccessTokenResponse.access_token);
            mGenkiUserInfoModel.setFitbitRefreshToken(fitbitAccessTokenResponse.refresh_token);
            mFitbitUseCase.execute(new FibitUserProfileSubscriber(), FitbitParam.Factory.getUserProfile(request));
        }

        @Override
        public void onError(Throwable t) {
            Timber.e("Fitbit token error: %s", t.getMessage());
            mView.showMessageDialog(mContext.getString(R.string.register_gobe_fitbit_gFit_error));
            mRouter.forceHideProgress();
        }
    }

    private class FibitUserProfileSubscriber extends DefaultSubscriber<FitbitUserProfileResponse> {
        @Override
        public void onNext(FitbitUserProfileResponse fitbitUserProfileResponse) {
            mView.hideProgressBar();
            Timber.d("Fitbit get user profile success: %s", fitbitUserProfileResponse.user.dateOfBirth);
            setUserInfo(fitbitUserProfileResponse);
            avatarUrl = fitbitUserProfileResponse.user.avatar;
            if (StringUtil.isEmpty(avatarUrl)) {
                mRouter.openBirthdayRegisterScreen();
                return;
            }
            mUserUseCase.execute(new DownloadImageSubscriber(), UserParam.Factory.downloadImage(fitbitUserProfileResponse.user.avatar));
        }

        @Override
        public void onError(Throwable t) {
            mRouter.forceHideProgress();
            mView.showMessageDialog(mContext.getString(R.string.register_gobe_fitbit_gFit_error));
            Timber.e("Fitbit user error: %s", t.getMessage());
        }
    }

    private void setUserInfo(FitbitUserProfileResponse fitbitUserProfileResponse) {
        if (StringUtil.isEmpty(mRegisterUserRequest.first_name)) {
            mRegisterUserRequest.first_name = fitbitUserProfileResponse.user.firstName;
        }
        if (StringUtil.isEmpty(mRegisterUserRequest.last_name)) {
            mRegisterUserRequest.last_name = fitbitUserProfileResponse.user.lastName;
        }
        mRegisterUserRequest.birthday = fitbitUserProfileResponse.user.dateOfBirth;
        mRegisterUserRequest.height = fitbitUserProfileResponse.user.height;
        mRegisterUserRequest.weight = fitbitUserProfileResponse.user.weight;
        mRegisterUserRequest.gender = fitbitUserProfileResponse.user.gender.equals("MALE") ? 1 : 2;
        mRegisterUserRequest.health_source = RegisterUserRequest.HealthSource.FITBIT.getValue();
    }

    private class DownloadImageSubscriber extends DefaultSubscriber<ResponseBody> {
        @Override
        public void onNext(ResponseBody responseBody) {
            String fileName = ImageUtils.getFileNameFromURL(avatarUrl);
            if (fileName.length() == 0 && avatarUrl.startsWith(AVATAR_URL_FACEBOOK)) {
                String raw = avatarUrl.substring(27);
                fileName = avatarUrl.substring(raw.indexOf(0, raw.indexOf("/"))) + ".png";
            }
            WriteFileAsync async = new WriteFileAsync(mContext, fileName, responseBody, new WriteFileAsync.OnDownloadCallback() {
                @Override
                public void onDownloadDone(String path) {
                    Timber.d("file name, %s", path);
                    mView.hideProgressBar();
                    if (StringUtil.isEmpty(mRegisterUserRequest.avatarPath)) {
                        mRegisterUserRequest.avatarPath = path;
                    }
                    if (!mRegisterUserRequest.mIsLoginFlag) {
                        mRouter.openBirthdayRegisterScreen();
                    }
                }

                @Override
                public void onDownloadFailure(Throwable throwable) {
                    Timber.d("Download failed");
                }
            });
            async.execute();
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }
    }

    private class LoginGobeSubscriber extends DefaultSubscriber<GobeAuthorizationResponse> {
        @Override
        public void onNext(GobeAuthorizationResponse gobeAuthorizationResponse) {
            mView.hideProgressBar();
            mRegisterUserRequest.mIsLoginFlag = false;
            Timber.d("Login Gobe  success ");
            setUserInfo(gobeAuthorizationResponse);
            avatarUrl = gobeAuthorizationResponse.user_info.avatar_url;
            if (StringUtil.isEmpty(avatarUrl)) {
                mRouter.openBirthdayRegisterScreen();
                return;
            }
            mUserUseCase.execute(new DownloadImageSubscriber(), UserParam.Factory.downloadImage(gobeAuthorizationResponse.user_info.avatar_url));

        }

        @Override
        public void onError(Throwable t) {
            Timber.e("Login Gobe error: %s", t.getMessage());
            mRouter.forceHideProgress();
            mView.showMessageDialog(mContext.getString(R.string.register_gobe_fitbit_gFit_error));
        }
    }

    private void setUserInfo(GobeAuthorizationResponse gobeAuthorizationResponse) {
        Timber.d("set user info");
        if (StringUtil.isEmpty(mRegisterUserRequest.last_name)) {
            mRegisterUserRequest.last_name = gobeAuthorizationResponse.user_info.lastname;
        }
        if (StringUtil.isEmpty(mRegisterUserRequest.first_name)) {
            mRegisterUserRequest.first_name = gobeAuthorizationResponse.user_info.firstname;
        }
        if (StringUtil.isEmpty(mRegisterUserRequest.email)) {
            mRegisterUserRequest.email = gobeAuthorizationResponse.user_info.account;
        }
        mRegisterUserRequest.weight = gobeAuthorizationResponse.user_info.weight;
        mRegisterUserRequest.height = gobeAuthorizationResponse.user_info.height;
        mRegisterUserRequest.gender = gobeAuthorizationResponse.user_info.sex;

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, gobeAuthorizationResponse.user_info.b_year);
        calendar.set(Calendar.MONTH, gobeAuthorizationResponse.user_info.b_month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, gobeAuthorizationResponse.user_info.b_day);
        mRegisterUserRequest.birthday = DateUtil.getYearMonthDayForApi(calendar);
    }
}
