package com.genkimiru.app.presentation.challenge_detail_history;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface ChallengeDetailActivityContract {

    interface ChallengeDetailPresenter extends BasePresenter {

    }

    interface ChallengeDetailView {

    }

    interface ChallengeDetailRouter {

    }
}
