package com.genkimiru.app.presentation.edit_profile;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityContract.EditProfileRouter;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityContract.EditProfileView;

import java.io.File;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class EditProfileActivityPresenter extends BasePresenterImp<EditProfileView, EditProfileRouter>
        implements EditProfileActivityContract.EditProfilePresenter {
    private UserUseCase mUserUseCase;
    private GenkiUserInfoModel mGenkiUserInfoModel;
    private String firstname, lastname, email, nickname;
    private File mAvatar;

    public EditProfileActivityPresenter(Context context, EditProfileView editProfileView, EditProfileRouter editProfileRouter, UserUseCase userUseCase, GenkiUserInfoModel genkiUserInfoModel) {
        super(context, editProfileView, editProfileRouter);
        this.mUserUseCase = userUseCase;
        this.mGenkiUserInfoModel = genkiUserInfoModel;
    }

    @Override
    public void updateUserInfo(String nickname, String firstName, String lastName, String email) {
        this.firstname = firstName;
        this.lastname = lastName;
        this.nickname = nickname;
        this.email = email;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token, email, firstName, lastName, mGenkiUserInfoModel.getHealthSource())
                .setNickname(nickname)
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(), mGenkiUserInfoModel.getGobePassword())
                .setGender(mGenkiUserInfoModel.getGender())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .create();
        mUserUseCase.execute(new UpdateUserInfoSubscriber(), (UserParam.Factory.updateUserProfile(request)));
    }

    @Override
    public void updateAvatar(File avatar) {
        mView.showProgressBar();
        mAvatar = avatar;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        UserInfoRequest request = new UserInfoRequest.Builder(
                token,
                mGenkiUserInfoModel.getEmail(),
                mGenkiUserInfoModel.getFirstName(),
                mGenkiUserInfoModel.getLastName(),
                mGenkiUserInfoModel.getHealthSource())
                .setHeight(mGenkiUserInfoModel.getHeight())
                .setBirthday(mGenkiUserInfoModel.getBirthday())
                .setGender(mGenkiUserInfoModel.getGender())
                .setWeight(mGenkiUserInfoModel.getWeight())
                .setGoBe(mGenkiUserInfoModel.getGobeUsername(), mGenkiUserInfoModel.getGobePassword())
                .setHeightType(mGenkiUserInfoModel.getHeightType())
                .setWeightType(mGenkiUserInfoModel.getWeightType())
                .setSurgeryStatus(mGenkiUserInfoModel.getSurgeryStatus())
                .setFeelCondition(mGenkiUserInfoModel.getFeelCondition())
                .setSmokingLevel(mGenkiUserInfoModel.getSmokingLevel())
                .setAlcohol(mGenkiUserInfoModel.getAlcoholLevel())
                .setBloodType(mGenkiUserInfoModel.getBloodType())
                .setLatitude(mGenkiUserInfoModel.getLatitude())
                .setLongtitude(mGenkiUserInfoModel.getLongitude())
                .setNickname(mGenkiUserInfoModel.getNickName())
                .setAddress(mGenkiUserInfoModel.getAddress())
                .setSmokingEachTime(mGenkiUserInfoModel.getSmokingEachTime())
                .setDrinkEachTime(mGenkiUserInfoModel.getDrinkEachTime())
                .setFitBitAccessToken(mGenkiUserInfoModel.getFitbitAccessToken())
                .setFitBitRefreshToken(mGenkiUserInfoModel.getFitbitRefreshToken())
                .setBloodPressureUnder(mGenkiUserInfoModel.getBloodPressureUnder())
                .setBloodPressureUpper(mGenkiUserInfoModel.getBloodPressureUpper())
                .setAvatar(mAvatar)
                .create();

        mUserUseCase.execute(new updateUserAvatarSubscriber(), (UserParam.Factory.updateUserAvatar(request)));
    }

    private class UpdateUserInfoSubscriber extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean aBoolean) {
            mGenkiUserInfoModel.setFirstName(firstname);
            mGenkiUserInfoModel.setLastName(lastname);
            mGenkiUserInfoModel.setEmail(email);
            mGenkiUserInfoModel.setNickName(nickname);
            mView.onUpdateUserInfoSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onUpdateUserInfoError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }

    private class updateUserAvatarSubscriber extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean aBoolean) {
            String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
            mUserUseCase.execute(new UserInformationSubscriber(), UserParam.Factory.getUserInformation(token));
        }

        @Override
        public void onError(Throwable throwable) {
            mView.hideProgressBar();
            mView.onUpdateUserInfoError(ApiErrorHandler.getMessage(mContext, throwable, R.string.update_error));
        }
    }

    private class UserInformationSubscriber extends DefaultSubscriber<UserInfoResponse> {

        @Override
        public void onNext(UserInfoResponse userInfoResponse) {
            Timber.d("Get user info success: %s", userInfoResponse.username);
            mGenkiUserInfoModel.setValueFromResponse(userInfoResponse);
            mView.hideProgressBar();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Get user info error: %s", throwable.getMessage());
            mView.hideProgressBar();
        }
    }
}
