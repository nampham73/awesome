package com.genkimiru.app.presentation.faq;

import android.content.Context;

import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.remote.extension.response.ArticleResponse;
import com.genkimiru.app.domain.bugreport.BugReportParam;
import com.genkimiru.app.domain.bugreport.BugReportUseCase;

import java.util.List;
import java.util.Locale;

import timber.log.Timber;

import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqPresenter;
import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqRouter;
import static com.genkimiru.app.presentation.faq.FaqActivityContract.FaqView;

public class FaqActivityPresenter extends BasePresenterImp<FaqView, FaqRouter> implements FaqPresenter {

    private BugReportUseCase mBugReportUseCase;

    public FaqActivityPresenter(Context context,
                                FaqView faqView,
                                FaqRouter faqRouter,
                                BugReportUseCase bugReportUseCase) {
        super(context, faqView, faqRouter);
        this.mBugReportUseCase = bugReportUseCase;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBugReportUseCase.clear();
    }

    @Override
    public void onStartListFaq() {
        String code;
        Locale locale = mContext.getResources().getConfiguration().locale;
        if (locale != null && locale.getLanguage().equals("ja")) {
            code = "ja";
        } else {
            code = "en-US";
        }
        mBugReportUseCase.execute(new FaqSubcriber(), BugReportParam.Factory.getFaq(code));

    }

    private class FaqSubcriber extends DefaultSubscriber<List<ArticleResponse>> {
        @Override
        public void onNext(List<ArticleResponse> articleResponses) {
            Timber.d("Get Faq Success :%s" ,articleResponses);
            mView.onGetSuccess(articleResponses);
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Get Faq Fail");
            mView.onGetFail();
        }
    }

}
