package com.genkimiru.app.presentation.register.blood_pressure_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressureFragmentRouter;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressureFragmentView;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressurePresenter;

public class BloodPressureFragmentPresenter extends BasePresenterImp<BloodPressureFragmentView,
        BloodPressureFragmentRouter> implements BloodPressurePresenter {

    private UserUseCase mUserUseCase;

    public BloodPressureFragmentPresenter(Context context, BloodPressureFragmentView bloodPressureFragmentView,
                                          BloodPressureFragmentRouter bloodPressureFragmentRouter, UserUseCase userUseCase) {
        super(context, bloodPressureFragmentView, bloodPressureFragmentRouter);
        mUserUseCase = userUseCase;
    }


    @Override
    public String validateBloodPressureUpper(String bloodPresSureUpper) {
        if (Float.parseFloat(bloodPresSureUpper) < 1) {
            bloodPresSureUpper = "1";
        }
        return bloodPresSureUpper;
    }

    @Override
    public String validateBloodPressureUnder(String bloodPresSureUnder) {
        if (Float.parseFloat(bloodPresSureUnder) < 1) {
            bloodPresSureUnder = "1";
        }
        return bloodPresSureUnder;
    }
}
