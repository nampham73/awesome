package com.genkimiru.app.presentation.main.contest_fragment.publics;

import com.genkimiru.app.base.scope.ChildFragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PublicContestFragmentBuilder {

    @ChildFragmentScope
    @ContributesAndroidInjector(modules = PublicContestFragmentModule.class)
    abstract PublicsContestFragment publicsContestFragment();
}
