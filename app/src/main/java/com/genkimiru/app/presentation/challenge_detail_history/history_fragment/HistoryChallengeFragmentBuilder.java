package com.genkimiru.app.presentation.challenge_detail_history.history_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HistoryChallengeFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = HistoryChallengeFragmentModule.class)
   abstract HistoryChallengeFragment historyChallengeFragment();
}
