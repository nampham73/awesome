package com.genkimiru.app.presentation.main.contest_fragment.adapter;

import android.content.Context;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.data.model.GenkiContestModel;

public class ContestAdapter extends BaseSimpleAdapter<GenkiContestModel, ContestViewHolder> {

    private Context mContext;

    public ContestAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.holder_contest_main;
    }

    @Override
    public ContestViewHolder createHolder(View view) {
        return new ContestViewHolder(mContext, view);
    }

    @Override
    public void releaseAll() {
        super.releaseAll();
        notifyDataSetChanged();
    }
}
