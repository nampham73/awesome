package com.genkimiru.app.presentation.nac.fragment.week;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NacWeekFragmentContract {
    interface NacWeekPresent extends BasePresenter{
            void onUpdateNacWeek();
    }
    interface NacWeekView {
            void onUpdateSuccess();
            void onUpdateError();
    }
    interface NacWeekRouter{

    }
}
