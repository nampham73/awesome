package com.genkimiru.app.presentation.main.dashboard_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;
import com.genkimiru.app.data.model.GenkiDashboardDataModel;
import com.genkimiru.app.data.model.GenkiDetailChartDataModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.widget.SleepChartView;

import java.util.Calendar;
import java.util.List;

public interface DashboardFragmentContract {

    interface DashboardPresenter extends BasePresenter {
        void requestAsyncCommonData();
        void requestAsyncSleepData();
        void requestStepMonthData();
    }

    interface DashboardView{
        void onPrepareLoadingData();
        void onDataLoadDone(GenkiDashboardDataModel model);
        void onDataLoadFailure(String message);
        void onLoadSleepDataFailure(String message);
        void onLoadSleepDataDone(List<SleepChartView.Entry> entries);
        void onLoadMonthStepSuccess(GenkiStepModel genkiStepModel);
        void onLoadMonthStepFailure(String message);
    }

    interface DashboardRouter {
        void onPrepareToSyncSleepData();
        void onPreparetoSyncStepData();
        void goChatWithWatson();
        void goECommerce();
        void showMonthHealthCarePoint(GenkiStepModel genkiStepModel);
    }
}
