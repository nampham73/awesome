package com.genkimiru.app.presentation.weight.weight_adapter;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;
import com.genkimiru.app.data.model.WeightModel;

import butterknife.BindView;

public class WeightViewHolder extends BaseSimpleViewHolder<WeightModel> {
    @BindView(R.id.weight_info_weight_txt)
    TextView mWeightTxt;
    @BindView(R.id.weight_info_time_txt)
    TextView mTimeTxt;


    public WeightViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(WeightModel weightModel) {
        mWeightTxt.setText(String.valueOf(weightModel.getWeight()));
        mTimeTxt.setText(weightModel.getTime());
    }
}
