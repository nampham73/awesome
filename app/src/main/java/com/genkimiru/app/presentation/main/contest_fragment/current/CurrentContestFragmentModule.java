package com.genkimiru.app.presentation.main.contest_fragment.current;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ChildFragmentScope;
import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.Provides;

import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.contest.ContestRepository;
import com.genkimiru.app.data.repository.contest.ContestRepositoryImp;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestContract.*;

@Module
public class CurrentContestFragmentModule {

    @Provides
    @ChildFragmentScope
    CurrentContestPresenter providePresenter(Context context, CurrentContestView currentContestView, CurrentContestRouter router, ContestUseCase useCase) {
        return new CurrentContestFragmentPresenter(context, currentContestView, router, useCase);
    }

    /*@Provides
    @ChildFragmentScope
    ContestUseCase provideContestUseCase(SchedulerProvider schedulerProvider, ContestRepository repository) {
        return new ContestUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }

    @Provides
    @ChildFragmentScope
    ContestRepository provideContestRepository(GenkiApi api) {
        return new ContestRepositoryImp(api);
    }*/

    @Provides
    @ChildFragmentScope
    CurrentContestView provideView(CurrentContestFragment view) {
        return view;
    }

    @Provides
    @ChildFragmentScope
    CurrentContestRouter provideRouter(CurrentContestFragment router) {
        return router;
    }

    @Provides
    @ChildFragmentScope
    ContestAdapter provideAdapter(CurrentContestFragment fragment) {
        return new ContestAdapter(fragment.getActivity());
    }
}
