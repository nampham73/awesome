package com.genkimiru.app.presentation.main.dashboard_fragment;

import android.content.Context;

import com.genkimiru.app.Constants;
import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiDashboardDataModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.DashboardRequest;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.domain.mydata.MyDataParam;
import com.genkimiru.app.domain.mydata.MyDataUseCase;
import com.genkimiru.app.widget.SleepChartView;

import java.util.Calendar;
import java.util.List;

import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.DashboardPresenter;
import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.DashboardRouter;
import static com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentContract.DashboardView;

public class DashboardFragmentPresenter
        extends BasePresenterImp<DashboardView, DashboardRouter>
        implements DashboardPresenter {

    private MyDataUseCase mMyDataUseCase;

    public DashboardFragmentPresenter(Context context,
                                      DashboardView dashboardView,
                                      DashboardRouter dashboardRouter,
                                      MyDataUseCase useCase) {
        super(context, dashboardView, dashboardRouter);
        mMyDataUseCase = useCase;
    }

    @Override
    public void requestAsyncCommonData() {
        mView.onPrepareLoadingData();
        String token = GenkiApplication.getPreferences().getString(Constants.Preference.USER_TOKEN).get();
        long timestamp = DateUtil.getCurrentTimeRound(5);
        DashboardRequest request = new DashboardRequest(token, timestamp);
        mMyDataUseCase.execute(new OnDashboardSubscription(), MyDataParam.Factory.fetchingDashboard(request));
    }

    @Override
    public void requestAsyncSleepData() {
        mRouter.onPrepareToSyncSleepData();
        String token = GenkiApplication.getPreferences().getString(Constants.Preference.USER_TOKEN).get();
        DetailChartDataRequest request = new DetailChartDataRequest.Builder(token)
                .setStartDate(DateUtil.getMidnight() / 1000L)
                .setEndDate(DateUtil.getToday() / 1000L)
                .setChartType(DetailChartDataRequest.ChartType.MIN_5)
                .setDataType(DetailChartDataRequest.DataType.SLEEP)
                .create();
        mMyDataUseCase.execute(new OnSleepChartDataSubscription(), MyDataParam.Factory.fetchingSleepChartData(request));
    }

    @Override
    public void requestStepMonthData() {
        mRouter.onPreparetoSyncStepData();
        Calendar startCalendar = DateUtil.today();
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        startCalendar.set(Calendar.HOUR_OF_DAY, 0);
        startCalendar.set(Calendar.MINUTE, 0);
        startCalendar.set(Calendar.SECOND, 0);
        startCalendar.set(Calendar.MILLISECOND, 0);
        long startDay = startCalendar.getTimeInMillis() / 1000L;
        long endDay = DateUtil.getToday() / 1000L;
        String token = GenkiApplication.getPreferences().getString(Constants.Preference.USER_TOKEN).get();
        DetailChartDataRequest request = new DetailChartDataRequest.Builder(token)
                .setStartDate(startDay)
                .setEndDate(endDay)
                .setDataType(DetailChartDataRequest.DataType.STEP)
                .setChartType(DetailChartDataRequest.ChartType.MONTHLY)
                .create();
        mMyDataUseCase.execute(new OnStepDataSubscription(), MyDataParam.Factory.fetchingStepChartData(request));
    }

    private class OnStepDataSubscription extends DefaultSubscriber<List<GenkiStepModel>> {
        @Override
        public void onNext(List<GenkiStepModel> genkiStepModels) {
            if (genkiStepModels == null || genkiStepModels.size() == 0) {
                return;
            }
            mView.onLoadMonthStepSuccess(genkiStepModels.get(0));
        }

        @Override
        public void onError(Throwable t) {
            mView.onLoadMonthStepFailure(ApiErrorHandler.getMessage(mContext, t, R.string.healthy_dashboard_no_data));
        }
    }

    private class OnDashboardSubscription extends DefaultSubscriber<GenkiDashboardDataModel> {
        @Override
        public void onNext(GenkiDashboardDataModel genkiDashboardDataModel) {
            super.onNext(genkiDashboardDataModel);
            mView.onDataLoadDone(genkiDashboardDataModel);
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onDataLoadFailure(ApiErrorHandler.getMessage(mContext, throwable, R.string.sleep_dashboard_timeout));
        }
    }

    private class OnSleepChartDataSubscription extends DefaultSubscriber<List<SleepChartView.Entry>> {
        @Override
        public void onNext(List<SleepChartView.Entry> entries) {
            mView.onLoadSleepDataDone(entries);
        }

        @Override
        public void onError(Throwable throwable) {
            mView.onLoadSleepDataFailure(ApiErrorHandler.getMessage(mContext, throwable, R.string.sleep_dashboard_no_data));
        }
    }
}
