package com.genkimiru.app.presentation.walking_challenge_map;

import android.content.Context;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapPresenter;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapRouter;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapView;

import dagger.Module;
import dagger.Provides;

@Module
public class WalkingChallengeMapActivityModule {

    @Provides
    @ActivityScope
    WalkingChallengeMapView provideView(WalkingChallengeMapActivity walkingChallengeMapActivity) {
        return walkingChallengeMapActivity;
    }

    @Provides
    @ActivityScope
    WalkingChallengeMapRouter provideRouter(WalkingChallengeMapActivity walkingChallengeMapActivity) {
        return walkingChallengeMapActivity;
    }

    @Provides
    @ActivityScope
    WalkingChallengeMapPresenter providePresenter(Context context, WalkingChallengeMapView walkingChallengeMapView, WalkingChallengeMapRouter walkingChallengeMapRouter) {
        return new WalkingChallengeMapActivityPresenter(context, walkingChallengeMapView, walkingChallengeMapRouter);
    }
}
