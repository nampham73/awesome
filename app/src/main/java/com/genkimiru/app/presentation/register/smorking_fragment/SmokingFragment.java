package com.genkimiru.app.presentation.register.smorking_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.dialog.CheckboxDialog;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingFragmentRouter;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingFragmentView;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentContract.SmokingPresenter;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class SmokingFragment extends SimpleBaseFragment<SmokingPresenter> implements SmokingFragmentView, SmokingFragmentRouter {

    @BindView(R.id.register_smorking_radio_group)
    RadioGroup mRadioGroup;
    @BindView(R.id.checkbox_sometime_smoking_tv)
    TextView mSometimeSmokingTxt;
    @BindView(R.id.checkbox_only_smoking_tv)
    TextView mOnlySmokingTxt;

    @Inject
    SmokingPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    private CheckboxDialog smorkingDialog;
    private RegisterActivity mRegisterActivity;
    private Disposable mDisposable;
    private String mLevelTxt;
    private int mCurrentSmokingButtonId;
    private boolean isDialogShowing = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_healthy_smoking;
    }

    @Override
    protected SmokingPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialDefaultValue();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurrentSmokingButtonId = mRadioGroup.getCheckedRadioButtonId();
        mDisposable = RxRadioGroup.checkedChanges(mRadioGroup)
                .skip(1) // Skip init value
                .subscribe(buttonId -> {
                    mCurrentSmokingButtonId = buttonId;
                    switch (buttonId) {
                        case R.id.checkbox_no_smoking:
                            Timber.d("Smoking level 1");
                            mRegisterUserRequest.smoking_level = RegisterUserRequest.SmokingLevel.LEVEL_NO_SMOKING.getValue();
                            mRegisterUserRequest.smoking_each_time = RegisterUserRequest.SmokingEachTime.LEVEL_SMALL.getValue();
                            setDefaultView();
                            break;
                        case R.id.checkbox_sometime_smoking:
                            Timber.d("Smoking level 2");
                            mRegisterUserRequest.smoking_level = RegisterUserRequest.SmokingLevel.LEVEL_SOMETIME_SMOKING.getValue();;
                            openSmokingDialog();
                            break;
                        case R.id.checkbox_only_smoking:
                            Timber.d("Smoking level 3");
                            mRegisterUserRequest.smoking_level = RegisterUserRequest.SmokingLevel.LEVEL_ALLWAYS_SMOKING.getValue();;
                            openSmokingDialog();
                            break;
                    }
                });
        setTextLevelEachTime();
    }

    private void openSmokingDialog() {
        Timber.d("openSmokingDialog");
        if (isDialogShowing) {
            return;
        }

        smorkingDialog = new CheckboxDialog.CheckboxDialogBuilder()
                .setTitle(getString(R.string.register_smoking_title_dialog))
                .setLayout(R.layout.dialog_checbox_smoking_content)
                .dialogListener(new CheckboxDialog.DialogListener() {
                    @Override
                    public void onComplete(CheckboxDialog checkboxDialog) {
                        setTextLevelEachTime();
                        checkboxDialog.dismiss();
                    }

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.register_smoking_small_rb:
                                Timber.d("Smoking level each time: %d", 1);
                                mRegisterUserRequest.smoking_each_time = RegisterUserRequest.SmokingEachTime.LEVEL_SMALL.getValue();
                                mLevelTxt = getString(R.string.register_smoking_small);
                                break;
                            case R.id.register_smoking_medium_rb:
                                Timber.d("Smoking level each time: %d", 2);
                                mRegisterUserRequest.smoking_each_time = RegisterUserRequest.SmokingEachTime.LEVEL_MEDIUM.getValue();
                                mLevelTxt = getString(R.string.register_smoking_medium);
                                break;
                            case R.id.register_smoking_large_rb:
                                Timber.d("Smoking level each time: %d", 3);
                                mRegisterUserRequest.smoking_each_time = RegisterUserRequest.SmokingEachTime.LEVEL_LARGE.getValue();
                                mLevelTxt = getString(R.string.register_smoking_large);
                                break;
                        }
                    }

                    @Override
                    public void onOpen(int checkedId) {
                        mRegisterUserRequest.smoking_each_time = RegisterUserRequest.SmokingEachTime.LEVEL_SMALL.getValue();
                        mLevelTxt = getString(R.string.register_smoking_small);
                    }

                    @Override
                    public void onDismiss() {
                        isDialogShowing = false;
                    }
                }).build();
        smorkingDialog.show(mRegisterActivity.getSupportFragmentManager());
        isDialogShowing = true;
    }

    private void setTextLevelEachTime() {
        switch (mCurrentSmokingButtonId) {
            case R.id.checkbox_sometime_smoking:
                mSometimeSmokingTxt.setText(("(" + mLevelTxt + ")"));
                mSometimeSmokingTxt.setVisibility(View.VISIBLE);
                mOnlySmokingTxt.setVisibility(View.GONE);
                break;
            case R.id.checkbox_only_smoking:
                mOnlySmokingTxt.setText("(" + mLevelTxt + ")");
                mSometimeSmokingTxt.setVisibility(View.GONE);
                mOnlySmokingTxt.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initialDefaultValue() {
        mRegisterUserRequest.smoking_level = RegisterUserRequest.SmokingLevel.LEVEL_NO_SMOKING.getValue();
        mRegisterUserRequest.smoking_each_time = RegisterUserRequest.SmokingEachTime.LEVEL_DEFAULT.getValue();
    }

    private void setDefaultView() {
        mSometimeSmokingTxt.setVisibility(View.GONE);
        mOnlySmokingTxt.setVisibility(View.GONE);
    }
}
