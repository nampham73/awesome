package com.genkimiru.app.presentation.main.contest_fragment.privates;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

import com.genkimiru.app.base.scope.ChildFragmentScope;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;
import com.genkimiru.app.presentation.main.contest_fragment.privates.PrivatesContestContract.*;

@Module
public class PrivatesContestFragmentModule {

    @ChildFragmentScope
    @Provides
    PrivateContestPresenter providePresenter(Context context, PrivateContestView view, PrivateContestRouter router, ContestUseCase useCase) {
        return new PrivatesContestFragmentPresenter(context, view, router, useCase);
    }

    @ChildFragmentScope
    @Provides
    PrivateContestView provideView(PrivatesContestFragment view) {
        return view;
    }

    @ChildFragmentScope
    @Provides
    PrivateContestRouter provideRouter(PrivatesContestFragment router) {
        return router;
    }

    @ChildFragmentScope
    @Provides
    ContestAdapter provideAdapter(Context context) {
        return new ContestAdapter(context);
    }


}
