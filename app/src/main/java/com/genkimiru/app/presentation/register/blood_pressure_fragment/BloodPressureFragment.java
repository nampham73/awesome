package com.genkimiru.app.presentation.register.blood_pressure_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressureFragmentRouter;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressureFragmentView;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentContract.BloodPressurePresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class BloodPressureFragment extends SimpleBaseFragment<BloodPressurePresenter>
        implements BloodPressureFragmentView, BloodPressureFragmentRouter {

    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_blood_pressure_upper_edt)
    EditText mBloodPressureUpperEdt;
    @BindView(R.id.register_blood_pressure_under_edt)
    EditText mBloodPressureUnderEdt;

    @Inject
    BloodPressurePresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_bloodpressure;
    }

    @Override
    protected BloodPressurePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mBloodPressureUpperEdt)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(bloodPressure -> {
                    Timber.d("blood pressure: %s", bloodPressure.toString());
                    if (StringUtil.isEmpty(bloodPressure)) {
                        return;
                    }

                    bloodPressure = mPresenter.validateBloodPressureUpper(bloodPressure.toString().trim());
                    mBloodPressureUpperEdt.setText(bloodPressure);
                    mBloodPressureUpperEdt.setSelection(mBloodPressureUpperEdt.getText().length());
                    getBloodPressure();
                });
        RxTextView.textChanges(mBloodPressureUnderEdt)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(heartBeat -> {
                    Timber.d("blood pressure heart beat: %s", heartBeat.toString());
                    if (StringUtil.isEmpty(heartBeat)) {
                        return;
                    }

                    heartBeat = mPresenter.validateBloodPressureUnder(heartBeat.toString());
                    mBloodPressureUnderEdt.setText(heartBeat);
                    mBloodPressureUnderEdt.setSelection(mBloodPressureUnderEdt.getText().length());
                    getBloodPressure();
                });
    }

    public void getBloodPressure() {
        if (mBloodPressureUpperEdt.getText().toString().trim().length() <= 0
                || mBloodPressureUnderEdt.getText().toString().length() <= 0) {
            return;
        }
        mRegisterUserRequest.blood_pressure_upper = Integer.parseInt(mBloodPressureUpperEdt.getText().toString().trim());
        mRegisterUserRequest.blood_pressure_under = Integer.parseInt(mBloodPressureUnderEdt.getText().toString().trim());
    }
}
