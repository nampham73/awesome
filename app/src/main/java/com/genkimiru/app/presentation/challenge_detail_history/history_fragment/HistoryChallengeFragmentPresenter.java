package com.genkimiru.app.presentation.challenge_detail_history.history_fragment;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.ListPresenterImp;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.data.remote.genki.request.ListContestRequest;
import com.genkimiru.app.data.remote.genki.response.ContestModelResponse;
import com.genkimiru.app.domain.contest.ContestParam;
import com.genkimiru.app.domain.contest.ContestUseCase;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengePresenter;
import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengeRouter;
import static com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeContract.HistoryChallengeView;

public class HistoryChallengeFragmentPresenter extends ListPresenterImp<HistoryChallengeView, HistoryChallengeRouter>
        implements HistoryChallengePresenter {
    private ContestUseCase mContestUseCase;

    HistoryChallengeFragmentPresenter(Context context,
                                      HistoryChallengeView historyChallengeView,
                                      HistoryChallengeRouter historyChallengeRouter,
                                      ContestUseCase contestUseCase) {
        super(context, historyChallengeView, historyChallengeRouter);
        this.mContestUseCase = contestUseCase;
    }

    @Override
    public void start() {
        super.start();
        mView.onPrepareLoadingData();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        ListContestRequest request = new ListContestRequest.Builder(getNextPage(), getPostPerPage(), "name", "desc", token)
                .create();
        mContestUseCase.execute(new ListContestSubscriber(), ContestParam.Factory.getContestPrivateOrPublic(request));
        setLoadingRequesting(true);
    }

    private class ListContestSubscriber extends DefaultSubscriber<List<ContestModelResponse>> {
        @Override
        public void onNext(List<ContestModelResponse> response) {
            mView.onDataLoadDone(convert(response));
            setLoadingRequesting(false);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            setLoadingRequesting(false);
            Timber.e("fetching contest page main failed: %s", t.getMessage());
            mView.onLoadingDataFailure(t, R.string.response_contest_Page_loading_data_failure);
        }
    }

    private List<GenkiContestModel> convert(List<ContestModelResponse> responses) {
        List<GenkiContestModel> items = new ArrayList<>();
        if (responses != null && responses.size() > 0) {
            for (ContestModelResponse response : responses) {
                GenkiContestModel model = new GenkiContestModel();
                model.setValueFromResponse(response);
                items.add(model);
            }
        }
        reCalculatedPaging(items.size());
        return items;
    }

}
