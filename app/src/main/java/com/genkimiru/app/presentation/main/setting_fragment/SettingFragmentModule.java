package com.genkimiru.app.presentation.main.setting_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingPresenter;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingRouter;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingView;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingFragmentModule {

    @Provides
    @FragmentScope
    public SettingView provideView(SettingFragment settingFragment) {
        return settingFragment;
    }

    @Provides
    @FragmentScope
    public SettingRouter provideRouter(SettingFragment settingFragment) {
        return settingFragment;
    }

    @Provides
    @FragmentScope
    public SettingPresenter providePresenter(Context context, SettingView settingView, SettingRouter settingRouter) {
        return new SettingFragmentPresenter(context, settingView, settingRouter);
    }

}
