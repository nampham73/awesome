package com.genkimiru.app.presentation.register.birthday_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayFragmentRouter;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayFragmentView;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentContract.BirthdayPresenter;

public class BirthdayFragmentPresenter extends BasePresenterImp<BirthdayFragmentView,
        BirthdayFragmentRouter> implements BirthdayPresenter {

    private UserUseCase mUserUseCase;

    public BirthdayFragmentPresenter(Context context, BirthdayFragmentView birthdayFragmentView,
                                     BirthdayFragmentRouter birthdayFragmentRouter, UserUseCase userUseCase) {
        super(context, birthdayFragmentView, birthdayFragmentRouter);
        mUserUseCase = userUseCase;
    }
}

