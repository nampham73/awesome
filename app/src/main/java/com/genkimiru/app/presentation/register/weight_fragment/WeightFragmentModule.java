package com.genkimiru.app.presentation.register.weight_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightFragmentRouter;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightFragmentView;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class WeightFragmentModule {

    @Provides
    @FragmentScope
    WeightFragmentView provideView(WeightFragment weightFragment) {
        return weightFragment;
    }

    @Provides
    @FragmentScope
    WeightFragmentRouter provideRouter(WeightFragment weightFragment) {
        return weightFragment;
    }

    @Provides
    @FragmentScope
    WeightPresenter providePresenter(Context context, WeightFragmentView weightFragmentView, WeightFragmentRouter weightFragmentRouter, UserUseCase userUseCase) {
        return new WeightFragmentPresenter(context, weightFragmentView, weightFragmentRouter, userUseCase);
    }
}
