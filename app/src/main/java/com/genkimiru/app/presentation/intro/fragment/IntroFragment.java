package com.genkimiru.app.presentation.intro.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import timber.log.Timber;

public class IntroFragment extends SimpleBaseFragment {

    @BindView(R.id.intro_background_img)
    ImageView mBackgroundImage;
    @BindView(R.id.intro_slogan_description)
    TextView mTxtView;
    @BindView(R.id.intro_slogan_tv)
    TextView mTxtViewBlance;

    IntroFragmentAdapter.Type mType;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_intro;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public void setLayoutType(IntroFragmentAdapter.Type type) {
        mType = type;
    }

    public void initializeView() {
        Timber.d("initializeView: mType = %s", mType);
        if (mType == null) {
            return;
        }

        if (mType == IntroFragmentAdapter.Type.WATER) {
            mTxtView.setTextColor(getResources().getColor(android.R.color.black));
            mTxtViewBlance.setTextColor(getResources().getColor(android.R.color.darker_gray));
        }

        mBackgroundImage.setImageResource(mType.getBackgroundId());
        mTxtView.setText(mType.getTextId());
    }
}
