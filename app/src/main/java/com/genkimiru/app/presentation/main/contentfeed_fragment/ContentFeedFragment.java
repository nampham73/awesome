package com.genkimiru.app.presentation.main.contentfeed_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.ListFragment;
import com.genkimiru.app.data.model.GenkiContentModel;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.contentfeed_fragment.adapter.ContentFeedAdapter;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentContract.*;

public class ContentFeedFragment extends ListFragment<ContentFeedAdapter, List<GenkiContentModel>, ContentFeedPresenter>
        implements ContentFeedView, ContentFeedRouter {

    @BindView(R.id.no_data_tv)
    TextView mNoContestTv;

    @Inject
    ContentFeedPresenter mPresenter;
    @BindView(R.id.content_feed_main_favorite_cbx)
    CheckBox mFavoriteCbx;

    private CompositeDisposable mCompositeDisposable;
    private MainActivity mainActivity;

    public static ContentFeedFragment newInstance() {
        return new ContentFeedFragment();
    }

    @Override
    protected boolean canSwipeToRefresh() {
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    public void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @NonNull
    @Override
    protected ContentFeedAdapter createAdapter() {
        return mAdapter = new ContentFeedAdapter(getActivity());
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main_content_feed;
    }

    @Override
    protected ContentFeedPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onLoadingDataFailure(Throwable throwable, String message) {
        super.onLoadingDataFailure(throwable, message);
    }

    @Override
    public void onDataLoadDone(List<GenkiContentModel> genkiContentModels) {
        super.onDataLoadDone(genkiContentModels);
        mAdapter.items(genkiContentModels);
        mAdapter.notifyDataSetChanged();
        mNoContestTv.setVisibility(mAdapter.getItemCount() <= 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void showProgress() {
        mainActivity.showProgress();
    }

    @Override
    protected void hideProgress() {
        mainActivity.hideProgress();
    }

    @Override
    protected ContentFeedPresenter getPresenter() {
        return mPresenter;
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(
                RxCompoundButton.checkedChanges(mFavoriteCbx)
                        .skip(1)
                        .subscribe(isChecked -> {
                            Timber.d("On click favorite change: %s", isChecked);
                            mPresenter.setIsLoadFavoriteList(isChecked);
                        }));
    }

    @Override
    public void resetView() {
        Timber.d("Reset view");
        mAdapter.releaseAll();
    }

    @Override
    public void onRefresh() {
       mAdapter.releaseAll();
       super.onRefresh();
    }
}
