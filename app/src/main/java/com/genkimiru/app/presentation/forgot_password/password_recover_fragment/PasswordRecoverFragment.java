package com.genkimiru.app.presentation.forgot_password.password_recover_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivity;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragmentContract.PasswordRecoverPresenter;
import static com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragmentContract.PasswordRecoverRouter;
import static com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragmentContract.PasswordRecoverView;

public class PasswordRecoverFragment extends SimpleBaseFragment<PasswordRecoverPresenter>
        implements PasswordRecoverView, PasswordRecoverRouter {

    @Inject
    PasswordRecoverPresenter mPresenter;

    @BindView(R.id.recover_password_email_edt)
    TextInputEditText mEmailEdt;
    @BindView(R.id.recover_password_send_btn)
    Button mSendBtn;

    private ForgotPasswordActivity mActivity;
    private TextDialogFragment confirmDialogFragment;
    private boolean mIsValidEmail;
    private CompositeDisposable mCompositeDisposable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ForgotPasswordActivity) getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissConfirmDialog();
        mCompositeDisposable.clear();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_recover_password;
    }

    @Override
    protected PasswordRecoverPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @OnClick(R.id.recover_password_send_btn)
    void onSendClick() {
        if (!mIsValidEmail) {
            showMessageDialog(getString(R.string.forgot_password_invalid_email));
            return;
        }
        mPresenter.onSendClick(mEmailEdt.getText().toString().trim());
    }

    @Override
    public void openNewPasswordScreen() {
        mActivity.openNewPasswordScreen();
    }

    @Override
    public void openSentDialog() {
        confirmDialogFragment = new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .message(getString(R.string.forgot_password_recover_sent_message))
                .leftButtonText(getString(android.R.string.ok))
                .onResultListener(new TextDialogFragment.OnResultListener() {
                    @Override
                    public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                        openNewPasswordScreen();
                    }

                    @Override
                    public void onClickRightButton(TextDialogFragment textDialogFragment) {
                        // Do nothing
                    }
                })
                .build();
        confirmDialogFragment.show(mActivity.getSupportFragmentManager());
    }

    @Override
    public void showMessageDialog(String message) {
        MessageUtil.createMessageDialog(message).show(mActivity.getSupportFragmentManager());
    }

    @Override
    public void showLoadingDialog() {
        mActivity.showProgress();
    }

    @Override
    public void dismissLoadingDialog() {
        mActivity.hideProgress();
    }

    private void dismissConfirmDialog() {
        if (confirmDialogFragment == null) {
            return;
        }
        confirmDialogFragment.dismiss();
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(
                RxTextView.textChanges(mEmailEdt)
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(email -> {
                            Timber.d("email: %s", email.toString());
                            if (StringUtil.isEmpty(email)) {
                                mSendBtn.setEnabled(false);
                                mSendBtn.setTextColor(mActivity.getResources().getColor(R.color.transparent_20_black));
                            } else {
                                mSendBtn.setEnabled(true);
                                mSendBtn.setTextColor(mActivity.getResources().getColor(R.color.white));
                            }
                            mIsValidEmail = StringUtil.validateEmailFormat(email.toString().trim());
                        }));
    }
}
