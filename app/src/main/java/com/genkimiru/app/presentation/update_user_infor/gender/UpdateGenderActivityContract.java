package com.genkimiru.app.presentation.update_user_infor.gender;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateGenderActivityContract {
    interface UpdateGenderPresenter extends BasePresenter {
        void updateGender(int gender);
    }

    interface UpdateGenderView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateGenderRouter {
    }
}
