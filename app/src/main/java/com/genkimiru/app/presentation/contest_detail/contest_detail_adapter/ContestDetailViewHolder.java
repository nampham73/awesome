package com.genkimiru.app.presentation.contest_detail.contest_detail_adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genkimiru.app.Constants;
import com.bumptech.glide.Glide;
import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;
import com.genkimiru.app.base.widget.CustomProgressBar;
import com.genkimiru.app.data.model.GenkiChallengeModel;
import com.genkimiru.app.data.model.GenkiContestDetailModel;
import com.genkimiru.app.presentation.challenge_detail_history.ChallengeDetailHistoryActivity;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_COUNT;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_DESCRIPTION;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_STATUS;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_TARGET;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_HISTORY_TITLE;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_USER_CONTEST_CHALLENGE_ID;
import static com.genkimiru.app.Constants.Extra.CHALLENGE_USER_DATA;

public class ContestDetailViewHolder extends BaseSimpleViewHolder<GenkiChallengeModel> implements View.OnClickListener {

    private static final int MAX_WINNER_SHOW = 3;

    @BindView(R.id.contest_detail_challenge_description_tv)
    TextView mDescriptionTxt;
    @BindView(R.id.contest_detail_challenge_title_tv)
    TextView mTitleTxt;
    @BindView(R.id.arc_progress)
    CustomProgressBar mCustomProgressBar;
    @BindView(R.id.contest_detail_challenge_join_now_tv)
    TextView mJoinNowTv;
    @BindView(R.id.contest_detail_winner_1_iv)
    CircleImageView mWinnerAvatar1;
    @BindView(R.id.contest_detail_winner_2_iv)
    CircleImageView mWinnerAvatar2;
    @BindView(R.id.contest_detail_winner_3_iv)
    CircleImageView mWinnerAvatar3;
    @BindView(R.id.contest_detail_remain_avatar_tv)
    TextView mRemainWinnerAvatar;
    @BindView(R.id.contest_detail_winner_avatar)
    LinearLayout mWinnerAvatarLayout;

    private int mTargetChallenge;
    private int mCountChallenge;
    private int mStatusChallenge;
    private int mUserContestChallengeId;
    private String mTitle;
    private String mDescription;
    private GenkiChallengeModel mGenkiChallengeData;

    public ContestDetailViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void bind(GenkiChallengeModel genkiChallengeModel) {
        mGenkiChallengeData = genkiChallengeModel;
        mTitleTxt.setText(genkiChallengeModel.getTitle());
        mDescriptionTxt.setText(genkiChallengeModel.getDescription());
        mCustomProgressBar.setProgress((int) genkiChallengeModel.getPercentChallenge());
        mCustomProgressBar.setBottomText((int) genkiChallengeModel.getPercentChallenge() + mContext.getString(R.string.contest_detail_percent));
        mCustomProgressBar.setImageID(getIconForChallengeType(genkiChallengeModel.getType(), GenkiContestDetailModel.ContestStatus.NOT_START));
        mRemainWinnerAvatar.setText(String.format("+%d", 0));

        if (genkiChallengeModel.getPercentChallenge() > 0 && genkiChallengeModel.getPercentChallenge() < 100) {
            mCustomProgressBar.setTextColor(Color.rgb(234, 62, 84));
            mCustomProgressBar.setImageID(getIconForChallengeType(genkiChallengeModel.getType(), GenkiContestDetailModel.ContestStatus.IN_PROGRESS));
        }

        if (genkiChallengeModel.getPercentChallenge() == 100) {
            mCustomProgressBar.setFinishedStrokeColor(Color.BLUE);
            mCustomProgressBar.setTextColor(Color.BLUE);
            mCustomProgressBar.setImageID(getIconForChallengeType(genkiChallengeModel.getType(), GenkiContestDetailModel.ContestStatus.FINISHED));
        }

        if (genkiChallengeModel.getWinner_avatars() != null
                && genkiChallengeModel.getWinner_avatars().size() > 0) {
            mJoinNowTv.setVisibility(View.GONE);
            mWinnerAvatarLayout.setVisibility(View.VISIBLE);
            mRemainWinnerAvatar.setVisibility(View.GONE);

            int winnerAvatarSize = genkiChallengeModel.getWinner_avatars().size();

            if (genkiChallengeModel.getWinner_avatars().get(0) != null) {
                Glide.with(mContext).load(genkiChallengeModel.getWinner_avatars().get(0)).into(mWinnerAvatar1);
            }

            if (winnerAvatarSize > 1 && genkiChallengeModel.getWinner_avatars().get(1) != null) {
                Glide.with(mContext).load(genkiChallengeModel.getWinner_avatars().get(1)).into(mWinnerAvatar2);
            }

            if (winnerAvatarSize > 2 && genkiChallengeModel.getWinner_avatars().get(2) != null) {
                Glide.with(mContext).load(genkiChallengeModel.getWinner_avatars().get(2)).into(mWinnerAvatar3);
            }

            int remainWinnerAvatar = winnerAvatarSize - MAX_WINNER_SHOW;

            if (remainWinnerAvatar > MAX_WINNER_SHOW) {
                mRemainWinnerAvatar.setVisibility(View.VISIBLE);
                mRemainWinnerAvatar.setText(String.format("+%d", remainWinnerAvatar));
            }
        }
    }

    @DrawableRes
    private int getIconForChallengeType(int type, int status) {
        switch (type) {
            case GenkiContestDetailModel.ChellengeType.WALKING:
                return getIconChallengeWalking(status);
            case GenkiContestDetailModel.ChellengeType.WATER:
                return getIconChallengeWater(status);
            default:
                return getIconChallengeCycling(status);
        }
    }

    @DrawableRes
    private int getIconChallengeWalking(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_walking_r;
            case GenkiContestDetailModel.ContestStatus.FINISHED:
                return R.drawable.ic_walking_b;
            default:
                return R.drawable.ic_walking;
        }
    }

    @DrawableRes
    private int getIconChallengeWater(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_running_r;
            case GenkiContestDetailModel.ContestStatus.FINISHED:
                return R.drawable.ic_running_b;
            default:
                return R.drawable.ic_running;
        }
    }

    @DrawableRes
    private int getIconChallengeCycling(int status) {
        switch (status) {
            case GenkiContestDetailModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_cycling_r;
            case GenkiContestDetailModel.ContestStatus.FINISHED:
                return R.drawable.ic_cycling_b;
            default:
                return R.drawable.ic_cycling_r;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mContext, ChallengeDetailHistoryActivity.class);
        intent.putExtra(CHALLENGE_USER_DATA, mGenkiChallengeData);
        mContext.startActivity(intent);
    }
}
