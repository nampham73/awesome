package com.genkimiru.app.presentation.register.account_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountFragmentRouter;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountFragmentView;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentContract.AccountPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountFragmentModule {

    @Provides
    @FragmentScope
    AccountFragmentView provideView(AccountFragment accountFragment) {
        return accountFragment;
    }

    @Provides
    @FragmentScope
    AccountFragmentRouter provideRouter(AccountFragment accountFragment) {
        return accountFragment;
    }

    @Provides
    @FragmentScope
    AccountPresenter providePresenter (Context context, AccountFragmentView accountFragmentView, AccountFragmentRouter accountFragmentRouter, UserUseCase userUseCase) {
        return new AccountFragmentPresenter(context, accountFragmentView, accountFragmentRouter, userUseCase);
    }
}
