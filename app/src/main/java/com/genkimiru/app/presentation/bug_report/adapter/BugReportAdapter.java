package com.genkimiru.app.presentation.bug_report.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;

import java.util.ArrayList;
import java.util.List;

public class BugReportAdapter extends BaseSimpleAdapter<BugImage, BugReportViewHolder> {

    private BugReportViewHolder.BugReportHolderListener mListener;

    public BugReportAdapter(Context context, BugReportViewHolder.BugReportHolderListener listener) {
        super(context);
        mItems = new ArrayList<>();
        mItems.add(new BugImage(null, BugImage.ImageType.ADD_IMAGE, ""));
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public BugReportViewHolder createHolder(View view) {
        return new BugReportViewHolder(mContext, view);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.holder_report_bug;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BugImage bugImage = mItems.get(position);
        ((BugReportViewHolder) holder).bind(bugImage, mListener);
    }

    public void addImage(BugImage bugImage) {
        mItems.add(mItems.size() - 1, bugImage);
        notifyDataSetChanged();
    }

    public void deleteImage(BugImage bugImage) {
        if(!mItems.contains(bugImage)) {
            return;
        }
        mItems.remove(bugImage);
        notifyDataSetChanged();
    }

    public List<BugImage> getBugImages() {
        return mItems;
    }
}
