package com.genkimiru.app.presentation.nutrition.nutrition_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.data.model.GenkiNutritionModel;

public class NutritionAdapter extends BaseSimpleAdapter<GenkiNutritionModel.NutriManualModel, NutritionViewHolder> {

    private NutritionViewHolder.UpdateListener mUpdateListener;

    public NutritionAdapter(Context context, NutritionViewHolder.UpdateListener updateListener) {
        super(context);
        mUpdateListener = updateListener;
    }

    @Override
    public NutritionViewHolder createHolder(View view) {
        return new NutritionViewHolder(view, mUpdateListener);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.layout_food_info;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

    }

    public void deleteItem(GenkiNutritionModel.NutriManualModel manualModel) {
        if (mItems != null && mItems.contains(manualModel)) {
            mItems.remove(manualModel);
            notifyDataSetChanged();
        }
    }

}
