package com.genkimiru.app.presentation.update_user_infor.gender;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.Gender.FEMALE;
import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.Gender.MALE;
import static com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityContract.UpdateGenderPresenter;
import static com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityContract.UpdateGenderRouter;
import static com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityContract.UpdateGenderView;

public class UpdateGenderActivity extends SimpleBaseActivity<UpdateGenderPresenter>
        implements UpdateGenderView, UpdateGenderRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_sex_male_rb)
    RadioButton mMaleRadioButton;
    @BindView(R.id.register_sex_female_rb)
    RadioButton mFemaleRadioButton;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateGenderPresenter mUpdateGenderPresenter;
    private MenuItem mApplyMenuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_sex;
    }

    @Override
    protected UpdateGenderPresenter registerPresenter() {
        return mUpdateGenderPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }
    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateGenderPresenter.updateGender(mUserInfoRequest.getGender());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.register_sex_male_rb, R.id.register_sex_female_rb})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.register_sex_male_rb:
                if (checked) {
                    mUserInfoRequest.setGender(MALE);
                    Timber.d("RegisterUserRequest: male");
                }
                break;
            case R.id.register_sex_female_rb:
                if (checked) {
                    mUserInfoRequest.setGender(FEMALE);
                    Timber.d("RegisterUserRequest: female");
                }
                break;
        }
        setEnabledMenuApply(true);
    }

    private void initialDefaultValue() {
        switch (mGenkiUserInfoModel.getGender()) {
            case MALE:
                mMaleRadioButton.setChecked(true);
                break;
            case FEMALE:
                mFemaleRadioButton.setChecked(true);
                break;
            default:
                mMaleRadioButton.setChecked(true);
                mUserInfoRequest.setGender(MALE);
                break;
        }
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
