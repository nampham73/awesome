package com.genkimiru.app.presentation.nutrition;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;

public interface NutritionActivityContract {
    interface NutritionPresenter extends BasePresenter {
        void getData(Calendar calendar, int viewType);

        void addListFoodToServer(Calendar calendar, String name, String cal, String pro, String fat, String carb, long timestamp);

        void deleteManualInputData(int id);

        void updateManualInputData(int id, String name, String cal, String pro, String fat, String carb);

    }

    interface NutritionView {
        void showChartData(ArrayList<BarEntry> entries);

        void loadDataFail(String message);

        void showData(GenkiNutritionModel genkiNutritionModel);

        void updateDataSuccess(boolean isSuccess);
    }

    interface NutritionRouter {

    }
}
