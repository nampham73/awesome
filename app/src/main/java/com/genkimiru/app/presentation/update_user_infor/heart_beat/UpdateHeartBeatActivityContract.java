package com.genkimiru.app.presentation.update_user_infor.heart_beat;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateHeartBeatActivityContract {
    interface UpdateHeartBeatPresenter extends BasePresenter {
        void updateHeartBeat(int feelCondition);
    }

    interface UpdateHeartBeatView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateHeartBeatRouter {

    }
}
