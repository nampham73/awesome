package com.genkimiru.app.presentation.walking_challenge;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.data.remote.genki.response.ResponseResult;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengePresenter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengeRouter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengeView;

import java.io.File;

import javax.inject.Inject;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class WalkingChallengeActivityPresenter extends BasePresenterImp<WalkingChallengeView, WalkingChallengeRouter>
        implements WalkingChallengePresenter {

    private UserUseCase mUserUseCase;
    public static final String STOP_CHALLENGE_FLAG = "stop";
    public WalkingChallengeActivityPresenter(Context context,
                                             WalkingChallengeView walkingChallengeView,
                                             WalkingChallengeRouter walkingChallengeRouter,
                                             UserUseCase userUseCase) {
        super(context, walkingChallengeView, walkingChallengeRouter);

        mUserUseCase = userUseCase;
    }

    @Override
    public void processChallenge(int id) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        ChallengeRequest request = new ChallengeRequest(id, STOP_CHALLENGE_FLAG, new Location("0,0"), token);
        mUserUseCase.execute(new ProcessChallengeSubscriber(),
                UserParam.Factory.processChallenge(request));
    }

    private class ProcessChallengeSubscriber extends DefaultSubscriber<Boolean> {
        @Override
        public void onNext(Boolean aBoolean) {
            mView.onProcessChallengeSuccess("Stopped challenge");
            mRouter.gotoChallengeDetail();
        }

        @Override
        public void onError(Throwable t) {
            mView.onProcessChallengeError(ApiErrorHandler.getMessage(mContext, t, 0));
        }
    }
}
