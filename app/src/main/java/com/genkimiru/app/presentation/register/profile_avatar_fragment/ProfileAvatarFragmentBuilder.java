package com.genkimiru.app.presentation.register.profile_avatar_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ProfileAvatarFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = ProfileAvatarFragmentModule.class)
    abstract ProfileAvatarFragment profileAvatarFragment();
}
