package com.genkimiru.app.presentation.walking_challenge;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengePresenter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengeRouter;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityContract.WalkingChallengeView;

import dagger.Module;
import dagger.Provides;

@Module
public class WalkingChallengeActivityModule {

    @Provides
    @ActivityScope
    WalkingChallengeView provideView(WalkingChallengeActivity walkingChallengeActivity) {
        return walkingChallengeActivity;
    }

    @Provides
    @ActivityScope
    WalkingChallengeRouter provideRouter (WalkingChallengeActivity walkingChallengeActivity) {
        return walkingChallengeActivity;
    }

    @Provides
    @ActivityScope
    WalkingChallengePresenter providePresenter(Context context,
                                               WalkingChallengeView walkingChallengeView,
                                               WalkingChallengeRouter walkingChallengeRouter,
                                               UserUseCase userUseCase) {
        return new WalkingChallengeActivityPresenter(context,
                walkingChallengeView,
                walkingChallengeRouter,
                userUseCase);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository){
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
