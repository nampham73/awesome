package com.genkimiru.app.presentation.register;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

public interface RegisterActivityContract {

    interface  RegisterPresenter extends BasePresenter {
        void loginFitbit(String code);
        void loginGobe(String email, String password);
        void loginFacebook(LoginUserInfoModel loginUserInfoModel);
        void loginGoogle(LoginUserInfoModel loginUserInfoModel);
        void handleSignInResult(Task<GoogleSignInAccount> completedTask);
    }

    interface RegisterView {
        void hideNavigationButton();
        void showNavigationButton();
        void showProgressBar();
        void hideProgressBar();
        void showMessageDialog(String message);
        void disableSwipe();
        void enableSwipe();
    }

    interface RegisterRouter {
        void openBirthdayRegisterScreen();
        void forceHideProgress();
    }
}
