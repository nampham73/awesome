package com.genkimiru.app.presentation.main.contest_fragment.current;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.contest_fragment.ContestListContract;

import java.util.List;

public interface CurrentContestContract {

    interface CurrentContestPresenter extends ContestListContract.ListPresenter {
    }

    interface CurrentContestView extends ListContract.RootView<List<GenkiContestModel>> {
    }

    interface CurrentContestRouter {
    }
}
