package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengePresenter;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengeRouter;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentContract.InformationChallengeView;

import dagger.Module;
import dagger.Provides;

@Module
public class InformationChallengeFragmentModule {

    @Provides
    @FragmentScope
    InformationChallengeView provideView(InformationChallengeFragment fragment) {
        return fragment;
    }

    @Provides
    @FragmentScope
    InformationChallengeRouter provideRouter(InformationChallengeFragment fragment) {
        return fragment;
    }

    @Provides
    @FragmentScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @FragmentScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository){
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }

    @Provides
    @FragmentScope
    InformationChallengePresenter providePresenter(Context context,
                                                   InformationChallengeView informationChallengeView,
                                                   InformationChallengeRouter informationChallengeRouter,
                                                   UserUseCase userUseCase) {
        return new InformationChallengeFragmentPresenter(context, informationChallengeView, informationChallengeRouter, userUseCase);
    }
}
