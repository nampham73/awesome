package com.genkimiru.app.presentation.register.sex_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface SexFragmentContract {

    interface SexPresenter extends BasePresenter {

    }

    interface SexFragmentView {

    }

    interface SexFragmentRouter {

    }
}
