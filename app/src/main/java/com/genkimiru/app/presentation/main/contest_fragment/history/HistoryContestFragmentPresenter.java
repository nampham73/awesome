package com.genkimiru.app.presentation.main.contest_fragment.history;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.ListPresenterImp;


import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.data.remote.genki.request.ListContestRequest;
import com.genkimiru.app.data.remote.genki.response.ContestModelResponse;
import com.genkimiru.app.domain.contest.ContestParam;
import com.genkimiru.app.domain.contest.ContestUseCase;
import com.genkimiru.app.presentation.main.contest_fragment.PagerContestFragment;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestContract.*;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.data.remote.genki.request.ListContestRequest.ContestSort.ASC;
import static com.genkimiru.app.data.remote.genki.request.ListContestRequest.ContestSort.DESC;
import static com.genkimiru.app.data.remote.genki.request.ListContestRequest.ContestSort.START_DATE;
import static com.genkimiru.app.data.remote.genki.request.ListContestRequest.ContestSort.END_DATE;
import static com.genkimiru.app.dialog.ContestSortDialogFragment.SortId.END_DATE_ID;
import static com.genkimiru.app.dialog.ContestSortDialogFragment.SortId.START_DATE_ID;

public class HistoryContestFragmentPresenter extends
        ListPresenterImp<HistoryContestRootView, HistoryContestRouter>
        implements HistoryContestPresenter {
    private ContestUseCase mContestUseCase;
    private int mSortName = START_DATE;
    private int mSortType = ASC;

    public HistoryContestFragmentPresenter(Context context,
                                           HistoryContestRootView historyContestRootView,
                                           HistoryContestRouter historyContestRouter, ContestUseCase useCase) {
        super(context, historyContestRootView, historyContestRouter);
        this.mContestUseCase = useCase;
    }

    @Override
    public void start() {
        super.start();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        ListContestRequest request = new ListContestRequest.Builder(
                getNextPage(),
                getLimit(),
                mSortName,
                mSortType,
                token).create();
        mContestUseCase.execute(new ListContestSubscriber(), ContestParam.Factory.getContestHistories(request));
        setLoadingRequesting(true);
    }

    @Override
    public void requestSort(String sortName, String sortType) {
        switch (sortName) {
            case END_DATE_ID:
                this.mSortName = END_DATE;
                break;
            case START_DATE_ID:
                this.mSortName = START_DATE;
                break;
        }
        switch (sortType) {
            case PagerContestFragment.SortType.ASC:
                mSortType = ASC;
                break;
            case PagerContestFragment.SortType.DESC:
                mSortType = DESC;
                break;
        }
        setNextPage(0);
        start();
    }

    private class ListContestSubscriber extends DefaultSubscriber<List<ContestModelResponse>> {
        @Override
        public void onNext(List<ContestModelResponse> response) {
            mView.onDataLoadDone(convert(response));
            setLoadingRequesting(false);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            setLoadingRequesting(false);
            Timber.e("fetching contest page main failed: %s", t.getMessage());
            mView.onLoadingDataFailure(t, R.string.response_contest_Page_loading_data_failure);
        }
    }

    private List<GenkiContestModel> convert(List<ContestModelResponse> responses) {
        List<GenkiContestModel> items = new ArrayList<>();
        if (responses != null && responses.size() > 0) {
            for (ContestModelResponse response : responses) {
                GenkiContestModel model = new GenkiContestModel();
                model.setHistory(true);
                model.setValueFromResponse(response);
                items.add(model);
            }
        }
        reCalculatedPaging(items.size());
        return items;
    }
}
