package com.genkimiru.app.presentation.register.account_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface AccountFragmentContract {

    interface AccountPresenter extends BasePresenter {
        boolean isValidUsername(String username);
        boolean isValidUsernameLength(String username);
        boolean isValidPassword(String password);
        boolean isValidPasswordLength(String password);
        boolean isValidEmail(String email);
        boolean isValidEmailPattern(String email);
    }

    interface AccountFragmentView {
        void showMessageDialog(String message);
    }

    interface AccountFragmentRouter {
        void openEulaScreen(int index);
    }

}
