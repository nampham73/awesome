package com.genkimiru.app.presentation.nac.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.genkimiru.app.R;

import java.util.HashMap;
import java.util.List;

public class NacFragmentAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> listHeader;
    private HashMap<String, List<String>> listitem;

    public NacFragmentAdapter(Context context, List<String> listHeader, HashMap<String, List<String>> listItem) {
        this.context = context;
        this.listHeader = listHeader;
        this.listitem = listItem;
    }

    @Override
    public int getGroupCount() {
        return this.listHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listitem.get(this.listHeader.get(groupPosition)).size();
    }

    public void setData(List<String> listHeader, HashMap<String, List<String>> listitem) {
        this.listHeader = listHeader;
        this.listitem = listitem;
        notifyDataSetChanged();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listitem.get(this.listHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.holder_faq_list_group, null);
        }
        TextView txtLiestGroup = convertView.findViewById(R.id.activity_faq_group_tv);
        txtLiestGroup.setTypeface(null, Typeface.BOLD);
        txtLiestGroup.setTextColor(Color.WHITE);
        txtLiestGroup.setBackgroundResource(R.color.dot_inactive_indicator_dark);
        txtLiestGroup.setText(headerTitle);

        if (isExpanded) {
            txtLiestGroup.setTypeface(null, Typeface.BOLD);
            txtLiestGroup.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.ic_arrow_down_white, 0);
        } else {
            txtLiestGroup.setTypeface(null, Typeface.NORMAL);
            txtLiestGroup.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.ic_arrow_right_white, 0);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.holder_faq_list_item, null);
        }
        TextView txtlistChild = convertView.findViewById(R.id.activity_faq_item);
        // txtlistChild.setText(Html.fromHtml(childText));
        txtlistChild.setText(childText);
        txtlistChild.setTypeface(null, Typeface.NORMAL);
        txtlistChild.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_end_date, 0, 0, 0);


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
