package com.genkimiru.app.presentation.register.account_complete_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;

public interface AccountCompleteFragmentContract {

    interface  AccountCompletePresenter extends BasePresenter {
        void register(RegisterUserRequest registerUserRequest);
        void prepareUserInfo();
    }

    interface AccountCompleteView {
        void showCompleteMessage();
        void showMessageDialog(String message);
        void onRegisterFail();
    }

    interface AccountCompleteRouter {
        void openDashboardScreen();
    }
}
