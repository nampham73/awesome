package com.genkimiru.app.presentation.register.sex_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SexFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = SexFragmentModule.class)
    abstract SexFragment sexFragment();
}
