package com.genkimiru.app.presentation.register.blood_pressure_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BloodPressureFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = BloodPressureFragmentModule.class)
    abstract BloodPressureFragment bloodPressureFragment();
}
