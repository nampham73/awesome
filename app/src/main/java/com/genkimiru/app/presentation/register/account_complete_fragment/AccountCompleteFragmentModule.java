package com.genkimiru.app.presentation.register.account_complete_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompletePresenter;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompleteRouter;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentContract.AccountCompleteView;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountCompleteFragmentModule {

    @Provides
    @FragmentScope
    AccountCompleteView provideView(AccountCompleteFragment accountCompleteFragment) {
        return accountCompleteFragment;
    }

    @Provides
    @FragmentScope
    AccountCompleteRouter provideRouter(AccountCompleteFragment accountCompleteFragment) {
        return accountCompleteFragment;
    }

    @Provides
    @FragmentScope
    AccountCompletePresenter providePresenter(Context context,
                                              AccountCompleteView accountCompleteView,
                                              AccountCompleteRouter accountCompleteRouter,
                                              UserUseCase userUseCase,
                                              GenkiUserInfoModel genkiUserInfoModel) {
        return new AccountCompleteFragmentPresenter(context, accountCompleteView, accountCompleteRouter, userUseCase, genkiUserInfoModel);
    }


}
