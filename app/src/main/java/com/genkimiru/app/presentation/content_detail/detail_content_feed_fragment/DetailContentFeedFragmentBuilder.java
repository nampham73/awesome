package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class DetailContentFeedFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = DetailContentFeedFragmentModule.class)
    abstract DetailContentFeedFragment detailContentFeedFragment();
}
