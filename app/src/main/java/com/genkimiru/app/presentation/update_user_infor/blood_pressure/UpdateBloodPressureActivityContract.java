package com.genkimiru.app.presentation.update_user_infor.blood_pressure;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateBloodPressureActivityContract {
    interface UpdateBloodPressurePresenter extends BasePresenter {
        void updateBloodPressure(int bloodPressureUpper, int bloodPressureUnder);
        String validateBloodPressure(String bloodPresSure);
    }

    interface UpdateBloodPressureView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateBloodPressureRouter {

    }
}
