package com.genkimiru.app.presentation.walking_challenge_map;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapPresenter;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapRouter;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityContract.WalkingChallengeMapView;

public class WalkingChallengeMapActivityPresenter extends BasePresenterImp<WalkingChallengeMapView, WalkingChallengeMapRouter>
        implements WalkingChallengeMapPresenter {

    public WalkingChallengeMapActivityPresenter(Context context, WalkingChallengeMapView walkingChallengeMapView, WalkingChallengeMapRouter walkingChallengeMapRouter) {
        super(context, walkingChallengeMapView, walkingChallengeMapRouter);
    }
}
