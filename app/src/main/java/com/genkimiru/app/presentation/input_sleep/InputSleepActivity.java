package com.genkimiru.app.presentation.input_sleep;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.widget.ClockSeekBar;


public class InputSleepActivity extends AppCompatActivity {


    private TextView textView1;
    private TextView textView2;
    private ClockSeekBar circleAlarmTimerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_sleep);
        initView();

    }

    private void initView(){
        textView1 = findViewById(R.id.start);
        textView2 = findViewById(R.id.end);

        circleAlarmTimerView = findViewById(R.id.circletimerview);
        circleAlarmTimerView.setOnTimeChangedListener(new ClockSeekBar.OnTimeChangedListener() {
            @Override
            public void start(String starting) {
                textView1.setText(starting);
            }

            @Override
            public void end(String ending) {
                textView2.setText(ending);
            }
        });
    }
}
