package com.genkimiru.app.presentation.main;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.view.BaseActivityView;
import com.genkimiru.app.presentation.main.contest_fragment.PagerContestFragment;
import com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragment;
import com.genkimiru.app.presentation.main.notification_fragment.NotificationFragment;
import com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragment;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragment;

import butterknife.BindView;

public class MainActivityView extends BaseActivityView<MainActivity, MainContract.MainPresenter> implements MainContract.MainView {

    @BindView(R.id.bottom_navigation_view)
    CustomBottomNavigationView mBottomNavigationView;
    @BindView(R.id.status_bar_layout)
    View mStatusBar;

    private FragmentManager mFragmentManager;

    public MainActivityView(MainActivity activity, FragmentManager fragmentManager) {
        super(activity);
        this.mFragmentManager = fragmentManager;
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        initializeBottomNavigationView();
    }

    public void setIsShowMenuItem(boolean isShow) {
        mActivity.mIsShowMenuItem = isShow;
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initializeBottomNavigationView() {
        mBottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            SimpleBaseFragment selectFragment = null;
            switch (item.getItemId()) {
                case R.id.item_dashboard:
                    selectFragment = DashboardFragment.newInstance();
                    mStatusBar.setBackgroundResource(R.color.light_grey);
                    break;
                case R.id.item_contest:
                    selectFragment = PagerContestFragment.newInstance();
                    mStatusBar.setBackgroundResource(R.drawable.bg_main);
                    setIsShowMenuItem(false);
                    break;
                case R.id.item_notification:
                    selectFragment = NotificationFragment.newInstance();
                    mStatusBar.setBackgroundResource(R.drawable.bg_main);
                    setIsShowMenuItem(false);
                    break;
                case R.id.item_content_feed:
                    selectFragment = ContentFeedFragment.newInstance();
                    mStatusBar.setBackgroundResource(R.drawable.bg_main);
                    setIsShowMenuItem(false);
                    break;
                case R.id.item_setting:
                    selectFragment = SettingFragment.newInstance();
                    mStatusBar.setBackgroundResource(R.drawable.bg_main);
                    setIsShowMenuItem(false);
                    break;
            }
            setItemNavigationView(selectFragment);
            return true;
        });
        mBottomNavigationView.setBadgePositionValue(2, 8); // hard code for showing layout design
        setItemNavigationView(DashboardFragment.newInstance());
    }

    private void setItemNavigationView(SimpleBaseFragment fragmentItem) {
        android.support.v4.app.FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragmentItem);
        fragmentTransaction.commit();
    }

    private enum NavigationView {

        DASHBOARD(0),
        CONTEST(1),
        NOTIFICATION(2),
        CONTENT_FEED(3),
        SETTING(4);

        private int mIndex;

        NavigationView(int index) {
            mIndex = index;
        }

        public int getIndex() {
            return mIndex;
        }
    }
}
