package com.genkimiru.app.presentation.main.contest_fragment.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivity;
import com.genkimiru.app.presentation.main.GlideApp;

import java.util.Locale;

import butterknife.BindView;

import static com.genkimiru.app.Constants.Extra.CONTEST_ID;
import static com.genkimiru.app.Constants.Extra.IS_CONTEST_HISTORY;

public class ContestViewHolder extends BaseSimpleViewHolder<GenkiContestModel> implements View.OnClickListener {

    @BindView(R.id.contest_main_title_tv)
    TextView mTvTitle;
    @BindView(R.id.contest_main_desc_tv)
    TextView mTvDescription;
    @BindView(R.id.contest_main_program_tv)
    TextView mTvProgram;
    @BindView(R.id.contest_main_user_award_tv)
    TextView mTvUserAward;
    @BindView(R.id.contest_main_user_join_tv)
    TextView mTvUserJoin;
    @BindView(R.id.user_contest_status_tv)
    TextView mTvUserContestStatus;
    @BindView(R.id.contest_main_date_ib)
    ImageView mIvStatusIcon;
    @BindView(R.id.contest_main_iv)
    ImageView mIvThumbnail;
    @BindView(R.id.contest_main_start_date_tv)
    TextView mTvStartDate;
    @BindView(R.id.contest_main_end_date_tv)
    TextView mTvEndDate;
    @BindView(R.id.contest_main_reward_point_tv)
    TextView mTvContestRewardPoint;
    @BindView(R.id.contest_main_won_layout)
    View mWonLayout;
    @BindView(R.id.contest_main_join_layout)
    View mJoinLayout;

    private GenkiContestModel mGenkiContestModel;
    private boolean mIsHistoryContest;

    public ContestViewHolder(Context context, View itemView) {
        super(context, itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void bind(GenkiContestModel genkiContestModel) {
        mTvProgram.setVisibility((genkiContestModel.isHistory() || StringUtil.isEmpty(genkiContestModel.getTitle()))
                ? View.GONE 
                : View.VISIBLE);
        mWonLayout.setVisibility(genkiContestModel.isHistory() ? View.GONE : View.VISIBLE);
        mJoinLayout.setVisibility(genkiContestModel.isHistory() ? View.GONE : View.VISIBLE);

        mGenkiContestModel = genkiContestModel;
        mTvTitle.setText(genkiContestModel.getName());
        mTvDescription.setText(genkiContestModel.getmDescription());
        mTvProgram.setText(genkiContestModel.getTitle());

        int current = genkiContestModel.getCurrentNumberAwardUser();
        int max = genkiContestModel.getMaxNumberAwardUser();
        mTvUserAward.setText(userAward(current, max));
        mTvUserJoin.setText(String.valueOf(genkiContestModel.getTotalJoin()));

        if (genkiContestModel.getUseChallengeStatus() > 0) {
            String status = mContext.getString(getUserStatus(genkiContestModel.getUseChallengeStatus()));
            int color = getBackgroundColorUseChallengeStatus(genkiContestModel.getUseChallengeStatus());
            mTvUserContestStatus.setText(status);
            mTvUserContestStatus.setBackgroundColor(ContextCompat.getColor(mContext, color));
            mTvUserContestStatus.setVisibility(View.VISIBLE);
        } else {
            mTvUserContestStatus.setVisibility(View.GONE);
        }


        int icon = getIconForType(genkiContestModel.getType(), genkiContestModel.getUseChallengeStatus());
        mIvStatusIcon.setImageResource(icon);
        GlideApp.with(mContext)
                .load(genkiContestModel.getRewardAvatarUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(mIvThumbnail);

        Locale currentLocale = mContext.getResources().getConfiguration().locale;
        String startDate = DateUtil.convertToDayMonthYearString(DateUtil.Format.API_FORMAT, genkiContestModel.getStartDate(), currentLocale);
        String endDate = DateUtil.convertToDayMonthYearString(DateUtil.Format.API_FORMAT, genkiContestModel.getEndDate(), currentLocale);
        mTvStartDate.setText(startDate);
        mTvEndDate.setText(endDate);
        mTvContestRewardPoint.setText(String.valueOf(genkiContestModel.getRewardPoint()));
    }

    private String userAward(int current, int total) {
        if (current > 100 || total > 100) return String.format("%1$s%n/%2$s", current, total);
        return String.format("%1$s/%2$s", current, total);
    }

    @ColorRes
    private int getBackgroundColorUseChallengeStatus(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.color.contest_challenge_in_progress;
            case GenkiContestModel.ContestStatus.NOT_START:
                return R.color.contest_challenge_not_start;
            default:
                return R.color.contest_challenge_normal;
        }
    }

    @StringRes
    private int getUserStatus(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.string.contest_status_in_progress;
            case GenkiContestModel.ContestStatus.FINISHED:
                return R.string.contest_status_finished;
            case GenkiContestModel.ContestStatus.CANCEL:
                return R.string.contest_status_cancel;
            default:
                return R.string.contest_status_not_start;
        }
    }

    @DrawableRes
    private int getIconForType(int type, int status) {
        switch (type) {
            case GenkiContestModel.ContestType.DAILY:
                return getIconForDaily(status);
            case GenkiContestModel.ContestType.WEEKLY:
                return getIconForWeekly(status);
            case GenkiContestModel.ContestType.MONTHLY:
                return getIconForMonthly(status);
            default:
                return getIconForOneDay(status);
        }
    }

    @DrawableRes
    private int getIconForWeekly(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_weekly_red;
            case GenkiContestModel.ContestStatus.NOT_START:
                return R.drawable.ic_weekly_blue;
            default:
                return R.drawable.ic_weekly_normal;
        }
    }

    @DrawableRes
    private int getIconForDaily(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_daily_red;
            case GenkiContestModel.ContestStatus.NOT_START:
                return R.drawable.ic_daily_blue;
            default:
                return R.drawable.ic_daily_normal;
        }
    }

    @DrawableRes
    private int getIconForOneDay(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_oneday_red;
            case GenkiContestModel.ContestStatus.NOT_START:
                return R.drawable.ic_oneday_blue;
            default:
                return R.drawable.ic_oneday_normal;
        }
    }

    @DrawableRes
    private int getIconForMonthly(int status) {
        switch (status) {
            case GenkiContestModel.ContestStatus.IN_PROGRESS:
                return R.drawable.ic_monthly_red;
            case GenkiContestModel.ContestStatus.NOT_START:
                return R.drawable.ic_monthly_blue;
            default:
                return R.drawable.ic_monthly_normal;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mContext, ContestDetailActivity.class);
        intent.putExtra(CONTEST_ID, mGenkiContestModel.getId());
        intent.putExtra(IS_CONTEST_HISTORY, mGenkiContestModel.isHistory());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }
}
