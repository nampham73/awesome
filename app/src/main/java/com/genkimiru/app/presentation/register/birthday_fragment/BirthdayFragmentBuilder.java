package com.genkimiru.app.presentation.register.birthday_fragment;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BirthdayFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = BirthdayFragmentModule.class)
    abstract BirthdayFragment birthdayFragment();
}
