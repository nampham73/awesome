package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class InformationChallengeFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = InformationChallengeFragmentModule.class)
    abstract InformationChallengeFragment informationChallengeFragment();
}
