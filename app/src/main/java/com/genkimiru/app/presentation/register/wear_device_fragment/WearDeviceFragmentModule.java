package com.genkimiru.app.presentation.register.wear_device_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.domain.gobe.GobeUseCase;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDeviceFragmentRouter;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDeviceFragmentView;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentContract.WearDevicePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class WearDeviceFragmentModule {

    @Provides
    @FragmentScope
    WearDeviceFragmentView provideView(WearDeviceFragment wearDeviceFragment) {
        return wearDeviceFragment;
    }

    @Provides
    @FragmentScope
    WearDeviceFragmentRouter proviDeviceFragmentRouter(WearDeviceFragment wearDeviceFragment) {
        return wearDeviceFragment;
    }

    @Provides
    @FragmentScope
    WearDevicePresenter providePresenter(Context context, WearDeviceFragmentView wearDeviceFragmentView,
                                         WearDeviceFragmentRouter wearDeviceFragmentRouter,
                                         UserUseCase userUseCase,
                                         GobeUseCase gobeUseCase,
                                         RegisterUserRequest registerUserRequest,
                                         GenkiUserInfoModel genkiUserInfoModel) {
        return new WearDeviceFragmentPresenter(context,
                wearDeviceFragmentView,
                wearDeviceFragmentRouter,
                userUseCase,
                gobeUseCase,
                registerUserRequest,
                genkiUserInfoModel);
    }
}
