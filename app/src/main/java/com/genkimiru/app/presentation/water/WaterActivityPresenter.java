package com.genkimiru.app.presentation.water;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterView;
import com.genkimiru.app.presentation.water.WaterActivityContract.WaterRouter;


public class WaterActivityPresenter extends BasePresenterImp<WaterView, WaterRouter> implements WaterActivityContract.WaterPresenter {
    public WaterActivityPresenter(Context context, WaterActivityContract.WaterView waterView, WaterActivityContract.WaterRouter waterRouter) {
        super(context, waterView, waterRouter);
    }
}
