package com.genkimiru.app.presentation.bug_report;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.presentation.bug_report.adapter.BugImage;
import com.genkimiru.app.presentation.bug_report.adapter.BugReportAdapter;
import com.genkimiru.app.presentation.bug_report.adapter.BugReportViewHolder;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GET_PHOTO;
import static com.genkimiru.app.presentation.bug_report.BugReportActivityContract.BugReportPresenter;
import static com.genkimiru.app.presentation.bug_report.BugReportActivityContract.BugReportRouter;
import static com.genkimiru.app.presentation.bug_report.BugReportActivityContract.BugReportView;

public class BugReportActivity extends SimpleBaseActivity<BugReportActivityContract.BugReportPresenter>
        implements BugReportView, BugReportRouter {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.bug_report_image_rcv)
    RecyclerView mBugImageRcv;
    @BindView(R.id.bug_report_title_tv)
    TextView mBugTitle;
    @BindView(R.id.bug_report_problem_tv)
    TextView mBugProblemTv;
    @BindView(R.id.bug_report_severity_group_rg)
    RadioGroup mBugSeverityGroupRg;

    @Inject
    BugReportPresenter mPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    private BugReportAdapter mBugReportAdapter;
    private MenuItem mSendMenuItem;
    private String mBugSeverity;
    private boolean mIsValidTitle;
    private boolean mIsValidProblemDescription;
    private CompositeDisposable mCompositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCompositeDisposable.clear();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_bug_report;
    }

    @Override
    protected BugReportPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menuIcon) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_report_bug, menuIcon);
        mSendMenuItem = menuIcon.findItem(R.id.action_sent);
        mSendMenuItem.setOnMenuItemClickListener(menuItem -> handleOnSendClick());
        setEnabledMenuSend(false);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("requestCode = " + requestCode + ", resultCode = " + resultCode);

        switch (requestCode) {
            case REQUEST_CODE_GET_PHOTO:
                if (data == null || data.getData() == null) {
                    return;
                }
                String imageName = data.getData().getLastPathSegment();
                imageName = imageName.replace(":","_") + ".jpg";
                Timber.d("Image name: %s", imageName);
                BugImage bugImage = new BugImage(data.getData(), BugImage.ImageType.BUG_IMAGE, imageName);
                mBugReportAdapter.addImage(bugImage);
                break;
            default:
                break;
        }
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(getText(R.string.bug_report_toolbar_title));
    }

    private void initialize() {
        mCompositeDisposable = new CompositeDisposable();
        mBugSeverity = BugSeverity.MEDIUM.getLevel();

        mBugReportAdapter = new BugReportAdapter(this, new BugReportViewHolder.BugReportHolderListener() {

            @Override
            public void onDeleteImageClick(BugImage bugImage) {
                mBugReportAdapter.deleteImage(bugImage);
            }

            @Override
            public void onAddImageClick() {
               openImageGallery();
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        mBugImageRcv.setAdapter(mBugReportAdapter);
        mBugImageRcv.setLayoutManager(gridLayoutManager);
        mBugImageRcv.setNestedScrollingEnabled(false);
        mBugImageRcv.setHasFixedSize(true);
    }

    private void initializeEventHandler() {
        mCompositeDisposable.add(
                RxTextView.textChanges(mBugTitle)
                        .skip(1) // Skip init value
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(title -> {
                            Timber.d("Bug title: %s", title.toString());
                            mIsValidTitle = StringUtil.isNotEmpty(title.toString().trim());
                            setEnabledMenuSend(mIsValidTitle && mIsValidProblemDescription);
                        }));

        mCompositeDisposable.add(
                RxTextView.textChanges(mBugProblemTv)
                        .skip(1) // Skip init value
                        .distinctUntilChanged(CharSequence::toString)
                        .subscribe(problem -> {
                            Timber.d("Bug problem: %s", problem.toString());
                            mIsValidProblemDescription = StringUtil.isNotEmpty(problem.toString().trim());
                            setEnabledMenuSend(mIsValidTitle && mIsValidProblemDescription);
                        }));

        mCompositeDisposable.add(
                RxRadioGroup.checkedChanges(mBugSeverityGroupRg)
                        .skip(1) // Skip init value
                        .subscribe(buttonId -> {
                            Timber.d("Severity checked change");
                            switch (buttonId) {
                                case R.id.bug_report_severity_low_rb:
                                    Timber.d("Severity: %s", BugSeverity.LOW.getLevel());
                                    mBugSeverity = BugSeverity.LOW.getLevel();
                                    break;
                                case R.id.bug_report_severity_medium_rb:
                                    Timber.d("Severity: %s", BugSeverity.MEDIUM.getLevel());
                                    mBugSeverity = BugSeverity.MEDIUM.getLevel();
                                    break;
                                case R.id.bug_report_severity_high_rb:
                                    Timber.d("Severity: %s", BugSeverity.HIGH.getLevel());
                                    mBugSeverity = BugSeverity.HIGH.getLevel();
                                    break;
                                case R.id.bug_report_severity_critical_rb:
                                    Timber.d("Severity: %s", BugSeverity.CRITICAL.getLevel());
                                    mBugSeverity = BugSeverity.CRITICAL.getLevel();
                                    break;
                                default:
                                    break;
                            }
                        })
        );
    }

    private void openImageGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_GET_PHOTO);
    }

    private void setEnabledMenuSend(boolean isEnabled) {
        mSendMenuItem.setEnabled(isEnabled);
        mSendMenuItem.getIcon().setAlpha(isEnabled ? 255 : 130);
    }

    private boolean handleOnSendClick() {
        Timber.d("On Send Click");
        showProgress();
        mPresenter.sendBugReport(
                mBugTitle.getText().toString().trim(),
                mBugProblemTv.getText().toString().trim(),
                mBugSeverity,
                mBugReportAdapter.getBugImages(),
                mGenkiUserInfoModel.getUsername()
        );
        return true;
    }

    @Override
    public void onReportSuccess() {
        hideProgress();
        MessageUtil.showShortToast(getApplicationContext(),getString(R.string.bug_report_success));
        finish();
    }

    @Override
    public void onReportFail() {
        hideProgress();
        MessageUtil.createMessageDialog(getString(R.string.bug_report_fail)).show(getSupportFragmentManager());
    }

    public enum BugSeverity {
        LOW("low"),
        MEDIUM("medium"),
        HIGH("high"),
        CRITICAL("critical");

        private String mLevel;

        BugSeverity(String level) {
            mLevel = level;
        }

        public String getLevel() {
            return mLevel;
        }
    }
}
