package com.genkimiru.app.presentation.main.contest_fragment;

import com.genkimiru.app.base.mvp.presenter.ListContract;

public interface ContestListContract {
    interface ListPresenter extends ListContract.ListPresenter{
        void requestSort(String sortName, String sorType);
    }
}
