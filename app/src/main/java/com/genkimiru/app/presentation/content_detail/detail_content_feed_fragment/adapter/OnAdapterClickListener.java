package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public interface OnAdapterClickListener {
    void onItemClicked(RecyclerView.ViewHolder holder, View view);
}
