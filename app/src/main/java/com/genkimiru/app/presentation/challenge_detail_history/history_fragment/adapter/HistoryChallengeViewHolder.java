package com.genkimiru.app.presentation.challenge_detail_history.history_fragment.adapter;

import android.view.View;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;
import com.genkimiru.app.data.model.GenkiContestModel;

import butterknife.BindView;

public class HistoryChallengeViewHolder extends BaseSimpleViewHolder<GenkiContestModel> {
    @BindView(R.id.history_start_day)
    TextView mTimeStart;
    @BindView(R.id.history_end_day)
    TextView mTimeEnd;

    public HistoryChallengeViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(GenkiContestModel genkiContestModel) {
        mTimeStart.setText(genkiContestModel.getStartDate());
        mTimeEnd.setText(genkiContestModel.getEndDate());
    }
}
