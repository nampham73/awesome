package com.genkimiru.app.presentation.nutrition;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.mydata.MyDataRepository;
import com.genkimiru.app.data.repository.mydata.MyDataRepositoryImp;
import com.genkimiru.app.domain.mydata.MyDataUseCase;
import com.genkimiru.app.presentation.nutrition.NutritionActivityContract.*;

@Module
public class NutritionActivityModule {
    @Provides
    @ActivityScope
    NutritionPresenter providePresenter(Context context, NutritionView view, NutritionRouter router, MyDataUseCase myDataUseCase) {
        return new NutritionActivityPresenter(context, view, router, myDataUseCase);
    }

    @Provides
    @ActivityScope
    NutritionView provideView(NutritionActivity nutritionActivity) {
        return nutritionActivity;
    }

    @Provides
    @ActivityScope
    NutritionRouter provideRouter(NutritionActivity nutritionActivity) {
        return nutritionActivity;
    }

    @Provides
    @ActivityScope
    MyDataUseCase provideUseCase(SchedulerProvider schedulerProvider, MyDataRepository repository) {
        return new MyDataUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }


    @Provides
    @ActivityScope
    MyDataRepository provideRepository(GenkiApi api) {
        return new MyDataRepositoryImp(api);
    }

}
