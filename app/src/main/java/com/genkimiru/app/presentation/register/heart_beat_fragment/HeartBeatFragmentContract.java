package com.genkimiru.app.presentation.register.heart_beat_fragment;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface HeartBeatFragmentContract {

    interface HeartBeatPresenter extends BasePresenter {

    }

    interface HeartBeatFragmentView {

    }

    interface HeartBeatFragmentRouter {

    }
}
