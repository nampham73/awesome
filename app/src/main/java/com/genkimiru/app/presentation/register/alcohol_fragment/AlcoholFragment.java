package com.genkimiru.app.presentation.register.alcohol_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.dialog.CheckboxDialog;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholFragmentRouter;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholFragmentView;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholPresenter;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class AlcoholFragment extends SimpleBaseFragment<AlcoholPresenter> implements AlcoholFragmentView, AlcoholFragmentRouter {

    @BindView(R.id.register_alcohol_radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.register_alcohol_1_level_each_time_tv)
    TextView mLevelEachTime1;
    @BindView(R.id.register_alcohol_2_level_each_time_tv)
    TextView mLevelEachTime2;
    @BindView(R.id.register_alcohol_3_level_each_time_tv)
    TextView mLevelEachTime3;
    @BindView(R.id.register_alcohol_4_level_each_time_tv)
    TextView mLevelEachTime4;
    @BindView(R.id.register_alcohol_5_level_each_time_tv)
    TextView mLevelEachTime5;

    @Inject
    AlcoholPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    private CheckboxDialog alcoholDialog;
    private RegisterActivity mRegisterActivity;
    private Disposable mDisposable;
    private String mLevelTxt;
    private String mLevel;
    private boolean isDialogShowing = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_alcohol;
    }

    @Override
    protected AlcoholPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialDefaultValue();
    }

    @Override
    public void onResume() {
        super.onResume();
        mDisposable = RxRadioGroup.checkedChanges(radioGroup)
                .skip(1) // Skip init value
                .subscribe(buttonId -> {
                    switch (buttonId) {
                        case R.id.register_1_radio_button:
                            Timber.d("Alcohol level: 1");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_1.getValue();
                            openAlcoHolDialog();
                            break;
                        case R.id.register_2_radio_button:
                            Timber.d("Alcohol level: 2");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_2.getValue();
                            openAlcoHolDialog();
                            break;
                        case R.id.register_3_radio_button:
                            Timber.d("Alcohol level: 3");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_3.getValue();
                            openAlcoHolDialog();
                            break;
                        case R.id.register_4_radio_button:
                            Timber.d("Alcohol level: 4");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_4.getValue();
                            openAlcoHolDialog();
                            break;
                        case R.id.register_5_radio_button:
                            Timber.d("Alcohol level: 5");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_5.getValue();
                            openAlcoHolDialog();
                            break;
                        case R.id.register_6_radio_button:
                            Timber.d("Alcohol level: 6");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_6.getValue();
                            mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkLevel.LEVEL_NO_DRINK.getValue();
                            setDefaultView();
                            break;
                        case R.id.register_7_radio_button:
                            Timber.d("Alcohol level: 7");
                            mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_1.getValue();
                            mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkLevel.LEVEL_NO_DRINK.getValue();
                            setDefaultView();
                            break;
                    }
                });
        setTextLevelEachTime();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    private void openAlcoHolDialog() {
        Timber.d("Open dialog alcohol");
        if (isDialogShowing) {
            Timber.d("Skip open dialog alcohol");
            return;
        }
        Timber.d("is Showing: ");
        alcoholDialog = new CheckboxDialog.CheckboxDialogBuilder()
                .setTitle(getString(R.string.register_alcohol_title_dialog))
                .setLayout(R.layout.dialog_checbox_alcohol_content)
                .dialogListener(new CheckboxDialog.DialogListener() {
                    @Override
                    public void onComplete(CheckboxDialog checkboxDialog) {
                        setTextLevelEachTime();
                        checkboxDialog.dismiss();
                    }

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.register_alcohol_180ml_rb:
                                Timber.d("Alcohol level each time: %d", 1);
                                mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_1_180.getValue();
                                mLevelTxt = getString(R.string.register_alcohol_180ml);
                                break;
                            case R.id.register_alcohol_360ml_rb:
                                Timber.d("Alcohol level each time: %d", 2);
                                mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_2_360.getValue();
                                mLevelTxt = getString(R.string.register_alcohol_360ml);
                                break;
                            case R.id.register_alcohol_540ml_rb:
                                Timber.d("Alcohol level each time: %d", 3);
                                mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_3_540.getValue();
                                mLevelTxt = getString(R.string.register_alcohol_540ml);
                                break;
                            case R.id.register_alcohol_720ml_rb:
                                Timber.d("Alcohol level each time: %d", 4);
                                mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_4_720.getValue();
                                mLevelTxt = getString(R.string.register_alcohol_720ml);
                                break;
                            case R.id.register_alcohol_900ml_rb:
                                Timber.d("Alcohol level each time: %d", 5);
                                mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_5_900.getValue();
                                mLevelTxt = getString(R.string.register_alcohol_900ml);
                                break;
                            case R.id.register_alcohol_900ml_more_rb:
                                Timber.d("Alcohol level each time: %d", 6);
                                mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_6_MORE_900.getValue();
                                mLevelTxt = getString(R.string.register_alcohol_900ml_more);
                                break;
                        }
                    }

                    @Override
                    public void onOpen(int checkedId) {
                        mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkEachTime.LEVEL_1_180.getValue();
                        mLevelTxt = getString(R.string.register_alcohol_180ml);
                    }

                    @Override
                    public void onDismiss() {
                        isDialogShowing = false;
                    }
                }).build();
        alcoholDialog.show(mRegisterActivity.getSupportFragmentManager());
        isDialogShowing = true;
    }

    private void setTextLevelEachTime() {
        int id = radioGroup.getCheckedRadioButtonId();
        switch (id) {
            case R.id.register_1_radio_button:
                mLevelEachTime1.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.VISIBLE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_2_radio_button:
                mLevelEachTime2.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.VISIBLE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_3_radio_button:
                mLevelEachTime3.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.VISIBLE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_4_radio_button:
                mLevelEachTime4.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.VISIBLE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_5_radio_button:
                mLevelEachTime5.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initialDefaultValue() {
        mRegisterUserRequest.alcohol_level = RegisterUserRequest.DrinkLevel.LEVEL_7.getValue();
        mRegisterUserRequest.drink_each_time = RegisterUserRequest.DrinkLevel.LEVEL_NO_DRINK.getValue();
    }

    private void setDefaultView() {
        mLevelEachTime1.setVisibility(View.GONE);
        mLevelEachTime2.setVisibility(View.GONE);
        mLevelEachTime3.setVisibility(View.GONE);
        mLevelEachTime4.setVisibility(View.GONE);
        mLevelEachTime5.setVisibility(View.GONE);
    }
}
