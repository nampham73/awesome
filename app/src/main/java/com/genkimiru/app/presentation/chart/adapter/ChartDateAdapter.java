package com.genkimiru.app.presentation.chart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.base.util.DateUtil;

import java.util.Calendar;

import static com.genkimiru.app.presentation.chart.adapter.ChartDateAdapter.DateType.DAY;
import static com.genkimiru.app.presentation.chart.adapter.ChartDateAdapter.DateType.MONTH;

public class ChartDateAdapter extends BaseSimpleAdapter<Calendar, ChartDateViewHolder> {

    private int mViewType = DAY;

    public ChartDateAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.holder_date_for_chart;
    }

    @Override
    public ChartDateViewHolder createHolder(View view) {
        return new ChartDateViewHolder(view);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(getItemLayoutResource(), parent, false);
        int width = defineViewWidth(mContext);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        view.setLayoutParams(params);
        return createHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Calendar calendar = mItems.get(position);
        if(calendar == null) {
            ((ChartDateViewHolder) holder).bind("");
            return;
        }
        switch (mViewType) {
            case DAY:
                ((ChartDateViewHolder) holder).bind(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                break;
            case MONTH:
                ((ChartDateViewHolder) holder).bind(DateUtil.getMonthString(calendar));
                break;
        }
    }

    private int defineViewWidth(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (wm == null) {
            return 0;
        }

        wm.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels / 7;
    }

    public Calendar getItemAt(int position) {
        if(mItems != null && mItems.size() > position) {
            return mItems.get(position);
        }
        return null;
    }

    public void clear() {
        if (mItems == null) {
            return;
        }
        mItems.clear();
        notifyDataSetChanged();
    }

    public void setViewType(int mViewType) {
        this.mViewType = mViewType;
    }

    @Override
    public int getItemViewType(int position) {
        return mViewType;
    }

    public @interface DateType {
        int DAY = 1;
        int MONTH = 2;
    }
}
