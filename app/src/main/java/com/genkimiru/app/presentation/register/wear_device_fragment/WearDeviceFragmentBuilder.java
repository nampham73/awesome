package com.genkimiru.app.presentation.register.wear_device_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class WearDeviceFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = WearDeviceFragmentModule.class)
    abstract WearDeviceFragment wearDeviceFragment();
}
