package com.genkimiru.app.presentation.main.contentfeed_fragment.adapter;

import android.content.Context;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.data.model.GenkiContentModel;

public class ContentFeedAdapter extends BaseSimpleAdapter<GenkiContentModel, ContentFeedViewHolder> {

    public ContentFeedAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.holder_content_feed_main;
    }

    @Override
    public ContentFeedViewHolder createHolder(View view) {
        return new ContentFeedViewHolder(view);
    }

    @Override
    public void releaseAll() {
        super.releaseAll();
        notifyDataSetChanged();
    }
}
