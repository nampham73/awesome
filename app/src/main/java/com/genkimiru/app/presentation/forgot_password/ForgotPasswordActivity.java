package com.genkimiru.app.presentation.forgot_password;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.presentation.forgot_password.new_password_fragment.NewPasswordFragment;
import com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragment;

import javax.inject.Inject;

import butterknife.BindView;

import static com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivityContract.*;

public class ForgotPasswordActivity extends SimpleBaseActivity<ForgotPasswordPresenter>
        implements ForgotPasswordView, ForgotPasswordRouter {

    @Inject
    ForgotPasswordPresenter mForgotPasswordPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        openRecoverPasswordScreen();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected ForgotPasswordPresenter registerPresenter() {
        return mForgotPasswordPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.forgot_password);
    }

    @Override
    public void openRecoverPasswordScreen() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        String tag = PasswordRecoverFragment.class.getName();
        fragmentTransaction.add(R.id.forgot_password_container, new PasswordRecoverFragment(), tag);
        fragmentTransaction.commit();
    }

    @Override
    public void openNewPasswordScreen() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        String tag = NewPasswordFragment.class.getName();
        fragmentTransaction.add(R.id.forgot_password_container, new NewPasswordFragment(), tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    @Override
    public void completeRecover() {
        finish();
    }
}
