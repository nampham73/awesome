package com.genkimiru.app.presentation.challenge_detail_history;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.genkimiru.app.R;
import com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeFragment;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragment;

public class ChallengeDetailHistoryAdapter extends FragmentPagerAdapter {
    private Context mContext;

    public ChallengeDetailHistoryAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        TypeChallenge typeChallenge = TypeChallenge.values()[position];
        switch (typeChallenge) {
            case INFORMATION:
                return new InformationChallengeFragment();
            case HISTORY:
                return new HistoryChallengeFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return TypeChallenge.values().length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        TypeChallenge typeChallenge = TypeChallenge.values()[position];
        switch (typeChallenge) {
            case INFORMATION:
                return mContext.getString(R.string.challenge_information);
            case HISTORY:
                return mContext.getString(R.string.challenge_history);
            default:
                return "";
        }

    }

    public enum TypeChallenge {
        INFORMATION(0),
        HISTORY(1);

        private int mIndex;

        TypeChallenge(int mIndex) {
            this.mIndex = mIndex;
        }
    }
}
