package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.genkimiru.app.R;

public class InformationChallengeFragmentAdapter extends FragmentPagerAdapter {
    public InformationChallengeFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        TypeInfor typeInfor = TypeInfor.values()[position];
        InformationFragment informationFragment = new InformationFragment();
        informationFragment.setType(typeInfor);
        return informationFragment;
    }

    @Override
    public int getCount() {
        return TypeInfor.values().length;
    }

    public enum TypeInfor {
        ONE(R.drawable.bg_running),
        TWO(R.drawable.bg_service),
        THREE(R.drawable.bg_eat_diet),
        FOURE(R.drawable.bg_limit_eat),
        FIVE(R.drawable.bg_answer_diet);

        private int mBackgroundInfor;

        TypeInfor(int mBackground) {
            this.mBackgroundInfor = mBackground;
        }

        public int getBackgroundInfor() {
            return mBackgroundInfor;
        }
    }

}
