package com.genkimiru.app.presentation.main.setting_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SettingFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = SettingFragmentModule.class)
    abstract SettingFragment settingFragment();
}
