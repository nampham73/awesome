package com.genkimiru.app.presentation.update_user_infor.hospital;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityContract.UpdateHospitalPresenter;
import static com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityContract.UpdateHospitalRouter;
import static com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityContract.UpdateHospitalView;

@Module
public class UpdateHospitalActivityModule {
    @Provides
    @ActivityScope
    UpdateHospitalView provideUpdateHospitalView(UpdateHospitalActivity updateHospitalActivity) {
        return updateHospitalActivity;
    }

    @Provides
    @ActivityScope
    UpdateHospitalRouter provideUpdateHospitalRouter(UpdateHospitalActivity updateHospitalActivity) {
        return updateHospitalActivity;
    }

    @Provides
    @ActivityScope
    UpdateHospitalPresenter provideUpdateHospitalPresenter(Context context, UpdateHospitalView updateHospitalView, UpdateHospitalRouter updateHospitalRouter, UserUseCase userUseCase,
                                                           GenkiUserInfoModel genkiUserInfoModel) {
        return new UpdateHospitalActivityPresenter(context, updateHospitalView, updateHospitalRouter, userUseCase, genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserInfoRequest provideUserInforRequest(GenkiUserInfoModel genkiUserInfoModel) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return new UserInfoRequest(token, genkiUserInfoModel.getHealthSource());
    }

    @Provides
    @ActivityScope
    UserUseCase providesUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }


}
