package com.genkimiru.app.presentation.register.hospital_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalFragmentRouter;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalFragmentView;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentContract.HospitalPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class HospitalFragmentModule {

    @Provides
    @FragmentScope
    HospitalFragmentView provideView(HospitalFragment hospitalFragment) {
        return hospitalFragment;
    }

    @Provides
    @FragmentScope
    HospitalFragmentRouter provideRouter(HospitalFragment hospitalFragment) {
        return hospitalFragment;
    }

    @Provides
    @FragmentScope
    HospitalPresenter providePresenter(Context context, HospitalFragmentView hospitalFragmentView,
                                       HospitalFragmentRouter hospitalFragmentRouter, UserUseCase userUseCase) {
        return new HospitalFragmentPresenter(context, hospitalFragmentView, hospitalFragmentRouter, userUseCase);
    }
}
