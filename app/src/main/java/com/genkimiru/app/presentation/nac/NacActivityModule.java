package com.genkimiru.app.presentation.nac;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.presentation.nac.adapter.NacAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class NacActivityModule {


    @Provides
    @ActivityScope
    NacAdapter provideNacAdaper(NacActivity nacActivity) {
        return new NacAdapter(nacActivity, nacActivity.getSupportFragmentManager());
    }
}
