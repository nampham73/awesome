package com.genkimiru.app.presentation.main.contest_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.ListFragment;
import com.genkimiru.app.data.model.GenkiContestModel;
import com.genkimiru.app.presentation.main.contest_fragment.adapter.ContestAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public abstract class ContestsListFragment<TPresenter extends ContestListContract.ListPresenter>
        extends ListFragment<ContestAdapter, List<GenkiContestModel>, TPresenter> {

    @BindView(R.id.no_data_tv)
    TextView mNoContestTv;

    @Inject
    ContestAdapter mAdapter;

    @NonNull
    @Override
    protected ContestAdapter createAdapter() {
        return mAdapter;
    }

    @Override
    protected boolean canSwipeToRefresh() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFirstLoad() && getPresenter() != null) getPresenter().start();
    }

    @Override
    public void onDataLoadDone(List<GenkiContestModel> genkiContestModels) {
        super.onDataLoadDone(genkiContestModels);
        if (mAdapter != null) {
            mAdapter.appendAll(genkiContestModels);
            mNoContestTv.setVisibility(mAdapter.getItemCount() <= 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onLoadingDataFailure(Throwable throwable, String message) {
        super.onLoadingDataFailure(throwable, message);
        mNoContestTv.setVisibility(View.VISIBLE);

    }

    @Override
    public void onLoadingDataFailure(Throwable throwable, int message) {
        super.onLoadingDataFailure(throwable, message);
        mNoContestTv.setVisibility(View.VISIBLE);
    }

    public void requestSorNameAction(String sortName, String sortType) {
        mAdapter.releaseAll();
        getPresenter().requestSort(sortName, sortType);
    }

    public ContestAdapter getAdapter() {
        return mAdapter;
    }

    public boolean isFirstLoad() {
        return true;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onRefresh() {
        mAdapter.releaseAll();
        super.onRefresh();
    }
}
