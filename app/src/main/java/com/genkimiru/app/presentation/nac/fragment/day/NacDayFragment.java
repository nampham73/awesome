package com.genkimiru.app.presentation.nac.fragment.day;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ExpandableListView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.presentation.nac.fragment.NacFragmentAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.*;

public class NacDayFragment extends SimpleBaseFragment<NacDayPresenter> implements NacDayView,NacDayRouter {

    @BindView(R.id.fragment_nac_day_expanded_listview)
    ExpandableListView mExpandableListView;

    NacFragmentAdapter mAdapter;
    List<String> listHeader;
    HashMap<String, List<String>> listItem;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        mAdapter = new NacFragmentAdapter(getActivity(), listHeader, listItem);
        mExpandableListView.setAdapter(mAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_nac_day;
    }

    @Override
    protected NacDayPresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    private void setData() {
        listHeader = new ArrayList<>();
        listItem = new HashMap<String, List<String>>();

        //add header
        listHeader.add("Today");
        listHeader.add("Yesterday");
        listHeader.add("Tomorrow");
        //add child
        List<String> today = new ArrayList<>();
        today.add("sadqwdwqdwqdwqd");
        today.add("sadqwdwqdwqdwqd");
        today.add("sadqwdwqdwqdwqd");
        today.add("sadqwdwqdwqdwqd");

        List<String> yesterday = new ArrayList<>();
        yesterday.add("sadqwdwqdwqdwqd");
        yesterday.add("sadqwdwqdwqdwqd");
        yesterday.add("sadqwdwqdwqdwqd");
        yesterday.add("sadqwdwqdwqdwqd");

        List<String> tomorrow = new ArrayList<>();
        tomorrow.add("sadqwdwqdwqdwqd");
        tomorrow.add("sadqwdwqdwqdwqd");
        tomorrow.add("sadqwdwqdwqdwqd");
        tomorrow.add("sadqwdwqdwqdwqd");

        listItem.put(listHeader.get(0), today);
        listItem.put(listHeader.get(1), yesterday);
        listItem.put(listHeader.get(2), tomorrow);
    }

    @Override
    public void onUpdateSuccess() {

    }

    @Override
    public void onUpdateError() {

    }
}
