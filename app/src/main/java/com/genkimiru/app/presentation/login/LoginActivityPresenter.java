package com.genkimiru.app.presentation.login;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.genki.request.LoginRequest;
import com.genkimiru.app.data.remote.genki.request.LoginWithGoogleRequest;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.response.LoginFacebookDataResponse;
import com.genkimiru.app.data.remote.genki.response.LoginWithGoogleResponse;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;
import com.genkimiru.app.data.remote.google.request.GoogleAccessTokenRequest;
import com.genkimiru.app.data.remote.google.response.GoogleAccessTokenResponse;
import com.genkimiru.app.domain.google.GoogleParam;
import com.genkimiru.app.domain.google.GoogleUseCase;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Facebook.AVATAR_URL;
import static com.genkimiru.app.Constants.Facebook.PARAM_ALL_FIELDS;
import static com.genkimiru.app.Constants.Facebook.PARAM_BIRTHDAY;
import static com.genkimiru.app.Constants.Facebook.PARAM_EMAIL;
import static com.genkimiru.app.Constants.Facebook.PARAM_FIELDS;
import static com.genkimiru.app.Constants.Facebook.PARAM_FIRST_NAME;
import static com.genkimiru.app.Constants.Facebook.PARAM_GENDER;
import static com.genkimiru.app.Constants.Facebook.PARAM_ID;
import static com.genkimiru.app.Constants.Facebook.PARAM_LAST_NAME;
import static com.genkimiru.app.Constants.Google.CLIENT_ID;
import static com.genkimiru.app.Constants.Google.CLIENT_SECRECT;
import static com.genkimiru.app.Constants.Google.GRANT_TYPE;
import static com.genkimiru.app.Constants.Google.REDIRECT_URI;
import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.data.remote.genki.GenkiApiErrorException.ErrorCode.LOGIN_FB_001_E_001;
import static com.genkimiru.app.data.remote.genki.GenkiApiErrorException.ErrorCode.LOGIN_GOOGLE_001_E_001;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginPresenter;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginRouter;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginView;

public class LoginActivityPresenter extends BasePresenterImp<LoginView, LoginRouter>
        implements LoginPresenter {

    private UserUseCase mUserUseCase;
    private GenkiUserInfoModel mGenkiUserInfoModel;
    private LoginUserInfoModel mLoginUserInfoModel = new LoginUserInfoModel();
    private GoogleUseCase mGoogleUseCase;
    private RegisterUserRequest mRegisterUserRequest;

    public LoginActivityPresenter(Context context,
                                  LoginView loginView,
                                  LoginRouter loginRouter,
                                  UserUseCase userUseCase,
                                  GenkiUserInfoModel genkiUserInfoModel,
                                  GoogleUseCase googleUseCase,
                                  RegisterUserRequest registerUserRequest) {
        super(context, loginView, loginRouter);
        mUserUseCase = userUseCase;
        mGenkiUserInfoModel = genkiUserInfoModel;
        mGoogleUseCase = googleUseCase;
        mRegisterUserRequest = registerUserRequest;
    }

    @Override
    public boolean isValidUsernameLength(String dataInput) {
        return StringUtil.isValidLength(dataInput, 8);
    }

    @Override
    public boolean isValidPasswordLength(String dataInput) {
        return StringUtil.isValidLength(dataInput, 8);
    }

    @Override
    public boolean isValidUsername(String username) {
        return (StringUtil.isNotEmpty(username));
    }

    @Override
    public boolean isValidPassword(String password) {
        return (StringUtil.isNotEmpty(password));
    }

    @Override
    public void login(String username, String password) {
        mView.showProgressDialog();
        Timber.d("login: username = %s, password = %s", username, password);
        LoginRequest loginRequest;
        if (StringUtil.validateEmailFormat(username)) {
            loginRequest = new LoginRequest(null, username, password);
        } else {
            loginRequest = new LoginRequest(username, null, password);
        }
        mUserUseCase.execute(new LoginAndGetUserInfoSubscriber(), UserParam.Factory.login(loginRequest));
    }

    @Override
    public void loginFacebook(CallbackManager callbackManager) {

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("userId %s", loginResult.getAccessToken().getUserId());
                Timber.d("authtoken %s", loginResult.getAccessToken().getToken());
                String accessToken = loginResult.getAccessToken().getToken();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), (object, response) -> {
                    if (object == null) {
                        return;
                    }
                    String lastname = object.optString(PARAM_LAST_NAME);
                    String email = object.optString(PARAM_EMAIL);
                    String firstname = object.optString(PARAM_FIRST_NAME);
                    String rawBirthdate = object.optString(PARAM_BIRTHDAY);
                    String gender = object.optString(PARAM_GENDER);
                    String avatar = String.format(AVATAR_URL, object.optString(PARAM_ID));
                    Timber.d("avatar %s", avatar);
                    String birthdate = StringUtil.isNotEmpty(rawBirthdate)
                            ? DateUtil.convertDateFormat(rawBirthdate,
                            DateUtil.Format.MONTH_DAY_YEAR_FORMAT,
                            DateUtil.Format.API_YEAR_MONTH_DAY_FORMAT)
                            : "";
                    int genderInt = gender == "female" ? 2 : 1;
                    mLoginUserInfoModel = new LoginUserInfoModel(email, firstname, lastname, avatar, birthdate, genderInt, accessToken);
                    mUserUseCase.execute(new LoginFacebookSubscriber(), UserParam.Factory.loginFacebook(loginResult.getAccessToken().getToken()));

                });
                Bundle parameter = new Bundle();
                parameter.putString(PARAM_FIELDS, PARAM_ALL_FIELDS);
                request.setParameters(parameter);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                mView.hideProgressDialog();
                Toast.makeText(mContext, R.string.facebook_login_cancel, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                mView.hideProgressDialog();
                Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void loginWithGooglePlus(String code) {
        Timber.d("login Google: code = %s", code);
        mView.showProgressDialog();
        GoogleAccessTokenRequest request = new GoogleAccessTokenRequest(code, REDIRECT_URI, CLIENT_ID, CLIENT_SECRECT, "", GRANT_TYPE);
        mGoogleUseCase.execute(new GooglePlusAccessTokenSubscriber(), GoogleParam.Factory.getAccessToken(request));
    }

    @Override
    public void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            mLoginUserInfoModel.setFirstName(account.getGivenName());
            mLoginUserInfoModel.setLastName(account.getFamilyName());
            mLoginUserInfoModel.setEmail(account.getEmail());
            mLoginUserInfoModel.setAvatar(account.getPhotoUrl().toString());
            loginWithGooglePlus(account.getServerAuthCode());
        } catch (ApiException e) {
            Timber.e(e, "Google sign in error");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUserUseCase.clear();
    }

    private class LoginFacebookSubscriber extends DefaultSubscriber<LoginFacebookDataResponse> {
        @Override
        public void onNext(LoginFacebookDataResponse loginFacebookResponse) {
            Timber.d("genki token: %s", loginFacebookResponse.token);
            mView.hideProgressDialog();
            mUserUseCase.execute(new GetUserInfoSubscriber(), UserParam.Factory.getUserInformation(loginFacebookResponse.token));
        }

        @Override
        public void onError(Throwable throwable) {
            if (throwable instanceof GenkiApiErrorException) {
                GenkiApiErrorException genkiApiErrorException = (GenkiApiErrorException) throwable;
                if (genkiApiErrorException.getErrorList() != null) {
                    for (String error : genkiApiErrorException.getErrorList()) {
                        if (error.equals(LOGIN_FB_001_E_001.getCode())) {
                            mView.showProgressDialog();
                            mRegisterUserRequest.mIsLoginFlag = true;
                            mRouter.openRegisterScreen(mLoginUserInfoModel);
                            return;
                        }
                    }
                }
            }
        }
    }

    private class LoginAndGetUserInfoSubscriber extends DefaultSubscriber<UserInfoResponse> {
        @Override
        public void onNext(UserInfoResponse userInfoResponse) {
            Timber.d("Login success: token = %s", userInfoResponse.token);
            GenkiApplication.getPreferences().getString(USER_TOKEN).set(userInfoResponse.token);
            mGenkiUserInfoModel.setValueFromResponse(userInfoResponse);
            mRouter.openDashboardScreen();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.hideProgressDialog();
            Timber.e("Login Error: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }

    private class GetUserInfoSubscriber extends DefaultSubscriber<UserInfoResponse> {
        @Override
        public void onNext(UserInfoResponse userInfoResponse) {
            Timber.d("GetUserInfo success: token = %s", userInfoResponse.token);
            GenkiApplication.getPreferences().getString(USER_TOKEN).set(userInfoResponse.token);
            mGenkiUserInfoModel.setValueFromResponse(userInfoResponse);
            mView.hideProgressDialog();
            mRouter.openDashboardScreen();
        }

        @Override
        public void onError(Throwable throwable) {
            mView.hideProgressDialog();
            Timber.e("GetUserInfo: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }

    private class GooglePlusAccessTokenSubscriber extends DefaultSubscriber<GoogleAccessTokenResponse> {

        @Override
        public void onNext(GoogleAccessTokenResponse googleAccessTokenResponse) {
            Timber.d("login google access_token: %s", googleAccessTokenResponse.access_token);
            String accesstoken = googleAccessTokenResponse.access_token;
            LoginWithGoogleRequest request = new LoginWithGoogleRequest(accesstoken);
            mUserUseCase.execute(new LoginGoogleSubscriber(), UserParam.Factory.loginGoogle(request));
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }
    }

    private class LoginGoogleSubscriber extends DefaultSubscriber<LoginWithGoogleResponse> {

        @Override
        public void onNext(LoginWithGoogleResponse loginWithGoogleResponse) {
            Timber.d("genki token: %s", loginWithGoogleResponse.data.token);
            mUserUseCase.execute(new GetUserInfoSubscriber(), UserParam.Factory.getUserInformation(loginWithGoogleResponse.data.token));
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
            if (t instanceof GenkiApiErrorException) {
                GenkiApiErrorException genkiApiErrorException = (GenkiApiErrorException) t;
                if (genkiApiErrorException.getErrorList() != null) {
                    for (String error : genkiApiErrorException.getErrorList()) {
                        if (error.equals(LOGIN_GOOGLE_001_E_001.getCode())) {
                            mRouter.openRegisterScreenFromLoginGoole(mLoginUserInfoModel);
                            return;
                        }
                    }
                }
            }
            mView.hideProgressDialog();
        }
    }
}
