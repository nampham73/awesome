package com.genkimiru.app.presentation.main.contest_fragment;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestFragmentBuilder;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestFragmentBuilder;
import com.genkimiru.app.presentation.main.contest_fragment.privates.PrivatesContestFragmentBuilder;
import com.genkimiru.app.presentation.main.contest_fragment.publics.PublicContestFragmentBuilder;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestFragmentBuilder;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PagerContestFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = {PagerContestFragmentModule.class,
            CurrentContestFragmentBuilder.class,
            HistoryContestFragmentBuilder.class,
            PrivatesContestFragmentBuilder.class,
            PublicContestFragmentBuilder.class,
            RecommendContestFragmentBuilder.class
    })
    abstract PagerContestFragment pagerContestFragment();
}
