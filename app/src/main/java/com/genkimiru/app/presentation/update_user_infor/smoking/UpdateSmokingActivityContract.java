package com.genkimiru.app.presentation.update_user_infor.smoking;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface UpdateSmokingActivityContract {

    interface UpdateSmokingPresenter extends BasePresenter {
        void updateSmoking(int smokingLevel,int smokingEachTime);
    }

    interface UpdateSmokingView {
        void onUpdateSuccess();
        void onUpdateError(String message);
    }

    interface UpdateSmokingRouter {

    }
}
