package com.genkimiru.app.presentation.chart.adapter;

import android.view.View;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;

import java.util.Calendar;

import butterknife.BindView;

public class ChartDateViewHolder extends BaseSimpleViewHolder<String> {

    @BindView(R.id.holder_date_tv)
    TextView mDateTv;

    public ChartDateViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(String tex) {
        mDateTv.setText(tex);
    }
}
