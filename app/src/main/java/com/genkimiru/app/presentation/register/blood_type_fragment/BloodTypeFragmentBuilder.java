package com.genkimiru.app.presentation.register.blood_type_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BloodTypeFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = BloodTypeFragmentModule.class)
    abstract BloodTypeFragment bloodTypeFragment();
}
