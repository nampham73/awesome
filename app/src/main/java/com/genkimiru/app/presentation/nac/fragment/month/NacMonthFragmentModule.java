package com.genkimiru.app.presentation.nac.fragment.month;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.nac.fragment.month.NacMonthFragmentContract.NacMonthPresenter;
import static com.genkimiru.app.presentation.nac.fragment.month.NacMonthFragmentContract.NacMonthRouter;
import static com.genkimiru.app.presentation.nac.fragment.month.NacMonthFragmentContract.NacMonthView;

@Module
public class NacMonthFragmentModule {

    @Provides
    @FragmentScope
    NacMonthView provideNacMonthView(NacMonthFragment nacMonthFragment) {
        return nacMonthFragment;
    }

    @Provides
    @FragmentScope
    NacMonthRouter provideNacMonthRouter(NacMonthFragment nacMonthFragment) {
        return nacMonthFragment;
    }

    @Provides
    @FragmentScope
    NacMonthPresenter provideNacMonthPresenter(Context context, NacMonthView nacMonthView, NacMonthRouter nacMonthRouter, UserUseCase userUseCase) {
        return new NacMonthFragmentPresenter(context, nacMonthView, nacMonthRouter, userUseCase);
    }

    @Provides
    @FragmentScope
    UserRepository provideResponsitory(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @FragmentScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
