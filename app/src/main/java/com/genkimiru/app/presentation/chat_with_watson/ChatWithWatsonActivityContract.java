package com.genkimiru.app.presentation.chat_with_watson;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.data.model.WatsonMessageModel;

public interface ChatWithWatsonActivityContract {

    interface ChatWithWatsonPresenter extends BasePresenter {
        void requestMessage(String message);
        void pauseTextToSpeech();
    }

    interface ChatWithWatsonView {
        void onSuccess(WatsonMessageModel waston);
        void onError(Throwable throwable);
    }

    interface ChatWithWatsonRouter {

    }
}
