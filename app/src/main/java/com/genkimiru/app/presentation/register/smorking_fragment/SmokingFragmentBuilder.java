package com.genkimiru.app.presentation.register.smorking_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SmokingFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = SmokingFragmentModule.class)
    abstract SmokingFragment smokingFragment();
}
