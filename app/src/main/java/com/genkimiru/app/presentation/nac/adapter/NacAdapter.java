package com.genkimiru.app.presentation.nac.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.genkimiru.app.R;
import com.genkimiru.app.presentation.nac.fragment.day.NacDayFragment;
import com.genkimiru.app.presentation.nac.fragment.month.NacMonthFragment;
import com.genkimiru.app.presentation.nac.fragment.week.NacWeekFragment;

public class NacAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public NacAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        TypeNac typeNac = TypeNac.values()[position] ;
            switch (typeNac) {
                case DAY:
                    return new NacDayFragment();
                case WEEK:
                    return new NacWeekFragment();
                case MONTH:
                    return new NacMonthFragment();
                default:
                    return null;
            }
        }

    @Override
    public int getCount() {
        return TypeNac.values().length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        TypeNac typeNac = TypeNac.values()[position];
        switch (typeNac) {
            case DAY:
                return mContext.getString(R.string.nac_day);
            case WEEK:
                return mContext.getString(R.string.nac_week);
            case MONTH:
                return mContext.getString(R.string.nac_month);
            default:
                return "";
        }
    }

    public enum TypeNac {
        DAY(0),
        WEEK(1),
        MONTH(2);
        private int index;

        TypeNac(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }
}
