package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Locale;

import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder {
    protected Context mContext;
    public BaseViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
        ButterKnife.bind(this,itemView);
    }

    public void setData(IDetailContentModel data){

    }
}
