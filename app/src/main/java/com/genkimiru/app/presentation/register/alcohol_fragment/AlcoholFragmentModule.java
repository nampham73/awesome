package com.genkimiru.app.presentation.register.alcohol_fragment;


import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholFragmentRouter;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholFragmentView;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class AlcoholFragmentModule {

    @Provides
    @FragmentScope
    AlcoholFragmentView provideView(AlcoholFragment alcoholFragment) {
        return alcoholFragment;
    }

    @Provides
    @FragmentScope
    AlcoholFragmentRouter provideRouter(AlcoholFragment alcoholFragment) {
        return alcoholFragment;
    }

    @Provides
    @FragmentScope
    AlcoholPresenter providePresenter(Context context, AlcoholFragmentView alcoholFragmentView, AlcoholFragmentRouter alcoholFragmentRouter, UserUseCase userUseCase) {
        return new AlcoholFragmentPresenter(context, alcoholFragmentView, alcoholFragmentRouter, userUseCase);
    }
}
