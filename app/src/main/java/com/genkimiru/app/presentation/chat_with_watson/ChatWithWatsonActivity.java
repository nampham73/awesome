package com.genkimiru.app.presentation.chat_with_watson;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.WatsonMessageModel;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonPresenter;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonRouter;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityContract.ChatWithWatsonView;
import com.genkimiru.app.presentation.chat_with_watson.WatsonAdapter.WatsonAdapter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class ChatWithWatsonActivity extends SimpleBaseActivity<ChatWithWatsonPresenter>
        implements ChatWithWatsonView, ChatWithWatsonRouter {

    private static final int RECOGNIZER_REQ_CODE = 100;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.chat_with_watson_message_edt)
    EditText mInputMessageEdt;
    @BindView(R.id.chat_with_watson_send_btn)
    Button mSendImg;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private WatsonAdapter mAdapter;

    @Inject
    ChatWithWatsonPresenter mPresenter;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_chat_with_watson;
    }

    @Override
    protected ChatWithWatsonPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new WatsonAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        this.mInputMessageEdt.setText("");
        mPresenter.requestMessage("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.pauseTextToSpeech();
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(getString(R.string.chat_with_watson_title));
    }

    @OnClick(R.id.chat_with_watson_send_btn)
    void sendMess() {
        mPresenter.requestMessage(mInputMessageEdt.getText().toString().trim());
        mInputMessageEdt.setText("");
        mPresenter.pauseTextToSpeech();
    }

    @OnClick(R.id.chat_with_watson_record_btn)
    void speak() {
        mPresenter.pauseTextToSpeech();
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, Locale.getDefault());
        startActivityForResult(intent, RECOGNIZER_REQ_CODE);
    }

    @Override
    public void onSuccess(WatsonMessageModel item) {
        mAdapter.add(item);
        if (mAdapter.getItemCount() > 1) {
            mRecyclerView.getLayoutManager().smoothScrollToPosition(mRecyclerView, null, mAdapter.getItemCount() - 1);
        }
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RECOGNIZER_REQ_CODE: {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    String yourResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0);
                    mInputMessageEdt.setText(yourResult);
                }
                break;
            }
        }
    }

    @SuppressLint("CheckResult")
    private void initializeEventHandler() {
        RxTextView.textChanges(mInputMessageEdt)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(message -> {
                    Timber.d("input message: %s", message.toString());
                    if (StringUtil.isEmpty(message)) {
                        setDisableLoginButton();
                    } else {
                        setEnableLoginButton();
                    }
                });
    }

    private void setDisableLoginButton() {
        mSendImg.setEnabled(false);
        mSendImg.setBackgroundResource(R.drawable.bg_watbot_send_disable_button);
    }

    private void setEnableLoginButton() {
        mSendImg.setEnabled(true);
        mSendImg.setBackgroundResource(R.drawable.bg_watbot_send_button);

    }
}
