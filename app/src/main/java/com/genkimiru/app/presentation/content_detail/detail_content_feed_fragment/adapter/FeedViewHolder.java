package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

import android.text.Spanned;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.genkimiru.app.R;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiFeedModel;
import com.genkimiru.app.presentation.main.GlideApp;

import java.util.Locale;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedViewHolder extends BaseViewHolder{
    @BindView(R.id.feed_avatar_iv)
    CircleImageView mIvAvatar;
    @BindView(R.id.feed_comment_tv)
    TextView mTvFeedComment;
    @BindView(R.id.feed_time_tv)
    TextView mTvFeedTime;

    public FeedViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setData(IDetailContentModel data) {
        if (data instanceof GenkiFeedModel){
            GenkiFeedModel model = (GenkiFeedModel) data;
            GlideApp.with(itemView).load(model.getAvatar()).error(R.color.light_grey).into(mIvAvatar);
            mTvFeedComment.setText(model.getCommentWithUsername());
            String date = model.getCreatedAt();
            Locale currentLocale = mContext.getResources().getConfiguration().locale;
            String time = DateUtil.getTimeAgo(itemView.getResources(), date, DateUtil.Format.API_FORMAT, currentLocale);
            mTvFeedTime.setText(time);
        }
    }

}
