package com.genkimiru.app.presentation.challenge_detail_history.information_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;

public class InformationFragment extends SimpleBaseFragment {
    @BindView(R.id.fragment_infor_background)
    ImageView mBackgroundImageView;
    InformationChallengeFragmentAdapter.TypeInfor mType;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViewInfor();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_information;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public void setType(InformationChallengeFragmentAdapter.TypeInfor type) {
        mType = type;
    }

    public void initializeViewInfor() {
        if (mType == null) {
            return;
        }
        mBackgroundImageView.setImageResource(mType.getBackgroundInfor());
    }

}
