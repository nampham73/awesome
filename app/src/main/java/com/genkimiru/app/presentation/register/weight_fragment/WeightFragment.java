package com.genkimiru.app.presentation.register.weight_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightFragmentRouter;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightFragmentView;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentContract.WeightPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class WeightFragment extends SimpleBaseFragment<WeightPresenter>
        implements WeightFragmentView, WeightFragmentRouter {

    @BindView(R.id.register_weight_edt)
    EditText mWeightEdt;

    @Inject
    WeightPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    private RegisterActivity mRegisterActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_weight;
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisible()) {
            return;
        }
        initialDefaultValue();
    }

    @Override
    protected WeightPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mWeightEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(weight -> {
                    Timber.d("weight: %s", weight.toString());
                    if (StringUtil.isEmpty(weight)) {
                        mRegisterActivity.disableSwipe();
                        return;
                    } else {
                        mRegisterActivity.enableSwipe();
                    }
                    weight = mPresenter.validateWeight(weight.toString().trim());
                    mWeightEdt.setText(weight.toString());
                    mWeightEdt.setSelection(mWeightEdt.getText().length());
                    setWeight();
                });
    }

    private void setWeight() {
        mRegisterUserRequest.weight = Float.parseFloat(mWeightEdt.getText().toString().trim());
        mRegisterUserRequest.weight_type = 1;

    }

    public void initialDefaultValue() {
        Timber.d("initialDefaultValue: %s", mRegisterUserRequest.weight);
        if (mRegisterUserRequest.weight == 0) {
            setWeight();
            return;
        }
        if (StringUtil.isEmpty(mWeightEdt.getText().toString().trim())) {
            mWeightEdt.setText(getString(R.string.register_weight_default_value));
        }
        mWeightEdt.setText(String.valueOf(mRegisterUserRequest.weight));
        mRegisterUserRequest.weight_type = 1;
    }
}
