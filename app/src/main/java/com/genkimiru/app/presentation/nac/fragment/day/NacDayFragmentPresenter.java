package com.genkimiru.app.presentation.nac.fragment.day;

import android.content.Context;

import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;

import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.NacDayPresenter;
import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.NacDayRouter;
import static com.genkimiru.app.presentation.nac.fragment.day.NacDayFragmentContract.NacDayView;

public class NacDayFragmentPresenter extends BasePresenterImp<NacDayView, NacDayRouter> implements NacDayPresenter {
    UserUseCase mUserUseCase;

    public NacDayFragmentPresenter(Context context, NacDayView nacDayView, NacDayRouter nacDayRouter, UserUseCase userUseCase) {
        super(context, nacDayView, nacDayRouter);
        this.mUserUseCase = userUseCase;
    }

    @Override
    public void onUpdateNacDay() {

    }

    private class NacDaySubcriber extends DefaultSubscriber {
        @Override
        public void onNext(Object o) {
            super.onNext(o);
        }

        @Override
        public void onError(Throwable t) {
            super.onError(t);
        }
    }
}
