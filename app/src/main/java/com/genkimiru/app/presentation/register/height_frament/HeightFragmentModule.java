package com.genkimiru.app.presentation.register.height_frament;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightFragmentRouter;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightFragmentView;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentContract.HeightPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class HeightFragmentModule {

    @Provides
    @FragmentScope
    HeightFragmentView provideView(HeightFragment heightFragment) {
        return heightFragment;
    }

    @Provides
    @FragmentScope
    HeightFragmentRouter provideRouter(HeightFragment heightFragment) {
        return heightFragment;
    }

    @Provides
    @FragmentScope
    HeightPresenter providePresenter(Context context, HeightFragmentView heightFragmentView,
                                     HeightFragmentRouter heightFragmentRouter, UserUseCase userUseCase) {
        return new HeightFragmentPresenter(context, heightFragmentView, heightFragmentRouter, userUseCase);
    }
}
