package com.genkimiru.app.presentation.bug_report.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleViewHolder;

import butterknife.BindView;

public class BugReportViewHolder extends BaseSimpleViewHolder<BugImage> {

    @BindView(R.id.holder_bug_report_image_iv)
    ImageView mBugImage;
    @BindView(R.id.holder_bug_report_image_delete_btn)
    ImageButton mImageDeleteBtn;
    @BindView(R.id.holder_bug_report_add_image_iv)
    ImageView mAddImage;

    public BugReportViewHolder(Context context, View itemView) {
        super(context, itemView);
    }

    @Override
    public void bind(BugImage bugImage) {
        switch (bugImage.getImageType()) {
            case BUG_IMAGE:
                mBugImage.setVisibility(View.VISIBLE);
                mImageDeleteBtn.setVisibility(View.VISIBLE);
                mAddImage.setVisibility(View.GONE);
                mBugImage.setImageURI(bugImage.getImageUri());
                break;
            case ADD_IMAGE:
                mBugImage.setVisibility(View.GONE);
                mImageDeleteBtn.setVisibility(View.GONE);
                mAddImage.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void bind(BugImage bugImage, BugReportHolderListener listener) {
        bind(bugImage);
        mImageDeleteBtn.setOnClickListener(view -> listener.onDeleteImageClick(bugImage));
        mAddImage.setOnClickListener(view -> listener.onAddImageClick());
    }

    public interface BugReportHolderListener {
        void onDeleteImageClick(BugImage bugImage);
        void onAddImageClick();
    }
}
