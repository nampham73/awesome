package com.genkimiru.app.presentation.register.alcohol_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholFragmentRouter;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholFragmentView;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentContract.AlcoholPresenter;

public class AlcoholFragmentPresenter extends BasePresenterImp<AlcoholFragmentView,
        AlcoholFragmentRouter> implements AlcoholPresenter {
    private UserUseCase mUserUseCase;

    public AlcoholFragmentPresenter(Context context, AlcoholFragmentView alcoholFragmentView,
                                    AlcoholFragmentRouter alcoholFragmentRouter, UserUseCase userUseCase) {
        super(context, alcoholFragmentView, alcoholFragmentRouter);
        mUserUseCase = userUseCase;
    }
}
