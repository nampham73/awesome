package com.genkimiru.app.presentation.setting_notification;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.NotificationSettingRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.setting_notification.NotificationSettingActivityContract.*;

public class NotificationSettingActivityPresenter
        extends BasePresenterImp<NotificationSettingView, NotificationSettingRouter>
        implements NotificationSettingPresenter {

    private UserUseCase mUserUseCase;

    public NotificationSettingActivityPresenter(Context context,
                                                NotificationSettingView notificationSettingView,
                                                NotificationSettingRouter notificationSettingRouter,
                                                UserUseCase userUseCase) {
        super(context, notificationSettingView, notificationSettingRouter);
        mUserUseCase = userUseCase;
    }

    @Override
    public void updateNotificationSetting(boolean isAnnouncementChecked,
                                          boolean isAdviceChecked,
                                          boolean isContestChecked,
                                          boolean isAcceptPush) {
        int announcement = isAnnouncementChecked ? 1 : 2;
        int advices = isAdviceChecked ? 1 : 2;
        int contest = isContestChecked ? 1 : 2;
        int acceptPush = isAcceptPush ? 1 : 2;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        NotificationSettingRequest request = new NotificationSettingRequest(announcement, advices, contest, acceptPush, token);
        mUserUseCase.execute(new UpdateNotificationSettingSubscriber(), UserParam.Factory.updateNotificationSetting(request));
    }

    private class UpdateNotificationSettingSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            Timber.d("Update notification setting success");
            mView.onUpdateSuccess();
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.e("Update notification setting error: %s", throwable.getMessage());
            mView.showMessageDialog(ApiErrorHandler.getMessage(mContext, throwable, 0));
            mView.onUpdateFail();
        }
    }
}
