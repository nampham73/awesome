package com.genkimiru.app.presentation.nac;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface NacActivityContract {

    interface NacPresenter extends BasePresenter {

    }

    interface NacView {

    }

    interface NacRouter {

    }
}
