package com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiDetailArticleModel;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.base.util.DateUtil.Format.API_FORMAT;

public class ArticleViewHolder extends BaseViewHolder implements View.OnClickListener {
    @BindView(R.id.title_article_tv)
    TextView mTvTitle;
    @BindView(R.id.description_article_tv)
    TextView mTvDescription;
    @BindView(R.id.article_wv)
    WebView mWebView;
    @BindView(R.id.detail_article_like_tv)
    TextView mTvLike;
    @BindView(R.id.detail_article_comment_tv)
    TextView mTvComment;
    @BindView(R.id.detail_article_share_tv)
    TextView mTvShare;

    private OnAdapterClickListener mOnAdapterClickListener;
    private GenkiDetailArticleModel model;
    private Locale currentLocale;


    @SuppressLint("SetJavaScriptEnabled")
    ArticleViewHolder(View itemView, OnAdapterClickListener listener) {
        super(itemView);
        mOnAdapterClickListener = listener;
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mTvLike.setOnClickListener(this);
        currentLocale = itemView.getContext().getResources().getConfiguration().locale;
    }

    @Override
    public void setData(IDetailContentModel data) {
        if (data instanceof GenkiDetailArticleModel) {
            model = (GenkiDetailArticleModel) data;
            String date = DateUtil.convertToYearMonthDayString(API_FORMAT, model.getCreatedAt(), currentLocale);
            String displayDate = "(" + date + ")" + String.format(
                    mContext.getResources().getString(R.string.content_feed_owner),
                    model.isAdmin() ? mContext.getString(R.string.content_feed_genki_text) : model.getProgramName());
            mTvTitle.setText(model.getName());
            mTvDescription.setText(displayDate);
            mTvLike.setSelected(model.getStatusLike());
            mTvLike.setText(String.valueOf(model.getLikeNumbers()));
            mTvComment.setText(String.valueOf(model.getCommentNumbers()));
            String html = getHtmlData(model.getContentArticle());
            mWebView.loadData(html, "text/html; charset=utf-8", null);
            mTvShare.setOnClickListener(this);
            mTvComment.setOnClickListener(this);
        }
    }

    private String findImageTag(String rawHtml) {
        String imgRegex = "<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";
        return findHtmlTag(imgRegex, "style=[\"'](.*)[\"']", "", rawHtml);
    }

    private String findIFrameTag(String rawHtml, String removeRegex, String replaceText) {
        String regex = "<iframe[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";
        return findHtmlTag(regex, removeRegex, replaceText, rawHtml);
    }

    private String findHtmlTag(String regex, String removeRegex, String replaceText, String rawHtml) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(rawHtml);
        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (matcher.find()) {
            String replacement = removeTag(removeRegex, matcher.group(0), replaceText);
            builder.append(rawHtml.substring(i, matcher.start()));
            if (replacement == null) builder.append(matcher.group(0));
            else builder.append(replacement);
            i = matcher.end();
        }
        builder.append(rawHtml.substring(i, rawHtml.length()));
        return builder.toString();
    }

    private String removeTag(String regex, String raw, String replaceText) {
        if (raw == null) return "";
        return raw.replaceAll(regex, replaceText);
    }

    private String getHtmlData(String bodyHTML) {
        if (bodyHTML != null && bodyHTML.toLowerCase().startsWith("<html")) {
            return bodyHTML;
        }
        String body = findImageTag(bodyHTML);
        body = findIFrameTag(body, "width=\"([^\"]+)", "width=\"100%");
        body = findIFrameTag(body, "height=\"([^\"]+)", "height=\"300");
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;}</style></head>";
        Timber.w(body);
        return "<html>" + head + "<body>" + body + "</body></html>";
    }

    @Override
    public void onClick(View v) {
        if (mOnAdapterClickListener != null) {
            mOnAdapterClickListener.onItemClicked(this, v);
        }
    }

    public GenkiDetailArticleModel getModel() {
        if (model == null) model = new GenkiDetailArticleModel();
        return model;
    }

    public void updateStateForLikeView(boolean status) {
        int like = status ? model.getLikeNumbers() + 1 : model.getLikeNumbers() - 1;
        mTvLike.setText(String.valueOf(like));
        mTvLike.setSelected(status);
        model.setStatusLike(status);
        model.setLikeNumbers(like);
    }

    public void updateStateForCommentView() {
        int comment = model.getCommentNumbers() + 1;
        mTvComment.setText(String.valueOf(comment));
        model.setCommentNumbers(comment);
    }

    public void updateShareUi() {
        int share = model.getShareNumbers() + 1;
        //mTvShare.setText(String.valueOf(share));
        model.setShareNumbers(share);
    }
}
