package com.genkimiru.app.presentation.step;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.remote.ApiErrorHandler;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.domain.mydata.MyDataParam;
import com.genkimiru.app.domain.mydata.MyDataUseCase;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.presentation.chart.BarChartActivity.CHART_COLUMN;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.DAY;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.MONTH;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.WEEK;
import static com.genkimiru.app.presentation.step.StepActivityContract.StepPresenter;
import static com.genkimiru.app.presentation.step.StepActivityContract.StepRouter;
import static com.genkimiru.app.presentation.step.StepActivityContract.StepView;

public class StepActivityPresenter extends BasePresenterImp<StepView, StepRouter>
        implements StepPresenter {

    private MyDataUseCase mMyDataUseCase;
    private Calendar mCurrentCalendar;

    public StepActivityPresenter(Context context, StepView stepView, StepRouter stepRouter, MyDataUseCase myDataUseCase) {
        super(context, stepView, stepRouter);
        mMyDataUseCase = myDataUseCase;
    }

    @Override
    public void getData(Calendar calendar, int viewType) {
        Calendar startCalendar = (Calendar) calendar.clone();
        Calendar endCalendar = (Calendar) calendar.clone();
        long currentTime = DateUtil.getToday() / 1000L;
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int dataType = DetailChartDataRequest.DataType.STEP;
        DetailChartDataRequest.Builder request
                = new DetailChartDataRequest.Builder(token)
                .setDataType(dataType);
        switch (viewType) {
            case DAY:
                getMinuteChartData(calendar);
                startCalendar.add(Calendar.DAY_OF_MONTH, -3);
                endCalendar.add(Calendar.DAY_OF_MONTH, 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startDay = startCalendar.getTimeInMillis() / 1000L;
                long endDay = endCalendar.getTimeInMillis() / 1000L;
                if (endDay > currentTime) {
                    endDay = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.DAILY)
                        .setStartDate(startDay)
                        .setEndDate(endDay);

                break;
            case WEEK:
                startCalendar.add(Calendar.DAY_OF_MONTH, -(7 * 3));
                endCalendar.add(Calendar.DAY_OF_MONTH, 7 * 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startWeek = startCalendar.getTimeInMillis() / 1000L;
                long endWeek = endCalendar.getTimeInMillis() / 1000L;
                if (endWeek > currentTime) {
                    endWeek = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.WEEKLY)
                        .setStartDate(startWeek)
                        .setEndDate(endWeek);
                break;
            case MONTH:
                startCalendar.add(Calendar.MONTH, -3);
                endCalendar.add(Calendar.MONTH, 3);
                endCalendar.set(Calendar.HOUR_OF_DAY, 23);
                endCalendar.set(Calendar.MINUTE, 59);
                long startMonth = startCalendar.getTimeInMillis() / 1000L;
                long endMonth = endCalendar.getTimeInMillis() / 1000L;
                if (endMonth > currentTime) {
                    endMonth = currentTime;
                }
                request.setChartType(DetailChartDataRequest.ChartType.MONTHLY)
                        .setStartDate(startMonth)
                        .setEndDate(endMonth);
                break;
        }

        mMyDataUseCase.execute(new StepDataSubscription(),
                MyDataParam.Factory.fetchingStepChartData(request.create()));
    }

    private void getMinuteChartData(Calendar calendar) {
        Calendar startCalendar = (Calendar) calendar.clone();
        startCalendar.add(Calendar.MINUTE, 30);
        Calendar endCalendar = (Calendar) calendar.clone();
        endCalendar.add(Calendar.DAY_OF_MONTH, 1);
        endCalendar.add(Calendar.MINUTE, 30);
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        int dataType = DetailChartDataRequest.DataType.STEP;
        long startDayForMinuteChart = startCalendar.getTimeInMillis() / 1000L;
        long endDayForMinuteChart = endCalendar.getTimeInMillis() / 1000L;
        DetailChartDataRequest.Builder requestMinuteChart = new DetailChartDataRequest.Builder(token)
                .setDataType(dataType).setChartType(DetailChartDataRequest.ChartType.MIN_5)
                .setStartDate(startDayForMinuteChart)
                .setEndDate(endDayForMinuteChart);
        mMyDataUseCase.execute(new StepMinuteDataSubscription(),
                MyDataParam.Factory.fetchingStepChartData(requestMinuteChart.create()));
    }

    private ArrayList<BarEntry> createChartData(List<GenkiStepModel> genkiStepModels) {
        if (genkiStepModels == null || genkiStepModels.size() == 0) {
            return null;
        }

        for (int i = genkiStepModels.size(); i < CHART_COLUMN; i++) {
            genkiStepModels.add(new GenkiStepModel());
        }

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        float start = 1f;

        for (int i = (int) start; i < genkiStepModels.size() + 1; i++) {
            float value = genkiStepModels.get(i - 1).getStep();
            entries.add(new BarEntry(i, value));
        }

        return entries;
    }

    public class StepDataSubscription extends DefaultSubscriber<List<GenkiStepModel>> {

        @Override
        public void onNext(List<GenkiStepModel> genkiStepModels) {
            Timber.d("Get step chart detail success");
            mView.showChartData(createChartData(genkiStepModels));
            mView.showData((genkiStepModels == null || genkiStepModels.size() == 0)
                    ? null
                    : genkiStepModels.get(genkiStepModels.size() / 2));
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.d("Get step chart detail error");
            mView.loadDataFail(ApiErrorHandler.getMessage(mContext, throwable, 0));
        }
    }

    public class StepMinuteDataSubscription extends DefaultSubscriber<List<GenkiStepModel>> {
        @Override
        public void onNext(List<GenkiStepModel> genkiStepModels) {
            Timber.d("Get minute step chart detail success");
            mView.showMinuteDataChart(createChartData(genkiStepModels));
        }

        @Override
        public void onError(Throwable throwable) {
            Timber.d("Get minute step chart detail error");
        }
    }
}
