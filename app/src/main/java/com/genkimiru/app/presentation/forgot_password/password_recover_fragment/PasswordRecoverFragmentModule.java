package com.genkimiru.app.presentation.forgot_password.password_recover_fragment;

import android.content.Context;

import com.genkimiru.app.base.scope.FragmentScope;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragmentContract.*;

@Module
public class PasswordRecoverFragmentModule {

    @Provides
    @FragmentScope
    PasswordRecoverView provideView(PasswordRecoverFragment passwordRecoverFragment) {
        return passwordRecoverFragment;
    }

    @Provides
    @FragmentScope
    PasswordRecoverRouter provideRouter(PasswordRecoverFragment passwordRecoverFragment) {
        return passwordRecoverFragment;
    }

    @Provides
    @FragmentScope
    PasswordRecoverPresenter providePresenter(Context context, PasswordRecoverView view,
                                              PasswordRecoverRouter router, UserUseCase userUseCase) {
        return new PasswordRecoverFragmentPresenter(context, view, router, userUseCase);
    }
}
