package com.genkimiru.app.presentation.main.contest_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.BaseSliderFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.dialog.ContestSortDialogFragment;
import com.genkimiru.app.widget.WheelView;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

import static com.genkimiru.app.presentation.main.contest_fragment.PagerContestFragment.SortType.ASC;
import static com.genkimiru.app.presentation.main.contest_fragment.PagerContestFragment.SortType.DESC;

public class PagerContestFragment extends BaseSliderFragment<ContestFragmentAdapter, BasePresenter>
        implements WheelView.OnWheelViewListener {

    @BindView(R.id.fragment_badge_contest_pager)
    ViewPager mViewPager;
    @BindView(R.id.fragment_badge_contest_tab)
    TabLayout mTabLayout;

    public static PagerContestFragment newInstance() {
        Bundle args = new Bundle();
        PagerContestFragment fragment = new PagerContestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    ContestFragmentAdapter mAdapter;
    @Inject
    ContestSortDialogFragment mSortDialogFragment;

    @BindView(R.id.contest_sort_tv)
    TextView mTvSortView;
    private int position = 0;
    private String mSortType = ASC;
    private String mSortName = "start_date";


    @BindView(R.id.contest_sort_cbx)
    CheckBox mSortCheckBox;

    @Override
    protected ViewPager getViewPager() {
        mViewPager.setOffscreenPageLimit(5);
        return mViewPager;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSortDialogFragment.setSortListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        RxCompoundButton.checkedChanges(mSortCheckBox)
                .skip(1)
                .subscribe(isSortDown -> {
                    mSortType = isSortDown ? DESC : ASC;
                    List<Fragment> fragments = getChildFragmentManager().getFragments();
                    if (fragments!=null && fragments.size() > 0){
                        for (Fragment fragment : fragments) {
                            if (fragment instanceof ContestsListFragment) {
                                ((ContestsListFragment) fragment).requestSorNameAction(mSortName, mSortType);
                            }
                        }
                    }
                });
    }

    @Override
    protected TabLayout getTabLayout() {
        return mTabLayout;
    }

    @Override
    protected ContestFragmentAdapter getPagerAdapter() {
        return mAdapter;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_main_contest;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @OnClick(R.id.contest_sort_tv)
    void openSortDialog() {
        mSortDialogFragment.setIndexSelected(position);
        if (!mSortDialogFragment.isAdded()){
            mSortDialogFragment.show(getChildFragmentManager());
        }
    }

    @Override
    public void onWheelViewItemSelected(int selectedIndex, WheelView.Result item) {
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments!=null && fragments.size() > 0){
            for (Fragment fragment : fragments) {
                if (fragment instanceof ContestsListFragment) {
                    ((ContestsListFragment) fragment).requestSorNameAction(item.id, mSortType);
                    mSortName = item.id;
                }
            }
            position = selectedIndex;
            mTvSortView.setText(item.name);
        }
    }

    public @interface SortType {
        String DESC = "desc";
        String ASC = "asc";
    }
}
