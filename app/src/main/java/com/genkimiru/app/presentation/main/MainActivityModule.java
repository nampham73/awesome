package com.genkimiru.app.presentation.main;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiDashboardDataModel;
import com.genkimiru.app.dialog.ContestSortDialogFragment;
import com.genkimiru.app.widget.SleepChartView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {
    private static final String FRAGMENT_MANAGER = "MainActivityModule.fragmentManager";

    @Provides
    @ActivityScope
    MainContract.MainView provideView(MainActivity mainActivity, @Named(FRAGMENT_MANAGER) FragmentManager fragmentManager) {
        return new MainActivityView(mainActivity, fragmentManager);
    }

    @Provides
    @ActivityScope
    @Named(FRAGMENT_MANAGER)
    FragmentManager provideFragmentManager(MainActivity activity) {
        return activity.getSupportFragmentManager();
    }

    @Provides
    @ActivityScope
    MainContract.MainRouter provideRouter(MainActivity mainActivity) {
        return mainActivity;
    }

    @Provides
    @ActivityScope
    MainContract.MainPresenter providePresenter(Context context, MainContract.MainView mainView, MainContract.MainRouter mainRouter) {
        return new MainActivityPresenter(context, mainView, mainRouter);
    }

    @Provides
    @ActivityScope
    GenkiDashboardDataModel provideGenkiDashboardDataModel() {
        return new GenkiDashboardDataModel();
    }

    @Provides
    @ActivityScope
    List<SleepChartView.Entry> provideEntries() {
        return new ArrayList<>();
    }
}
