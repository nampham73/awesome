package com.genkimiru.app.presentation.content_detail;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;

public interface ContentDetailContract {
    interface ContentDetailPresenter extends BasePresenter{}
    interface ContentDetailView extends BaseView<ContentDetailPresenter>{}
    interface ContentDetailRouter{}
}
