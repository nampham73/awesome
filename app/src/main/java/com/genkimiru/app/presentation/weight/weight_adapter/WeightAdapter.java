package com.genkimiru.app.presentation.weight.weight_adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.adapter.BaseSimpleAdapter;
import com.genkimiru.app.data.model.WeightModel;

public class WeightAdapter extends BaseSimpleAdapter<WeightModel, WeightViewHolder> {

    public WeightAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemLayoutResource() {
        return R.layout.layout_record_weight;
    }

    @Override
    public WeightViewHolder createHolder(View view) {
        return new WeightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
    }
}
