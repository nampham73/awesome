package com.genkimiru.app.presentation.energy;

import android.os.Bundle;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.genkimiru.app.presentation.chart.BarChartActivity;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;

import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.DAY;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.MONTH;
import static com.genkimiru.app.presentation.chart.BaseChartActivity.ViewType.WEEK;
import static com.genkimiru.app.presentation.energy.EnergyActivityContract.*;

public class EnergyActivity extends BarChartActivity<EnergyPresenter> implements EnergyView, EnergyRouter {

    @BindView(R.id.energy_cal_in_tv)
    TextView mCalInTv;
    @BindView(R.id.energy_cal_out_tv)
    TextView mCalOutTv;

    @Inject
    EnergyPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_energy;
    }

    @Override
    protected EnergyPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void onDayChange(Calendar calendar) {
        super.onDayChange(calendar);
        showProgress();
        hideSummarizeAndAverageText();
        mPresenter.getData(calendar, DAY);
    }

    @Override
    protected void onWeekChange(Calendar calendar) {
        super.onWeekChange(calendar);
        showProgress();
        showAverageText();
        mPresenter.getData(calendar, WEEK);
    }

    @Override
    protected void onMonthChange(Calendar calendar) {
        super.onMonthChange(calendar);
        showProgress();
        showAverageText();
        mPresenter.getData(calendar, MONTH);
    }

    @Override
    protected int getMainColor() {
        return R.color.colorPrimary;
    }

    @Override
    protected BarChart getMainChart() {
        return findViewById(R.id.energy_bar_chart);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.energy_title);
    }

    @Override
    public void showChartData(BarData barData) {
        hideProgress();
        if (barData == null) {
            mMainChart.clear();
            return;
        }

        if (mMainChart.getBarData() != null) {
            mMainChart.clearValues();
        }

        barData.setValueTextSize(10f);
        barData.setBarWidth(0.2f);
        mMainChart.setData(barData);

        float barSpace = 0.02f;
        float groupSpace = 0.4f;
        int groupCount = 7;

        mMainChart.getXAxis().setAxisMinimum(0);
        mMainChart.getXAxis().setAxisMaximum(0 + mMainChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        mMainChart.groupBars(0, groupSpace, barSpace); // perform the "explicit" grouping
        mMainChart.invalidate();
    }

    @Override
    public void showDate(GenkiEnergyModel genkiEnergyModel) {
        if(genkiEnergyModel == null) {
            return;
        }

        mCalInTv.setText(StringUtil.formatUnnecessaryZero(genkiEnergyModel.getAverageKCalIn()));
        mCalOutTv.setText(StringUtil.formatUnnecessaryZero(genkiEnergyModel.getAverageKCalOut()));
    }

    @Override
    public void loadDataFail(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }

    @Override
    protected void setBarChartData(ArrayList<BarEntry> entries, int barColor) {
        // Do nothing
    }
}
