package com.genkimiru.app.presentation.update_user_infor.alcohol;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.dialog.CheckboxDialog;
import com.jakewharton.rxbinding2.widget.RxRadioGroup;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivityContract.UpdateAlcoholPresenter;
import static com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivityContract.UpdateAlcoholRouter;
import static com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivityContract.UpdateAlcoholView;

public class UpdateAlcoholActivity extends SimpleBaseActivity<UpdateAlcoholPresenter> implements UpdateAlcoholView, UpdateAlcoholRouter {
    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_alcohol_radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.register_alcohol_1_level_each_time_tv)
    TextView mLevelEachTime1;
    @BindView(R.id.register_alcohol_2_level_each_time_tv)
    TextView mLevelEachTime2;
    @BindView(R.id.register_alcohol_3_level_each_time_tv)
    TextView mLevelEachTime3;
    @BindView(R.id.register_alcohol_4_level_each_time_tv)
    TextView mLevelEachTime4;
    @BindView(R.id.register_alcohol_5_level_each_time_tv)
    TextView mLevelEachTime5;

    private int mCurrentAlcoholButtonId;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;
    @Inject
    UpdateAlcoholPresenter mUpdateAlcoholPresenter;

    private CheckboxDialog alcoholDialog;
    private Disposable mDisposable;
    private String mLevelTxt;
    private boolean isDialogShowing = false;
    private MenuItem mApplyMenuItem;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        initialDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_alcohol;
    }

    @Override
    protected UpdateAlcoholPresenter registerPresenter() {
        return mUpdateAlcoholPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }

    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mUpdateAlcoholPresenter.updateAlcohol(mUserInfoRequest.getAlcoholLevel(), mUserInfoRequest.getDrinkEachTime());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onResume() {
        super.onResume();
        initialDefaultValue();
        mCurrentAlcoholButtonId = radioGroup.getCheckedRadioButtonId();
        mDisposable = RxRadioGroup.checkedChanges(radioGroup)
                .skip(1) // Skip init value
                .subscribe(buttonId -> {
                    mCurrentAlcoholButtonId = buttonId;
                    setEnabledMenuApply(true);
                    switch (buttonId) {
                        case R.id.register_1_radio_button:
                            Timber.d("Alcohol level: 1");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_1.getValue());
                            openAlcoHolDialog();
                            break;
                        case R.id.register_2_radio_button:
                            Timber.d("Alcohol level: 2");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_2.getValue());
                            openAlcoHolDialog();
                            break;
                        case R.id.register_3_radio_button:
                            Timber.d("Alcohol level: 3");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_3.getValue());
                            openAlcoHolDialog();
                            break;
                        case R.id.register_4_radio_button:
                            Timber.d("Alcohol level: 4");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_4.getValue());
                            openAlcoHolDialog();
                            break;
                        case R.id.register_5_radio_button:
                            Timber.d("Alcohol level: 5");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_5.getValue());
                            openAlcoHolDialog();
                            break;
                        case R.id.register_6_radio_button:
                            Timber.d("Alcohol level: 6");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_6.getValue());
                            mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkLevel.LEVEL_NO_DRINK.getValue());
                            setDefaultView();
                            break;
                        case R.id.register_7_radio_button:
                            Timber.d("Alcohol level: 7");
                            mUserInfoRequest.setAlcoholLevel(RegisterUserRequest.DrinkLevel.LEVEL_7.getValue());
                            mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkLevel.LEVEL_NO_DRINK.getValue());
                            setDefaultView();
                            break;
                    }
                });
        setTextLevelEachTime();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    private void openAlcoHolDialog() {
        Timber.d("Open dialog alcohol");
        if (isDialogShowing) {
            Timber.d("Skip open dialog alcohol");
            return;
        }
        Timber.d("is Showing: ");
        alcoholDialog = new CheckboxDialog.CheckboxDialogBuilder()
                .setTitle(getString(R.string.register_alcohol_title_dialog))
                .setLayout(R.layout.dialog_checbox_alcohol_content)
                .dialogListener(new CheckboxDialog.DialogListener() {
                    @Override
                    public void onComplete(CheckboxDialog checkboxDialog) {
                        setTextLevelEachTime();
                        checkboxDialog.dismiss();
                    }

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case R.id.register_alcohol_180ml_rb:
                                Timber.d("Alcohol level each time: %d", 1);
                                mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_1_180.getValue());
                                mLevelTxt = getString(R.string.register_alcohol_180ml);
                                break;
                            case R.id.register_alcohol_360ml_rb:
                                Timber.d("Alcohol level each time: %d", 2);
                                mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_2_360.getValue());
                                mLevelTxt = getString(R.string.register_alcohol_360ml);
                                break;
                            case R.id.register_alcohol_540ml_rb:
                                Timber.d("Alcohol level each time: %d", 3);
                                mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_3_540.getValue());
                                mLevelTxt = getString(R.string.register_alcohol_540ml);
                                break;
                            case R.id.register_alcohol_720ml_rb:
                                Timber.d("Alcohol level each time: %d", 4);
                                mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_4_720.getValue());
                                mLevelTxt = getString(R.string.register_alcohol_720ml);
                                break;
                            case R.id.register_alcohol_900ml_rb:
                                Timber.d("Alcohol level each time: %d", 5);
                                mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_5_900.getValue());
                                mLevelTxt = getString(R.string.register_alcohol_900ml);
                                break;
                            case R.id.register_alcohol_900ml_more_rb:
                                Timber.d("Alcohol level each time: %d", 6);
                                mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_6_MORE_900.getValue());
                                mLevelTxt = getString(R.string.register_alcohol_900ml_more);
                                break;
                        }
                    }

                    @Override
                    public void onOpen(int checkedId) {
                        mUserInfoRequest.setDrinkEachTime(RegisterUserRequest.DrinkEachTime.LEVEL_1_180.getValue());
                        mLevelTxt = getString(R.string.register_alcohol_180ml);
                    }

                    @Override
                    public void onDismiss() {
                        isDialogShowing = false;
                    }
                }).build();
        alcoholDialog.show(getSupportFragmentManager());
        isDialogShowing = true;
    }

    private void setTextLevelEachTime() {
        int id = radioGroup.getCheckedRadioButtonId();
        switch (id) {
            case R.id.register_1_radio_button:
                mLevelEachTime1.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.VISIBLE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_2_radio_button:
                mLevelEachTime2.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.VISIBLE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_3_radio_button:
                mLevelEachTime3.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.VISIBLE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_4_radio_button:
                mLevelEachTime4.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.VISIBLE);
                mLevelEachTime5.setVisibility(View.GONE);
                break;
            case R.id.register_5_radio_button:
                mLevelEachTime5.setText(mLevelTxt);
                mLevelEachTime1.setVisibility(View.GONE);
                mLevelEachTime2.setVisibility(View.GONE);
                mLevelEachTime3.setVisibility(View.GONE);
                mLevelEachTime4.setVisibility(View.GONE);
                mLevelEachTime5.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initialDefaultValue() {
        setAlcoholLevel();
    }

    private void setAlcoholLevel() {
        int alcoholLevel = mGenkiUserInfoModel.getAlcoholLevel();
        switch (RegisterUserRequest.DrinkLevel.getFromValue(alcoholLevel)) {
            case LEVEL_1:
                radioGroup.check(R.id.register_1_radio_button);
                break;
            case LEVEL_2:
                radioGroup.check(R.id.register_2_radio_button);
                break;
            case LEVEL_3:
                radioGroup.check(R.id.register_3_radio_button);
                break;
            case LEVEL_4:
                radioGroup.check(R.id.register_4_radio_button);
                break;
            case LEVEL_5:
                radioGroup.check(R.id.register_5_radio_button);
                break;
            case LEVEL_6:
                radioGroup.check(R.id.register_6_radio_button);
                break;
            case LEVEL_7:
                radioGroup.check(R.id.register_7_radio_button);
                break;
            case NONE:
                break;

        }
        mCurrentAlcoholButtonId = radioGroup.getCheckedRadioButtonId();
        setAlcoholEachTime();
    }

    private void setAlcoholEachTime() {
        int alcoholEachTime = mGenkiUserInfoModel.getDrinkEachTime();
        switch (RegisterUserRequest.DrinkEachTime.getFromValue(alcoholEachTime)) {
            case LEVEL_1_180:
                mLevelTxt = getString(R.string.register_alcohol_180ml);
                break;
            case LEVEL_2_360:
                mLevelTxt = getString(R.string.register_alcohol_360ml);
                break;
            case LEVEL_3_540:
                mLevelTxt = getString(R.string.register_alcohol_540ml);
                break;
            case LEVEL_4_720:
                mLevelTxt = getString(R.string.register_alcohol_720ml);
                break;
            case LEVEL_5_900:
                mLevelTxt = getString(R.string.register_alcohol_900ml);
                break;
            case LEVEL_6_MORE_900:
                mLevelTxt = getString(R.string.register_alcohol_900ml_more);
                break;
            case NONE:
                break;

        }
    }

    private void setDefaultView() {
        mLevelEachTime1.setVisibility(View.GONE);
        mLevelEachTime2.setVisibility(View.GONE);
        mLevelEachTime3.setVisibility(View.GONE);
        mLevelEachTime4.setVisibility(View.GONE);
        mLevelEachTime5.setVisibility(View.GONE);
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
