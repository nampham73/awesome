package com.genkimiru.app.presentation.eula;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.genkimiru.app.R;
import com.genkimiru.app.presentation.eula.fragment.EulaFragment;

public class EulaPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public EulaPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        TypeView typeView = TypeView.values()[position];
        EulaFragment eulaFragment = new EulaFragment();
        eulaFragment.setLayoutTypeView(typeView);
        return eulaFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        TypeView typeView = TypeView.values()[position];

        if (typeView == null) {
            return "";
        }

        switch (typeView) {
            case TERMS_OF_USE:
                return mContext.getString(R.string.eula_term_of_use);
            case PRIVACY_POLICY:
                return mContext.getString(R.string.eula_policy);
            default:
                return "";

        }
    }

    @Override
    public int getCount() {
        return TypeView.values().length;
    }

    public enum TypeView {

        TERMS_OF_USE(0),
        PRIVACY_POLICY(1);

        private int mIndex;

        TypeView(int index) {
            this.mIndex = index;
        }

        public int getIndex() {
            return mIndex;
        }
    }
}
