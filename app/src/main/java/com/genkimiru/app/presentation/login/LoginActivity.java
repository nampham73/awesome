package com.genkimiru.app.presentation.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.BaseActivity;
import com.genkimiru.app.data.model.LoginUserInfoModel;
import com.genkimiru.app.presentation.eula.EulaActivity;
import com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivity;
import com.genkimiru.app.presentation.intro.IntroActivity;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.EULA_INDEX;
import static com.genkimiru.app.Constants.Extra.IS_LOGIN_STATE;
import static com.genkimiru.app.Constants.Facebook.PARAM_EMAIL;
import static com.genkimiru.app.Constants.Facebook.PARAM_PUBLIC_PROFILE;
import static com.genkimiru.app.Constants.Facebook.PARAM_PUBLIC_USER_BIRTHDAY;
import static com.genkimiru.app.Constants.Facebook.PARAM_PUBLIC_USER_GENDER;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginPresenter;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginRouter;
import static com.genkimiru.app.presentation.login.LoginActivityContract.LoginView;
import static com.genkimiru.app.presentation.login.LoginActivityView.RC_SIGN_IN;

public class LoginActivity extends BaseActivity<LoginPresenter, LoginView> implements LoginRouter {

    @Inject
    LoginPresenter mLoginPresenter;
    @Inject
    LoginView mLoginView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.login_facebook_btn)
    RelativeLayout mLoginFacebook;

    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter registerPresenter() {
        return mLoginPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected LoginView registerView() {
        return mLoginView;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
        mToolbarTitle.setText(R.string.login);
    }

    @Override
    public void openEulaScreen(int index) {
        Timber.d("openEulaScreen index = %s", index);
        Intent intent = new Intent(LoginActivity.this, EulaActivity.class);
        intent.putExtra(EULA_INDEX, index);
        startActivity(intent);
    }

    @Override
    public void openForgotPasswordScreen() {
        Timber.d("openForgotPasswordScreen");
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void openDashboardScreen() {
        Timber.d("openDashboardScreen");
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void openFacebookScreen() {
        mLoginPresenter.loginFacebook(mCallbackManager);
        Collection<String> permissions = Arrays.asList(PARAM_PUBLIC_PROFILE, PARAM_PUBLIC_USER_BIRTHDAY, PARAM_PUBLIC_USER_GENDER, PARAM_EMAIL);
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, permissions);
    }

    @Override
    public void openRegisterScreen(LoginUserInfoModel loginUserInfoModel) {
        Timber.d("openRegisterScreen");
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.putExtra(IS_LOGIN_STATE, true);
        intent.putExtra(Constants.Extra.DETAIL_LOGIN_FACEBOOK, loginUserInfoModel);
        startActivity(intent);
        finish();
    }

    public void openRegisterScreenFromLoginGoole(LoginUserInfoModel loginUserInfoModel) {
        Timber.d("openRegisterScreen");
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.putExtra(IS_LOGIN_STATE, true);
        intent.putExtra(Constants.Extra.DETAIL_LOGIN_GOOGLE, loginUserInfoModel);
        startActivity(intent);
        finish();
    }

    @Override
    public void openMainActivity() {
        Timber.d("openMainActivityScreen");
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            mLoginPresenter.handleSignInResult(task);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, IntroActivity.class);
        startActivity(intent);
        finish();
    }
}
