package com.genkimiru.app.presentation.setting_privacy;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyPresenter;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyRouter;
import static com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityContract.SettingPrivacyView;

@Module
public class SettingPrivacyActivityModule {

    @Provides
    @ActivityScope
    SettingPrivacyView providePrivacyView(SettingPrivacyActivity settingPrivacyActivity) {
        return settingPrivacyActivity;
    }

    @Provides
    @ActivityScope
    SettingPrivacyRouter providePrivacyRouter(SettingPrivacyActivity settingPrivacyActivity) {
        return settingPrivacyActivity;
    }

    @Provides
    @ActivityScope
    SettingPrivacyPresenter providePresenter(Context context, SettingPrivacyView settingPrivacyView, SettingPrivacyRouter settingPrivacyRouter, UserUseCase userUseCase) {
        return new SettingPrivacyActivityPresenter(context, settingPrivacyView, settingPrivacyRouter, userUseCase);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
