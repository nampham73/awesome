package com.genkimiru.app.presentation.register.nickname_fragment;

import android.content.Context;

import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragmentContract.NicknamePresenter;

public class NicknameFragmentPresenter extends BasePresenterImp<NicknameFragmentContract.NicknameView, NicknameFragmentContract.NicknameRouter>
        implements NicknamePresenter {

    private UserUseCase mUserUseCase;

    public NicknameFragmentPresenter(Context context, NicknameFragmentContract.NicknameView nicknameView,
                                     NicknameFragmentContract.NicknameRouter nicknameRouter, UserUseCase userUseCase) {
        super(context, nicknameView, nicknameRouter);
        mUserUseCase = userUseCase;
    }
}
