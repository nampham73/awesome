package com.genkimiru.app.presentation.register.height_frament;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HeightFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = HeightFragmentModule.class)
    abstract HeightFragment heightFragment();
}
