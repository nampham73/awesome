package com.genkimiru.app.presentation.bug_report;

import android.content.Context;
import android.os.Build;

import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.mvp.presenter.BasePresenterImp;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.ImageUtils;
import com.genkimiru.app.data.remote.extension.request.BugReportRequest;
import com.genkimiru.app.data.remote.extension.request.BugReportUploadImageRequest;
import com.genkimiru.app.data.remote.extension.response.BugReportUploadImageResponse;
import com.genkimiru.app.domain.bugreport.BugReportParam;
import com.genkimiru.app.domain.bugreport.BugReportUseCase;
import com.genkimiru.app.presentation.bug_report.adapter.BugImage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import timber.log.Timber;

import static com.genkimiru.app.presentation.bug_report.BugReportActivityContract.*;

public class BugReportActivityPresenter extends BasePresenterImp<BugReportView, BugReportRouter> implements BugReportPresenter {

    private BugReportUseCase mBugReportUseCase;
    private String mTitle;
    private String mProblem;
    private String mSeverity;
    private String mUsername;

    public BugReportActivityPresenter(Context context,
                                      BugReportView bugReportView,
                                      BugReportRouter bugReportRouter,
                                      BugReportUseCase bugReportUseCase) {
        super(context, bugReportView, bugReportRouter);
        mBugReportUseCase = bugReportUseCase;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBugReportUseCase.clear();
    }

    @Override
    public void sendBugReport(String title,
                              String problem,
                              String severity,
                              List<BugImage> bugImageList,
                              String username) {
        mTitle = title;
        mProblem = problem;
        mSeverity = severity;
        mUsername = username;

        if (bugImageList.size() > 1) {
            callUploadImageApi(bugImageList);
            return;
        }

        callReportBugApi(null);
    }

    private void callUploadImageApi(List<BugImage> bugImageList) {
        List<BugReportUploadImageRequest> bugReportUploadImageRequests = new ArrayList<>();
        for (int i = 0; i < bugImageList.size() - 1; i++) {
            BugImage bugImage = bugImageList.get(i);
            BugReportUploadImageRequest request = new BugReportUploadImageRequest(
                    ImageUtils.convertToBase64(bugImage.getImageUri(), mContext),
                    bugImage.getTitle());
            bugReportUploadImageRequests.add(request);
        }
        mBugReportUseCase.execute(new BugReportUploadImageSubscriber(),
                BugReportParam.Factory.uploadImage(bugReportUploadImageRequests));
    }

    private void callReportBugApi(List<String> uploads) {
        List<BugReportRequest.CustomField> customFields = createCustomFields(mUsername);
        BugReportRequest.Comment comment = new BugReportRequest.Comment(mProblem, uploads);
        BugReportRequest.Ticket ticket = new BugReportRequest.Ticket();
        ticket.subject = mTitle;
        ticket.priority = mSeverity;
        ticket.comment = comment;
        ticket.custom_fields = customFields;

        mBugReportUseCase.execute(new BugReportSubscriber(),
                BugReportParam.Factory.reportBug(new BugReportRequest(ticket)));
    }

    private List<BugReportRequest.CustomField> createCustomFields(String username) {
        List<BugReportRequest.CustomField> customFields = new ArrayList<>();

        BugReportRequest.CustomField customFieldUserId
                = new BugReportRequest.CustomField(BugReportRequest.CustomFieldType.USER_ID.id,username);

        BugReportRequest.CustomField customFieldCreatedDate = new BugReportRequest.CustomField(
                BugReportRequest.CustomFieldType.CREATE_DATE.id,
                DateUtil.getCustomISO8601Day(Calendar.getInstance()));

        String deviceInfo = Build.PRODUCT + " "
                + Build.DEVICE + " "
                + Build.MODEL + " "
                + Build.VERSION.SDK_INT;
        BugReportRequest.CustomField customFieldDevice
                = new BugReportRequest.CustomField(BugReportRequest.CustomFieldType.DEVICE.id, deviceInfo);

        customFields.add(customFieldUserId);
        customFields.add(customFieldCreatedDate);
        customFields.add(customFieldDevice);
        return customFields;
    }

    private class BugReportUploadImageSubscriber extends DefaultSubscriber<List<BugReportUploadImageResponse>> {

        @Override
        public void onNext(List<BugReportUploadImageResponse> bugReportUploadImageResponses) {
            Timber.d("Bug report upload image success: %s", bugReportUploadImageResponses);
            List<String> uploadTokens = new ArrayList<>();
            for(BugReportUploadImageResponse response: bugReportUploadImageResponses) {
                Timber.d("uploadTokens = %s", response.upload.token);
                uploadTokens.add(response.upload.token);
            }
            callReportBugApi(uploadTokens);
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Bug report upload image fail");
            mView.onReportFail();
        }
    }

    private class BugReportSubscriber extends DefaultSubscriber<ResponseBody> {

        @Override
        public void onNext(ResponseBody responseBody) {
            Timber.d("Bug report success: %s", responseBody);
            mView.onReportSuccess();
        }

        @Override
        public void onError(Throwable t) {
            Timber.d("Bug report fail");
            mView.onReportFail();
        }
    }
}
