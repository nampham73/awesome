package com.genkimiru.app.presentation.setting_email_marketing;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface SettingEmailMarketingActivityContract {

    interface SettingEmailMarketingPresenter extends BasePresenter {
        void updateEmailMarketingSetting(boolean isReceiveEmailChecked);

    }

    interface SettingEmailMarketingView {
        void showMessageDialog(String message);
        void onUpdateFail();
        void onUpdateSuccess();
    }

    interface SettingEmailMarketingRouter {

    }
}
