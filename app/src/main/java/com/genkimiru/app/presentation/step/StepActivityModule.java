package com.genkimiru.app.presentation.step;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.mydata.MyDataRepository;
import com.genkimiru.app.data.repository.mydata.MyDataRepositoryImp;
import com.genkimiru.app.domain.mydata.MyDataUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.step.StepActivityContract.*;

@Module
public class StepActivityModule {

    @Provides
    @ActivityScope
    StepView provideView(StepActivity stepActivity) {
        return stepActivity;
    }

    @Provides
    @ActivityScope
    StepRouter provideRouter(StepActivity stepActivity) {
        return stepActivity;
    }

    @Provides
    @ActivityScope
    StepPresenter providePresenter(Context context, StepView view, StepRouter router, MyDataUseCase myDataUseCase) {
        return new StepActivityPresenter(context, view, router, myDataUseCase);
    }

    @Provides
    @ActivityScope
    MyDataUseCase provideUseCase(SchedulerProvider schedulerProvider, MyDataRepository repository){
        return new MyDataUseCase(schedulerProvider.io(), schedulerProvider.ui(), repository);
    }

    @Provides
    @ActivityScope
    MyDataRepository provideRepository(GenkiApi api){
        return new MyDataRepositoryImp(api);
    }

}
