package com.genkimiru.app.presentation.splash;

import android.content.Intent;
import android.os.Bundle;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.presentation.intro.IntroActivity;
import com.genkimiru.app.presentation.login.LoginActivity;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.step.StepActivity;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static com.genkimiru.app.presentation.splash.SplashActivityContract.*;

public class SplashActivity extends SimpleBaseActivity<SplashPresenter> implements SplashView, SplashRouter {

    private static final int TIME_OUT = 2500; // ms

    @Inject
    SplashPresenter mPresenter;

    private Disposable mStartIntroDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mStartIntroDisposable = Single.just(true)
                .delay(TIME_OUT, TimeUnit.MILLISECONDS)
                .subscribe(aBoolean -> checkScreenNavigation());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mStartIntroDisposable != null) {
            mStartIntroDisposable.dispose();
            mStartIntroDisposable = null;
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_splash;
    }

    @Override
    protected SplashPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void openIntroActivity() {
        Timber.d("openIntroActivity");
        Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {
        Timber.d("openLoginActivity");
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openMainActivity() {
        Timber.d("openMainActivity");
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkScreenNavigation() {
        if (!mPresenter.checkUserInformation()) {
            openIntroActivity();
            return;
        }
        mPresenter.getUserInformation();
    }

    @Override
    public void onGetUserInfoSuccess() {
        openMainActivity();
    }

    @Override
    public void onGetUserInfoError(String message) {
        MessageUtil.createMessageDialog(message, new TextDialogFragment.OnResultListener() {
            @Override
            public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                openIntroActivity();
            }

            @Override
            public void onClickRightButton(TextDialogFragment textDialogFragment) {
                // Do nothing
            }
        }).show(getSupportFragmentManager());
    }
}
