package com.genkimiru.app.presentation.main.contest_fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.genkimiru.app.R;
import com.genkimiru.app.presentation.main.contest_fragment.current.CurrentContestFragment;
import com.genkimiru.app.presentation.main.contest_fragment.history.HistoryContestFragment;
import com.genkimiru.app.presentation.main.contest_fragment.privates.PrivatesContestFragment;
import com.genkimiru.app.presentation.main.contest_fragment.publics.PublicsContestFragment;
import com.genkimiru.app.presentation.main.contest_fragment.recommend.RecommendContestFragment;

public class ContestFragmentAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private SparseArray<Fragment> mAvailableFragments;
    public ContestFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        mAvailableFragments = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int position) {
        TypeContest typeContest = TypeContest.values()[position];
        switch (typeContest) {
            case CURRENT:
                return new CurrentContestFragment();
            case PRIVATES:
                return new PrivatesContestFragment();
            case PUBLICS:
                return new PublicsContestFragment();
            case RECOMMEND:
                return new RecommendContestFragment();
            case HISTORY:
                return new HistoryContestFragment();
            default:
                return null;
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        mAvailableFragments.put(position, (Fragment) obj);
        return obj;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mAvailableFragments.remove(position);
    }

    @Override
    public int getCount() {
        return TypeContest.values().length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        TypeContest typeContest = TypeContest.values()[position];
        switch (typeContest) {
            case CURRENT:
                return mContext.getString(R.string.contests_current);
            case PRIVATES:
                return mContext.getString(R.string.contests_privates);
            case PUBLICS:
                return mContext.getString(R.string.contests_publics);
            case RECOMMEND:
                return mContext.getString(R.string.contests_recommend);
            case HISTORY:
                return mContext.getString(R.string.contests_history);
            default:
                return "";
        }
    }

    public Fragment getAvailableFragment(int position) {
        return mAvailableFragments.get(position);
    }

    public enum TypeContest {
        CURRENT(0),
        PRIVATES(1),
        PUBLICS(2),
        RECOMMEND(3),
        HISTORY(4);
        private int mIndex;

        TypeContest(int mIndex) {
            this.mIndex = mIndex;
        }

        public int getIndex() {
            return mIndex;
        }
    }
}
