package com.genkimiru.app.presentation.change_password;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.presentation.change_password.ChangePasswordActivityContract.*;

@Module
public class ChangePasswordActivityModule {

    @Provides
    @ActivityScope
    ChangePasswordRouter provideRouter(ChangePasswordActivity changePasswordActivity) {
        return changePasswordActivity;
    }

    @Provides
    @ActivityScope
    ChangePasswordView provideView(ChangePasswordActivity changePasswordActivity) {
        return changePasswordActivity;
    }

    @Provides
    @ActivityScope
    ChangePasswordPresenter providePresenter(Context context, ChangePasswordView view, ChangePasswordRouter router, UserUseCase userUseCase) {
        return new ChangePasswordActivityPresenter(context, view, router, userUseCase);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserUseCase provideUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository){
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
