package com.genkimiru.app.presentation.photo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.Constants;
import com.genkimiru.app.R;
import com.genkimiru.app.base.permission.PermissionsManager;
import com.genkimiru.app.base.permission.PermissionsResultAction;
import com.genkimiru.app.base.util.StorageUtil;

import java.io.File;

import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Extra.CROP;
import static com.genkimiru.app.Constants.Extra.RETAKE_CODE;
import static com.genkimiru.app.Constants.FileName.CAPTURE_IMAGE_NAME;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_CAMERA;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_CAMERA_NO_CROP;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_CROP;
import static com.genkimiru.app.Constants.RequestCode.REQUEST_CODE_GALLERY;

public class PhotoActivity extends AppCompatActivity{

    private static final String FILE_PATH_CAMERA = StorageUtil.getExternalImagePath(CAPTURE_IMAGE_NAME);

    private Type mActionType;

    public enum Type {
        Capture,
        Gallery,
        Take
    }

    public static Intent createIntent(Context context, Type type) {
        Intent intent = new Intent(context, PhotoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.Extra.GET_PHOTO_TYPE, type);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = savedInstanceState != null ? savedInstanceState : getIntent().getExtras();
        mActionType = (Type) bundle.getSerializable(Constants.Extra.GET_PHOTO_TYPE);
        switch (mActionType) {
            case Capture:
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this, PermissionsManager.Type.CAMERA_STORAGE.getPermissions(), new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        startCameraActivity();
                    }

                    @Override
                    public void onDenied(String permission) {
                        finish();
                    }
                });
                break;
            case Gallery:
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this, PermissionsManager.Type.STORAGE.getPermissions(), new PermissionsResultAction() {
                    @Override
                    public void onGranted() {
                        startGalleryActivity();
                    }

                    @Override
                    public void onDenied(String permission) {
                        finish();
                    }
                });
                break;
            case Take:
                PermissionsManager.getInstance().requestPermissionsIfNecessaryForResult(this, PermissionsManager.Type.CAMERA_STORAGE.getPermissions(), new PermissionsResultAction() {
                    @Override
                    public void onGranted() { startCameraNoCropActivity(); }

                    @Override
                    public void onDenied(String permission) {
                        finish();
                    }
                });
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.Extra.GET_PHOTO_TYPE, mActionType);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            finish();
        }
        return super.dispatchTouchEvent(ev);
    }

    private void startCameraActivity() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, createPhotoUri());
        startActivityForResult(Intent.createChooser(intent, getString(R.string.avatar_picker_choose_camera_app)), REQUEST_CODE_CAMERA);
    }

    private void startCameraNoCropActivity() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, createPhotoUri());
        startActivityForResult(Intent.createChooser(intent, getString(R.string.avatar_picker_choose_camera_app)), REQUEST_CODE_CAMERA_NO_CROP);
    }

    private void startGalleryActivity() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.avatar_picker_choose_gallery_app)), REQUEST_CODE_GALLERY);
    }

    private Uri createPhotoUri() {
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(PhotoActivity.this,
                    BuildConfig.APPLICATION_ID + ".file_provider",
                    new File(FILE_PATH_CAMERA));
        }
        return Uri.fromFile(new File(FILE_PATH_CAMERA));
    }

    /**
     * Start crop activity
     *
     * @param uri  Image data needed to transfer to crop activity
     * @param code Request code will be used to retry in case we need to retake the original image
     */
    private void startCropActivity(Uri uri, int code) {
        Intent intent = new Intent(getApplicationContext(), PhotoCroppingActivity.class);
        intent.putExtra(RETAKE_CODE, code);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_CODE_CROP);
    }

    /**
     * Start no crop activity
     *
     * @param code Request code will be used to retry in case we need to retake the original image
     */
    private void startNoCropActivity(Uri uri, int code) {
        Intent intent = new Intent(getApplicationContext(), PhotoCroppingActivity.class);
        intent.putExtra(RETAKE_CODE, code);
        intent.putExtra(CROP, false);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_CODE_CAMERA_NO_CROP);
    }

    /**
     * Clear image file after crop image is success or cancel crop image
     */
    private void clearImageFile() {
        StorageUtil.clearFile(FILE_PATH_CAMERA)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    Timber.d("clear camera image is success!");
                });
    }

    private void setReturnResult(Uri uriCropImage) {
        Intent photoIntent = getIntent();
        photoIntent.setData(uriCropImage);
        setResult(RESULT_OK, photoIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("requestCode = " + requestCode + ", resultCode = " + resultCode);
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = createPhotoUri();
                    startCropActivity(selectedImage, REQUEST_CODE_CAMERA);
                } else {
                    finish();
                }
                break;
            case REQUEST_CODE_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    startCropActivity(selectedImage, REQUEST_CODE_GALLERY);
                } else {
                    finish();
                }
                break;
            case REQUEST_CODE_CROP:
                switch (resultCode) {
                    case RESULT_OK:
                        clearImageFile();
                        Uri croppedPhoto = data.getData();
                        setReturnResult(croppedPhoto);
                        finish();
                        break;
                    case PhotoCroppingActivity.RESULT_RETAKE:
                        // Retake the photo
                        switch (data.getIntExtra(RETAKE_CODE, -1)) {
                            case REQUEST_CODE_CAMERA:
                                startCameraActivity();
                                break;
                            case REQUEST_CODE_GALLERY:
                                startGalleryActivity();
                                break;
                            default:
                                break;
                        }
                        break;
                    case PhotoCroppingActivity.RESULT_FAILURE_IO:
                        Timber.w("IOException so cannot save image");
                    default:
                        clearImageFile();
                        finish();
                        break;
                }
                break;
            case REQUEST_CODE_CAMERA_NO_CROP:
                switch (resultCode) {
                    case RESULT_OK:
                        Uri selectedImage = createPhotoUri();
                        startNoCropActivity(selectedImage, REQUEST_CODE_CAMERA_NO_CROP);
                        finish();
                        break;
                    case PhotoCroppingActivity.RESULT_RETAKE:
                        // Retake the photo
                        switch (data.getIntExtra(RETAKE_CODE, -1)) {
                            case REQUEST_CODE_CAMERA_NO_CROP:
                                startCameraNoCropActivity();
                                break;
                            default:
                                break;
                        }
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionsManager.getInstance().notifyPermissionsChange(permissions, grantResults);
    }
}
