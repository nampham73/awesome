package com.genkimiru.app.presentation.register.nickname_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragmentContract.NicknameRouter;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragmentContract.NicknamePresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;
import javax.inject.Inject;
import butterknife.BindView;
import timber.log.Timber;

public class NicknameFragment extends SimpleBaseFragment<NicknameFragmentContract.NicknamePresenter>
        implements NicknameFragmentContract.NicknameView, NicknameRouter {

    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_nickname_edt)
    EditText mNicknameEdt;

    @Inject
    NicknameFragmentContract.NicknamePresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_nickname;
    }

    @Override
    protected NicknamePresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRegisterUserRequest.nick_name = mNicknameEdt.getText().toString();
    }

    private void initializeEventHandler() {
        RxTextView.textChanges(mNicknameEdt)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(nickname -> {
                    Timber.d("nickname: %s", nickname.toString());
                    if (nickname.toString().trim().length() <= 0) {
                        return;
                    }
                    mRegisterUserRequest.nick_name = nickname.toString().trim();
                });
    }
}
