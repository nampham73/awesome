package com.genkimiru.app.presentation.register.heart_beat_fragment;

import com.genkimiru.app.base.scope.FragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HeartBeatFragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = HeartBeatFragmentModule.class)
    abstract HeartBeatFragment heartBeatFragment();
}
