package com.genkimiru.app.presentation.update_user_infor.birthday;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.dialog.DatePickerDialog;

import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Api.MIN_USER_AGE;
import static com.genkimiru.app.base.util.DateUtil.Format.API_YEAR_MONTH_DAY_FORMAT;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayPresenter;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayRouter;
import static com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityContract.UpdateBirthdayView;

public class UpdateBirthDayActivity extends SimpleBaseActivity<UpdateBirthdayPresenter> implements UpdateBirthdayRouter, UpdateBirthdayView {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_birthday_tv)
    EditText mDateEdt;
    DatePickerDialog mDatePickerDialog;

    private Locale mCurrentLocale;
    private MenuItem mApplyMenuItem;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateBirthdayPresenter mPresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
        mCurrentLocale = getResources().getConfiguration().locale;

    }

    @Override
    protected void onResume() {
        super.onResume();
        initDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_birthday;
    }

    @Override
    protected UpdateBirthdayPresenter registerPresenter() {
        return mPresenter;
    }


    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }
    private void setEnabledMenuApply(boolean isEnabled) {
        mApplyMenuItem.setEnabled(isEnabled);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
            mPresenter.updateBirthday(mUserInfoRequest.getBirthday());
            mGenkiUserInfoModel.setBirthday(mUserInfoRequest.getBirthday());
            showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.register_birthday_tv)
    void updateBirthday() {
        showDateDialog();
    }

    public void showDateDialog() {
        Timber.d("showDateDialog");
        setEnabledMenuApply(true);
        mDatePickerDialog
                = new DatePickerDialog((year, month, day) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            DateUtil.getYearMonthDayForApi(calendar);
            mDateEdt.setText(DateUtil.getYearMonthDayString(calendar, mCurrentLocale));
            mDatePickerDialog.dismiss();
            if (mDateEdt.getText().toString().trim().length() <= 0) {
                return;
            }
            mUserInfoRequest.setBirthday(DateUtil.getYearMonthDayForApi(calendar));
        });

        Calendar calendar = DateUtil.convertDateTimeToCalendar(mGenkiUserInfoModel.getBirthday(), API_YEAR_MONTH_DAY_FORMAT);
        if (calendar != null) {
            mDatePickerDialog.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        }
        mDatePickerDialog.show(getSupportFragmentManager());
    }

    private void initDefaultValue() {
        if (StringUtil.isNotEmpty(mGenkiUserInfoModel.getBirthday())) {
            Calendar calendar = DateUtil.getCalendar(mGenkiUserInfoModel.getBirthday(), API_YEAR_MONTH_DAY_FORMAT);
            mDateEdt.setText(DateUtil.getYearMonthDayString(calendar, mCurrentLocale));
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - MIN_USER_AGE);
            mGenkiUserInfoModel.setBirthday(DateUtil.getYearMonthDayForApi(calendar));
            mDateEdt.setText(DateUtil.getYearMonthDayString(calendar, mCurrentLocale));
        }
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();
    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());
    }
}
