package com.genkimiru.app.presentation.update_user_infor.blood_pressure;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.genkimiru.app.R;
import com.genkimiru.app.base.activity.SimpleBaseActivity;
import com.genkimiru.app.base.util.MessageUtil;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

import static com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityContract.UpdateBloodPressurePresenter;
import static com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityContract.UpdateBloodPressureRouter;
import static com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityContract.UpdateBloodPressureView;

public class UpdateBloodPressureActivity extends SimpleBaseActivity<UpdateBloodPressurePresenter>
        implements UpdateBloodPressureView, UpdateBloodPressureRouter {

    @BindView(R.id.linear_background)
    LinearLayout mLinearLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_update_user_infor_view)
    View mView;
    @BindView(R.id.register_blood_pressure_upper_edt)
    EditText mBloodPressureEdt;
    @BindView(R.id.register_blood_pressure_under_edt)
    EditText mBloodPressureHeartBeatEdt;

    private MenuItem mApplyMenuItem;

    @Inject
    UserInfoRequest mUserInfoRequest;
    @Inject
    UpdateBloodPressurePresenter mBloodPressurePresenter;
    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBackgroundLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeEventHandler();
        initDefaultValue();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_register_bloodpressure;
    }

    @Override
    protected UpdateBloodPressurePresenter registerPresenter() {
        return mBloodPressurePresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    protected void initializeToolbar() {
        super.initializeToolbar();
        mToolbar.setVisibility(View.VISIBLE);
        mToolBarUtils.toolbar(mToolbar).addBackButton().build();
    }

    private void setBackgroundLayout() {
        mView.setVisibility(View.VISIBLE);
        mLinearLayout.setBackgroundResource(R.drawable.bg_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update_setting, menu);
        mApplyMenuItem = menu.findItem(R.id.activity_update_apply);
        setEnabledMenuApply(false);
        return true;
    }
    private void setEnabledMenuApply(boolean isEnabled) {
        if (mApplyMenuItem==null)return;
        mApplyMenuItem.setEnabled(isEnabled);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.activity_update_apply) {
        mBloodPressurePresenter.updateBloodPressure(mUserInfoRequest.getBlood_pressure_upper(), mUserInfoRequest.getBlood_pressure_under());
        showProgress();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initializeEventHandler() {
        RxTextView.textChanges(mBloodPressureEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(bloodPressure -> {
                    Timber.d("blood pressure: %s", bloodPressure.toString());
                    if (StringUtil.isEmpty(bloodPressure)) {
                        setEnabledMenuApply(false);
                        return;
                    }
                    setEnabledMenuApply(true);
                    bloodPressure = mBloodPressurePresenter.validateBloodPressure(bloodPressure.toString().trim());
                    mBloodPressureEdt.setText(bloodPressure);
                    mBloodPressureEdt.setSelection(mBloodPressureEdt.getText().length());
                    getBloodPressure();
                });
        RxTextView.textChanges(mBloodPressureHeartBeatEdt)
                .skip(1)
                .distinctUntilChanged(CharSequence::toString)
                .subscribe(heartBeat -> {
                    Timber.d("blood pressure heart beat: %s", heartBeat.toString());
                    if (StringUtil.isEmpty(heartBeat)) {
                       setEnabledMenuApply(false);
                        return;
                    }
                    setEnabledMenuApply(true);
                    heartBeat = mBloodPressurePresenter.validateBloodPressure(heartBeat.toString().trim());
                    mBloodPressureHeartBeatEdt.setText(heartBeat);
                    mBloodPressureHeartBeatEdt.setSelection(mBloodPressureHeartBeatEdt.getText().length());
                    getBloodPressure();
                });
    }

    private void getBloodPressure() {
        if (mBloodPressureEdt.getText().toString().length() <= 0
                || mBloodPressureHeartBeatEdt.getText().toString().length() <= 0) {
            return;
        }
        mUserInfoRequest.setBlood_pressure_upper(Integer.valueOf(mBloodPressureEdt.getText().toString().trim()));
        mUserInfoRequest.setBlood_pressure_under(Integer.valueOf(mBloodPressureHeartBeatEdt.getText().toString().trim()));
    }

    private void initDefaultValue(){
        mBloodPressureEdt.setText(String.valueOf(mGenkiUserInfoModel.getBloodPressureUpper()));
        mBloodPressureHeartBeatEdt.setText(String.valueOf(mGenkiUserInfoModel.getBloodPressureUnder()));
    }

    @Override
    public void onUpdateSuccess() {
        hideProgress();
        finish();

    }

    @Override
    public void onUpdateError(String message) {
        hideProgress();
        MessageUtil.createMessageDialog(message).show(getSupportFragmentManager());

    }
}
