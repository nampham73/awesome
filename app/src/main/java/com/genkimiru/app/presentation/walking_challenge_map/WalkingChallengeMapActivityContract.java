package com.genkimiru.app.presentation.walking_challenge_map;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public class WalkingChallengeMapActivityContract {

    interface WalkingChallengeMapPresenter extends BasePresenter {

    }

    interface WalkingChallengeMapView {

    }

    interface WalkingChallengeMapRouter {

    }
}
