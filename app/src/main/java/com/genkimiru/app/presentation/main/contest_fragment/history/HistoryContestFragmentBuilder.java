package com.genkimiru.app.presentation.main.contest_fragment.history;

import com.genkimiru.app.base.scope.ChildFragmentScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HistoryContestFragmentBuilder {

    @ChildFragmentScope
    @ContributesAndroidInjector(modules = HistoryContestFragmentModule.class)
    abstract HistoryContestFragment historyContestFragment();
}
