package com.genkimiru.app.presentation.main.setting_fragment;

import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.genkimiru.app.Constants;
import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.TextDialogFragment;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.presentation.bug_report.BugReportActivity;
import com.genkimiru.app.presentation.change_password.ChangePasswordActivity;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivity;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivity;
import com.genkimiru.app.presentation.eula.EulaActivity;
import com.genkimiru.app.presentation.faq.FaqActivity;
import com.genkimiru.app.presentation.intro.IntroActivity;
import com.genkimiru.app.presentation.main.GlideApp;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingPresenter;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentContract.SettingView;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivity;
import com.genkimiru.app.presentation.setting_notification.NotificationSettingActivity;
import com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Argument.ARG_HEALTH_SOURCE;
import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class SettingFragment extends SimpleBaseFragment<SettingPresenter>
        implements SettingView, SettingFragmentContract.SettingRouter {

    @BindView(R.id.setting_user_name_tv)
    TextView mUsernameTv;
    @BindView(R.id.setting_user_mail_tv)
    TextView mUserMailTv;
    @BindView(R.id.setting_avatar_iv)
    ImageView mAvatarIv;

    @Inject
    GenkiUserInfoModel mGenkiUserInfoModel;
    @Inject
    SettingPresenter mSettingPresenter;

    TextDialogFragment mLogoutDialogFragment;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_setting;
    }

    @Override
    protected SettingPresenter registerPresenter() {
        return mSettingPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    public static SettingFragment newInstance() {
        return new SettingFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialize();
    }

    @Override
    public void openEditProfileScreen() {
        Timber.d("openEditProfileScreen");
        Intent intent = new Intent(getContext(), EditProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void openChangePasswordScreen() {
        Timber.d("openChangePasswordScreen");
        Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void openPrivacySettingScreen() {
        Timber.d("openPrivacySettingScreen");
        Intent intent = new Intent(getActivity(), SettingPrivacyActivity.class);
        startActivity(intent);
    }

    @Override
    public void openNotificationSettingScreen() {
        Timber.d("openNotificationSettingScreen");
        Intent intent = new Intent(getContext(), NotificationSettingActivity.class);
        startActivity(intent);
    }

    @Override
    public void openSettingEmailMarketingScreen() {
        Timber.d("openSettingEmailMarketingScreen");
        Intent intent = new Intent(getActivity(), SettingEmailMarketingActivity.class);
        startActivity(intent);
    }

    @Override
    public void openTermOfUseScreen() {
        Timber.d("openTermOfUseScreen");
        Intent intent = new Intent(getActivity(), EulaActivity.class);
        startActivity(intent);
    }

    @Override
    public void openHealthSourceSetting() {
        Timber.d("openHealthSourceSetting");
        Intent intent = new Intent(getActivity(), RegisterActivity.class);
        intent.putExtra(ARG_HEALTH_SOURCE, true);
        startActivity(intent);
    }

    @Override
    public void openReportBugScreen() {
        Intent intent = new Intent(getContext(), BugReportActivity.class);
        startActivity(intent);
    }

    @Override
    public void openFaqScreen() {
        Timber.d("openFaqScreen");
        Intent intent = new Intent(getContext(), FaqActivity.class);
        startActivity(intent);
    }

    @Override
    public void openChatWithWatson() {
        Timber.d("openChatWithWatsonScreen");
        Intent intent = new Intent(getContext(), ChatWithWatsonActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.setting_edit_profile_layout)
    void onEditProfileClick() {
        openEditProfileScreen();
    }

    @OnClick(R.id.setting_change_password_tv)
    void onChangePasswordClick() {
        openChangePasswordScreen();
    }

    @OnClick(R.id.setting_privacy_tv)
    void onSettingPrivacyClick() {
        openPrivacySettingScreen();
    }

    @OnClick(R.id.setting_term_of_use_tv)
    void onSettingToUClick() {
        openTermOfUseScreen();
    }

    @OnClick(R.id.setting_logout_tv)
    void onLogoutClick() {
        mLogoutDialogFragment = new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .title(getString(R.string.setting_logout))
                .message(getString(R.string.setting_logout_warning))
                .leftButtonText(getString(android.R.string.cancel))
                .rightButtonText(getString(android.R.string.yes))
                .onResultListener(new TextDialogFragment.OnResultListener() {

                    @Override
                    public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                        dismissDialog();
                    }

                    @Override
                    public void onClickRightButton(TextDialogFragment textDialogFragment) {
                        GenkiApplication.getPreferences().getString(USER_TOKEN).delete();
                        LoginManager.getInstance().logOut();
                        mGenkiUserInfoModel.setHealthSource(Constants.HealthSource.UNKNOWN);
                        Intent intent = new Intent(getContext(), IntroActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }).build();
        mLogoutDialogFragment.show(getActivity().getSupportFragmentManager());
    }

    @OnClick(R.id.activity_setting_email_marketting)
    void onSettingEmailMarketing() {
        openSettingEmailMarketingScreen();
    }

    @OnClick(R.id.setting_push_notification_tv)
    void onSettingNotificationClick() {
        openNotificationSettingScreen();
    }

    @OnClick(R.id.setting_health_source_tv)
    void onHealthSourceClick() {
        openHealthSourceSetting();
    }

    @OnClick(R.id.setting_report_bug_tv)
    void onReportBugClick() {
        openReportBugScreen();
    }

    @OnClick(R.id.setting_faq_tv)
    void onFaqClick() {
        openFaqScreen();
    }

    @OnClick(R.id.setting_chat_with_watson_tv)
    void onChatWithWatson() {
        openChatWithWatson();
    }

    private void dismissDialog() {
        if (mLogoutDialogFragment == null) {
            return;
        }
        mLogoutDialogFragment.dismiss();
    }

    private void initialize() {
        if (StringUtil.isNotEmpty(mGenkiUserInfoModel.getNickName())) {
            mUsernameTv.setText(mGenkiUserInfoModel.getNickName());
        } else {
            mUsernameTv.setText(mGenkiUserInfoModel.getFirstName() + " " + mGenkiUserInfoModel.getLastName());
        }

        mUserMailTv.setText(mGenkiUserInfoModel.getEmail());

        GlideApp.with(getActivity().getApplicationContext())
                .load(mGenkiUserInfoModel.getAvatar())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(mAvatarIv);
    }
}
