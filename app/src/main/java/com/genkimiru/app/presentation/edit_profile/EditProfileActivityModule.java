package com.genkimiru.app.presentation.edit_profile;

import android.content.Context;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.data.model.GenkiUserInfoModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.repository.user.UserRepository;
import com.genkimiru.app.data.repository.user.UserRepositoryImp;
import com.genkimiru.app.domain.user.UserUseCase;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityContract.EditProfileRouter;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityContract.EditProfileView;

import dagger.Module;
import dagger.Provides;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

@Module
public class EditProfileActivityModule {

    @Provides
    @ActivityScope
    EditProfileRouter provideRouter(EditProfileActivity editProfileActivity) {
        return editProfileActivity;
    }

    @Provides
    @ActivityScope
    EditProfileView provideView(EditProfileActivity editProfileActivity) {
        return editProfileActivity;
    }

    @Provides
    @ActivityScope
    EditProfileActivityPresenter providePresenter(Context context, EditProfileView editProfileView, EditProfileRouter editProfileRouter,UserUseCase userUseCase,GenkiUserInfoModel genkiUserInfoModel) {
        return new EditProfileActivityPresenter(context, editProfileView, editProfileRouter,userUseCase,genkiUserInfoModel);
    }

    @Provides
    @ActivityScope
    UserRepository provideUserRepository(GenkiApi api) {
        return new UserRepositoryImp(api);
    }

    @Provides
    @ActivityScope
    UserInfoRequest provideUserInforRequest(GenkiUserInfoModel genkiUserInfoModel) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return new UserInfoRequest(token, genkiUserInfoModel.getHealthSource());
    }

    @Provides
    @ActivityScope
    UserUseCase providesUserUseCase(SchedulerProvider schedulerProvider, UserRepository userRepository) {
        return new UserUseCase(schedulerProvider.io(), schedulerProvider.ui(), userRepository);
    }
}
