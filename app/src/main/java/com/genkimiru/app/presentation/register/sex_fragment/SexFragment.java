package com.genkimiru.app.presentation.register.sex_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RadioButton;

import com.genkimiru.app.R;
import com.genkimiru.app.base.fragment.SimpleBaseFragment;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexFragmentRouter;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexFragmentView;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentContract.SexPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class SexFragment extends SimpleBaseFragment<SexPresenter> implements SexFragmentView, SexFragmentRouter {

    private RegisterActivity mRegisterActivity;

    @BindView(R.id.register_sex_male_rb)
    RadioButton mMaleRadioButton;
    @BindView(R.id.register_sex_female_rb)
    RadioButton mFemaleRadioButton;

    @Inject
    SexPresenter mPresenter;
    @Inject
    RegisterUserRequest mRegisterUserRequest;

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_register_sex;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mRegisterActivity = (RegisterActivity) getActivity();
    }

    @Override
    protected SexPresenter registerPresenter() {
        return mPresenter;
    }

    @Override
    protected boolean isNeedInjection() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Timber.d("RegisterUserRequest %s", mRegisterUserRequest.birthday);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            return;
        }
        initialDefaultValue();
    }

    @OnClick({R.id.register_sex_male_rb, R.id.register_sex_female_rb})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {
            case R.id.register_sex_male_rb:
                if (checked) {
                    mRegisterUserRequest.gender = 1;
                    Timber.d("RegisterUserRequest: male");
                }
                break;
            case R.id.register_sex_female_rb:
                if (checked) {
                    mRegisterUserRequest.gender = 2;
                    Timber.d("RegisterUserRequest: female");
                }
                break;
        }
    }

    private void initialDefaultValue() {
        int gender = mRegisterUserRequest.gender;
        switch (gender) {
            case 1:
                mMaleRadioButton.setChecked(true);
                break;
            case 2:
                mFemaleRadioButton.setChecked(true);
                break;
            default:
                mMaleRadioButton.setChecked(true);
                mRegisterUserRequest.gender = 1;
                break;
        }
    }
}
