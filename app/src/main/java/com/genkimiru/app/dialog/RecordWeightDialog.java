package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class RecordWeightDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_record_weight_edt)
    EditText mWeightEdt;
    @BindView(R.id.dialog_record_weight_time_edt)
    EditText mTimeEdt;

    public RecordWeightDialog() {
    }

    private OnResultListener mListener;

    @SuppressLint("ValidFragment")
    public RecordWeightDialog(OnResultListener mListener) {
        this.mListener = mListener;
    }

    public static class RecordWeightDialogBuilder{
        private OnResultListener onResultListener;

        public RecordWeightDialogBuilder dialogListener(OnResultListener onResultListener) {
            this.onResultListener = onResultListener;
            return this;
        }

        public RecordWeightDialog build() {
            return new RecordWeightDialog(onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_record_weight;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onCancel(RecordWeightDialog recordWeightDialog);
        void onAdd(RecordWeightDialog recordWeightDialog, String weight, String time);
    }

    @OnClick(R.id.dialog_record_weight_add_bt)
    void onAdd() {
        mListener.onAdd(this, mWeightEdt.getText().toString(), mTimeEdt.getText().toString());
    }
    @OnClick(R.id.dialog_record_weight_cancel_bt)
    void onCancel() {
        mListener.onCancel(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if(window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.x = (int)dpToPx(getContext(), 8);
        layoutParams.width = getResources().getDisplayMetrics().widthPixels - layoutParams.x * 2;
        window.setAttributes(layoutParams);
    }

    private float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

}
