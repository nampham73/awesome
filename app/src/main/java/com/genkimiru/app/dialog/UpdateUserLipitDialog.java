package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class UpdateUserLipitDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.dialog_user_lipit_neutral_fat)
    EditText mLipitNeutralFatEdt;
    @BindView(R.id.dialog_user_lipit_cholesterol_hd_edt)
    EditText mLipitCholesterolHdEdt;
    @BindView(R.id.dialog_user_lipit_cholesterol_ld)
    EditText mLipitCholesterolLdEdt;
    @BindView(R.id.dialog_checkbox_update_cancel_bt)
    Button mCheckUpdateCancel;
    @BindView(R.id.dialog_checkbox_update_confirm_bt)
    Button mCheckUpdateConfirm;

    private UpdateUserLipitDialog.OnResultListener mListener;

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_update_user_info_lipit;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }



    public UpdateUserLipitDialog() {
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 16);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @SuppressLint("ValidFragment")
    public UpdateUserLipitDialog(UpdateUserLipitDialog.OnResultListener listener) {
        mListener = listener;
    }

    public static class UpdateUserLipitBuilder {

        private OnResultListener onResultListener;


        public UpdateUserLipitDialog.UpdateUserLipitBuilder dialogListener(UpdateUserLipitDialog.OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public UpdateUserLipitDialog build() {
            return new UpdateUserLipitDialog(onResultListener);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }


    public interface OnResultListener {
        void onUpdateUserLipit(String lipitNeutralFat, String lipitCholesterolHd, String lipitCholesterolLd, UpdateUserLipitDialog updateUserLipitDialog);

        void onCancel(UpdateUserLipitDialog loginDialog);
    }

    @OnClick(R.id.dialog_checkbox_update_cancel_bt)
    void onUpdateUserLipit() {
        Timber.d("onLogin: %s - %s -%s", mLipitNeutralFatEdt.getText().toString(), mLipitCholesterolHdEdt.getText().toString(), mLipitCholesterolLdEdt.getText().toString());
        mListener.onUpdateUserLipit(mLipitNeutralFatEdt.getText().toString(), mLipitCholesterolHdEdt.getText().toString(), mLipitCholesterolLdEdt.getText().toString(), this);
    }

    @OnClick(R.id.dialog_checkbox_update_confirm_bt)
    void onCancel() {
        mListener.onCancel(this);
    }
}
