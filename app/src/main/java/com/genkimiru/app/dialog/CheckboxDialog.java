package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class CheckboxDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.register_checkbox_dialog_content)
    FrameLayout mContentDialog;
    @BindView(R.id.dialog_checkbox_title)
    TextView mTitleDialog;

    private RadioGroup mRadioGroup;
    private Button mConfirmBtn;

    private String mTitle;
    private int mLayoutId;
    private DialogListener mDialogListener;

    public CheckboxDialog() {
    }

    @SuppressLint("ValidFragment")
    public CheckboxDialog(DialogListener dialogListener) {
        mDialogListener = dialogListener;
    }

    @SuppressLint("ValidFragment")
    public CheckboxDialog(String mTitle, int mLayoutId, DialogListener dialogListener) {
        this.mTitle = mTitle;
        this.mLayoutId = mLayoutId;
        mDialogListener = dialogListener;
    }

    public static class CheckboxDialogBuilder {
        private String title;
        private int layoutId;
        private DialogListener mDialogListener;

        public CheckboxDialogBuilder setTitle(String mTitle) {
            this.title = mTitle;
            return this;
        }

        public CheckboxDialogBuilder setLayout(int mLayoutId) {
            this.layoutId = mLayoutId;
            return this;
        }

        public CheckboxDialogBuilder dialogListener(DialogListener dialogListener) {
            mDialogListener = dialogListener;
            return this;
        }

        public CheckboxDialog build() {
            if (title != null) {
                return new CheckboxDialog(title, layoutId, mDialogListener);
            }
            return new CheckboxDialog(title, layoutId, mDialogListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        setLayoutContent(mLayoutId);
        mTitleDialog.setText(mTitle);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_checkbox;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mDialogListener.onDismiss();
    }

    private void setLayoutContent(int layoutId) {
        View view = getLayoutInflater().inflate(layoutId, mContentDialog);
        mRadioGroup = view.findViewById(R.id.checkbox_dialog_radio_group);
        mConfirmBtn = view.findViewById(R.id.register_dialog_checkbox_ok_bt);
        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> mDialogListener.onCheckedChanged(group, checkedId));
        mDialogListener.onOpen(mRadioGroup.getCheckedRadioButtonId());
    }

    @OnClick(R.id.register_dialog_checkbox_ok_bt)
    public void onConfirm() {
        Timber.d("onConfirm");
        mDialogListener.onComplete(this);
    }

    public interface DialogListener {
        void onComplete(CheckboxDialog checkboxDialog);

        void onCheckedChanged(RadioGroup group, int checkedId);

        void onOpen(int checkedId);

        void onDismiss();
    }
}
