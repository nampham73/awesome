package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class InputWaterDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_input_water_level_edt)
    EditText mLevelEdt;

    private OnResultListener mListener;

    @SuppressLint("ValidFragment")
    public InputWaterDialog(OnResultListener mListener) {
        this.mListener = mListener;
    }

    public InputWaterDialog() {
    }

    public static class InputWaterDialogBuilder{
        private OnResultListener onResultListener;

        public InputWaterDialogBuilder dialogListener(OnResultListener onResultListener) {
            this.onResultListener = onResultListener;
            return this;
        }

        public InputWaterDialog buider() {
            return new InputWaterDialog(onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window==null)return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 8);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_input_water;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }
    public interface OnResultListener {
        void onAdd(View view, InputWaterDialog inputWaterDialog);
        void onCancel(InputWaterDialog inputWaterDialog);
    }

    @OnClick(R.id.dialog_input_water_add_bt)
    void onAdd(){
        mListener.onAdd(mLevelEdt, this);
    }

    @OnClick(R.id.dialog_input_water_cancel_bt)
    void onCancel() {
        mListener.onCancel(this);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
