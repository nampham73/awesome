package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class UpdateUserSugarDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_user_sugar_edt)
    EditText mSugar;
    @BindView(R.id.dialog_checkbox_update_cancel_bt)
    Button mCheckUpdateCancel;
    @BindView(R.id.dialog_checkbox_update_update_bt)
    Button mCheckUpdateConfirm;
    @BindView(R.id.dialog_checkbox_update_delete_bt)
    Button mCheckUpdateDelete;
    @BindView(R.id.update_sugar_title_tv)
    TextView mTitleDialog;
    @BindView(R.id.update_sugar_measure)
    TextView mMeasureSugar;

    private String mTitle;
    private String mMeasure;
    private OnResultListener mListener;

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_update_user_sugar;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public UpdateUserSugarDialog() {
    }


    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 16);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @SuppressLint("ValidFragment")
    public UpdateUserSugarDialog(String title, String measure, OnResultListener listener) {
        mTitle = title;
        mMeasure = measure;
        mListener = listener;
    }

    public static class UpdateUserSugarDialogBuilder {
        private String title;
        private String measure;
        private OnResultListener onResultListener;

        public UpdateUserSugarDialogBuilder setTitle(String mTitle, String measure) {
            this.title = mTitle;
            this.measure = measure;
            return this;
        }


        public UpdateUserSugarDialogBuilder dialogListener(OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public UpdateUserSugarDialog build() {
            if (title != null && measure != null) {
                return new UpdateUserSugarDialog(title, measure, onResultListener);
            }
            return new UpdateUserSugarDialog(title, measure, onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
        mTitleDialog.setText(mTitle);
        mMeasureSugar.setText(mMeasure);
    }


    public interface OnResultListener {
        void onUpdateUserSugar(String sugar, UpdateUserSugarDialog updateUserSugarDialog);

        void onCancel(UpdateUserSugarDialog updateUserSugarDialog);

        void onDelete(UpdateUserSugarDialog updateUserSugarDialog);

    }

    @OnClick(R.id.dialog_checkbox_update_update_bt)
    void onUpdateUserLiver() {
        Timber.d("onLogin: %s", mSugar.getText().toString().trim());
        mListener.onUpdateUserSugar(mSugar.getText().toString().trim(), this);
    }

    @OnClick(R.id.dialog_checkbox_update_cancel_bt)
    void onCancel() {
        mListener.onCancel(this);
    }

    @OnClick(R.id.dialog_checkbox_update_delete_bt)
    void onDelete() {
        mListener.onDelete(this);
    }

}
