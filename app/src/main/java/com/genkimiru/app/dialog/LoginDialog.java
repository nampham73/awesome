package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class LoginDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.dialog_login_username_edt)
    EditText mUsernameEdt;
    @BindView(R.id.dialog_login_password_edt)
    EditText mPasswordEdt;
    @BindView(R.id.register_dialog_checkbox_login_title_tv)
    TextView mTitleDialog;

    private String mTitle;
    private OnResultListener mListener;

    public LoginDialog() {
    }

    @SuppressLint("ValidFragment")
    public LoginDialog(String title, OnResultListener listener) {
        mTitle = title;
        mListener = listener;
    }

    public static class LoginDialogBuilder {
        private String title;

        private OnResultListener onResultListener;

        public LoginDialogBuilder setTitle(String mTitle) {
            this.title = mTitle;
            return this;
        }


        public LoginDialogBuilder dialogListener(OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public LoginDialog build() {
            if (title != null) {
                return new LoginDialog(title, onResultListener);
            }
            return new LoginDialog(title, onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
        mTitleDialog.setText(mTitle);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_login;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onLogin(String username, String password, LoginDialog loginDialog);

        void onCancel(LoginDialog loginDialog);
    }

    @OnClick(R.id.register_dialog_checkbox_login_ok_bt)
    void onLogin() {
        Timber.d("onLogin: %s - %s", mUsernameEdt.getText().toString(), mPasswordEdt.getText().toString());
        mListener.onLogin(mUsernameEdt.getText().toString(), mPasswordEdt.getText().toString(), this);
    }

    @OnClick(R.id.register_dialog_checkbox_login_cancel_bt)
    void onCancel() {
        mListener.onCancel(this);
    }
}
