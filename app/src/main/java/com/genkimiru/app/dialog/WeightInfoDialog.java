package com.genkimiru.app.dialog;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public class WeightInfoDialog extends SimpleBaseDialogFragment {
    public WeightInfoDialog() {
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_weight_info;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if(window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int)dpToPx(getContext(), 8);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
    }

    private float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
