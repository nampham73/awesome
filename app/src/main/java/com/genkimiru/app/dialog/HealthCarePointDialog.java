package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class HealthCarePointDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.dialog_health_care_number_point)
    TextView mHealthCareNumberPoint;
    @BindView(R.id.dialog_cost_reduce_effect)
    TextView mHealthCareCostReduceEffect;

    private String mNumberPoint;
    private String mCostReduceEffect;

    private OnResultListener mListener;


    public HealthCarePointDialog() {

    }

    @SuppressLint("ValidFragment")
    public HealthCarePointDialog(String numberPoint, String costReduceEffect, OnResultListener listener) {
        mNumberPoint = numberPoint;
        mCostReduceEffect = costReduceEffect;
        mListener = listener;
    }


    public static class HealthCarePointDialogBuilder {

        private String numberPoint;
        private String costReduceEffect;
        private OnResultListener onResultListener;

        public HealthCarePointDialogBuilder setHealthCarePoint(String numberPoint, String costReduceEffect) {
            this.numberPoint = numberPoint;
            this.costReduceEffect = costReduceEffect;
            return this;
        }

        public HealthCarePointDialogBuilder dialogListener(OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public HealthCarePointDialog build() {
            if (numberPoint != null && costReduceEffect != null) {
                return new HealthCarePointDialog(numberPoint, costReduceEffect, onResultListener);
            }
            return new HealthCarePointDialog(numberPoint, costReduceEffect, onResultListener);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
        mHealthCareNumberPoint.setText(mNumberPoint);
        mHealthCareCostReduceEffect.setText(mCostReduceEffect);
    }

    public interface OnResultListener {
        void onHealthCarePoint(HealthCarePointDialog healthCarePointDialog);
    }

    @OnClick(R.id.dialog_health_care_ok_bt)
    void onHealthCarePoint() {
        mListener.onHealthCarePoint(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_health_care_point;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }


    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 35);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
