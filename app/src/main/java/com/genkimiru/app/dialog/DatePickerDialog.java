package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import java.util.Calendar;
import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Api.MIN_USER_AGE;

public class DatePickerDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.date_picker)
    DatePicker mDatePicker;

    private Integer mDay;
    private Integer mMonth;
    private Integer mYear;

    private OnResultListener mListener;

    public DatePickerDialog() {
    }

    @SuppressLint("ValidFragment")
    public DatePickerDialog(OnResultListener listener) {
        mListener = listener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        setMaxUserAge();
        if (mDay != null && mMonth != null && mYear != null) {
            mDatePicker.updateDate(mYear, mMonth, mDay);
        }
        mDatePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
    }

    public void setDate(int year, int month, int day) {
        mYear = year;
        mMonth = month;
        mDay = day;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_date_picker;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @OnClick(R.id.date_picker_ok)
    public void onConfirm() {
        Timber.d("onConfirm: %s - %s - %s", mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
        mListener.onConfirm(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
    }

    public interface OnResultListener {
        void onConfirm(int year, int month, int day);
    }

    private void setMaxUserAge() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - MIN_USER_AGE);
        mDatePicker.setMaxDate(calendar.getTimeInMillis());
    }
}
