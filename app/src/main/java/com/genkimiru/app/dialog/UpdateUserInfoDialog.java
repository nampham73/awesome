package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.StringUtil;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class UpdateUserInfoDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_user_name_edt)
    EditText mUserNameEdt;
    @BindView(R.id.dialog_user_email_edt)
    EditText mUserEmailEdt;
    @BindView(R.id.dialog_last_name_edt)
    EditText mUserLastNameEdt;
    @BindView(R.id.dialog_first_name_edt)
    EditText mUserFirstNameEdt;
    @BindView(R.id.dialog_checkbox_update_cancel_bt)
    Button mCheckUpdateCancel;
    @BindView(R.id.dialog_checkbox_update_confirm_bt)
    Button mCheckUpdateConfirm;

    private OnResultListener mListener;
    private String mNickName;
    private String mEmail;
    private String mFirstName;
    private String mLastName;

    public UpdateUserInfoDialog() {
    }

    @SuppressLint("ValidFragment")
    public UpdateUserInfoDialog(OnResultListener listener) {
        mListener = listener;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window==null)return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 16);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    public void setInitializeData(String nickName, String email, String firstName, String lastName){
        mNickName = nickName;
        mEmail = email;
        mFirstName = firstName;
        mLastName = lastName;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_update_user_info;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public static class UpdateUserDialogBuilder {

        private OnResultListener onResultListener;

        public UpdateUserDialogBuilder dialogListener(OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public UpdateUserInfoDialog build() {
            return new UpdateUserInfoDialog(onResultListener);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mUserNameEdt.setText(StringUtil.isNotEmpty(mNickName) ? mNickName : "");
        mUserEmailEdt.setText(StringUtil.isNotEmpty(mEmail) ? mEmail : "");
        mUserFirstNameEdt.setText(StringUtil.isNotEmpty(mFirstName) ? mFirstName : "");
        mUserLastNameEdt.setText(StringUtil.isNotEmpty(mLastName) ? mLastName : "");
        mUserNameEdt.setSelection(mUserNameEdt.getText().length());
        mUserEmailEdt.setSelection(mUserEmailEdt.getText().length());
        mUserFirstNameEdt.setSelection(mUserFirstNameEdt.getText().length());
        mUserLastNameEdt.setSelection(mUserLastNameEdt.getText().length());
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    public interface OnResultListener {

        void onUpdateUserInfo(String username, String firstName, String lastName, String email, UpdateUserInfoDialog userInfoDialog);

        void onCancel(UpdateUserInfoDialog userInfoDialog);
    }

    @OnClick(R.id.dialog_checkbox_update_confirm_bt)
    void onLogin() {
        Timber.d("onLogin: %s - %s - %s -%s", mUserNameEdt.getText().toString(), mUserFirstNameEdt.getText().toString(), mUserLastNameEdt.getText().toString(), mUserEmailEdt.getText().toString());
        mListener.onUpdateUserInfo(mUserNameEdt.getText().toString(),
                mUserFirstNameEdt.getText().toString(),
                mUserLastNameEdt.getText().toString(),
                mUserEmailEdt.getText().toString(), this);
    }

    @OnClick(R.id.dialog_checkbox_update_cancel_bt)
    void onCancel() {
        mListener.onCancel(this);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
