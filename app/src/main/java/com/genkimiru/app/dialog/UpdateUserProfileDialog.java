package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;


public class UpdateUserProfileDialog extends SimpleBaseDialogFragment {


    @BindView(R.id.dialog_upadate_user_profile_nickname_edt)
    EditText mNickNameEdt;
    @BindView(R.id.dialog_upadate_user_profile_email_edt)
    EditText mEmailEdt;
    @BindView(R.id.dialog_upadate_user_profile_surname_edt)
    EditText mSurnameEdt;
    @BindView(R.id.dialog_upadate_user_profile_name_edt)
    EditText mNameEdt;

    public UpdateUserProfileDialog() {
    }

    private OnResultListener mListener;

    @SuppressLint("ValidFragment")
    public UpdateUserProfileDialog(OnResultListener mListener) {
        this.mListener = mListener;
    }

    public static class UpdateUserProfileDialogBuilder {
        private OnResultListener onResultListener;

        public UpdateUserProfileDialogBuilder dialogListener(OnResultListener dialogListener) {
            this.onResultListener = dialogListener;
            return this;
        }

        public UpdateUserProfileDialog build() {
            return new UpdateUserProfileDialog(onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_update_user_profile;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onUpdate();
        void onCancel(UpdateUserProfileDialog updateUserProfileDialog);
    }

    @OnClick(R.id.dialog_update_user_profile_update_btn)
    void onUpdate() {

    }
    @OnClick(R.id.dialog_update_user_profile_cancel_btn)
    void onCancel() {
        mListener.onCancel(this);
    }
}
