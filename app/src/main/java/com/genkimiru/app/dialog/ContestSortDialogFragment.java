package com.genkimiru.app.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.genkimiru.app.dialog.ContestSortDialogFragment.SortId.END_DATE_ID;
import static com.genkimiru.app.dialog.ContestSortDialogFragment.SortId.START_DATE_ID;

public class ContestSortDialogFragment extends SimpleBaseDialogFragment {
    @BindView(R.id.wheel_view)
    WheelView mWheelView;
    private WheelView.OnWheelViewListener mListener;
    private int indexSelected;

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_contest_sort;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public ContestSortDialogFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window==null)return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 8);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        mWheelView.setItems(prepareDataForWheelView());
        mWheelView.setSelection(indexSelected);
        mWheelView.setOnWheelViewListener(mListener);
    }

    @OnClick(R.id.sort_dialog_checkbox_login_cancel_bt)
    void onDialogCancel(){
        dismiss();
    }

    @OnClick(R.id.sort_dialog_checkbox_login_ok_bt)
    void onSubmitDialodAction(){
        mWheelView.requestSubmitItemSelected();
        dismiss();
    }

    private List<WheelView.Result> prepareDataForWheelView() {
        List<WheelView.Result> results = new ArrayList<>();
        WheelView.Result endtDate = new WheelView.Result(END_DATE_ID, getFilterName(R.string.contest_filter_end_date));
        WheelView.Result startDate = new WheelView.Result(START_DATE_ID, getFilterName(R.string.contest_filter_start_date));

        results.add(startDate);
        results.add(endtDate);
        return results;
    }

    private String getFilterName(@StringRes int res){
        return getString(res);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public void setSortListener(WheelView.OnWheelViewListener listener){
        this.mListener = listener;
    }

    public void setIndexSelected(int indexSelected) {
        this.indexSelected = indexSelected;
    }

    public @interface SortId {
        String END_DATE_ID = "end_date";
        String START_DATE_ID = "start_date";
    }
}
