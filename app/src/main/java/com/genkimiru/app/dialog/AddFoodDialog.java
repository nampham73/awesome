package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.DateUtil;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;

public class AddFoodDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_add_food_name_food_edt)
    EditText mNameFoodEdt;
    @BindView(R.id.dialog_add_food_cal_edt)
    EditText mCalEdt;
    @BindView(R.id.dialog_add_food_protein_edt)
    EditText mProteinEdt;
    @BindView(R.id.dialog_add_food_fat_edt)
    EditText mFatEdt;
    @BindView(R.id.dialog_add_food_carb_edt)
    EditText mCarbEdt;
    @BindView(R.id.dialog_add_food_time_edt)
    TextView mTimeEdt;

    private long mEatTime;

    public AddFoodDialog() {
    }

    private OnResultListener mListener;

    public static class AddFoodDialogBuilder {
        OnResultListener onResultListener;

        public AddFoodDialogBuilder dialogListener(OnResultListener onResultListener) {
            this.onResultListener = onResultListener;
            return this;
        }

        public AddFoodDialog build() {
            return new AddFoodDialog(onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @SuppressLint("ValidFragment")
    public AddFoodDialog(OnResultListener mListener) {
        this.mListener = mListener;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_add_food;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onAdd(AddFoodDialog addFoodDialog, String name, String cal, String pro, String fat, String carb, String time, long timestamp);

        void onCancel(AddFoodDialog addFoodDialog);
    }

    @OnClick(R.id.dialog_add_food_cancel_btn)
    void onCancel() {
        mListener.onCancel(this);
    }

    @OnClick(R.id.dialog_add_food_add_btn)
    void onAdd() {
        if (mNameFoodEdt.getText().toString().length() > 0
                && mCalEdt.getText().toString().length() > 0
                && mProteinEdt.getText().toString().length() > 0
                && mFatEdt.getText().toString().length() > 0
                && mCarbEdt.getText().toString().length() > 0) {
            mListener.onAdd(this, mNameFoodEdt.getText().toString(),
                    mCalEdt.getText().toString(), mProteinEdt.getText().toString(),
                    mFatEdt.getText().toString(), mCarbEdt.getText().toString(),
                    mTimeEdt.getText().toString(), mEatTime);
        }
    }

    @OnClick(R.id.dialog_add_food_time_edt)
    void openTimepickerDialog() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker = new TimePickerDialog(getContext(), AlertDialog.THEME_TRADITIONAL, (timePicker, selectedHour, selectedMinute) -> {
            Calendar calendar = DateUtil.today();
            calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
            calendar.set(Calendar.MINUTE, selectedMinute);
            mEatTime = calendar.getTimeInMillis() / 1000L;
            mTimeEdt.setText(selectedHour + ":" + selectedMinute);
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 8);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        mEatTime = mcurrentTime.getTimeInMillis() / 1000L;
        mTimeEdt.setText(hour + ":" + minute);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
