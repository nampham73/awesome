package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.StringUtil;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.presentation.nutrition.nutrition_adapters.NutritionAdapter;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;

public class EditFoodDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_edit_food_name_food_txt)
    TextView mNameFoodEdt;
    @BindView(R.id.dialog_edit_food_cal_edt)
    EditText mCalEdt;
    @BindView(R.id.dialog_edit_food_protein_edt)
    EditText mProteinEdt;
    @BindView(R.id.dialog_edit_food_fat_edt)
    EditText mFatEdt;
    @BindView(R.id.dialog_edit_food_carb_edt)
    EditText mCarbEdt;
    @BindView(R.id.dialog_edit_food_time_edt)
    TextView mTimeEdt;

    private NutritionAdapter mAdapter;

    public void setmAdapter(NutritionAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    public EditFoodDialog() {
    }


    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = getArguments();
        if(bundle != null) {
            GenkiNutritionModel.NutriManualModel manualModel = (GenkiNutritionModel.NutriManualModel) bundle.get("CLICKEDFOOD");
            mNameFoodEdt.setText(manualModel.getFood());
            mCalEdt.setText(StringUtil.formatUnnecessaryZero(manualModel.getKcal()));
            mProteinEdt.setText(StringUtil.formatUnnecessaryZero(manualModel.getProtein()));
            mFatEdt.setText(StringUtil.formatUnnecessaryZero(manualModel.getFat()));
            mCarbEdt.setText(StringUtil.formatUnnecessaryZero(manualModel.getCarb()));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(manualModel.getStart() * 1000L);
            mTimeEdt.setText(String.format("%s:%s", String.valueOf(calendar.get(Calendar.HOUR)), String.valueOf(calendar.get(Calendar.MINUTE))));
        }

    }

    private OnResultListener mListener;

    public static class EditFoodDialogBuilder {
        private OnResultListener onResultListener;
        public EditFoodDialogBuilder dialogListener(OnResultListener onResultListener) {
            this.onResultListener = onResultListener;
            return this;
        }

        public EditFoodDialog build() {
            return new EditFoodDialog(onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @SuppressLint("ValidFragment")
    public EditFoodDialog(OnResultListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window==null)return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 8);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
    }
    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_edit_food;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onUpdate(EditFoodDialog editFoodDialog, String name, String cal, String pro, String fat, String carb, String time);
        void onCancel(EditFoodDialog editFoodDialog);
        void onDelete(EditFoodDialog editFoodDialog);
    }

    @OnClick(R.id.dialog_edit_food_cancel_btn)
    void onCancel(){
        mListener.onCancel(this);
    }

    @OnClick(R.id.dialog_edit_food_update_btn)
    void onUpdate() {
        if (mCalEdt.getText().toString().length() > 0
                && mProteinEdt.getText().toString().length() > 0
                && mFatEdt.getText().toString().length() > 0
                && mCarbEdt.getText().toString().length() > 0
                && mTimeEdt.getText().toString().length() > 0) {
            mListener.onUpdate(this, mNameFoodEdt.getText().toString(), mCalEdt.getText().toString(), mProteinEdt.getText().toString(),
                    mFatEdt.getText().toString(), mCarbEdt.getText().toString(), mTimeEdt.getText().toString());
        }

    }

    @OnClick(R.id.dialog_edit_food_delete_btn)
    void onDelete() {
        mListener.onDelete(this);
    }

}
