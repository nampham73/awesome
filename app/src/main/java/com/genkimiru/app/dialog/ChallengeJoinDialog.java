package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class ChallengeJoinDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.dialog_challenge_join_password_edt)
    EditText mPasswordEdt;

    private OnResultListener mListener;

    public ChallengeJoinDialog() {
    }

    @SuppressLint("ValidFragment")
    public ChallengeJoinDialog(OnResultListener listener) {
        mListener = listener;
    }

    public static class ChallengeJoinDialogBuilder {
        private OnResultListener onResultListener;

        public ChallengeJoinDialogBuilder dialogListener(OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public ChallengeJoinDialog build() {

            return new ChallengeJoinDialog(onResultListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_challenge_join;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onOK(String username, String password, LoginDialog loginDialog);

        void onLater(ChallengeJoinDialog challengeJoinDialog);
    }

    @OnClick(R.id.dialog_challenge_join_ok_bt)
    void onOk() {

    }

    @OnClick(R.id.dialog_challenge_join_later_bt)
    void onLater() {
        mListener.onLater(this);
    }
}
