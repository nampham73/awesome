package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class RecordFoodDialog extends SimpleBaseDialogFragment {
    @BindView(R.id.dialog_record_food_protein_edt)
    EditText mProteinEdt;
    @BindView(R.id.dialog_record_food_fat_edt)
    EditText mFatEdt;
    @BindView(R.id.dialog_record_food_carb_edt)
    EditText mCarbEdt;

    private OnResultListener mListener;

    public RecordFoodDialog() {
    }

    @SuppressLint("ValidFragment")
    public RecordFoodDialog(OnResultListener mListener) {
        this.mListener = mListener;
    }

    public static class RecordFoodDialogBuider{
        private OnResultListener onResultListener;

        public RecordFoodDialogBuider dialogListener(OnResultListener onResultListener) {
            this.onResultListener = onResultListener;
            return this;
        }

        public RecordFoodDialog build() {
            return new RecordFoodDialog(onResultListener);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_fitbit_record_food;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public static interface OnResultListener {
        void onUpdate(RecordFoodDialog recordFoodDialog, EditText mProteinEdt, EditText mFatEdt, EditText mCarbEdt);
        void onCancel(RecordFoodDialog recordFoodDialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window==null)return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 8);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
    }
    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @OnClick(R.id.dialog_record_food_update_btn)
    void onUpdate() {
        mListener.onUpdate(this, mProteinEdt, mFatEdt, mCarbEdt);
    }
    @OnClick(R.id.dialog_record_food_cancel_btn)
    void onCancel() {
        mListener.onCancel(this);
    }

}
