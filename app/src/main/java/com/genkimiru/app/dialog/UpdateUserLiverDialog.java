package com.genkimiru.app.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.genkimiru.app.R;
import com.genkimiru.app.base.dialog.SimpleBaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class UpdateUserLiverDialog extends SimpleBaseDialogFragment {

    @BindView(R.id.dialog_liver_got)
    EditText mLiverGotEdt;
    @BindView(R.id.dialog_user_liver_gpt)
    EditText mLiverGptEdt;
    @BindView(R.id.dialog_liver_vgt_edt)
    EditText mLiverVgtEdt;
    @BindView(R.id.dialog_checkbox_update_cancel_bt)
    Button mCheckUpdateCancel;
    @BindView(R.id.dialog_checkbox_update_update_bt)
    Button mCheckUpdateConfirm;
    @BindView(R.id.dialog_checkbox_update_delete_bt)
    Button mCheckUpdateDelete;

    private UpdateUserLiverDialog.OnResultListener mListener;

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_update_user_liver;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public UpdateUserLiverDialog() {
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window == null) return;
        window.setGravity(Gravity.CENTER | GravityCompat.START);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = (int) dpToPx(getContext(), 16);
        params.width = getResources().getDisplayMetrics().widthPixels - params.x * 2;
        window.setAttributes(params);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @SuppressLint("ValidFragment")
    public UpdateUserLiverDialog(UpdateUserLiverDialog.OnResultListener listener) {
        mListener = listener;
    }

    public static class UpdateUserLipitBuilder {

        private OnResultListener onResultListener;


        public UpdateUserLiverDialog.UpdateUserLipitBuilder dialogListener(UpdateUserLiverDialog.OnResultListener dialogListener) {
            onResultListener = dialogListener;
            return this;
        }

        public UpdateUserLiverDialog build() {
            return new UpdateUserLiverDialog(onResultListener);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Dialog);
    }


    public interface OnResultListener {
        void onUpdateUserLiver(String LiverGotEdt, String LiverGptEdt, String LiverVgtEdt, UpdateUserLiverDialog updateUserLiverDialog);

        void onCancel(UpdateUserLiverDialog updateUserLiverDialog);

        void onDelete(UpdateUserLiverDialog updateUserLiverDialog);

    }
   @OnClick(R.id.dialog_checkbox_update_update_bt)
    void onUpdateUserLiver() {
        Timber.d("onLogin: %s - %s -%s", mLiverGotEdt.getText().toString(), mLiverGptEdt.getText().toString(), mLiverVgtEdt.getText().toString());
        mListener.onUpdateUserLiver(mLiverGotEdt.getText().toString(), mLiverGptEdt.getText().toString(), mLiverVgtEdt.getText().toString(), this);
    }

    @OnClick(R.id.dialog_checkbox_update_cancel_bt)
    void onCancel() {
        mListener.onCancel(this);
    }

    @OnClick(R.id.dialog_checkbox_update_delete_bt)
    void onDelete() {
        mListener.onDelete(this);
    }

}
