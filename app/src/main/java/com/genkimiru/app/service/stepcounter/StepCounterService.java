package com.genkimiru.app.service.stepcounter;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.interactor.DefaultSubscriber;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.data.model.GenkiStepCounterModel;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.domain.user.UserParam;
import com.genkimiru.app.domain.user.UserUseCase;

import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Preference.PHONE_LAST_STEP_COUNTER_PUSH;
import static com.genkimiru.app.Constants.Preference.PHONE_LAST_STEP_COUNTER_TIME;
import static com.genkimiru.app.Constants.Preference.PHONE_STEP_COUNTER;
import static com.genkimiru.app.Constants.Preference.USER_TOKEN;
import static com.genkimiru.app.data.remote.genki.request.AddManualInputRequest.DataType.STEP;
import static com.genkimiru.app.service.stepcounter.StepCounterService.Action.NONE;
import static com.genkimiru.app.service.stepcounter.StepCounterService.Action.START;
import static com.genkimiru.app.service.stepcounter.StepCounterService.Action.STOP;

public class StepCounterService extends Service implements SensorEventListener {

    private static final int PUSH_INTERVAL = 5 * 60; // seconds
    private static final int STEP_SAVE_INTERVAL = 30; // seconds

    private static int sSteps;
    private static Action sCurrentAction = NONE;

    @Inject
    UserUseCase mUserUseCase;

    private long mLastStepCounterTime;
    private int mLatestStepCounterPush;
    private SensorManager mSensorManager;
    private CompositeDisposable mCompositeDisposable;

    public static void requestCurrentStepCounter() {
        postStepCounter();
    }

    public static void Start(Context context) {
        if (sCurrentAction == START) {
            return;
        }
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) == null) {
            return;
        }
        Intent intent = new Intent(context, StepCounterService.class);
        intent.setAction(START.getActionStr());
        context.startService(intent);
    }

    public static void Stop(Context context) {
        if (sCurrentAction == STOP || sCurrentAction == NONE) {
            return;
        }

        Intent intent = new Intent(context, StepCounterService.class);
        intent.setAction(STOP.getActionStr());
        context.startService(intent);
    }

    public enum Action {
        NONE("None"),
        START("Start"),
        STOP("Stop");

        private String actionStr;

        Action(String actionStr) {
            this.actionStr = actionStr;
        }

        public static Action getActionFromString(String str) {
            for (Action action : Action.values()) {
                if (action.getActionStr().equals(str)) {
                    return action;
                }
            }
            return NONE;
        }

        public String getActionStr() {
            return actionStr;
        }
    }

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
        startForeground(1, new Notification());
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Action action = Action.getActionFromString(intent.getAction());
        Timber.e("onStartCommand: %s", action);
        sCurrentAction = action;
        switch (action) {
            case START:
                Timber.e("Start step counter");
                start();
                registerStepCounterSensor();
                return START_REDELIVER_INTENT;
            case STOP:
                Timber.e("Stop step counter");
                stop();
                return START_NOT_STICKY;
            default:
                return START_NOT_STICKY;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        mLastStepCounterTime = System.currentTimeMillis() + ((sensorEvent.timestamp - SystemClock.elapsedRealtimeNanos()) / 1000000L);
        checkForNewDay(mLastStepCounterTime);
        sSteps += sensorEvent.values.length;
        postStepCounter();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private static void postStepCounter() {
        CompositeDisposable compositeDisposable = new CompositeDisposable();

        GenkiStepCounterModel genkiStepCounterModel = new GenkiStepCounterModel(0, 0, sSteps);
        GenkiApplication.getEventBus().send(genkiStepCounterModel);

        compositeDisposable.add(GenkiApplication.getEventBus().toFlowable()
                .filter(o -> o instanceof GenkiStepCounterModel)
                .map(o -> (GenkiStepCounterModel) o)
                .subscribe());

    }

    private void registerStepCounterSensor() {
        Timber.d("registerStepCounterSensor");
        Sensor countSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if (countSensor == null) {
            Timber.w("Count sensor not available!");
            return;
        }
        mSensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
    }

    private void start() {
        mCompositeDisposable.clear();

        checkForNewDay(DateUtil.today().getTimeInMillis());
        sSteps = GenkiApplication.getPreferences().getInteger(PHONE_STEP_COUNTER, 0).get();
        mLastStepCounterTime = GenkiApplication.getPreferences().getLong(PHONE_LAST_STEP_COUNTER_TIME, 0L).get();

        // Start save steps interval
        mCompositeDisposable.add(
                Flowable.interval(STEP_SAVE_INTERVAL, STEP_SAVE_INTERVAL, TimeUnit.SECONDS).subscribe(aLong -> {
                    GenkiApplication.getPreferences().getInteger(PHONE_STEP_COUNTER).set(sSteps);
                    GenkiApplication.getPreferences().getLong(PHONE_LAST_STEP_COUNTER_TIME).set(mLastStepCounterTime);
                })
        );

        // Start push steps to server interval
        mCompositeDisposable.add(
                Flowable.interval(PUSH_INTERVAL, PUSH_INTERVAL, TimeUnit.SECONDS).subscribe(aLong -> {
                    long endTime = DateUtil.today().getTimeInMillis() / 1000;
                    long startTime = endTime - PUSH_INTERVAL;
                    mLatestStepCounterPush = sSteps;
                    int stepPush = mLatestStepCounterPush - GenkiApplication.getPreferences().getInteger(PHONE_LAST_STEP_COUNTER_PUSH).get();
                    Timber.d("Steps to push: %s", stepPush);
                    if (stepPush <= 0) {
                        Timber.d("Steps are not changed, skip push to server");
                        return;
                    }
                    String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
                    AddManualInputRequest.StepData stepData = new AddManualInputRequest.StepData(startTime, endTime, stepPush);
                    AddManualInputRequest addManualInputRequest = new AddManualInputRequest(token, startTime, endTime, STEP, Arrays.asList(stepData));
                    mUserUseCase.execute(new AddManualStepSubscriber(), UserParam.Factory.addManualInput(addManualInputRequest));
                })
        );
    }

    private void stop() {
        mCompositeDisposable.clear();
        mSensorManager.unregisterListener(this);
        GenkiApplication.getPreferences().getInteger(PHONE_LAST_STEP_COUNTER_PUSH).delete();
        GenkiApplication.getPreferences().getInteger(PHONE_STEP_COUNTER).delete();
        GenkiApplication.getPreferences().getLong(PHONE_LAST_STEP_COUNTER_TIME).delete();
    }

    private void checkForNewDay(long currentTime) {
        Calendar newStepCounterCalendar = Calendar.getInstance();
        newStepCounterCalendar.setTimeInMillis(currentTime);
        newStepCounterCalendar.set(Calendar.HOUR_OF_DAY, 0);
        newStepCounterCalendar.set(Calendar.MINUTE, 0);
        newStepCounterCalendar.set(Calendar.SECOND, 0);

        long oldStepCounterTime = GenkiApplication.getPreferences().getLong(PHONE_LAST_STEP_COUNTER_TIME, 0L).get();
        if (oldStepCounterTime == 0) {
            return;
        }

        Calendar oldStepCounterCalendar = Calendar.getInstance();
        oldStepCounterCalendar.setTimeInMillis(oldStepCounterTime);
        oldStepCounterCalendar.set(Calendar.HOUR_OF_DAY, 0);
        oldStepCounterCalendar.set(Calendar.MINUTE, 0);
        oldStepCounterCalendar.set(Calendar.SECOND, 0);

        Timber.d("New counter date: %s/%s/%s",
                newStepCounterCalendar.get(Calendar.DAY_OF_MONTH),
                newStepCounterCalendar.get(Calendar.MONTH),
                newStepCounterCalendar.get(Calendar.YEAR));
        Timber.d("Old counter date: %s/%s/%s",
                oldStepCounterCalendar.get(Calendar.DAY_OF_MONTH),
                oldStepCounterCalendar.get(Calendar.MONTH),
                oldStepCounterCalendar.get(Calendar.YEAR));

        if ((newStepCounterCalendar.get(Calendar.DATE) - oldStepCounterCalendar.get(Calendar.DATE)) >= 1) {
            Timber.d("Reset step counter for new day");
            sSteps = 0;
            GenkiApplication.getPreferences().getInteger(PHONE_LAST_STEP_COUNTER_PUSH).set(sSteps);
            GenkiApplication.getPreferences().getInteger(PHONE_STEP_COUNTER).set(sSteps);
            GenkiApplication.getPreferences().getLong(PHONE_LAST_STEP_COUNTER_TIME).set(mLastStepCounterTime);
        }

        Timber.d("Still on the same day, not reset step counter");
    }

    public class AddManualStepSubscriber extends DefaultSubscriber<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            super.onNext(aBoolean);
            Timber.d("push step manual input success");
            GenkiApplication.getPreferences().getInteger(PHONE_LAST_STEP_COUNTER_PUSH).set(mLatestStepCounterPush);
        }

        @Override
        public void onError(Throwable t) {
            Timber.e("push step manual input fail");
        }
    }
}
