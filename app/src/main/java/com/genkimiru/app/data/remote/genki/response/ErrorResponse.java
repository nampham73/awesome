package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class ErrorResponse {

    public int status;
    public List<String> body;
}
