package com.genkimiru.app.data.repository.user;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.genki.request.ActivePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.data.remote.genki.request.ChangePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.LoginFacebookRequest;
import com.genkimiru.app.data.remote.genki.request.LoginRequest;
import com.genkimiru.app.data.remote.genki.request.LoginWithGoogleRequest;
import com.genkimiru.app.data.remote.genki.request.NotificationSettingRequest;
import com.genkimiru.app.data.remote.genki.request.PrivacySettingRequest;
import com.genkimiru.app.data.remote.genki.request.RecoverPasswordRequest;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.request.SettingEmailMarketingRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.remote.genki.response.LoginFacebookDataResponse;
import com.genkimiru.app.data.remote.genki.response.LoginFacebookResponse;
import com.genkimiru.app.data.remote.genki.response.LoginResponse;
import com.genkimiru.app.data.remote.genki.response.LoginWithGoogleResponse;
import com.genkimiru.app.data.remote.genki.response.RecoverPasswordResponse;
import com.genkimiru.app.data.remote.genki.response.RegisterUserResponse;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;

import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Flowable;
import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Network.HTTP_STATUS_OK;

public class UserRepositoryImp implements UserRepository {

    private GenkiApi mApi;

    public UserRepositoryImp(GenkiApi api) {
        mApi = api;
    }

    @Override
    public Flowable<LoginResponse> login(LoginRequest loginRequest) {
        return mApi.login(BuildConfig.HEADER_SERVER_VALUE, loginRequest).flatMap(responseResult -> {
            Timber.d("repository login status: %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(responseResult.convertBodyTo(LoginResponse.class));
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<UserInfoResponse> loginAndFetchingUserInfo(LoginRequest request) {
        return login(request).flatMap(loginResponse -> {
            String token = loginResponse.token;
            return getUserInformation(token);
        });
    }

    @Override
    public Flowable<LoginFacebookDataResponse> loginFacebook(LoginFacebookRequest exchange_token) {
        return mApi.loginFacebook(BuildConfig.HEADER_SERVER_VALUE, exchange_token).flatMap(responseResult -> {
            Timber.d("repository login facebook status : %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                LoginFacebookResponse response = responseResult.convertBodyTo(LoginFacebookResponse.class);
                if (response != null) {
                    return Single.just(response.data);
                } else return new GenkiApiErrorException(responseResult).toSingleError();
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> addManualInput(AddManualInputRequest addManualInputRequest) {
        String timezone = TimeZone.getDefault().getID();
        return mApi.addManualInput(BuildConfig.HEADER_SERVER_VALUE, timezone, addManualInputRequest)
                .flatMap(responseResult -> {
                    Timber.d("repository add manual input status: %s", responseResult.status);
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(true);
                    } else {
                        return new GenkiApiErrorException(responseResult).toSingleError();
                    }
                }).retryWhen(throwable -> {
                    AtomicInteger counter = new AtomicInteger();
                    return throwable.takeWhile(throwable1 -> counter.getAndIncrement() != 3)
                            .flatMap(throwable1 -> {
                                Timber.d("add manual input delay retry by %s second(s)", counter.get());
                                return Flowable.timer(counter.get(), TimeUnit.SECONDS);
                            });
                }).toFlowable();
    }

    @Override
    public Flowable<LoginWithGoogleResponse> loginGoogle(LoginWithGoogleRequest loginWithGoogleRequest) {
        return mApi.loginGoolePlus(BuildConfig.HEADER_SERVER_VALUE, loginWithGoogleRequest).flatMap(responseResult -> {
            Timber.d("repository login goolge status : %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                LoginWithGoogleResponse response = responseResult.convertBodyTo(LoginWithGoogleResponse.class);
                if (response != null) {
                    return Single.just(responseResult.convertBodyTo(LoginWithGoogleResponse.class));
                } else return new GenkiApiErrorException(responseResult).toSingleError();
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<UserInfoResponse> getUserInformation(String token) {
        return mApi.getUserInformation(BuildConfig.HEADER_SERVER_VALUE, token).flatMap(responseResult -> {
            Timber.d("repository get user information status: %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                UserInfoResponse infoResponse = responseResult.convertBodyTo(UserInfoResponse.class);
                if (infoResponse != null) {
                    infoResponse.token = token;
                    return Single.just(infoResponse);
                } else return new GenkiApiErrorException(responseResult).toSingleError();
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<RegisterUserResponse> register(RegisterUserRequest registerUserRequest) {
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.username);
        RequestBody password = RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.password);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.email);
        RequestBody birthday = RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.birthday);
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.gender));
        RequestBody requestFile = null;
        if (registerUserRequest.avatar != null) {
            requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), registerUserRequest.avatar);
        }

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = requestFile != null ?
                MultipartBody.Part.createFormData("avatar", registerUserRequest.avatar.getName(), requestFile) : null;

        RequestBody firstname = registerUserRequest.first_name != null
                ? RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.first_name)
                : null;
        RequestBody lastname = registerUserRequest.last_name != null
                ? RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.last_name)
                : null;
        RequestBody height = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.height));
        RequestBody heightType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.height_type));
        RequestBody weight = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.weight));
        RequestBody weightType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.weight_type));
//        RequestBody bloodPressure = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.blood_pressure));
        RequestBody bloodType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.blood_type));
        RequestBody surgery_status = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.surgery_status));
        RequestBody feelCondition = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.feel_condition));
        RequestBody smokingLevel = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.smoking_level));
        RequestBody alcoholLevel = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.alcohol_level));
        RequestBody gobeUsername = RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.gobe_username);
        RequestBody gobePassword = RequestBody.create(MediaType.parse("text/plain"), registerUserRequest.gobe_password);
        RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.latitude));
        RequestBody longtitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.longitude));
        RequestBody nickName = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.nick_name));
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.address));
        RequestBody smokingEachTime = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.smoking_each_time));
        RequestBody drinkEachTime = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.drink_each_time));
        RequestBody healthSource = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.health_source));
        RequestBody fitbitAccessToken = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.fitbit_access_token));
        RequestBody fitbitRefreshToken = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.fitbit_refresh_token));
        RequestBody fitbitExpiresIn = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.fitbit_expires_in));
        RequestBody bloodPressureUnder = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.blood_pressure_under));
        RequestBody bloodPressureUpper = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(registerUserRequest.blood_pressure_upper));

        return mApi.register(BuildConfig.HEADER_SERVER_VALUE, username, password, email, birthday,
                gender, body, firstname, lastname, height, heightType, weight, weightType, bloodType,
                surgery_status, feelCondition, smokingLevel, alcoholLevel, gobeUsername, gobePassword,
                latitude, longtitude, nickName, address, smokingEachTime, drinkEachTime, healthSource,
                fitbitAccessToken, fitbitRefreshToken, fitbitExpiresIn, bloodPressureUnder, bloodPressureUpper).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(responseResult.convertBodyTo(RegisterUserResponse.class));
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<RecoverPasswordResponse> recoverPassword(RecoverPasswordRequest recoverPasswordRequest) {
        return mApi.recoverPassword(BuildConfig.HEADER_SERVER_VALUE, recoverPasswordRequest)
                .flatMap(responseResult -> {
                    Timber.d("repository recoverPassword status: %s", responseResult.status);
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(responseResult.convertBodyTo(RecoverPasswordResponse.class));
                    } else {
                        return new GenkiApiErrorException(responseResult).toSingleError();
                    }
                }).toFlowable();
    }

    @Override
    public Flowable<Boolean> activePassword(ActivePasswordRequest activePasswordRequest) {
        return mApi.activePassword(BuildConfig.HEADER_SERVER_VALUE, activePasswordRequest)
                .flatMap(responseResult -> {
                    Timber.d("repository active password status: %s", responseResult.status);
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(true);
                    } else {
                        return new GenkiApiErrorException(responseResult).toSingleError();
                    }
                }).toFlowable();
    }

    @Override
    public Flowable<Boolean> updatePrivacySetting(PrivacySettingRequest privacySettingRequest) {
        return mApi.updatePrivacySetting(BuildConfig.HEADER_SERVER_VALUE, privacySettingRequest)
                .flatMap(responseResult -> {
                    Timber.d("repository update privacy setting status: %s", responseResult.status);
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(true);
                    } else {
                        return new GenkiApiErrorException(responseResult).toSingleError();
                    }
                }).toFlowable();
    }

    @Override
    public Flowable<Boolean> updateEmailMarketingSetting(SettingEmailMarketingRequest settingEmailMarketingRequest) {
        return mApi.updateEmailMarketingSetting(BuildConfig.HEADER_SERVER_VALUE, settingEmailMarketingRequest)
                .flatMap(responseResult -> {
                    Timber.d("repository update email marketing setting status: %s", responseResult.status);
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(true);
                    } else {
                        return new GenkiApiErrorException(responseResult).toSingleError();
                    }
                }).toFlowable();
    }

    @Override
    public Flowable<Integer> updateHealthSourceSetting(UserInfoRequest healthSourceRequest) {
        int healthSource = healthSourceRequest.getHealthSource();
        return mApi.updateUser(BuildConfig.HEADER_SERVER_VALUE, healthSourceRequest)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) return Single.just(healthSource);
                    return new GenkiApiErrorException(responseResult).toSingleError();
                }).toFlowable();
    }

    @Override
    public Flowable<Boolean> updateUserProfile(UserInfoRequest userInfoRequest) {
        return mApi.updateUser(BuildConfig.HEADER_SERVER_VALUE, userInfoRequest)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) return Single.just(true);
                    return new GenkiApiErrorException(responseResult).toSingleError();
                }).toFlowable();
    }

    @Override
    public Flowable<Boolean> updateUserAvatar(UserInfoRequest userInfoRequest) {
        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), userInfoRequest.getToken());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), userInfoRequest.getEmail());
        RequestBody birthday = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getBirthday()));
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getGender()));

        RequestBody requestFile = null;
        if (userInfoRequest.avatar != null) {
            requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), userInfoRequest.avatar);
        }

        RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), userInfoRequest.getFirst_name());
        RequestBody lastName = RequestBody.create(MediaType.parse("text/plain"), userInfoRequest.getLast_name());
        RequestBody height = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getHeight()));
        RequestBody weight = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getWeight()));
        RequestBody heightType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getHeightType()));
        RequestBody weightType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getWeightType()));
        RequestBody surgeryStatus = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getSurgeryStatus()));
        RequestBody feelCondition = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getFeelcondition()));
        RequestBody smokingLevel = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getSmokingLevel()));
        RequestBody alcoholLevel = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getAlcoholLevel()));
        RequestBody bloodType = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getBlood_type()));
        RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getLatitude()));
        RequestBody longtitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getLongitude()));
        RequestBody gobeUsername = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getGobe_username()));
        RequestBody gobePassword = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getGobe_password()));
        RequestBody nickName = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getNickname()));
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getAddress()));
        RequestBody smokingEachTime = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getSmokingeEachTime()));
        RequestBody drinkEachTime = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getDrinkEachTime()));
        RequestBody fitBitAccessToken = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getFitbit_access_token()));
        RequestBody fitBitRefreshToken = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getFitbit_refresh_token()));
        RequestBody healthSource = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getHealthSource()));
        RequestBody bloodPressureUnder = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getBlood_pressure_under()));
        RequestBody bloodPressureUpper = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userInfoRequest.getBlood_pressure_upper()));


        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = requestFile != null ?
                MultipartBody.Part.createFormData("avatar", userInfoRequest.avatar.getName(), requestFile) : null;

        return mApi.updateAvatar(BuildConfig.HEADER_SERVER_VALUE, token, email, birthday, gender, body, firstName,
                lastName, height, weight, heightType, weightType, surgeryStatus, feelCondition, smokingLevel, alcoholLevel, bloodType, latitude,
                longtitude, gobeUsername, gobePassword, nickName, address, smokingEachTime, drinkEachTime, fitBitAccessToken, fitBitRefreshToken,
                healthSource, bloodPressureUnder, bloodPressureUpper).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> changePassword(ChangePasswordRequest changePasswordRequest) {
        return mApi.changePassword(BuildConfig.HEADER_SERVER_VALUE, changePasswordRequest)
                .flatMap(responseResult -> {
                    Timber.d("repository change password status: %s", responseResult.status);
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(true);
                    } else {
                        return new GenkiApiErrorException(responseResult).toSingleError();
                    }
                }).toFlowable();
    }

    @Override
    public Flowable<ResponseBody> downloadImage(String url) {
        return mApi.downloadImage(url).toFlowable();
    }

    @Override
    public Flowable<Boolean> updateNotificationSetting(NotificationSettingRequest notificationSettingRequest) {
        return mApi.updateNotificationSetting(BuildConfig.HEADER_SERVER_VALUE, notificationSettingRequest).flatMap(responseResult -> {
            Timber.d("repository update notification setting status: %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> processChallenge(ChallengeRequest challengeRequest) {
        return mApi.processChallenge(BuildConfig.HEADER_SERVER_VALUE, challengeRequest).flatMap(responseResult -> {
            Timber.d("repository process challenge status: %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            }

            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }
}
