package com.genkimiru.app.data.remote.genki.response;

public class StepResponse {
    public long timestamp;
    public float step;
    public float second;
    public float numAverage;
}
