package com.genkimiru.app.data.remote.google.response;

public class GoogleFitUserProfileResponse {
    public String name;
    public String picture;
    public String locale;
    public String gender;
    public String family_name;
    public String given_name;
}
