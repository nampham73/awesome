package com.genkimiru.app.data.remote.genki.request;

public class JoinRequest {
    public Integer contest_id;
    public String token;

    public JoinRequest(Integer id, String token) {
        this.contest_id = id;
        this.token = token;
    }
}
