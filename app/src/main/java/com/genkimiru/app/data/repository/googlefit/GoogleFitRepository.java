package com.genkimiru.app.data.repository.googlefit;

import com.genkimiru.app.data.remote.google.request.GoogleAccessTokenRequest;
import com.genkimiru.app.data.remote.google.request.GoogleFitUserProfileRequest;
import com.genkimiru.app.data.remote.google.response.GoogleAccessTokenResponse;
import com.genkimiru.app.data.remote.google.response.GoogleFitUserProfileResponse;

import io.reactivex.Flowable;
import okhttp3.ResponseBody;

public interface GoogleFitRepository {
    Flowable<GoogleAccessTokenResponse> getAccessToken(GoogleAccessTokenRequest googleAccessTokenRequest);
    Flowable<GoogleFitUserProfileResponse> getUserProfile(GoogleFitUserProfileRequest googleFitUserProfileRequest);
    Flowable<ResponseBody> getImageRequest(String url);
}
