package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.DetailDataChartResponse;
import com.genkimiru.app.data.remote.genki.response.SleepResponse;

import java.util.ArrayList;
import java.util.List;

public class GenkiDetailChartDataModel {
    private float status;
    private float startDate;
    private float endDate;
    private String dataType;
    private String chartType;
    private float itemPerPage;
    private float index;
    private String lastUpdateDate;
    private float count;
    private float totalPage;
    private List<GenkiSleepModel> data;

    public GenkiDetailChartDataModel(float status,
                                     float startDate,
                                     float endDate,
                                     String dataType,
                                     String chartType,
                                     float itemPerPage,
                                     float index,
                                     String lastUpdateDate,
                                     float count,
                                     float totalPage,
                                     List<GenkiSleepModel> data) {
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.dataType = dataType;
        this.chartType = chartType;
        this.itemPerPage = itemPerPage;
        this.index = index;
        this.lastUpdateDate = lastUpdateDate;
        this.count = count;
        this.totalPage = totalPage;
        this.data = data;
    }

//    public static GenkiDetailChartDataModel convert(DetailDataChartResponse response) {
//        List<GenkiSleepModel> data = new ArrayList<>();
//        if (response.data != null && response.data.size() > 0) {
//            for (SleepResponse datum : response.data) {
//                GenkiSleepModel model = GenkiSleepModel.convert(datum);
//                if (model!=null)data.add(model);
//            }
//        }
//        return new GenkiDetailChartDataModel(response.status,
//                response.startDate,
//                response.endDate,
//                response.dataType,
//                response.chartType,
//                response.itemPerPage,
//                response.index,
//                response.lastUpdateDate,
//                response.count,
//                response.totalPage,
//                data);
//    }
}

