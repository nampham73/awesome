package com.genkimiru.app.data.model;

import java.io.Serializable;

public class GenkiStepCounterModel implements Serializable {

    private long startTime;
    private long endTime;
    private int step;

    public GenkiStepCounterModel(long startTime, long endTime, int step) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.step = step;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}
