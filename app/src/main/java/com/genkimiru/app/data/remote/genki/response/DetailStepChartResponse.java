package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class DetailStepChartResponse extends DetailDataChartResponse {
    public List<StepResponse> data;
}
