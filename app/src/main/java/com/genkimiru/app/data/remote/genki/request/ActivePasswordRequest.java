package com.genkimiru.app.data.remote.genki.request;

public class ActivePasswordRequest {
    public String password;
    public String code;

    public ActivePasswordRequest(String password, String code) {
        this.password = password;
        this.code = code;
    }
}
