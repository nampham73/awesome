package com.genkimiru.app.data.remote.fitbit.request;

public class FitbitUserProfileRequest {

    public String token;

    public FitbitUserProfileRequest(String token) {
        this.token = token;
    }
}
