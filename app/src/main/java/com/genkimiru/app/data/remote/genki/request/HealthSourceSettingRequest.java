package com.genkimiru.app.data.remote.genki.request;

public class HealthSourceSettingRequest {
    public String token;
    public int health_source;

    public HealthSourceSettingRequest(String token,int healthSource){
        this.token = token;
        this.health_source = healthSource;
    }
}
