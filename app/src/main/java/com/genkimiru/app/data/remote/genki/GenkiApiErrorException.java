package com.genkimiru.app.data.remote.genki;

import com.genkimiru.app.R;
import com.genkimiru.app.data.remote.genki.response.ErrorResponse;
import com.genkimiru.app.data.remote.genki.response.ResponseResult;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import timber.log.Timber;

public class GenkiApiErrorException extends Exception {

    private ResponseResult mResponse;

    public GenkiApiErrorException(ResponseResult responseResult) {
        mResponse = responseResult;
    }

    public int getMessageId() {
        ErrorResponse errorResponse = parseErrorResponse();
        Timber.e("errorResponse = %s", errorResponse);
        return errorResponse == null ? R.string.unexpected_error : ErrorCode.getMessageIdByCode(errorResponse.body.get(0));
    }

    public int getMessageId(int defaultMessageId) {
        if (defaultMessageId == 0) {
            return getMessageId();
        }
        int messageId = getMessageId();
        return messageId == R.string.unexpected_error ? defaultMessageId : messageId;
    }

    public void setResponse(ResponseResult response) {
        this.mResponse = response;
    }

    public <T> Single<T> toSingleError() {
        return Single.error(this);
    }

    public String getMessage() {
        ErrorResponse errorResponse = parseErrorResponse();
        if (errorResponse == null) {
            return "";
        }
        return "Http status: " + errorResponse.status + " - Error code: "
                + errorResponse.body.get(0);
    }

    private ErrorResponse parseErrorResponse() {
        Timber.e("ResultResponse: %s", mResponse);
        if (mResponse == null) {
            return null;
        }
        Gson gson = new Gson();
        String jsonString = gson.toJson(mResponse);
        return gson.fromJson(jsonString, ErrorResponse.class);
    }

    public List<String> getErrorList(){
        List<String> result = null;
        ErrorResponse errorResponse = parseErrorResponse();
        if (errorResponse != null){
            result = errorResponse.body;
        }
        return result;
    }

    public enum ErrorCode {

        UNEXPECTED("", R.string.unexpected_error),
        LOGIN_001_E_001("Login_001_E_001", R.string.login_001_e_001),
        LOGIN_001_E_005("Login_001_E_005", R.string.login_001_e_005),
        LOGIN_001_E_008("Login_001_E_008", R.string.login_001_e_008),
        LOGIN_001_E_009("Login_001_E_009", R.string.login_001_e_009),
        LOGIN_001_E_0013("Login_001_E_0013", R.string.login_001_e_013),

        SEND_CODE_PASSWORD_001_E_001("SendCodePassword_001_E_001", R.string.send_code_password_001_e_001),
        SEND_CODE_PASSWORD_001_E_004("SendCodePassword_001_E_004", R.string.send_code_password_001_e_004),
        SEND_CODE_PASSWORD_001_E_005("SendCodePassword_001_E_005", R.string.send_code_password_001_e_005),

        RECOVER_PASSWORD_001_E_001("RecoverPassword_001_E_001", R.string.recover_password_001_e_001),
        RECOVER_PASSWORD_001_E_002("RecoverPassword_001_E_002", R.string.recover_password_001_e_002),
        RECOVER_PASSWORD_001_E_003("RecoverPassword_001_E_003", R.string.recover_password_001_e_003),
        RECOVER_PASSWORD_001_E_004("RecoverPassword_001_E_004", R.string.recover_password_001_e_004),
        RECOVER_PASSWORD_001_E_007("RecoverPassword_001_E_007", R.string.recover_password_001_e_007),
        RECOVER_PASSWORD_001_E_008("RecoverPassword_001_E_008", R.string.recover_password_001_e_008),

        UPDATE_PRIVACY_SETTING_001_E_001("UpdatePrivacySetting_001_E_001", R.string.unexpected_error),
        UPDATE_PRIVACY_SETTING_001_E_002("UpdatePrivacySetting_001_E_002", R.string.unexpected_error),

        UPDATE_PASSWORD_001_E001("UpdatePassword_001_E_001", R.string.update_password_001_e_001),
        UPDATE_PASSWORD_001_E002("UpdatePassword_001_E_002", R.string.update_password_001_e_002),
        UPDATE_PASSWORD_001_E003("UpdatePassword_001_E_003", R.string.update_password_001_e_003),
        UPDATE_PASSWORD_001_E004("UpdatePassword_001_E_004", R.string.update_password_001_e_004),
        UPDATE_PASSWORD_001_E005("UpdatePassword_001_E_005", R.string.update_password_001_e_005),
        UPDATE_PASSWORD_001_E006("UpdatePassword_001_E_006", R.string.update_password_001_e_006),
        UPDATE_PASSWORD_001_E007("UpdatePassword_001_E_007", R.string.update_password_001_e_007),
        UPDATE_PASSWORD_001_E008("UpdatePassword_001_E_008", R.string.update_password_001_e_008),

        UPDATE_NOTIFICATION_SETTING_001_001_E_006("UpdateNotificationSetting_001_001_E_006", R.string.update_notification_setting_001_001_e_006),


        UPDATE_EMAIL_MARKETING_SETTING_001_E_001("UpdateEmailMarketingSetting_001_E_001", R.string.unexpected_error),
        UPDATE_EMAIL_MARKETING_SETTING_001_E_002("UpdateEmailMarketingSetting_001_E_002", R.string.unexpected_error),

        LIST_CONTEST_HISTORY_CHALLENGE_001_E_001("HistoryContest_001_E_001", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_002("HistoryContest_001_E_002", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_003("HistoryContest_001_E_003", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_004("HistoryContest_001_E_004", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_005("HistoryContest_001_E_005", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_006("HistoryContest_001_E_006", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_007("HistoryContest_001_E_007", R.string.unexpected_error),
        LIST_CONTEST_HISTORY_CHALLENGE_001_E_008("HistoryContest_001_E_008", R.string.unexpected_error),

        JOIN_CONTEST_001_E_006("JoinContest_001_E_006", R.string.join_contest_001_e_006),
        JOIN_CONTEST_001_E_007("JoinContest_001_E_007", R.string.join_contest_001_e_007),
        JOIN_CONTEST_001_E_008("JoinContest_001_E_008", R.string.join_contest_001_e_008),
        JOIN_CONTEST_001_E_009("JoinContest_001_E_009", R.string.join_contest_001_e_009),
        JOIN_CONTEST_001_E_010("JoinContest_001_E_010", R.string.join_contest_001_e_010),
        JOIN_CONTEST_001_E_012("JoinContest_001_E_012", R.string.join_contest_001_e_012),
        JOIN_CONTEST_001_E_013("JoinContest_001_E_013", R.string.join_contest_001_e_013),
        JOIN_CONTEST_001_E_014("JoinContest_001_E_014", R.string.join_contest_001_e_014),
        JOIN_CONTEST_001_E_015("JoinContest_001_E_015", R.string.join_contest_001_e_015),
        JOIN_CONTEST_001_E_016("JoinContest_001_E_016", R.string.join_contest_001_e_016),

        START_STOP_CHALLENGE_001_E_002("StartStopChallenge_001_E_002", R.string.StartStopChallenge_001_E_002),
        START_STOP_CHALLENGE_001_E_005("StartStopChallenge_001_E_005", R.string.StartStopChallenge_001_E_005),
        START_STOP_CHALLENGE_001_E_006("StartStopChallenge_001_E_006", R.string.StartStopChallenge_001_E_006),
        START_STOP_CHALLENGE_001_E_011("StartStopChallenge_001_E_011", R.string.StartStopChallenge_001_E_011),
        START_STOP_CHALLENGE_001_E_012("StartStopChallenge_001_E_012", R.string.StartStopChallenge_001_E_012),

        REGISTER_USER_001_E_001("RegisterUser_001_E_001", R.string.register_001_e_001),
        REGISTER_USER_001_E_002("RegisterUser_001_E_002", R.string.register_001_e_002),
        REGISTER_USER_001_E_003("RegisterUser_001_E_003", R.string.register_001_e_003),
        REGISTER_USER_001_E_004("RegisterUser_001_E_004", R.string.register_001_e_004),
        REGISTER_USER_001_E_005("RegisterUser_001_E_005", R.string.register_001_e_005),
        REGISTER_USER_001_E_006("RegisterUser_001_E_006", R.string.register_001_e_006),
        REGISTER_USER_001_E_007("RegisterUser_001_E_007", R.string.register_001_e_007),
        REGISTER_USER_001_E_008("RegisterUser_001_E_008", R.string.register_001_e_008),
        REGISTER_USER_001_E_009("RegisterUser_001_E_009", R.string.register_001_e_009),
        REGISTER_USER_001_E_010("RegisterUser_001_E_010", R.string.register_001_e_010),
        REGISTER_USER_001_E_011("RegisterUser_001_E_011", R.string.register_001_e_011),
        REGISTER_USER_001_E_014("RegisterUser_001_E_014", R.string.register_001_e_014),
        REGISTER_USER_001_E_015("RegisterUser_001_E_015", R.string.register_001_e_015),
        REGISTER_USER_001_E_016("RegisterUser_001_E_016", R.string.register_001_e_016),
        REGISTER_USER_001_E_017("RegisterUser_001_E_017", R.string.register_001_e_017),
        REGISTER_USER_001_E_022("RegisterUser_001_E_022", R.string.register_001_e_022),
        REGISTER_USER_001_E_041("RegisterUser_001_E_041", R.string.register_001_e_041),
        REGISTER_USER_001_E_042("RegisterUser_001_E_042", R.string.register_001_e_042),
        REGISTER_USER_001_E_043("RegisterUser_001_E_043", R.string.register_001_e_043),
        REGISTER_USER_001_E_044("RegisterUser_001_E_044", R.string.register_001_e_044),
        REGISTER_USER_001_E_047("RegisterUser_001_E_047", R.string.register_001_e_047),
        REGISTER_USER_001_E_048("RegisterUser_001_E_048", R.string.register_001_e_048),
        REGISTER_USER_001_E_055("RegisterUser_001_E_055", R.string.register_001_e_055),
        REGISTER_USER_001_E_056("RegisterUser_001_E_056", R.string.register_001_e_056),
        REGISTER_USER_001_E_057("RegisterUser_001_E_057", R.string.register_001_e_057),
        REGISTER_USER_001_E_058("RegisterUser_001_E_058", R.string.register_001_e_058),
        REGISTER_USER_001_E_059("RegisterUser_001_E_059", R.string.register_001_e_059),
        REGISTER_USER_001_E_061("RegisterUser_001_E_061", R.string.register_001_e_061),
        REGISTER_USER_001_E_068("RegisterUser_001_E_068", R.string.register_001_e_068),
        REGISTER_USER_001_E_069("RegisterUser_001_E_069", R.string.register_001_e_069),

        GET_USER_INFORMATION_SRVICE("GetUserInformationService_Err_01", R.string.get_user_information_service_error),

        ADD_FAVOURITE_ARTICLE_001_E_004("AddFavouriteArticle_001_E_004", R.string.add_favourite_article_001_e_004),
        DELETE_FAVOURITE_CONTENT_001_E_003("DeleteFavouriteContent_001_E_003", R.string.delete_favourite_content_001_e_003),

        LOGIN_FB_001_E_001("LoginFB_001_E_001", R.string.LoginFB_001_E_001),
        LOGIN_FB_001_E_002("LoginFB_001_E_002", R.string.LoginFB_001_E_002),
        LOGIN_FB_001_E_003("LoginFB_001_E_003", R.string.LoginFB_001_E_003),
        LOGIN_FB_001_E_004("LoginFB_001_E_004", R.string.LoginFB_001_E_004),

        LOGIN_GOOGLE_001_E_001("LoginGoogle_001_E_001", R.string.logingoogle_001_e_001),
        LOGIN_GOOGLE_001_E_002("LoginGoogle_001_E_001", R.string.logingoogle_001_e_002),
        LOGIN_GOOGLE_001_E_003("LoginGoogle_001_E_001", R.string.logingoogle_001_e_003),
        LOGIN_GOOGLE_001_E_004("LoginGoogle_001_E_001", R.string.logingoogle_001_e_004),

        REGISTER_USER_001_E_078("RegisterUser_001_E_078", R.string.blood_pressure_error),
        REGISTER_USER_001_E_079("RegisterUser_001_E_079", R.string.blood_pressure_error),
        REGISTER_USER_001_E_080("RegisterUser_001_E_080", R.string.blood_pressure_error),
        REGISTER_USER_001_E_081("RegisterUser_001_E_081", R.string.blood_pressure_error),
        REGISTER_USER_001_E_082("RegisterUser_001_E_082", R.string.blood_pressure_error),
        REGISTER_USER_001_E_083("RegisterUser_001_E_083", R.string.blood_pressure_error),
        REGISTER_USER_001_E_084("RegisterUser_001_E_084", R.string.blood_pressure_error),

        UPDATE_USER_001_E_057("UpdateUser_001_E_057", R.string.blood_pressure_error),
        UPDATE_USER_001_E_058("UpdateUser_001_E_058", R.string.blood_pressure_error),
        UPDATE_USER_001_E_059("UpdateUser_001_E_059", R.string.blood_pressure_error),
        UPDATE_USER_001_E_060("UpdateUser_001_E_060", R.string.blood_pressure_error),
        UPDATE_USER_001_E_061("UpdateUser_001_E_061", R.string.blood_pressure_error),
        UPDATE_USER_001_E_062("UpdateUser_001_E_062", R.string.blood_pressure_error),
        UPDATE_USER_001_E_063("UpdateUser_001_E_063", R.string.blood_pressure_error);

        private String mCode;
        private int mMessageId;

        ErrorCode(String code, int messageId) {
            mCode = code;
            mMessageId = messageId;
        }

        static int getMessageIdByCode(String code) {
            for (ErrorCode errorCode : ErrorCode.values()) {
                if (errorCode.getCode().equals(code)) {
                    return errorCode.getMessageId();
                }
            }
            return UNEXPECTED.getMessageId();
        }

        public String getCode() {
            return mCode;
        }

        public int getMessageId() {
            return mMessageId;
        }
    }
}
