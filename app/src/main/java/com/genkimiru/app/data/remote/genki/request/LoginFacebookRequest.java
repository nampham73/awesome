package com.genkimiru.app.data.remote.genki.request;

public class LoginFacebookRequest {
    public String exchange_token;

    public LoginFacebookRequest(String exchange_token) {
        this.exchange_token = exchange_token;
    }
}
