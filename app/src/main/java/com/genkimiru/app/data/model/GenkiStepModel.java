package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.StepResponse;

public class GenkiStepModel {
    private long timestamp;
    private float step;
    private float second;
    private float numAverage;

    public GenkiStepModel() {

    }

    public GenkiStepModel(long timestamp, float step, float second, float numAverage) {
        this.timestamp = timestamp;
        this.step = step;
        this.second = second;
        this.numAverage = numAverage;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getStep() {
        return step;
    }

    public float getSecond() {
        return second;
    }

    public float getNumAverage() {
        return numAverage;
    }

    public static GenkiStepModel convert(StepResponse response){
        return new GenkiStepModel(response.timestamp, response.step, response.second, response.numAverage);
    }
}
