package com.genkimiru.app.data.remote.google;

import com.genkimiru.app.base.net.BaseRestApi;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class GoogleRestApi extends BaseRestApi<GoogleApi>{

    private static final String BASE_URL = "https://www.googleapis.com/";

    public GoogleRestApi(OkHttpClient client, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxAdapter) {
        super(client, gsonConverterFactory, rxAdapter);
    }

    @Override
    protected String registerBaseUrl() {
        return BASE_URL;
    }

    @Override
    protected Class<GoogleApi> registerApiClassType() {
        return GoogleApi.class;
    }

}
