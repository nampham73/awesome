package com.genkimiru.app.data.remote.genki.request;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.genkimiru.app.data.remote.genki.request.ListContentRequest.SortType.ASC;
import static com.genkimiru.app.data.remote.genki.request.ListContentRequest.SortType.DESC;

public class ListContentRequest {

    public String token;
    public Integer limit;
    public Integer offset;
    public String sort_name;
    public String sort_type;

    public ListContentRequest(String token, Integer limit, Integer offset, String sortName, String sortType) {
        this.token = token;
        this.limit = limit;
        this.offset = offset;
        this.sort_name = sortName;
        this.sort_type = sortType;
    }

    public ListContentRequest(String token, String sort_name, String sort_type) {
        this.token = token;
        this.sort_name = sort_name;
        this.sort_type = sort_type;
        limit = 20;
        offset = 0;
    }

    public ListContentRequest(String token, int offset, int limit) {
        this.token = token;
        this.sort_name = "created_at";
        this.sort_type = SortType.DESC;
        this.offset = offset;
        this.limit = limit;
    }

    public ListContentRequest(String token) {
        this.token = token;
        this.sort_name = "created_at";
        this.sort_type = SortType.DESC;
        limit = 20;
        offset = 0;
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({ASC, DESC})
    public @interface SortType {
        String ASC = "asc";
        String DESC = "desc";
    }
}

