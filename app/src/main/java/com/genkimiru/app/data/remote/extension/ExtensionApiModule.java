package com.genkimiru.app.data.remote.extension;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ExtensionApiModule {

    @Provides
    @Singleton
    ExtensionApi provideExtensionApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        ExtensionRestApi extensionRestApi = new ExtensionRestApi(client, gsonConverter, rxAdapter);
        return extensionRestApi.getApi();
    }
}
