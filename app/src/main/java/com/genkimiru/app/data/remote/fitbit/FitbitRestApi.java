package com.genkimiru.app.data.remote.fitbit;

import com.genkimiru.app.base.net.BaseRestApi;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class FitbitRestApi extends BaseRestApi<FitbitApi> {

    private static final String BASE_URL = "https://api.fitbit.com/";

    public FitbitRestApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        super(client, gsonConverter, rxAdapter);
    }

    @Override
    protected String registerBaseUrl() {
        return BASE_URL;
    }

    @Override
    protected Class<FitbitApi> registerApiClassType() {
        return FitbitApi.class;
    }
}
