package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.StressResponse;

public class GenkiStressModel {
    public long timestamp;
    public float stress_level;
    public float second;

    public GenkiStressModel(long timestamp, float stress_level, float second) {
        this.timestamp = timestamp;
        this.stress_level = stress_level;
        this.second = second;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getStressLevel() {
        return stress_level;
    }

    public float getSecond() {
        return second;
    }

    public static GenkiStressModel convert(StressResponse response) {
        return new GenkiStressModel(response.timestamp, response.stress_level, response.second);
    }
}
