package com.genkimiru.app.data.remote.extension.response;

public class ArticleResponse {

    public long id;
    public String title;
    public String body;

}
