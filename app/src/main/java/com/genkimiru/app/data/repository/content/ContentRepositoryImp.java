package com.genkimiru.app.data.repository.content;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.base.util.JsonGenerator;
import com.genkimiru.app.data.model.GenkiContentModel;
import com.genkimiru.app.data.model.GenkiDetailArticleModel;
import com.genkimiru.app.data.model.GenkiFeedModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.genki.request.ContentDetailRequest;
import com.genkimiru.app.data.remote.genki.request.ListContentRequest;
import com.genkimiru.app.data.remote.genki.response.BodyFeedResponse;
import com.genkimiru.app.data.remote.genki.response.ContentModelResponse;
import com.genkimiru.app.data.remote.genki.response.FeedModelResponse;
import com.genkimiru.app.data.remote.genki.response.ListContentResponse;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Single;

import static com.genkimiru.app.Constants.Network.HTTP_STATUS_OK;

public class ContentRepositoryImp implements ContentRepository {

    private GenkiApi mApi;

    public ContentRepositoryImp(GenkiApi api) {
        mApi = api;
    }

    @Override
    public Flowable<List<GenkiContentModel>> fetchingContents(ListContentRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingContents(BuildConfig.HEADER_SERVER_KEY, map).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                ListContentResponse response = responseResult.convertBodyTo(ListContentResponse.class);
                List<GenkiContentModel> genkiContentModels = convertListContentResponseToModel(response);
                return Single.just(genkiContentModels);
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<List<GenkiContentModel>> fetchingFavoriteContents(ListContentRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingFavoriteContents(BuildConfig.HEADER_SERVER_KEY, map).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                ListContentResponse response = responseResult.convertBodyTo(ListContentResponse.class);
                List<GenkiContentModel> genkiContentModels = convertListContentResponseToModel(response);
                return Single.just(genkiContentModels);
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    private List<GenkiContentModel> convertListContentResponseToModel(ListContentResponse listContentResponse) {
        List<GenkiContentModel> genkiContentModels = new ArrayList<>();
        for (ListContentResponse.Content content : listContentResponse.contents) {
            GenkiContentModel genkiContentModel = new GenkiContentModel();
            genkiContentModel.setValueFromResponse(content);
            genkiContentModels.add(genkiContentModel);
        }
        return genkiContentModels;
    }

    @Override
    public Flowable<ContentModelResponse> fetchingDetailContent(ContentDetailRequest map) {
        Map<String, String> param = JsonGenerator.getInstance().convertObjectToMap(map);
        return mApi.fetchingDetailArticle(BuildConfig.HEADER_SERVER_KEY, param)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        return Single.just(responseResult.convertBodyTo(ContentModelResponse.class));
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                }).toFlowable();
    }

    @Override
    public Flowable<List<FeedModelResponse>> fetchingFeeds(ContentDetailRequest map) {
        Map<String, String> param = JsonGenerator.getInstance().convertObjectToMap(map);
        return mApi.fetchingComments(BuildConfig.HEADER_SERVER_KEY, param)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        BodyFeedResponse feedResponse = responseResult.convertBodyTo(BodyFeedResponse.class);
                        if (feedResponse == null) feedResponse = new BodyFeedResponse();
                        if (feedResponse.comment!=null){
                            return Single.just(feedResponse.comment);
                        }
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                }).toFlowable();
    }

    @Override
    public Flowable<List<IDetailContentModel>> getArticleAndFeeds(ContentDetailRequest article, ContentDetailRequest feeds) {
        return Flowable.zip(fetchingDetailContent(article), fetchingFeeds(feeds), this::parse);
    }

    @Override
    public Flowable<Boolean> requestLike(ContentDetailRequest likes) {
        return mApi.requestLike(BuildConfig.HEADER_SERVER_KEY, likes).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) return Single.just(true);
            return Single.just(false);
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> requestUnLike(ContentDetailRequest likes) {
        return mApi.requestUnLike(BuildConfig.HEADER_SERVER_KEY, likes).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) return Single.just(false);
            return Single.just(true);
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> requestAddComment(ContentDetailRequest comment) {
        return mApi.requestAddComment(BuildConfig.HEADER_SERVER_KEY, comment).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) return Single.just(true);
            return Single.just(false);
        }).toFlowable();
    }

    private List<IDetailContentModel> parse(ContentModelResponse article, List<FeedModelResponse> feeds) {
        List<IDetailContentModel> items = new ArrayList<>();

        /// add detail article item.
        GenkiDetailArticleModel detail = new GenkiDetailArticleModel();
        detail.setValueFrom(article);
        items.add(detail);

        ///add feed into list
        if (feeds != null && feeds.size() > 0) {
            for (FeedModelResponse feed : feeds) {
                GenkiFeedModel feedModel = new GenkiFeedModel();
                feedModel.setValueFrom(feed);
                items.add(feedModel);
            }
        }
        return items;
    }
}
