package com.genkimiru.app.data.remote.genki.request;

public class DashboardRequest {
    private String token;
    private long current_time;


    public DashboardRequest(String token, long current) {
        this.token = token;
        this.current_time = current;
    }
}
