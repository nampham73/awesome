package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.google.response.GoogleFitUserProfileResponse;

import java.io.Serializable;

public class GFitUserInfoModel implements Serializable {
    private String name;
    private String given_name;
    private String family_name;
    private String picture;
    private String gender;
    private String locale;
    private int healthSource;
    private String avatarPath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGiven_name() {
        return given_name;
    }

    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getGender() {
        if (gender == null) gender = "";
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public void setHealthSource(int healthSource) {
        this.healthSource = healthSource;
    }

    public int getHealthSource() {
        return healthSource;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public void setUserInfoModel(GoogleFitUserProfileResponse googleFitUserProfileResponse) {
        name = googleFitUserProfileResponse.name;
        given_name = googleFitUserProfileResponse.given_name;
        family_name = googleFitUserProfileResponse.family_name;
        gender = googleFitUserProfileResponse.gender;
        locale = googleFitUserProfileResponse.locale;
        picture = googleFitUserProfileResponse.picture;
    }
}
