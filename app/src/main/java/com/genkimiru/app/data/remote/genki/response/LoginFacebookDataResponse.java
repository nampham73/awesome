package com.genkimiru.app.data.remote.genki.response;

public class LoginFacebookDataResponse {

    public int id;
    public String username;
    public String token;
    public int role_id;
    public String email;
    public String birthday;
    public int gender;
    public String avatar;
    public String first_name;
    public String last_name;
    public String nick_name;
    public String address;
    public String code;
    public String code_life_time;
    public String mailchimp_key;
    public boolean health_flag;
    public boolean profile_flag;
    public boolean announcement_flag;
    public boolean advices_flag;
    public boolean contest_notifications_flag;
    public String latitude;
    public String longitude;
    public String total_reward_point;
    public String used_reward_point;
    public String genky_mailchimp_subscriber_hash;
    public String google_id;
    public String facebook_id;
    public String created_at;
    public String updated_at;
    public boolean email_marketing_setting;
    public boolean enabled;
    public int health_source;
    public String refresh_token;

}
