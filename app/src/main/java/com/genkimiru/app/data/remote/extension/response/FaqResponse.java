package com.genkimiru.app.data.remote.extension.response;

import com.genkimiru.app.data.remote.extension.response.ArticleResponse;
import com.genkimiru.app.data.remote.genki.response.ResponseResult;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FaqResponse extends ResponseResult {

    public List<ArticleResponse> articles;
}
