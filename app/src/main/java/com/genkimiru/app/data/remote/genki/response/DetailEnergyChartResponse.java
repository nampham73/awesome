package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class DetailEnergyChartResponse extends DetailDataChartResponse {
    public List<EnergyResponse> data;
}
