package com.genkimiru.app.data.remote.genki.response;

public class ContentModelResponse {
    public String content_article;
    public boolean enabled;
    public boolean status_like;
    public int comment_numbers;
    public String program_name;
    public int share_numbers;
    public int type;
    public String url;
    public int id;
    public String picture;
    public int like_numbers;
    public String first_name;
    public String title;
    public String username;
    public String description;
    public String last_name;
    public String created_at;
    public boolean is_created_by_admin;
}
