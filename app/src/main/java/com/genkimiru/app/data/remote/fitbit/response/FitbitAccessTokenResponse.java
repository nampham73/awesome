package com.genkimiru.app.data.remote.fitbit.response;

public class FitbitAccessTokenResponse {

    public String access_token;
    public int expires_in;
    public String refresh_token;
    public String scope;
    public String token_type;
    public String user_id;
}
