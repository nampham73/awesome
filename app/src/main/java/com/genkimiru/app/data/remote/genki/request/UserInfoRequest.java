package com.genkimiru.app.data.remote.genki.request;

import android.support.annotation.NonNull;

import com.genkimiru.app.Constants;

import java.io.File;

public class UserInfoRequest {
    private String email;
    private String first_name;
    private String last_name;
    private String gobe_username;
    private String gobe_password;
    private String token;
    private Integer health_source;
    private String fitbit_access_token;
    private String fitbit_refresh_token;
    private String birthday;
    private String bloodPressure;
    private Integer blood_pressure_under;
    private Integer blood_pressure_upper;
    private String blood_type;

    public String getGobe_username() {
        return gobe_username;
    }

    public void setGobe_username(String gobe_username) {
        this.gobe_username = gobe_username;
    }

    public String getGobe_password() {
        return gobe_password;
    }

    public void setGobe_password(String gobe_password) {
        this.gobe_password = gobe_password;
    }

    public String getFitbit_access_token() {
        return fitbit_access_token;
    }

    public void setFitbit_access_token(String fitbit_access_token) {
        this.fitbit_access_token = fitbit_access_token;
    }

    public Integer getFeel_condition() {
        return feel_condition;
    }

    public void setFeel_condition(Integer feel_condition) {
        this.feel_condition = feel_condition;
    }

    public Integer getHeight_type() {
        return height_type;
    }

    public void setHeight_type(Integer height_type) {
        this.height_type = height_type;
    }

    public Integer getSurgery_status() {
        return surgery_status;
    }

    public void setSurgery_status(Integer surgery_status) {
        this.surgery_status = surgery_status;
    }

    public Integer getSmoking_level() {
        return smoking_level;
    }

    public void setSmoking_level(Integer smoking_level) {
        this.smoking_level = smoking_level;
    }

    public Integer getSmoking_each_time() {
        return smoking_each_time;
    }

    public void setSmoking_each_time(Integer smoking_each_time) {
        this.smoking_each_time = smoking_each_time;
    }

    public Integer getWeight_type() {
        return weight_type;
    }

    public void setWeight_type(Integer weight_type) {
        this.weight_type = weight_type;
    }

    public Integer getAlcohol_level() {
        return alcohol_level;
    }

    public void setAlcohol_level(Integer alcohol_level) {
        this.alcohol_level = alcohol_level;
    }

    public Integer getDrink_each_time() {
        return drink_each_time;
    }

    public void setDrink_each_time(Integer drink_each_time) {
        this.drink_each_time = drink_each_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    private Integer gender;
    private Integer feel_condition;
    private Float height;
    private Integer height_type;
    private Integer surgery_status;
    private Integer smoking_level;
    private Integer smoking_each_time;
    private Float weight;
    private Integer weight_type;
    private Integer alcohol_level;
    private Integer drink_each_time;
    private String nick_name;
    private Double latitude;
    private Double longitude;
    private String address;
    public transient File avatar;

    public UserInfoRequest(String token, String email, String firstName, String lastName, Integer healthSource) {
        this.email = email;
        this.first_name = firstName;
        this.last_name = lastName;
        this.token = token;
        this.health_source = healthSource;
    }

    public UserInfoRequest(String token, Integer healthSource) {
        this.token = token;
        this.health_source = healthSource;
    }

    public String getNickname() {
        return nick_name;
    }

    public void setNickname(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getDrinkEachTime() {
        return drink_each_time;
    }

    public void setDrinkEachTime(Integer drink_each_time) {
        this.drink_each_time = drink_each_time;
    }

    public File getAvatar() {
        return avatar;
    }

    public void setAvatar(File avatar) {
        this.avatar = avatar;
    }

    public Integer getAlcoholLevel() {
        return alcohol_level;
    }

    public void setAlcoholLevel(Integer alcohol_level) {
        this.alcohol_level = alcohol_level;
    }

    public void setNickName(String nick_name) {
        this.nick_name = nick_name;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getWeightType() {
        return weight_type;
    }

    public void setWeightType(Integer weight_type) {
        this.weight_type = weight_type;
    }

    public Integer getSmokingLevel() {
        return smoking_level;
    }

    public void setSmokingLevel(Integer smoking_level) {
        this.smoking_level = smoking_level;
    }

    public Integer getSmokingeEachTime() {
        return smoking_each_time;
    }

    public void setSmokingEachTime(Integer smoking_each_time) {
        this.smoking_each_time = smoking_each_time;
    }

    public Integer getSurgeryStatus() {
        return surgery_status;
    }

    public void setSurgeryStatus(Integer surgery_status) {
        this.surgery_status = surgery_status;
    }

    public Integer getHeightType() {
        return height_type;
    }

    public void setHeightType(Integer height_type) {
        this.height_type = height_type;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Integer getFeelcondition() {
        return feel_condition;
    }

    public void setFeelcondition(Integer feel_condition) {
        this.feel_condition = feel_condition;
    }

    public void setGobeUsername(String gobe_username) {
        this.gobe_username = gobe_username;
    }

    public void setGobePassword(String gobe_password) {
        this.gobe_password = gobe_password;
    }

    public Integer getHealthSource() {
        return health_source;
    }

    public void setFitbitAccessToken(String fitbitAccessToken) {
        this.fitbit_access_token = fitbitAccessToken;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBlood_type() {
        return blood_type;
    }

    public void setBlood_type(String blood_type) {
        this.blood_type = blood_type;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public Integer getBlood_pressure_under() {
        return blood_pressure_under;
    }

    public void setBlood_pressure_under(Integer blood_pressure_under) {
        this.blood_pressure_under = blood_pressure_under;
    }

    public Integer getBlood_pressure_upper() {
        return blood_pressure_upper;
    }

    public void setBlood_pressure_upper(Integer blood_pressure_upper) {
        this.blood_pressure_upper = blood_pressure_upper;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setFirstname(String first_name) {
        this.first_name = first_name;
    }

    public void setLastname(String last_name) {
        this.last_name = last_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getHealth_source() {
        return health_source;
    }

    public void setHealth_source(Integer health_source) {
        this.health_source = health_source;
    }

    public void setFitbit_refresh_token(String fitbit_refresh_token) {
        this.fitbit_refresh_token = fitbit_refresh_token;
    }

    public String getFitbit_refresh_token() {
        return fitbit_refresh_token;
    }

    public static class Builder {
        private String email;
        private String firstNname;
        private String lastNname;
        private String token;
        private Integer healthSource;
        private String gobeUSername;
        private String gobePassword;
        private String fitbitAccessToken;
        private String fitbitRefreshToken;
        private String birthday;
        private String bloodPressure;
        private Integer bloodPressureUnder;
        private Integer bloodPressureUpper;
        private String bloodType;
        private Integer gender;
        private Integer feelCondition;
        private Float height;
        private Integer heightType;
        private Integer surgeryStatus;
        private Integer smokingLevel;
        private Integer smokingEachTime;
        private Float weight;
        private Integer weightType;
        private Integer alcoholLevel;
        private Integer drinkEachTime;
        private String username;
        private double latitude;
        private double longtitude;
        private String address;
        private String nickname;
        public transient File avatar;

        public Builder(String token, String email, String firstName, String lastName, Integer healthSource) {
            this.email = email;
            this.firstNname = firstName;
            this.lastNname = lastName;
            this.token = token;
            this.healthSource = healthSource;
        }

        public Builder setGoBe(@NonNull String username, @NonNull String password) {
            this.gobeUSername = username;
            this.gobePassword = password;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setAvatar(File avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder setLatitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongtitude(double longtitude) {
            this.longtitude = longtitude;
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setNickname(String nickname){
            this.nickname = nickname;
            return this;
        }

        public Builder setDrinkEachTime(Integer drinkEachTime) {
            this.drinkEachTime = drinkEachTime;
            return this;
        }

        public Builder setAlcohol(Integer alcoholLevel) {
            this.alcoholLevel = alcoholLevel;
            return this;
        }

        public Builder setWeight(Float weight) {
            this.weight = weight;
            return this;
        }

        public Builder setWeightType(Integer weightType) {
            this.weightType = weightType;
            return this;
        }

        public Builder setSmokingLevel(Integer smokingLevel) {
            this.smokingLevel = smokingLevel;
            return this;
        }

        public Builder setSmokingEachTime(Integer smokingEachTime) {
            this.smokingEachTime = smokingEachTime;
            return this;
        }

        public Builder setSurgeryStatus(Integer surgeryStatus) {
            this.surgeryStatus = surgeryStatus;
            return this;
        }

        public Builder setHeightType(Integer heightType) {
            this.heightType = heightType;
            return this;
        }

        public Builder setHeight(Float height) {
            this.height = height;
            return this;
        }

        public Builder setFeelCondition(Integer feelCondition) {
            this.feelCondition = feelCondition;
            return this;
        }

        public Builder setBirthday(String birthday) {
            this.birthday = birthday;
            return this;
        }

        public Builder setGender(Integer gender) {
            this.gender = gender;
            return this;
        }

        public Builder setBloodPressure(String bloodPressure) {
            this.bloodPressure = bloodPressure;
            return this;
        }

        public Builder setBloodPressureUnder(Integer bloodPressureUnder) {
            this.bloodPressureUnder = bloodPressureUnder;
            return this;
        }

        public Builder setBloodPressureUpper(Integer bloodPressureUpper) {
            this.bloodPressureUpper = bloodPressureUpper;
            return this;
        }

        public Builder setBloodType(String bloodType) {
            this.bloodType = bloodType;
            return this;
        }

        public Builder setFitBitAccessToken(@NonNull String accessToken) {
            this.fitbitAccessToken = accessToken;
            return this;
        }

        public Builder setFitBitRefreshToken(@NonNull String refreshToken) {
            this.fitbitRefreshToken = refreshToken;
            return this;
        }


        public UserInfoRequest create() {
            UserInfoRequest request = new UserInfoRequest(token, email, firstNname, lastNname, healthSource);
            switch (healthSource) {
                case Constants.HealthSource.GOBE:
                    request.setGobeUsername(gobeUSername);
                    request.setGobePassword(gobePassword);
                    break;
                case Constants.HealthSource.FITBIT:
                    request.setFitbitAccessToken(fitbitAccessToken);
                    request.setFitbit_refresh_token(fitbitRefreshToken);
                    break;
            }
            request.setBirthday(birthday);
            request.setBloodPressure(bloodPressure);
            request.setBlood_type(bloodType);
            request.setGender(gender);
            request.setBlood_pressure_under(bloodPressureUnder);
            request.setBlood_pressure_upper(bloodPressureUpper);
            request.setFeelcondition(feelCondition);
            request.setHeight(height);
            request.setHeightType(heightType);
            request.setSurgeryStatus(surgeryStatus);
            request.setSmokingLevel(smokingLevel);
            request.setSmokingEachTime(smokingEachTime);
            request.setWeight(weight);
            request.setWeightType(weightType);
            request.setAlcoholLevel(alcoholLevel);
            request.setDrinkEachTime(drinkEachTime);
            request.setNickName(nickname);
            request.setHeightType(heightType);
            request.setWeightType(weightType);
            request.setLongitude(longtitude);
            request.setLatitude(latitude);
            request.setAddress(address);
            request.setAvatar(avatar);

            return request;
        }
    }
}
