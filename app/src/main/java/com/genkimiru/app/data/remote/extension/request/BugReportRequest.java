package com.genkimiru.app.data.remote.extension.request;

import java.util.List;

import timber.log.Timber;

public class BugReportRequest {

    public Ticket ticket;

    public static class Ticket {
        public String subject;
        public String priority;
        public String status = "open";
        public Comment comment;
        public List<CustomField> custom_fields;
    }

    public static class Comment {
        public String body;
        public List<String> uploads;

        public Comment(String body, List<String> uploads) {
            this.body = body;
            this.uploads = uploads;
        }
    }

    public static class CustomField {
        public String id;
        public String value;

        public CustomField(String id, String value) {
            this.id = id;
            this.value = value;
        }
    }

    public BugReportRequest(Ticket ticket) {
        this.ticket = ticket;
    }

    public enum CustomFieldType {

        USER_ID("360004389492"),
        CREATE_DATE("360004421311"),
        DEVICE("360004425931");

        public String id;

        CustomFieldType(String id) {
            this.id = id;
        }
    }
}
