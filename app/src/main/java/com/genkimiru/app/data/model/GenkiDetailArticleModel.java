package com.genkimiru.app.data.model;

import com.genkimiru.app.Constants;
import com.genkimiru.app.data.remote.genki.response.ContentModelResponse;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;

public class GenkiDetailArticleModel implements IDetailContentModel {
    private String mContentArticle;
    private boolean mEnabled;
    private boolean mStatusLike;
    private int mCommentNumbers;
    private String mProgramName;
    private int mShareNumbers;
    private int mType;
    private String mUrl;
    private int mId;
    private String mPicture;
    private int mLikeNumbers;
    private String mFirstName;
    private String mTitle;
    private String mUsername;
    private String mDescription;
    private String mLastName;
    private String mCreatedAt;
    private boolean isAdmin;

    public void setValueFrom(ContentModelResponse response) {
        if (response == null) return;
        mId = response.id;
        mContentArticle = response.content_article;
        mEnabled = response.enabled;
        mStatusLike = response.status_like;
        mCommentNumbers = response.comment_numbers;
        mProgramName = response.program_name;
        mShareNumbers = response.share_numbers;
        mType = response.type;
        isAdmin = response.is_created_by_admin;
        mUrl = response.url;
        mPicture = response.picture;
        mLikeNumbers = response.like_numbers;
        mFirstName = response.first_name;
        mTitle = response.title;
        mUsername = response.username;
        mDescription = response.description;
        mLastName = response.last_name;
        mCreatedAt = response.created_at;
    }


    @Override
    public int getId() {
        return mId;
    }

    @Override
    public String getName() {
        return mTitle;
    }

    @Override
    public int getViewType() {
        return Constants.RecyclerViewType.DETAIL_ARTICLE_VIEW_TYPE;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getContentArticle() {
        return mContentArticle;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public boolean getStatusLike() {
        return mStatusLike;
    }

    public void setStatusLike(boolean mStatusLike) {
        this.mStatusLike = mStatusLike;
    }

    public void setLikeNumbers(int likeNumbers) {
        this.mLikeNumbers = likeNumbers;
    }

    public int getLikeNumbers() {
        return mLikeNumbers;
    }

    public int getCommentNumbers() {
        return mCommentNumbers;
    }

    public void setCommentNumbers(int mCommentNumbers) {
        this.mCommentNumbers = mCommentNumbers;
    }

    public int getShareNumbers() {
        return mShareNumbers;
    }

    public void setShareNumbers(int mShareNumbers) {
        this.mShareNumbers = mShareNumbers;
    }

    public String getOwner() {
        return mFirstName + " " + mLastName;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getProgramName() {
        return mProgramName;
    }

    public void setProgramName(String mProgramName) {
        this.mProgramName = mProgramName;
    }
}
