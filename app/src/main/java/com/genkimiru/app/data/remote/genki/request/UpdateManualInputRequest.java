package com.genkimiru.app.data.remote.genki.request;



public class UpdateManualInputRequest {
    public int id;
    public int data_type;
    public ManualData data;

    public UpdateManualInputRequest(int id, int data_type, ManualData data) {
        this.id = id;
        this.data_type = data_type;
        this.data = data;
    }

    public static class ManualData {
        public float kcal_in;
        public float protein_in;
        public float fat_in;
        public float carbonhydrate_in;
        public String food_name;

        public ManualData(float kcal_in, float protein_in, float fat_in, float carbonhydrate_in, String food_name) {
            this.kcal_in = kcal_in;
            this.protein_in = protein_in;
            this.fat_in = fat_in;
            this.carbonhydrate_in = carbonhydrate_in;
            this.food_name = food_name;
        }
    }

    public @interface DataType {
        int SLEEP = 1;
        int STEP = 2;
        int HEART = 3;
        int ENERGY = 4;
        int WEIGHT_BMI = 5;
        int NUTRI = 6;
    }
}
