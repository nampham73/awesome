package com.genkimiru.app.data.remote.genki;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.data.remote.genki.request.ContentDetailRequest;
import com.genkimiru.app.data.remote.genki.request.ActivePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.ChangePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.DeleteManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.HealthSourceSettingRequest;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.LoginFacebookRequest;
import com.genkimiru.app.data.remote.genki.request.LoginRequest;
import com.genkimiru.app.data.remote.genki.request.LoginWithGoogleRequest;
import com.genkimiru.app.data.remote.genki.request.NotificationSettingRequest;
import com.genkimiru.app.data.remote.genki.request.PrivacySettingRequest;
import com.genkimiru.app.data.remote.genki.request.RecoverPasswordRequest;
import com.genkimiru.app.data.remote.genki.request.SettingEmailMarketingRequest;
import com.genkimiru.app.data.remote.genki.request.UpdateManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.remote.genki.response.ResponseResult;

import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface GenkiApi {

    /*-------- Login And Register -------*/
    @POST("login")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> login(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body LoginRequest request
    );

    @POST("user/register")
    @Multipart
    Single<ResponseResult> register(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Part("username") RequestBody username,
            @Part("password") RequestBody password,
            @Part("email") RequestBody email,
            @Part("birthday") RequestBody birthday,
            @Part("gender") RequestBody gender,
            @Part MultipartBody.Part imageFile,
            @Part("first_name") RequestBody firstname,
            @Part("last_name") RequestBody lastname,
            @Part("height") RequestBody height,
            @Part("height_type") RequestBody heightType,
            @Part("weight") RequestBody weight,
            @Part("weight_type") RequestBody weightType,
//            @Part("blood_pressure") RequestBody bloodPressure,
            @Part("blood_type") RequestBody bloodType,
            @Part("surgery_status") RequestBody surgeryStatus,
            @Part("feel_condition") RequestBody feelCondition,
            @Part("smoking_level") RequestBody smokingLevel,
            @Part("alcohol_level") RequestBody alcoholLevel,
            @Part("gobe_username") RequestBody gobeUsername,
            @Part("gobe_password") RequestBody gobePassword,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("nick_name") RequestBody nick_name,
            @Part("address") RequestBody address,
            @Part("smoking_each_time") RequestBody smokingEachTime,
            @Part("drink_each_time") RequestBody drinkEachTime,
            @Part("health_source") RequestBody healthSource,
            @Part("fitbit_access_token") RequestBody fitbitAccessToken,
            @Part("fitbit_refresh_token") RequestBody fitbitRefreshToken,
            @Part("fitbit_expires_in") RequestBody fitbitExpiresIn,
            @Part("blood_pressure_under") RequestBody bloodPressureUnder,
            @Part("blood_pressure_upper") RequestBody bloodPressureUpper
    );

    @POST("user/forgot_password")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> recoverPassword(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body RecoverPasswordRequest request
    );

    @POST("user/active_password")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> activePassword(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body ActivePasswordRequest request
    );

    @POST("user/privacy/update")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> updatePrivacySetting(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body PrivacySettingRequest request
    );

    @POST("user/password")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> changePassword(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body ChangePasswordRequest request
    );

    @POST("user/email/subscribe")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> updateEmailMarketingSetting(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body SettingEmailMarketingRequest request
    );

    @GET("user/detail")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> getUserInformation(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Query("token") String token);

    @GET
    @Streaming
    Single<ResponseBody> downloadImage(@Url String fileUrl);

    @POST("user/notification/update")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> updateNotificationSetting(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body NotificationSettingRequest request
    );

    /*-------- Contest -------*/

    @GET("contest/list")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingContests(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                            @QueryMap Map<String, String> request);

    @GET("/api/v1/contest/{id}")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingContestDetail(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                 @Path("id") int id,
                                                 @Query("token") String token);

    @GET("contest/history/list")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingContestHistories(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                    @QueryMap Map<String, String> request);

    @GET("contest/user/list")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingContestCurrentUser(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                      @QueryMap Map<String, String> request);

    @GET("contest/user/recommend")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingContestRecommend(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                    @QueryMap Map<String, String> request);

    @POST("/api/v1/contest/user/join")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> joinContest(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                       @Body JoinRequest request);

    @POST("/api/v1/user_contest_challenge/start_stop")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> processChallenge(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                       @Body ChallengeRequest request);

    @GET("content/list")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingContents(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                            @QueryMap Map<String, String> request);

    @GET("favorite/article/list")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingFavoriteContents(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                    @QueryMap Map<String, String> request);

    @POST("user/setting/health_source/update")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> updateHealthSourceSetting(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                     @Body HealthSourceSettingRequest request);

    @POST("user/update")
    Single<ResponseResult> updateUser(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                      @Body UserInfoRequest request);

    @GET("article/detail")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingDetailArticle(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                                 @QueryMap Map<String, String> request);

    @GET("comment/user/list")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> fetchingComments(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                            @QueryMap Map<String, String> request);

    @POST("favorite/article/add")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> requestLike(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                       @Body ContentDetailRequest request);

    @POST("favourite/article/user/delete")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> requestUnLike(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                         @Body ContentDetailRequest request);

    @POST("comment/user/add")
    @Headers({"Content-Type:application/json"})
    Single<ResponseResult> requestAddComment(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                                             @Body ContentDetailRequest request);

    @POST("login_facebook")
    Single<ResponseResult> loginFacebook(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                         @Body LoginFacebookRequest request);

    @POST("user/my_data/manual/import")
    Single<ResponseResult> addManualInput(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                          @Header("timezone") String timeZone,
                                          @Body AddManualInputRequest request);

    @POST("login_google")
    Single<ResponseResult> loginGoolePlus(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                          @Body LoginWithGoogleRequest request);

    @GET("user/my_data/dashboard")
    Single<ResponseResult> fetchingDashboard(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                             @Header("timezone") String timeZone,
                                             @QueryMap Map<String, String> map);

    @GET("user/my_data")
    Single<ResponseResult> fetchingDetailMyChart(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                                 @Header("timezone") String timeZone,
                                                 @QueryMap Map<String, String> map);
    @POST("user/my_data/manual/delete")
    Single<ResponseResult> deleteManualInput(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                             @Header("timezone") String timeZone,
                                             @Query("token") String token,
                                             @Body DeleteManualInputRequest request);
    @POST("user/my_data/manual/update")
    Single<ResponseResult> updateManualInput(@Header(BuildConfig.HEADER_SERVER_KEY) String appkey,
                                             @Header("timezone") String timeZone,
                                             @Query("token") String token,
                                             @Body UpdateManualInputRequest request);

    @POST("user/update")
    @Multipart
    Single<ResponseResult> updateAvatar(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Part("token") RequestBody token,
            @Part("email") RequestBody email,
            @Part("birthday") RequestBody birthday,
            @Part("gender") RequestBody gender,
            @Part MultipartBody.Part imageFile,
            @Part("first_name") RequestBody firstname,
            @Part("last_name") RequestBody lastname,
            @Part("height") RequestBody height,
            @Part("weight") RequestBody weight,
            @Part("height_type") RequestBody heightType,
            @Part("weight_type") RequestBody weightType,
            @Part("surgery_status") RequestBody surgeryStatus,
            @Part("feel_condition") RequestBody feelCondition,
            @Part("smoking_level") RequestBody smokingLevel,
            @Part("alcohol_level") RequestBody alcoholLevel,
            @Part("blood_type") RequestBody bloodType,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("gobe_username") RequestBody gobeUsername,
            @Part("gobe_password") RequestBody gobePassword,
            @Part("nick_name") RequestBody nick_name,
            @Part("address") RequestBody address,
            @Part("smoking_each_time") RequestBody smokingEachTime,
            @Part("drink_each_time") RequestBody drinkEachTime,
            @Part("fitbit_access_token") RequestBody fitbitAccessToken,
            @Part("fitbit_refresh_token") RequestBody fitbitRefreshToken,
            @Part("health_source") RequestBody healthSource,
            @Part("blood_pressure_under") RequestBody bloodPressureUnder,
            @Part("blood_pressure_upper") RequestBody bloodPressureUpper
            );
}
