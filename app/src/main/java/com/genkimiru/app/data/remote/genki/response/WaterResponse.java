package com.genkimiru.app.data.remote.genki.response;

public class WaterResponse {
    public long timestamp;
    public float water_intake;
    public float enough_water_percent;
    public float not_enough_water_percent;
    public float second;
    public float total_enough_water_time;
    public float total_not_enough_water_time;
}
