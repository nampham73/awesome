package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.ListContentResponse;

import java.io.Serializable;

public class GenkiContentModel implements Serializable {

    private int id;
    private String title;
    private String picture;
    private String url;
    private String description;
    private int contestId;
    private int type;
    private int programId;
    private String program_name;
    private String createdAt;
    private String updatedAt;
    private boolean enabled;
    private String owner;
    private boolean isCreatedByAdmin;

    public @interface ContentType {
        int ARTICLE  = 1;
        int ADVERTISEMENT = 2;
    }

    public void setValueFromResponse(ListContentResponse.Content content) {
        this.id = content.id;
        this.title = content.title;
        this.picture = content.picture;
        this.url = content.url;
        this.description = content.description;
        this.contestId = content.contest_id;
        this.type = content.type;
        this.programId = content.program_id;
        this.program_name = content.program_name;
        this.createdAt = content.created_at;
        this.updatedAt = content.updated_at;
        this.updatedAt = content.updated_at;
        this.enabled = content.enabled;
        this.owner = content.username;
        this.isCreatedByAdmin = content.is_created_by_admin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getProgram_name() {
        return program_name;
    }

    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isCreatedByAdmin() {
        return isCreatedByAdmin;
    }

    public void setCreatedByAdmin(boolean createdByAdmin) {
        isCreatedByAdmin = createdByAdmin;
    }
}
