package com.genkimiru.app.data.model;

import java.io.Serializable;
import java.util.List;

public class GenkiChallengeModel implements Serializable {

    private int id;
    private String title;
    private String description;
    private int type;
    private String thumbnail_url;
    private int target;
    private int contest_id;
    private String created_at;
    private String updated_at;
    private int num_winner;
    private List<String> winner_avatars;
    private float percentChallenge;
    private int sumChallengeComplete;
    private int challengeStatus;
    private int userContestChallengeId;

    public int getChallengeStatus() {
        return challengeStatus;
    }

    public void setChallengeStatus(int challengeStatus) {
        this.challengeStatus = challengeStatus;
    }

    public int getSumChallengeComplete() {
        return sumChallengeComplete;
    }

    public void setSumChallengeComplete(int sumChallengeComplete) {
        this.sumChallengeComplete = sumChallengeComplete;
    }

    public float getPercentChallenge() {
        return percentChallenge;
    }

    public void setPercentChallenge(float percentChallenge) {
        this.percentChallenge = percentChallenge;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getContest_id() {
        return contest_id;
    }

    public void setContest_id(int contest_id) {
        this.contest_id = contest_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getNum_winner() {
        return num_winner;
    }

    public void setNum_winner(int num_winner) {
        this.num_winner = num_winner;
    }

    public List<String> getWinner_avatars() {
        return winner_avatars;
    }

    public void setWinner_avatars(List<String> winner_avatars) {
        this.winner_avatars = winner_avatars;
    }

    public int getUserContestChallengeId() {
        return userContestChallengeId;
    }

    public void setUserContestChallengeId(int userContestChallengeId) {
        this.userContestChallengeId = userContestChallengeId;
    }


    public @interface ChallengeType {
        int WALKING = 1;
        int RUNNING = 2;
        int CYCLING = 3;
    }
}
