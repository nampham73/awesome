package com.genkimiru.app.data.remote.genki.response;

public class SleepResponse {
    public long timestamp;
    public float sleep_event;
    public float second;
    public float total_sleep_time;
    public long average_start_time;
    public long average_end_time;
    public float average_sleep_time;
    public float average_deep_time;
    public float average_light_time;
    public float average_rem_time;
    public float average_wake_time;

}
