package com.genkimiru.app.data.remote.genki.response;

public class HeartResponse {
    public long timestamp;
    public float heart_rate;
    public float min;
    public float max;
    public float second;

}
