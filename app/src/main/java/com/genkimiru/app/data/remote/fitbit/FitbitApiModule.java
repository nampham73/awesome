package com.genkimiru.app.data.remote.fitbit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class FitbitApiModule {

    @Provides
    @Singleton
    FitbitApi provideFitbitApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        FitbitRestApi fitbitRestApi = new FitbitRestApi(client, gsonConverter, rxAdapter);
        return fitbitRestApi.getApi();
    }
}
