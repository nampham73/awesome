package com.genkimiru.app.data.remote.gobe.response;

public class GobeErrorResponse {
    public int[] response_code;
    public long timestamp;
    public int request;
}
