package com.genkimiru.app.data.remote.genki.response;

public class EnergyResponse {
    public long timestamp;
    public float kcal_in;
    public float kcal_out;
    public float second;
    public float average_kcal_in;
    public float average_kcal_out;
}
