package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class ListContestResponse {
    public List<ContestModelResponse> contests;
}
