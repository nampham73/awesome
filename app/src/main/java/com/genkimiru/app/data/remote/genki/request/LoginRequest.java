package com.genkimiru.app.data.remote.genki.request;

public class LoginRequest {

    public String username;
    public String email;
    public String password;
    public String type;

    public LoginRequest(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.type = "app";
    }
}
