package com.genkimiru.app.data.remote.genki.response;

public class BmiResponse {
    public long timestamp;
    public float kcal_in;
    public float fat_in;
    public float second;
    public float bmi;
    public float weight;
    public float carbonhydrate_in;
    public float average_kcal_in;
    public float protein_in;
}
