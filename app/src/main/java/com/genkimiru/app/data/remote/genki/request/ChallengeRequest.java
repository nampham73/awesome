package com.genkimiru.app.data.remote.genki.request;

import android.location.Location;

public class ChallengeRequest {
    public int user_contest_challenge_id;
    public String start_stop_flag;
    public double check_in_latitude;
    public double check_in_longitude;
    public String token;

    public ChallengeRequest(int user_contest_challenge_id, String start_stop_flag, Location location, String token) {
        this.user_contest_challenge_id = user_contest_challenge_id;
        this.start_stop_flag = start_stop_flag;
        check_in_latitude = location.getLatitude();
        check_in_longitude = location.getLongitude();
        this.token = token;
    }
}
