package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class NutriResponse {

    public long timestamp;
    public float kcal_in;
    public float fat_in;
    public float protein_in;
    public float carbonhydrate_in;
    public float second;
    public float average_kcal_in;
    public List<Manual> manual_list;

    public class Manual {
        public int id;
        public String food_name;
        public float kcal_in;
        public float fat_in;
        public float protein_in;
        public float carbonhydrate_in;
        public long start_time;
        public long end_time;
    }
}
