package com.genkimiru.app.data.repository.contest;

import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.ListContestRequest;
import com.genkimiru.app.data.remote.genki.response.ContestDetailResponse;
import com.genkimiru.app.data.remote.genki.response.ContestModelResponse;

import java.util.List;

import io.reactivex.Flowable;

public interface ContestRepository {
    Flowable<List<ContestModelResponse>> fetchingContestList(ListContestRequest request);
    Flowable<ContestDetailResponse> fetchingContestDetail(int id, String token);
    Flowable<List<ContestModelResponse>> fetchingContestPrivateOrPublic(ListContestRequest request);
    Flowable<List<ContestModelResponse>> fetchingContestHistories(ListContestRequest request);
    Flowable<List<ContestModelResponse>> fetchingContestCurrentUser(ListContestRequest request);
    Flowable<List<ContestModelResponse>> fetchingContestRecommend(ListContestRequest request);
    Flowable<Boolean> joinContest(JoinRequest request);
}
