package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.WaterResponse;

public class GenkiWaterModel {
    private long timestamp;
    private float water_intake;
    private float enough_water_percent;
    private float not_enough_water_percent;
    private float second;
    private float total_enough_water_time;
    private float total_not_enough_water_time;


    public GenkiWaterModel(long timestamp,
                           float water_intake,
                           float enough_water_percent,
                           float not_enough_water_percent,
                           float second,
                           float total_enough_water_time,
                           float total_not_enough_water_time) {
        this.timestamp = timestamp;
        this.water_intake = water_intake;
        this.enough_water_percent = enough_water_percent;
        this.not_enough_water_percent = not_enough_water_percent;
        this.second = second;
        this.total_enough_water_time = total_enough_water_time;
        this.total_not_enough_water_time = total_not_enough_water_time;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getWaterIntake() {
        return water_intake;
    }

    public float getEnoughWaterPercent() {
        return enough_water_percent;
    }

    public float getNotEnoughWaterPercent() {
        return not_enough_water_percent;
    }

    public float getSecond() {
        return second;
    }

    public float getTotalEnoughWaterTime() {
        return total_enough_water_time;
    }

    public float getTotalNotEnoughWaterTime() {
        return total_not_enough_water_time;
    }

    public static GenkiWaterModel convert(WaterResponse response) {
        return new GenkiWaterModel(response.timestamp,
                response.water_intake,
                response.enough_water_percent,
                response.not_enough_water_percent,
                response.second,
                response.total_enough_water_time,
                response.total_not_enough_water_time);
    }
}
