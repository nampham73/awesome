package com.genkimiru.app.data.repository.content;

import com.genkimiru.app.data.model.GenkiContentModel;
import com.genkimiru.app.data.remote.genki.request.ContentDetailRequest;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;
import com.genkimiru.app.data.remote.genki.request.ListContentRequest;
import com.genkimiru.app.data.remote.genki.response.ContentModelResponse;
import com.genkimiru.app.data.remote.genki.response.FeedModelResponse;

import java.util.List;

import io.reactivex.Flowable;

public interface ContentRepository {
    Flowable<List<GenkiContentModel>> fetchingContents(ListContentRequest listContentRequest);
    Flowable<ContentModelResponse> fetchingDetailContent(ContentDetailRequest map);
    Flowable<List<FeedModelResponse>> fetchingFeeds(ContentDetailRequest map);
    Flowable<List<IDetailContentModel>> getArticleAndFeeds(ContentDetailRequest article, ContentDetailRequest feeds);
    Flowable<Boolean> requestLike(ContentDetailRequest likes);
    Flowable<Boolean> requestUnLike(ContentDetailRequest likes);
    Flowable<Boolean> requestAddComment(ContentDetailRequest comment);
    Flowable<List<GenkiContentModel>> fetchingFavoriteContents(ListContentRequest listContentRequest);
}
