package com.genkimiru.app.data.remote.genki.request;

public class ContentDetailRequest {

    private Integer article_id;
    private Integer id;
    private String type;
    private String token;
    private String comment;
    private Integer limit;
    private Integer offset;
    private String sort_name;
    private String sort_type;

    public ContentDetailRequest(Integer article_id,
                                Integer id,
                                String type,
                                String token,
                                String comment,
                                Integer limit,
                                Integer offset,
                                String sortName,
                                String sort) {
        this.article_id = article_id;
        this.id = id;
        this.type = type;
        this.token = token;
        this.comment = comment;
        this.limit = limit;
        this.offset = offset;
        this.sort_name = sortName;
        this.sort_type = sort;
    }


    public static class Builder {
        private Integer article_id;
        private Integer id;
        private String type;
        private String token;
        private String comment;
        private Integer limit;
        private Integer offset;
        private String sortName = "created_at";
        private String sortType;

        public Builder(String token) {
            this.token = token;
        }

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setArticleId(int id) {
            this.article_id = id;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setComment(String comment) {
            this.comment = comment;
            return this;
        }

        public Builder setLimit(Integer limit) {
            this.limit = limit;
            return this;
        }

        public Builder setOffset(Integer offset) {
            this.offset = offset;
            return this;
        }

        public Builder setSortName(String sortName) {
            this.sortName = sortName;
            return this;
        }

        public Builder setSortType(String sortType){
            this.sortType = sortType;
            return this;
        }

        public ContentDetailRequest create() {
            return new ContentDetailRequest(article_id,
                    id,
                    type,
                    token,
                    comment,
                    limit,
                    offset,
                    sortName,
                    sortType);
        }
    }

    public @interface SortType {
        String ASC = "asc";
        String DESC = "desc";
    }
}
