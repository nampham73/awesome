package com.genkimiru.app.data.repository.mydata;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.util.JsonGenerator;
import com.genkimiru.app.data.model.GenkiDashboardDataModel;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DashboardRequest;
import com.genkimiru.app.data.remote.genki.request.DeleteManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.data.remote.genki.request.UpdateManualInputRequest;
import com.genkimiru.app.data.remote.genki.response.DashboardResponse;
import com.genkimiru.app.data.remote.genki.response.DetailNutritionChartResponse;
import com.genkimiru.app.data.remote.genki.response.DetailEnergyChartResponse;
import com.genkimiru.app.data.remote.genki.response.DetailSleepChartResponse;
import com.genkimiru.app.data.remote.genki.response.DetailStepChartResponse;
import com.genkimiru.app.data.remote.genki.response.NutriResponse;
import com.genkimiru.app.data.remote.genki.response.EnergyResponse;
import com.genkimiru.app.data.remote.genki.response.SleepResponse;
import com.genkimiru.app.data.remote.genki.response.StepResponse;
import com.genkimiru.app.widget.SleepChartView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Flowable;
import io.reactivex.Single;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Dashboard.DONE;
import static com.genkimiru.app.Constants.Network.HTTP_STATUS_OK;
import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class MyDataRepositoryImp implements MyDataRepository {

    private GenkiApi mApi;
    private static final int RETRY_COUNT = 5;

    public MyDataRepositoryImp(GenkiApi api) {
        mApi = api;
    }

    @Override
    public Flowable<GenkiDashboardDataModel> fetchingDashboard(DashboardRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        String timezone = TimeZone.getDefault().getID();
        return mApi.fetchingDashboard(BuildConfig.HEADER_SERVER_KEY, timezone, map)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        DashboardResponse dashboard = responseResult.convertBodyTo(DashboardResponse.class);
                        if (dashboard != null && dashboard.status == DONE) {
                            return Single.just(GenkiDashboardDataModel.convert(dashboard.data));
                        }
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                })
                .retryWhen(throwable -> {
                    AtomicInteger counter = new AtomicInteger();
                    return throwable.takeWhile(throwable1 -> counter.getAndIncrement() != RETRY_COUNT)
                            .flatMap(throwable1 -> {
                                Timber.d("fetchingDashboard retry by %s second(s)", counter.get());
                                return Flowable.timer(counter.get(), TimeUnit.SECONDS);
                            });
                })
                .toFlowable();
    }

    @Override
    public Flowable<List<SleepChartView.Entry>> fetchingDetailSleep(DetailChartDataRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        String timezone = TimeZone.getDefault().getID();
        return mApi.fetchingDetailMyChart(BuildConfig.HEADER_SERVER_KEY, timezone, map)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        DetailSleepChartResponse response = responseResult.convertBodyTo(DetailSleepChartResponse.class);
                        if (response != null && response.status == DONE) {
                            return Single.just(convertSleepResponse(response.data));
                        }
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                })
                .retryWhen(throwable -> {
                    AtomicInteger counter = new AtomicInteger();
                    return throwable.takeWhile(throwable1 -> counter.getAndIncrement() != RETRY_COUNT)
                            .flatMap(throwable1 -> {
                                Timber.d("fetchingDetailSleep retry by %s second(s)", counter.get());
                                return Flowable.timer(counter.get(), TimeUnit.SECONDS);
                            });
                })
                .toFlowable();
    }

    @Override
    public Flowable<List<GenkiStepModel>> fetchingDetailStep(DetailChartDataRequest request) {
        String timezone = TimeZone.getDefault().getID();
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingDetailMyChart(BuildConfig.HEADER_SERVER_KEY, timezone, map)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        DetailStepChartResponse response = responseResult.convertBodyTo(DetailStepChartResponse.class);
                        if (response != null && response.status == DONE) {
                            return Single.just(convertStepResponse(response.data));
                        }
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                })
                .retryWhen(throwable -> {
                    AtomicInteger counter = new AtomicInteger();
                    return throwable.takeWhile(throwable1 -> counter.getAndIncrement() != RETRY_COUNT)
                            .flatMap(throwable1 -> {
                                Timber.d("fetchingDetailStep retry by %s second(s)", counter.get());
                                return Flowable.timer(counter.get(), TimeUnit.SECONDS);
                            });
                })
                .toFlowable();
    }

    @Override
    public Flowable<List<GenkiEnergyModel>> fetchingDetailEnergy(DetailChartDataRequest request) {
        String timezone = TimeZone.getDefault().getID();
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingDetailMyChart(BuildConfig.HEADER_SERVER_KEY, timezone, map)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        DetailEnergyChartResponse response = responseResult.convertBodyTo(DetailEnergyChartResponse.class);
                        if (response != null && response.status == DONE) {
                            return Single.just(convertEnergyResponse(response.data));
                        }
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                })
                .retryWhen(throwable -> {
                    AtomicInteger counter = new AtomicInteger();
                    return throwable.takeWhile(throwable1 -> counter.getAndIncrement() != RETRY_COUNT)
                            .flatMap(throwable1 -> {
                                Timber.d("fetchingDetailEnergy retry by %s second(s)", counter.get());
                                return Flowable.timer(counter.get(), TimeUnit.SECONDS);
                            });
                })
                .toFlowable();
    }

    @Override
    public Flowable<List<GenkiNutritionModel>> fetchingDetailNutrition(DetailChartDataRequest request) {
        String timezone = TimeZone.getDefault().getID();
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingDetailMyChart(BuildConfig.HEADER_SERVER_KEY, timezone, map)
                .flatMap(responseResult -> {
                    if (responseResult.status == HTTP_STATUS_OK) {
                        DetailNutritionChartResponse response = responseResult.convertBodyTo(DetailNutritionChartResponse.class);
                        if(response != null && response.status == DONE) {
                            return  Single.just(convertNutritionResponse(response.data));
                        }
                    }
                    return new GenkiApiErrorException(responseResult).toSingleError();
                }).retryWhen(throwable -> {
                    AtomicInteger counter = new AtomicInteger();
                    return throwable.takeWhile(throwable1 -> counter.getAndIncrement() != RETRY_COUNT)
                            .flatMap(throwable1 -> {
                                Timber.d("fetchingDetailNutrition retry by %s second(s)", counter.get());
                                return Flowable.timer(counter.get(), TimeUnit.SECONDS);
                            });
                }).toFlowable();
    }

    @Override
    public Flowable<Boolean> addNutritionManualInputData(AddManualInputRequest request) {
        String timezone = TimeZone.getDefault().getID();
        return mApi.addManualInput(BuildConfig.HEADER_SERVER_KEY, timezone, request).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> deleteNutritionManualInputData(DeleteManualInputRequest request) {
        String timezone = TimeZone.getDefault().getID();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return mApi.deleteManualInput(BuildConfig.HEADER_SERVER_KEY, timezone, token, request).flatMap(responseResult -> {
            if(responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            }
            return  new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<Boolean> updateNutritionManualInputData(UpdateManualInputRequest request) {
        String timezone = TimeZone.getDefault().getID();
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        return mApi.updateManualInput(BuildConfig.HEADER_SERVER_KEY, timezone, token, request).flatMap(responseResult -> {
            if(responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    private List<SleepChartView.Entry> convertSleepResponse(List<SleepResponse> responses){
        List<SleepChartView.Entry> entries = new ArrayList<>();
        if (responses != null && responses.size() > 0) {
            for (SleepResponse datum : responses) {
                SleepChartView.Entry entry = new SleepChartView.Builder(datum.sleep_event)
                        .setValue(datum.second)
                        .build();
                entries.add(entry);
            }
        }
        return entries;
    }

    private List<GenkiStepModel> convertStepResponse(List<StepResponse> responses) {
        List<GenkiStepModel> genkiStepModelList = new ArrayList<>();
        if (responses != null && responses.size() > 0) {
            for (StepResponse response : responses) {
               GenkiStepModel model = GenkiStepModel.convert(response);
               genkiStepModelList.add(model);
            }
        }
        return genkiStepModelList;
    }

    private List<GenkiNutritionModel> convertNutritionResponse(List<NutriResponse> responses) {
        List<GenkiNutritionModel> genkiNutritionModelList = new ArrayList<>();
        if(responses != null && responses.size() > 0) {
            for (NutriResponse response : responses) {
                GenkiNutritionModel model = GenkiNutritionModel.convert(response);
                genkiNutritionModelList.add(model);
            }
        }
        return genkiNutritionModelList;
    }

    private List<GenkiEnergyModel> convertEnergyResponse(List<EnergyResponse> responses) {
        List<GenkiEnergyModel> genkiEnergyModelList = new ArrayList<>();
        if (responses != null && responses.size() > 0) {
            for (EnergyResponse response : responses) {
                GenkiEnergyModel model = GenkiEnergyModel.convert(response);
                genkiEnergyModelList.add(model);
            }
        }
        return genkiEnergyModelList;
    }
}
