package com.genkimiru.app.data.remote.google;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GoogleApiModule {

    @Provides
    @Singleton
    GoogleApi provideGoogleApi(OkHttpClient client, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxAdapter) {
        GoogleRestApi googleRestApi = new GoogleRestApi(client, gsonConverterFactory, rxAdapter);
        return googleRestApi.getApi();
    }
}
