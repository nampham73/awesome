package com.genkimiru.app.data.remote.genki.response;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.genkimiru.app.base.util.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FeedModelResponse implements Comparable<FeedModelResponse> {
    public int article_id;
    public String owner_name;
    public int status;
    public int owner_id;
    public String created_at;
    public String avatar;
    public String comment;
    public boolean enable;

    @SuppressLint("SimpleDateFormat")
    public long getTime() {
        SimpleDateFormat df = new SimpleDateFormat(DateUtil.Format.API_FORMAT);
        if (created_at != null && created_at.length() > 0) {
            try {
                return df.parse(created_at).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Override
    public int compareTo(@NonNull FeedModelResponse o) {
        if (getTime() > o.getTime()) return -1;
        else if (getTime() < o.getTime()) return 1;
        return 0;
    }
}
