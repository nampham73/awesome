package com.genkimiru.app.data.remote.genki.request;

import android.support.annotation.IntDef;
import android.support.annotation.IntRange;

public class ListContestRequest {
    public Integer id;
    public Integer type;
    public String start_date;
    public String end_date;
    public Integer scope;
    public Integer membership_type;
    public Integer bmi_from;
    public Integer bmi_to;
    public Integer age_from;
    public Integer age_to;
    public Integer gender;
    public Integer drinking_habit;
    public Integer smoking_habit;
    public Integer limit;
    public Integer offset;
    public String sort_name;
    public Integer sort_by;
    public String sort_type;
    public String token;

    @IntDef({ContestScope.PRIVATE, ContestScope.PUBLIC})
    public @interface ContestScope {
        int PUBLIC = 1;
        int PRIVATE = 2;
    }

    @IntDef({ContestSort.ASC, ContestSort.DESC, ContestSort.CREATED_AT,
            ContestSort.START_DATE, ContestSort.END_DATE})
    public @interface ContestSort {
        int ASC = 1;
        int DESC = 2;
        int CREATED_AT = 3;
        int START_DATE = 4;
        int END_DATE = 5;
    }

    public ListContestRequest() {

    }

    public ListContestRequest(
            Integer id,
            Integer type,
            String start_date,
            String end_date,
            Integer scope,
            Integer membership_type,
            Integer bmi_from,
            Integer bmi_to,
            Integer age_from,
            Integer age_to,
            Integer gender,
            Integer drinking_habit,
            Integer smoking_habit,
            int limit,
            int offset,
            Integer sort_by,
            String sort_name,
            String sort_type,
            String token) {
        this.id = id;
        this.type = type;
        this.start_date = start_date;
        this.end_date = end_date;
        this.scope = scope;
        this.membership_type = membership_type;
        this.bmi_from = bmi_from;
        this.bmi_to = bmi_to;
        this.age_from = age_from;
        this.age_to = age_to;
        this.gender = gender;
        this.drinking_habit = drinking_habit;
        this.smoking_habit = smoking_habit;
        this.limit = limit;
        this.offset = offset;
        this.sort_by = sort_by;
        this.sort_name = sort_name;
        this.sort_type = sort_type;
        this.token = token;
    }

    public static class Builder {
        private Integer id;
        private Integer type;
        private String start_date;
        private String end_date;
        private Integer scope;
        private Integer membership_type;
        private Integer bmi_from;
        private Integer bmi_to;
        private Integer age_from;
        private Integer age_to;
        private Integer gender;
        private Integer drinking_habit;
        private Integer smoking_habit;
        private int limit;
        private int offset;
        private Integer sort_by;
        private String sort_name;
        private String sort_type;
        private String token;

        public Builder(@IntRange(from = 0) int offset, @IntRange(from = 1) int limit, String sortName, String sortType, String token) {
            this.limit = limit;
            this.offset = offset;
            this.sort_name = sortName;
            this.sort_type = sortType;
            this.token = token;
        }

        public Builder(@IntRange(from = 0) int offset, @IntRange(from = 1) int limit, int sortBy, int sortType, String token) {
            this.limit = limit;
            this.offset = offset;
            this.sort_by = sortBy;
            this.sort_type = String.valueOf(sortType);
            this.token = token;
        }

        public Builder(int id, String token) {
            this.id = id;
            this.token = token;
        }

        public Builder setType(@IntRange(from = 1, to = 4) int type) {
            this.type = type;
            return this;
        }

        public Builder setStartDate(String date) {
            this.start_date = date;
            return this;
        }

        public Builder setEndDate(String date) {
            this.end_date = date;
            return this;
        }

        public Builder setScope(@IntRange(from = 1, to = 2) int scope) {
            this.scope = scope;
            return this;
        }

        public Builder setMemberShipType(@IntRange(from = 1, to = 3) int memberShipType) {
            this.membership_type = memberShipType;
            return this;
        }

        public Builder setBmiFrom(@IntRange(from = 0) int bmiFrom) {
            this.bmi_from = bmiFrom;
            return this;
        }

        public Builder setBmiTo(@IntRange(from = 0) int bmiTo) {
            this.bmi_to = bmiTo;
            return this;
        }

        public Builder setAgeFrom(@IntRange(from = 0) int ageFrom) {
            this.age_from = ageFrom;
            return this;
        }

        public Builder setAgeTo(@IntRange(from = 0) int ageTo) {
            this.age_to = ageTo;
            return this;
        }

        public Builder setGender(@IntRange(from = 1, to = 3) int gender) {
            this.gender = gender;
            return this;
        }

        public Builder setDrinkingHabit(@IntRange(from = 0, to = 1) int habit) {
            this.drinking_habit = habit;
            return this;
        }

        public Builder setSmokeHabit(@IntRange(from = 0, to = 1) int smoke) {
            this.smoking_habit = smoke;
            return this;
        }

        public Builder build() {
            return this;
        }

        public ListContestRequest create() {
            if (bmi_from != null && bmi_to != null && bmi_from > bmi_to) bmi_from = bmi_to;
            if (age_from != null && age_to != null && age_from > age_to) age_from = age_to;
            return new ListContestRequest(id, type,
                    start_date,
                    end_date,
                    scope,
                    membership_type,
                    bmi_from,
                    bmi_to,
                    age_from,
                    age_to,
                    gender,
                    drinking_habit,
                    smoking_habit,
                    limit, offset,
                    sort_by,
                    sort_name,
                    sort_type,
                    token);
        }
    }
}
