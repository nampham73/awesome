package com.genkimiru.app.data.model;

import java.io.Serializable;

public class WatsonMessageModel implements Serializable {
    int id;
    String message;


    public WatsonMessageModel() {
    }

    public WatsonMessageModel(int id, String message, String createdAt) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public @interface MessageType {
        int MESSAGE_SELF = 1;
        int MESSAGE_BOT = 2;
        int MESSAGE_DEFAULT = 100;
    }
}
