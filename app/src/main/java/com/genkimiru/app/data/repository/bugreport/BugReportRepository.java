package com.genkimiru.app.data.repository.bugreport;

import com.genkimiru.app.data.remote.extension.request.BugReportRequest;
import com.genkimiru.app.data.remote.extension.request.BugReportUploadImageRequest;
import com.genkimiru.app.data.remote.extension.response.ArticleResponse;
import com.genkimiru.app.data.remote.extension.response.BugReportUploadImageResponse;

import java.util.List;

import io.reactivex.Flowable;
import okhttp3.ResponseBody;

public interface BugReportRepository {
    Flowable<ResponseBody> reportBug(BugReportRequest reportRequest);
    Flowable<List<BugReportUploadImageResponse>> uploadBugImage(List<BugReportUploadImageRequest> reportRequests);
    Flowable<List<ArticleResponse>> getFaq(String lang);

}
