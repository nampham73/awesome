package com.genkimiru.app.data.remote.genki.request;

import java.util.List;

public class AddManualInputRequest {

    public String token;
    public long start_time;
    public long end_time;
    public int data_type;
    public List<MyData> data;

    public AddManualInputRequest(String token, long start_time, long end_time, int data_type, List<MyData> data) {
        this.token = token;
        this.start_time = start_time;
        this.end_time = end_time;
        this.data_type = data_type;
        this.data = data;
    }

    public static class MyData {

        public long start_time;
        public long end_time;

        public MyData(long start_time, long end_time) {
            this.start_time = start_time;
            this.end_time = end_time;
        }
    }

    public static class StepData extends MyData {

        public int step;

        public StepData(long start_time, long end_time, int step) {
            super(start_time, end_time);
            this.step = step;
        }
    }

    public static class NutriData extends MyData {
        public float kcal_in;
        public float fat_in;
        public float protein_in;
        public float carbonhydrate_in;
        public String food_name;

        public NutriData(long start_time, long end_time, float kcal_in, float fat_in, float protein_in, float carbonhydrate_in, String food_name) {
            super(start_time, end_time);
            this.kcal_in = kcal_in;
            this.fat_in = fat_in;
            this.protein_in = protein_in;
            this.carbonhydrate_in = carbonhydrate_in;
            this.food_name = food_name;
        }
    }

    public @interface DataType {
        int SLEEP = 1;
        int STEP = 2;
        int HEART = 3;
        int ENERGY = 4;
        int WEIGHT_BMI = 5;
        int NUTRI = 6;
        int STRESS = 7;
        int WATER = 8;
        int HYDRATION = 9;
    }
}
