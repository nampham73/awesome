package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.LoginWithGoogleResponse;

import java.io.Serializable;

public class LoginUserInfoModel implements Serializable {
    private String email;
    private String username;
    private String firstName;
    private String lastName;
    private String avatar;
    private String birthDay;
    private int gender;
    private String accessToken;
    private String avatarPath;
    private float latitude;
    private float longitude;
    private String nickName;

    public LoginUserInfoModel(String email, String firstName, String lastName, String avatar, String birthDay, int gender, String token) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.avatar = avatar;
        this.birthDay = birthDay;
        this.gender = gender;
        this.accessToken = token;
    }

    public LoginUserInfoModel(String email, String firstName, String lastName, String avatar) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.avatar = avatar;
    }

    public LoginUserInfoModel() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setLoginUserInfoModel(LoginWithGoogleResponse loginWithGoogleResponse) {
        email = loginWithGoogleResponse.data.email;
        lastName = loginWithGoogleResponse.data.last_name;
        firstName = loginWithGoogleResponse.data.first_name;
        avatar = loginWithGoogleResponse.data.avatar;
        birthDay = loginWithGoogleResponse.data.birthday;
        gender = loginWithGoogleResponse.data.gender;
        nickName = loginWithGoogleResponse.data.nick_name;
        longitude = loginWithGoogleResponse.data.longitude;
        latitude = loginWithGoogleResponse.data.latitude;
        username = loginWithGoogleResponse.data.username;
    }
}
