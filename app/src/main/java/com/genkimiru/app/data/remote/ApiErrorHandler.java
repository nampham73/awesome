package com.genkimiru.app.data.remote;

import android.content.Context;

import com.genkimiru.app.R;
import com.genkimiru.app.base.net.NoConnectivityException;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.gobe.GobeApiErrorException;

import retrofit2.HttpException;

import static com.genkimiru.app.Constants.Network.HTTP_STATUS_UNAUTHORIZED;

public class ApiErrorHandler {

    public static String getMessage(Context context, Throwable throwable, int defaultMessageId) {
        if (throwable instanceof NoConnectivityException) {
            return context.getResources().getString(R.string.connectivity_error);
        }

        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() == HTTP_STATUS_UNAUTHORIZED) {
                return context.getResources().getString(R.string.genki_unauthorized);
            }
        }

        if (throwable instanceof GenkiApiErrorException) {
            return context.getResources().getString(((GenkiApiErrorException) throwable).getMessageId(defaultMessageId));
        }

        if (throwable instanceof GobeApiErrorException) {
            return context.getResources().getString(((GobeApiErrorException) throwable).getMessageId());
        }

        return context.getResources().getString(R.string.unexpected_error);
    }
}
