package com.genkimiru.app.data.remote.extension.request;

public class BugReportUploadImageRequest {

    public String file;
    public String title;

    public BugReportUploadImageRequest(String file, String title) {
        this.file = file;
        this.title = title;
    }
}
