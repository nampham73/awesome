package com.genkimiru.app.data.repository.fitbit;

import com.genkimiru.app.data.remote.fitbit.FitbitApi;
import com.genkimiru.app.data.remote.fitbit.request.FitbitAccessTokenRequest;
import com.genkimiru.app.data.remote.fitbit.request.FitbitUserProfileRequest;
import com.genkimiru.app.data.remote.fitbit.response.FitbitAccessTokenResponse;
import com.genkimiru.app.data.remote.fitbit.response.FitbitUserProfileResponse;

import io.reactivex.Flowable;
import io.reactivex.Single;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Fitbit.GET_TOKEN_AUTH_HEADER;

public class FitbitRepositoryImp implements FitbitRepository {

    private FitbitApi mApi;

    public FitbitRepositoryImp(FitbitApi api) {
        mApi = api;
    }

    @Override
    public Flowable<FitbitAccessTokenResponse> getAccessToken(FitbitAccessTokenRequest fitbitAccessTokenRequest) {
        return mApi.getAccessToken(GET_TOKEN_AUTH_HEADER,
                fitbitAccessTokenRequest.clientId,
                fitbitAccessTokenRequest.grant_type,
                fitbitAccessTokenRequest.redirect_uri,
                fitbitAccessTokenRequest.code)
                .flatMap(response -> {
                    Timber.d("Repository getAccessToken response: %s", response);
                    return Single.just(response);
                }).toFlowable();
    }

    @Override
    public Flowable<FitbitUserProfileResponse> getUserProfile(FitbitUserProfileRequest fitbitUserProfileRequest) {
        return mApi.getUserProfile(fitbitUserProfileRequest.token)
                .flatMap(response -> {
                    Timber.d("Repository getUserProfile response: %s", response);
                    return Single.just(response);
                }).toFlowable();
    }
}
