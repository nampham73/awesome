package com.genkimiru.app.data.repository.gobe;

import com.genkimiru.app.data.remote.gobe.GobeApi;
import com.genkimiru.app.data.remote.gobe.GobeApiErrorException;
import com.genkimiru.app.data.remote.gobe.request.GobeAuthorizationRequest;
import com.genkimiru.app.data.remote.gobe.response.GobeAuthorizationResponse;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class GobeRepositoryImp implements GobeRepository {

    private GobeApi mApi;

    public GobeRepositoryImp(GobeApi api) {
        mApi = api;
    }

    @Override
    public Flowable<GobeAuthorizationResponse> authorize(GobeAuthorizationRequest request) {
        return mApi.authorize(request.email, request.password).flatMap(response -> {
            String responseString = response.string();
            JSONObject responseJson = new JSONObject(responseString);
            Object responseCodeObject = responseJson.get("response_code");
            if (responseCodeObject instanceof JSONArray) {
                return new GobeApiErrorException(responseString).toSingleError();
            }
            Gson gson = new Gson();
            return Single.just(gson.fromJson(responseString, GobeAuthorizationResponse.class));
        }).toFlowable();
    }
}
