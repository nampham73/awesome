package com.genkimiru.app.data.remote.gobe;

import com.genkimiru.app.base.net.BaseRestApi;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class GobeRestApi extends BaseRestApi<GobeApi> {

    private static final String BASE_URL = "https://api.healbe.com/";

    public GobeRestApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        super(client, gsonConverter, rxAdapter);
    }

    @Override
    protected String registerBaseUrl() {
        return BASE_URL;
    }

    @Override
    protected Class<GobeApi> registerApiClassType() {
        return GobeApi.class;
    }
}
