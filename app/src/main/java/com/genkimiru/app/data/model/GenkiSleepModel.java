package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.SleepResponse;

public class GenkiSleepModel {
    private long timestamp;
    private float sleep_event;
    private float second;
    private float total_sleep_time;
    private long average_start_time;
    private long average_end_time;
    private float average_sleep_time;
    private float average_deep_time;
    private float average_light_time;
    private float average_rem_time;
    private float average_wake_time;

    public GenkiSleepModel(long timestamp,
                           float sleep_event,
                           float second,
                           float total_sleep_time,
                           long average_start_time,
                           long average_end_time,
                           float average_sleep_time,
                           float average_deep_time,
                           float average_light_time,
                           float average_rem_time,
                           float average_wake_time) {
        this.timestamp = timestamp;
        this.sleep_event = sleep_event;
        this.second = second;
        this.total_sleep_time = total_sleep_time;
        this.average_start_time = average_start_time;
        this.average_end_time = average_end_time;
        this.average_sleep_time = average_sleep_time;
        this.average_deep_time = average_deep_time;
        this.average_light_time = average_light_time;
        this.average_rem_time = average_rem_time;
        this.average_wake_time = average_wake_time;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getSleepEvent() {
        return sleep_event;
    }

    public float getSecond() {
        return second;
    }

    public float getTotalSleepTime() {
        return total_sleep_time;
    }

    public long getAverageStartTime() {
        return average_start_time;
    }

    public long getAverageEndTime() {
        return average_end_time;
    }

    public float getAverageSleepTime() {
        return average_sleep_time;
    }

    public float getAverageDeepTime() {
        return average_deep_time;
    }

    public float getAverageLightTime() {
        return average_light_time;
    }

    public float getAverageRemTime() {
        return average_rem_time;
    }

    public float getAverageWakeTime() {
        return average_wake_time;
    }

    public float getTotalSleep() {
        return total_sleep_time;
    }

    public static GenkiSleepModel convert(SleepResponse response) {
        if (response == null) return null;
        return new GenkiSleepModel(response.timestamp,
                response.sleep_event,
                response.second,
                response.total_sleep_time,
                response.average_start_time,
                response.average_end_time,
                response.average_sleep_time,
                response.average_deep_time,
                response.average_light_time,
                response.average_rem_time,
                response.average_wake_time);
    }
}
