package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class ListContentResponse {

    public List<Content> contents;
    public int total;

    public class Content {
        public int id;
        public String title;
        public String picture;
        public String url;
        public String description;
        public int contest_id;
        public int type;
        public int program_id;
        public String program_name;
        public String created_at;
        public String updated_at;
        public boolean enabled;
        public String username;
        public boolean is_created_by_admin;
    }
}
