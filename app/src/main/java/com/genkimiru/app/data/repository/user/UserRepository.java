package com.genkimiru.app.data.repository.user;

import com.genkimiru.app.data.remote.genki.request.ActivePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.data.remote.genki.request.ChangePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.LoginFacebookRequest;
import com.genkimiru.app.data.remote.genki.request.LoginRequest;
import com.genkimiru.app.data.remote.genki.request.LoginWithGoogleRequest;
import com.genkimiru.app.data.remote.genki.request.NotificationSettingRequest;
import com.genkimiru.app.data.remote.genki.request.PrivacySettingRequest;
import com.genkimiru.app.data.remote.genki.request.RecoverPasswordRequest;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.request.SettingEmailMarketingRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;
import com.genkimiru.app.data.remote.genki.response.LoginFacebookDataResponse;
import com.genkimiru.app.data.remote.genki.response.LoginResponse;
import com.genkimiru.app.data.remote.genki.response.LoginWithGoogleResponse;
import com.genkimiru.app.data.remote.genki.response.RecoverPasswordResponse;
import com.genkimiru.app.data.remote.genki.response.RegisterUserResponse;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;

import io.reactivex.Flowable;
import okhttp3.ResponseBody;

public interface UserRepository {
    Flowable<LoginResponse> login(LoginRequest loginRequest);
    Flowable<UserInfoResponse> loginAndFetchingUserInfo(LoginRequest request);
    Flowable<UserInfoResponse> getUserInformation(String token);
    Flowable<RegisterUserResponse> register(RegisterUserRequest registerUserRequest);
    Flowable<RecoverPasswordResponse> recoverPassword(RecoverPasswordRequest recoverPasswordRequest);
    Flowable<Boolean> activePassword(ActivePasswordRequest activePasswordRequest);
    Flowable<Boolean> updatePrivacySetting(PrivacySettingRequest privacySettingRequest);
    Flowable<Boolean> changePassword(ChangePasswordRequest changePasswordRequest);
    Flowable<ResponseBody> downloadImage(String url);
    Flowable<Boolean> updateNotificationSetting(NotificationSettingRequest notificationSettingRequest);
    Flowable<Boolean> updateEmailMarketingSetting(SettingEmailMarketingRequest settingEmailMarketingRequest);
    Flowable<Integer>updateHealthSourceSetting(UserInfoRequest healthSourceRequest);
    Flowable<Boolean>updateUserProfile(UserInfoRequest userInfoRequest);
    Flowable<Boolean>updateUserAvatar(UserInfoRequest userInfoRequest);
    Flowable<LoginFacebookDataResponse> loginFacebook(LoginFacebookRequest loginFacebookRequest);
    Flowable<Boolean> addManualInput(AddManualInputRequest addManualInputRequest);
    Flowable<LoginWithGoogleResponse> loginGoogle(LoginWithGoogleRequest loginWithGoogleRequest);
    Flowable<Boolean> processChallenge(ChallengeRequest challengeRequest);
}
