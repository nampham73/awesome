package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.HeartResponse;

public class GenkiHeartModel {
    private long timestamp;
    private float heart_rate;
    private float min;
    private float max;
    private float second;

    public GenkiHeartModel(long timestamp, float heart_rate, float min, float max, float second) {
        this.timestamp = timestamp;
        this.heart_rate = heart_rate;
        this.min = min;
        this.max = max;
        this.second = second;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getHeartRate() {
        return heart_rate;
    }

    public float getMin() {
        return min;
    }

    public float getMax() {
        return max;
    }

    public float getSecond() {
        return second;
    }

    public static GenkiHeartModel convert(HeartResponse response) {
        return new GenkiHeartModel(
                response.timestamp,
                response.heart_rate,
                response.min,
                response.max,
                response.second);
    }
}
