package com.genkimiru.app.data.remote.genki.response;

public class StressResponse {
    public long timestamp;
    public float stress_level;
    public float second;
}
