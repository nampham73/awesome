package com.genkimiru.app.data.remote.genki.request;

public class RecoverPasswordRequest {
    public String email;

    public RecoverPasswordRequest(String email) {
        this.email = email;
    }
}
