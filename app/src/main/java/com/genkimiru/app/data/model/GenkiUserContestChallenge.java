package com.genkimiru.app.data.model;

import com.genkimiru.app.base.util.JsonGenerator;

import java.io.Serializable;

public class GenkiUserContestChallenge implements Serializable {
    private int id;
    private int contest_challenge_id;
    private int user_id;
    private int challenge_id;
    private int challenge_status;
    private int challenge_count;
    private String challenge_start_date_time;
    private String challenge_end_date_time;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContest_challenge_id() {
        return contest_challenge_id;
    }

    public void setContest_challenge_id(int contest_challenge_id) {
        this.contest_challenge_id = contest_challenge_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(int challenge_id) {
        this.challenge_id = challenge_id;
    }

    public int getChallenge_status() {
        return challenge_status;
    }

    public void setChallenge_status(int challenge_status) {
        this.challenge_status = challenge_status;
    }

    public int getChallenge_count() {
        return challenge_count;
    }

    public void setChallenge_count(int challenge_count) {
        this.challenge_count = challenge_count;
    }

    public String getChallenge_start_date_time() {
        return challenge_start_date_time;
    }

    public void setChallenge_start_date_time(String challenge_start_date_time) {
        this.challenge_start_date_time = challenge_start_date_time;
    }

    public String getChallenge_end_date_time() {
        return challenge_end_date_time;
    }

    public void setChallenge_end_date_time(String challenge_end_date_time) {
        this.challenge_end_date_time = challenge_end_date_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return JsonGenerator.getInstance().convertJson(this);
    }
}
