package com.genkimiru.app.data.remote.genki.request;

public class DeleteManualInputRequest {

    public int id;
    public int data_type;

    public DeleteManualInputRequest(int id, int data_type) {
        this.id = id;
        this.data_type = data_type;
    }

    public @interface DataType {
        int SLEEP = 1;
        int STEP = 2;
        int HEART = 3;
        int ENERGY = 4;
        int WEIGHT_BMI = 5;
        int NUTRI = 6;
    }
}
