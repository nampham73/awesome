package com.genkimiru.app.data.model;

import android.support.annotation.NonNull;

import com.genkimiru.app.data.remote.genki.response.ContestModelResponse;

import java.io.Serializable;

public class GenkiContestModel implements Serializable {
    private int mId;
    private String mName;
    private String mDescription;
    private int mType;
    private String mStartDate;
    private String mEndDate;
    private int mRewardPoint;
    private String mRewardAvatarUrl;
    private String mTitle;
    private int mScope;
    private int mMembershipType;
    private int mMaxNumberAwardUser;
    private int mCurrentNumberAwardUser;
    private String mAddress;
    private String mLatitude;
    private String mLongitude;
    private int mRadius;
    private float mBmiFrom;
    private float mBmiTo;
    private int mAgeFrom;
    private int mAgeTo;
    private int mGender;
    private boolean mDrinkingHabit;
    private boolean mSmokingHabit;
    private boolean mEnabled;
    private String mCreatedAt;
    private String mUpdatedAt;
    private int mTotalJoin;
    private int mUseChallengeStatus;
    private boolean isHistory;

    public boolean isHistory() {
        return isHistory;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public @interface ContestType{
        int ONE_TIME  =1;
        int DAILY = 2;
        int WEEKLY = 3;
        int MONTHLY = 4;
    }

    public @interface ContestStatus{
        int NOT_START = 1;
        int IN_PROGRESS = 2;
        int FINISHED = 3;
        int CANCEL = 4;
    }

    public void setValueFromResponse(@NonNull ContestModelResponse response) {
        this.mId = response.id;
        this.mName = response.name;
        this.mDescription = response.description;
        this.mType = response.type;
        this.mStartDate = response.start_date;
        this.mEndDate = response.end_date;
        this.mRewardPoint = response.reward_point;
        this.mRewardAvatarUrl = response.reward_avatar_url;
        this.mTitle = response.title;
        this.mScope = response.scope;
        this.mMaxNumberAwardUser = response.max_number_award_user;
        this.mCurrentNumberAwardUser = response.current_number_award_user;
        this.mAddress = response.address;
        this.mLatitude = response.latitude;
        this.mLongitude = response.longitude;
        this.mRadius = response.radius;
        this.mBmiFrom = response.bmi_from;
        this.mBmiTo = response.bmi_to;
        this.mAgeFrom = response.age_from;
        this.mAgeTo = response.age_to;
        this.mGender = response.gender;
        this.mDrinkingHabit = response.drinking_habit;
        this.mSmokingHabit = response.smoking_habit;
        this.mEnabled = response.enabled;
        this.mCreatedAt = response.created_at;
        this.mUpdatedAt = response.updated_at;
        this.mMembershipType = response.membership_type;
        this.mTotalJoin = response.total_join;
        this.mUseChallengeStatus  = response.user_contest_status;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String mEndDate) {
        this.mEndDate = mEndDate;
    }

    public int getRewardPoint() {
        return mRewardPoint;
    }

    public void setRewardPoint(int mRewardPoint) {
        this.mRewardPoint = mRewardPoint;
    }

    public String getRewardAvatarUrl() {
        return mRewardAvatarUrl;
    }

    public void setRewardAvatarUrl(String mRewardAvatarUrl) {
        this.mRewardAvatarUrl = mRewardAvatarUrl;
    }

    public String getmDescription() {
        return mDescription;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public int getScope() {
        return mScope;
    }

    public void setScope(int mScope) {
        this.mScope = mScope;
    }

    public int getMembershipType() {
        return mMembershipType;
    }

    public void setMembershipType(int mMembershipType) {
        this.mMembershipType = mMembershipType;
    }

    public int getMaxNumberAwardUser() {
        return mMaxNumberAwardUser;
    }

    public void setMaxNumberAwardUser(int mMaxNumberAwardUser) {
        this.mMaxNumberAwardUser = mMaxNumberAwardUser;
    }

    public int getCurrentNumberAwardUser() {
        return mCurrentNumberAwardUser;
    }

    public void setCurrentNumberAwardUser(int mCurrentNumberAwardUser) {
        this.mCurrentNumberAwardUser = mCurrentNumberAwardUser;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public int getRadius() {
        return mRadius;
    }

    public void setRadius(int mRadius) {
        this.mRadius = mRadius;
    }

    public float getBmiFrom() {
        return mBmiFrom;
    }

    public void setBmiFrom(int bmiFrom) {
        this.mBmiFrom = bmiFrom;
    }

    public float getBmiTo() {
        return mBmiTo;
    }

    public void setBmiTo(int bmiTo) {
        this.mBmiTo = bmiTo;
    }

    public int getAgeFrom() {
        return mAgeFrom;
    }

    public void setAgeFrom(int ageFrom) {
        this.mAgeFrom = ageFrom;
    }

    public int getAgeTo() {
        return mAgeTo;
    }

    public void setAgeTo(int ageTo) {
        this.mAgeTo = ageTo;
    }

    public int getGender() {
        return mGender;
    }

    public void setGender(int gender) {
        this.mGender = gender;
    }

    public boolean isDrinkingHabit() {
        return mDrinkingHabit;
    }

    public void setDrinkingHabit(boolean mDrinkingHabit) {
        this.mDrinkingHabit = mDrinkingHabit;
    }

    public boolean isSmokingHabit() {
        return mSmokingHabit;
    }

    public void setSmokingHabit(boolean mSmokingHabit) {
        this.mSmokingHabit = mSmokingHabit;
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean mEnabled) {
        this.mEnabled = mEnabled;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public int getTotalJoin() {
        return mTotalJoin;
    }

    public void setTotalJoin(int mTotalJoin) {
        this.mTotalJoin = mTotalJoin;
    }

    public int getUseChallengeStatus() {
        return mUseChallengeStatus;
    }
}
