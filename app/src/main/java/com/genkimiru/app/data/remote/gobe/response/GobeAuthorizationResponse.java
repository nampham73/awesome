package com.genkimiru.app.data.remote.gobe.response;

import java.util.List;

public class GobeAuthorizationResponse {

    public int response_code;
    public long timestamp;
    public int request;
    public String public_key;
    public String access_id;
    public String registerDate;
    public String userUUID;
    public String DeviceUUID;
    public UserInfo user_info;
    public List<Integration> integrations;

    public class UserInfo {

        public String firstname;
        public String lastname;
        public String country;
        public String CountryISOCode;
        public String region;
        public String AdministrativeArea;
        public String city;
        public String postalcode;
        public String street_address_1;
        public String street_address_2;
        public Module modules;
        public Preferences preferences;
        public int sex;
        public float weight;
        public int height;
        public int b_day;
        public int b_month;
        public int b_year;
        public int normal_Ps;
        public int normal_Pd;
        public int armlength;
        public int health_level;
        public int step_length;
        public int hr_while_standing;
        public int diet_type;
        public int user_weight_src_id;
        public String avatar_url;
        public String account;
        public boolean wristband_paired;
        public String hash;
        public String access_token_expired_at;

        public class Module {
            public Alarm alarms;
            public Sleep sleep;
            public Water water;

            public class Alarm {
                public int last_modified;
                public List<AlarmItem> items;

                public class AlarmItem {
                    public int alarm_id;
                    public int sleep_quality;
                    public int minutes_since_midnight;
                    public long flags;
                    public FlagsDecoded flags_decoded;

                    public class FlagsDecoded {
                        public int[] days;
                        public int[] types;
                        public boolean enabled;
                        public boolean repeat_every_week;
                        public int time_window_duration;
                    }
                }
            }

            public class Sleep {

                public Config config;
                public PrivateData private_data;

                public class Config {
                    public int last_modified;
                    public int user_sleep_duration;
                }

                public class PrivateData {
                    public String private_data;
                    public int last_modified;
                }
            }

            public class Water {

                public Config config;

                public class Config {
                    public int rest_offset;
                    public int glass_volume;
                    public int vibro_signal;
                    public int rest_duration;
                }
            }
        }

        public class Preferences {
            public String distance_unit;
            public int sleep_duration;
            public String water_unit;
            public int glass_volume;
            public int vibro_signal;
            public int rest_offset;
            public int rest_duration;
        }

    }

    public class Integration {
        public int id;
        public String name;
    }
}
