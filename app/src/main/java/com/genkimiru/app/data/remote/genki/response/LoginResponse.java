package com.genkimiru.app.data.remote.genki.response;

public class LoginResponse {

    public int id;
    public String username;
    public String token;
    public int role_id;
    public String email;
    public String birthday;
    public int gender;
    public String avatar;
    public String first_name;
    public String last_name;
    public String nick_name;
    public String address;
    public boolean health_flag;
    public boolean profile_flag;
    public boolean email_marketing_setting;
    public double latitude;
    public double longitude;
}
