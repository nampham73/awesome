package com.genkimiru.app.data.remote.genki.response;

import java.util.List;

public class DetailSleepChartResponse extends DetailDataChartResponse {
    public List<SleepResponse> data;
}
