package com.genkimiru.app.data.remote.genki.response;

public class DetailDataChartResponse {
    public float status;
    public float startDate;
    public float endDate;
    public String dataType;
    public String chartType;
    public float itemPerPage;
    public float index;
    public String lastUpdateDate;
    public float count;
    public float totalPage;
}
