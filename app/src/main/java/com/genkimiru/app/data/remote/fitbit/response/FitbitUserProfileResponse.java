package com.genkimiru.app.data.remote.fitbit.response;

public class FitbitUserProfileResponse {

    public User user;

    public class User {
        public int age;
        public String country;
        public String dateOfBirth;
        public String displayName;
        public String displayNameSetting;
        public String encodedId;
        public String firstName;
        public String fullName;
        public String lastName;
        public String gender = "";
        public float height;
        public float weight;
        public String locale;
        public String memberSince;
        public String timezone;
        public String avatar;
    }
}
