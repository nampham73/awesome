package com.genkimiru.app.data.remote.fitbit.request;

public class FitbitAccessTokenRequest {

    public String clientId;
    public String grant_type;
    public String redirect_uri;
    public String code;

    public FitbitAccessTokenRequest(String clientId, String grant_type, String redirect_uri, String code) {
        this.clientId = clientId;
        this.grant_type = grant_type;
        this.redirect_uri = redirect_uri;
        this.code = code;
    }
}
