package com.genkimiru.app.data.remote.gobe;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GobeApi {

    @GET("api?request=1")
    Single<ResponseBody> authorize(@Query("email") String email, @Query("password") String password);
}
