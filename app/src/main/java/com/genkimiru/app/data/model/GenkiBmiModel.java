package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.BmiResponse;

public class GenkiBmiModel {
    private long timestamp;
    private float kcal_in;
    private float fat_in;
    private float second;
    private float weight;
    private float bmi;
    private float carbonhydrate_in;
    private float average_kcal_in;
    private float protein_in;

    public GenkiBmiModel(long timestamp,
                         float kcal_in,
                         float fat_in,
                         float second,
                         float weight,
                         float bmi,
                         float carbonhydrate_in,
                         float average_kcal_in,
                         float protein_in) {
        this.timestamp = timestamp;
        this.kcal_in = kcal_in;
        this.fat_in = fat_in;
        this.second = second;
        this.weight = weight;
        this.bmi = bmi;
        this.carbonhydrate_in = carbonhydrate_in;
        this.average_kcal_in = average_kcal_in;
        this.protein_in = protein_in;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getKCalIn() {
        return kcal_in;
    }

    public float getFatIn() {
        return fat_in;
    }

    public float getSecond() {
        return second;
    }

    public float getCarbonHydrateIn() {
        return carbonhydrate_in;
    }

    public float getAverageKCalIn() {
        return average_kcal_in;
    }

    public float getProteinIn() {
        return protein_in;
    }

    public float getWeight() {
        return weight;
    }

    public float getBmi() {
        return bmi;
    }

    public static GenkiBmiModel convert(BmiResponse response) {
        return new GenkiBmiModel(response.timestamp,
                response.kcal_in,
                response.fat_in,
                response.second,
                response.weight,
                response.bmi,
                response.carbonhydrate_in,
                response.average_kcal_in,
                response.protein_in);
    }
}
