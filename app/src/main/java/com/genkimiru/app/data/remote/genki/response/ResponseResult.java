package com.genkimiru.app.data.remote.genki.response;

import com.google.gson.Gson;

public class ResponseResult {

    public int status;
    public Object body;

    public <T> T convertBodyTo(Class<T> classOfT) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(body);
        return gson.fromJson(jsonString, classOfT);
    }
}
