package com.genkimiru.app.data.remote.genki.request;

public class DetailChartDataRequest {

    private String token;
    private long start_date;
    private Long end_date;
    private int data_type;
    private int chart_type;
    private Integer index;

    public DetailChartDataRequest(String token,
                                  long start_date,
                                  Long end_date,
                                  int data_type,
                                  int chart_type, Integer index) {
        this.token = token;
        this.start_date = start_date;
        this.end_date = end_date;
        this.data_type = data_type;
        this.chart_type = chart_type;
        this.index = index;
    }

    public static class Builder {
        private String token;
        private long start_date;
        private Long end_date;
        private int data_type;
        private int chart_type;
        private Integer index;

        public Builder(String token) {
            this.token = token;
        }

        public Builder setStartDate(long startDate) {
            this.start_date = startDate;
            return this;
        }

        public Builder setDataType(int data_type) {
            this.data_type = data_type;
            return this;
        }

        public Builder setChartType(int chart_type) {
            this.chart_type = chart_type;
            return this;
        }

        public Builder setEndDate(Long end_date) {
            this.end_date = end_date;
            return this;
        }

        public Builder setIndex(Integer index) {
            this.index = index;
            return this;
        }

        public DetailChartDataRequest create() {
            return new DetailChartDataRequest(token, start_date, end_date, data_type, chart_type, index);
        }
    }

    public @interface ChartType {
        int MIN_5 = 1;
        int HOUR = 2;
        int DAILY = 3;
        int WEEKLY = 4;
        int MONTHLY = 5;
    }

    public @interface DataType {
        int SLEEP = 1;
        int STEP = 2;
        int HEART = 3;
        int ENERGY = 4;
        int WEIGHT_AND_BMI = 5;
        int NUTRI = 6;
        int STRESS = 7;
        int WATER = 8;
        int HYDRATION = 9;
    }
}
