package com.genkimiru.app.data.remote.gobe;

import com.genkimiru.app.R;
import com.genkimiru.app.data.remote.gobe.response.GobeErrorResponse;
import com.google.gson.Gson;

import io.reactivex.Single;
import timber.log.Timber;

public class GobeApiErrorException extends Exception {

    private String mResponse;

    public GobeApiErrorException() {
    }

    public GobeApiErrorException(String responseBody) {
        mResponse = responseBody;
    }

    public int getMessageId() {
        GobeErrorResponse errorResponse = parseErrorResponse();
        Timber.d("Gobe api errorResponse = %s", errorResponse);
        return errorResponse == null ? R.string.unexpected_error : GobeApiErrorException.ErrorCode.getMessageIdByCode(errorResponse.response_code[1]);
    }

    public void setResponse(String response) {
        this.mResponse = response;
    }

    public <T> Single<T> toSingleError() {
        return Single.error(this);
    }

    public String getMessage() {
        GobeErrorResponse errorResponse = parseErrorResponse();
        if (errorResponse == null) {
            return "";
        }
        return "Gobe api error code: "
                + errorResponse.response_code[1];
    }

    private GobeErrorResponse parseErrorResponse() {
        Timber.d("Gobe api response body: %s", mResponse);
        if (mResponse == null) {
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(mResponse, GobeErrorResponse.class);
    }

    public enum ErrorCode {

        UNEXPECTED(-1, R.string.unexpected_error),
        RESPONSE_CODE_FAIL(1, R.string.response_code_fail),
        RESPONSE_CODE_PARAMETERS_ARE_MISSING(3, R.string.response_code_parameters_are_missing),
        RESPONSE_CODE_PARAMETERS_ARE_WRONG(4, R.string.response_code_parameters_are_wrong),
        RESPONSE_CODE_UPLOAD_FAILED(5, R.string.response_code_upload_failed),
        RESPONSE_CODE_ACCESS_ID_ERROR(30, R.string.response_code_access_id_error),
        RESPONSE_CODE_GET_ACCESS_ID_ERROR(31, R.string.response_code_get_access_id_error),
        RESPONSE_CODE_LOGIN_OR_PASSWORD_WRONG(32, R.string.response_code_login_or_password_wrong),
        RESPONSE_CODE_USER_EXISTS(33, R.string.response_code_user_exists),
        RESPONSE_CODE_USER_DATA_EMPTY(34, R.string.response_code_user_data_empty),
        RESPONSE_CODE_USER_NOT_EXISTS(35, R.string.response_code_user_not_exists),
        RESPONSE_CODE_USER_GROUP_EMPTY(36, R.string.response_code_user_group_empty),
        RESPONSE_CODE_TOO_MUCH_WRONG_TRIES(37, R.string.response_code_too_much_wrong_tries),
        RESPONSE_CODE_HEALBE_NOT_FOUND(60, R.string.response_code_healbe_not_found),
        RESPONSE_CODE_HEALBE_REQUIRE_REGISTRATION(61, R.string.response_code_healbe_require_registration),
        RESPONSE_CODE_HEALBE_ALREADY_REGISTERED(62, R.string.response_code_healbe_already_registered),
        RESPONSE_CODE_INTERNAL_ERROR(500, R.string.response_code_internal_error);

        private int mCode;
        private int mMessageId;

        ErrorCode(int code, int messageId) {
            mCode = code;
            mMessageId = messageId;
        }

        static int getMessageIdByCode(int code) {
            for (ErrorCode errorCode : ErrorCode.values()) {
                if (errorCode.getCode() == code) {
                    return errorCode.getMessageId();
                }
            }
            return R.string.unexpected_error;
        }

        public int getCode() {
            return mCode;
        }

        public int getMessageId() {
            return mMessageId;
        }
    }
}
