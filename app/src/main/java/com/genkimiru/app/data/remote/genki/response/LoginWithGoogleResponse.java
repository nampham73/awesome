package com.genkimiru.app.data.remote.genki.response;

public class LoginWithGoogleResponse {
    public Data data;

    public class Data {
        public String username;
        public String token;
        public int role_id;
        public String email;
        public String birthday;
        public int gender;
        public String avatar;
        public String first_name;
        public String last_name;
        public String nick_name;
        public String address;
        public String code;
        public boolean health_flag;
        public boolean profile_flag;
        public boolean announcement_flag;
        public boolean advices_flag;
        public boolean contest_notifications_flag;
        public float latitude;
        public float longitude;
        public float health_source;
        public String created_at;
        public String updated_at;
        public boolean email_marketing_setting;
        public boolean enabled;
        public String refresh_token;
    }
}
