package com.genkimiru.app.data.remote.gobe.request;

public class GobeAuthorizationRequest {
    public String email;
    public String password;

    public GobeAuthorizationRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
