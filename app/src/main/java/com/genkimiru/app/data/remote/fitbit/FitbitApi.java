package com.genkimiru.app.data.remote.fitbit;

import com.genkimiru.app.data.remote.fitbit.response.FitbitAccessTokenResponse;
import com.genkimiru.app.data.remote.fitbit.response.FitbitUserProfileResponse;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface FitbitApi {

    @FormUrlEncoded
    @POST("oauth2/token")
    Single<FitbitAccessTokenResponse> getAccessToken(
            @Header("Authorization") String authorization,
            @Field("clientId") String clientId,
            @Field("grant_type") String grantType,
            @Field("redirect_uri") String redirectUri,
            @Field("code") String code
    );

    @GET("1/user/-/profile.json")
    Single<FitbitUserProfileResponse> getUserProfile(@Header("Authorization") String authorization);
}
