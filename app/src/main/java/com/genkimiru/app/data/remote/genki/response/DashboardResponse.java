package com.genkimiru.app.data.remote.genki.response;

public class DashboardResponse {
    public int status;
    public String message;
    public DashboardDataResponse data;
}
