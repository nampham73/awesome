package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.ContestDetailResponse;

import java.io.Serializable;
import java.util.List;

public class GenkiContestDetailModel implements Serializable {

    private int id;
    private String name;
    private int type;
    private String description;
    private String start_date;
    private String end_date;
    private int reward_point;
    private String reward_avatar_url;
    private String detailImageUrl;
    private String title;
    private int scope;
    private int max_number_award_user;
    private int current_number_award_user;
    private String address;
    private float latitude;
    private float longitude;
    private int radius;
    private float bmi_from;
    private float bmi_to;
    private int age_from;
    private int age_to;
    private int gender;
    private boolean drinking_habit;
    private boolean smoking_habit;
    private String created_at;
    private String updated_at;
    private int total_join;
    private int user_contest_status;
    private List<GenkiChallengeModel> listChallenge;
    private List<GenkiUserContestChallenge> listGenkiUserContestChallenges;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getReward_point() {
        return reward_point;
    }

    public void setReward_point(int reward_point) {
        this.reward_point = reward_point;
    }

    public String getReward_avatar_url() {
        return reward_avatar_url;
    }

    public void setReward_avatar_url(String reward_avatar_url) {
        this.reward_avatar_url = reward_avatar_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = scope;
    }

    public int getMax_number_award_user() {
        return max_number_award_user;
    }

    public void setMax_number_award_user(int max_number_award_user) {
        this.max_number_award_user = max_number_award_user;
    }

    public int getCurrent_number_award_user() {
        return current_number_award_user;
    }

    public void setCurrent_number_award_user(int current_number_award_user) {
        this.current_number_award_user = current_number_award_user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public float getBmi_from() {
        return bmi_from;
    }

    public void setBmi_from(float bmi_from) {
        this.bmi_from = bmi_from;
    }

    public float getBmi_to() {
        return bmi_to;
    }

    public void setBmi_to(float bmi_to) {
        this.bmi_to = bmi_to;
    }

    public int getAge_from() {
        return age_from;
    }

    public void setAge_from(int age_from) {
        this.age_from = age_from;
    }

    public int getAge_to() {
        return age_to;
    }

    public void setAge_to(int age_to) {
        this.age_to = age_to;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isDrinking_habit() {
        return drinking_habit;
    }

    public void setDrinking_habit(boolean drinking_habit) {
        this.drinking_habit = drinking_habit;
    }

    public boolean isSmoking_habit() {
        return smoking_habit;
    }

    public void setSmoking_habit(boolean smoking_habit) {
        this.smoking_habit = smoking_habit;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getTotal_join() {
        return total_join;
    }

    public void setTotal_join(int total_join) {
        this.total_join = total_join;
    }

    public int getUser_contest_status() {
        return user_contest_status;
    }

    public void setUser_contest_status(int user_contest_status) {
        this.user_contest_status = user_contest_status;
    }

    public List<GenkiChallengeModel> getListChallenge() {
        return listChallenge;
    }

    public void setListChallenge(List<GenkiChallengeModel> listChallenge) {
        this.listChallenge = listChallenge;
    }

    public List<GenkiUserContestChallenge> getListGenkiUserContestChallenges() {
        return listGenkiUserContestChallenges;
    }

    public void setListGenkiUserContestChallenges(List<GenkiUserContestChallenge> listGenkiUserContestChallenges) {
        this.listGenkiUserContestChallenges = listGenkiUserContestChallenges;
    }

    public String getDetailImageUrl() {
        return detailImageUrl;
    }

    public void setDetailImageUrl(String detailImageUrl) {
        this.detailImageUrl = detailImageUrl;
    }

    public void setContestDetailValue(ContestDetailResponse response) {
        name = response.name;
        id = response.id;
        title = response.title;
        description = response.description;
        start_date = response.start_date;
        end_date = response.end_date;
        reward_point = response.reward_point;
        type = response.type;
        reward_avatar_url = response.reward_avatar_url;
        detailImageUrl = response.detail_image_url;
        scope = response.scope;
        max_number_award_user = response.max_number_award_user;
        current_number_award_user = response.current_number_award_user;
        address = response.address;
        latitude = response.latitude;
        longitude = response.longitude;
        radius = response.radius;
        bmi_from = response.bmi_from;
        bmi_to = response.bmi_to;
        age_from = response.age_to;
        age_to = response.age_to;
        gender = response.gender;
        smoking_habit = response.smoking_habit;
        drinking_habit = response.drinking_habit;
        created_at = response.created_at;
        updated_at = response.updated_at;
        total_join = response.total_join;
        user_contest_status = response.user_contest_status;
        listChallenge = response.challenges;
        listGenkiUserContestChallenges = response.user_contest_challenges;
    }

    public @interface ContestStatus{
        int NOT_JOIN = 0;
        int NOT_START = 1;
        int IN_PROGRESS = 2;
        int FINISHED = 3;
        int CANCEL = 4;
    }

    public @interface ChellengeType {
        int WALKING = 1;
        int WATER = 2;
        int CLYCLING = 3;
    }
}
