package com.genkimiru.app.data.repository.bugreport;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.data.remote.extension.ExtensionApi;
import com.genkimiru.app.data.remote.extension.request.BugReportRequest;
import com.genkimiru.app.data.remote.extension.request.BugReportUploadImageRequest;
import com.genkimiru.app.data.remote.extension.response.ArticleResponse;
import com.genkimiru.app.data.remote.extension.response.BugReportUploadImageResponse;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import okhttp3.ResponseBody;

public class BugReportRepositoryImp implements BugReportRepository {

    private ExtensionApi mApi;

    public BugReportRepositoryImp(ExtensionApi api) {
        mApi = api;
    }

    @Override
    public Flowable<ResponseBody> reportBug(BugReportRequest reportRequest) {
        return mApi.reportBug(BuildConfig.HEADER_SERVER_VALUE, reportRequest).toFlowable();
    }

    @Override
    public Flowable<List<BugReportUploadImageResponse>> uploadBugImage(List<BugReportUploadImageRequest> reportRequests) {
        return Flowable.just(reportRequests)
                .flatMapIterable(requests -> requests)
                .flatMap(request -> mApi.uploadBugImage(BuildConfig.HEADER_SERVER_VALUE, request).toFlowable())
                .toList()
                .toFlowable();
    }

    @Override
    public Flowable<List<ArticleResponse>> getFaq(String lang) {
        return mApi.getFaq(BuildConfig.HEADER_SERVER_VALUE,lang).flatMap(faqResponse -> {
            if (faqResponse!=null)return Single.just(faqResponse.articles);
            return new GenkiApiErrorException(faqResponse).toSingleError();
        }).toFlowable();
    }

}
