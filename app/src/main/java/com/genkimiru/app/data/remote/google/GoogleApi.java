package com.genkimiru.app.data.remote.google;

import com.genkimiru.app.data.remote.google.response.GoogleAccessTokenResponse;
import com.genkimiru.app.data.remote.google.response.GoogleFitUserProfileResponse;
import com.genkimiru.app.data.remote.genki.response.LoginWithGoogleResponse;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface GoogleApi {

    @FormUrlEncoded
    @POST("oauth2/v4/token")
    Single<GoogleAccessTokenResponse> getAccessToken (
            @Field("code") String code,
            @Field("redirect_uri") String redirectUri,
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecrect,
            @Field("scope") String scope,
            @Field("grant_type") String grantType
    );

    @GET("oauth2/v1/userinfo?alt=json")
    Single<GoogleFitUserProfileResponse> getUserprofile(@Header("Authorization") String authorization);

    @GET
    @Streaming
    Single<ResponseBody> downloadImage(@Url String fileUrl);

    @GET("plus/v1/people")
    Single<LoginWithGoogleResponse> getUserInfo(@Header("Authorization") String authorization);


}
