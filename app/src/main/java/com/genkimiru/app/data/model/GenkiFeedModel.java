package com.genkimiru.app.data.model;

import android.text.Html;
import android.text.Spanned;

import com.genkimiru.app.Constants;
import com.genkimiru.app.data.remote.genki.response.FeedModelResponse;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.adapter.IDetailContentModel;

public class GenkiFeedModel implements IDetailContentModel {
    private int mArticleId;
    private String mOwnerName;
    private int mStatus;
    private int mOwnerId;
    private String mCreatedAt;
    private String mAvatar;
    private String mComment;
    private boolean mEnable;

    public void setValueFrom(FeedModelResponse response) {
        mArticleId = response.article_id;
        mOwnerName = response.owner_name;
        mStatus = response.status;
        mOwnerId = response.owner_id;
        mCreatedAt = response.created_at;
        mAvatar = response.avatar;
        mComment = response.comment;
        mEnable = response.enable;
    }

    @Override
    public int getId() {
        return mOwnerId;
    }

    @Override
    public String getName() {
        return mComment;
    }

    @Override
    public int getViewType() {
        return Constants.RecyclerViewType.FEED_VIEW_TYPE;
    }

    public int getArticleId() {
        return mArticleId;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public String getComment() {
        return mComment;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }


    public Spanned getCommentWithUsername() {
        if (mOwnerName == null || mOwnerName.length() == 0) mOwnerName = "Genki User";
        return Html.fromHtml("<b>" + mOwnerName + "</b> " + mComment);
    }

    public void setComment(String mComment) {
        this.mComment = mComment;
    }

    public void setOwnerName(String mOwnerName) {
        this.mOwnerName = mOwnerName;
    }

    public void setOwnerId(int id){
        this.mOwnerId = id;
    }

    public void setAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public void setArticleId(int mArticleId) {
        this.mArticleId = mArticleId;
    }
}
