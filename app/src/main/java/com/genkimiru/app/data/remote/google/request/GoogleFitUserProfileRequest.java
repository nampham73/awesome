package com.genkimiru.app.data.remote.google.request;

public class GoogleFitUserProfileRequest {

    public String token;

    public GoogleFitUserProfileRequest(String token) {
        this.token = token;
    }
}
