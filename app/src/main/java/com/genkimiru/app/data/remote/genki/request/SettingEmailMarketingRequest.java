package com.genkimiru.app.data.remote.genki.request;

public class SettingEmailMarketingRequest {

    public  int email_marketing_setting;
    public String token;

    public SettingEmailMarketingRequest(int email_marketing_setting, String token) {
        this.email_marketing_setting = email_marketing_setting;
        this.token = token;
    }
}
