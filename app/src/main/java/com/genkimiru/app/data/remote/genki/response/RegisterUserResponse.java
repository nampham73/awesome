package com.genkimiru.app.data.remote.genki.response;

public class RegisterUserResponse {

    public String token;
    public int role_id;

    public RegisterUserResponse(String token, int role_id) {
        this.token = token;
        this.role_id = role_id;
    }
}
