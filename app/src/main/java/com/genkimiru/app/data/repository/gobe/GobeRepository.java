package com.genkimiru.app.data.repository.gobe;

import com.genkimiru.app.data.remote.gobe.request.GobeAuthorizationRequest;
import com.genkimiru.app.data.remote.gobe.response.GobeAuthorizationResponse;

import io.reactivex.Flowable;

public interface GobeRepository {
    Flowable<GobeAuthorizationResponse> authorize(GobeAuthorizationRequest request);
}
