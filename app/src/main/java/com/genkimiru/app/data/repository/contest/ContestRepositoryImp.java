package com.genkimiru.app.data.repository.contest;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.base.util.JsonGenerator;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.remote.genki.GenkiApiErrorException;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.ListContestRequest;
import com.genkimiru.app.data.remote.genki.response.ContestDetailResponse;
import com.genkimiru.app.data.remote.genki.response.ContestModelResponse;
import com.genkimiru.app.data.remote.genki.response.ListContestResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Single;
import timber.log.Timber;

import static com.genkimiru.app.Constants.Network.HTTP_STATUS_OK;

public class ContestRepositoryImp implements ContestRepository {
    private GenkiApi mApi;

    public ContestRepositoryImp(GenkiApi api) {
        this.mApi = api;
    }

    @Override
    public Flowable<List<ContestModelResponse>> fetchingContestPrivateOrPublic(ListContestRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingContests(BuildConfig.HEADER_SERVER_KEY, map).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                ListContestResponse response = responseResult.convertBodyTo(ListContestResponse.class);
                return Single.just(getContests(response));
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<List<ContestModelResponse>> fetchingContestHistories(ListContestRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingContestHistories(BuildConfig.HEADER_SERVER_KEY, map).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                ListContestResponse response = responseResult.convertBodyTo(ListContestResponse.class);
                return Single.just(getContests(response));
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<List<ContestModelResponse>> fetchingContestCurrentUser(ListContestRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingContestCurrentUser(BuildConfig.HEADER_SERVER_KEY, map).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                ListContestResponse response = responseResult.convertBodyTo(ListContestResponse.class);
                return Single.just(getContests(response));
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<List<ContestModelResponse>> fetchingContestRecommend(ListContestRequest request) {
        Map<String, String> map = JsonGenerator.getInstance().convertObjectToMap(request);
        return mApi.fetchingContestRecommend(BuildConfig.HEADER_SERVER_KEY, map).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                ListContestResponse response = responseResult.convertBodyTo(ListContestResponse.class);
                return Single.just(getContests(response));
            }
            return new GenkiApiErrorException(responseResult).toSingleError();
        }).toFlowable();
    }

    @Override
    public Flowable<List<ContestModelResponse>> fetchingContestList(ListContestRequest request) {
        return null;
    }

    @Override
    public Flowable<ContestDetailResponse> fetchingContestDetail(int id, String token) {
        return mApi.fetchingContestDetail(BuildConfig.HEADER_SERVER_VALUE, id, token).flatMap(responseResult -> {
            Timber.d("repository get contest detail status: %s", responseResult.status);
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(responseResult.convertBodyTo(ContestDetailResponse.class));
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }


    private List<ContestModelResponse> getContests(ListContestResponse response) {
        if (response != null) return response.contests;
        return new ArrayList<>();
    }

    @Override
    public Flowable<Boolean> joinContest(JoinRequest request) {
        return mApi.joinContest(BuildConfig.HEADER_SERVER_VALUE, request).flatMap(responseResult -> {
            if (responseResult.status == HTTP_STATUS_OK) {
                return Single.just(true);
            } else {
                return new GenkiApiErrorException(responseResult).toSingleError();
            }
        }).toFlowable();
    }
}
