package com.genkimiru.app.data.remote.genki.request;

public class ChangePasswordRequest {
    String token;
    String password;
    String current_password;

    public ChangePasswordRequest(String token, String password, String current_password) {
        this.token = token;
        this.password = password;
        this.current_password = current_password;
    }
}
