package com.genkimiru.app.data.repository.googlefit;

import com.genkimiru.app.data.remote.google.GoogleApi;
import com.genkimiru.app.data.remote.google.request.GoogleAccessTokenRequest;
import com.genkimiru.app.data.remote.google.request.GoogleFitUserProfileRequest;
import com.genkimiru.app.data.remote.google.response.GoogleAccessTokenResponse;
import com.genkimiru.app.data.remote.google.response.GoogleFitUserProfileResponse;

import io.reactivex.Flowable;
import okhttp3.ResponseBody;

public class GoogleFitRepositoryImp implements GoogleFitRepository {

    private GoogleApi mApi;

    public GoogleFitRepositoryImp(GoogleApi mApi) {
        this.mApi = mApi;
    }

    @Override
    public Flowable<GoogleAccessTokenResponse> getAccessToken(GoogleAccessTokenRequest googleAccessTokenRequest) {
        return mApi.getAccessToken(googleAccessTokenRequest.code,
                googleAccessTokenRequest.redirect_uri,
                googleAccessTokenRequest.client_id,
                googleAccessTokenRequest.client_screct,
                googleAccessTokenRequest.scope,
                googleAccessTokenRequest.grant_type).toFlowable();
    }

    @Override
    public Flowable<GoogleFitUserProfileResponse> getUserProfile(GoogleFitUserProfileRequest googleFitUserProfileRequest) {
        return mApi.getUserprofile(googleFitUserProfileRequest.token).toFlowable();
    }

    @Override
    public Flowable<ResponseBody> getImageRequest(String url) {
        return mApi.downloadImage(url).toFlowable();
    }
}
