package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.LoginResponse;
import com.genkimiru.app.data.remote.genki.response.UserInfoResponse;

import java.io.Serializable;

import static com.genkimiru.app.data.remote.genki.request.RegisterUserRequest.Gender.MALE;

public class GenkiUserInfoModel implements Serializable {

    private int mId;
    private String mUsername;
    private int mRoleId;
    private String mEmail;
    private String mBirthday;
    private int mGender= MALE;
    private String mAvatar;
    private String mFirstName;
    private String mLastName;
    private String mNickName;
    private String mAddress;
    private String mCode;
    private String mCodeLifeTime;
    private String mCreatedAt;
    private String mUpdatedAt;
    private float mHeight;
    private int mHeightType;
    private boolean mHealthFlag;
    private boolean mProfileFlag;
    private boolean mAnnouncementFlag;
    private boolean mAdvicesFlag;
    private boolean mContestNotificationsFlag;
    private boolean mEmailMarketingSettingFlag;
    private float mWeight;
    private int mWeightType;
    private String mBloodPressure;
    private int mBloodPressureUnder;
    private int mBloodPressureUpper;
    private String mBloodType;
    private int mSurgeryStatus;
    private int mFeelCondition;
    private int mSmokingLevel;
    private int mAlcoholLevel;
    private int mSmokingEachTime;
    private int mDrinkEachTime;
    private String mGobeUsername;
    private String mGobePassword;
    private String mFitbitAccessToken;
    private String mFitbitRefreshToken;
    private int mHealthSource;
    private String mGoogleId;
    private String mFacebookId;
    private double mLatitude;
    private double mLongitude;
    private String mGenkiToken;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public int getRoleId() {
        return mRoleId;
    }

    public void setRoleId(int roleId) {
        mRoleId = roleId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String birthday) {
        mBirthday = birthday;
    }

    public int getGender() {
        return mGender;
    }

    public void setGender(int gender) {
        mGender = gender;
    }

    public String getAvatar() {
        return mAvatar;
    }

    public String getRealName() {
        return mFirstName + " " + mLastName;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getNickName() {
        return mNickName;
    }

    public void setNickName(String nickName) {
        mNickName = nickName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getCodeLifeTime() {
        return mCodeLifeTime;
    }

    public void setCodeLifeTime(String codeLifeTime) {
        mCodeLifeTime = codeLifeTime;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public float getHeight() {
        return mHeight;
    }

    public void setHeight(float height) {
        mHeight = height;
    }

    public int getHeightType() {
        return mHeightType;
    }

    public void setHeightType(int heightType) {
        mHeightType = heightType;
    }

    public boolean isHealthFlag() {
        return mHealthFlag;
    }

    public void setHealthFlag(boolean healthFlag) {
        mHealthFlag = healthFlag;
    }

    public boolean isProfileFlag() {
        return mProfileFlag;
    }

    public void setProfileFlag(boolean profileFlag) {
        mProfileFlag = profileFlag;
    }

    public int getBloodPressureUnder() {
        return mBloodPressureUnder;
    }

    public void setBloodPressureUnder(int mBloodPressureUnder) {
        this.mBloodPressureUnder = mBloodPressureUnder;
    }

    public int getBloodPressureUpper() {
        return mBloodPressureUpper;
    }

    public void setBloodPressureUpper(int mBloodPressureUpper) {
        this.mBloodPressureUpper = mBloodPressureUpper;
    }

    public boolean isAnnouncementFlag() {
        return mAnnouncementFlag;
    }

    public void setAnnouncementFlag(boolean announcementFlag) {
        mAnnouncementFlag = announcementFlag;
    }

    public boolean isAdvicesFlag() {
        return mAdvicesFlag;
    }

    public void setAdvicesFlag(boolean advicesFlag) {
        mAdvicesFlag = advicesFlag;
    }

    public boolean isContestNotificationsFlag() {
        return mContestNotificationsFlag;
    }

    public void setContestNotificationsFlag(boolean contestNotificationsFlag) {
        mContestNotificationsFlag = contestNotificationsFlag;
    }

    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight) {
        mWeight = weight;
    }

    public int getWeightType() {
        return mWeightType;
    }

    public void setWeightType(int weightType) {
        mWeightType = weightType;
    }

    public String getBloodPressure() {
        return mBloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        mBloodPressure = bloodPressure;
    }

    public String getBloodType() {
        return mBloodType;
    }

    public void setBloodType(String bloodType) {
        mBloodType = bloodType;
    }

    public int getSurgeryStatus() {
        return mSurgeryStatus;
    }

    public void setSurgeryStatus(int surgeryStatus) {
        mSurgeryStatus = surgeryStatus;
    }

    public int getFeelCondition() {
        return mFeelCondition;
    }

    public void setFeelCondition(int feelCondition) {
        mFeelCondition = feelCondition;
    }

    public int getSmokingLevel() {
        return mSmokingLevel;
    }

    public void setSmokingLevel(int smokingLevel) {
        mSmokingLevel = smokingLevel;
    }

    public int getAlcoholLevel() {
        return mAlcoholLevel;
    }

    public void setAlcoholLevel(int alcoholLevel) {
        mAlcoholLevel = alcoholLevel;
    }

    public int getSmokingEachTime() {
        return mSmokingEachTime;
    }

    public void setSmokingEachTime(int smokingEachTime) {
        mSmokingEachTime = smokingEachTime;
    }

    public int getDrinkEachTime() {
        return mDrinkEachTime;
    }

    public void setDrinkEachTime(int drinkEachTime) {
        mDrinkEachTime = drinkEachTime;
    }

    public String getGobeUsername() {
        return mGobeUsername;
    }

    public void setGobeUsername(String gobeUsername) {
        mGobeUsername = gobeUsername;
    }

    public String getGobePassword() {
        return mGobePassword;
    }

    public void setGobePassword(String gobePassword) {
        mGobePassword = gobePassword;
    }

    public String getFitbitAccessToken() {
        return mFitbitAccessToken;
    }

    public void setFitbitAccessToken(String fitbitAccessToken) {
        mFitbitAccessToken = fitbitAccessToken;
    }

    public String getFitbitRefreshToken() {
        return mFitbitRefreshToken;
    }

    public void setFitbitRefreshToken(String fitbitRefreshToken) {
        mFitbitRefreshToken = fitbitRefreshToken;
    }

    public int getHealthSource() {
        return mHealthSource;
    }

    public void setHealthSource(int healthSource) {
        mHealthSource = healthSource;
    }

    public String getGoogleId() {
        return mGoogleId;
    }

    public void setGoogleId(String googleId) {
        mGoogleId = googleId;
    }

    public String getFacebookId() {
        return mFacebookId;
    }

    public void setFacebookId(String facebookId) {
        mFacebookId = facebookId;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public boolean isEmailMarketingSettingFlag() {
        return mEmailMarketingSettingFlag;
    }

    public void setEmailMarketingSettingFlag(boolean emailMarketingSettingFlag) {
        mEmailMarketingSettingFlag = emailMarketingSettingFlag;
    }

    public void setToken(String token) {
        this.mGenkiToken = token;
    }

    public String getToken() {
        return mGenkiToken;
    }

    public void setValueFromResponse(UserInfoResponse userInfoResponse) {
        mId = userInfoResponse.id;
        mUsername = userInfoResponse.username;
        mRoleId = userInfoResponse.role_id;
        mEmail = userInfoResponse.email;
        mBirthday = userInfoResponse.birthday;
        mGender = userInfoResponse.gender==0?MALE:userInfoResponse.gender;
        mAvatar = userInfoResponse.avatar;
        mFirstName = userInfoResponse.first_name;
        mLastName = userInfoResponse.last_name;
        mNickName = userInfoResponse.nick_name;
        mAddress = userInfoResponse.address;
        mCode = userInfoResponse.code;
        mCodeLifeTime = userInfoResponse.code_life_time;
        mCreatedAt = userInfoResponse.created_at;
        mUpdatedAt = userInfoResponse.updated_at;
        mHeight = userInfoResponse.height;
        mBloodPressureUnder = userInfoResponse.blood_pressure_under;
        mBloodPressureUpper = userInfoResponse.blood_pressure_upper;
        mHeightType = userInfoResponse.height_type;
        mHealthFlag = userInfoResponse.health_flag;
        mProfileFlag = userInfoResponse.profile_flag;
        mAnnouncementFlag = userInfoResponse.announcement_flag;
        mAdvicesFlag = userInfoResponse.advices_flag;
        mContestNotificationsFlag = userInfoResponse.contest_notifications_flag;
        mEmailMarketingSettingFlag = userInfoResponse.email_marketing_setting;
        mWeight = userInfoResponse.weight;
        mWeightType = userInfoResponse.weight_type;
        mBloodPressure = userInfoResponse.blood_pressure;
        mBloodType = userInfoResponse.blood_type;
        mSurgeryStatus = userInfoResponse.surgery_status;
        mFeelCondition = userInfoResponse.feel_condition;
        mSmokingLevel = userInfoResponse.smoking_level;
        mAlcoholLevel = userInfoResponse.alcohol_level;
        mSmokingEachTime = userInfoResponse.smoking_each_time;
        mDrinkEachTime = userInfoResponse.drink_each_time;
        mGobeUsername = userInfoResponse.gobe_username;
        mGobePassword = userInfoResponse.gobe_password;
        mFitbitAccessToken = userInfoResponse.fitbit_access_token;
        mFitbitRefreshToken = userInfoResponse.fitbit_refresh_token;
        mHealthSource = userInfoResponse.health_source;
        mGoogleId = userInfoResponse.google_id;
        mFacebookId = userInfoResponse.facebook_id;
        mLatitude = userInfoResponse.latitude;
        mLongitude = userInfoResponse.longitude;
        mGenkiToken = userInfoResponse.token;
    }

    public void setValueFromResponse(LoginResponse loginResponse) {
        mId = loginResponse.id;
        mUsername = loginResponse.username;
        mRoleId = loginResponse.role_id;
        mEmail = loginResponse.email;
        mBirthday = loginResponse.birthday;
        mGender = loginResponse.gender==0?MALE:loginResponse.gender;
        mAvatar = loginResponse.avatar;
        mFirstName = loginResponse.first_name;
        mLastName = loginResponse.last_name;
        mNickName = loginResponse.nick_name;
        mAddress = loginResponse.address;
        mHealthFlag = loginResponse.health_flag;
        mProfileFlag = loginResponse.profile_flag;
        mEmailMarketingSettingFlag = loginResponse.email_marketing_setting;
        mLatitude = loginResponse.latitude;
        mLongitude = loginResponse.longitude;
    }
}
