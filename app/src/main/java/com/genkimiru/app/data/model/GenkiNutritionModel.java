package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.NutriResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenkiNutritionModel implements Serializable {
    private long timestamp;
    private float kcal;
    private float fat;
    private float protein;
    private float carb;
    private float second;
    private float kcalAverage;
    private List<NutriManualModel> manual_list;

    public GenkiNutritionModel() {

    }

    public GenkiNutritionModel(long timestamp, float kcal, float fat, float protein, float carb, float second, float kcalAverage, List<NutriManualModel> manual_list) {
        this.timestamp = timestamp;
        this.kcal = kcal;
        this.fat = fat;
        this.protein = protein;
        this.carb = carb;
        this.second = second;
        this.kcalAverage = kcalAverage;
        this.manual_list = manual_list;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float getKcal() {
        return kcal;
    }

    public void setKcal(float kcal) {
        this.kcal = kcal;
    }

    public float getFat() {
        return fat;
    }

    public void setFat(float fat) {
        this.fat = fat;
    }

    public float getProtein() {
        return protein;
    }

    public void setProtein(float protein) {
        this.protein = protein;
    }

    public float getCarb() {
        return carb;
    }

    public void setCarb(float carb) {
        this.carb = carb;
    }

    public float getSecond() {
        return second;
    }

    public void setSecond(float second) {
        this.second = second;
    }

    public float getKcalAverage() {
        return kcalAverage;
    }

    public void setKcalAverage(float kcalAverage) {
        this.kcalAverage = kcalAverage;
    }

    public List<NutriManualModel> getManual_list() {
        return manual_list;
    }

    public void setManual_list(List<NutriManualModel> manual_list) {
        this.manual_list = manual_list;
    }

    public static GenkiNutritionModel convert(NutriResponse response) {
        List<NutriManualModel> manuals = new ArrayList<>();
        for (NutriResponse.Manual resManual : response.manual_list) {
            NutriManualModel manual = new NutriManualModel(
                    resManual.id,
                    resManual.kcal_in,
                    resManual.fat_in,
                    resManual.food_name,
                    resManual.protein_in,
                    resManual.carbonhydrate_in,
                    resManual.start_time,
                    resManual.end_time);
            manuals.add(manual);
        }
        return new GenkiNutritionModel(
                response.timestamp,
                response.kcal_in,
                response.fat_in,
                response.protein_in,
                response.carbonhydrate_in,
                response.second,
                response.average_kcal_in,
                manuals);
    }

    public static class NutriManualModel implements Serializable{
        private int id;
        private float kcal;
        private float fat;
        private String food;
        private float protein;
        private float carb;
        private long start;
        private long end;

        public NutriManualModel(int id, float kcal, float fat, String food, float protein, float carb, long start, long end) {
            this.id = id;
            this.kcal = kcal;
            this.fat = fat;
            this.food = food;
            this.protein = protein;
            this.carb = carb;
            this.start = start;
            this.end = end;
        }

        public NutriManualModel(float kcal, float fat, String food, float protein, float carb, long start, long end) {
            this.kcal = kcal;
            this.fat = fat;
            this.food = food;
            this.protein = protein;
            this.carb = carb;
            this.start = start;
            this.end = end;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public float getKcal() {
            return kcal;
        }

        public void setKcal(float kcal) {
            this.kcal = kcal;
        }

        public float getFat() {
            return fat;
        }

        public void setFat(float fat) {
            this.fat = fat;
        }

        public String getFood() {
            return food;
        }

        public void setFood(String food) {
            this.food = food;
        }

        public float getProtein() {
            return protein;
        }

        public void setProtein(float protein) {
            this.protein = protein;
        }

        public float getCarb() {
            return carb;
        }

        public void setCarb(float carb) {
            this.carb = carb;
        }

        public long getStart() {
            return start;
        }

        public void setStart(long start) {
            this.start = start;
        }

        public long getEnd() {
            return end;
        }

        public void setEnd(long end) {
            this.end = end;
        }
    }
}
