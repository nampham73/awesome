package com.genkimiru.app.data.remote.extension.response;

public class BugReportUploadImageResponse {

    public Upload upload;

    public class Upload {
        public String token;
    }
}
