package com.genkimiru.app.data.remote.genki;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.base.net.BaseRestApi;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class GenkiRestApi extends BaseRestApi<GenkiApi> {

    private static final String BASE_URL = BuildConfig.DATA_SERVER_URL + BuildConfig.DATA_SERVER_API_VERSION;

    public GenkiRestApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        super(client, gsonConverter, rxAdapter);
    }

    @Override
    protected String registerBaseUrl() {
        return BASE_URL;
    }

    @Override
    protected Class<GenkiApi> registerApiClassType() {
        return GenkiApi.class;
    }
}
