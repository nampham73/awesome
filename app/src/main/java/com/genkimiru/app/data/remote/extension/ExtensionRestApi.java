package com.genkimiru.app.data.remote.extension;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.base.net.BaseRestApi;

import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ExtensionRestApi extends BaseRestApi<ExtensionApi> {

    private static final String BASE_URL = BuildConfig.DATA_SERVER_URL_EXTENSION;

    public ExtensionRestApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        super(client, gsonConverter, rxAdapter);
    }

    @Override
    protected String registerBaseUrl() {
        return BASE_URL;
    }

    @Override
    protected Class<ExtensionApi> registerApiClassType() {
        return ExtensionApi.class;
    }
}
