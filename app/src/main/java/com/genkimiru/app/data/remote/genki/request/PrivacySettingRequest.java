package com.genkimiru.app.data.remote.genki.request;

public class PrivacySettingRequest {

    public int health_flag;
    public int profile_flag;
    public String token;

    public PrivacySettingRequest(int health_flag, int profile_flag, String token) {
        this.health_flag = health_flag;
        this.profile_flag = profile_flag;
        this.token = token;
    }
}
