package com.genkimiru.app.data.remote.genki.request;

import com.google.gson.annotations.Expose;

import java.io.File;

public class RegisterUserRequest {

    public String username;
    public String password;
    public String email;
    public String birthday;
    public int gender = 1;
    public String first_name;
    public String last_name;
    public float height = 175;
    public int height_type = 1;
    public float weight = 65;
    public int weight_type = 1;
    public int blood_pressure_under;
    public int blood_pressure_upper;
    public String blood_type;
    public int surgery_status;
    public int feel_condition = 1;
    public int smoking_level = 1;
    public int alcohol_level = 7;
    public String gobe_username = "";
    public String gobe_password = "";
    public float latitude;
    public float longitude;
    public String nick_name = "";
    public String address;
    public int smoking_each_time = -1;
    public int drink_each_time = -1;
    public int health_source;
    public transient File avatar;
    public String fitbit_access_token;
    public String fitbit_refresh_token;
    public long fitbit_expires_in;
    @Expose(serialize = false, deserialize = false)
    public String avatarPath;
    public boolean mIsLoginFlag;

    public enum HealthSource {

        GOBE(1),
        FITBIT(2),
        APPLE_KIT(3),
        SMARTPHONE(4),
        GOOGLE_FIT(5);

        private int value;

        HealthSource(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum DrinkLevel {
        LEVEL_NO_DRINK(-1),
        NONE(0),
        LEVEL_1(1),
        LEVEL_2(2),
        LEVEL_3(3),
        LEVEL_4(4),
        LEVEL_5(5),
        LEVEL_6(6),
        LEVEL_7(7);

        private int value;

        DrinkLevel(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DrinkLevel getFromValue(int value) {
            for (DrinkLevel drinkLevel : DrinkLevel.values()) {
                if (drinkLevel.getValue() == value) {
                    return drinkLevel;
                }
            }
            return NONE;
        }

    }
    public enum DrinkEachTime {
        NONE(0),
        LEVEL_1_180(1),
        LEVEL_2_360(2),
        LEVEL_3_540(3),
        LEVEL_4_720(4),
        LEVEL_5_900(5),
        LEVEL_6_MORE_900(6);

        private int value;

        DrinkEachTime(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DrinkEachTime getFromValue(int value) {
            for (DrinkEachTime drinkEachTime : DrinkEachTime.values()) {
                if (drinkEachTime.getValue() == value) {
                    return drinkEachTime;
                }
            }
            return NONE;
        }
    }

    public enum SmokingLevel {
        NONE(0),
        LEVEL_NO_SMOKING(1),
        LEVEL_SOMETIME_SMOKING(2),
        LEVEL_ALLWAYS_SMOKING(3);

        private int value;

        SmokingLevel(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static SmokingLevel getFromValue(int value) {
            for(SmokingLevel smokingLevel: SmokingLevel.values()) {
                if(smokingLevel.getValue() == value) {
                    return smokingLevel;
                }
            }
            return NONE;
        }
    }

    public enum SmokingEachTime {
        NONE(0),
        LEVEL_DEFAULT(-1),
        LEVEL_SMALL(1),
        LEVEL_MEDIUM(2),
        LEVEL_LARGE(3);

        private int value;

        SmokingEachTime(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static SmokingEachTime getFromValue(int value) {
            for(SmokingEachTime smokingEachTimel: SmokingEachTime.values()) {
                if(smokingEachTimel.getValue() == value) {
                    return smokingEachTimel;
                }
            }
            return NONE;
        }
    }

    public @interface Gender {
        int MALE = 1;
        int FEMALE = 2;
    }

    public @interface SurgeryStatus {
        int YES = 1;
        int NO = 0;
    }

    public @interface FeelCondition {
        int VERY_GOOD = 1;
        int GOOD = 2;
        int BAD = 3;
        int VERY_BAD = 4;
    }

    public void resetData() {
        username = null;
        password = null;
        email = null;
        birthday = null;
        gender = 1;
        first_name = null;
        last_name = null;
        height = 175;
        height_type = 1;
        weight = 65;
        weight_type = 1;
//        blood_pressure = null;
        blood_pressure_upper = 120;
        blood_pressure_under = 80;
        blood_type = null;
        surgery_status = 0;
        feel_condition = 1;
        smoking_level = 1;
        alcohol_level = 7;
        gobe_username = "";
        gobe_password = "";
        latitude =0;
        longitude=0;
        nick_name = "";
        address=null;
        smoking_each_time = -1;
        drink_each_time = -1;
        health_source = 0;
        avatar = null;
        avatarPath  = null;
    }
}
