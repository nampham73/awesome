package com.genkimiru.app.data.repository.fitbit;

import com.genkimiru.app.data.remote.fitbit.request.FitbitAccessTokenRequest;
import com.genkimiru.app.data.remote.fitbit.request.FitbitUserProfileRequest;
import com.genkimiru.app.data.remote.fitbit.response.FitbitAccessTokenResponse;
import com.genkimiru.app.data.remote.fitbit.response.FitbitUserProfileResponse;

import io.reactivex.Flowable;

public interface FitbitRepository {
    Flowable<FitbitAccessTokenResponse> getAccessToken(FitbitAccessTokenRequest fitbitAccessTokenRequest);
    Flowable<FitbitUserProfileResponse> getUserProfile(FitbitUserProfileRequest fitbitUserProfileRequest);
}
