package com.genkimiru.app.data.repository.mydata;

import com.genkimiru.app.data.model.GenkiDashboardDataModel;
import com.genkimiru.app.data.model.GenkiEnergyModel;
import com.genkimiru.app.data.model.GenkiStepModel;
import com.genkimiru.app.data.model.GenkiNutritionModel;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DashboardRequest;
import com.genkimiru.app.data.remote.genki.request.DeleteManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.data.remote.genki.request.UpdateManualInputRequest;
import com.genkimiru.app.widget.SleepChartView;

import java.util.List;

import io.reactivex.Flowable;

public interface MyDataRepository {
    Flowable<GenkiDashboardDataModel> fetchingDashboard(DashboardRequest request);
    Flowable<List<SleepChartView.Entry>> fetchingDetailSleep(DetailChartDataRequest request);
    Flowable<List<GenkiStepModel>> fetchingDetailStep(DetailChartDataRequest request);
    Flowable<List<GenkiNutritionModel>> fetchingDetailNutrition(DetailChartDataRequest request);
    Flowable<Boolean> addNutritionManualInputData(AddManualInputRequest request);
    Flowable<Boolean> deleteNutritionManualInputData(DeleteManualInputRequest request);
    Flowable<Boolean> updateNutritionManualInputData(UpdateManualInputRequest request);
    Flowable<List<GenkiEnergyModel>> fetchingDetailEnergy(DetailChartDataRequest request);
}
