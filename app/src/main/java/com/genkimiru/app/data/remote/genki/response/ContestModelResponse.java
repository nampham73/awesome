package com.genkimiru.app.data.remote.genki.response;

public class ContestModelResponse {
    public int scope;
    public int reward_point;
    public boolean drinking_habit;
    public float bmi_to;
    public int total_join;
    public int membership_type;
    public String reward_avatar_url;
    public int type;
    public int max_number_award_user;
    public int id;
    public String title;
    public String end_date;
    public String name;
    public String description;
    public String created_at;
    public int current_number_award_user;
    public int gender;
    public String longitude;
    public int age_from;
    public int age_to;
    public boolean enabled;
    public boolean smoking_habit;
    public float bmi_from;
    public String updated_at;
    public String address;
    public int radius;
    public String latitude;
    public String start_date;
    public int user_contest_status;
}
