package com.genkimiru.app.data.remote.genki.response;

public class DashboardDataResponse {
    public SleepResponse sleep;
    public StepResponse step;
    public HeartResponse heart;
    public EnergyResponse energy;
    public BmiResponse bmi;
    public NutriResponse nutri;
    public StressResponse stress;
    public WaterResponse water;
}
