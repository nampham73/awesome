package com.genkimiru.app.data.remote.extension;

import com.genkimiru.app.BuildConfig;
import com.genkimiru.app.data.remote.extension.request.BugReportRequest;
import com.genkimiru.app.data.remote.extension.request.BugReportUploadImageRequest;
import com.genkimiru.app.data.remote.extension.response.BugReportUploadImageResponse;
import com.genkimiru.app.data.remote.extension.response.FaqResponse;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ExtensionApi {

    @POST("report-bug")
    @Headers({"Content-Type:application/json"})
    Single<ResponseBody> reportBug(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body BugReportRequest reportRequest);

    @POST("upload-img-bug")
    @Headers({"Content-Type:application/json"})
    Single<BugReportUploadImageResponse> uploadBugImage(
            @Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
            @Body BugReportUploadImageRequest reportRequest);

    @GET("get-faq")
    @Headers({"Content-Type:application/json"})
    Single<FaqResponse> getFaq(@Header(BuildConfig.HEADER_SERVER_KEY) String appKey,
                               @Query("lang") String lang);
}
