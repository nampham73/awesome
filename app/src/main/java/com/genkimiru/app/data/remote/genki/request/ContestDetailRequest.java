package com.genkimiru.app.data.remote.genki.request;

public class ContestDetailRequest {
    public int id;
    public String token;

    public ContestDetailRequest(int id, String token) {
        this.id = id;
        this.token = token;
    }
}
