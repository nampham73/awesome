package com.genkimiru.app.data.model;

import java.io.Serializable;

public class WeightModel implements Serializable {
    private int weight;
    private String time;

    public WeightModel(int weight, String time) {
        this.weight = weight;
        this.time = time;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getTime() {

        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
