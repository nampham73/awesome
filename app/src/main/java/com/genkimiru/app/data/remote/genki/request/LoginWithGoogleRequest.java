package com.genkimiru.app.data.remote.genki.request;

public class LoginWithGoogleRequest {
    public String code;
    public String access_token;
    public int type;

    public LoginWithGoogleRequest(String access_token) {
        this.code = "defaults";
        this.type = 3;
        this.access_token = access_token;
    }
}
