package com.genkimiru.app.data.remote.google.request;

public class GoogleAccessTokenRequest {

    public String code;
    public String redirect_uri;
    public String client_id;
    public String client_screct;
    public String scope;
    public String grant_type;

    public GoogleAccessTokenRequest(String code, String redirect_uri, String client_id, String client_screct, String scope, String grant_type) {
        this.code = code;
        this.redirect_uri = redirect_uri;
        this.client_id = client_id;
        this.client_screct = client_screct;
        this.scope = scope;
        this.grant_type = grant_type;
    }
}
