package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.DashboardResponse;

public class GenkiDashboardModel {
    private int status;
    private String message;
    private GenkiDashboardDataModel data;

    public GenkiDashboardModel(int status, String message, GenkiDashboardDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public GenkiDashboardDataModel getData() {
        return data;
    }

    public static GenkiDashboardModel convert(DashboardResponse response) {
        return new GenkiDashboardModel(response.status, response.message, GenkiDashboardDataModel.convert(response.data));
    }
}
