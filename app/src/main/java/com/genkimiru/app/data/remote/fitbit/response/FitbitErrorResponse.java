package com.genkimiru.app.data.remote.fitbit.response;

import java.util.List;

public class FitbitErrorResponse {

    public List<Error> errors;
    public boolean success;

    public class Error {
        public String errorType;
        public String message;
    }
}
