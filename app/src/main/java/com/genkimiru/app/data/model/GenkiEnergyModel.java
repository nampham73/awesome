package com.genkimiru.app.data.model;

import com.genkimiru.app.data.remote.genki.response.EnergyResponse;

public class GenkiEnergyModel {
    private long timestamp;
    private float kcal_in;
    private float kcal_out;
    private float second;
    private float average_kcal_in;
    private float average_kcal_out;

    public GenkiEnergyModel() {}


    public GenkiEnergyModel(long timestamp,
                            float kcal_in,
                            float kcal_out,
                            float second,
                            float average_kcal_in,
                            float average_kcal_out) {
        this.timestamp = timestamp;
        this.kcal_in = kcal_in;
        this.kcal_out = kcal_out;
        this.second = second;
        this.average_kcal_in = average_kcal_in;
        this.average_kcal_out = average_kcal_out;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getKCalIn() {
        return kcal_in;
    }

    public float getKCalOut() {
        return kcal_out;
    }

    public float getSecond() {
        return second;
    }

    public float getAverageKCalIn() {
        return average_kcal_in;
    }

    public float getAverageKCalOut() {
        return average_kcal_out;
    }

    public static GenkiEnergyModel convert(EnergyResponse response) {
        return new GenkiEnergyModel(response.timestamp,
                response.kcal_in,
                response.kcal_out,
                response.second,
                response.average_kcal_in,
                response.average_kcal_out);
    }
}
