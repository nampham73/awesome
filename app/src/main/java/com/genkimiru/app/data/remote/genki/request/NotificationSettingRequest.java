package com.genkimiru.app.data.remote.genki.request;

public class NotificationSettingRequest {

    public int announcement;
    public int advices;
    public int contest_notifications;
    public int push_notifications;
    public String token;

    public NotificationSettingRequest(int announcement, int advices, int contest_notifications, int push_notifications, String token) {
        this.announcement = announcement;
        this.advices = advices;
        this.contest_notifications = contest_notifications;
        this.push_notifications = push_notifications;
        this.token = token;
    }
}
