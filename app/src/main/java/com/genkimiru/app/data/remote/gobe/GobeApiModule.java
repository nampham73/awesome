package com.genkimiru.app.data.remote.gobe;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GobeApiModule {

    @Provides
    @Singleton
    GobeApi provideGobeApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        GobeRestApi gobeRestApi = new GobeRestApi(client, gsonConverter, rxAdapter);
        return gobeRestApi.getApi();
    }
}
