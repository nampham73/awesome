package com.genkimiru.app.data.remote.genki;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GenkiApiModule {

    @Provides
    @Singleton
    GenkiApi provideGenkiApi(OkHttpClient client, GsonConverterFactory gsonConverter, RxJava2CallAdapterFactory rxAdapter) {
        GenkiRestApi genkiRestApi = new GenkiRestApi(client, gsonConverter, rxAdapter);
        return genkiRestApi.getApi();
    }
}
