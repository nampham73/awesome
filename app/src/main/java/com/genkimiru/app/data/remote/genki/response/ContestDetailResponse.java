package com.genkimiru.app.data.remote.genki.response;

import com.genkimiru.app.data.model.GenkiChallengeModel;
import com.genkimiru.app.data.model.GenkiUserContestChallenge;

import java.util.List;

public class ContestDetailResponse {

    public int id;
    public String name;
    public int type;
    public String description;
    public String start_date;
    public String end_date;
    public int reward_point;
    public String reward_avatar_url;
    public String detail_image_url;
    public String title;
    public int scope;
    public int max_number_award_user;
    public int current_number_award_user;
    public String address;
    public float latitude;
    public float longitude;
    public int radius;
    public float bmi_from;
    public float bmi_to;
    public int age_from;
    public int age_to;
    public int gender;
    public boolean drinking_habit;
    public boolean smoking_habit;
    public String created_at;
    public String updated_at;
    public int total_join;
    public int user_contest_status;
    public List<GenkiChallengeModel> challenges;
    public List<GenkiUserContestChallenge> user_contest_challenges;
}
