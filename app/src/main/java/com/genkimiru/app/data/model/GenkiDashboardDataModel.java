package com.genkimiru.app.data.model;

import com.genkimiru.app.base.util.JsonGenerator;
import com.genkimiru.app.data.remote.genki.response.DashboardDataResponse;

public class GenkiDashboardDataModel {
    private GenkiSleepModel sleep;
    private GenkiStepModel step;
    private GenkiHeartModel heart;
    private GenkiEnergyModel energy;
    private GenkiBmiModel bmi;
    private GenkiNutritionModel nutri;
    private GenkiStressModel stress;
    private GenkiWaterModel water;
    private boolean isEmpty = true;

    public GenkiDashboardDataModel(){
    }

    public GenkiDashboardDataModel(GenkiSleepModel sleep,
                                   GenkiStepModel step,
                                   GenkiHeartModel heart,
                                   GenkiEnergyModel energy,
                                   GenkiBmiModel bmi,
                                   GenkiNutritionModel nutri,
                                   GenkiStressModel stress,
                                   GenkiWaterModel water) {
        isEmpty = false;
        this.sleep = sleep;
        this.step = step;
        this.heart = heart;
        this.energy = energy;
        this.bmi = bmi;
        this.nutri = nutri;
        this.stress = stress;
        this.water = water;
    }

    public GenkiSleepModel getSleep() {
        return sleep;
    }

    public GenkiStepModel getStep() {
        return step;
    }

    public GenkiHeartModel getHeart() {
        return heart;
    }

    public GenkiEnergyModel getEnergy() {
        return energy;
    }

    public GenkiBmiModel getBmi() {
        return bmi;
    }

    public GenkiNutritionModel getNutri() {
        return nutri;
    }

    public GenkiStressModel getStress() {
        return stress;
    }

    public GenkiWaterModel getWater() {
        return water;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void copy(GenkiDashboardDataModel genkiDashboardDataModel) {
        isEmpty = false;
        this.sleep = genkiDashboardDataModel.sleep;
        this.step = genkiDashboardDataModel.step;
        this.heart = genkiDashboardDataModel.heart;
        this.energy = genkiDashboardDataModel.energy;
        this.bmi = genkiDashboardDataModel.bmi;
        this.nutri = genkiDashboardDataModel.nutri;
        this.stress = genkiDashboardDataModel.stress;
        this.water = genkiDashboardDataModel.water;
    }

    public static GenkiDashboardDataModel convert(DashboardDataResponse response) {
        return new GenkiDashboardDataModel(GenkiSleepModel.convert(response.sleep),
                GenkiStepModel.convert(response.step),
                GenkiHeartModel.convert(response.heart),
                GenkiEnergyModel.convert(response.energy),
                GenkiBmiModel.convert(response.bmi),
                GenkiNutritionModel.convert(response.nutri),
                GenkiStressModel.convert(response.stress),
                GenkiWaterModel.convert(response.water));
    }

    @Override
    public String toString() {
        return "[" + GenkiDashboardDataModel.class.getSimpleName() + "]"
                + JsonGenerator.getInstance().convertJson(this);
    }
}
