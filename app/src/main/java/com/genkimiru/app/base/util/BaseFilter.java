package com.genkimiru.app.base.util;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseFilter<TModel> {

    protected abstract boolean isValid(TModel model);

    public List<TModel> filter(List<TModel> modelList) {
        List<TModel> filteredList = new ArrayList<>();
        if (StaticMethods.isNotEmpty(modelList)) {
            for (TModel model : modelList) {
                if (isValid(model)) {
                    filteredList.add(model);
                }
            }
        }
        return filteredList;
    }
}