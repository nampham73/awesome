package com.genkimiru.app.base.mvp.view;

import android.support.annotation.NonNull;
import android.view.View;

import com.genkimiru.app.base.dialog.BaseDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.ButterKnife;

public abstract class BaseDialogFragmentView<TDialogFragment extends BaseDialogFragment, TPresenter extends BasePresenter>
        extends BaseViewImp<TPresenter> {

    protected TDialogFragment mDialogFragment;
    protected TPresenter mPresenter;
    private View mView;

    public BaseDialogFragmentView (TDialogFragment dialogFragment) {
        super();
        mDialogFragment = dialogFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        mView = view;
        ButterKnife.bind(this, mView);
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void setPresenter(TPresenter presenter) {
        mPresenter = presenter;
    }
}
