package com.genkimiru.app.base.permission;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Permissions {
    static final int GRANTED = 0;
    static final int DENIED = 1;
    static final int NOT_FOUND = 2;

    @IntDef({GRANTED, DENIED, NOT_FOUND})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    @Type
    private final int type;

    public Permissions(@Type int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
