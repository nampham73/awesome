package com.genkimiru.app.base.util;

public enum ImageType {
    SMALL_AVATAR,
    NORMAL_AVATAR
}
