package com.genkimiru.app.base.util;

import android.content.Context;
import android.widget.Toast;

import com.genkimiru.app.base.dialog.TextDialogFragment;

public class MessageUtil {

    public static TextDialogFragment createMessageDialog(Context context, int messageId) {
        String message = context.getResources().getString(messageId);
        return createMessageDialog( message);
    }

    public static TextDialogFragment createMessageDialog(String message) {
        return new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .message(message)
                .leftButtonText("OK")
                .onResultListener(new TextDialogFragment.OnResultListener() {
                    @Override
                    public void onClickLeftButton(TextDialogFragment textDialogFragment) {
                        textDialogFragment.dismiss();
                    }

                    @Override
                    public void onClickRightButton(TextDialogFragment textDialogFragment) {
                        // Do nothing
                    }
                })
                .build();
    }

    public static TextDialogFragment createMessageDialog(String message, TextDialogFragment.OnResultListener resultListener) {
        return new TextDialogFragment.TextDialogBuilder()
                .cancelable(false)
                .message(message)
                .leftButtonText("OK")
                .onResultListener(resultListener)
                .build();
    }

    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
