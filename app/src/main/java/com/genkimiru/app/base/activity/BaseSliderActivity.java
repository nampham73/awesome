package com.genkimiru.app.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.PreconditionUtil;
import com.genkimiru.app.base.widget.CustomViewPager;

public abstract class BaseSliderActivity<TPagerAdapter extends FragmentPagerAdapter, TPresenter extends BasePresenter>
        extends SimpleBaseActivity<TPresenter> {

    protected CustomViewPager mViewPager;
    protected TabLayout mTabLayout;
    protected TPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewPager = getViewPager();
        mTabLayout = getTabLayout();
        mPagerAdapter = getPagerAdapter();
        initializePager();
    }

    protected abstract CustomViewPager getViewPager();
    protected abstract TabLayout getTabLayout();
    protected abstract TPagerAdapter getPagerAdapter();

    private void initializePager() {
        PreconditionUtil.checkMustNotNull(mViewPager);
        PreconditionUtil.checkMustNotNull(mTabLayout);
        PreconditionUtil.checkMustNotNull(mPagerAdapter);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
