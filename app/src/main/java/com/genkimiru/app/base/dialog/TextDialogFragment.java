package com.genkimiru.app.base.dialog;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.StaticMethods;

import butterknife.BindView;

public class TextDialogFragment extends SimpleBaseDialogFragment {

    @BindView(R.id.message_text)
    TextView messageTextView;
    @BindView(R.id.left_button)
    Button leftButton;
    @BindView(R.id.right_bound)
    View rightBoundView;
    @BindView(R.id.right_button)
    Button rightButton;
    @BindView(R.id.title_text)
    TextView titleTv;

    private Spanned spannedMessage;
    private String title;
    private String message;
    private String leftButtonText;
    private String rightButtonText;
    private int leftButtonColor;
    private int rightButtonColor;
    private OnResultListener onResultListener;

    public static class TextDialogBuilder {
        private boolean cancelable;
        private String title;
        private String message;
        private Spanned spannedMessage;
        private String leftButtonText;
        private String rightButtonText;
        private int leftButtonColor;
        private int rightButtonColor;
        private OnResultListener onResultListener;

        public TextDialogBuilder cancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public TextDialogBuilder title(String title) {
            this.title = title;
            return this;
        }

        public TextDialogBuilder message(Spanned spannedMessage) {
            this.spannedMessage = spannedMessage;
            return this;
        }

        public TextDialogBuilder message(String message) {
            this.message = message;
            return this;
        }

        public TextDialogBuilder leftButtonText(String text) {
            leftButtonText = text;
            return this;
        }

        public TextDialogBuilder rightButtonText(String text) {
            rightButtonText = text;
            return this;
        }

        public TextDialogBuilder leftButtonColor(int leftButtonIDColor) {
            this.leftButtonColor = leftButtonIDColor;
            return this;
        }

        public TextDialogBuilder rightButtonColor(int rightButtonIDColor) {
            this.rightButtonColor = rightButtonIDColor;
            return this;
        }

        public TextDialogBuilder onResultListener(OnResultListener onResultListener) {
            this.onResultListener = onResultListener;
            return this;
        }

        public TextDialogFragment build() {

            if (spannedMessage != null) {
                return new TextDialogFragment(cancelable, spannedMessage, leftButtonText, rightButtonText, onResultListener);
            }
            if (title != null) {
                return new TextDialogFragment(cancelable, title, message, leftButtonText, rightButtonText, leftButtonColor, rightButtonColor, onResultListener);
            }
            return new TextDialogFragment(cancelable, message, leftButtonText, rightButtonText, onResultListener);
        }
    }

    public TextDialogFragment() {
        // Do nothing
    }

    @SuppressLint("ValidFragment")
    public TextDialogFragment(boolean cancelable,
                              String message,
                              String leftButtonText,
                              String rightButtonText,
                              OnResultListener onResultListener) {
        this.message = message;
        this.leftButtonText = leftButtonText;
        this.rightButtonText = rightButtonText;
        this.onResultListener = onResultListener;

        setCancelable(cancelable);
    }

    @SuppressLint("ValidFragment")
    public TextDialogFragment(boolean cancelable,
                              String title,
                              String message,
                              String leftButtonText,
                              String rightButtonText,
                              int leftButtonColor,
                              int rightButtonColor,
                              OnResultListener onResultListener) {
        this.title = title;
        this.message = message;
        this.leftButtonText = leftButtonText;
        this.rightButtonText = rightButtonText;
        this.leftButtonColor = leftButtonColor;
        this.rightButtonColor = rightButtonColor;
        this.onResultListener = onResultListener;

        setCancelable(cancelable);
    }


    @SuppressLint("ValidFragment")
    public TextDialogFragment(boolean cancelable,
                              Spanned message,
                              String leftButtonText,
                              String rightButtonText,
                              OnResultListener onResultListener) {
        this.spannedMessage = message;
        this.leftButtonText = leftButtonText;
        this.rightButtonText = rightButtonText;
        this.onResultListener = onResultListener;

        setCancelable(cancelable);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);

        if (StaticMethods.isNotEmpty(message)) {
            messageTextView.setVisibility(View.VISIBLE);
            messageTextView.setText(message);
        }

        if (spannedMessage != null) {
            messageTextView.setVisibility(View.VISIBLE);
            messageTextView.setText(spannedMessage);
        }

        if (StaticMethods.isNotEmpty(leftButtonText)) {
            leftButton.setVisibility(View.VISIBLE);
            leftButton.setText(leftButtonText);
            leftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onResultListener != null) {
                        onResultListener.onClickLeftButton(TextDialogFragment.this);
                    }
                    dismiss();
                }
            });

            if (leftButtonColor != 0) {
                leftButton.setTextColor(getContext().getResources().getColor(leftButtonColor));
            }
        }

        if (StaticMethods.isNotEmpty(rightButtonText)) {
            rightButton.setVisibility(View.VISIBLE);
            rightBoundView.setVisibility(View.VISIBLE);
            rightButton.setText(rightButtonText);
            rightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onResultListener != null) {
                        onResultListener.onClickRightButton(TextDialogFragment.this);
                    }
                    dismiss();
                }
            });
            if (rightButtonColor != 0) {
                rightButton.setTextColor(getContext().getResources().getColor(rightButtonColor));
            }
        } else {
            rightBoundView.setVisibility(View.GONE);
        }

        if (StaticMethods.isNotEmpty(title)) {
            titleTv.setVisibility(View.VISIBLE);
            titleTv.setText(title);
        } else {
            titleTv.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_text;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    public interface OnResultListener {
        void onClickLeftButton(TextDialogFragment textDialogFragment);
        void onClickRightButton(TextDialogFragment textDialogFragment);
    }
}
