package com.genkimiru.app.base.mvp.presenter;

import android.support.annotation.StringRes;

public interface ListContract {

    interface RootView<T> {
        void onPrepareLoadingData();

        void onLoadingDataFailure(Throwable throwable, @StringRes int message);

        void onLoadingDataFailure(Throwable throwable, String message);

        void onDataLoadDone(T data);
    }

    interface ListPresenter extends BasePresenter {
        void start();

        void requestLoadingMore();

        void setPullDownToRefresh(boolean status);
    }

    interface Router {
    }
}
