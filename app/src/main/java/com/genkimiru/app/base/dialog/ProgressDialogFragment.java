package com.genkimiru.app.base.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public class ProgressDialogFragment extends SimpleBaseDialogFragment {

    public static ProgressDialogFragment create() {
        return new ProgressDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.FragmentTransparentDialog);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_progress;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
