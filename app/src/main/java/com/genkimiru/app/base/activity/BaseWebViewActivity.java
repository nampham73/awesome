package com.genkimiru.app.base.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public abstract class BaseWebViewActivity<TPresenter extends BasePresenter>
        extends SimpleBaseActivity<TPresenter> {

    protected WebView mWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWebView = getWebView();
        showUrlToWebViewWith(getUrl());
    }

    public void showUrlToWebViewWith(String url) {
        setupWebView();
        mWebView.loadUrl(url);
    }

    protected abstract String getUrl();

    protected abstract WebView getWebView();

    protected abstract boolean isEnableJavaScript();

    private void setupWebView() {
        mWebView.getSettings().setJavaScriptEnabled(isEnableJavaScript());
        mWebView.setWebViewClient(new WebViewContentClient());
    }

    private class WebViewContentClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgress();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            hideProgress();
        }
    }
}
