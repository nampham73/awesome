package com.genkimiru.app.base.util;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.subjects.PublishSubject;


public class LoadMoreRecyclerOnScrollListener extends OnScrollListener {
    private final LinearLayoutManager mLayoutManager;
    private final PublishSubject<RecyclerView> mSubject = PublishSubject.create();

    public LoadMoreRecyclerOnScrollListener(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (mLayoutManager.findLastCompletelyVisibleItemPosition() == recyclerView.getAdapter().getItemCount() - 1) {
            mSubject.onNext(recyclerView);
        }
    }

    public Flowable<RecyclerView> observable() {
        return mSubject.toFlowable(BackpressureStrategy.LATEST);
    }
}