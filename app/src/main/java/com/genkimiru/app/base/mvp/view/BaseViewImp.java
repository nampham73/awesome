package com.genkimiru.app.base.mvp.view;

import android.view.View;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.RxLifecycle;

import io.reactivex.subjects.BehaviorSubject;

public abstract class BaseViewImp<TPresenter extends BasePresenter> implements BaseView<TPresenter> {

    protected enum Event {
        INIT,
        CREATE,
        VIEW_CREATED,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    protected BehaviorSubject<BaseViewImp.Event> lifecycleSubject = BehaviorSubject.create();

    public BaseViewImp() {
        lifecycleSubject.onNext(Event.INIT);
    }

    @Override
    public void onCreate() {
        lifecycleSubject.onNext(Event.CREATE);
    }

    @Override
    public void onViewCreated(View view) {
        lifecycleSubject.onNext(Event.VIEW_CREATED);
    }

    @Override
    public void onResume() {
        lifecycleSubject.onNext(Event.RESUME);
    }

    @Override
    public void onPause() {
        lifecycleSubject.onNext(Event.PAUSE);
    }

    @Override
    public void onStop() {
        lifecycleSubject.onNext(Event.STOP);
    }

    @Override
    public void onDestroy() {
        lifecycleSubject.onNext(Event.DESTROY);
    }

    protected <T> LifecycleTransformer<T> bindUntilPause() {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, Event.PAUSE);
    }

    protected <T> LifecycleTransformer<T> bindUntilDestroy() {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, Event.DESTROY);
    }
}
