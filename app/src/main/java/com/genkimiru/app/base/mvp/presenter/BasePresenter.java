package com.genkimiru.app.base.mvp.presenter;

public interface BasePresenter {
    void onCreate();
    void onResume();
    void onPause();
    void onStop();
    void onDestroy();
}
