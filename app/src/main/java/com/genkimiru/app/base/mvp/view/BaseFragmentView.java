package com.genkimiru.app.base.mvp.view;

import android.support.annotation.NonNull;
import android.view.View;

import com.genkimiru.app.base.fragment.BaseFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.ButterKnife;

public abstract class BaseFragmentView<TFragment extends BaseFragment, TPresenter extends BasePresenter>
        extends BaseViewImp<TPresenter> {

    protected TFragment mFragment;
    protected TPresenter mPresenter;
    private View mView;

    public BaseFragmentView(TFragment fragment) {
        super();
        mFragment = fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        mView = view;
        ButterKnife.bind(this, mView);
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void setPresenter(TPresenter presenter) {
        mPresenter = presenter;
    }
}
