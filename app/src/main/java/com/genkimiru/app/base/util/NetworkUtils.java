package com.genkimiru.app.base.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {

    private NetworkUtils() {
        throw new IllegalAccessError("NetworkUtils class");
    }

    /**
     * check network is connecting or not.
     *
     * @param con
     * @return true if network is Ok. Otherwise return false.
     */
    public static boolean isConnecting(Context con) {
        boolean isConnecting = false;
        if (con == null) {
            return false;
        }

        final ConnectivityManager cm = (ConnectivityManager) con
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            final NetworkInfo info = cm.getActiveNetworkInfo();
            if (info != null) {
                isConnecting = info.isConnectedOrConnecting();
            }

        }

        return isConnecting;
    }

    public static boolean isNotConnecting(Context con) {
        return !isConnecting(con);
    }
}
