package com.genkimiru.app.base.interactor;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.DisposableSubscriber;

public abstract class UseCase<TRepository, TParam> {

    private final Scheduler mBackgroundScheduler;
    private final Scheduler mPostedScheduler;
    protected TRepository mRepository;
    private CompositeDisposable mCompositeDisposable;

    protected UseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, TRepository repository) {
        mBackgroundScheduler = backgroundScheduler;
        mPostedScheduler = postedScheduler;
        mRepository = repository;
        mCompositeDisposable = new CompositeDisposable();
    }

    protected abstract Flowable buildUseCase(TParam param);

    public void execute(DisposableSubscriber subscriber, TParam params) {
        buildUseCase(params)
                .subscribeOn(mBackgroundScheduler)
                .observeOn(mPostedScheduler)
                .subscribeWith(subscriber);
        addDisposable(subscriber);
    }

    public void clear() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    private void addDisposable(Disposable disposable) {
        if (mCompositeDisposable == null || disposable == null) {
            return;
        }
        mCompositeDisposable.add(disposable);
    }
}
