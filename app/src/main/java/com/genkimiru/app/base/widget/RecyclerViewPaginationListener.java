package com.genkimiru.app.base.widget;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import timber.log.Timber;

public abstract class RecyclerViewPaginationListener extends RecyclerView.OnScrollListener {
    private RecyclerView.LayoutManager mLayoutManager;
    private int visibleThreshold = 5;
    private int currentPage = 1;
    private int previousTotalItemCount = 0;
    private int startingPageIndex = 0;
    private int mLastVisiblePosition = 0;

    public RecyclerViewPaginationListener(RecyclerView.LayoutManager manager) {
        this.mLayoutManager = manager;
        if (manager instanceof GridLayoutManager) {
            visibleThreshold = visibleThreshold * ((GridLayoutManager) manager).getSpanCount();
        } else if (manager instanceof StaggeredGridLayoutManager) {
            visibleThreshold = visibleThreshold * ((StaggeredGridLayoutManager) manager).getSpanCount();
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        onOriginalScrolled(recyclerView, dx, dy);

        int totalItemCount = mLayoutManager.getItemCount();
        if (mLayoutManager instanceof StaggeredGridLayoutManager) {
            int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mLayoutManager)
                    .findLastVisibleItemPositions(null);
            mLastVisiblePosition = getLastVisibleItem(lastVisibleItemPositions);
        } else if (mLayoutManager instanceof LinearLayoutManager) {
            mLastVisiblePosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else if (mLayoutManager instanceof GridLayoutManager) {
            mLastVisiblePosition = ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        }
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
        }
        Timber.w("pre visible: " + mLastVisiblePosition + ", total: " + totalItemCount);
        if ((mLastVisiblePosition > visibleThreshold)
                && (mLastVisiblePosition + visibleThreshold) > totalItemCount) {
            currentPage++;
            loadMore();
            previousTotalItemCount = totalItemCount;
        }
    }

    protected abstract void loadMore();

    protected void onOriginalScrolled(RecyclerView recyclerView, int dx, int dy) {
        ///keep default scrolled property of recyclerView.
    }

    private int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int minSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                minSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] < minSize) {
                minSize = lastVisibleItemPositions[i];
            }
        }
        return minSize;
    }
}
