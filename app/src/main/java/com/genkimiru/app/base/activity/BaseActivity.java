package com.genkimiru.app.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;

import timber.log.Timber;

public abstract class BaseActivity<TPresenter extends BasePresenter, TView extends BaseView> extends SimpleBaseActivity<TPresenter> {

    private TView mView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mView != null) {
            mView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mView != null) {
            mView.onPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mView != null) {
            mView.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mView != null) {
            mView.onDestroy();
        }
    }

    protected abstract TView registerView();

    private void initializeView() {
        mView = registerView();
        if (mView == null) {
            Timber.w("Activity didn't register view!");
            return;
        }
        mView.onCreate();
        mView.setPresenter(registerPresenter());
        mView.onViewCreated(getWindow().getDecorView());
    }
}
