package com.genkimiru.app.base.interactor;

import io.reactivex.subscribers.DisposableSubscriber;

public class DefaultSubscriber<T> extends DisposableSubscriber<T> {

    @Override
    public void onNext(T t) {

    }

    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void onComplete() {

    }
}
