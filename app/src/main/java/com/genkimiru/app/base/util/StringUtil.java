package com.genkimiru.app.base.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public static final String EMPTY = "";
    private static final String LINKIT_CHATROOM_PREFIX = ",";
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,32}");

    private StringUtil() {
        throw new IllegalAccessError("StringUtil class");
    }

    public static boolean isEmpty(CharSequence text) {
        return text == null || text.length() == 0;
    }

    public static boolean isNotEmpty(CharSequence text) {
        return text != null && text.length() > 0;
    }

    public static boolean isEmpty(String text) {
        return text == null || text.trim().length() == 0;
    }

    public static boolean isNotEmpty(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static String getNullSafeString(String string) {
        return string != null ? string : "";
    }

    public static String getEmptySafeString(String string) {
        return !isEmpty(string) ? string : "-";
    }

    public static String randomString(int length) {
        StringBuilder buffer = new StringBuilder();
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        int charactersLength = characters.length();

        for (int i = 0; i < length; i++) {
            double index = Math.random() * charactersLength;
            buffer.append(characters.charAt((int) index));
        }
        return buffer.toString();
    }

    public static Spanned formatHtml(String string) {
        return Html.fromHtml(string);
    }

    public static boolean isValidLength(String string, int length) {
        return string.length() >= length;
    }

    public static boolean equals(String firstString, String secondString) {
        return TextUtils.equals(firstString, secondString);
    }

    public static String arrayToStringWithComma(Collection<?> arrays) {
        if (StaticMethods.isEmpty(arrays)) {
            return "";
        }
        StringBuilder b = new StringBuilder();
        for (Object object : arrays) {
            b.append(String.valueOf(object));
            b.append(", ");
        }
        String result = b.toString().trim();
        result = result.substring(0, result.length() - 1);
        return result;
    }

    public static String trim(String text) {
        return text.replace(LINKIT_CHATROOM_PREFIX, "").trim();
    }

    public static String toBase64(String string) {
        if (string == null) {
            return "";
        }
        return Base64.encodeToString(string.trim().getBytes(), Base64.NO_WRAP);
    }

    public static boolean validateEmailFormat(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public static String formatNumber(double num) {
        DecimalFormat df2 = new DecimalFormat("#,###,###,###.#");
        return df2.format(num);
    }

    public static String formatUnnecessaryZero(double num)
    {
        if(num == (long) num)
            return String.format(Locale.US, "%d",(long)num);
        else
            return String.format("%s",num);
    }

    @SuppressLint("PackageManagerGetSignatures")
    public static void generateHashKeyForFacebook(Context context) {
        PackageInfo info;
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }
}