package com.genkimiru.app.base.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class JsonGenerator {
    private static JsonGenerator sInstance;
    private Gson gson;

    public static JsonGenerator getInstance() {
        if (sInstance == null) {
            sInstance = new JsonGenerator();
        }
        return sInstance;
    }

    private JsonGenerator() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public String convertJson(Object object) {
        if (gson == null) {
            return null;
        }
        return gson.toJson(object);
    }

    public boolean mayBeJSON(String string) {
        return string != null
                && ("null".equals(string)
                || (string.startsWith("[") && string.endsWith("]"))
                || (string.startsWith("{") && string.endsWith("}")));
    }

    public <T> T parserJson(String json, Type type) {
        if (mayBeJSON(json)) {
            return gson.fromJson(json, type);
        }
        return null;
    }

    public <T> T parserJson(JsonElement json, Type type) {
        return gson.fromJson(json, type);
    }

    public Map<String, String> convertObjectToMap(Object o) {
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        String json = convertJson(o);
        if (mayBeJSON(json)) {
            return parserJson(json, type);
        }
        return null;
    }
}
