package com.genkimiru.app.base.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.presenter.ListContract;
import com.genkimiru.app.base.widget.RecyclerViewPaginationListener;

import butterknife.BindView;

public abstract class ListFragment<TAdapter extends Adapter, TData, TPresenter extends ListContract.ListPresenter>
        extends SimpleBaseFragment<TPresenter>
        implements ListContract.RootView<TData>,
        SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    protected Context mContext;
    protected TAdapter mAdapter;

    @NonNull
    protected abstract TAdapter createAdapter();

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_list_view;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    protected void initViews() {
        initSwipeRefreshLayout();
        initRecyclerView(mRecyclerView);
    }

    @Override
    public void onPrepareLoadingData() {
        if (mRefreshLayout != null && mRefreshLayout.isRefreshing()) {
            return;
        }
        showProgress();
    }

    @Override
    public void onLoadingDataFailure(Throwable throwable, int message) {
        hideProgress();
        if (mRefreshLayout != null) {
            mRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onLoadingDataFailure(Throwable throwable, String message) {
        hideProgress();
        if (mRefreshLayout != null) {
            mRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onDataLoadDone(TData data) {
        hideProgress();
        mRefreshLayout.setRefreshing(false);
        getPresenter().setPullDownToRefresh(false);
    }

    private void initSwipeRefreshLayout() {
        if (mRefreshLayout != null) {
            mRefreshLayout.setOnRefreshListener(this);
            mRefreshLayout.setColorSchemeColors(
                    ContextCompat.getColor(mContext, R.color.colorBlue),
                    ContextCompat.getColor(mContext, R.color.colorPrimary),
                    ContextCompat.getColor(mContext, R.color.colorPrimaryDark),
                    ContextCompat.getColor(mContext, R.color.colorRed),
                    ContextCompat.getColor(mContext, R.color.colorAccent));
            setEnableSwipeToRefresh(canSwipeToRefresh());
        }
    }

    protected void initRecyclerView(RecyclerView recyclerView) {
        mAdapter = createAdapter();
        LinearLayoutManager manager = getLayoutManagerOfRecyclerView();
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(getDefaultOnScrollListenerOfRecyclerView(recyclerView));
    }

    public LinearLayoutManager getLayoutManagerOfRecyclerView() {
        return new LinearLayoutManager(mContext);
    }

    public void setEnableSwipeToRefresh(boolean enable) {
        if (mRefreshLayout == null) {
            return;
        }
        mRefreshLayout.setEnabled(enable);
    }

    protected boolean canSwipeToRefresh() {
        return false;
    }

    protected abstract void showProgress();

    protected abstract void hideProgress();

    protected abstract TPresenter getPresenter();

    protected RecyclerView.OnScrollListener getDefaultOnScrollListenerOfRecyclerView(@NonNull RecyclerView recyclerView) {
        return new RecyclerViewPaginationListener(recyclerView.getLayoutManager()) {
            @Override
            protected void loadMore() {
                if (getPresenter() != null) getPresenter().requestLoadingMore();
            }
        };
    }

    @Override
    public void onRefresh() {
        mRefreshLayout.setRefreshing(true);
        getPresenter().setPullDownToRefresh(true);
        getPresenter().start();
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }
}
