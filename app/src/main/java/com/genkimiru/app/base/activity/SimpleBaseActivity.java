package com.genkimiru.app.base.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.genkimiru.app.base.dialog.ProgressDialogFragment;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.ToolbarUtil;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;
import timber.log.Timber;

public abstract class SimpleBaseActivity<TPresenter extends BasePresenter> extends AppCompatActivity
        implements HasFragmentInjector, HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> supportFragmentInjector;
    @Inject
    DispatchingAndroidInjector<android.app.Fragment> frameworkFragmentInjector;

    protected ToolbarUtil mToolBarUtils;

    private TPresenter mPresenter;
    private ProgressDialogFragment mProgressDialogFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (isNeedInjection()) {
            AndroidInjection.inject(this);
        }
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());
        ButterKnife.bind(this);
        initializePresenter();
        initializeToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPresenter != null) {
            mPresenter.onPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mPresenter != null) {
            mPresenter.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
    }

    protected void initializeToolbar() {
        mToolBarUtils = new ToolbarUtil(this);
        // Implement at sub class if needed
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return isNeedInjection() ? supportFragmentInjector : null;
    }

    @Override
    public AndroidInjector<android.app.Fragment> fragmentInjector() {
        return isNeedInjection() ? frameworkFragmentInjector : null;
    }

    public void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        IBinder windowsToken = view.getWindowToken();
        if (windowsToken != null) {
            InputMethodManager ipm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (ipm == null) {
                return;
            }
            ipm.hideSoftInputFromWindow(windowsToken, 0);
        }
    }

    public void showKeyboard() {
        InputMethodManager inputMethodManager = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
        if (inputMethodManager == null) {
            return;
        }
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void showProgress() {
        if (mProgressDialogFragment == null) {
            mProgressDialogFragment = ProgressDialogFragment.create();
            mProgressDialogFragment.setCancelable(false);
            mProgressDialogFragment.show(getSupportFragmentManager());
        }
    }

    public void hideProgress() {
        if (mProgressDialogFragment != null) {
            mProgressDialogFragment.dismissAllowingStateLoss();
            mProgressDialogFragment = null;
        }
    }

    protected abstract int getContentViewId();
    protected abstract TPresenter registerPresenter();
    protected abstract boolean isNeedInjection();

    private void initializePresenter() {
        mPresenter = registerPresenter();
        if (mPresenter == null) {
            Timber.w("Activity didn't register presenter!");
            return;
        }
        mPresenter.onCreate();
    }
}
