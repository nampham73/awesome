package com.genkimiru.app.base.mvp.presenter;

import android.content.Context;

import timber.log.Timber;

public class ListPresenterImp<TView extends ListContract.RootView, TRouter> extends BasePresenterImp<TView, TRouter> implements ListContract.ListPresenter {
    private int mItemCountFromResponse;
    protected int mNextPage = 0;
    private boolean isLoadingRequesting, isPullDownToRefresh;
    private boolean isFirstLoad = true;

    public ListPresenterImp(Context context, TView tView, TRouter tRouter) {
        super(context, tView, tRouter);
    }

    protected int getPostPerPage() {
        return 20;
    }

    protected int getLimit() {
        return mNextPage + getPostPerPage();
    }

    protected int getNextPage() {
        if (isFirstLoad) {
            isFirstLoad = false;
            return 0;
        }
        if (isPullDownToRefresh)return 0;
        if (!isLoadingRequesting) {
            mNextPage += getPostPerPage();
        }
        return mNextPage;
    }

    @Override
    public void start() {
        Timber.d("request loading data from server with nextPage: " + mNextPage + ", limit: " + getPostPerPage());
        mView.onPrepareLoadingData();
    }

    @Override
    public void requestLoadingMore() {
        if (canLoadMore()) start();
    }

    protected void reCalculatedPaging(int itemCount) {
        mItemCountFromResponse = itemCount;
    }

    private boolean canLoadMore() {
        return !isLoadingRequesting && mItemCountFromResponse == getPostPerPage();
    }

    public void setLoadingRequesting(boolean loadingRequesting) {
        isLoadingRequesting = loadingRequesting;
    }

    public void setNextPage(int mNextPage) {
        isFirstLoad = true;
        this.mNextPage = mNextPage;
    }

    @Override
    public void setPullDownToRefresh(boolean pullDownToRefresh) {
        isPullDownToRefresh = pullDownToRefresh;
    }
}
