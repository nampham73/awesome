package com.genkimiru.app.base.converter;

import com.genkimiru.app.base.model.BaseModel;

public abstract class BaseConverter<TDto, TModel extends BaseModel> {

    protected TDto tDto;
    protected TModel tModel;

    public BaseConverter(TDto dto, TModel model) {
        tDto = dto;
        tModel = model;
    }

    public abstract TModel toModel();
    public abstract TDto toDto();
}
