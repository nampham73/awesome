package com.genkimiru.app.base.util;

public final class PreconditionUtil {

    private PreconditionUtil() {
        throw new IllegalAccessError("PreconditionUtil class");
    }
    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     *
     * @param reference an object reference
     * @return the non-null reference that was validated
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> T checkMustNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    public static void checkMustNotNull(Object reference, String errorMessage) {
        if (reference == null) {
            throw new NullPointerException(errorMessage);
        }
    }

    public static void checkMustBeNotZero(int value, String errorMessage) {
        if (value == 0) {
            throw new UnsupportedOperationException(errorMessage);
        }
    }

    public static void checkValidDay(int value, String errorMessage) {
        checkMustNotNull(value, errorMessage);
        if (value < 1 || value > 31) {
            throw new UnsupportedOperationException(errorMessage);
        }
    }

    public static void checkValidMonth(int value, String errorMessage) {
        checkMustNotNull(value, errorMessage);
        if (value < 0 || value > 11) {
            throw new UnsupportedOperationException(errorMessage);
        }
    }
}
