package com.genkimiru.app.base.mvp.view;

import android.view.View;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;

public interface BaseView<TPresenter extends BasePresenter> {
    void onCreate();
    void onViewCreated(View view);
    void onResume();
    void onPause();
    void onStop();
    void onDestroy();
    View getView();
    void setPresenter(TPresenter presenter);
}
