package com.genkimiru.app.base.mvp.presenter;

import android.content.Context;

import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.RxLifecycle;

import io.reactivex.subjects.BehaviorSubject;

public class BasePresenterImp<TView, TRouter> implements BasePresenter {

    protected Context mContext;
    protected TView mView;
    protected TRouter mRouter;

    protected enum Event {
        INIT,
        CREATE,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    protected BehaviorSubject<Event> lifecycleSubject = BehaviorSubject.create();

    public BasePresenterImp(Context context, TView view, TRouter router) {
        mContext = context;
        mView = view;
        mRouter = router;
        lifecycleSubject.onNext(Event.INIT);
    }

    @Override
    public void onCreate() {
        lifecycleSubject.onNext(Event.CREATE);
    }

    @Override
    public void onResume() {
        lifecycleSubject.onNext(Event.RESUME);
    }

    @Override
    public void onPause() {
        lifecycleSubject.onNext(Event.PAUSE);
    }

    @Override
    public void onStop() {
        lifecycleSubject.onNext(Event.STOP);
    }

    @Override
    public void onDestroy() {
        lifecycleSubject.onNext(Event.DESTROY);
    }

    protected <T> LifecycleTransformer<T> bindUntilPause() {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, Event.PAUSE);
    }

    protected <T> LifecycleTransformer<T> bindUntilDestroy() {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, Event.DESTROY);
    }
}
