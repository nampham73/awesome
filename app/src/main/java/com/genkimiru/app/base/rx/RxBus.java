package com.genkimiru.app.base.rx;

import io.reactivex.Flowable;
import io.reactivex.subjects.PublishSubject;

import static io.reactivex.BackpressureStrategy.LATEST;

public class RxBus {

    public RxBus() {
    }

    private PublishSubject<Object> bus = PublishSubject.create();

    public void send(Object object) {
        bus.onNext(object);
    }

    public Flowable<Object> toFlowable() {
        return bus.toFlowable(LATEST);
    }
}
