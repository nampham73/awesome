package com.genkimiru.app.base.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class StorageUtil {

    private StorageUtil() {
        throw new IllegalAccessError("StorageUtil class");
    }

    public static File getProfileImagePath(Context context) {
        File filesDir = context.getFilesDir();
        File path = new File(filesDir, "UserImages");
        return path;
    }

    public static File getProfileImagePath(Context context, String userImage, ImageType imageType) {
        if (StringUtil.isEmpty(userImage)) {
            return new File("");
        }
        File identity = getProfileImagePath(context);
        File imageTypeFile = new File(identity, imageType.toString());

        File userImageFiles = new File(imageTypeFile, userImage);
        userImageFiles.mkdirs();
        String fileName = userImage + ".tmp";

        return new File(userImageFiles, fileName);
    }

    public static String getInternalImagePath(Context context, String fileName) {
        StringBuilder builder = new StringBuilder(context.getFilesDir().getAbsolutePath());
        builder.append(File.separator);
        builder.append(fileName);
        return builder.toString();
    }

    public static String getExternalImagePath(String fileName) {
        StringBuilder builder = new StringBuilder(Environment.getExternalStorageDirectory().getAbsolutePath());
        builder.append(File.separator);
        builder.append(fileName);
        return builder.toString();
    }

    public static Observable<Boolean> clearFile(String filePath) {
        return Observable.defer(() -> {
            File file = new File(filePath);
            boolean success = false;
            if (file.exists()) {
                success = file.delete();
            }
            return Observable.just(success);
        }).subscribeOn(Schedulers.io());
    }

    public static void syncFileSystem() {
        Timber.d("Execute sync command");
        Runtime r = Runtime.getRuntime();
        try {
            r.exec("sync"); r.exec("sync"); r.exec("sync");
        } catch (IOException e) {
            Timber.d(e, "Failed to exec sync command");
        }
    }
}

