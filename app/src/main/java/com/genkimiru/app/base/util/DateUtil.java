package com.genkimiru.app.base.util;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.text.TextUtils;

import com.genkimiru.app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

import static com.genkimiru.app.base.util.DateUtil.Format.DAY_MONTH_YEAR_FORMAT;
import static com.genkimiru.app.base.util.DateUtil.Format.HOUR_DAY_MONTH_YEAR_FORMAT;
import static com.genkimiru.app.base.util.DateUtil.Format.HOUR_DAY_MONTH_YEAR_FORMAT_JAP;
import static com.genkimiru.app.base.util.DateUtil.Format.MONTH_DAY_YEAR_HOURS_MIN_SECOND_FORMAT;
import static com.genkimiru.app.base.util.DateUtil.Format.YEAR_MONTH_DAY_FORMAT;
import static java.util.Locale.JAPANESE;

public class DateUtil {
    private static final String YEAR_2016 = "/2016";
    public static final int LEAP_YEAR_DEFAULT = 2016;

    private DateUtil() {
        throw new IllegalAccessError("DateUtil class");
    }

    public static String getYearMonthDayString(Calendar calendar, Locale locale) {
        if (locale == null || !locale.getLanguage().equals("ja")) {
            return DateFormat.API_YEAR_MONTH_DAY_FORMAT.format(calendar.getTime());
        }
        return DateFormat.YEAR_MONTH_DAY_FORMAT_JAP.format(calendar.getTime());
    }

    public static String getWeekDayMonthYearString(Calendar calendar, Locale locale) {
        if (locale == null || !locale.getLanguage().equals("ja")) {
            return DateFormat.DAY_MONTH_YEAR_WEEK_FORMAT.format(calendar.getTime());
        }
        return DateFormat.DAY_MONTH_YEAR_WEEK_FORMAT_JAP.format(calendar.getTime());
    }

    public static String getMonthDayString(Calendar calendar) {
        return DateFormat.MONTH_DAY_FORMAT_JAP.format(calendar.getTime());
    }

    public static String getYearMonthDayForApi(Calendar calendar) {
        return DateFormat.API_YEAR_MONTH_DAY_FORMAT.format(calendar.getTime());
    }

    public static String getYearMonthDayHoursForApi(Calendar calendar) {
        return DateFormat.YEAR_MONTH_DAY_HOURS_API_FORMAT.format(calendar.getTime());
    }

    public static String getMonthDayForApi(Calendar calendar) {
        return DateFormat.API_MONTH_DAY_FORMAT.format(calendar.getTime());
    }

    public static String getDayOfWeek(Calendar calendar) {
        return DateFormat.SHORT_WEEK_FORMAT.format(calendar.getTime());
    }

    public static String getCustomISO8601Day(Calendar calendar) {
        return DateFormat.CUSTOM_ISO8601_FORMAT.format(calendar.getTime());
    }

    public static String getHourMinute(Calendar calendar) {
        return DateFormat.HOUR_MINUTE_FORMAT.format(calendar.getTime());
    }

    public static String getYearMonth(Calendar calendar, Locale locale) {
        return locale.getLanguage().equals(JAPANESE.getLanguage())
                ? DateFormat.YEAR_MONTH_FORMAT_JAP.format(calendar.getTime())
                : DateFormat.YEAR_MONTH_FORMAT.format(calendar.getTime());
    }

    public static Calendar convertDateTimeToCalendar(String dateTime, String dateTimeFormat) {
        if (TextUtils.isEmpty(dateTime)) {
            return null;
        }
        SimpleDateFormat dateFormat = new DefaultDateFormat(dateTimeFormat);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(dateFormat.parse(dateTime));
            return calendar;
        } catch (ParseException e) {
            Timber.e(e);
        }
        return null;
    }

    public static String getHourMinuteString(Calendar calendar) {
        return DateFormat.SHORT_TIME_FORMAT.format(calendar.getTime());
    }

    public static String getMonthString(Calendar calendar) {
        return DateFormat.SHORT_MONTH_FORMAT.format(calendar.getTime());
    }

    public static String getFullMonthString(Calendar calendar) {
        return DateFormat.FULL_MONTH_FORMAT.format(calendar.getTime());
    }

    public static Calendar getNowTime() {
        return Calendar.getInstance();
    }

    public static String getActivityLogHeader(Calendar calendar) {
        Calendar now = getNowTime();

        int currentYear = now.get(Calendar.YEAR);

        String header = calendar.get(Calendar.YEAR) != currentYear ? getYearMonthDayString(calendar, null) : getMonthDayString(calendar);
        header += " (" + getDayOfWeek(calendar) + ")";

        return header;
    }

    public static Calendar getCalendar(String date, String dateFormat) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new DefaultDateFormat(dateFormat);
            calendar.setTime(sdf.parse(date));
            return calendar;
        } catch (ParseException e) {
            Timber.e(e, "convert date format is failed");
            return null;
        }
    }

    public static String convertDateFormat(String date, String oldDateFormat, String newDateFormat) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new DefaultDateFormat(oldDateFormat);
            calendar.setTime(dateFormat.parse(date));
            SimpleDateFormat sdf = new DefaultDateFormat(newDateFormat);
            return sdf.format(calendar.getTime());
        } catch (ParseException e) {
            Timber.e(e, "convert date format is failed");
            return StringUtil.EMPTY;
        }
    }

    public static String convertContestDateFormat(String dateString, String newFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(newFormat);
        try {
            Date date = simpleDateFormat.parse(dateString);
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return StringUtil.EMPTY;
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertToHourYearMonthDayString(String currentPattern, String val, Locale locale) {
        if (StringUtil.isEmpty(val)) {
            return "n/a";
        }
        try {
            Date date = new SimpleDateFormat(currentPattern).parse(val);
            if (locale == null || !locale.getLanguage().equals("ja")) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(HOUR_DAY_MONTH_YEAR_FORMAT);
                return simpleDateFormat.format(date);
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(HOUR_DAY_MONTH_YEAR_FORMAT_JAP);
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "n/a";
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertToYearMonthDayString(String currentPattern, String val, Locale locale) {
        if (StringUtil.isEmpty(val)) {
            return "n/a";
        }
        try {
            Date date = new SimpleDateFormat(currentPattern).parse(val);
            if (locale == null || !locale.getLanguage().equals("ja")) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT);
                return simpleDateFormat.format(date);
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Format.YEAR_MONTH_DAY_FORMAT_JAP);
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "n/a";
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertToDayMonthYearString(String currentPattern, String val, Locale locale) {
        if (StringUtil.isEmpty(val)) {
            return "n/a";
        }
        try {
            Date date = new SimpleDateFormat(currentPattern).parse(val);
            if (locale == null || !locale.getLanguage().equals("ja")) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DAY_MONTH_YEAR_FORMAT);
                return simpleDateFormat.format(date);
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Format.YEAR_MONTH_DAY_FORMAT_JAP);
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "n/a";
    }

    public static String changeTimeZone(String date, String dateFormat, TimeZone oldTimeZone, TimeZone newTimeZone) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat defaultDateFormat = new DefaultDateFormat(dateFormat);
            calendar.setTime(defaultDateFormat.parse(date));
            long oldTime = calendar.getTime().getTime();
            long timeZoneAlteredTime = oldTime - oldTimeZone.getRawOffset() + newTimeZone.getRawOffset();
            calendar.setTimeInMillis(timeZoneAlteredTime);
            return defaultDateFormat.format(calendar.getTime());
        } catch (ParseException e) {
            Timber.e(e, "change time zone is failed");
            return StringUtil.EMPTY;
        }
    }

    public static String convertDateFormatLeapYear(String date, String oldDateFormat, String newDateFormat) {
        try {
            String leapDate = date + YEAR_2016;
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new DefaultDateFormat(oldDateFormat);
            calendar.setTime(dateFormat.parse(leapDate));
            SimpleDateFormat sdf = new DefaultDateFormat(newDateFormat);
            return sdf.format(calendar.getTime());
        } catch (ParseException e) {
            Timber.e(e, "convert date format is failed");
            return StringUtil.EMPTY;
        }
    }

    public static Calendar getCalendarLeapYear(String date, String dateFormat) {
        try {
            String leapDate = date + YEAR_2016;
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new DefaultDateFormat(dateFormat);
            calendar.setTime(sdf.parse(leapDate));
            return calendar;
        } catch (ParseException e) {
            Timber.e(e, "convert date format is failed");
            return null;
        }
    }

    public static Calendar set(Calendar calendar, int month, int day) {
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        return calendar;
    }

    public static Calendar set(Calendar calendar, int year, int month, int day) {
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar;
    }

    public static Calendar now() {
        return Calendar.getInstance();
    }

    public static Calendar leapYearTo(int year) {
        Calendar calendar = now();
        calendar.set(Calendar.YEAR, year);
        return calendar;
    }

    public static Calendar today() {
        return Calendar.getInstance();
    }

    public static long getTheDayBefore() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTimeInMillis();
    }

    public static long getToday() {
        return today().getTimeInMillis();
    }

    public static long getMidnight() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static String getPrettyTime(Date date) {
        Calendar now = getNowTime();
        Calendar past = Calendar.getInstance();
        past.setTime(date);

        String time = getHourMinuteString(past);

        if (now.get(Calendar.YEAR) != past.get(Calendar.YEAR)) {
            return getYearMonthDayString(past, null);
        }

        if (now.get(Calendar.MONTH) != past.get(Calendar.MONTH) ||
                now.get(Calendar.DAY_OF_MONTH) != past.get(Calendar.DAY_OF_MONTH)) {
            return getMonthDayString(past);
        }

        return time;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getTimeAgo(Resources resources, String val, String pattern, Locale locale) {
        if (val != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                Date date = simpleDateFormat.parse(val);
                return getTimeAgo(resources, date, locale);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return getTimeAgo(resources, getNowTime().getTime(), locale);
    }

    /***
     * This function will round time in hour pattern like 8:38->8:40, 15:01->15:05
     * @return return timestamps
     */
    public static long getCurrentTimeRound(int distance) {
        Date whateverDateYouWant = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(whateverDateYouWant);
        int unroundedMinutes = calendar.get(Calendar.MINUTE);
        int mod = unroundedMinutes % distance;
        calendar.add(Calendar.MINUTE, unroundedMinutes == 0 ? +distance : distance - mod);
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String formattedDate = format1.format(calendar.getTime());
        long result = calendar.getTimeInMillis() / 1000L;
        Timber.v("round-down-timestamp: %s  - %s", formattedDate, String.valueOf(result));
        return result;
    }

    public static String getTimeAgo(Resources resources, Date timeInThePast, Locale locale) {
        Date now = getNowTime().getTime();
        long timeElapsed = now.getTime() - timeInThePast.getTime();
        if (timeElapsed < 0) {
            // Abnormal case
            Timber.e("getTimeAgo >> timeElapsed < 0. Stranger case! It is consider as [Just Now] case!");
            return resources.getString(R.string.just_now);
        }

        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeElapsed);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(timeElapsed);
        long hours = TimeUnit.MILLISECONDS.toHours(timeElapsed);
        if (seconds == 0) {
            return resources.getString(R.string.just_now);
        } else if (inThisMinute(seconds)) {
            return String.format(resources.getString(R.string.seconds_ago), seconds);
        } else if (inThisHour(minutes)) {
            return String.format(resources.getString(R.string.minutes_ago), minutes);
        } else if (inThisDay(hours)) {
            return String.format(resources.getString(R.string.hours_ago), hours);
        } else if (inThisYear(timeInThePast)) {
            if (locale == null || !locale.getLanguage().equals("ja")) {
                return DateFormat.MONTH_DAY_FORMAT.format(timeInThePast);
            }
            return DateFormat.MONTH_DAY_FORMAT_JAP.format(timeInThePast);
        } else {
            if (locale == null || !locale.getLanguage().equals("ja")) {
                return DateFormat.YEAR_MONTH_DAY_FORMAT.format(timeInThePast);
            }
            return DateFormat.YEAR_MONTH_DAY_FORMAT_JAP.format(timeInThePast);
        }
    }

    public static long convertDateToEpochWith(String date) {
        long epoch = 0;
        try {
            epoch = new SimpleDateFormat(MONTH_DAY_YEAR_HOURS_MIN_SECOND_FORMAT).parse(date).getTime();
        } catch (ParseException e) {
            Timber.e(e, "convert date to epoch is failed");
        }
        return epoch;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertDate(String oldPattern, String newPattern, String val) {
        try {
            Date date = new SimpleDateFormat(oldPattern).parse(val);
            return new SimpleDateFormat(newPattern).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "n/a";
    }

    public static List<Calendar> getDaysUntilNow(int range) {
        List<Calendar> results = new ArrayList<>();

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.YEAR, -range);
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        startDate.set(Calendar.MILLISECOND, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);

        for (; startDate.before(endDate); startDate.add(Calendar.DATE, 1)) {
            Calendar calendar = (Calendar) startDate.clone();
            results.add(calendar);
        }

        results.add(endDate);
        return results;
    }

    public static List<Calendar> getMonthsUntilNow(int range) {
        List<Calendar> results = new ArrayList<>();

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.YEAR, -range);
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        startDate.set(Calendar.MILLISECOND, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);

        for (; startDate.before(endDate); startDate.add(Calendar.MONTH, 1)) {
            Calendar calendar = (Calendar) startDate.clone();
            results.add(calendar);
        }

        results.add(endDate);
        return results;
    }

    public static List<Calendar> getWeeksUntilNow(int range) {
        List<Calendar> results = new ArrayList<>();

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.YEAR, -range);
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        startDate.set(Calendar.MILLISECOND, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.set(Calendar.HOUR_OF_DAY, 0);
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);

        for (; startDate.before(endDate); startDate.add(Calendar.DATE, 1)) {
            Calendar calendar = (Calendar) startDate.clone();
            if (calendar.get(Calendar.DAY_OF_WEEK) == calendar.getFirstDayOfWeek()) {
                results.add(calendar);
            }
        }
        return results;
    }

    private static boolean inThisMinute(long seconds) {
        return seconds <= 60;
    }

    private static boolean inThisHour(long minutes) {
        return minutes <= 60;
    }

    private static boolean inThisDay(long hours) {
        return hours <= 24;
    }

    private static boolean inThisYear(Date inputDate) {
        Calendar inputCalendar = Calendar.getInstance();
        inputCalendar.setTime(inputDate);
        int inputYear = inputCalendar.get(Calendar.YEAR);

        Calendar nowCalendar = getNowTime();
        int currentYear = nowCalendar.get(Calendar.YEAR);

        return inputYear == currentYear;
    }

    public interface Format {
        String YEAR_MONTH_DAY_FORMAT_JAP = "yyyy'年'MM'月'dd'日'";
        String DAY_MONTH_YEAR_FORMAT = "dd MMM yyyy";
        String MONTH_DAY_FORMAT_JAP = "MM'月'dd'日'";
        String YEAR_MONTH_DAY_FORMAT = "yyyy/MM/dd";
        String API_YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd";
        String MONTH_DAY_FORMAT = "MM/dd";
        String SHORT_WEEK_FORMAT = "EEE";
        String CUSTOM_ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        String MONTH_DAY_YEAR_HOURS_MIN_SECOND_FORMAT = "MM/dd/yyyy HH:mm:ss";
        String MONTH_DAY_YEAR_FORMAT = "MM/dd/yyyy";
        String SHORT_TIME_FORMAT = "HH:mm";
        String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
        String HOUR_DAY_MONTH_YEAR_FORMAT = "hh:mma, dd MMM yyyy";
        String HOUR_DAY_MONTH_YEAR_FORMAT_JAP = "yyyy'年'MM'月'dd'日', ahh:mm";
        String API_FORMAT = "yyyy-MM-dd hh:mm:ss";
        String SHORT_MONTH_FORMAT = "MMM";
        String FULL_MONTH_FORMAT = "MMMM";
        String HOUR_MINUTE_FORMAT = "HH:mm";
        String WEEK_MONTH_DAY_YEAR_FORMAT = "EEEE, MMM d, yyyy";
        String WEEK_MONTH_DAY_YEAR_FORMAT_JAP = "yyyy'年'MM'月'dd'日'";
        String DAY_MONTH_YEAR_WEEK_FORMAT = "EEEE, yyyy MMM dd";
        String DAY_MONTH_YEAR_WEEK_FORMAT_JAP = "yyyy'年'MM'月'dd'日', (EEEE)";
        String YEAR_MONTH_FORMAT = "yyyy MMMM";
        String YEAR_MONTH_FORMAT_JAP = "yyyy'年'MM'月'";


    }

    private interface DateFormat {
        SimpleDateFormat YEAR_MONTH_DAY_FORMAT_JAP = new DefaultDateFormat(Format.YEAR_MONTH_DAY_FORMAT_JAP);
        SimpleDateFormat YEAR_MONTH_DAY_FORMAT = new DefaultDateFormat(Format.YEAR_MONTH_DAY_FORMAT);
        SimpleDateFormat MONTH_DAY_FORMAT_JAP = new DefaultDateFormat(Format.MONTH_DAY_FORMAT_JAP);
        SimpleDateFormat MONTH_DAY_FORMAT = new DefaultDateFormat(Format.MONTH_DAY_FORMAT);
        SimpleDateFormat YEAR_MONTH_DAY_HOURS_API_FORMAT = new DefaultDateFormat(Format.API_FORMAT);
        SimpleDateFormat API_YEAR_MONTH_DAY_FORMAT = new DefaultDateFormat(Format.API_YEAR_MONTH_DAY_FORMAT);
        SimpleDateFormat SHORT_WEEK_FORMAT = new DefaultDateFormat(Format.SHORT_WEEK_FORMAT);
        SimpleDateFormat API_MONTH_DAY_FORMAT = new DefaultDateFormat(Format.MONTH_DAY_FORMAT);
        SimpleDateFormat SHORT_TIME_FORMAT = new DefaultDateFormat(Format.SHORT_TIME_FORMAT);
        SimpleDateFormat CUSTOM_ISO8601_FORMAT = new DefaultDateFormat(Format.CUSTOM_ISO8601_FORMAT);
        SimpleDateFormat SHORT_MONTH_FORMAT = new DefaultDateFormat(Format.SHORT_MONTH_FORMAT);
        SimpleDateFormat FULL_MONTH_FORMAT = new DefaultDateFormat(Format.FULL_MONTH_FORMAT);
        SimpleDateFormat HOUR_MINUTE_FORMAT = new DefaultDateFormat(Format.HOUR_MINUTE_FORMAT);
        SimpleDateFormat DAY_MONTH_YEAR_WEEK_FORMAT = new DefaultDateFormat(Format.DAY_MONTH_YEAR_WEEK_FORMAT);
        SimpleDateFormat DAY_MONTH_YEAR_WEEK_FORMAT_JAP = new DefaultDateFormat(Format.DAY_MONTH_YEAR_WEEK_FORMAT_JAP);
        SimpleDateFormat YEAR_MONTH_FORMAT = new DefaultDateFormat(Format.YEAR_MONTH_FORMAT);
        SimpleDateFormat YEAR_MONTH_FORMAT_JAP = new DefaultDateFormat(Format.YEAR_MONTH_FORMAT_JAP);
    }
}
