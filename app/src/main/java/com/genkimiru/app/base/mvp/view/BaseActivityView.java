package com.genkimiru.app.base.mvp.view;

import android.support.annotation.NonNull;
import android.view.View;

import com.genkimiru.app.base.activity.BaseActivity;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.ButterKnife;

public abstract class BaseActivityView<TActivity extends BaseActivity, TPresenter extends BasePresenter>
        extends BaseViewImp<TPresenter> {

    protected TActivity mActivity;
    protected TPresenter mPresenter;
    private View mView;

    public BaseActivityView(TActivity activity) {
        super();
        mActivity = activity;
    }

    @Override
    public void onViewCreated(@NonNull View view) {
        super.onViewCreated(view);
        mView = view;
        ButterKnife.bind(this, mView);
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void setPresenter(TPresenter presenter) {
        mPresenter = presenter;
    }
}
