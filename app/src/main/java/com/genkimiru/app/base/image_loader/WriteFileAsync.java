package com.genkimiru.app.base.image_loader;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;

import okhttp3.ResponseBody;

public class WriteFileAsync extends AsyncTask<Void, Void, WriteFileAsync.Result> {
    private OnDownloadCallback callback;
    private static final String TAG = WriteFileAsync.class.getSimpleName();
    private WeakReference<Context> weakReference;
    private ResponseBody body;
    private String fileName;

    public WriteFileAsync(Context context, String fileName, ResponseBody body, OnDownloadCallback callback) {
        this.callback = callback;
        this.weakReference = new WeakReference<>(context);
        this.fileName = fileName;
        this.body = body;
    }

    @Override
    protected WriteFileAsync.Result doInBackground(Void... voids) {
        String path = getPath();
        Throwable throwable = writeResponseBodyToDisk(body, path);
        Result result = new Result();
        result.throwable = throwable;
        result.path = path;
        return result;
    }

    @Override
    protected void onPostExecute(@NonNull Result result) {
        super.onPostExecute(result);
        if (callback != null) {
            if (result.throwable == null) callback.onDownloadDone(result.path);
            else callback.onDownloadFailure(result.throwable);
        }
    }

    public String getPath() {
        return weakReference.get().getFilesDir() + File.separator + fileName;
    }

    private Throwable writeResponseBodyToDisk(@NonNull ResponseBody body, String path) {
        try {
            File futureStudioIconFile = new File(path);
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }
                outputStream.flush();
                return null;
            } catch (IOException e) {
                return e.getCause();
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return e.getCause();
        }
    }

    public interface OnDownloadCallback {
        void onDownloadDone(String absolutePath);

        void onDownloadFailure(Throwable throwable);
    }

    static class Result {
        private String path;
        private Throwable throwable;
    }
}
