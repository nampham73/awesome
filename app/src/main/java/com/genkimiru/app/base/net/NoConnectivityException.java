package com.genkimiru.app.base.net;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    public NoConnectivityException() {
    }

    @Override
    public String getMessage() {
        return "No network connection";
    }
}
