package com.genkimiru.app.base.define;

import java.util.regex.Pattern;

public interface Constants {
    interface Validate {
        Pattern NAME_VALIDATE = Pattern.compile("\\A\\w(\\w|\\s|-){0,19}\\Z");
        Pattern PASSWORD_VALIDATE = Pattern.compile("(?=.*[a-zA-Z])(?=.*\\d).{8,}");
        Pattern POSTAL_CODE_PATTERN = Pattern.compile("\\d{3}-?\\d{4}");
    }
    interface Alpha {
        float ALPHA_ENABLE = 1.0f;
        float ALPHA_DISABLE = 0.2f;
    }
}
