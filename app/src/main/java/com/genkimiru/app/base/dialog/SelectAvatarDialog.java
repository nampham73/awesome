package com.genkimiru.app.base.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import com.genkimiru.app.R;
import com.genkimiru.app.base.mvp.presenter.BasePresenter;

import butterknife.OnClick;

public class SelectAvatarDialog extends SimpleBaseDialogFragment {

    private DialogSelectListener mDialogSelectListener;

    @Override
    protected int getLayoutResource() {
        return R.layout.dialog_avatar_select;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
    }

    public void setDialogSelectListener(DialogSelectListener dialogSelectListener) {
        this.mDialogSelectListener = dialogSelectListener;
    }

    @Override
    protected BasePresenter registerPresenter() {
        return null;
    }

    @Override
    protected boolean isNeedInjection() {
        return false;
    }

    @OnClick(R.id.register_profile_camera_layout)
    void getAvartaFromCamera() {
        mDialogSelectListener.onCameraClick(this);
    }

    @OnClick(R.id.register_profile_gallery_layout)
    void getAvartaFromGallery() {
        mDialogSelectListener.onGalleryClick(this);
    }

    public interface DialogSelectListener {
        void onCameraClick(SelectAvatarDialog selectAvatarDialog);

        void onGalleryClick(SelectAvatarDialog selectAvatarDialog);
    }
}
