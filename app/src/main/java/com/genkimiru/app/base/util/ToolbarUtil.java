package com.genkimiru.app.base.util;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.genkimiru.app.R;

public class ToolbarUtil {

    private AppCompatActivity mActivity;
    private Toolbar toolbar;
    private int titleStringResourceId;
    private String titleString;
    private boolean hasBackButton = false;
    private Runnable mRunWhenBackTask;
    private int menuResourceId;
    private int navigationIconResourceId;
    private int logoResourceId;

    public ToolbarUtil(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    public ToolbarUtil toolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
        return this;
    }

    public ToolbarUtil title(int titleStringResourceId) {
        this.titleStringResourceId = titleStringResourceId;
        return this;
    }

    public ToolbarUtil title(String titleString) {
        this.titleString = titleString;
        return this;
    }

    public ToolbarUtil logo(int logoResourceId) {
        this.logoResourceId = logoResourceId;
        return this;
    }

    public ToolbarUtil addBackButton() {
        this.hasBackButton = true;
        return this;
    }

    public ToolbarUtil runWhenBack(Runnable runnable) {
        this.mRunWhenBackTask = runnable;
        return this;
    }

    public ToolbarUtil menuLayoutResource(int menuResourceId) {
        this.menuResourceId = menuResourceId;
        return this;
    }

    public ToolbarUtil navigationIconId(int navigationIconResourceId) {
        this.navigationIconResourceId = navigationIconResourceId;
        return this;
    }

    public void build() {
        PreconditionUtil.checkMustNotNull(toolbar, "You must set toolbar!");
        mActivity.setSupportActionBar(toolbar);
        PreconditionUtil.checkMustNotNull(mActivity.getSupportActionBar(), "Must set toolbar as activity's actionbar by calling {mActivity.setSupportActionBar(toolbar)}.");
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(hasBackButton);
        mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (menuResourceId != 0) {
            toolbar.inflateMenu(menuResourceId);
        }
//        if (titleStringResourceId != 0) {
//            mActivity.getSupportActionBar().setTitle(titleStringResourceId);
//        } else if (StaticMethods.nonNull(titleString)) {
//            mActivity.getSupportActionBar().setTitle(titleString);
//        }
        mActivity.getSupportActionBar().setTitle("");
        if (navigationIconResourceId != 0) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setNavigationIcon(mActivity.getResources().getDrawable(navigationIconResourceId));
            } else {
                toolbar.setNavigationIcon(mActivity.getResources().getDrawable(navigationIconResourceId, null));
            }
        }

        if (logoResourceId != 0) {
            mActivity.getSupportActionBar().setLogo(logoResourceId);
            mActivity.getSupportActionBar().setDisplayUseLogoEnabled(true);
        }

        if (hasBackButton) {
            toolbar.setNavigationOnClickListener(view -> {
                if (mRunWhenBackTask != null) {
                    mRunWhenBackTask.run();
                }
                mActivity.onBackPressed();
            });
        }
    }
}
