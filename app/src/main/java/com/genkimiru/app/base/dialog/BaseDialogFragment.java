package com.genkimiru.app.base.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.mvp.view.BaseView;

import timber.log.Timber;

public abstract class BaseDialogFragment<TPresenter extends BasePresenter, TView extends BaseView>
        extends SimpleBaseDialogFragment<TPresenter> {

    private TView mView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mView != null) {
            mView.onViewCreated(view);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mView != null) {
            mView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mView != null) {
            mView.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mView != null) {
            mView.onStop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mView != null) {
            mView.onDestroy();
        }
    }

    protected abstract TView registerView();

    private void initializeView() {
        mView = registerView();
        if (mView == null) {
            Timber.w("Dialog fragment didn't register view!");
            return;
        }
        mView.onCreate();
        mView.setPresenter(registerPresenter());
    }
}
