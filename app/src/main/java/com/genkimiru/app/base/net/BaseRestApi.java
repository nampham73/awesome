package com.genkimiru.app.base.net;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseRestApi<TApi> {

    private TApi mApi;

    public BaseRestApi(OkHttpClient client,
                       GsonConverterFactory gsonConverter,
                       RxJava2CallAdapterFactory rxAdapter) {

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(registerBaseUrl())
                .addConverterFactory(gsonConverter)
                .addCallAdapterFactory(rxAdapter).build();
        mApi = retrofit.create(registerApiClassType());
    }

    public TApi getApi() {
        return mApi;
    }

    protected abstract String registerBaseUrl();
    protected abstract Class<TApi> registerApiClassType();
}
