package com.genkimiru.app.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.genkimiru.app.base.mvp.presenter.BasePresenter;
import com.genkimiru.app.base.util.PreconditionUtil;

public abstract class BaseSliderFragment<TPagerAdapter extends FragmentPagerAdapter, TPresenter extends BasePresenter>
        extends SimpleBaseFragment<TPresenter> {

    protected ViewPager mViewPager;
    protected TabLayout mTabLayout;
    TPagerAdapter mPagerAdapter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewPager = getViewPager();
        mTabLayout = getTabLayout();
        mPagerAdapter = getPagerAdapter();
        initializePager();
    }

    protected abstract ViewPager getViewPager();

    protected abstract TabLayout getTabLayout();

    protected abstract TPagerAdapter getPagerAdapter();

    private void initializePager() {
        PreconditionUtil.checkMustNotNull(mViewPager);
        PreconditionUtil.checkMustNotNull(mTabLayout);
        PreconditionUtil.checkMustNotNull(mPagerAdapter);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
