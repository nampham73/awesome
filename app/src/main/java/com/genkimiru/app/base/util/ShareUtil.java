package com.genkimiru.app.base.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

public class ShareUtil {
    public static Intent twitter(Context context, String message) {
        Intent shareIntent;
        if (doesPackageExist(context, "com.twitter.android")) {
            shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setClassName("com.twitter.android",
                    "com.twitter.android.PostActivity");
            shareIntent.setType("text/*");
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
            return shareIntent;
        } else {
            String tweetUrl = "https://twitter.com/intent/tweet?text=" + message;
            Uri uri = Uri.parse(tweetUrl);
            shareIntent = new Intent(Intent.ACTION_VIEW, uri);
            return shareIntent;
        }
    }

    private static boolean doesPackageExist(Context cxt, String packageName) {
        PackageManager pm = cxt.getPackageManager();
        return pm.getPackageArchiveInfo(packageName, 0) != null;
    }

    public static void facebook(Activity activity,String link){
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(link))
                .build();
        ShareDialog dialog = new ShareDialog(activity);
        dialog.canShow(content, ShareDialog.Mode.AUTOMATIC);
    }
}
