package com.genkimiru.app.base.adapter;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genkimiru.app.BuildConfig;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import timber.log.Timber;

public abstract class BaseSimpleAdapter<TItem, TViewHolder extends BaseSimpleViewHolder> extends Adapter {

    private static final int TYPE_ITEM = 100;
    private static final int TYPE_LOADING = 300;
    protected List<TItem> mItems;
    protected WeakReference<View> mLoadingMoreView;
    protected Context mContext;

    public BaseSimpleAdapter(Context context) {
        super();
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        checkIfMainThread();
        switch (viewType) {
            case TYPE_ITEM:
                View view = LayoutInflater.from(mContext).inflate(getItemLayoutResource(), parent, false);
                TViewHolder holder = createHolder(view);
                return holder;
            case TYPE_LOADING:
                // Footer view is loading more
                mLoadingMoreView.get().setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                return new ViewHolder(mLoadingMoreView.get()) {
                };
            default:
                throw new UnsupportedOperationException("The item type is invalid");
        }
    }

    public TViewHolder createHolder(View view) {
        throw new UnsupportedOperationException("You must implement this method at " + this.getClass().toString());
    }

    public int getItemLayoutResource() {
        throw new UnsupportedOperationException("You must implement this method at " + this.getClass().toString());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        checkIfMainThread();
        int viewType = getItemViewType(position);
        switch (viewType) {
            case TYPE_ITEM:
                TItem item = mItems.get(position);
                ((TViewHolder) holder).bind(item);
                break;
            case TYPE_LOADING:
                // No need to bind for footer view
                break;
            default:
                throw new UnsupportedOperationException("The item type is invalid");
        }
    }

    public BaseSimpleAdapter items(List<TItem> items) {
        checkIfMainThread();
        if (isEmpty(mItems)) {
            mItems = new ArrayList<>();
        }
        mItems.addAll(items);
        return this;
    }

    public void set(List<TItem> items) {
        mItems = items;
    }

    public void appendAll(List<TItem> items) {
        if (isEmpty(mItems)) mItems = new ArrayList<>();
        if (mItems.size() == 0 && !isEmpty(items)) {
            set(items);
            notifyDataSetChanged();
        } else {
            int oldIndex = mItems.size();
            int index = 0;
            if (!isEmpty(items)) {
                mItems.addAll(items);
                index = items.size();
            } else {
                index = mItems.size();
            }
            notifyItemRangeInserted(oldIndex, index);
        }
    }

    public void addItem(TItem item, int pos) {
        if (isEmpty(mItems)) {
            mItems = new ArrayList<>();
        }
        mItems.add(pos, item);
        notifyItemInserted(pos);
    }

    public void removeItem(int pos) {
        mItems.remove(pos);
        notifyItemRemoved(pos);
    }

    public void addFooterView(View loadingView) {
        checkIfMainThread();
        mLoadingMoreView = new WeakReference<>(loadingView);
        notifyDataSetChanged();
    }

    public void removeFooterView() {
        checkIfMainThread();
        mLoadingMoreView = null;
        notifyDataSetChanged();
    }

    private void checkIfMainThread() {
        if (BuildConfig.DEBUG && Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("Current thread is not home_menu thread");
        }
    }

    @Override
    public int getItemCount() {
        checkIfMainThread();
        int count = 0;
        if (isNotEmpty(mItems)) {
            count += mItems.size();
        }
        if (mLoadingMoreView != null) {
            count++;
        }
        return count;
    }

    private boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    private boolean isNotEmpty(Collection collection) {
        return collection != null && !collection.isEmpty();
    }

    public void releaseAll() {
        if (isNotEmpty(mItems)) {
            mItems.clear();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < mItems.size()) {
            return TYPE_ITEM;
        } else if (position < getItemCount()) {
            return TYPE_LOADING;
        } else {
            return super.getItemViewType(position);
        }
    }

    public List<TItem> getItems() {
        return mItems;
    }
}
