package com.genkimiru.app.base.storage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public abstract class BaseDAO<TDBItem extends RealmObject, TModel> {

    protected Class<TDBItem> mDbClass;
    private Realm mRealm;

    public BaseDAO(Class<TDBItem> dbClass) {
        this.mDbClass = dbClass;
        mRealm = Realm.getDefaultInstance();
    }

    /**
     * Return the name of Primary key. {@link io.realm.annotations.PrimaryKey}
     *
     * @return name of Primary key
     */
    protected abstract String getPrimaryKeyWord();

    protected TDBItem convertFromModelToDB(TModel model) {
        // Implement if need
        return null;
    }

    protected abstract TModel convertFromDBToDBModel(TDBItem dbItem);

    final public Flowable<Boolean> saveOrUpdateFromModels(List<TModel> models) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
                    for (TModel tModel : models) {
                        TDBItem tdbItem = convertFromModelToDB(tModel);
                        mRealm.copyToRealmOrUpdate(tdbItem);
                    }
                }, () -> replaySubject.onNext(true),
                replaySubject::onError);
        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }

    final public Flowable<Boolean> saveOrUpdateFromModel(TModel tModel) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
            TDBItem tdbItem = convertFromModelToDB(tModel);
            mRealm.copyToRealmOrUpdate(tdbItem);
        }, () -> replaySubject.onNext(true), replaySubject::onError);
        return replaySubject.toFlowable(BackpressureStrategy.LATEST);

    }

    final public Flowable<Boolean> copyOrUpdateList(List<TDBItem> tdbItems) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
            for (TDBItem tdbItem : tdbItems) {
                realm.copyToRealmOrUpdate(tdbItem);
            }
        }, () -> replaySubject.onNext(true), replaySubject::onError);
        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }

    public Flowable<List<TDBItem>> getAll() {
        return mRealm.asFlowable().subscribeOn(Schedulers.io())
                .map(mRealm -> mRealm.where(mDbClass).findAll());
    }

    public Flowable<TDBItem> getOnlyOneByCondition(String fieldName, int fieldValue) {
        return mRealm.asFlowable().subscribeOn(Schedulers.io())
                .map(mRealm -> mRealm.where(mDbClass).equalTo(fieldName, fieldValue).findFirst());
    }

    final public Flowable<Boolean> saveModelList(List<TModel> models) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
            for (TModel tModel : models) {
                TDBItem tdbItem = convertFromModelToDB(tModel);
                mRealm.copyToRealm(tdbItem);
            }
        }, () -> replaySubject.onNext(true), replaySubject::onError);

        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }

    final public Flowable<Boolean> saveOrUpdate(TDBItem item) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
            mRealm.copyToRealmOrUpdate(item);
        }, () -> replaySubject.onNext(true), replaySubject::onError);

        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }

    final public Flowable<Boolean> saveOrUpdateList(List<TDBItem> items) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
            for (TDBItem item : items) {
                mRealm.copyToRealmOrUpdate(item);
            }
        }, () -> replaySubject.onNext(true), replaySubject::onError);

        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }

    final public Flowable<Boolean> saveDto(TModel model) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        mRealm.executeTransactionAsync(realm -> {
            TDBItem tdbItem = convertFromModelToDB(model);
            realm.copyToRealm(tdbItem);
        }, () -> replaySubject.onNext(true), replaySubject::onError);

        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }

    public Flowable<TDBItem> getById(int id) {
        return mRealm.asFlowable().subscribeOn(Schedulers.io())
                .map(mRealm -> mRealm.where(mDbClass).equalTo(getPrimaryKeyWord(), id).findFirst());
    }

    final public Flowable<List<TModel>> getAllByConditionThenConvertToModel(String fieldName, int fieldValue) {
        return getAllByCondition(fieldName, fieldValue).map(tdbItems -> {
            List<TModel> tModels = new ArrayList<>();
            for (TDBItem dbTdbItem : tdbItems) {
                TModel model = convertFromDBToDBModel(dbTdbItem);
                tModels.add(model);
            }
            return tModels;
        });
    }

    final public Flowable<List<TModel>> getAllThenConvertToModel() {
        return getAll().map(tdbItems -> {
            List<TModel> tModels = new ArrayList<>();
            for (TDBItem dbTdbItem : tdbItems) {
                TModel model = convertFromDBToDBModel(dbTdbItem);
                tModels.add(model);
            }

            return tModels;
        });
    }

    public Flowable<RealmResults<TDBItem>> getAllByCondition(String fieldName, int fieldValue) {
        return mRealm.asFlowable().subscribeOn(Schedulers.io())
                .map(realm -> realm.where(mDbClass).equalTo(fieldName, fieldValue).findAll());
    }

    final public Flowable<Boolean> deleteAllByCondition(String fieldName, int fieldValue) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        getAllByCondition(fieldName, fieldValue).subscribe(tdbItems -> {
            mRealm.executeTransactionAsync(
                    realm -> tdbItems.deleteAllFromRealm(),
                    () -> replaySubject.onNext(true),
                    replaySubject::onError);
        });
        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }


    final public Flowable<Boolean> deleteById(int id) {
        ReplaySubject<Boolean> replaySubject = ReplaySubject.create();
        getById(id).subscribe(tdbItem -> {
            mRealm.executeTransactionAsync(
                    realm -> tdbItem.deleteFromRealm(),
                    () -> replaySubject.onNext(true),
                    replaySubject::onError);
        });
        return replaySubject.toFlowable(BackpressureStrategy.LATEST);
    }
}
