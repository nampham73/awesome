package com.genkimiru.app.base.util;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Locale;

public final class DefaultDateFormat extends SimpleDateFormat {

    private static final Locale DEFAULT_LOCATE = Locale.JAPAN;

    public DefaultDateFormat(@NonNull String pattern) {
        super(pattern, Locale.getDefault());
    }
}
