package com.genkimiru.app.base.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.genkimiru.app.base.define.Constants;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observable;
import timber.log.Timber;

public class StaticMethods {


    private StaticMethods() {
        throw new IllegalAccessError("StaticMethods class");
    }

    public static <T> List copy(List<T> list) {
        List<T> copyedList = new ArrayList<>();
        if (isNotEmpty(list)) {
            for (T input : list) {
                copyedList.add(input);
            }
        }
        return copyedList;
    }

    public static boolean isEmpty(int[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    public static boolean isNotEmpty(Object[] arrays) {
        return arrays != null && arrays.length > 0;
    }

    public static int size(Collection<?> collection) {
        return collection == null ? 0 : collection.size();
    }

    public static boolean isEmpty(CharSequence text) {
        return text == null || text.length() == 0;
    }

    public static boolean isNotEmpty(CharSequence text) {
        return text != null && text.length() > 0;
    }

    public static boolean isEmpty(String text) {
        return text == null || text.trim().length() == 0;
    }

    public static boolean isNotEmpty(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static boolean isLastIndex(Collection collection, int index) {
        if (isEmpty(collection)) {
            return false;
        }
        return index == collection.size() - 1;
    }

    public static boolean isLastIndex(int index, int size) {
        return index == size - 1;
    }

    public static <T> T getLast(List<T> list) {
        return isEmpty(list) ? null : list.get(list.size() - 1);
    }

    public static boolean isTrue(String textYN) {
        return "Y".equals(textYN);
    }

    public static int count(Collection<?> collection) {
        return collection != null ? collection.size() : 0;
    }

    public static boolean contains(int value, int bit) {
        return (value & bit) == bit;
    }

    public static <T> T get(List<T> list, int position) {
        try {
            return list.get(position);
        } catch (Exception e) {
            Timber.e(e, "StaticMethod().get() is failed");
            return null;
        }
    }

    public static Integer[] toObject(int[] intArr) {
        Integer[] integers = new Integer[intArr.length];

        for (int i = 0; i < intArr.length; i++) {
            integers[i] = intArr[i];
        }

        return integers;
    }

    public static <I, O> List<O> transform(List<I> list, Function<? super I, ? extends O> function) {
        List<O> transformedList = new ArrayList<>();
        if (isNotEmpty(list)) {
            for (I input : list) {
                transformedList.add(function.apply(input));
            }
        }
        return transformedList;
    }

    public static <I, O> Set<O> transform(Set<I> set, Function<? super I, ? extends O> function) {
        Set<O> transformedSet = new HashSet<>();
        if (isNotEmpty(set)) {
            for (I input : set) {
                transformedSet.add(function.apply(input));
            }
        }
        return transformedSet;
    }

    public static <T> List<T> filter(List<T> list, Predicate<T> predicate) {
        List<T> filteredList = new ArrayList<>();
        if (isNotEmpty(list)) {
            for (T input : list) {
                if (predicate.apply(input)) {
                    filteredList.add(input);
                }
            }
        }
        return filteredList;
    }

    public static <T> Set<T> filter(Set<T> set, Predicate<T> predicate) {
        Set<T> filteredSet = new HashSet<>();
        if (isNotEmpty(set)) {
            for (T input : set) {
                if (predicate.apply(input)) {
                    filteredSet.add(input);
                }
            }
        }
        return filteredSet;
    }

    public static int[] toArray(int value) {
        int[] intArray = new int[1];
        intArray[0] = value;
        return intArray;
    }

    public static int[] toArray(Collection<? extends Integer> collection) {
        int index = 0;
        int count = collection.size();
        int[] intArray = new int[count];
        for (Integer integer : collection) {
            intArray[index++] = integer.intValue();
        }
        return intArray;
    }

    public static <T> T checkNotNull(T object) {
        if (object == null) {
            throw new NullPointerException();
        }
        return object;
    }

    public static int checkElementIndex(int index, int size) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return index;
    }

    public static int checkPositionIndex(int index, int size) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        return index;
    }

    public static void checkPositionIndexes(int start, int end, int size) {
        if (start < 0 || end < start || end > size) {
            throw new IndexOutOfBoundsException();
        }
    }

    public static <T> List<T> reverse(List<T> list) {
        final List reverseList = new ArrayList<>();

        for (int i = list.size() - 1; i == 0; i--) {
            reverseList.add(list.get(i));
        }

        return reverseList;
    }

    public static String capitalize(final String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        final char firstChar = str.charAt(0);
        if (Character.isTitleCase(firstChar)) {
            // already capitalized
            return str;
        }
        return String.format("%s%s", Character.toTitleCase(firstChar), str.substring(1));
    }

    public static String getSafeString(JSONObject jsonObject, String name) {
        try {
            return jsonObject.getString(name);
        } catch (Exception e) {
            Timber.e(e, "getSafeString() is failed");
            return "";
        }
    }

    public static String addThousandSeparatorCommas(int count) {
        return String.format("%,d", count);
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideKeyboard(Context context, View view) {
        final InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager == null) {
            return;
        }
        if (view == null) {
            return;
        }
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void scanMediaFile(Context context, Uri uri) {
        Intent scanFileIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
        context.sendBroadcast(scanFileIntent);
    }

    public static String encodeForUrl(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Timber.e(e, "encodeForUrl is failed");
            return text;
        }
    }

    public static Observable<View> onMeasureSize(final View view) {
        return Observable.create(new ObservableOnSubscribe<View>() {
            @Override
            public void subscribe(ObservableEmitter<View> emitter) {
                view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        if (view.getWidth() == 0 || view.getHeight() == 0) {
                            return;
                        }
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        emitter.onNext(view);
                        emitter.onComplete();
                    }
                });
            }
        });
    }

    public static int getPixelFromDip(Resources resources, int dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics());
    }

    public static int getPixelFromDip(Resources resources, float dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics());
    }

    public static void setColor(SpannableString fullText, String colorText, int color) {
        if (StaticMethods.isEmpty(fullText) || StaticMethods.isEmpty(colorText)) {
            return;
        }
        final int start = fullText.toString().toUpperCase().indexOf(colorText.toUpperCase());
        final int end = start + colorText.length();
        if (start == -1) {
            return;
        }
        fullText.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public static void setBold(SpannableString fullText, String boldText) {
        final int start = fullText.toString().indexOf(boldText);
        final int end = start + boldText.length();
        if (start == -1) {
            return;
        }
        fullText.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public static void setUnderLineText(final TextView view) {
        setUnderLineText(view, view.getText().toString());
    }

    public static void setUnderLineText(final TextView view, final String clickableText) {
        if (view == null || StaticMethods.isEmpty(clickableText)) {
            return;
        }
        final CharSequence text = view.getText();
        if (StaticMethods.isEmpty(text)) {
            return;
        }
        final String string = text.toString();
        final UnderlineSpan span = new UnderlineSpan();
        final int start = string.indexOf(clickableText);
        final int end = start + clickableText.length();
        if (start == -1) {
            return;
        }
        if (text instanceof Spannable) {
            ((Spannable) text).setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            final SpannableString s = SpannableString.valueOf(text);
            s.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            view.setText(s);
        }
        MovementMethod m = view.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            view.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    public static String getHashKey(String string) {
        String hashKey;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte byteData[] = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            hashKey = sb.toString();
        } catch (Exception e) {
            Timber.e(e, "getHashKey() is failed");
            hashKey = urlEncode(string);
        }
        return hashKey;
    }

    @SafeVarargs
    public static <T> List<T> asList(T... array) {
        return Arrays.asList(array);
    }

    public static String urlEncode(String string) {
        try {
            return URLEncoder.encode(string, "utf-8");
        } catch (Exception e) {
            Timber.e(e, "urlEncode() is failed");
            return string;
        }
    }

    public static Intent installIntent(String packageName) {
        return new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        return StringUtil.isNotEmpty(password)
                && Constants.Validate.PASSWORD_VALIDATE.matcher(password).matches();
    }

    public static boolean isValidName(String name) {
        return StringUtil.isNotEmpty(name)
                && Constants.Validate.NAME_VALIDATE.matcher(name).matches();
    }

    public static boolean isValidPostalCode(String postalCode) {
        return StringUtil.isNotEmpty(postalCode)
                && Constants.Validate.POSTAL_CODE_PATTERN.matcher(postalCode).matches();
    }

    public static boolean toBoolean(int integer) {
        if (integer == 1) {
            return true;
        } else if (integer == 0) {
            return false;
        } else {
            return false;
        }
    }

    public static boolean nonNull(Object o) {
        return o != null;
    }

    public static boolean isNull(Object o) {
        return o == null;
    }

    public static String getMimeImageType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        if (StaticMethods.isEmpty(type)) {
            return "image/png";
        }
        return type;
    }

    public static boolean isValidResourceId(int id) {
        return id > 0;
    }

    public static Bitmap getBitmapFromResource(Context context, int id) {
        return BitmapFactory.decodeResource(context.getResources(), id);
    }

    interface Function<I, O> {
        O apply(I input);
    }

    interface Predicate<T> {
        boolean apply(T input);
    }

    public static void waitForMeasure(final View view, final OnMeasuredCallback callback) {
        int width = view.getWidth();
        int height = view.getHeight();

        if (width > 0 && height > 0) {
            callback.onMeasured(view, width, height);
            return;
        }

        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                final ViewTreeObserver observer = view.getViewTreeObserver();
                if (observer.isAlive()) {
                    observer.removeOnPreDrawListener(this);
                }

                callback.onMeasured(view, view.getWidth(), view.getHeight());
                return true;
            }
        });
    }

    public interface OnMeasuredCallback {
        void onMeasured(View view, int width, int height);
    }

    public static void showClearButtonByEditText(ImageView clearButton, EditText editText) {
        boolean isHidden = (isEmpty(editText.getText().toString()))
                || !editText.hasFocus()
                || (editText.getVisibility() != View.VISIBLE);
        clearButton.setVisibility(isHidden ? View.GONE : View.VISIBLE);
    }

    public static <T extends View> T findViewById(View view, int id) {
        return (T) view.findViewById(id);
    }


    public static <T> T[] convertCollectionToArrays(Class<T> clazz, Collection<T> collection) {
        if (isEmpty(collection)) {
            return (T[]) Array.newInstance(clazz, 0);
        }

        T[] array = (T[]) Array.newInstance(clazz, collection.size());
        array = collection.toArray(array);
        return array;
    }

    public static <T> Collection<T> convertArraysToCollection(T[] array){
        return Arrays.asList(array);
    }

    public static <T> List convertArrayToArrayList(T[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }

    public static boolean isValidBeNotZero(long value){
        return value != 0;
    }

    public static void setEnableView(View view, boolean enable) {
        view.setEnabled(enable);
        float alpha = enable ? Constants.Alpha.ALPHA_ENABLE : Constants.Alpha.ALPHA_DISABLE;
        view.setAlpha(alpha);
    }

    public static void setEllipsizeAndClearButton(Context context,
                                                  EditText editText,
                                                  TextView ellipsizeTextView,
                                                  ImageView clearImageView,
                                                  EditTextCallback callback) {
        editText.setVisibility(View.GONE);
        ellipsizeTextView.setVisibility(View.VISIBLE);
        clearImageView.setVisibility(View.GONE);
        ellipsizeTextView.setText(editText.getText());
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showClearButtonByEditText(clearImageView, editText);
            }

            @Override
            public void afterTextChanged(Editable s) {
                ellipsizeTextView.setText(editText.getText().toString().trim());
            }
        });
        ellipsizeTextView.setOnClickListener(v -> {
            hideTextView(context, editText, ellipsizeTextView);
        });
        editText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                showTextView(editText, ellipsizeTextView);
            }
            showClearButtonByEditText(clearImageView, editText);

            if (nonNull(callback)) {
                callback.onFocusChange(hasFocus);
            }
        });
    }

    public static void setEllipsizeAndClearButton(Context context,
                                                  EditText editText,
                                                  TextView ellipsizeTextView,
                                                  ImageView clearImageView) {
        setEllipsizeAndClearButton(context, editText, ellipsizeTextView, clearImageView, null);
    }

    private static void hideTextView(Context context, EditText editText, TextView textView) {
        editText.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        editText.requestFocus();
        editText.setSelection(editText.getText().length());
        showKeyboard(context);
    }

    private static void showTextView(EditText editText, TextView textView) {
        editText.setVisibility(View.GONE);
        textView.setVisibility(View.VISIBLE);
    }

    public static boolean lessMoreThanDistance(long from, long to, long distance) {
        return to > from && (to - from) < distance;
    }

    public interface EditTextCallback {
        void onFocusChange(boolean isFocused);
    }

//    public static void vibrate(Context context, int duration){
//        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
//        vibrator.vibrate(duration);
//    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }
}
