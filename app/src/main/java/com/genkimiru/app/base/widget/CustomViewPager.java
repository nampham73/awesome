package com.genkimiru.app.base.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import timber.log.Timber;

public class CustomViewPager extends ViewPager {

    private float initialXValue;
    private SwipeDirection direction;

    public enum SwipeDirection {
        left,
        right,
        all,
        none
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.direction = SwipeDirection.all;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isSwipeAllowed(event)) {
            return false;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!isSwipeAllowed(event)) {
            return false;
        }

        return super.onInterceptTouchEvent(event);
    }

    private boolean isSwipeAllowed(MotionEvent event) {
        if (direction == SwipeDirection.all) {
            Timber.d("allow swipe left/right");
            return true;
        }

        if (direction == SwipeDirection.none) {//lock any swipe
            Timber.d("lock swipe left/right");
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            initialXValue = event.getX();
            return true;
        }

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float diffX = event.getX() - initialXValue;
            if (diffX > 0
                    && direction == SwipeDirection.right) {
                Timber.d("lock swipe from left to right");
                // lock swipe from left to right
                return false;
            } else if (diffX < 0
                    && direction == SwipeDirection.left) {
                Timber.d("lock swipe from right to left");
                // lock swipe from right to left
                return false;
            }
        }
        return true;
    }

    public void setAllowedSwipeDirection(SwipeDirection direction) {
        this.direction = direction;
    }

}
