package com.genkimiru.app.base.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;


import com.genkimiru.app.base.adapter.section.LoadingSection;
import com.genkimiru.app.base.adapter.section.Section;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseSectionAdapter extends Adapter<ViewHolder> {

    protected Context mContext;
    protected List<Section> mSections;

    public BaseSectionAdapter(Context context) {
        mContext = context;
        mSections = new ArrayList<>();
    }

    public Section getSectionByType(int type) {
        for (Section section : mSections) {
            if (section.viewType() == type) {
                return section;
            }
        }

        return null;
    }

    public void removeSection(Section section) {

        // remove add if exist: TODO
        boolean exist = false;
        for (Section s : mSections) {
            if (s == section) {
                exist = true;
                break;
            }
        }


        if (!exist) {
            return;
        }

        mSections.remove(section);
    }

    public int getSectionPosByType(int type) {
        int pos = 0;
        for (Section section : mSections) {
            if (section.viewType() == type) {
                return pos;
            }
            pos++;
        }

        return pos;
    }

    public void addSection(Section section) {

        // Not add if exist: TODO
        boolean exist = false;
        for (Section s : mSections) {
            if (s == section) {
                exist = true;
                break;
            }
        }


        if (exist) {
            return;
        }
        mSections.add(section);
        notifyDataSetChanged();
    }

    public void addSection(Section section, int pos) {
        // Not add if exist: TODO
        boolean exist = false;
        for (Section s : mSections) {
            if (s == section) {
                exist = true;
                break;
            }
        }


        if (exist) {
            return;
        }
        mSections.add(pos, section);
    }

    public void notifyUpdatePos(int pos) {
        throw new UnsupportedOperationException("notifyUpdatePos>> Please implement if using!");
    }

    public boolean isLoadingSection(Section section) {
        return section instanceof LoadingSection;
    }

    @Override
    public int getItemViewType(int position) {
        int sectionPosition = position;
        for (Section section : mSections) {
            if (sectionPosition < section.itemCount()) {
                return section.viewType();
            }
            sectionPosition -= section.itemCount();
        }
        return 0;
    }

    public void showLoadingSection(boolean isShown) {
        Section loadingSection = null;
        for (Section section : mSections) {
            if (isLoadingSection(section)) {
                loadingSection = section;
                break;
            }
        }

        if (loadingSection != null) {
            if (isShown) {
                // Do nothing
            } else {
                mSections.remove(loadingSection);
            }
        } else {
            if (isShown) {
                // Do nothing
                mSections.add(createLoadingSection());
            } else {
                // Do nothing
                mSections.remove(loadingSection);
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List items) {

    }

    public void removeAtPos(int i) {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        for (Section section : mSections) {
            if (section.viewType() == viewType) {
                return section.onCreateViewHolder(parent);
            }
        }
        return null;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int sectionPosition = position;
        for (Section section : mSections) {
            if (sectionPosition < section.itemCount()) {
                section.onBindViewHolder(holder, sectionPosition);
                return;
            }
            sectionPosition -= section.itemCount();
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        for (Section section : mSections) {
            count += section.itemCount();
        }
        return count;
    }

    public void clear() {
        mSections.clear();
    }

    protected Section createLoadingSection() {
        return new LoadingSection(mContext, Integer.MAX_VALUE);
    }

}
