package com.genkimiru.app.base.net;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.genkimiru.app.R;
import com.genkimiru.app.base.util.MessageUtil;

import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.genkimiru.app.Constants.Network.HTTP_STATUS_UNAUTHORIZED;

public class UnauthorizedInterceptor implements Interceptor {

    private Context mContext;
    private Class<?> mTargetActivity;

    public UnauthorizedInterceptor(Context context, Class<?> targetActivity) {
        mContext = context;
        mTargetActivity = targetActivity;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        okhttp3.Response response = chain.proceed(request);
        if (response.code() == HTTP_STATUS_UNAUTHORIZED) {
            Single.just(true).observeOn(AndroidSchedulers.mainThread()).subscribe(aBoolean -> {
                MessageUtil.showShortToast(mContext, "Session timeout!");
                Intent intent = new Intent(mContext, mTargetActivity);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intent);
            });
        }
        return response;
    }
}
