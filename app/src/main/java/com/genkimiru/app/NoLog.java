package com.genkimiru.app;

import timber.log.Timber;

public class NoLog extends Timber.Tree {
    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        // Do nothing
    }
}
