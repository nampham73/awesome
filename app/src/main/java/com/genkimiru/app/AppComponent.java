package com.genkimiru.app;

import com.genkimiru.app.base.net.NetworkModule;
import com.genkimiru.app.data.remote.extension.ExtensionApiModule;
import com.genkimiru.app.data.remote.fitbit.FitbitApiModule;
import com.genkimiru.app.data.remote.genki.GenkiApiModule;
import com.genkimiru.app.data.remote.gobe.GobeApiModule;
import com.genkimiru.app.data.remote.google.GoogleApiModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        GenkiApiModule.class,
        GobeApiModule.class,
        FitbitApiModule.class,
        GoogleApiModule.class,
        ExtensionApiModule.class,
        ActivityBuilder.class,
        ServiceBuilder.class})
public interface AppComponent extends AndroidInjector<GenkiApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<GenkiApplication> {
    }
}
