package com.genkimiru.app;

import com.genkimiru.app.base.scope.ActivityScope;
import com.genkimiru.app.presentation.bug_report.BugReportActivity;
import com.genkimiru.app.presentation.bug_report.BugReportActivityModule;
import com.genkimiru.app.presentation.challenge_detail_history.ChallengeDetailHistoryActivity;
import com.genkimiru.app.presentation.challenge_detail_history.ChallengeDetailHistoryActivityModule;
import com.genkimiru.app.presentation.challenge_detail_history.history_fragment.HistoryChallengeFragmentBuilder;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentBuilder;
import com.genkimiru.app.presentation.challenge_detail_history.information_fragment.InformationChallengeFragmentModule;
import com.genkimiru.app.presentation.change_password.ChangePasswordActivity;
import com.genkimiru.app.presentation.change_password.ChangePasswordActivityModule;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivity;
import com.genkimiru.app.presentation.chat_with_watson.ChatWithWatsonActivityModule;
import com.genkimiru.app.presentation.content_detail.ContentDetailActivity;
import com.genkimiru.app.presentation.content_detail.ContentDetailActivityModule;
import com.genkimiru.app.presentation.content_detail.detail_content_feed_fragment.DetailContentFeedFragmentBuilder;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivity;
import com.genkimiru.app.presentation.contest_detail.ContestDetailActivityModule;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivity;
import com.genkimiru.app.presentation.edit_profile.EditProfileActivityModule;
import com.genkimiru.app.presentation.energy.EnergyActivity;
import com.genkimiru.app.presentation.energy.EnergyActivityModule;
import com.genkimiru.app.presentation.faq.FaqActivity;
import com.genkimiru.app.presentation.faq.FaqActivityModule;
import com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivity;
import com.genkimiru.app.presentation.forgot_password.ForgotPasswordActivityModule;
import com.genkimiru.app.presentation.forgot_password.new_password_fragment.NewPasswordFragmentBuilder;
import com.genkimiru.app.presentation.forgot_password.password_recover_fragment.PasswordRecoverFragmentBuilder;
import com.genkimiru.app.presentation.login.LoginActivity;
import com.genkimiru.app.presentation.login.LoginActivityModule;
import com.genkimiru.app.presentation.main.MainActivity;
import com.genkimiru.app.presentation.main.MainActivityModule;
import com.genkimiru.app.presentation.main.contentfeed_fragment.ContentFeedFragmentBuilder;
import com.genkimiru.app.presentation.main.contest_fragment.PagerContestFragmentBuilder;
import com.genkimiru.app.presentation.main.dashboard_fragment.DashboardFragmentBuilder;
import com.genkimiru.app.presentation.main.setting_fragment.SettingFragmentBuilder;
import com.genkimiru.app.presentation.nac.NacActivity;
import com.genkimiru.app.presentation.nac.NacActivityModule;
import com.genkimiru.app.presentation.nutrition.NutritionActivity;
import com.genkimiru.app.presentation.nutrition.NutritionActivityModule;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivity;
import com.genkimiru.app.presentation.other_wear_device.OtherWearDeviceActivityModule;
import com.genkimiru.app.presentation.register.RegisterActivity;
import com.genkimiru.app.presentation.register.RegisterActivityModule;
import com.genkimiru.app.presentation.register.account_complete_fragment.AccountCompleteFragmentBuilder;
import com.genkimiru.app.presentation.register.account_fragment.AccountFragmentBuilder;
import com.genkimiru.app.presentation.register.alcohol_fragment.AlcoholFragmentBuilder;
import com.genkimiru.app.presentation.register.birthday_fragment.BirthdayFragmentBuilder;
import com.genkimiru.app.presentation.register.blood_pressure_fragment.BloodPressureFragmentBuilder;
import com.genkimiru.app.presentation.register.blood_type_fragment.BloodTypeFragmentBuilder;
import com.genkimiru.app.presentation.register.heart_beat_fragment.HeartBeatFragmentBuilder;
import com.genkimiru.app.presentation.register.height_frament.HeightFragmentBuilder;
import com.genkimiru.app.presentation.register.hospital_fragment.HospitalFragmentBuilder;
import com.genkimiru.app.presentation.register.nickname_fragment.NicknameFragmentBuilder;
import com.genkimiru.app.presentation.register.profile_avatar_fragment.ProfileAvatarFragmentBuilder;
import com.genkimiru.app.presentation.register.sex_fragment.SexFragmentBuilder;
import com.genkimiru.app.presentation.register.smorking_fragment.SmokingFragmentBuilder;
import com.genkimiru.app.presentation.register.wear_device_fragment.WearDeviceFragmentBuilder;
import com.genkimiru.app.presentation.register.weight_fragment.WeightFragmentBuilder;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivity;
import com.genkimiru.app.presentation.setting_email_marketing.SettingEmailMarketingActivityModule;
import com.genkimiru.app.presentation.setting_notification.NotificationSettingActivity;
import com.genkimiru.app.presentation.setting_notification.NotificationSettingActivityModule;
import com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivity;
import com.genkimiru.app.presentation.setting_privacy.SettingPrivacyActivityModule;
import com.genkimiru.app.presentation.splash.SplashActivity;
import com.genkimiru.app.presentation.splash.SplashActivityModule;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivity;
import com.genkimiru.app.presentation.walking_challenge.WalkingChallengeActivityModule;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivity;
import com.genkimiru.app.presentation.walking_challenge_map.WalkingChallengeMapActivityModule;
import com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivity;
import com.genkimiru.app.presentation.update_user_infor.alcohol.UpdateAlcoholActivityModule;
import com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthDayActivity;
import com.genkimiru.app.presentation.update_user_infor.birthday.UpdateBirthdayActivityModule;
import com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivity;
import com.genkimiru.app.presentation.update_user_infor.blood_pressure.UpdateBloodPressureActivityModule;
import com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivity;
import com.genkimiru.app.presentation.update_user_infor.blood_type.UpdateBloodTypeActivityModule;
import com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivity;
import com.genkimiru.app.presentation.update_user_infor.gender.UpdateGenderActivityModule;
import com.genkimiru.app.presentation.update_user_infor.heart_beat.UpdateHeartBeatActivity;
import com.genkimiru.app.presentation.update_user_infor.heart_beat.UpdateHeartBeatActivityModule;
import com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivity;
import com.genkimiru.app.presentation.update_user_infor.height.UpdateHeightActivityModule;
import com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivity;
import com.genkimiru.app.presentation.update_user_infor.hospital.UpdateHospitalActivityModule;
import com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivity;
import com.genkimiru.app.presentation.update_user_infor.smoking.UpdateSmokingActivityModule;
import com.genkimiru.app.presentation.update_user_infor.weight.UpdateWeightActivity;
import com.genkimiru.app.presentation.update_user_infor.weight.UpdateWeightActivityModule;
import com.genkimiru.app.presentation.step.StepActivity;
import com.genkimiru.app.presentation.step.StepActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashActivity splashActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity loginActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {
            ForgotPasswordActivityModule.class,
            PasswordRecoverFragmentBuilder.class,
            NewPasswordFragmentBuilder.class})
    abstract ForgotPasswordActivity forgotPasswordActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = EditProfileActivityModule.class)
    abstract EditProfileActivity editprofileActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class,
            PagerContestFragmentBuilder.class,
            DashboardFragmentBuilder.class,
            ContentFeedFragmentBuilder.class,
            SettingFragmentBuilder.class})
    abstract MainActivity mainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {RegisterActivityModule.class,
            AccountCompleteFragmentBuilder.class, AccountFragmentBuilder.class, AlcoholFragmentBuilder.class, BirthdayFragmentBuilder.class,
            BloodPressureFragmentBuilder.class, NicknameFragmentBuilder.class, BloodTypeFragmentBuilder.class,
            HeartBeatFragmentBuilder.class, HeightFragmentBuilder.class, HospitalFragmentBuilder.class, ProfileAvatarFragmentBuilder.class,
            SexFragmentBuilder.class, SmokingFragmentBuilder.class, WearDeviceFragmentBuilder.class, WeightFragmentBuilder.class})
    abstract RegisterActivity registerActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = SettingPrivacyActivityModule.class)
    abstract SettingPrivacyActivity settingPrivacyActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = SettingEmailMarketingActivityModule.class)
    abstract SettingEmailMarketingActivity settingEmailMarketingActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = ChangePasswordActivityModule.class)
    abstract ChangePasswordActivity changePasswordActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = OtherWearDeviceActivityModule.class)
    abstract OtherWearDeviceActivity otherWearDeviceActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = NotificationSettingActivityModule.class)
    abstract NotificationSettingActivity notificationSettingActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = ContestDetailActivityModule.class)
    abstract ContestDetailActivity contestDetailActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = BugReportActivityModule.class)
    abstract BugReportActivity bugReportActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = ChatWithWatsonActivityModule.class)
    abstract ChatWithWatsonActivity chatWithWatsonActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ChallengeDetailHistoryActivityModule.class,
            HistoryChallengeFragmentBuilder.class,
            InformationChallengeFragmentBuilder.class})
    abstract ChallengeDetailHistoryActivity challengeDetailHistory();

    @ActivityScope
    @ContributesAndroidInjector(modules = FaqActivityModule.class)
    abstract FaqActivity faqActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {ContentDetailActivityModule.class, DetailContentFeedFragmentBuilder.class})
    abstract ContentDetailActivity contentDetailActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = StepActivityModule.class)
    abstract StepActivity stepActivityModule();

    @ActivityScope
    @ContributesAndroidInjector(modules = NacActivityModule.class)
    abstract NacActivity nacActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateBirthdayActivityModule.class)
    abstract UpdateBirthDayActivity birthDayActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateBloodPressureActivityModule.class)
    abstract UpdateBloodPressureActivity bloodPressureActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateBloodTypeActivityModule.class)
    abstract UpdateBloodTypeActivity updateBloodTypeActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateGenderActivityModule.class)
    abstract UpdateGenderActivity updateGenderActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateSmokingActivityModule.class)
    abstract UpdateSmokingActivity updateSmokingActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateAlcoholActivityModule.class)
    abstract UpdateAlcoholActivity updateAlcoholActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateWeightActivityModule.class)
    abstract UpdateWeightActivity updateWeightActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateHeightActivityModule.class)
    abstract UpdateHeightActivity updateHeightActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateHospitalActivityModule.class)
    abstract UpdateHospitalActivity updateHospitalActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = UpdateHeartBeatActivityModule.class)
    abstract UpdateHeartBeatActivity updateHeartBeatActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = NutritionActivityModule.class)
    abstract NutritionActivity nutritionActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = EnergyActivityModule.class)
    abstract EnergyActivity energyActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = WalkingChallengeActivityModule.class)
    abstract WalkingChallengeActivity walkingChallengeActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = WalkingChallengeMapActivityModule.class)
    abstract WalkingChallengeMapActivity walkingChallengeMapActivity();
}
