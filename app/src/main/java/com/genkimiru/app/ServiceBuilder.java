package com.genkimiru.app;

import com.genkimiru.app.base.scope.ServiceScope;
import com.genkimiru.app.service.stepcounter.StepCounterService;
import com.genkimiru.app.service.stepcounter.StepCounterServiceModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ServiceBuilder {

    @ServiceScope
    @ContributesAndroidInjector(modules = StepCounterServiceModule.class)
    abstract StepCounterService stepCounterService();
}
