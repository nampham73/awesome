package com.genkimiru.app.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.genkimiru.app.GenkiApplication;
import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.data.remote.genki.GenkiApi;
import com.genkimiru.app.data.repository.content.ContentRepositoryImp;
import com.genkimiru.app.domain.content.ContentUseCase;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

import static com.genkimiru.app.Constants.Preference.USER_TOKEN;

public class LikeView extends AppCompatTextView {
    private Disposable disposable;
    private ContentUseCase mContentUseCase;

    @Inject
    GenkiApi mGenkiApi;

    public LikeView(Context context) {
        super(context);
        SchedulerProvider provider = new SchedulerProvider();
        mContentUseCase = new ContentUseCase(provider.ui(), provider.io(), new ContentRepositoryImp(mGenkiApi));
    }

    public LikeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LikeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void startAsyncLikeRequest(String articleId) {
        String token = GenkiApplication.getPreferences().getString(USER_TOKEN).get();
        execute(articleId, token);
    }


    private void execute(String articleId, String token) {

    }

    public void stopAsyncRequest() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
