package com.genkimiru.app.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.genkimiru.app.R;
import com.genkimiru.app.base.util.DateUtil;
import com.genkimiru.app.base.util.StringUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import timber.log.Timber;

public class SleepChartView extends View {

    private static final String NONE_TIME = "--:--";

    private List<Entry> entries;
    private RectF rectF;
    private Rect bounds = new Rect();
    private float[] mScale;
    private Paint slidePaint, circlePaint, textPaint;
    private int mStrokeWidth;
    int iconResource;
    private Bitmap mIconBitmap;
    private String startTime="22:35", endTime="06:15";

    public static final int DEEP = 1;
    public static final int REM = 2;
    public static final int LIGHT = 3;
    public static final int WAKE = 4;


    public SleepChartView(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public SleepChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public SleepChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SleepChartView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defAttr, int defStyle) {
        if (isInEditMode()) return;
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.SleepChartView, defAttr, defStyle);
        try {
            iconResource = array.getResourceId(R.styleable.SleepChartView_icon_chart, R.drawable.ic_sleep);
            mIconBitmap = BitmapFactory.decodeResource(getResources(), iconResource);
        } finally {
            array.recycle();
        }

        entries = new ArrayList<>();
        Resources resources = getResources();
        mStrokeWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, resources.getDisplayMetrics());
        float textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, resources.getDisplayMetrics());

        slidePaint = new Paint();
        slidePaint.setAntiAlias(true);
        slidePaint.setDither(true);
        slidePaint.setStyle(Paint.Style.STROKE);
        slidePaint.setStrokeWidth(mStrokeWidth);

        circlePaint = new Paint();
        circlePaint.setAntiAlias(true);
        circlePaint.setDither(true);
        circlePaint.setStyle(Paint.Style.FILL);

        textPaint = new Paint();
        textPaint.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(textSize);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
        mScale = scale();
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size = Math.min(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(size, size);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        float radius = mStrokeWidth / 2;
        float textWidth = textPaint.measureText("00:00");
        float deltaText = textWidth / 2;
        //Create the box
        rectF = new RectF(radius + deltaText, radius + deltaText, h - radius - deltaText, h - radius - deltaText);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawPie(canvas);
        drawIcon(canvas);
    }

    private void drawIcon(Canvas canvas) {
        float dx = getWidth() / 2;
        float dy = getHeight() / 2;
        float marginTop = 30;
        if (mIconBitmap != null) {
            int width = mIconBitmap.getWidth() / 2;
            int height = mIconBitmap.getHeight() / 2;
            float disWidth = dx - width;
            float disHeight = dy - height - marginTop;
            canvas.drawBitmap(mIconBitmap, disWidth, disHeight, null);
        }
    }

    private void drawPie(Canvas canvas) {
        float sliceStartPoint = -180;
        int radius = mStrokeWidth / 2;
        if(entries.size() > 0) {
            slidePaint.setColor(ContextCompat.getColor(getContext(),R.color.sleep_deep));
            canvas.drawArc(rectF, -180, 180, false, slidePaint);
        } else {
            drawStartText(canvas, radius);
            drawEndText(canvas, radius);
        }
        for (int i = 0; i < entries.size(); i++) {
            Entry entry = entries.get(i);
            if (i == 0) {
                circlePaint.setColor(ContextCompat.getColor(getContext(),entry.mColor));
                String start = startTime;
                textPaint.getTextBounds(start, 0, start.length(), bounds);
                float textWidth = textPaint.measureText(start);
                float textHeight = bounds.height();
                float deltaText = textWidth / 4;
                float rawLeft = (float) (Math.cos(Math.toRadians(360)) * radius);
                float rawTop = (float) (Math.sin(Math.toRadians(360)) * radius + getHeight() / 2);
                float left = 0.3f + (rawLeft + textWidth / 2) - radius;
                float top = rawTop - radius - 5;
                float right = left + mStrokeWidth;
                float bottom = top + mStrokeWidth;
                canvas.drawArc(left, top, right, bottom, 0, 180, false, circlePaint);
                drawText(canvas, start, left - deltaText, rawTop + textHeight * 2);
            }

            if (i == entries.size() - 1) {
                circlePaint.setColor(ContextCompat.getColor(getContext(), entry.mColor));
                String end = endTime;

                textPaint.getTextBounds(end, 0, end.length(), bounds);
                float textWidth = textPaint.measureText(end);
                float textHeight = bounds.height();
                float deltaText = textWidth / 4;
                float rawLeft = (float) (Math.cos(Math.toRadians(360)) * (getWidth() - mStrokeWidth));
                float left = 0.5f + rawLeft - textWidth / 2;
                float rawTop = (float) (Math.sin(Math.toRadians(360)) * radius + getHeight() / 2);
                float top = rawTop - radius - 5;
                float right = left + mStrokeWidth;
                float bottom = top + mStrokeWidth;
                canvas.drawArc(left, top, right, bottom, 0, 180, false, circlePaint);
                drawText(canvas, end, right - textWidth + deltaText, rawTop + textHeight * 2);
            }
            slidePaint.setColor(ContextCompat.getColor(getContext(), entry.mColor));
            canvas.drawArc(rectF, sliceStartPoint, mScale[i], false, slidePaint);
            sliceStartPoint += mScale[i]; //Update starting point of the next slice
        }
    }

    private void drawText(Canvas canvas, String text, float dx, float dy) {
        canvas.drawText(text, dx, dy, textPaint);
    }

    private void drawStartText(Canvas canvas,int radius){
        String start = startTime;
        textPaint.getTextBounds(start, 0, start.length(), bounds);
        float textWidth = textPaint.measureText(start);
        float textHeight = bounds.height();
        float deltaText = textWidth / 4;
        float rawLeft = (float) (Math.cos(Math.toRadians(360)) * radius);
        float rawTop = (float) (Math.sin(Math.toRadians(360)) * radius + getHeight() / 2);
        float left = 0.3f + (rawLeft + textWidth / 2) - radius;
        drawText(canvas, start, left - deltaText, rawTop + textHeight * 2);
    }

    private void drawEndText(Canvas canvas,int radius) {
        String end = endTime;
        textPaint.getTextBounds(end, 0, end.length(), bounds);
        float textWidth = textPaint.measureText(end);
        float textHeight = bounds.height();
        float deltaText = textWidth / 4;
        float rawLeft = (float) (Math.cos(Math.toRadians(360)) * (getWidth() - mStrokeWidth));
        float left = 0.5f + rawLeft - textWidth / 2;
        float rawTop = (float) (Math.sin(Math.toRadians(360)) * radius + getHeight() / 2);
        float right = left + mStrokeWidth;
        drawText(canvas, end, right - textWidth + deltaText, rawTop + textHeight * 2);
    }

    private float[] scale() {
        if (entries == null || entries.size() == 0) return new float[0];
        float[] scaledValues = new float[this.entries.size()];
        float total = getTotal(); //Total all values supplied to the chart
        for (int i = 0; i < this.entries.size(); i++) {
            scaledValues[i] = ((this.entries.get(i).mValue / total) * 180); //Scale each value
        }
        return scaledValues;
    }

    public void setStartTime(long startTime) {
        if(startTime == 0) {
            this.startTime = NONE_TIME;
            return;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startTime * 1000L);
        this.startTime = DateUtil.getHourMinute(calendar);
    }

    public void setEndTime(long endTime) {
        if(endTime == 0) {
            this.endTime = NONE_TIME;
            return;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(endTime * 1000L);
        this.endTime = DateUtil.getHourMinute(calendar);
    }

    private float getTotal() {
        float total = 0;
        if (entries != null && entries.size() > 0) {
            for (Entry entry : entries) {
                total += entry.mValue;
            }
        }
        return total;
    }

    public static class Entry {
        private int mId;
        private float mValue;
        private int mColor;

        Entry(int id, float value, int color) {
            this.mId = id;
            this.mValue = value;
            this.mColor = color;
        }
    }

    public static class Builder {
        private int mId;
        private float mValue;
        private int mColor;

        public Builder(float id){
            this((int)id);
        }

        public Builder(int id) {
            this.mId = id;
            mValue = 0;
            mColor = autoDetectColor(id);
        }

        public Builder setValue(float value) {
            mValue = value;
            return this;
        }

        public Builder setColor(int mColor) {
            this.mColor = mColor;
            return this;
        }

        public Entry build() {
            return new Entry(mId, mValue, mColor);
        }

        @ColorRes
        private int autoDetectColor(int id) {
            switch (id) {
                case REM:
                    return R.color.sleep_rem;
                case LIGHT:
                    return R.color.sleep_light;
                case WAKE:
                    return R.color.sleep_falling;
            }
            return R.color.sleep_deep;
        }
    }


}
