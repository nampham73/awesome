package com.genkimiru.app.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.genkimiru.app.R;

/**
 * References idea from this topic on stackoverflow.com
 * https://stackoverflow.com/questions/10784208/draw-rounded-edge-arc-in-android-with-embossed-effect
 * Reference bitmap shader for draw arc(use bitmap when draw arc instead use colors range)
 * https://stackoverflow.com/questions/18527467/mask-imageview-with-round-corner-background
 */
public class HaftCircleView extends View {
    private Paint paint;
    private RectF rectF;
    //private LinearGradient shader;
    private int startColor, endColor, centerColor, endCenterCenterColor, endCenterColor;
    private int mStrokeWidth = 20;
    private Shader mShader;

    public HaftCircleView(Context context, int startColor, int endColor) {
        super(context);
        this.startColor = startColor;
        this.endColor = endColor;
        init(context);
    }

    public HaftCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HaftCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        if (isInEditMode()) return;
        Resources resources = getResources();
        mStrokeWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, resources.getDisplayMetrics());
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_mask);
        mShader = new BitmapShader(bm,Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(mStrokeWidth);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size = Math.min(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(size, size);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(rectF, -180, 180, false, paint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int radius = mStrokeWidth / 2;
        rectF = new RectF(radius, radius, w - radius, h - radius);
        paint.setShader(mShader);
    }

    public void setStartColor(int startColor) {
        this.startColor = startColor;
    }

    public void setEndColor(int endColor) {
        this.endColor = endColor;
    }

    public void setEndCenterCenterColor(int endCenterCenterColor) {
        this.endCenterCenterColor = endCenterCenterColor;
    }

    public void setCenterColor(int centerColor) {
        this.centerColor = centerColor;
    }



    public void setEndCenterColor(int endCenterColor) {
        this.endCenterColor = endCenterColor;
    }
}
