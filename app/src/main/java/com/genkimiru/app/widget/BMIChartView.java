package com.genkimiru.app.widget;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.genkimiru.app.R;

import timber.log.Timber;


public class BMIChartView extends FrameLayout {
    private float value, textSize = 0;
    private ImageView ivClockWise;
    private TextView tvContent;
    private boolean isAnimationIsRunning = false;

    public BMIChartView(@NonNull Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public BMIChartView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public BMIChartView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BMIChartView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attributeSet, int defAttr, int defStyle) {
        if (isInEditMode()) return;
        TypedArray array = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.BMIChartView, defAttr, defStyle);
        String text;
        Drawable mClockWiseRes;
        Drawable mBackgroundRes;
        try {
            mBackgroundRes = array.getDrawable(R.styleable.BMIChartView_chart_bg);
            mClockWiseRes = array.getDrawable(R.styleable.BMIChartView_chart_clockwise);
            textSize = array.getFloat(R.styleable.BMIChartView_textSize, 11f);
            text = array.getString(R.styleable.BMIChartView_text);
        } finally {
            array.recycle();
        }
        ivClockWise = new ImageView(getContext());
        tvContent = new TextView(getContext());
        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        ivClockWise.setRotation(180);
        ivClockWise.setLayoutParams(params);
        if (ivClockWise != null) {
            removeView(ivClockWise);
            ivClockWise.setImageDrawable(mClockWiseRes);
            addView(ivClockWise);
        }

        if (tvContent != null) {
            tvContent.setText(convert(text));
            tvContent.setTextSize(textSize);
            tvContent.setTextColor(Color.WHITE);
            tvContent.setTypeface(Typeface.DEFAULT_BOLD);
            tvContent.setGravity(Gravity.CENTER);
            LayoutParams pContent = new LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            pContent.gravity = Gravity.CENTER;
            tvContent.setLayoutParams(pContent);
            addView(tvContent);
        }
        setBackground(mBackgroundRes);
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    private Spanned convert(String text) {
        String content = String.format("<strong>%s</strong><br/><font size=\"2\">kg/m2</font>", text);
        return Html.fromHtml(content);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (tvContent != null && ivClockWise != null) {
            tvContent.setWidth(ivClockWise.getWidth());
            tvContent.requestLayout();
        }
    }

    public float getValue() {
        return value;
    }

    public void reset() {
        if (ivClockWise == null || tvContent == null) return;
        ivClockWise.setRotation(180);
        tvContent.setText(convert(String.valueOf(0.0)));
        this.value = 0;
    }

    public void setValue(float value) {
        if (ivClockWise == null) return;
        if (isAnimationIsRunning) {
            Log.e("action", "skip this action because the animation is running!");
            return;
        }
        float angle = calAngle(value);
        Timber.w("rotation: %s, append: %s = %s",ivClockWise.getRotation(), angle, ivClockWise.getRotation() + angle);
        float cal = ivClockWise.getRotation() + angle;
        ivClockWise.animate().rotation(cal)
                .setDuration(400L)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        isAnimationIsRunning = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        isAnimationIsRunning = false;
                        if (tvContent != null) tvContent.setText(convert(String.valueOf(value)));
                        setClockWiseColor(value);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();
    }

    private float calAngle(float value) {
        if (value >= 0 && value < 18.5) {
            return value * 90 / 18.5f;
        } else if (value >= 18.5 && value < 25) {
            return value * 180 / 25f;
        } else if (value >= 25 && value < 30) {
            return value * 270 / 30f;
        }
        return 320;
    }

    private void setClockWiseColor(float value) {
        if (value < 18.5) {
            ivClockWise.setColorFilter(getContext().getResources().getColor(R.color.bmi_color_low), PorterDuff.Mode.SRC_ATOP);
        } else if(value < 25) {
            ivClockWise.setColorFilter(getContext().getResources().getColor(R.color.bmi_color_normal), PorterDuff.Mode.SRC_ATOP);
        } else if(value < 30) {
            ivClockWise.setColorFilter(getContext().getResources().getColor(R.color.bmi_color_obesity_1), PorterDuff.Mode.SRC_ATOP);
        } else if(value < 35) {
            ivClockWise.setColorFilter(getContext().getResources().getColor(R.color.bmi_color_obesity_2), PorterDuff.Mode.SRC_ATOP);
        } else if(value < 40) {
            ivClockWise.setColorFilter(getContext().getResources().getColor(R.color.bmi_color_obesity_3), PorterDuff.Mode.SRC_ATOP);
        } else {
            ivClockWise.setColorFilter(getContext().getResources().getColor(R.color.bmi_color_obesity_4), PorterDuff.Mode.SRC_ATOP);
        }
    }
}
