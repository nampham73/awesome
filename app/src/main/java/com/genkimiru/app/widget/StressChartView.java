package com.genkimiru.app.widget;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.genkimiru.app.R;

import timber.log.Timber;


public class StressChartView extends FrameLayout {
    private ImageView ivClockWise;
    private float max, value = 0;
    private boolean isAnimationIsRunning = false;
    private int delta = -3;

    public StressChartView(@NonNull Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public StressChartView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public StressChartView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, null, defStyleAttr, defStyleRes);
    }

    public StressChartView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, null, defStyleAttr, 0);
    }

    private void init(Context context, AttributeSet attrs, int defAttrs, int defRes) {
        if (isInEditMode()) return;
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.StressChartView, defAttrs, defRes);
        try {
            int startColor = array.getColor(R.styleable.StressChartView_startColor, ContextCompat.getColor(context, R.color.start_color_stress_view));
            int endColor = array.getColor(R.styleable.StressChartView_endColor, ContextCompat.getColor(context, R.color.end_color_stress_view));
            int startCenterColor = array.getColor(R.styleable.StressChartView_endCenterCenterColor, ContextCompat.getColor(context, R.color.end_center_center_color_stress_view));
            int centerColor = array.getColor(R.styleable.StressChartView_centerColor, ContextCompat.getColor(context, R.color.center_color_stress_view));
            int endCenterColor = array.getColor(R.styleable.StressChartView_endCenterColor, ContextCompat.getColor(context, R.color.end_center_color_stress_view));
            Drawable clockWiseDrawable = array.getDrawable(R.styleable.StressChartView_clockwise);

            HaftCircleView haftCircleView = new HaftCircleView(context, startColor, endColor);
            haftCircleView.setStartColor(startColor);
            haftCircleView.setEndCenterCenterColor(startCenterColor);
            haftCircleView.setCenterColor(centerColor);
            haftCircleView.setEndCenterColor(endCenterColor);
            haftCircleView.setEndColor(endColor);

            LayoutParams params = new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            haftCircleView.setLayoutParams(params);
            addView(haftCircleView);

            ivClockWise = new ImageView(getContext());
            LayoutParams pClockWise = new LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            pClockWise.gravity = Gravity.CENTER;
            Resources resources = getContext().getResources();
            int topMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, resources.getDisplayMetrics());
            ivClockWise.setLayoutParams(pClockWise);
            ivClockWise.setPadding(topMargin, topMargin, topMargin, topMargin);
            ivClockWise.setImageDrawable(clockWiseDrawable);
            ivClockWise.setRotation(delta);
            addView(ivClockWise);
        } finally {
            array.recycle();
        }
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setValue(int value) {
        if (ivClockWise == null) return;
        if (isAnimationIsRunning) {
            Log.e("action", "skip this action because the animation is running!");
            return;
        }

        ivClockWise.setRotation(delta);
        float cal = 0;
        if (value >= max) {
            cal = 180 - delta;
            this.value = max;
        } else {
            cal = value * 180 / max;
            cal = ivClockWise.getRotation() + (cal > 0 ? cal - delta : cal);
            this.value += value;
        }

        //updateClockWise(value);
        ivClockWise.animate().rotation(cal)
                .setDuration(400L)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        isAnimationIsRunning = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        isAnimationIsRunning = false;
                        updateClockWise(value);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();
    }

    private void updateClockWise(int value) {
        float range = value / max;
        int resource = 0;
        if (range >= 0 && range <= 0.2f) resource = R.drawable.ic_needle_dgreen;
        else if (range > 0.2f && range <= 0.4f) resource = R.drawable.ic_needle_green;
        else if (range > 0.4f && range <= 0.6f) resource = R.drawable.ic_needle_yellow;
        else if (range > 0.6f && range <= 0.8f) resource = R.drawable.ic_needle_orange;
        else resource = R.drawable.ic_needle_red;
        ivClockWise.setImageResource(resource);
    }
}
