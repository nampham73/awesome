package com.genkimiru.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.genkimiru.app.base.rx.RxBus;
import com.genkimiru.app.base.util.StorageUtil;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

public class GenkiApplication extends DaggerApplication {

    private static RxSharedPreferences sPreferences = null;

    private static RxBus sRxBus = null;

    public static RxSharedPreferences getPreferences() {
        return sPreferences;
    }

    public static RxBus getEventBus() {
        return sRxBus;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeTimber();
        initializeSharePreference();
        sRxBus = new RxBus();
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig("ERG9Ss70M4VURuzUYNi2Ez8h6",
                        "2EJf9VNuc1nbZRmn54GC6dL3tc7sPSOmQY2Xx8bwikzEvwkUbm"))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

    private void initializeTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new NoLog());
        }
    }

    private void initializeSharePreference() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.registerOnSharedPreferenceChangeListener((sharedPreferences, key) -> {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            boolean ret = editor.commit();
            if (!ret) {
                Timber.w("editor.commit = false");
            }

            StorageUtil.syncFileSystem();
        });
        sPreferences = RxSharedPreferences.create(preferences);
    }
}
