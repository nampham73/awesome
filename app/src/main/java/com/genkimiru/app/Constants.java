package com.genkimiru.app;

import com.genkimiru.app.base.util.StringUtil;

public interface Constants {

    interface Argument {
        String ARG_HEALTH_SOURCE = "health-source-type";
    }

    interface Api {
        int MIN_USER_AGE = 13;
    }

    interface Input {
        int USERNAME_MIN_LENGTH = 8;
        int PASSWORD_MIN_LENGTH = 8;
    }

    interface Extra {
        String EULA_INDEX = "com.genkimiru.app.GET_EULA_INDEX";
        String GET_PHOTO_TYPE = "com.genkimiru.app.GET_PHOTO_TYPE";
        String RETAKE_CODE = "com.genkimiru.app.RETAKE_CODE";
        String IS_CONTEST_HISTORY = "com.genkimiru.app.IS_CONTEST_HISTORY";
        String CONTEST_ID = "com.genkimiru.app.CONTEST_ID";
        String ARTICLE_ID = "com.genkimiru.app.ARTICLE_ID";
        String ARTICLE_TYPE = "com.genkimiru.app.ARTICLE_TYPE";
        String ARTICLE_TITLE = "com.genkimiru.app.ARTICLE_TITLE";
        String DETAIL_LOGIN_FACEBOOK = "com.genkimiru.app.DETAIL_LOGIN_FACEBOOK";
        String CHALLENGE_HISTORY_TARGET = "com.genkimiru.app.CHALLENGE_HISTORY_TARGET";
        String CHALLENGE_HISTORY_COUNT = "com.genkimiru.app.CHALLENGE_HISTORY_COUNT";
        String CHALLENGE_HISTORY_STATUS = "com.genkimiru.app.CHALLENGE_HISTORY_STATUS";
        String CHALLENGE_HISTORY_TITLE = "com.genkimiru.app.CHALLENGE_HISTORY_TITLE";
        String CHALLENGE_HISTORY_DESCRIPTION = "com.genkimiru.app.CHALLENGE_HISTORY_TITLE";
        String CHALLENGE_USER_CONTEST_CHALLENGE_ID = "com.genkimiru.app.CHALLENGE_USER_CONTEST_CHALLENGE_ID";
        String CHALLENGE_USER_DATA = "com.genkimiru.app.CHALLENGE_USER_DATA";
        String DETAIL_LOGIN_GOOGLE = "com.genkimiru.app.DETAIL_LOGIN_GOOGLE";
        String IS_LOGIN_STATE = "com.genkimiru.app.IS_LOGIN_STATE";
        String REGISTER_USER_INFO = "com.genkimiru.app.REGISTER_USER_INFO";
        String ADVICE = "com.genkimiru.app.ADVICE";
        String GET_PHOTO_TYPE_WITH_CROP_OPTION = "com.genkimiru.app.GET_PHOTO_TYPE_WITH_CROP_OPTION";
        String CROP = "com.genkimiru.app.CROP";
    }

    interface UrlRequest {
        String PERSONAL_POLICY_URL_REQUEST = "file:///android_asset/privacypolicy.html";
        String TERM_OF_USE_REQUEST = "file:///android_asset/termofuse.html";
    }

    interface Network {
        int HTTP_STATUS_OK = 200;
        int HTTP_STATUS_UNAUTHORIZED = 401;
    }

    interface Dashboard{
        int DONE = 2;
    }

    interface Preference {
        String USER_TOKEN = "com.genkimiru.app.USER_TOKEN";
        String RECOVER_PASSWORD_CODE = "com.genkimiru.app.RECOVER_PASSWORD_CODE";
        String PHONE_STEP_COUNTER = "com.genkimiru.app.PHONE_STEP_COUNTER";
        String PHONE_LAST_STEP_COUNTER_TIME = "com.genkimiru.app.PHONE_LAST_STEP_COUNTER_TIME";
        String PHONE_LAST_STEP_COUNTER_PUSH = "com.genkimiru.app.PHONE_LAST_STEP_COUNTER_PUSH";
    }

    interface RequestCode {
        int start = 1;
        int REQUEST_CODE_CAMERA = start + 1;
        int REQUEST_CODE_GALLERY = REQUEST_CODE_CAMERA + 1;
        int REQUEST_CODE_GET_PHOTO = REQUEST_CODE_GALLERY + 1;
        int REQUEST_CODE_CROP = REQUEST_CODE_GET_PHOTO + 1;
        int REQUEST_CODE_OTHER_DEVICE = REQUEST_CODE_GET_PHOTO + 1;
        int REQUEST_CODE_GOOGLE_FIT_LOGIN = REQUEST_CODE_OTHER_DEVICE + 1;
        int REQUEST_CODE_CAMERA_NO_CROP = REQUEST_CODE_GOOGLE_FIT_LOGIN + 1;
    }

    interface FileName {
        String CAPTURE_IMAGE_NAME = "CAPTURE_IMAGE_NAME.jpg";
    }

    interface Fitbit {
        String CLIENT_ID = "22CW6Q";
        String CLIENT_SECRET = "7cbe79878ce923f8e613e259c847934b";
        String REDIRECT_URI = "genki://";
        String SCOPE = "sleep+settings+nutrition+activity+social+heartrate+profile+weight+location";
        String EXPIRES = "31536000";
        String AUTH_URI = "https://www.fitbit.com/oauth2/authorize?response_type=code"
                + "&client_id=" + CLIENT_ID
                + "&redirect_uri=" + REDIRECT_URI
                + "&scope=" + SCOPE
                + "&expires_in=" + EXPIRES;
        String GRANT_TYPE = "authorization_code";
        String GET_TOKEN_AUTH_HEADER_PREFIX = "Basic ";
        String GET_TOKEN_AUTH_HEADER = GET_TOKEN_AUTH_HEADER_PREFIX + StringUtil.toBase64(CLIENT_ID + ":" + CLIENT_SECRET);
    }

    interface Google {
        String CLIENT_ID = "23426677627-o0ajrj1jhl2vklk5mtadlfbp52hsodut.apps.googleusercontent.com";
        String CLIENT_SECRECT = "LB1EAPv0hyNZ0U3Ihp8lo5Iw";
        String REDIRECT_URI = "https://genkimiru.android/oauth2callback";
        String SCOPE = "https://www.googleapis.com/auth/fitness.activity.read+"
                + "https://www.googleapis.com/auth/fitness.blood_glucose.read+"
                + "https://www.googleapis.com/auth/fitness.blood_pressure.read+"
                + "https://www.googleapis.com/auth/fitness.body.read+"
                + "https://www.googleapis.com/auth/fitness.body_temperature.read+"
                + "https://www.googleapis.com/auth/fitness.location.read+"
                + "https://www.googleapis.com/auth/fitness.nutrition.read+"
                + "https://www.googleapis.com/auth/fitness.oxygen_saturation.read+"
                + "https://www.googleapis.com/auth/fitness.reproductive_health.read+"
                + "https://www.googleapis.com/auth/userinfo.profile";
        String GRANT_TYPE = "authorization_code";
        String AUTH_URI = "https://accounts.google.com/o/oauth2/v2/auth?"
                + "redirect_uri=" + REDIRECT_URI
                + "&prompt=consent&response_type=code"
                + "&client_id=" + CLIENT_ID
                + "&scope=" + SCOPE + "&access_type=offline";
    }

    interface Watson {
        String USERNAME = "4c0cb661-c6c9-4fae-8414-a15c5a3c99b0";
        String PASSWORD = "uRIClvrFoGWr";
        String WORKSPACE_ID = "98bf1dd8-37aa-40b7-be6e-c0c372734098";
        String URL = "https://gateway.watsonplatform.net/assistant/api";
        String DATE_CREATED = "2018-06-02";
    }

    @interface HealthSource {
        int UNKNOWN = -1;
        int GOBE = 1;
        int FITBIT = 2;
        int APPLE_KIT = 3;
        int SMART_PHONE = 4;
        int GOOGLE_FIT = 5;
    }

    interface RecyclerViewType {
        int DETAIL_ARTICLE_VIEW_TYPE = 1000;
        int FEED_VIEW_TYPE = DETAIL_ARTICLE_VIEW_TYPE + 1;
    }

    interface Facebook {
        String PARAM_LAST_NAME = "last_name";
        String PARAM_EMAIL = "email";
        String PARAM_FIRST_NAME = "first_name";
        String PARAM_GENDER = "gender";
        String PARAM_BIRTHDAY = "birthday";
        String PARAM_ID = "id";
        String PARAM_PUBLIC_PROFILE = "public_profile";
        String PARAM_PUBLIC_USER_BIRTHDAY = "user_birthday";
        String PARAM_PUBLIC_USER_GENDER = "user_gender";
        String AVATAR_URL_FACEBOOK = "https://graph.facebook.com";
        String AVATAR_URL = "https://graph.facebook.com/%s/picture?type=large";
        String PARAM_FIELDS = "fields";
        String PARAM_ALL_FIELDS = "email,first_name,last_name,gender,birthday,id";
    }

    interface LoginEcommerce {
        String DATA_SERVER_CHECK_TOKEN = "/authentication/account/fastlogin?token=";
    }

    interface ChallengePermission {
        int REQUEST_LOCATION = 1;
    }
}
