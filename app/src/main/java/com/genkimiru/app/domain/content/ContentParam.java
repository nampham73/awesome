package com.genkimiru.app.domain.content;

import com.genkimiru.app.data.remote.genki.request.ContentDetailRequest;
import com.genkimiru.app.data.remote.genki.request.ListContentRequest;

public class ContentParam {

    private ListContentRequest mListContentRequest;
    private ContentDetailRequest mFeedContentRequest;
    private ContentDetailRequest mArticleContentRequest;
    private ContentDetailRequest mContentDetailRequest;
    private Type mType;

    private ContentParam(ListContentRequest listContentRequest, Type type) {
        mListContentRequest = listContentRequest;
        mType = type;
    }

    private ContentParam(ContentDetailRequest request, Type type){
        mContentDetailRequest = request;
        mType = type;
    }

    private ContentParam(ContentDetailRequest articleParam, ContentDetailRequest feedParam) {
        mArticleContentRequest = articleParam;
        mFeedContentRequest = feedParam;
        mType = Type.GET_DETAIL;
    }

    public ListContentRequest getListContentRequest() {
        return mListContentRequest;
    }

    public ContentDetailRequest getArticleContentRequest() {
        return mArticleContentRequest;
    }

    public ContentDetailRequest getFeedContentRequest() {
        return mFeedContentRequest;
    }

    public ContentDetailRequest getActionRequest() {
        return mContentDetailRequest;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        GET_LIST,
        GET_FAVORITE_LIST,
        GET_DETAIL,
        REQUEST_LIKE,
        REQUEST_UNLIKE,
        REQUEST_ADD_COMMENT
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static ContentParam getListContent(ListContentRequest listContentRequest) {
            return new ContentParam(listContentRequest, Type.GET_LIST);
        }

        public static ContentParam getListFavoriteContent(ListContentRequest listContentRequest) {
            return new ContentParam(listContentRequest, Type.GET_FAVORITE_LIST);
        }

        public static ContentParam getDetailArticle(ContentDetailRequest articleParam, ContentDetailRequest feedParam) {
            return new ContentParam(articleParam, feedParam);
        }

        public static ContentParam requestLike(ContentDetailRequest maps) {
            return new ContentParam(maps, Type.REQUEST_LIKE);
        }

        public static ContentParam requestUnLike(ContentDetailRequest maps) {
            return new ContentParam(maps, Type.REQUEST_UNLIKE);
        }

        public static ContentParam requestAddComment(ContentDetailRequest request) {
            return new ContentParam(request, Type.REQUEST_ADD_COMMENT);
        }
    }
}
