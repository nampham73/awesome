package com.genkimiru.app.domain.content;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.remote.genki.request.ContentDetailRequest;
import com.genkimiru.app.data.repository.content.ContentRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class ContentUseCase extends UseCase<ContentRepository, ContentParam> {

    public ContentUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, ContentRepository contentRepository) {
        super(backgroundScheduler, postedScheduler, contentRepository);
    }

    @Override
    protected Flowable buildUseCase(ContentParam contentParam) {
        switch (contentParam.getType()) {
            case GET_LIST:
                return mRepository.fetchingContents(contentParam.getListContentRequest());
            case GET_FAVORITE_LIST:
                return mRepository.fetchingFavoriteContents(contentParam.getListContentRequest());
            case GET_DETAIL:
                ContentDetailRequest article = contentParam.getArticleContentRequest();
                ContentDetailRequest feeds = contentParam.getFeedContentRequest();
                return mRepository.getArticleAndFeeds(article, feeds);
            case REQUEST_LIKE:
                return mRepository.requestLike(contentParam.getActionRequest());
            case REQUEST_UNLIKE:
                return mRepository.requestUnLike(contentParam.getActionRequest());
            case REQUEST_ADD_COMMENT:
                return mRepository.requestAddComment(contentParam.getActionRequest());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
