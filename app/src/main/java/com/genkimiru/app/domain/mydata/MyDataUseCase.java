package com.genkimiru.app.domain.mydata;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.repository.mydata.MyDataRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class MyDataUseCase extends UseCase<MyDataRepository, MyDataParam> {

    public MyDataUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, MyDataRepository myDataRepository) {
        super(backgroundScheduler, postedScheduler, myDataRepository);
    }

    @Override
    protected Flowable buildUseCase(MyDataParam myDataParam) {
        switch (myDataParam.getType()) {
            case DASHBOARD:
                return mRepository.fetchingDashboard(myDataParam.getDashboardRequest());
            case DETAIL_SLEEP:
                return mRepository.fetchingDetailSleep(myDataParam.getDetailChartDataRequest());
            case DETAIL_STEP:
                return mRepository.fetchingDetailStep(myDataParam.getDetailChartDataRequest());
            case DETAIl_NUTRITION:
                return  mRepository.fetchingDetailNutrition(myDataParam.getDetailChartDataRequest());
            case ADD_MANUAL_NUTRITION:
                return  mRepository.addNutritionManualInputData(myDataParam.getAddManualInputRequest());
            case DELETE_MANUAL_NUTRITION:
                return  mRepository.deleteNutritionManualInputData(myDataParam.getDeleteMunualInputRequest());
            case UPDATE_MANUAL_NITRITION:
                return  mRepository.updateNutritionManualInputData(myDataParam.getUpdateManualInputRequest());
            case DETAIL_ENERGY:
                return mRepository.fetchingDetailEnergy(myDataParam.getDetailChartDataRequest());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
