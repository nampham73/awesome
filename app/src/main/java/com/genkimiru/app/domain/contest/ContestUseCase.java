package com.genkimiru.app.domain.contest;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.repository.contest.ContestRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class ContestUseCase extends UseCase<ContestRepository, ContestParam> {

    public ContestUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, ContestRepository contestRepository) {
        super(backgroundScheduler, postedScheduler, contestRepository);
    }

    @Override
    protected Flowable buildUseCase(ContestParam contestParam) {
        switch (contestParam.getType()) {
            case LIST_CONTEST:
                return mRepository.fetchingContestList(contestParam.getListContestRequest());
            case CONTEST_DETAIL:
                return mRepository.fetchingContestDetail(contestParam.getId(), contestParam.getToken());
            case PRIVATE_PUBLIC_CONTEST:
                return mRepository.fetchingContestPrivateOrPublic(contestParam.getListContestRequest());
            case HISTORY_CONTEST:
                return mRepository.fetchingContestHistories(contestParam.getListContestRequest());
            case CURRENT_USER_CONTEST:
                return mRepository.fetchingContestCurrentUser(contestParam.getListContestRequest());
            case RECOMMEND_CONTEST:
                return mRepository.fetchingContestRecommend(contestParam.getListContestRequest());
            case JOIN_CONTEST:
                return mRepository.joinContest(new JoinRequest(contestParam.getId(), contestParam.getToken()));
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
