package com.genkimiru.app.domain.fitbit;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.repository.fitbit.FitbitRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class FitbitUseCase extends UseCase<FitbitRepository, FitbitParam> {

    public FitbitUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, FitbitRepository fitbitRepository) {
        super(backgroundScheduler, postedScheduler, fitbitRepository);
    }

    @Override
    protected Flowable buildUseCase(FitbitParam fitbitParam) {
        switch (fitbitParam.getType()) {
            case GET_ACCESS_TOKEN:
                return mRepository.getAccessToken(fitbitParam.getFitbitAccessTokenRequest());
            case GET_USER_PROFILE:
                return mRepository.getUserProfile(fitbitParam.getFitbitUserProfileRequest());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
