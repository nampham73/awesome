package com.genkimiru.app.domain.google;

import com.genkimiru.app.data.remote.google.request.GoogleAccessTokenRequest;
import com.genkimiru.app.data.remote.google.request.GoogleFitUserProfileRequest;

public class GoogleParam {

    private GoogleAccessTokenRequest mGoogleAccessTokenRequest;
    private GoogleFitUserProfileRequest mGoogleFitUserProfileRequest;
    private String mImageUrl;
    private Type mType;

    private GoogleParam(GoogleAccessTokenRequest googleAccessTokenRequest) {
        mGoogleAccessTokenRequest = googleAccessTokenRequest;
        mType = Type.GOOGLE_FIT_GET_ACCESS_TOKEN;
    }

    private GoogleParam(GoogleFitUserProfileRequest googleFitUserProfileRequest) {
        mGoogleFitUserProfileRequest = googleFitUserProfileRequest;
        mType = Type.GOOGLE_FIT_GET_USER_PROFILE;
    }

    public GoogleAccessTokenRequest getGoogleFitAccessTokenRequest() {
        return mGoogleAccessTokenRequest;
    }

    public GoogleFitUserProfileRequest getmGoogleFitUserProfileRequest() {
        return mGoogleFitUserProfileRequest;
    }

    private GoogleParam(String imageUrlRequest) {
        mImageUrl = imageUrlRequest;
        mType = Type.DOWNLOAD_IMAGES;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        GOOGLE_FIT_GET_ACCESS_TOKEN,
        GOOGLE_FIT_GET_USER_PROFILE,
        DOWNLOAD_IMAGES
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static GoogleParam getAccessToken(GoogleAccessTokenRequest googleAccessTokenRequest) {
            return new GoogleParam(googleAccessTokenRequest);
        }

        public static GoogleParam getUserProfile(GoogleFitUserProfileRequest googleFitUserProfileRequest) {
            return new GoogleParam(googleFitUserProfileRequest);
        }

        public static GoogleParam downloadImage(String imageUrlRequest) {
            return new GoogleParam(imageUrlRequest);
        }
    }
}
