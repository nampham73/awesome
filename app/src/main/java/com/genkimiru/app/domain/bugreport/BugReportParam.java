package com.genkimiru.app.domain.bugreport;

import com.genkimiru.app.data.remote.extension.request.BugReportRequest;
import com.genkimiru.app.data.remote.extension.request.BugReportUploadImageRequest;

import java.util.List;

public class BugReportParam {

    private BugReportRequest mBugReportRequest;
    private String mFaqRequest;
    private List<BugReportUploadImageRequest> mBugReportUploadImageRequests;
    private Type mType;

    public BugReportParam(String faqRequest) {
        mFaqRequest = faqRequest;
        mType = Type.GET_FAQ;
    }

    public BugReportParam(BugReportRequest bugReportRequest) {
        mBugReportRequest = bugReportRequest;
        mType = Type.REPORT_BUG;
    }

    public BugReportParam(List<BugReportUploadImageRequest> bugReportUploadImageRequests) {
        mBugReportUploadImageRequests = bugReportUploadImageRequests;
        mType = Type.UPLOAD_IMAGE;
    }

    public BugReportRequest getBugReportRequests() {
        return mBugReportRequest;
    }

    public List<BugReportUploadImageRequest> getBugReportUploadImageRequest() {
        return mBugReportUploadImageRequests;
    }

    public String getFaqRequest() {
        return mFaqRequest;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        REPORT_BUG,
        UPLOAD_IMAGE,
        GET_FAQ
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static BugReportParam reportBug(BugReportRequest bugReportRequest) {
            return new BugReportParam(bugReportRequest);
        }

        public static BugReportParam uploadImage(List<BugReportUploadImageRequest> bugReportUploadImageRequests) {
            return new BugReportParam(bugReportUploadImageRequests);
        }

        public static BugReportParam getFaq(String faqRequest) {
            return new BugReportParam(faqRequest);
        }
    }
}
