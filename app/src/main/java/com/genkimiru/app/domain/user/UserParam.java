package com.genkimiru.app.domain.user;

import com.genkimiru.app.data.remote.genki.request.ActivePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.ChallengeRequest;
import com.genkimiru.app.data.remote.genki.request.ChangePasswordRequest;
import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.LoginFacebookRequest;
import com.genkimiru.app.data.remote.genki.request.LoginRequest;
import com.genkimiru.app.data.remote.genki.request.LoginWithGoogleRequest;
import com.genkimiru.app.data.remote.genki.request.NotificationSettingRequest;
import com.genkimiru.app.data.remote.genki.request.PrivacySettingRequest;
import com.genkimiru.app.data.remote.genki.request.RecoverPasswordRequest;
import com.genkimiru.app.data.remote.genki.request.RegisterUserRequest;
import com.genkimiru.app.data.remote.genki.request.SettingEmailMarketingRequest;
import com.genkimiru.app.data.remote.genki.request.UserInfoRequest;

import static com.genkimiru.app.domain.user.UserParam.Type.ADD_MANUAL_INPUT;
import static com.genkimiru.app.domain.user.UserParam.Type.DOWNLOAD_IMAGES;
import static com.genkimiru.app.domain.user.UserParam.Type.GET_USER_INFORMATION;
import static com.genkimiru.app.domain.user.UserParam.Type.HEALTH_SOURCE_SETTING;
import static com.genkimiru.app.domain.user.UserParam.Type.UPDATE_USER_AVATAR;
import static com.genkimiru.app.domain.user.UserParam.Type.UPDATE_USER_PROFILE;

public class UserParam {

    private LoginRequest mLoginRequest;
    private RegisterUserRequest mRegisterUserRequest;
    private RecoverPasswordRequest mRecoverPasswordRequest;
    private ActivePasswordRequest mActivePasswordRequest;
    private PrivacySettingRequest mPrivacySettingRequest;
    private SettingEmailMarketingRequest mSettingEmailMarketingRequest;
    private ChangePasswordRequest mChangePasswordRequest;
    private NotificationSettingRequest mNotificationSettingRequest;
    private UserInfoRequest mHealthSourceSettingRequest;
    private String mToken;
    private String mImageUrl;
    private Type mType;
    private LoginFacebookRequest mFacebookToken;
    private AddManualInputRequest mAddManualInputRequest;
    private LoginWithGoogleRequest mLoginWithGoogleRequest;
    private ChallengeRequest mChallengeRequest;

    public LoginWithGoogleRequest getmLoginWithGoogleRequest() {
        return mLoginWithGoogleRequest;
    }

    public void setmLoginWithGoogleRequest(LoginWithGoogleRequest mLoginWithGoogleRequest) {
        this.mLoginWithGoogleRequest = mLoginWithGoogleRequest;
    }

    private UserParam(LoginRequest loginRequest) {
        mLoginRequest = loginRequest;
        mType = Type.LOGIN;
    }

    private UserParam(LoginFacebookRequest requestToken) {
        mFacebookToken = requestToken;
        mType = Type.LOGIN_FACEBOOK;
    }

    private UserParam(RegisterUserRequest registerUserRequest) {
        mRegisterUserRequest = registerUserRequest;
        mType = Type.REGISTER;
    }

    private UserParam(RecoverPasswordRequest recoverPasswordRequest) {
        mRecoverPasswordRequest = recoverPasswordRequest;
        mType = Type.RECOVER_PASSWORD;
    }

    private UserParam(ActivePasswordRequest activePasswordRequest) {
        mActivePasswordRequest = activePasswordRequest;
        mType = Type.ACTIVE_PASSWORD;
    }

    private UserParam(PrivacySettingRequest privacySettingRequest) {
        mPrivacySettingRequest = privacySettingRequest;
        mType = Type.UPDATE_PRIVACY_SETTING;
    }

    private UserParam(SettingEmailMarketingRequest settingEmailMarketingRequest) {
        mSettingEmailMarketingRequest = settingEmailMarketingRequest;
        mType = Type.UPDATE_EMAIL_MARKETING_SETTING;
    }

    private UserParam(ChangePasswordRequest changePasswordRequest) {
        mChangePasswordRequest = changePasswordRequest;
        mType = Type.CHANGE_PASSWORD;
    }

    private UserParam(LoginWithGoogleRequest loginWithGoogleRequest) {
        mLoginWithGoogleRequest = loginWithGoogleRequest;
        mType = Type.LOGIN_GOOGLE;
    }

    private UserParam(UserInfoRequest userInfoRequest) {
        mHealthSourceSettingRequest = userInfoRequest;
        mType = Type.UPDATE_USER_AVATAR;
    }

    private UserParam(Type type, String request) {
        switch (type) {
            case GET_USER_INFORMATION:
                mToken = request;
                break;
            case DOWNLOAD_IMAGES:
                mImageUrl = request;
                break;
        }
        mType = type;
    }

    private UserParam(NotificationSettingRequest notificationSettingRequest) {
        mNotificationSettingRequest = notificationSettingRequest;
        mType = Type.UPDATE_NOTIFICATION;
    }

    private UserParam(Type type, UserInfoRequest map) {
        mHealthSourceSettingRequest = map;
        mType = type;
    }

    private UserParam(AddManualInputRequest addManualInputRequest) {
        mAddManualInputRequest = addManualInputRequest;
        mType = ADD_MANUAL_INPUT;
    }

    private UserParam(ChallengeRequest challengeRequest) {
        mChallengeRequest = challengeRequest;
        mType = Type.PROCESS_CHALLENGE;
    }

    public LoginRequest getLoginRequest() {
        return mLoginRequest;
    }

    public LoginFacebookRequest getFacebookToken() {
        return mFacebookToken;
    }

    public RegisterUserRequest getRegisterUserRequest() {
        return mRegisterUserRequest;
    }

    public RecoverPasswordRequest getRecoverPasswordRequest() {
        return mRecoverPasswordRequest;
    }

    public ActivePasswordRequest getActivePasswordRequest() {
        return mActivePasswordRequest;
    }

    public PrivacySettingRequest getPrivacySettingRequest() {
        return mPrivacySettingRequest;
    }

    public SettingEmailMarketingRequest getSettingEmailMarketingRequest() {
        return mSettingEmailMarketingRequest;
    }

    public ChangePasswordRequest getChangePasswordRequest() {
        return mChangePasswordRequest;
    }

    public String getToken() {
        return mToken;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public NotificationSettingRequest getNotificationSettingRequest() {
        return mNotificationSettingRequest;
    }

    public UserInfoRequest getHealthSourceSettingRequest() {
        return mHealthSourceSettingRequest;
    }

    public AddManualInputRequest getAddManualInputRequest() {
        return mAddManualInputRequest;
    }

    public ChallengeRequest getChallengeRequest() {
        return mChallengeRequest;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        LOGIN,
        REGISTER,
        RECOVER_PASSWORD,
        ACTIVE_PASSWORD,
        UPDATE_PRIVACY_SETTING,
        CHANGE_PASSWORD,
        UPDATE_NOTIFICATION,
        GET_USER_INFORMATION,
        DOWNLOAD_IMAGES,
        UPDATE_EMAIL_MARKETING_SETTING,
        HEALTH_SOURCE_SETTING,
        LOGIN_FACEBOOK,
        ADD_MANUAL_INPUT,
        LOGIN_GOOGLE,
        UPDATE_USER_PROFILE,
        UPDATE_USER_AVATAR,
        PROCESS_CHALLENGE
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static UserParam login(LoginRequest loginRequest) {
            return new UserParam(loginRequest);
        }

        public static UserParam register(RegisterUserRequest registerUserRequest) {
            return new UserParam(registerUserRequest);
        }

        public static UserParam recoverPassword(RecoverPasswordRequest recoverPasswordRequest) {
            return new UserParam(recoverPasswordRequest);
        }

        public static UserParam activePassword(ActivePasswordRequest activePasswordRequest) {
            return new UserParam(activePasswordRequest);
        }

        public static UserParam updatePrivacySetting(PrivacySettingRequest privacySettingRequest) {
            return new UserParam(privacySettingRequest);
        }

        public static UserParam updateEmailMarketingSetting(SettingEmailMarketingRequest settingEmailMarketingRequest) {
            return new UserParam(settingEmailMarketingRequest);
        }

        public static UserParam changePassword(ChangePasswordRequest changePasswordRequest) {
            return new UserParam(changePasswordRequest);
        }

        public static UserParam getUserInformation(String token) {
            return new UserParam(GET_USER_INFORMATION, token);
        }

        public static UserParam downloadImage(String imageUrlRequest) {
            return new UserParam(DOWNLOAD_IMAGES, imageUrlRequest);
        }

        public static UserParam updateNotificationSetting(NotificationSettingRequest notificationSettingRequest) {
            return new UserParam(notificationSettingRequest);
        }

        public static UserParam updateHealthSourceSetting(UserInfoRequest healthSourceSettingRequest) {
            return new UserParam(HEALTH_SOURCE_SETTING, healthSourceSettingRequest);
        }

        public static UserParam loginFacebook(String requesToken) {
            return new UserParam(new LoginFacebookRequest(requesToken));
        }

        public static UserParam addManualInput(AddManualInputRequest addManualInputRequest) {
            return new UserParam(addManualInputRequest);
        }

        public static UserParam loginGoogle(LoginWithGoogleRequest request) {
            return new UserParam(request);
        }

        public static UserParam updateUserProfile(UserInfoRequest userInfoRequest) {
            return new UserParam(UPDATE_USER_PROFILE, userInfoRequest);
        }

        public static UserParam updateUserAvatar(UserInfoRequest userInfoRequest) {
            return new UserParam(userInfoRequest);
        }

        public static UserParam processChallenge(ChallengeRequest challengeRequest) {
            return new UserParam(challengeRequest);
        }
    }
}
