package com.genkimiru.app.domain.mydata;

import com.genkimiru.app.data.remote.genki.request.AddManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DashboardRequest;
import com.genkimiru.app.data.remote.genki.request.DeleteManualInputRequest;
import com.genkimiru.app.data.remote.genki.request.DetailChartDataRequest;
import com.genkimiru.app.data.remote.genki.request.UpdateManualInputRequest;

public class MyDataParam {
    private DashboardRequest mDashboardRequest;
    private DetailChartDataRequest mDetailChartDataRequest;
    private AddManualInputRequest mAddManualInputRequest;
    private DeleteManualInputRequest mDeleteManualInputRequest;
    private UpdateManualInputRequest mUpdateManualInputRequest;
    private Type mType;

    private MyDataParam(DashboardRequest request) {
        mType = Type.DASHBOARD;
        mDashboardRequest = request;
    }

    public MyDataParam(UpdateManualInputRequest mUpdateManualInputRequest, Type mType) {
        this.mUpdateManualInputRequest = mUpdateManualInputRequest;
        this.mType = mType;
    }

    public MyDataParam(DeleteManualInputRequest mDeleteManualInputRequest, Type mType) {
        this.mDeleteManualInputRequest = mDeleteManualInputRequest;
        this.mType = mType;
    }

    private MyDataParam(DetailChartDataRequest request, Type type){
        mDetailChartDataRequest = request;
        mType = type;
    }
    private MyDataParam(AddManualInputRequest request, Type type) {
        mAddManualInputRequest = request;
        mType = type;
    }



    public Type getType() {
        return mType;
    }

    public DashboardRequest getDashboardRequest() {
        return mDashboardRequest;
    }

    public DetailChartDataRequest getDetailChartDataRequest() {
        return mDetailChartDataRequest;
    }

    public AddManualInputRequest getAddManualInputRequest() {
        return mAddManualInputRequest;
    }

    public UpdateManualInputRequest getUpdateManualInputRequest() {
        return mUpdateManualInputRequest;
    }

    public DeleteManualInputRequest getDeleteMunualInputRequest() {
        return mDeleteManualInputRequest;
    }

    public enum Type {
        DASHBOARD,
        DETAIL_SLEEP,
        DETAIL_STEP,
        DETAIL_ENERGY,
        DETAIl_NUTRITION,
        ADD_MANUAL_NUTRITION,
        DELETE_MANUAL_NUTRITION,
        UPDATE_MANUAL_NITRITION
    }


    public static class Factory {
        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static MyDataParam fetchingDashboard(DashboardRequest request) {
            return new MyDataParam(request);
        }

        public static MyDataParam fetchingSleepChartData(DetailChartDataRequest request) {
            return new MyDataParam(request, Type.DETAIL_SLEEP);
        }

        public static MyDataParam fetchingStepChartData(DetailChartDataRequest request) {
            return new MyDataParam(request, Type.DETAIL_STEP);
        }

        public static MyDataParam fetchingNutritionChartData(DetailChartDataRequest request) {
            return new MyDataParam(request, Type.DETAIl_NUTRITION);
        }

        public static MyDataParam addNutritionManualinputData(AddManualInputRequest request) {
            return new MyDataParam(request, Type.ADD_MANUAL_NUTRITION);
        }

        public static MyDataParam deleteNutritionManualInputData(DeleteManualInputRequest request) {
            return  new MyDataParam(request, Type.DELETE_MANUAL_NUTRITION);
        }

        public static MyDataParam updateNutritionManualInputData(UpdateManualInputRequest request) {
            return new MyDataParam(request, Type.UPDATE_MANUAL_NITRITION);
        }

        public static MyDataParam fetchingEnergyChartData(DetailChartDataRequest request) {
            return new MyDataParam(request, Type.DETAIL_ENERGY);
        }
    }
}
