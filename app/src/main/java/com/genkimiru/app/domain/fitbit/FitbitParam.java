package com.genkimiru.app.domain.fitbit;

import com.genkimiru.app.data.remote.fitbit.request.FitbitAccessTokenRequest;
import com.genkimiru.app.data.remote.fitbit.request.FitbitUserProfileRequest;

public class FitbitParam {

    private FitbitAccessTokenRequest mFitbitAccessTokenRequest;
    private FitbitUserProfileRequest mFitbitUserProfileRequest;
    private Type mType;

    private FitbitParam(FitbitAccessTokenRequest fitbitAccessTokenRequest) {
        mFitbitAccessTokenRequest = fitbitAccessTokenRequest;
        mType = Type.GET_ACCESS_TOKEN;
    }

    private FitbitParam(FitbitUserProfileRequest fitbitUserProfileRequest) {
        mFitbitUserProfileRequest = fitbitUserProfileRequest;
        mType = Type.GET_USER_PROFILE;
    }

    public FitbitAccessTokenRequest getFitbitAccessTokenRequest() {
        return mFitbitAccessTokenRequest;
    }

    public FitbitUserProfileRequest getFitbitUserProfileRequest() {
        return mFitbitUserProfileRequest;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        GET_ACCESS_TOKEN,
        GET_USER_PROFILE
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static FitbitParam getAccessToken(FitbitAccessTokenRequest fitbitAccessTokenRequest) {
            return new FitbitParam(fitbitAccessTokenRequest);
        }

        public static FitbitParam getUserProfile(FitbitUserProfileRequest fitbitUserProfileRequest) {
            return new FitbitParam(fitbitUserProfileRequest);
        }
    }
}
