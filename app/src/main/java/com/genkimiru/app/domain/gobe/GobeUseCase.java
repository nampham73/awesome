package com.genkimiru.app.domain.gobe;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.repository.gobe.GobeRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class GobeUseCase extends UseCase<GobeRepository, GobeParam> {

    public GobeUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, GobeRepository gobeRepository) {
        super(backgroundScheduler, postedScheduler, gobeRepository);
    }

    @Override
    protected Flowable buildUseCase(GobeParam gobeParam) {
        switch (gobeParam.getType()) {
            case AUTHORIZATION:
                return mRepository.authorize(gobeParam.getAuthorizationRequest());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
