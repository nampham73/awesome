package com.genkimiru.app.domain.bugreport;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.repository.bugreport.BugReportRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class BugReportUseCase extends UseCase<BugReportRepository, BugReportParam> {

    public BugReportUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, BugReportRepository bugReportRepository) {
        super(backgroundScheduler, postedScheduler, bugReportRepository);
    }

    @Override
    protected Flowable buildUseCase(BugReportParam bugReportParam) {
        switch (bugReportParam.getType()) {
            case REPORT_BUG:
                return mRepository.reportBug(bugReportParam.getBugReportRequests());
            case UPLOAD_IMAGE:
                return mRepository.uploadBugImage(bugReportParam.getBugReportUploadImageRequest());
            case GET_FAQ:
                return mRepository.getFaq(bugReportParam.getFaqRequest());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
