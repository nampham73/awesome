package com.genkimiru.app.domain.google;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.repository.googlefit.GoogleFitRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class GoogleUseCase extends UseCase<GoogleFitRepository, GoogleParam> {

    public GoogleUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, GoogleFitRepository googleFitRepository) {
        super(backgroundScheduler, postedScheduler, googleFitRepository);
    }

    @Override
    protected Flowable buildUseCase(GoogleParam googleParam) {
        switch (googleParam.getType()) {
            case GOOGLE_FIT_GET_ACCESS_TOKEN:
                return mRepository.getAccessToken(googleParam.getGoogleFitAccessTokenRequest());
            case GOOGLE_FIT_GET_USER_PROFILE:
                return mRepository.getUserProfile(googleParam.getmGoogleFitUserProfileRequest());
            case DOWNLOAD_IMAGES:
                return mRepository.getImageRequest(googleParam.getImageUrl());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
