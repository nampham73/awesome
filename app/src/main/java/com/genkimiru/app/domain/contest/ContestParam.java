package com.genkimiru.app.domain.contest;

import com.genkimiru.app.data.remote.genki.request.JoinRequest;
import com.genkimiru.app.data.remote.genki.request.ListContestRequest;

public class ContestParam {
    private Type mType;
    private ListContestRequest mRequest;
    private String mToken;
    private int id;
    private ListContestRequest mHistoryContest;

    private ContestParam(Type type, ListContestRequest request) {
        this.mType = type;
        this.mRequest = request;
    }

    private ContestParam(int id, String token) {
        this.mType = Type.CONTEST_DETAIL;
        this.id = id;
        this.mToken = token;
    }

    private ContestParam(Type type, JoinRequest request) {
        this.mType = type;
        this.id = request.contest_id;
        this.mToken = request.token;
    }

    public enum Type {
        LIST_CONTEST,
        CONTEST_DETAIL,
        PRIVATE_PUBLIC_CONTEST,
        HISTORY_CONTEST,
        CURRENT_USER_CONTEST,
        RECOMMEND_CONTEST,
        JOIN_CONTEST
    }

    public Type getType() {
        return mType;
    }

    public String getToken() {
        return mToken;
    }

    public int getId() {
        return id;
    }

    public ListContestRequest getListContestRequest() {
        return mRequest;
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static ContestParam getContestPrivateOrPublic(ListContestRequest request) {
            return new ContestParam(Type.PRIVATE_PUBLIC_CONTEST, request);
        }

        public static ContestParam getContestHistories(ListContestRequest request) {
            return new ContestParam(Type.HISTORY_CONTEST, request);
        }

        public static ContestParam getContestCurrentUser(ListContestRequest request) {
            return new ContestParam(Type.CURRENT_USER_CONTEST, request);
        }

        public static ContestParam getContestRecommend(ListContestRequest request) {
            return new ContestParam(Type.RECOMMEND_CONTEST, request);
        }

        public static ContestParam getContestDetail(int id, String token) {
            return new ContestParam(id, token);
        }

        public static ContestParam joinContest(JoinRequest request) {
            return new ContestParam(Type.JOIN_CONTEST, request);
        }
    }
}
