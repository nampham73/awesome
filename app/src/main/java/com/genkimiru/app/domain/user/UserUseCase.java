package com.genkimiru.app.domain.user;

import com.genkimiru.app.base.interactor.UseCase;
import com.genkimiru.app.data.repository.user.UserRepository;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;

public class UserUseCase extends UseCase<UserRepository, UserParam> {

    public UserUseCase(Scheduler backgroundScheduler, Scheduler postedScheduler, UserRepository userRepository) {
        super(backgroundScheduler, postedScheduler, userRepository);
    }

    @Override
    protected Flowable buildUseCase(UserParam userParam) {
        switch (userParam.getType()) {
            case LOGIN:
                return mRepository.loginAndFetchingUserInfo(userParam.getLoginRequest());
            case REGISTER:
                return mRepository.register(userParam.getRegisterUserRequest());
            case RECOVER_PASSWORD:
                return mRepository.recoverPassword(userParam.getRecoverPasswordRequest());
            case ACTIVE_PASSWORD:
                return mRepository.activePassword(userParam.getActivePasswordRequest());
            case UPDATE_PRIVACY_SETTING:
                return mRepository.updatePrivacySetting(userParam.getPrivacySettingRequest());
            case UPDATE_EMAIL_MARKETING_SETTING:
                return mRepository.updateEmailMarketingSetting(userParam.getSettingEmailMarketingRequest());
            case CHANGE_PASSWORD:
                return mRepository.changePassword(userParam.getChangePasswordRequest());
            case GET_USER_INFORMATION:
                return mRepository.getUserInformation(userParam.getToken());
            case DOWNLOAD_IMAGES:
                return mRepository.downloadImage(userParam.getImageUrl());
            case UPDATE_NOTIFICATION:
                return mRepository.updateNotificationSetting(userParam.getNotificationSettingRequest());
            case HEALTH_SOURCE_SETTING:
                return mRepository.updateHealthSourceSetting(userParam.getHealthSourceSettingRequest());
            case LOGIN_FACEBOOK:
                return mRepository.loginFacebook(userParam.getFacebookToken());
            case ADD_MANUAL_INPUT:
                return mRepository.addManualInput(userParam.getAddManualInputRequest());
            case LOGIN_GOOGLE:
                return mRepository.loginGoogle(userParam.getmLoginWithGoogleRequest());
            case UPDATE_USER_PROFILE:
                return mRepository.updateUserProfile(userParam.getHealthSourceSettingRequest());
            case UPDATE_USER_AVATAR:
                return mRepository.updateUserAvatar(userParam.getHealthSourceSettingRequest());
            case PROCESS_CHALLENGE:
                return mRepository.processChallenge(userParam.getChallengeRequest());
            default:
                throw new UnsupportedOperationException("This request is not support in this case!");
        }
    }
}
