package com.genkimiru.app.domain.gobe;


import com.genkimiru.app.data.remote.gobe.request.GobeAuthorizationRequest;

public class GobeParam {

    private GobeAuthorizationRequest mAuthorizationRequest;
    private Type mType;

    private GobeParam(GobeAuthorizationRequest gobeAuthorizationRequest) {
        mAuthorizationRequest = gobeAuthorizationRequest;
        mType = Type.AUTHORIZATION;
    }

    public GobeAuthorizationRequest getAuthorizationRequest() {
        return mAuthorizationRequest;
    }

    public Type getType() {
        return mType;
    }

    public enum Type {
        AUTHORIZATION
    }

    public static class Factory {

        private Factory() {
            throw new IllegalAccessError("Factory class");
        }

        public static GobeParam authorization(GobeAuthorizationRequest request){
            return new GobeParam(request);
        }
    }
}
