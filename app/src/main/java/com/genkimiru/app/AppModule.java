package com.genkimiru.app;

import android.content.Context;

import com.genkimiru.app.base.scheduler.SchedulerProvider;
import com.genkimiru.app.data.model.GenkiContestDetailModel;
import com.genkimiru.app.data.model.GenkiUserInfoModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(GenkiApplication application){
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    SchedulerProvider provideScheduler(){
        return new SchedulerProvider();
    }

    @Provides
    @Singleton
    GenkiUserInfoModel provideGenkiUserInfo(){
        return new GenkiUserInfoModel();
    }

    @Provides
    @Singleton
    GenkiContestDetailModel provideGenkiContestDetail(){
        return new GenkiContestDetailModel();
    }
}
